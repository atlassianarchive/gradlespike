plan(key:'REST', name:'BD stageBac', description:'Build and Deploy bamboo-plan-templates to stageBac') {
    project(key:'PLANTEMPLATES', name:'Plan Templates', description:'Plan Templates Project')
    repository(name:'bamboo-plan-templates')
    trigger(description:'polling trigger', type:'polling', strategy:'periodically', frequency:'15'){
        repository(name:'bamboo-plan-templates')
    }

    stage(name:"build", description: "build the plugin"){
        job(name: "build", key: 'build', description: 'build' ){
            artifactDefinition(name:'Plugin Jar', location:'target', pattern:'bamboo-plan-templates-*.jar', shared:'true')
            //artifactDefinition(name:'Plugin Jar', pattern:'readme.txt', shared:'true')
            
            task(type:'checkout', description:'Checkout Default Repository'){
                repository(name:'bamboo-plan-templates')
            }
            task(type:'maven2', description:'Build and Test', goal:'clean verify', hasTests:'true', mavenExecutable:'Maven 2', buildJdk:'JDK 1.6')
        }
    }
    stage(name:"deploy", description: "deploy on stageBac"){
        job(name: "job2", key: 'job2Key', description: 'job21description' ){
            artifactSubscription(name:'Plugin Jar', destination:'./plugin')
            
            task(type:'deployBambooPlugin', description:'Upload the plugin', artifact:'Plugin Jar', url:'http://davids-mac-pro.dyn.syd.atlassian.com:6990/bamboo', username:'${bamboo.username}', passwordVariable:'${bamboo.password}')
        }
    }
}
