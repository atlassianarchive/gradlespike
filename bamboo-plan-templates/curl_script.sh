response=`curl -X POST --data-binary @stagebac.template.groovy https://staging-bamboo.atlassian.com/rest/plantemplate/1.0/?os_authType=basic --header "Content-Type:text/plain" --user ${bamboo.user}:${bamboo.password}`

echo $response

if [ "$(echo $response | grep -F '<status-code>500</status-code>')" = "" ]; then
    exit 0
fi

exit 1