plan(key:'TEMPLATETEST', name:'Fedora15 OpenLDAP', description:'A sensible description goes here...') {
    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')

    repository(name:'Puppet Upstream', url:'git@bitbucket.org:atlassian/buildeng-agents-puppet.git', branch:'master',
                shallow:true, cached:true) {
        repositoryAuthMethod(username:'some username', privateKey:'asdfasdfadsf', method:'ssh')
    }

    repository(name:'Dummy Repo', url:'git@bitbucket.org:atlassian/dummy', branch:'master',
                shallow:true, cached:true) {
        repositoryAuthMethod(username:'some username', privateKey:'asdfasdfadsf', method:'ssh')
    }

    permission(user:'asdf')
    permission(group:'asdf')
    permission(loggedInUsers:'asdf')
    permission(AnonymousUsers:'asdf')

//    notifications {}
//    trigger(description:'My trigger', buildStrategy:'', repositoriesToPoll:'')    

    variable(key:'ami', value:'ami-7aa60313')
    variable(key:'ec2.config', value:'fedora15x64ldap.py')
    variable(key:'ec2-user', value: 'ec2-user')
    
    stage(name:'some-stage', description:'stage description', manual:false) {
        job(name:'job name', key:'job-key', enabled:true) {
            checkoutTask(description:"Checkout Default Repository", isFinal:false, enabled:true, clean:false) {
                repository(name:'Puppet Upstream', chechoutDir:'./')
                repository(name:'Dummy Repo', checkoutDir:'./dummy')
            }
            requirement(key:'maven2', value:'true', exists:false)
            requirement(key:'groovy', value:'true', exists:false)
            requirement(key:'os', value:'Linux', exists:false)
            artifactDefinition(name:'cargo logs', location:'./logs', pattern:'*.txt', shared:true)
            artifactSubscription(name:'built war', destination:'./artifacts')
        }
    }
}