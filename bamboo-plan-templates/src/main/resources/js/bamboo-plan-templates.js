AJS.$(document).ready(function() {
    (function() {    	
        if(BAMBOO.currentPlan && AJS.$("body").hasClass( "dec_planConfig" )) {
            AJS.$.getJSON(AJS.contextPath() + "/rest/plantemplate/1.0/plan-template-variables/" + BAMBOO.currentPlan.key, function( data ) {
            	if(data["plantemplate.enabled"] == 'true') {
	                AJS.messages.warning('.bamboo-page-header-extra', {
	                        title: "Plan templates plugin",
	                        body: "This plan is managed by a plan template. Any modification will be erased after"
	                        + " the next execution of the plan template."
	                        + "<span id=\"plan-template-respository-link\"/>"
	                        + "<span id=\"plan-template-deployer-link\"/>",
	                        closeable: false
	                });
            	}

                if(data["plantemplate.repository.url"] != undefined)
                    AJS.$("#plan-template-respository-link")
                            .replaceWith(' <a id="plan-template-respository-link" href="' + data["plantemplate.repository.url"] + '">Plan template file.</a>');
                if(data["plantemplate.deployer.planKey"] != undefined)
                    AJS.$("#plan-template-deployer-link")
                            .replaceWith(' <a id="plan-template-deployer-link" href="' + AJS.contextPath() + "/browse/" + data["plantemplate.deployer.planKey"] + '">Plan deploying the template.</a>');
            }).fail(function() {});
        }
    })();
});
