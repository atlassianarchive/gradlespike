
<html>
<head>
    <title>[@ww.text name='plantemplates.header' /]</title>
    <meta name="decorator" content="adminpage">
</head>
<body>

<h1>[@ww.text name='plantemplates.header' /]</h1>

[@ui.bambooSection]
<p style="float: right">
    <button class="aui-button aui-button-link" id="plan-templates-doc">[@ww.text name='planTemplates.validationRules' /]</button>
    <button class="aui-button aui-button-link" id="plan-templates-pretty-printing">[@ww.text name='planTemplates.prettyPrinting' /]</button>
</p>
[/@ui.bambooSection]

[@ww.form action="runPlanTemplates.action"
namespace="/admin/planTemplates"
submitLabelKey='global.button.run']

[@ui.bambooSection]
        [@ww.textarea labelKey='planTemplates.global.dsl' name='dsl' required=true cssStyle='max-width: 80%; height: 200px;'/]
        [@ww.textarea labelKey='planTemplates.global.shortcuts' name='shortcuts' required=true cssStyle='max-width: 80%; height: 200px;'/]
[/@ui.bambooSection]
[/@ww.form]
[@ui.bambooSection]
    [@ww.label name='outputLog'/]
[/@ui.bambooSection]

<script>

AJS.$("#plan-templates-doc").click(function() {
   AJS.$.getJSON(AJS.contextPath() + "/rest/plantemplate/1.0/validation-rules", function( data ) {
	var dialog = new AJS.Dialog({
	    width: 400, 
	    height: 600, 
	    id: "validation-rules-dialog", 
	    closeOnOutsideClick: true
	});
	
	dialog.addHeader("[@ww.text name='planTemplates.validationRules' /]");
	dialog.addPanel("", '<pre>' + JSON.stringify(data, undefined, 2) + '</pre>' , "panel-body");
	dialog.addLink("Close", function (dialog) {
	    dialog.hide();
	}, "#");

    dialog.gotoPage(0);
    dialog.gotoPanel(0);
    dialog.show();
   });
});

AJS.$("#plan-templates-pretty-printing").click(function() {
    AJS.$.get(AJS.contextPath() + "/rest/plantemplate/1.0/pretty-printing-validation-rules", function( data ) {
        var dialog = new AJS.Dialog({
            width: 800,
            height: 600,
            id: "validation-rules-dialog",
            closeOnOutsideClick: true
        });

        dialog.addHeader("[@ww.text name='planTemplates.prettyPrinting' /]");
        dialog.addPanel("", "<pre>" + data + "</pre>" , "panel-body");
        dialog.addLink("Close", function (dialog) {
            dialog.hide();
        }, "#");

        dialog.gotoPage(0);
        dialog.gotoPanel(0);
        dialog.show();
    });
});
</script>

</body>
</html>
