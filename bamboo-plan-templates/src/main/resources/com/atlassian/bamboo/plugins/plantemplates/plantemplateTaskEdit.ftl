[#-- @ftlvariable name="uiConfigSupport" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[@ww.textfield labelKey='bambooServer' name='bambooServer' cssClass="long-field" required='true'/]
[@ww.textfield labelKey='template' name='template' cssClass="long-field" required='true'/]
[@ww.textfield labelKey='shortcuts' name='shortcuts' cssClass="long-field" /]
[@ww.textfield labelKey='username' name='username' cssClass="long-field" required='true'/]

[@ui.bambooSection dependsOn='passwordVariableCheck' showOn='false']
    [@ww.password labelKey='password' name='password' cssClass="long-field"/]
[/@ui.bambooSection]

[@ui.bambooSection dependsOn='passwordVariableCheck' showOn='true']
	[@ww.textfield labelKey="passwordVariable" name="passwordVariable" cssClass="long-field"/]
[/@ui.bambooSection]

[@ww.checkbox labelKey='passwordVariableCheck' name='passwordVariableCheck' toggle='true'/]

[@ww.select labelKey='executionStrategy' name='executionStrategy'
    list=executionStrategies listKey='first' listValue='second' cssClass="long-field" required='true']
[/@ww.select]