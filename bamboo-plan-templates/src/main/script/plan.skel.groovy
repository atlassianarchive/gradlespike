// this is a skel to get started with PlanTemplates

plan(key:'', name:'', description:'') {
    label(name:'plan-templates') // we like to know with are created with a plan template 
    repository(name:'test')

    // using shorcut
    sampleShortcut(sampleName: 'nameOne', sampleDescription: 'descriptionOne')

    trigger(description:'trigger1', type:'polling', strategy:'periodically', frequency:'180'){
        repository(name:'test')
    }
        
    project(key: '', name:'', description:'')
    stage(name:'', description: '', manual: 'false'){
        job(name: 'job1', key: 'job11Key', description: 'hellojob' ){
            requirement(key:'', condition:'')
            task(type:'checkout', description:'task05'){
                repository(name:'test')
            }
            task(type:'script', description:'task1', scriptBody:'''
                    echo hello1
                '''
            )
        }
    }
}
