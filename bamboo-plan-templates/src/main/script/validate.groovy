#!/usr/bin/env groovy
@Grab('org.apache.httpcomponents:httpclient-osgi:4.1.2')
@Grab('org.apache.httpcomponents:httpclient-osgi:4.1.2')
@Grab('org.apache.httpcomponents:httpcore:4.1.2')
@Grab('com.fasterxml.jackson.core:jackson-databind:2.1.1')

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

def console = System.console()

if (!this.args || this.args.size() < 4){
	println ''
    println '\t# Usage '
    println ''
    println '\t\t$ validate.groovy <template_file> <shortcuts_file> <bamboo_url> <username>'
	println ''
    println '\tFor example:'
	println ''
    println '\t\t$ validate.groovy plan.skel.groovy shorcuts.skel.groovy https://myBamboo.atlassian.com myUsername'
    println ''
    println ''
    return
}


String template_file = new File(this.args[0]).getText()
String shortcuts_file = new File(this.args[1]).getText()  
String bamboo_url = this.args[2]
String username = this.args[3]

String password = new String(console.readPassword('Bamboo Password: '))
String encoded = new String(Base64.encodeBase64((username + ":" + password).getBytes()))

println "Validating '${this.args[0]}' with shortcuts '${this.args[1]}' on '${bamboo_url}' "
println "Username: '${username}'"

def post_url = "${bamboo_url}/rest/plantemplate/1.0/expandDsl"


print "Validation "

DefaultHttpClient client = new DefaultHttpClient()

HttpPost httpPost = new HttpPost(post_url)
httpPost.addHeader("Authorization", "Basic " + encoded)
            
ObjectMapper mapper = new ObjectMapper()

body = [dsl: template_file, shortcuts: shortcuts_file]
String jsonDoc = mapper.writeValueAsString(body)
httpPost.setEntity(new StringEntity(jsonDoc, "application/json", "UTF-8"))
HttpResponse response = client.execute(httpPost)
def respBody = EntityUtils.toString(response.getEntity())

if (response.getStatusLine().getStatusCode() == 200 || response.getStatusLine().getStatusCode() == 201){
	println "[OK]"
	println "expandDsl --------------------------------------"
}else{
	println "[Errors]"
}

if(!respBody){
	println "Response empty, are you sure that you have a DSL in your template?"
}else{
	println respBody
}
