package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

public class DeployJiraPluginTaskMapping extends AbstractDeployPluginTaskMapping {

    public static final String DEPLOY_JIRA_PLUGIN_TASK_PLUGIN_KEY = "com.atlassian.bamboo.plugins.deploy.continuous-plugin-deployment:jiradeploy";


    protected String taskKey(){
        return DEPLOY_JIRA_PLUGIN_TASK_PLUGIN_KEY;
    }


}