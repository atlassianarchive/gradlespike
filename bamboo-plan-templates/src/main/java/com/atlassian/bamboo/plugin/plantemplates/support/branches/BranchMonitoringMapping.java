package com.atlassian.bamboo.plugin.plantemplates.support.branches;

import com.atlassian.bamboo.build.DefaultBuildDefinition;
import com.atlassian.bamboo.chains.branches.BranchNotificationStrategy;
import com.atlassian.bamboo.plan.branch.BranchMonitoringConfiguration;
import com.google.common.base.Function;

import javax.annotation.Nullable;
import java.util.Map;

@SuppressWarnings("rawtypes")
public class BranchMonitoringMapping implements Function<Map, BranchMonitoringConfiguration>
{
    public BranchMonitoringConfiguration apply(@Nullable Map map)
    {
        DefaultBuildDefinition defaultBuildDefinition = new DefaultBuildDefinition(false);
        BranchMonitoringConfiguration configuration = defaultBuildDefinition.getBranchMonitoringConfiguration();

        if (map == null)
            return configuration;

        if (map.containsKey("enabled"))
            configuration.setMonitoringEnabled(Boolean.valueOf((String) map.get("enabled")));

        if (map.containsKey("matchingPattern"))
            configuration.setMatchingPattern((String) map.get("matchingPattern"));

        if (map.containsKey("timeOfInactivityInDays"))
            configuration.setTimeOfInactivityInDays(Integer.parseInt((String) map.get("timeOfInactivityInDays")));

        if (map.containsKey("notificationStrategy"))
            configuration.setDefaultBranchNotificationStrategy(BranchNotificationStrategy.valueOf((String) map.get("notificationStrategy")));

        if (map.containsKey("remoteJiraBranchLinkingEnabled"))
            configuration.setRemoteJiraBranchLinkingEnabled(Boolean.valueOf((String) map.get("remoteJiraBranchLinkingEnabled")));

        return configuration;
    }
}
