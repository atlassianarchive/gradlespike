package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

public interface TaskMapping
{
    public Map<String, String> convert(@SuppressWarnings("rawtypes") Map task);
}
