package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.plugin.BambooPluginUtils;
import com.atlassian.bamboo.plugin.plantemplates.support.RepositoryService;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import com.google.common.collect.Lists;

public class CheckoutTaskMapping extends AbstractTaskMapping
{
    
    private RepositoryService repositoryService;
    
    public CheckoutTaskMapping(RepositoryService repositoryService)
    {
        this.repositoryService = repositoryService;
    }
    
    private final static String CO_TASK_PLUGIN_KEY = BambooPluginUtils.TASK_VCS_CHECKOUT_PLUGIN_KEY;
    
    @SuppressWarnings("rawtypes") 
    public Map<String, String> convert(Map task)
    {
        Builder<String, String> mapBuilder = ImmutableMap.<String, String>builder();
        
        mapBuilder.put("userDescription", (String) task.get("description"));        

        String taskDisabled = (String)task.get("taskDisabled");
        if(Strings.isNullOrEmpty(taskDisabled)){
            mapBuilder.put("taskDisabled", "false");
        }else{
            mapBuilder.put("taskDisabled", taskDisabled);
        }
        
        populateRepositories(task, mapBuilder);
        
        copyFromDslToBambooProperty(task, mapBuilder, "cleanCheckout");

        mapBuilder.put("createTaskKey", CO_TASK_PLUGIN_KEY);
        return mapBuilder.build();
    }

    @SuppressWarnings("rawtypes")
    private void populateRepositories(Map task, Builder<String, String> mapBuilder)
    {
        Object repositoryRepresentation = task.get("repository");
        List repositories = null;
        if (repositoryRepresentation instanceof List) {
            repositories = (List) repositoryRepresentation;
        }
        else if (repositoryRepresentation instanceof Map) {
            repositories = Lists.newArrayList(repositoryRepresentation);
        }
        
        if(repositories != null){
            int counter = 0;
            for (Object repositoryObject : repositories)
            {
                Map repository = (Map) repositoryObject;
                copyRepositoryDetails(repository, mapBuilder, counter);
                counter++;
            }
        }
        else {
            mapBuilder.put("selectedRepository_0", "defaultRepository");
            mapBuilder.put("checkoutDir_0", "");
        }
    }

    @SuppressWarnings("rawtypes")
    private void copyRepositoryDetails(Map repository, Builder<String, String> mapBuilder, Integer position)
    {
        //TODO: get rid of extra default behaviour, add errors instead.
        
        String repositoryName = (String) repository.get("name");
        String repositoryId = "defaultRepository";
        if (!Strings.isNullOrEmpty(repositoryName)) {
            repositoryId = repositoryService.findRespositoryIdByName(repositoryName).toString();
        }
        mapBuilder.put("selectedRepository_"+ position, repositoryId);
        copyFromDslToBambooProperty(repository, mapBuilder, "checkoutDirectory", "checkoutDir_" + position, "");
    }

}
