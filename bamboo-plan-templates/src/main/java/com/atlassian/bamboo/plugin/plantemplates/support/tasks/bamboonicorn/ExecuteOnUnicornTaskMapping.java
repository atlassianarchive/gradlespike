package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.atlassian.bamboo.plugin.plantemplates.support.tasks.AbstractTaskMapping;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import java.util.Map;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_EXECUTE_ON_UNICORN;

public class ExecuteOnUnicornTaskMapping extends AbstractTaskMapping
{
    private static final ImmutableSet<String> TASK_FIELDS = ImmutableSet.of("unicornInstance", "scriptBody", "arguments", "scriptFile");

    protected String taskKey()
    {
        return BAMBOONICORN_EXECUTE_ON_UNICORN.createTaskMappingKey();
    }

    @Override
    public Map<String, String> convert(@SuppressWarnings("rawtypes") Map task)
    {
        ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.builder();
        final String defaultCheckBox = "false";
        final String defaultScriptLocation = "INLINE";

        mapBuilder.put("userDescription", (String) task.get("description"));
        copyFromDslToBambooProperty(task, mapBuilder, "taskDisabled", "taskDisabled", defaultCheckBox);
        copyFromDslToBambooProperty(task, mapBuilder, "scriptLocation", "scriptLocation", defaultScriptLocation);
        mapBuilder.putAll(buildMap(task, TASK_FIELDS));
        mapBuilder.put("createTaskKey", taskKey());

        return mapBuilder.build();
    }
}
