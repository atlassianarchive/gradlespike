package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.atlassian.bamboo.plugin.plantemplates.support.tasks.AbstractTaskMapping;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

public abstract class AbstractVersionTaskMapping extends AbstractTaskMapping
{
    private static final ImmutableMap<String, String> DEPLOYMENT_ENVIRONMENTS = ImmutableMap.of("jirastudio-dev", "DEV", "jirastudio-dog", "DOG", "jirastudio-prd", "PROD", "jirastudio-prd-virtual", "PROD-VIRTUAL");

    protected abstract String taskKey();
    protected abstract ImmutableSet<String> getTaskFields();

    @Override
    public Map<String, String> convert(@SuppressWarnings("rawtypes") Map task)
    {
        ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.builder();
        final String defaultDeploymentEnvironmentKey = "jirastudio-dev";
        final String defaultCheckBox = "false";

        mapBuilder.put("userDescription", (String) task.get("description"));
        mapBuilder.putAll(buildMap(task, getTaskFields()));
        copyFromDslToBambooProperty(task, mapBuilder, "taskDisabled", "taskDisabled", defaultCheckBox);
        populateWithMappedValue(task, mapBuilder, "deploymentEnvironment", "deploymentEnvironment", defaultDeploymentEnvironmentKey);

        mapBuilder.put("createTaskKey", taskKey());

        return mapBuilder.build();
    }

    private void populateWithMappedValue(Map task, ImmutableMap.Builder<String, String> mapBuilder, String templateKey, String bambooKey, String defaultMapKey)
    {
        String mapKey = (String) task.get(templateKey);
        final String devEnvironment = DEPLOYMENT_ENVIRONMENTS.get(Strings.nullToEmpty(mapKey));
        if (!StringUtils.isBlank(devEnvironment)) {
            mapBuilder.put(bambooKey, devEnvironment);
        } else {
            mapBuilder.put(bambooKey, DEPLOYMENT_ENVIRONMENTS.get(defaultMapKey));
        }
    }
}
