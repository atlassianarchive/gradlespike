package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;

import com.google.common.base.Joiner;

public class TemplateValidationException extends RuntimeException
{

    private static final long serialVersionUID = -7508875933533460065L;
    
    private List<String> errors;

    private String expandedDsl;

    public TemplateValidationException(String message, List<String> errors)
    {
        super(message);
        this.errors = errors;
    }
    
    public TemplateValidationException(String message, List<String> errors, String expandedDsl)
    {
        super(message);
        this.errors = errors;
        this.expandedDsl = expandedDsl;
    }
    
    public List<String> getErrors()
    {
        return errors;
    }
    
    @Override
    public String toString()
    {
        
        StringBuilder sb = new StringBuilder();        
        if(expandedDsl !=null){
            sb.append("expanded dsl ------------------- ")
              .append("\r\n")
              .append(expandedDsl)
              .append("\r\n")
              .append("errors ------------------------- ")
              .append("\r\n");
        }
        
        sb.append("TemplateValidationException")
            .append(": ")
            .append(getMessage())
            .append("\r\n\t")
            .append(Joiner.on("\r\n\t").skipNulls().join(errors))
            .append("\r\n");
        
        
        
        
        return sb.toString();
    }

 
        
 
}
