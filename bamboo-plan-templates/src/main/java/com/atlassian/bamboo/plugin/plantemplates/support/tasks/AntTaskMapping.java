package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import com.atlassian.bamboo.plugin.BambooPluginUtils;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import java.util.Map;

public class AntTaskMapping extends AbstractTaskMapping implements TaskMapping
{
    private static final String ANT_TASK_PLUGIN_KEY =  "com.atlassian.bamboo.plugins.ant:task.builder.ant";

    // userDescription
    // taskDisabled (false, true)
    // label
    // buildFile
    // target
    // buildJdk:JDK 1.7.0_40
    // selectFields:buildJdk
    // environmentVariables
    // workingSubDirectory:my-sub/
    // testChecked:true
    // testResultsDirectory:**/test-reports/*.xml
    // createTaskKey -> com.atlassian.bamboo.plugins.ant:task.builder.ant
    @SuppressWarnings("rawtypes")
    public Map<String, String> convert(Map task)
    {
        Builder<String, String> mapBuilder = ImmutableMap.<String, String>builder();
        
        mapBuilder.put("userDescription", (String) task.get("description"));        

        String taskDisabled = (String)task.get("taskDisabled");
        if(Strings.isNullOrEmpty(taskDisabled)){
            mapBuilder.put("taskDisabled", "false");
        }else{
            mapBuilder.put("taskDisabled", taskDisabled);
        }
        
        String script = (String) task.get("executable");
        if(!Strings.isNullOrEmpty(script)){
            mapBuilder.put("label", script);
        }

        copyFromDslToBambooProperty(task, mapBuilder, "target");
        copyFromDslToBambooProperty(task, mapBuilder, "buildJdk");
        copyFromDslToBambooProperty(task, mapBuilder, "buildFile");
        copyFromDslToBambooProperty(task, mapBuilder, "environmentVariables");
        copyFromDslToBambooProperty(task, mapBuilder, "workingSubDirectory");
        copyFromDslToBambooProperty(task, mapBuilder, "testResultsDirectory");
        copyFromDslToBambooProperty(task, mapBuilder, "hasTests", "testChecked");
        mapBuilder.put("createTaskKey", ANT_TASK_PLUGIN_KEY);

        return mapBuilder.build();
    }
}
