package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public class NodeJsTaskMapping  extends AbstractTaskMapping {



    @SuppressWarnings("rawtypes")
    public Map<String, String> convert(Map task)
    {
        ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.<String, String>builder();

        mapBuilder.put("userDescription", (String) task.get("description"));

        String taskDisabled = (String)task.get("taskDisabled");
        if(Strings.isNullOrEmpty(taskDisabled)){
            mapBuilder.put("taskDisabled", "false");
        }else{
            mapBuilder.put("taskDisabled", taskDisabled);
        }


        copyFromDslToBambooProperty(task, mapBuilder, "arguments");
        copyFromDslToBambooProperty(task, mapBuilder, "script", "command");
        copyFromDslToBambooProperty(task, mapBuilder, "executable", "runtime");
        copyFromDslToBambooProperty(task, mapBuilder, "workingSubDirectory");
        mapBuilder.put("createTaskKey", "com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.builder.node");

        return mapBuilder.build();

    }

}
