package com.atlassian.bamboo.plugin.plantemplates;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.build.PlanCreationDeniedException;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.plugin.plantemplates.support.BranchMonitoringService;
import com.atlassian.bamboo.plugin.plantemplates.support.DslTagHelper;
import com.atlassian.bamboo.plugin.plantemplates.support.PlanBuildExpiryService;
import com.atlassian.bamboo.plugin.plantemplates.support.PlanDependenciesService;
import com.atlassian.bamboo.plugin.plantemplates.support.PlanService;
import com.atlassian.bamboo.plugin.plantemplates.support.ProjectService;
import com.atlassian.bamboo.plugin.plantemplates.support.StageService;
import com.atlassian.bamboo.variable.VariableConfigurationService;

import com.google.common.collect.Lists;

public class PerformPlanChanges
{
    private PlanService planService;
    private ProjectService projectService;    
    private StageService stageService;
    private BranchMonitoringService branchMonitoringService;
    private VariableConfigurationService variableConfigurationService;
    private AutoVariablesService autoVariablesService;
    private PlanDependenciesService planDependenciesService;
    private PlanBuildExpiryService planBuildExpiryService;

    
    public PerformPlanChanges(PlanService planService,
                              ProjectService projectService,
                              StageService stageService,
                              BranchMonitoringService branchMonitoringService,
                              VariableConfigurationService variableConfigurationService,
                              AutoVariablesService autoVariablesService,
                              PlanDependenciesService planDependenciesService,
                              PlanBuildExpiryService planBuildExpiryService
        )
    {
        this.planService = planService;
        this.projectService = projectService;
        this.stageService = stageService;
        this.branchMonitoringService = branchMonitoringService;
        this.variableConfigurationService = variableConfigurationService;
        this.autoVariablesService = autoVariablesService;
        this.planDependenciesService = planDependenciesService;
        this.planBuildExpiryService = planBuildExpiryService;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public List<Chain> performChanges(Map planDslMap) throws PlanCreationDeniedException
    {
        if(planDslMap == null){
            throw new IllegalArgumentException("No plan template dsl were found to process. Check the syntax of your template");
        }
        List planRepresentations = DslTagHelper.getDslTagList(planDslMap.get("plan"));
        Map<String, String> additionalVariables = autoVariablesService.getAdditionalVariables(planDslMap);
        List<Chain> resultPlans = Lists.newArrayList();

        for (Object planObject : planRepresentations) {
            Chain chain = performChangesOnSinglePlan((Map) planObject, additionalVariables);
            resultPlans.add(chain);
        }
        return resultPlans;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private Chain performChangesOnSinglePlan(Map planRepresentation, Map<String, String> additionalVariables) throws PlanCreationDeniedException
    {
        projectService.createOrUpdate((Map<String, String>) planRepresentation.get("project"));
        Chain chain = planService.createOrUpdate(planRepresentation);
        
        chain = stageService.performStageChanges(planRepresentation.get("stage"), chain);
        planService.save(chain);

        branchMonitoringService.applyAndSaveBranchMonitoringConfiguration((Map) planRepresentation.get("branchMonitoring"), chain);

        for (Map.Entry<String, String> additionalVariablesEntry : additionalVariables.entrySet()) {
            variableConfigurationService.createPlanVariable(chain, additionalVariablesEntry.getKey(), additionalVariablesEntry.getValue());
        }

        planDependenciesService.performChanges(chain, (Map) planRepresentation.get("dependencies"));
        planBuildExpiryService.performChanges(chain, (Map) planRepresentation.get("buildExpiry"));

        enablePlanIfRequired(chain, (String) planRepresentation.get("enabled"));

        return chain;
    }

    private void enablePlanIfRequired(final Chain chain, final String enabled) {
        boolean enableState = true;
        if ("false".equals(enabled)) {
            enableState = false;
        }
        chain.setSuspendedFromBuilding(!enableState);
        planService.save(chain);
    }

}
