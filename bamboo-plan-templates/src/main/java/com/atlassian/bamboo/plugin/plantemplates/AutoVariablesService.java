package com.atlassian.bamboo.plugin.plantemplates;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Service to add automatically additional variables to the plan.
 * <p>
 * This is useful to define variables used internally by plan template to display useful information in the UI.
 * </p>
 */
public class AutoVariablesService {
    /**
     * Prefix used by the variables defined by plantemplate.
     */
    public static final String PLANTEMPLATE_VARIABLE_PREFIX = "plantemplate.";
    /**
     * Variable which contains the URL of the repository for the plan template file.
     */
    public static final String PLANTEMPLATE_REPOSITORY_URL = PLANTEMPLATE_VARIABLE_PREFIX + "repository.url";
    /**
     * Variable which contains the plan key of the plan in charge of deploying the template.
     */
    public static final String PLANTEMPLATE_DEPLOYER_PLAN_KEY = PLANTEMPLATE_VARIABLE_PREFIX + "deployer.planKey";

    /**
     * Provides a Map of the additional variables defined in the {@code dslMap}.
     *
     * @param dslMap DSL of the template.
     * @return a map of the additional variables.
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> getAdditionalVariables(Map<String, Object> dslMap) {
        Map<String, String> additionalVariables = new HashMap<String, String>();
        
        additionalVariables.put("plantemplate.enabled", "true");

        Map<String, String> templateRepository = (Map<String, String>) dslMap.get("templateRepository");
        if (templateRepository != null)
            additionalVariables.putAll(getTemplateRepositoryVariables(templateRepository));

        Map<String, String> deployer = (Map<String, String>) dslMap.get("deployer");
        if (deployer != null)
            additionalVariables.putAll(getDeployerVariables(deployer));

        return Collections.unmodifiableMap(additionalVariables);
    }

    /**
     * Extracts the variables related to the plan deploying the template.
     *
     * @param deployer DSL of the plan deploying the template.
     * @return the variables related to the plan deploying the template.
     */
    private Map<String, String> getDeployerVariables(Map<String, String> deployer) {
        Map<String, String> variables = new HashMap<String, String>();
        String planKey = deployer.get("planKey");
        if (planKey != null)
            variables.put(PLANTEMPLATE_DEPLOYER_PLAN_KEY, planKey);
        return Collections.unmodifiableMap(variables);
    }

    /**
     * Extracts the variables related to the template repository.
     *
     * @param templateRepository template repository DSL.
     * @return the variables related to the template repository.
     */
    private Map<String, String> getTemplateRepositoryVariables(Map<String, String> templateRepository) {
        Map<String, String> variables = new HashMap<String, String>();
        String url = templateRepository.get("url");
        if (url != null)
            variables.put(PLANTEMPLATE_REPOSITORY_URL, url);
        return Collections.unmodifiableMap(variables);
    }
}
