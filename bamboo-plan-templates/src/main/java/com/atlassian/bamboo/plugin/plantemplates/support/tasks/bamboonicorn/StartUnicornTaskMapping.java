package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.atlassian.bamboo.plugin.plantemplates.support.tasks.AbstractTaskMapping;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_START_UNICORN;

public class StartUnicornTaskMapping extends AbstractTaskMapping
{
    protected String taskKey()
    {
        return BAMBOONICORN_START_UNICORN.createTaskMappingKey();
    }

    @Override
    public Map<String, String> convert(@SuppressWarnings("rawtypes") Map task)
    {
        ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.builder();
        final String defaultCheckBox = "false";

        mapBuilder.put("userDescription", (String) task.get("description"));
        copyFromDslToBambooProperty(task, mapBuilder, "taskDisabled", "taskDisabled", defaultCheckBox);
        copyFromDslToBambooProperty(task, mapBuilder, "unicornInstance");
        copyFromDslToBambooProperty(task, mapBuilder, "halTimeout");
        mapBuilder.put("createTaskKey", taskKey());

        return mapBuilder.build();
    }
}
