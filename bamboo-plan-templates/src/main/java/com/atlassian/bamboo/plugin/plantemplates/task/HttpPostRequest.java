package com.atlassian.bamboo.plugin.plantemplates.task;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;


public class HttpPostRequest
{
    private String getBambooServerUrlWithEndingSlash(final String bamboServerUrl)  {
        if (bamboServerUrl.endsWith("/")){
            return bamboServerUrl;
        } else {
            return bamboServerUrl + "/";
        }
    }

    public String postDsl(String bamboServerUrl, String templateBody, String shortcutsBody, String userName, String password)
    {
        String postUrl = getBambooServerUrlWithEndingSlash(bamboServerUrl) + "rest/plantemplate/1.0/json?os_authType=basic";
        return post(postUrl, templateBody, shortcutsBody, userName, password);
    }
    public String expandDsl(String bamboServerUrl, String templateBody, String shortcutsBody, String userName, String password)
    {
        String postUrl = getBambooServerUrlWithEndingSlash(bamboServerUrl) + "rest/plantemplate/1.0/expandDsl?os_authType=basic";
        return post(postUrl, templateBody, shortcutsBody, userName, password);
    }

    private String post(String postUrl,
                        String templateBody,
                        String shortcutsBody,
                        String userName,
                        String password)
    {
        DefaultHttpClient client = new DefaultHttpClient();
        
        Builder<String, String> jsonMapBuilder = ImmutableMap.<String, String>builder();
        
        if(!Strings.isNullOrEmpty(templateBody)){
            jsonMapBuilder.put("dsl", templateBody);
        }
        if(!Strings.isNullOrEmpty(shortcutsBody)){
            jsonMapBuilder.put("shortcuts", shortcutsBody);
        }

        try
        {
            String encoded = new String(Base64.encodeBase64((userName + ":" + password).getBytes()));

            HttpPost httpPost = new HttpPost(postUrl);
            httpPost.addHeader("Authorization", "Basic " + encoded);
            
            ObjectMapper mapper = new ObjectMapper();
            String jsonDoc = mapper.writeValueAsString(jsonMapBuilder.build());

            httpPost.setEntity(new StringEntity(jsonDoc, "application/json", "UTF-8"));
            HttpResponse response = client.execute(httpPost);
            
            if (response.getStatusLine().getStatusCode() == 200 || response.getStatusLine().getStatusCode() == 201){
                return EntityUtils.toString(response.getEntity());
            }
            // Error 400 or 500            
            throw new HttpPostException("Error in POST to: '"+ postUrl + "' STATUS '"+ response.getStatusLine().getStatusCode() +"' with response: " + EntityUtils.toString(response.getEntity()));
        }
        catch (ClientProtocolException e)
        {
            throw new RuntimeException(e);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            client.getConnectionManager().shutdown();
        }
    }
    
}
