package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.bamboo.build.strategy.BuildStrategy;
import com.atlassian.bamboo.build.strategy.BuildStrategyConfigurationService;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.plugin.plantemplates.support.triggers.TriggerMappingFactory;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.apache.log4j.Logger;

import javax.annotation.Nullable;

public class TriggerService
{
    private static final Logger log = Logger.getLogger(TriggerService.class);

    private BuildStrategyConfigurationService buildStrategyConfigurationService;
    private RepositoryService repositoryService;
    private TriggerMappingFactory triggerMappingFactory;

    public TriggerService(BuildStrategyConfigurationService buildStrategyConfigurationService,
                          RepositoryService repositoryService, TriggerMappingFactory triggerMappingFactory)
    {
        this.buildStrategyConfigurationService = buildStrategyConfigurationService;
        this.repositoryService = repositoryService;
        this.triggerMappingFactory = triggerMappingFactory;
    }

    @SuppressWarnings("rawtypes")
    public void performTriggerChanges(Chain plan, Object triggers)
    {
        deleteTriggers(plan);

        List triggerList = DslTagHelper.getDslTagList(triggers);
        
        for (Object triggerObject : triggerList)
        {
            Map trigger = (Map) triggerObject;
            createTriggers(plan, trigger);
        }
        
    }
    
    @SuppressWarnings("rawtypes")
    private void createTriggers(Chain plan, Map trigger)
    {
        String description = (String) trigger.get("description");
        String type = (String) trigger.get("type");
        
        Object repositoryRepresentation = trigger.get("repository");
        Set<Long> repositoriesIds = Sets.<Long>newHashSet();
        if(repositoryRepresentation != null){
            repositoriesIds = extractRespositoriesIds(repositoryRepresentation); 
        }
        
        Map<String, String> converted = triggerMappingFactory.create(type).convert(trigger);
        BuildConfiguration buildConfiguration = convertToBuildConfiguration(converted);
        processConditions(buildConfiguration, trigger);
        log.info("PlanTemplates: Adding Trigger: '" + description + "'");        
        buildStrategyConfigurationService.createBuildStrategy(plan.getPlanKey(), description, repositoriesIds, buildConfiguration);
    }
    
    private BuildConfiguration convertToBuildConfiguration(Map<String, String> triggerMap) {
        BuildConfiguration buildConfiguration = new BuildConfiguration();
        for (String key : triggerMap.keySet())
        {
            buildConfiguration.setProperty(key, triggerMap.get(key));
        }
        
        return buildConfiguration;
    }

    @SuppressWarnings("rawtypes")
    private Set<Long> extractRespositoriesIds(Object repositoryRepresentation)
    {
        List repositories = null;
        Set<Long> ids = Sets.newHashSet();
        
        if(repositoryRepresentation instanceof Map)
            repositories = Lists.newArrayList(repositoryRepresentation);
        else if(repositoryRepresentation instanceof List)
            repositories = (List) repositoryRepresentation;
        
        for (Object repositoryObject : repositories)
        {
            Map repository = (Map) repositoryObject;
            String repoName = (String) repository.get("name");
            if(!Strings.isNullOrEmpty(repoName)){
                Long repoId = repositoryService.findRespositoryIdByName(repoName);
                if(repoId != null)
                    ids.add(repoId);
            }
        }
        return ids;
    }

    private void deleteTriggers(Chain plan)
    {
        if(plan.getBuildDefinition()!=null){
            List<BuildStrategy> buildStrategies = plan.getBuildDefinition().getBuildStrategies();
            for (BuildStrategy buildStrategy : buildStrategies)
            {
                buildStrategyConfigurationService.deleteBuildStrategy(plan.getPlanKey(), buildStrategy.getId());
            }
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void processConditions(BuildConfiguration buildConfiguration, Map triggerRepresentation) {
        Object planGreenObject = triggerRepresentation.get("planGreenCondition");
        List planGreenConditions;
        if (planGreenObject instanceof List) {
            planGreenConditions = (List) planGreenObject;
        }
        else if (planGreenObject instanceof Map) {
            planGreenConditions = Lists.newArrayList(planGreenObject);
        }
        else {
            buildConfiguration.setProperty("custom.triggerrCondition.plansGreen.enabled", "false");
            return;
        }

        buildConfiguration.setProperty("custom.triggerrCondition.plansGreen.enabled", "true");
        String planGreenKeys = Joiner.on(",").join(Lists.transform(planGreenConditions, new Function()
        {
            @Override
            public Object apply(@Nullable Object input)
            {
                Map planGreenCondition = (Map) input;
                return planGreenCondition.get("planKey");
            }
        }));
        buildConfiguration.setProperty("custom.triggerrCondition.plansGreen.plans", planGreenKeys);
    }
}
