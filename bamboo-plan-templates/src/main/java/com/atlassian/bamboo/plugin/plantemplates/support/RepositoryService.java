package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.repository.RepositoryConfigurationService;
import com.atlassian.bamboo.repository.RepositoryData;
import com.atlassian.bamboo.repository.RepositoryDefinitionManager;
import com.atlassian.bamboo.repository.nullrepository.NullRepository;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.apache.log4j.Logger;

public class RepositoryService
{
    private static Logger logger = Logger.getLogger(RepositoryService.class);
    
    private RepositoryConfigurationService repositoryConfigurationService;
    private RepositoryDefinitionManager repositoryDefinitionManager;
    

    public RepositoryService(RepositoryConfigurationService repositoryConfigurationService,
                             RepositoryDefinitionManager repositoryDefinitionManager)
    {
        this.repositoryConfigurationService = repositoryConfigurationService;
        this.repositoryDefinitionManager = repositoryDefinitionManager;
    }
    
    
    
    @SuppressWarnings("rawtypes")
    public void updateRespository(Plan plan, Object repoRepresentation)
    {
        if (repoRepresentation == null)
        {
            return;
        }

        if (repoRepresentation instanceof Map)
        {
            updateRespositoryList(plan, Lists.newArrayList(repoRepresentation));
            return;
        }

        if (repoRepresentation instanceof List)
        {
            updateRespositoryList(plan, (List) repoRepresentation);
            return;
        }
        throw new IllegalArgumentException("repoRepresentation is not a Map or a List is "
                                           + repoRepresentation.getClass());
    }

    @SuppressWarnings("rawtypes")
    private void updateRespositoryList(Plan plan, List repoRepresentation)
    {
        for (Object repoObject : repoRepresentation)
        {
            Map repositoryMap = (Map) repoObject;
            String name = (String) repositoryMap.get("name");
            final RepositoryData repository = findRespositoryDefinitionByName(name);
            if(repository == null)
                return;
              
            try{
                logger.info("PlanTemplates: Attaching GlobalRepository: '" + name + "'");
                repositoryConfigurationService.attachGlobalRepositoryToPlan(plan , repository.getId(), true);
            }catch(IllegalArgumentException e){
                logger.info("Repository with name "+ name + " is already attached to the plan " + plan.getBuildKey());
            }
            
        }
    }

    @SuppressWarnings("rawtypes")
    public String findRespository(Map repoRepresentation)
    {
        if(repoRepresentation == null)
            return NullRepository.KEY;
        return findRepositoryKeyByName((String) repoRepresentation.get("name"));
    }

    private String findRepositoryKeyByName(String repoName)
    {
        RepositoryData repository = findRespositoryDefinitionByName(repoName);
        if(repository == null)
            return NullRepository.KEY;
        return repository.getPluginKey();
    }
  
    private RepositoryData findRespositoryDefinitionByName(final String name)
    {
        return Iterables.find(repositoryDefinitionManager.getGlobalRepositoryDefinitions(), new Predicate<RepositoryData>()
        {
            @Override
            public boolean apply(@Nullable RepositoryData input)
            {
                return input.getName().equals(name);
            }
        }, null);
    }

    public Long findRespositoryIdByName(String name)
    {
        RepositoryData repositoryData = findRespositoryDefinitionByName(name);
        if(repositoryData == null)
            throw new IllegalArgumentException("Shared repository '" + name + "' is not found, please ensure that the shared repository exists");
        return repositoryData.getId();
    }


    
}
