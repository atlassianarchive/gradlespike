package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.atlassian.bamboo.plugin.plantemplates.support.tasks.AbstractTaskMapping;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import java.util.Map;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_INSTALL_ONDEMAND;

public class InstallOnDemandTaskMapping extends AbstractTaskMapping
{
    private static final ImmutableSet<String> TASK_FIELDS = ImmutableSet.of("unicornInstance", "localYaml");

    protected String taskKey()
    {
        return BAMBOONICORN_INSTALL_ONDEMAND.createTaskMappingKey();
    }

    @Override
    public Map<String, String> convert(@SuppressWarnings("rawtypes") Map task)
    {
        ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.builder();
        final String defaultIcebatVersion = "latest";
        final String defaultCheckBox = "false";

        mapBuilder.put("userDescription", (String) task.get("description"));
        mapBuilder.putAll(buildMap(task, TASK_FIELDS));
        copyFromDslToBambooProperty(task, mapBuilder, "taskDisabled", "taskDisabled", defaultCheckBox);
        copyFromDslToBambooProperty(task, mapBuilder, "cleanBeforeInstall", "cleanBeforeInstall", defaultCheckBox);
        copyFromDslToBambooProperty(task, mapBuilder, "version", "icebatVersion", defaultIcebatVersion);
        mapBuilder.put("createTaskKey", taskKey());

        return mapBuilder.build();
    }
}
