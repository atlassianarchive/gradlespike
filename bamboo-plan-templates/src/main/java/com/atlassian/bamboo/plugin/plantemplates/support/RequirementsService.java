package com.atlassian.bamboo.plugin.plantemplates.support;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementImpl;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementSet;
import com.google.common.base.Predicates;
import com.google.common.collect.Lists;

import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

public class RequirementsService
{
    private static final Logger log = Logger.getLogger(RequirementsService.class);
    
    @SuppressWarnings("rawtypes")
    public Job performChanges(Object requirementsRepresentationObject, Job job)
    {
        List requirements;
        if (requirementsRepresentationObject instanceof List) {
            requirements = (List) requirementsRepresentationObject;
        }
        else if (requirementsRepresentationObject instanceof Map){
            requirements = Lists.newArrayList(requirementsRepresentationObject);
        }
        else if (requirementsRepresentationObject == null){
            requirements = Lists.newArrayList();
        }
        else {
            throw new IllegalArgumentException("Requirements representation was not a list or map");
        }

        RequirementSet requirementSet = job.getRequirementSet();
        requirementSet.removeRequirements(Predicates.<Requirement>alwaysTrue());

        for (Object requirementObject : requirements) {

            Map requirementMap = (Map) requirementObject;
            String key = (String) requirementMap.get("key");
            String value = (String) requirementMap.get("value");
            String condition = (String) requirementMap.get("condition");

            boolean regexMatch = "match".equals(condition);
            if(condition.equals("exists")){
                value = ".*";
                regexMatch = true;
            }
            log.info("PlanTemplates: adding Requirement with key: '" + key + "'");        
            requirementSet.addRequirement(new RequirementImpl(key, regexMatch, value));
        }
        return job;
    }


}
