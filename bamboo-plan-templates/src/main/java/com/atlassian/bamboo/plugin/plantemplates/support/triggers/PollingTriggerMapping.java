package com.atlassian.bamboo.plugin.plantemplates.support.triggers;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

public class PollingTriggerMapping implements TriggerMapping
{

    @Override
    @SuppressWarnings("rawtypes")
    public Map<String, String> convert(Map trigger)
    {
        Builder<String,String> mapBuilder = ImmutableMap.<String, String>builder();
        
        mapBuilder.put("selectedBuildStrategy", "poll");
        
        String strategy = (String) trigger.get("strategy");
        
        if (strategy.equals("periodically")) {
            mapBuilder.put("repository.change.poll.type", "PERIOD");
            mapBuilder.put("repository.change.poll.pollingPeriod", (String) trigger.get("frequency")); 
        }
        else {
            mapBuilder.put("repository.change.poll.type", "CRON");
            mapBuilder.put("repository.change.poll.cronExpression", (String) trigger.get("cronExpression")); 
        }

        return mapBuilder.build();
    }

}
