package com.atlassian.bamboo.plugin.plantemplates.support.tasks;


import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * Supports 'Maven Version Updater for Bamboo Variables' task
 */
public class MavenVersionUpdaterTaskMapping extends AbstractTaskMapping
{

    public static final String MAVEN_VERSION_VARIABLE_UPDATER_TASK_PLUGIN_KEY = "com.atlassian.bamboo.plugins.variable.updater.variable-updater:variable-updater";

    @Override
    public Map<String, String> convert(@SuppressWarnings("rawtypes") Map task)
    {
        ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.<String, String>builder();

        mapBuilder.put("taskDisabled", Boolean.valueOf((String) task.get("taskDisabled")).toString());
        copyFromDslToBambooProperty(task, mapBuilder, "description", "userDescription");

        copyFromDslToBambooProperty(task, mapBuilder, "versionPattern");
        mapBuilder.put("includeGlobals", Boolean.valueOf((String)task.get("includeGlobals")).toString());
        copyFromDslToBambooProperty(task, mapBuilder, "variablePattern");

        mapBuilder.put("createTaskKey", MAVEN_VERSION_VARIABLE_UPDATER_TASK_PLUGIN_KEY);

        return mapBuilder.build();
    }
}
