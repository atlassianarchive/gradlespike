package com.atlassian.bamboo.plugin.plantemplates.support.triggers;

import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import com.google.common.collect.Lists;

public class PushTriggerMapping implements TriggerMapping
{

    @Override
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Map<String, String> convert(Map trigger)
    {
        Builder<String,String> mapBuilder = ImmutableMap.<String, String>builder();
        
        mapBuilder.put("selectedBuildStrategy", "trigger");
        
        List servers;
        Object serverObject = trigger.get("server");
        if (serverObject instanceof List) {
            servers = (List) serverObject;
        }
        else if (serverObject instanceof Map){
            servers = Lists.newArrayList(serverObject);
        }
        else {
            mapBuilder.put("repository.change.trigger.triggerIpAddress", "");
            return mapBuilder.build();  
        }
        
        String ips = Joiner.on(",").join(Lists.transform(servers, new Function()
        {
            @Override
            public Object apply(@Nullable Object input)
            {
                Map server = (Map) input;
                return server.get("ip");
            }
            
        }));
        
        mapBuilder.put("repository.change.trigger.triggerIpAddress", ips); 
        return mapBuilder.build();
    }

}
