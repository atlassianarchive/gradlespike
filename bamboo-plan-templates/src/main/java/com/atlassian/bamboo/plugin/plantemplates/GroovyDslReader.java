package com.atlassian.bamboo.plugin.plantemplates;


public class GroovyDslReader
{
    public static String READER = "class PlanBuilder extends groovy.util.BuilderSupport {\n" +
            "    \n" +
            "    def debug = false\n" +
            "    protected void setParent(Object parent, Object child) {}\n" +
            "\n" +
            "    protected Object createNode(Object name){\n" +
            "        throw new Exception()        \n" +
            "    }\n" +
            "    protected Object createNode(Object name, Object value){\n" +
            "        throw new Exception()        \n" +
            "    }\n" +
            "    protected Object createNode(Object name, Map attributes){\n" +
            "        if(debug) {println \"createNode(${name}, ${attributes})\"}\n" +
            "        def result = [:]\n" +
            "        result['attributes'] = attributes\n" +
            "        result['type'] = name\n" +
            "        return result\n" +
            "    }\n" +
            "    protected Object createNode(Object name, Map attributes, Object value){\n" +
            "        throw new Exception()\n" +
            "    }\n" +
            "    protected void nodeCompleted(Object parent, Object node) {\n" +
            "        if(parent != null) {\n" +
            "            def keys = parent.keySet()\n" +
            "            assert keys.size() == 2\n" +
            "\n" +
            "            //This is pretty crap. Needs a rethink\n" +
            "            def attr = parent.attributes[node.type]\n" +
            "            //If it doesn't exist in the parent, add it\n" +
            "            if(attr == null) {\n" +
            "                parent.attributes[node.type] = node.attributes\n" +
            "            }\n" +
            "            else {\n" +
            "                //If it already exists, we want to make it a list of items\n" +
            "                if(! (attr instanceof List)) {\n" +
            "                    parent.attributes[node.type] = [attr]\n" +
            "                }\n" +
            "                parent.attributes[node.type].add(node.attributes)\n" +
            "            }\n" +
            "        }\n" +
            "        else {\n" +
            "            //Re-jig the parent node to be like the children\n" +
            "            def attributes = node.attributes\n" +
            "            def type = node.type\n" +
            "            node.remove('type')\n" +
            "            node.remove('attributes')\n" +
            "            node.put(type, attributes)\n" +
            "        }\n" +
            "        if(debug) {println \"node completed parent: ${parent}, node: ${node}\"}\n" +
            "    }\n" +
            "}\n" +
            "\n" +
            "\n" +
            "//For my own sanity\n" +
            "def prettyprint(Map node, Integer level) {\n" +
            "    def prefix = \"  \"*level + \"-\"\n" +
            "    for(key in node.keySet()) {\n" +
            "        if(node[key] instanceof Map) {\n" +
            "            println prefix + \"${key}:\"\n" +
            "            prettyprint(node[key], level+1)\n" +
            "        }\n" +
            "        else if (node[key] instanceof List) {\n" +
            "            for(item in node[key]) {\n" +
            "                def m = [:]\n" +
            "                m[key] = item\n" +
            "                prettyprint(m, level)\n" +
            "            }\n" +
            "        }\n" +
            "        else {\n" +
            "            println prefix + \"${key}:${node[key]}\"\n" +
            "        }\n" +
            "    }\n" +
            "\n" +
            "\n" +
            "}\n" +
            "\n" +
            "def builder = new PlanBuilder()\n" +
            "def result = builder.plan(key:'TEMPLATETEST', name:'Fedora15 OpenLDAP', description:'A sensible description goes here...') {\n" +
            "    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')\n" +
            "\n" +
            "    repository(name:'Puppet Upstream', url:'git@bitbucket.org:atlassian/buildeng-agents-puppet.git', branch:'master',\n" +
            "                shallow:true, cached:true) {\n" +
            "        repositoryAuthMethod(username:'some username', privateKey:'asdfasdfadsf', method:'ssh')\n" +
            "    }\n" +
            "\n" +
            "    repository(name:'Dummy Repo', url:'git@bitbucket.org:atlassian/dummy', branch:'master',\n" +
            "                shallow:true, cached:true) {\n" +
            "        repositoryAuthMethod(username:'some username', privateKey:'asdfasdfadsf', method:'ssh')\n" +
            "    }\n" +
            "\n" +
            "    permission(user:'asdf')\n" +
            "    permission(group:'asdf')\n" +
            "    permission(loggedInUsers:'asdf')\n" +
            "    permission(AnonymousUsers:'asdf')\n" +
            "\n" +
            "//    notifications {}\n" +
            "//    trigger(description:'My trigger', buildStrategy:'', repositoriesToPoll:'')    \n" +
            "\n" +
            "    planVariable(key:'ami', value:'ami-7aa60313')\n" +
            "    planVariable(key:'ec2.config', value:'fedora15x64ldap.py')\n" +
            "    planVariable(key:'ec2-user', value: 'ec2-user')\n" +
            "    \n" +
            "    stage(name:'some-stage', description:'stage description', manual:false) {\n" +
            "        job(name:'job name', key:'job-key', enabled:true) {\n" +
            "            checkoutTask(description:\"Checkout Default Repository\", isFinal:false, enabled:true, clean:false) {\n" +
            "                repository(name:'Puppet Upstream', chechoutDir:'./')\n" +
            "                repository(name:'Dummy Repo', checkoutDir:'./dummy')\n" +
            "            }\n" +
            "            requirement(key:'maven2', value:'true', exists:false)\n" +
            "            requirement(key:'groovy', value:'true', exists:false)\n" +
            "            requirement(key:'os', value:'Linux', exists:false)\n" +
            "            artifactDefinition(name:'cargo logs', location:'./logs', pattern:'*.txt', shared:true)\n" +
            "            artifactSubscription(name:'built war', destination:'./artifacts')\n" +
            "        }\n" +
            "    }\n" +
            "}";
}
