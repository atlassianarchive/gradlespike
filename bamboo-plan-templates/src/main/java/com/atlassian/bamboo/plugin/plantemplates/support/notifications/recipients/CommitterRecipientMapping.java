package com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients;

import java.util.Map;

public class CommitterRecipientMapping implements RecipientMapping
{
    @Override
    public String getRecipientKey()
    {
        return "com.atlassian.bamboo.plugin.system.notifications:recipient.committer";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public String getRecipientData(Map notificationMap)
    {
        return "";
    }
}
