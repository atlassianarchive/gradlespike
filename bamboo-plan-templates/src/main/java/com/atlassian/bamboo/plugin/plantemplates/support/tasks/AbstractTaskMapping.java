package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Collection;
import java.util.Map;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import org.apache.commons.lang.StringUtils;


public abstract class AbstractTaskMapping implements TaskMapping
{

    @SuppressWarnings("rawtypes")
    protected void copyFromDslToBambooProperty(Map task, Builder<String, String> mapBuilder, String templateKey)
    {
        copyFromDslToBambooProperty(task, mapBuilder, templateKey, templateKey);
    }
    

    @SuppressWarnings("rawtypes")
    protected void copyFromDslToBambooProperty(Map task, Builder<String, String> mapBuilder, String templateKey, String bambooKey)
    {
        String value = (String) task.get(templateKey);
        if(!Strings.isNullOrEmpty(value)){
            mapBuilder.put(bambooKey, value);
        }
    }
    
    @SuppressWarnings("rawtypes")
    protected void copyFromDslToBambooProperty(Map task, Builder<String, String> mapBuilder, String templateKey, String bambooKey, String defaultValue)
    {
        String value = (String) task.get(templateKey);
        mapBuilder.put(bambooKey, StringUtils.defaultIfBlank(value, defaultValue));
    }

    @SuppressWarnings("rawtypes")
    protected Map<String, String> buildMap(Map task, Collection<String> fields)
    {
        ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.builder();
        for (String field : fields)
        {
            copyFromDslToBambooProperty(task, mapBuilder, field);
        }

        return mapBuilder.build();
    }

}