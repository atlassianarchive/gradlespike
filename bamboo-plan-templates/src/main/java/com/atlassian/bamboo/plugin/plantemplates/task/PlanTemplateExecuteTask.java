package com.atlassian.bamboo.plugin.plantemplates.task;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;

import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;

public class PlanTemplateExecuteTask implements TaskType
{

    protected final ProcessService processService;
    protected FileResourceToString fileResourceToString = new FileResourceToString();
    
    private HttpPostRequest httpPostRequest = new HttpPostRequest();
    
    public PlanTemplateExecuteTask(ProcessService processService)
    {
        this.processService = processService;
    }

    public void setFileResourceToString(FileResourceToString fileResourceToString)
    {
        this.fileResourceToString = fileResourceToString;
    }
    public void setHttpPostRequest(HttpPostRequest httpPostRequest)
    {
        this.httpPostRequest = httpPostRequest;
    }


    private String retrievePassword(@NotNull TaskContext taskContext){
        String password = null;

        if (Boolean.parseBoolean(taskContext.getConfigurationMap().get("passwordVariableCheck"))){
            password = taskContext.getConfigurationMap().get("passwordVariable");
        } else {
            password = taskContext.getConfigurationMap().get("password");
        }

        return password;
    }


    @Override
    @NotNull
    public TaskResult execute(@NotNull TaskContext taskContext) throws TaskException
    {
        
        String templatePath = taskContext.getConfigurationMap().get("template");
        String bambooServer = taskContext.getConfigurationMap().get("bambooServer");
        String shortcuts = taskContext.getConfigurationMap().get("shortcuts");
        String username = taskContext.getConfigurationMap().get("username");
        String password = retrievePassword(taskContext);
        //make it compatible with existing plan template tasks
        String executionStrategy = StringUtils.defaultIfEmpty(taskContext.getConfigurationMap().get("executionStrategy"), PlanTemplateExecuteTaskConfigurator.EXECUTE_ON_MASTER);

        boolean isBranchBuild = taskContext.getBuildContext().isBranch();

        BuildLogger buildLogger = taskContext.getBuildLogger();
        if(Strings.isNullOrEmpty(templatePath)){
            buildLogger.addBuildLogEntry("Error templatePath is null or empty");
            return TaskResultBuilder.newBuilder(taskContext).failed().build();
        }

        String planTemplate;
        String shortcutsTemplate;
        try{
            planTemplate = fileResourceToString.convert(taskContext.getWorkingDirectory(), templatePath);
            if(planTemplate.trim().isEmpty()){
                throw new IllegalArgumentException("The plan template file " + templatePath + " is empty");
            }
            shortcutsTemplate = fileResourceToString.convert(taskContext.getWorkingDirectory(), shortcuts);
        }catch(Exception e) {
            logException(buildLogger, e);
            return TaskResultBuilder.newBuilder(taskContext).failed().build();
        }

        // Support 1 template and multiple shortcuts. The template cannot be larger than 1M and the concatenated shortcuts cannot be larger than 5M
        if(planTemplate.trim().length() > 1024*1024 || shortcutsTemplate.trim().length() > 5*1024*1024) {
            logException(buildLogger, new IllegalArgumentException("Plan template or shortcuts cannot be longer than 1MB"));
            return TaskResultBuilder.newBuilder(taskContext).failed().build();
        }
        String plankey = taskContext.getBuildContext().getPlanKey();
        planTemplate = new StringBuilder(planTemplate)
            .append("\ndeployer(planKey:'").append(plankey).append("')")
            .toString();

        buildLogger.addBuildLogEntry("Updating plan template: " + templatePath);
        
        try{
            String expandDsl = httpPostRequest.expandDsl(bambooServer, planTemplate, shortcutsTemplate, username, password);
            Iterable<String> split = Splitter.on("\n").split(expandDsl);
            for (String line : split)
            {
                buildLogger.addBuildLogEntry(line);
            }

            if((!isBranchBuild && !PlanTemplateExecuteTaskConfigurator.VALIDATE_ONLY.equals(executionStrategy))
                    || PlanTemplateExecuteTaskConfigurator.EXECUTE_ON_MASTER_AND_BRANCHES.equals(executionStrategy))
            {
                String response = httpPostRequest.postDsl(bambooServer, planTemplate, shortcutsTemplate, username, password);
                buildLogger.addBuildLogEntry(response);
            }

        }catch(HttpPostException e){
            logException(buildLogger, e);
            return TaskResultBuilder.newBuilder(taskContext).failed().build();
        }
       
        return TaskResultBuilder.newBuilder(taskContext).success().build();
    }

    private void logException(BuildLogger buildLogger, Exception e)
    {
        Iterable<String> split = Splitter.on("\n").split(e.getMessage());
        for (String line : split)
        {
            buildLogger.addErrorLogEntry(line);
        }
    }



}
