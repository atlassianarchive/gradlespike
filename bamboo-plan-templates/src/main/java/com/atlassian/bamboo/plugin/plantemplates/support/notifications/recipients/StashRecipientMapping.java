package com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients;

import java.util.Map;

public class StashRecipientMapping implements RecipientMapping
{
    @Override
    public String getRecipientKey()
    {
        return "com.atlassian.bamboo.plugins.bamboo-stash-plugin:recipient.stash";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public String getRecipientData(Map notificationMap)
    {
        return "";
    }
}
