package com.atlassian.bamboo.plugin.plantemplates.support.notifications;

import com.atlassian.bamboo.notification.NotificationManager;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.AfterXFailuresNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.BuildCompleteNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.ChangeOfJobStatusNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.ChangeOfStatusNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.CommentAddedNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.DeploymentFinishedMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.DeploymentStartedAndFinishedMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.FailedJobNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.FailedNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.FirstFailedJobForPlanNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.JobCompleteNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.JobErrorNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.JobHungNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.JobQueueTimeoutNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.NoAgentNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.NotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.ResponsibilitiesChangedMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.ResponsibleRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.CommitterRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.EmailRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.GroupRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.HipChatRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.HttpNotificationsRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.IMRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.RecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.StashRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.UserRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.WatcherRecipientMapping;

public class NotificationMappingFactory
{
    private NotificationManager notificationManager;

    public NotificationMappingFactory(NotificationManager notificationManager)
    {
        this.notificationManager = notificationManager;
    }

    public NotificationMapping create(String notificationType, String recipientType) {

        RecipientMapping recipientMapping = getRecipientMapping(recipientType);

        return getNotificationMapping(notificationType.toLowerCase(), recipientMapping);
    }

    private NotificationMapping getNotificationMapping(String notificationType, RecipientMapping recipientMapping)
    {
        /** plans notification **/

        if (notificationType.equalsIgnoreCase("all builds completed")) {
            return new BuildCompleteNotificationMapping(recipientMapping, notificationManager);
        }
        if (notificationType.equalsIgnoreCase("change of build status")) {
            return new ChangeOfStatusNotificationMapping(recipientMapping, notificationManager);
        }
        if (notificationType.equalsIgnoreCase("failed builds and first successful")) {
            return new FailedNotificationMapping(recipientMapping, notificationManager);
        }
        if (notificationType.equalsIgnoreCase("after x build failures")) {
            return new AfterXFailuresNotificationMapping(recipientMapping, notificationManager);
        }
        if (notificationType.equalsIgnoreCase("comment added")) {
            return new CommentAddedNotificationMapping(recipientMapping, notificationManager);
        }
        if (notificationType.equalsIgnoreCase("change of responsibilities")) {
            return new ResponsibilitiesChangedMapping(recipientMapping, notificationManager);
        }

        /** Job notifications**/

        if (notificationType.equalsIgnoreCase("all jobs completed")) {
            return new JobCompleteNotificationMapping(recipientMapping, notificationManager);
        }
        if (notificationType.equalsIgnoreCase("change of job status")) {
            return new ChangeOfJobStatusNotificationMapping(recipientMapping, notificationManager);
        }
        if (notificationType.equalsIgnoreCase("failed jobs and first successful")) {
            return new FailedJobNotificationMapping(recipientMapping, notificationManager);
        }
        if (notificationType.equalsIgnoreCase("first failed job for plan")) {
            return new FirstFailedJobForPlanNotificationMapping(recipientMapping, notificationManager);
        }
        if (notificationType.equalsIgnoreCase("job error")) {
            return new JobErrorNotificationMapping(recipientMapping, notificationManager);
        }
        if (notificationType.equalsIgnoreCase("job hung")) {
            return new JobHungNotificationMapping(recipientMapping, notificationManager);
        }
        if (notificationType.equalsIgnoreCase("job queue timeout")) {
            return new JobQueueTimeoutNotificationMapping(recipientMapping, notificationManager);
        }
        if (notificationType.equalsIgnoreCase("job queued without capable agents")) {
            return new NoAgentNotificationMapping(recipientMapping, notificationManager);
        }
        
        /** DeploymentNotifications **/
        if (notificationType.equalsIgnoreCase("deployment started and finished")) {
            return new DeploymentStartedAndFinishedMapping(recipientMapping, notificationManager);
        }
        if (notificationType.equalsIgnoreCase("deployment finished")) {
            return new DeploymentFinishedMapping(recipientMapping, notificationManager);
        }
        
        throw new IllegalArgumentException("Unknown notification condition: " + notificationType);
    }

    public RecipientMapping getRecipientMapping(String recipientType) {
        if (recipientType.equalsIgnoreCase("email")) {
            return new EmailRecipientMapping();
        }
        if (recipientType.equalsIgnoreCase("user")) {
            return new UserRecipientMapping();
        }
        if (recipientType.equalsIgnoreCase("group")) {
            return new GroupRecipientMapping();
        }
        if (recipientType.equalsIgnoreCase("responsible")) {
            return new ResponsibleRecipientMapping();
        }
        if (recipientType.equalsIgnoreCase("committers")) {
            return new CommitterRecipientMapping();
        }
        if (recipientType.equalsIgnoreCase("watchers")) {
            return new WatcherRecipientMapping();
        }
        if (recipientType.equalsIgnoreCase("IM")) {
            return new IMRecipientMapping();
        }
        if (recipientType.equalsIgnoreCase("hipchat")) {
            return new HipChatRecipientMapping();
        }
        if (recipientType.equalsIgnoreCase("stash")) {
            return new StashRecipientMapping();
        }
        if (recipientType.equalsIgnoreCase("httpNotification")) {
            return new HttpNotificationsRecipientMapping();
        }
        throw new IllegalArgumentException("Unsupported notification recipient: " + recipientType);
    }
}
