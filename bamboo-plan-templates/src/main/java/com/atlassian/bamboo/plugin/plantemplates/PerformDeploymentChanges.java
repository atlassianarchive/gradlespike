package com.atlassian.bamboo.plugin.plantemplates;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.deployments.projects.DeploymentProject;
import com.atlassian.bamboo.deployments.projects.service.DeploymentProjectService;
import com.atlassian.bamboo.exception.WebValidationException;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plugin.plantemplates.support.DeploymentVersioningService;
import com.atlassian.bamboo.plugin.plantemplates.support.DslTagHelper;
import com.atlassian.bamboo.plugin.plantemplates.support.EnvironmentDeploymentService;

import org.apache.log4j.Logger;

public class PerformDeploymentChanges
{
    private static final Logger log = Logger.getLogger(PerformDeploymentChanges.class);
    
    private DeploymentProjectService deploymentProjectService;

    private EnvironmentDeploymentService environmentDeploymentService;

    private DeploymentVersioningService deploymentVersioningService;
    
    public PerformDeploymentChanges(DeploymentProjectService deploymentProjectService, EnvironmentDeploymentService environmentDeploymentService, DeploymentVersioningService deploymentVersioningService)
    {
        this.deploymentProjectService = deploymentProjectService;
        this.environmentDeploymentService = environmentDeploymentService;
        this.deploymentVersioningService = deploymentVersioningService; 
        
    }

    @SuppressWarnings("rawtypes")
    public void performChanges(Map deploymentRepresentation)
    {
        List deploymentRepresentations = DslTagHelper.getDslTagList(deploymentRepresentation.get("deployment"));
        for (Object deploymentObject : deploymentRepresentations)
        {
            createOrUpdateDeployment((Map) deploymentObject);
        }
    }

    @SuppressWarnings("rawtypes") 
    private void createOrUpdateDeployment(Map deploymentMap)
    {
        String planKey = (String) deploymentMap.get("planKey");
        final String name = (String) deploymentMap.get("name");
        String description = (String) deploymentMap.get("description");
        
        log.info("PlanTemplates: Performing changes for deployment '" + name +"' for the plankey '" + planKey + "'");
        
        List<DeploymentProject> deploymentProjectsRelatedToPlan = deploymentProjectService.getDeploymentProjectsRelatedToPlan(PlanKeys.getPlanKey(planKey));

        DeploymentProject deploymentProjectToEdit = findDeploymentByName(name, deploymentProjectsRelatedToPlan);
        
        try
        {
            DeploymentProject deploymentProject = null;
            if (deploymentProjectToEdit == null)
            {
                log.info("PlanTemplates: Add a new deployment '" + name +"' for the plankey '" + planKey + "'");
                deploymentProject = deploymentProjectService.addDeploymentProject(name, description, planKey);
            }
            else
            {
                log.info("PlanTemplates: Editing deployment '" + name +"' for the plankey '" + planKey + "'");
                deploymentProject = deploymentProjectService.editDeploymentProject(deploymentProjectToEdit.getId(), name, description, planKey);
            }
            
            // environment changes
            environmentDeploymentService.performChanges(deploymentMap.get("environment"), deploymentProject);
            deploymentVersioningService.performChanges(deploymentMap.get("versioning"), deploymentProject);
            log.info("PlanTemplates: Finished to perform changes for deployment '" + name +"' for the plankey '" + planKey + "'");

        }
        catch (WebValidationException e)
        {
            log.error("Error performing changes on deployment with name " + name, e);
            throw new IllegalArgumentException(e);
        }
    }

    private DeploymentProject findDeploymentByName(final String name,
                                                   List<DeploymentProject> deploymentProjectsRelatedToPlan)
    {
        for (DeploymentProject deploymentProject : deploymentProjectsRelatedToPlan)
        {
            if (name.equals(deploymentProject.getName()))
                return deploymentProject;
        }
        return null;
    }
    
    
    
    
}
