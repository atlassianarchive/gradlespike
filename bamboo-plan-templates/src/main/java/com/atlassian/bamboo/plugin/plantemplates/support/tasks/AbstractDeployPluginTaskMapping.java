package com.atlassian.bamboo.plugin.plantemplates.support.tasks;


import com.atlassian.bamboo.plugin.plantemplates.support.tasks.deployTask.Crypto;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

abstract public class AbstractDeployPluginTaskMapping extends AbstractTaskMapping{


    private Crypto crypto = new Crypto();

    abstract protected String taskKey();

    public void setCrypto(Crypto crypto)
    {
        this.crypto = crypto;
    }


    @Override
    public Map<String, String> convert(@SuppressWarnings("rawtypes") Map task) {

        ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.<String, String> builder();

        mapBuilder.put("userDescription", (String) task.get("description"));

        String taskDisabled = (String) task.get("taskDisabled");
        if (Strings.isNullOrEmpty(taskDisabled))
        {
            mapBuilder.put("taskDisabled", "false");
        }
        else
        {
            mapBuilder.put("taskDisabled", taskDisabled);
        }

        copyFromDslToBambooProperty(task, mapBuilder, "username", "confDeployUsername");

        String rawpass = (String) task.get("password");
        if(!Strings.isNullOrEmpty(rawpass)){
            Map<String, String> encrypt = crypto.encrypt(rawpass);
            mapBuilder.put("confDeployPassword", encrypt.get("encrypted"));
            mapBuilder.put("confDeployKey", encrypt.get("key"));
        }
        String passwordVariable = (String) task.get("passwordVariable");
        if(!Strings.isNullOrEmpty(passwordVariable)){
            mapBuilder.put("confDeployPasswordVariable", passwordVariable);
            mapBuilder.put("confDeployPasswordVariableCheck", "true");
        }

        copyFromDslToBambooProperty(task, mapBuilder, "url", "confDeployURL");
        copyFromDslToBambooProperty(task, mapBuilder, "artifact", "confDeployJar");

        copyFromDslToBambooProperty(task, mapBuilder, "enableTrafficLogging");
        copyFromDslToBambooProperty(task, mapBuilder, "certificateCheckDisabled");

        String pluginInstallationTimeout = (String) task.get("pluginInstallationTimeout");
        if (Strings.isNullOrEmpty(pluginInstallationTimeout)){
            mapBuilder.put("pluginInstallationTimeout", "90");
        }
        else {
            mapBuilder.put("pluginInstallationTimeout", pluginInstallationTimeout);
        }


        String runOnBbranch = (String) task.get("runOnBranch");
        if("true".equals(runOnBbranch)){
            mapBuilder.put("deployBranchEnabled", "true");
        }

        String useAtlassianId = (String) task.get("useAtlassianId");
        if("true".equalsIgnoreCase(useAtlassianId)) {
            mapBuilder.put("useAtlassianId", "true");
            copyFromDslToBambooProperty(task, mapBuilder, "atlassianIdBaseURL");
            copyFromDslToBambooProperty(task, mapBuilder, "atlassianIdUsername");
            String rawAtlassianIdPassword = (String) task.get("atlassianIdPassword");
            if(!Strings.isNullOrEmpty(rawAtlassianIdPassword)) {
                Map<String, String> encrypt = crypto.encrypt(rawAtlassianIdPassword);
                mapBuilder.put("atlassianIdKey", encrypt.get("key"));
                mapBuilder.put("atlassianIdPassword", encrypt.get("encrypted"));
            }
            String atlassianIdPasswordVariable = (String) task.get("atlassianIdPasswordVariable");
            if(!Strings.isNullOrEmpty(atlassianIdPasswordVariable)) {
                mapBuilder.put("atlassianIdPasswordVariableCheck", "true");
                mapBuilder.put("atlassianIdPasswordVariable", atlassianIdPasswordVariable);
            }
            String useAtlassianIdWebSudo = (String) task.get("useAtlassianIdWebSudo");
            mapBuilder.put("useAtlassianIdWebSudo", Boolean.valueOf(useAtlassianIdWebSudo).toString());
            copyFromDslToBambooProperty(task, mapBuilder, "atlassianIdAppName");
        }

        mapBuilder.put("createTaskKey", taskKey());

        return mapBuilder.build();
    }
}
