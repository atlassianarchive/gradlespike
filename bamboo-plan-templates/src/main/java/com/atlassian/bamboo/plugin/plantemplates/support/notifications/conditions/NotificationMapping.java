package com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions;

import com.atlassian.bamboo.notification.NotificationRule;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.RecipientMapping;

import java.util.Map;

public interface NotificationMapping
{
    @SuppressWarnings("rawtypes")
    public NotificationRule createNotificationRule(Map notificationMap);

    public RecipientMapping getRecipientMapping();
}
