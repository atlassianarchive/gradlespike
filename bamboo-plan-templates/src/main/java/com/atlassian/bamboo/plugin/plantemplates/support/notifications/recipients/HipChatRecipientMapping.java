package com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients;

import java.util.Map;

public class HipChatRecipientMapping implements RecipientMapping
{
    @Override
    public String getRecipientKey()
    {
        return "com.atlassian.bamboo.plugins.bamboo-hipchat:recipient.hipchat";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public String getRecipientData(Map notificationMap)
    {
        String roomName = (String) notificationMap.get("room");
        String apiToken = (String) notificationMap.get("apiKey");
        String notify = (String) notificationMap.get("notify");

        if (notify == null) {
            notify = "false";
        }

        return String.format("%s|%s|%s", apiToken, notify, roomName);
    }
}
