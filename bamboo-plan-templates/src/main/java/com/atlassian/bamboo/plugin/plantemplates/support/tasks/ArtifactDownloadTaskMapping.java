package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinition;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionManager;
import com.atlassian.bamboo.plugin.plantemplates.support.DslTagHelper;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;

import org.apache.log4j.Logger;

public class ArtifactDownloadTaskMapping extends AbstractTaskMapping implements TaskMapping
{
    private static final Logger log = Logger.getLogger(ArtifactDownloadTaskMapping.class);

    
    public static final String ARTIFACT_DOWNLOADER_KEY = "com.atlassian.bamboo.plugins.bamboo-artifact-downloader-plugin:artifactdownloadertask";
    
    private ArtifactDefinitionManager artifactDefinitionManager;
    private PlanManager planManager;

    public ArtifactDownloadTaskMapping(ArtifactDefinitionManager artifactDefinitionManager, PlanManager planManager)
    {
        this.artifactDefinitionManager = artifactDefinitionManager;
        this.planManager = planManager;
    }

    @SuppressWarnings("rawtypes")
    public Map<String, String> convert(Map task)
    {
        
        ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.<String, String>builder();

        String description = (String) task.get("description");
        String planKey = (String) task.get("planKey");

        String taskDisabled = (String)task.get("taskDisabled");
        if(Strings.isNullOrEmpty(taskDisabled)){
            mapBuilder.put("taskDisabled", "false");
        }else{
            mapBuilder.put("taskDisabled", taskDisabled);
        }

        Chain planChain = getPlanChain(planKey);
        mapBuilder.put("sourcePlanKey", planKey);

        mapBuilder.put("userDescription", description);
        mapBuilder.put("createTaskKey", ARTIFACT_DOWNLOADER_KEY);

        
        List artifacts = DslTagHelper.getDslTagList(task.get("artifact"));        
        int artifactIndex = 0;
        for (Object artifactObject : artifacts)
        {
            Map artifactMap = (Map) artifactObject;
            String artifactName = (String) artifactMap.get("name");
            String localPath = (String) artifactMap.get("localPath");
            
            mapBuilder.put("artifactId_" + artifactIndex, findArtifactDefinitionIdByName(planChain, artifactName));
            mapBuilder.put("localPath_" + artifactIndex , localPath);
            artifactIndex++;
        }
        
        return mapBuilder.build();

    }

    private Chain getPlanChain(String planKey)
    {
        if (Strings.isNullOrEmpty(planKey)) {
            String errorMsg = "PlanTemplates: ArtifactDownloadTask can not get the plan with plan key : '" + planKey + "'";
            log.error(errorMsg);
            throw new IllegalArgumentException(errorMsg);
        }
        Chain planChain = planManager.getPlanByKey(PlanKeys.getPlanKey(planKey), Chain.class);
        if(planChain == null) {
            String errorMsg = "PlanTemplates: ArtifactDownloadTask can not get the plan with plan key : '" + planKey + "'";
            log.error(errorMsg);
            throw new IllegalArgumentException(errorMsg);
        }
        return planChain;
    }

    private String findArtifactDefinitionIdByName(Chain planChain, String name)
    {
        List<ArtifactDefinition> artifactsByPlan = artifactDefinitionManager.findSharedArtifactsByChain(planChain);
        
        for (ArtifactDefinition artifactDefinition : artifactsByPlan)
        {
            if (artifactDefinition != null && name.equals(artifactDefinition.getName()))
                return Long.toString(artifactDefinition.getId());
        }
        
        return "-1"; // means all artifacts
    }

}
