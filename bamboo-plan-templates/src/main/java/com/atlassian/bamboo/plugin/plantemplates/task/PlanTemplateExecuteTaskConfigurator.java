package com.atlassian.bamboo.plugin.plantemplates.task;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.Pair;
import com.atlassian.bamboo.utils.error.ErrorCollection;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import com.opensymphony.xwork2.TextProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class PlanTemplateExecuteTaskConfigurator extends AbstractTaskConfigurator
{

    private static final List<String> FIELDS_TO_COPY = ImmutableList.of(
                                                                        "bambooServer", 
                                                                        "template",
                                                                        "shortcuts",
                                                                        "username",
                                                                        "password",
                                                                        "passwordVariable",
                                                                        "passwordVariableCheck",
                                                                        "executionStrategy"
                                                                        );

    public static final String VALIDATE_ONLY = "executionStrategy.validateOnly";
    public static final String EXECUTE_ON_MASTER = "executionStrategy.executeOnMaster";
    // Stash uses branch build for plan templates
    public static final String EXECUTE_ON_MASTER_AND_BRANCHES = "executionStrategy.executeOnMasterAndBranches";

    private TextProvider textProvider;

    @Override
    @NotNull
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params,
                                                     @Nullable TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> config = Maps.newHashMap();


        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(config, params, FIELDS_TO_COPY);
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        context.put("executionStrategies", getExecutionStrategies());
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition)
    {
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
        // make it compatible with existing plan template tasks
        if(context.get("executionStrategy") == null)
        {
            context.put("executionStrategy", EXECUTE_ON_MASTER);
        }
        context.put("executionStrategies", getExecutionStrategies());
    }

    @Override
    public void populateContextForView(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition)
    {
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
        // make it compatible with existing plan template tasks
        if(context.get("executionStrategy") == null)
        {
            context.put("executionStrategy", EXECUTE_ON_MASTER);
        }
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection)
    {
        if(Strings.isNullOrEmpty(params.getString("bambooServer")))
            errorCollection.addErrorMessage("Bamboo server can not be empty");
        if(Strings.isNullOrEmpty(params.getString("template")))
            errorCollection.addErrorMessage("template can not be empty");
        if(Strings.isNullOrEmpty(params.getString("username")))
            errorCollection.addErrorMessage("username can not be empty");
        
        super.validate(params, errorCollection);
    }

    public Collection<Pair<String, String>> getExecutionStrategies()
    {
        // use textProvider.getText(key, defaultValue)
        // because the resource file might not be picked up if update the plugin without restarting bamboo
        return Lists.newArrayList(Pair.make(EXECUTE_ON_MASTER, textProvider.getText(EXECUTE_ON_MASTER, "Execute on Master, validate only on Branches")),
                Pair.make(VALIDATE_ONLY, textProvider.getText(VALIDATE_ONLY, "Validate only on both Master and Branches")),
                Pair.make(EXECUTE_ON_MASTER_AND_BRANCHES, textProvider.getText(EXECUTE_ON_MASTER_AND_BRANCHES, "Execute on both Master and Branches")));
    }

    public void setTextProvider(TextProvider textProvider)
    {
        this.textProvider = textProvider;
    }

    public TextProvider getTextProvider()
    {
        return textProvider;
    }

}
