package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentTaskService;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.TaskMappingFactory;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskManager;
import com.atlassian.bamboo.task.TaskModuleDescriptor;
import com.atlassian.spring.container.ContainerManager;

import com.google.common.collect.Lists;

import org.apache.log4j.Logger;

public class TaskDeploymentService
{
    
    private static final Logger log = Logger.getLogger(TaskDeploymentService.class);
    
    private EnvironmentTaskService environmentTaskService;    
    private TaskManager taskManager;
    private TaskMappingFactory taskMappingFactory;
    private PerformChangesForHacksOnContinuousDeploymentPlugin performChangesForHacksOnContinuousDeploymentPlugin;
    
    public TaskDeploymentService(EnvironmentTaskService environmentTaskService, TaskMappingFactory taskMappingFactory, PerformChangesForHacksOnContinuousDeploymentPlugin performChangesForHacksOnContinuousDeploymentPlugin)
    {
        this.environmentTaskService = environmentTaskService;
        this.taskMappingFactory = taskMappingFactory;
        this.performChangesForHacksOnContinuousDeploymentPlugin = performChangesForHacksOnContinuousDeploymentPlugin;
    }

    public void setTaskManager(TaskManager taskManager)
    {
        this.taskManager = taskManager;
    }
    
    public TaskManager getTaskManager()
    {
        if (this.taskManager == null)
            taskManager = (TaskManager) ContainerManager.getComponent("taskManager");
        return taskManager;
    }

    @SuppressWarnings("rawtypes")
    public List<TaskDefinition> performTasksChanges(Object taskRepresentation, Environment environment)
    {
        deleteExistingTask(environment);
        List<TaskDefinition> taskDefinitions = Lists.newArrayList();
        
        List tasks = DslTagHelper.getDslTagList(taskRepresentation);
        for (Object taskObject : tasks)
        {
            TaskDefinition taskDefinition = createTask((Map) taskObject, environment);
            taskDefinitions.add(taskDefinition);
        }
        // FIXME Continuous Deployment Plugin is very hacky we have to apply changes after creation to make the fix 
        // we can remove this when they fix the current plugin or bamboo deployments itself 
        performChangesForHacksOnContinuousDeploymentPlugin.perform(environment, taskDefinitions);
        
        return taskDefinitions;
    }
    
    private void deleteExistingTask(Environment environment)
    {
        List<TaskDefinition> taskDefinitions = environment.getTaskDefinitions();
        for (TaskDefinition taskDefinition : taskDefinitions)
        {
            environmentTaskService.deleteTask(environment.getId(), taskDefinition.getId());
        }
    }

    @SuppressWarnings("rawtypes")
    private TaskDefinition createTask(Map taskRepresentation, Environment environment)
    {
        String taskType = (String) taskRepresentation.get("type");
        
        // TODO how to put a deployment task as a final???
        // String isFinal = (String) taskRepresentation.get("final");
        
        Map<String, String> taskParams = taskMappingFactory.create(taskType).convert(taskRepresentation);
        final TaskModuleDescriptor taskDescriptor = getTaskManager().getTaskDescriptor(taskParams.get("createTaskKey"));
        if(taskDescriptor == null){
            String error = "PlanTemplates: Deployments task - Could not get the taskDescriptor of the task '" + taskParams.get("userDescription") + "' with type: '" + taskType  + "' bamboo key '" +  taskParams.get("createTaskKey") + "'";
            log.error(error);
            throw new RuntimeException(error);
        }

        log.info("PlanTemplates: Creating Deployment task: '" + taskParams.get("userDescription") + "' with type: '" + taskType  + "'");        
        TaskDefinition taskDefinition = environmentTaskService.createTask(environment.getId(), taskDescriptor, taskParams.get("userDescription"), true, taskParams);
        return taskDefinition;
        
    }

}
