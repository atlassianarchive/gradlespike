package com.atlassian.bamboo.plugin.plantemplates.support.tasks;


public class DeployConfluencePluginTaskMapping extends AbstractDeployPluginTaskMapping {

    public static final String DEPLOY_CONF_PLUGIN_TASK_PLUGIN_KEY = "com.atlassian.bamboo.plugins.deploy.continuous-plugin-deployment:confdeploy";


    protected String taskKey(){
        return DEPLOY_CONF_PLUGIN_TASK_PLUGIN_KEY;
    }


}
