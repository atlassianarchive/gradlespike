package com.atlassian.bamboo.plugin.plantemplates.task;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Set;

import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.google.common.io.Files;
import org.apache.commons.io.IOUtils;

public class FileResourceToString
{
    /**
     * Read file(s) in the specified path and return the content.
     * If there are more than 1 files, separate them with comma. And the content will be concatenated.
     *
     * @param workingDirectory Working directory of bamboo build agent
     * @param templatePaths Path of the file(s). Separated by comma if more than 1
     * @return Content of the files.
     * @throws IOException
     */
    public String convert(File workingDirectory, final String templatePaths) throws IOException
    {
        if(Strings.isNullOrEmpty(templatePaths))
        {
            return "";
        }
        StringBuilder shortCut = new StringBuilder();
        for(File file : validateFiles(workingDirectory, templatePaths))
        {
            shortCut.append(Files.toString(file, Charsets.UTF_8));
            shortCut.append(IOUtils.LINE_SEPARATOR);
        }
        return shortCut.toString();
    }

    private Set<File> validateFiles(File workingDirectory, final String templatePaths) throws IOException
    {
        Set<File> files = Sets.newHashSet();
        Set<String> nonExistingFiles = Sets.newHashSet();
        Set<String> overSizedFiles = Sets.newHashSet();
        for(String templatePath : templatePaths.split(","))
        {
            templatePath = templatePath.trim();
            if(!templatePath.isEmpty())
            {
                File file = new File(workingDirectory, templatePath);
                if(!file.exists() || file.isDirectory())
                {
                    nonExistingFiles.add(templatePath);
                    continue;
                }

                if(file.length() > 1024 * 1024)
                {
                    overSizedFiles.add(templatePath);
                }

                files.add(file);
            }
        }
        StringBuilder errorMsg = new StringBuilder();
        if(!nonExistingFiles.isEmpty())
        {
            errorMsg.append("The following files do not exist or are directories").append(IOUtils.LINE_SEPARATOR);
            for(String nonExistingFile : nonExistingFiles)
                errorMsg.append(nonExistingFile).append(IOUtils.LINE_SEPARATOR);
        }
        if(!overSizedFiles.isEmpty())
        {
            errorMsg.append("The following files are over 1MB, they might slow down bamboo").append(IOUtils.LINE_SEPARATOR);
            for(String overSizedFile : overSizedFiles)
                errorMsg.append(overSizedFile).append(IOUtils.LINE_SEPARATOR);
        }
        if(errorMsg.length() > 0)
            throw new IllegalArgumentException(errorMsg.toString());
        return files;
    }
}
