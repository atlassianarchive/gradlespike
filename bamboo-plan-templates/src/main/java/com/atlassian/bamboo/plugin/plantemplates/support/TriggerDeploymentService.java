package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.bamboo.build.strategy.BuildStrategy;
import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentTriggerService;
import com.atlassian.bamboo.plugin.plantemplates.support.triggers.TriggerMappingFactory;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;

import com.google.common.collect.Sets;

import org.apache.log4j.Logger;

public class TriggerDeploymentService
{
    private static final Logger log = Logger.getLogger(TriggerDeploymentService.class);

    private TriggerMappingFactory triggerMappingFactory;
    private EnvironmentTriggerService environmentTriggerService;

    public TriggerDeploymentService(TriggerMappingFactory triggerMappingFactory,
                                    EnvironmentTriggerService environmentTriggerService)
    {
        this.triggerMappingFactory = triggerMappingFactory;
        this.environmentTriggerService = environmentTriggerService;
    }

    @SuppressWarnings("rawtypes")
    public void performTriggersChanges(Object triggersRepresentation, Environment environment)
    {
        

        deleteExistingTriggers(environment);
        
        List triggers = DslTagHelper.getDslTagList(triggersRepresentation);
        for (Object trigger : triggers)
        {
            createTriggers((Map) trigger, environment);
        }
        
    }

    private void deleteExistingTriggers(Environment environment)
    {
        List<BuildStrategy> triggers = environment.getTriggers();
        if(triggers == null) {
            return;
        }
        for (BuildStrategy buildStrategy : triggers)
        {
            environmentTriggerService.deleteEnvironmentTrigger(environment.getId(), buildStrategy.getId());
        }
    }
    
    @SuppressWarnings("rawtypes")
    private void createTriggers(Map trigger, Environment environment)
    {
        String description = (String) trigger.get("description");
        String type = (String) trigger.get("type");
        
        // no repositories for deployments
        Set<Long> repositoriesIds = Sets.<Long>newHashSet();
        
        Map<String, String> converted = triggerMappingFactory.create(type).convert(trigger);
        BuildConfiguration buildConfiguration = convertToBuildConfiguration(converted);

        log.info("PlanTemplates: Adding a deployment trigger  '" + description + "' for the environment: " + environment.getName());        
        environmentTriggerService.createEnvironmentTrigger(environment.getId(), description, repositoriesIds, buildConfiguration);
    }
    
    private BuildConfiguration convertToBuildConfiguration(Map<String, String> triggerMap) {
        BuildConfiguration buildConfiguration = new BuildConfiguration();
        for (String key : triggerMap.keySet())
        {
            buildConfiguration.setProperty(key, triggerMap.get(key));
        }
        
        return buildConfiguration;
    }

}
