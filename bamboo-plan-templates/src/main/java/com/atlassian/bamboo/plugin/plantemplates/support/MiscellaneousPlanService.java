package com.atlassian.bamboo.plugin.plantemplates.support;


import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.plan.Plan;

import java.util.List;
import java.util.Map;

/**
 * Update a Plan's <code>BuildDefinition</code> according to <code>planMiscellaneous</code> in a template
 * Currently it only enables/disables sandbox of a plan
 */
public class MiscellaneousPlanService {
    private BuildDefinitionManager buildDefinitionManager;

    public MiscellaneousPlanService(BuildDefinitionManager buildDefinitionManager){
        this.buildDefinitionManager = buildDefinitionManager;
    }

    /**
     * Update a Plan's BuildDefinition with the key/value pairs passed
     *
     * @param plan The plan to be updated
     * @param miscRepresentation A map contains key/value pairs of the BuildDefinition's attributes
     * @return The plan that has been updated
     */
    public Plan performChanges(Plan plan, Map<String, Object> miscRepresentation){

        if(miscRepresentation != null) {
            BuildDefinition buildDefinition = buildDefinitionManager.getBuildDefinition(plan);
            createOrUpdateSandbox(buildDefinition, (Map<String, String>) miscRepresentation.get("sandbox"));
            buildDefinitionManager.savePlanAndDefinition(plan, buildDefinition);
        }
        return plan;
    }

    void createOrUpdateSandbox(BuildDefinition buildDefinition, Map<String, String> sandboxConfig) {
        if(sandboxConfig != null) {
            Boolean enabled = Boolean.parseBoolean(sandboxConfig.get("enabled"));
            if(enabled) {
                String strategy = Boolean.parseBoolean(sandboxConfig.get("automaticPromotion"))? "automatic": "manual";
                buildDefinition.getCustomConfiguration().put("custom.sandbox.promotion.strategy", "sandbox.promotion." + strategy);
            }
            buildDefinition.getCustomConfiguration().put("custom.sandbox.enabled", enabled.toString());
        }
    }

}
