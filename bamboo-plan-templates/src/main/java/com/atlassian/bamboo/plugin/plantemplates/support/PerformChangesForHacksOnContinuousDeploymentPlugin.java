package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentTaskService;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.ArtifactDownloadTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.DeployPluginTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.DeployBambooPluginTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.DeployConfluencePluginTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.DeployJiraPluginTaskMapping;
import com.atlassian.bamboo.task.TaskDefinition;

import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.apache.log4j.Logger;


public class PerformChangesForHacksOnContinuousDeploymentPlugin
{
    
    private static final Logger log = Logger.getLogger(PerformChangesForHacksOnContinuousDeploymentPlugin.class);

    
    private EnvironmentTaskService environmentTaskService;

    public PerformChangesForHacksOnContinuousDeploymentPlugin(EnvironmentTaskService environmentTaskService)
    {
        this.environmentTaskService = environmentTaskService;
    }
    
    public void perform(Environment environment, List<TaskDefinition> taskDefinitions)
    {
        // format of the hack
        // confDeployJar = v2:{artifactId}:{artifactDownloaderTaskId}:{artifactDownloaderTransferId}:{artifactName}

        // It seems that getting the list of task definitions from the environment doesn't return the last task added :S
        // List<TaskDefinition> taskDefinitions = environment.getTaskDefinitions();
        
        TaskDefinition artifactDownloadTask = findDownloadTask(taskDefinitions);
        if(artifactDownloadTask == null){
            return;
        }
        
        List<TaskDefinition> deployTasks = findDeployTasks(taskDefinitions);
        
        if(deployTasks == null || deployTasks.isEmpty()){
            log.info("PlanTemplates: Patch for the hack to the deployment task not applied for environment: '" + environment.getName() + "' no deploy task found");
        }
        
        for (TaskDefinition deployTask : deployTasks)
        {
            editDeployTask(environment, artifactDownloadTask, deployTask);
        }
     }

    private void editDeployTask(Environment environment, TaskDefinition artifactDownloadTask, TaskDefinition deployTask)
    {
        String artifactName = deployTask.getConfiguration().get("confDeployJar");
        if(artifactName == null || artifactName.startsWith("v2:")){
            // needs to update the task id
            Iterable<String> split = Splitter.on(':').trimResults().omitEmptyStrings().split(artifactName);
            artifactName = Iterables.getLast(split);
        }
        String transferId = "0";
        Long taskId = artifactDownloadTask.getId();
        String artifactId = artifactDownloadTask.getConfiguration().get("artifactId_0");
        String confDeployJar =  Joiner.on(":").skipNulls().join("v2", artifactId, taskId, transferId, artifactName);
        
        Map<String, String> newConfigurationMap = Maps.newHashMap(deployTask.getConfiguration());
        newConfigurationMap.put("confDeployJar", confDeployJar);
        
        log.info("PlanTemplates: Applying patch for the hack to the deployment task '" + deployTask.getUserDescription() + "' to set confDeployJar=" + confDeployJar );
        environmentTaskService.editTask(environment.getId(), deployTask.getId(), deployTask.getUserDescription(), deployTask.isEnabled(), newConfigurationMap);
    }

    private TaskDefinition findDownloadTask(List<TaskDefinition> taskDefinitions)
    {
        for (TaskDefinition taskDefinition : taskDefinitions)
        {
            if(ArtifactDownloadTaskMapping.ARTIFACT_DOWNLOADER_KEY.equals(taskDefinition.getPluginKey())){
                return taskDefinition;
            }
        }
        
        log.info("PlanTemplates: Patch for the hack not applied because Download task is not found");
        return null;
    }
    
    private List<TaskDefinition> findDeployTasks(List<TaskDefinition> taskDefinitions)
    {
        return Lists.newArrayList(Iterables.filter(taskDefinitions, new Predicate<TaskDefinition>()
        {
            @Override
            public boolean apply(@Nullable TaskDefinition taskDefinition)
            {
                String pluginKey = taskDefinition.getPluginKey();
                return DeployBambooPluginTaskMapping.DEPLOY_BAMBOO_PLUGIN_TASK_PLUGIN_KEY.equals(pluginKey) 
                    || DeployConfluencePluginTaskMapping.DEPLOY_CONF_PLUGIN_TASK_PLUGIN_KEY.equals(pluginKey)
                    || DeployJiraPluginTaskMapping.DEPLOY_JIRA_PLUGIN_TASK_PLUGIN_KEY.equals(pluginKey)
                    || DeployPluginTaskMapping.DEPLOY_PLUGIN_TASK_PLUGIN_KEY.equals(pluginKey);
            }
        }));
    }

}


