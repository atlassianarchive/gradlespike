package com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions;

import com.atlassian.bamboo.notification.NotificationManager;
import com.atlassian.bamboo.notification.NotificationRule;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.RecipientMapping;

import java.util.Map;

public class NoAgentNotificationMapping extends AbstractNotificationMapping
{
    public NoAgentNotificationMapping(RecipientMapping recipientMapping, NotificationManager notificationManager)
    {
        super(recipientMapping, notificationManager);
    }

    @Override
    @SuppressWarnings("rawtypes")
    public NotificationRule createNotificationRule(Map notificationMap)
    {
        String conditionKey = "com.atlassian.bamboo.plugin.system.notifications:buildMissingCapableAgent";
        String conditionData = "";
        String recipientData = recipientMapping.getRecipientData(notificationMap);
        String recipientKey = recipientMapping.getRecipientKey();

        NotificationRule notificationRule = notificationManager.createNotificationRule(conditionKey, conditionData, recipientData, recipientKey);
        return notificationRule;
    }
}
