package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.variable.VariableConfigurationService;
import com.atlassian.bamboo.variable.VariableDefinition;

import com.google.common.collect.Lists;

import org.apache.log4j.Logger;

public class VariableService
{
    private static final Logger log = Logger.getLogger(VariableService.class);
    
    private VariableConfigurationService variableConfigurationService;
    
    public VariableService(VariableConfigurationService variableConfigurationService)
    {
        this.variableConfigurationService = variableConfigurationService;
    }

    @SuppressWarnings("rawtypes")
    public void performVariableChanges(Chain chain, Object variableRepresentation){
        List variables = null;
        if (variableRepresentation instanceof List) {
            variables = (List) variableRepresentation;
        }
        else if (variableRepresentation instanceof Map) {
            variables = Lists.newArrayList(variableRepresentation);
        }
        else if (variableRepresentation == null) {
            variables = Lists.newArrayList();
        }
        else {
            throw new IllegalArgumentException("Plan variables were neither a list nor a map");
        }
        
        for (VariableDefinition variableDefinition : chain.getVariables())
        {
            variableConfigurationService.deleteVariableDefinition(variableDefinition);
        }

        for (Object variableObject : variables)
        {
            Map variable = (Map) variableObject;
            String key  = (String) variable.get("key");
            String value  = (String) variable.get("value");
            log.info("PlanTemplates: Adding VariableDefinition with key: '" + key + "'");        
            variableConfigurationService.createPlanVariable(chain, key, value);
        }        
        
    }

}
