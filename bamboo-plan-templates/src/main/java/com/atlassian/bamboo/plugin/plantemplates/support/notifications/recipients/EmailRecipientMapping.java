package com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients;

import java.util.Map;

public class EmailRecipientMapping implements RecipientMapping
{
    @Override
    public String getRecipientKey()
    {
        return "com.atlassian.bamboo.plugin.system.notifications:recipient.email";
    }

    @Override
    public String getRecipientData(@SuppressWarnings("rawtypes") Map notificationMap)
    {
        return (String) notificationMap.get("email");
    }
}
