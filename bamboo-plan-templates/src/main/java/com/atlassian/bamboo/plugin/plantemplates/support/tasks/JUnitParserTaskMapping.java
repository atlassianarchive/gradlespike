package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;

public class JUnitParserTaskMapping extends AbstractTaskMapping
{
    private static final String JUNIT_PARSER_PLUGIN_KEY = "com.atlassian.bamboo.plugins.testresultparser:task.testresultparser.junit";
    private static final String DEFAULT_DIRECTORY_PATTERN = "**/test-reports/*.xml";

    @Override
    public Map<String, String> convert(@SuppressWarnings("rawtypes") Map task)
    {
        ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.<String, String>builder();

        String description = (String) task.get("description");
        String directoryPattern = (String) task.get("resultsDirectory");

        String taskDisabled = (String)task.get("taskDisabled");
        if(Strings.isNullOrEmpty(taskDisabled)){
            mapBuilder.put("taskDisabled", "false");
        }else{
            mapBuilder.put("taskDisabled", taskDisabled);
        }

        if (directoryPattern == null) {
            directoryPattern = DEFAULT_DIRECTORY_PATTERN;
        }

        mapBuilder.put("userDescription", description);
        mapBuilder.put("testResultsDirectory", directoryPattern);
        mapBuilder.put("createTaskKey", JUNIT_PARSER_PLUGIN_KEY);

        return mapBuilder.build();

    }
}
