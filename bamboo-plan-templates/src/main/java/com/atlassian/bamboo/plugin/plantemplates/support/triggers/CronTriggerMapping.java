package com.atlassian.bamboo.plugin.plantemplates.support.triggers;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import java.util.Map;

public class CronTriggerMapping implements TriggerMapping
{

    @Override
    @SuppressWarnings("rawtypes")
    public Map<String, String> convert(Map trigger)
    {
        Builder<String,String> mapBuilder = ImmutableMap.<String, String>builder();

        Boolean onlyBranchesWithChanges = Boolean.parseBoolean((String) trigger.get("onlyBranchesWithChanges"));

        mapBuilder.put("selectedBuildStrategy", "schedule")
                  .put("repository.change.schedule.cronExpression", (String)trigger.get("cronExpression"))
                  .put("custom.rejectBranchBuildWithoutChange.enabled", onlyBranchesWithChanges.toString());

        return mapBuilder.build();
    }

}
