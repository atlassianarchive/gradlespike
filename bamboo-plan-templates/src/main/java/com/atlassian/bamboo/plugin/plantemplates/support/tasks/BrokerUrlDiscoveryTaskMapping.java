package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

public class BrokerUrlDiscoveryTaskMapping extends AbstractTaskMapping
{

    private static final String BROKER_KEY = "com.atlassian.bamboo.buildeng.brokertask.brokerurldiscovery:brokerKey";

    @SuppressWarnings("rawtypes")
    @Override
    public Map<String, String> convert(Map task)
    {
        Builder<String, String> mapBuilder = ImmutableMap.<String, String>builder();
        mapBuilder.put("userDescription", (String) task.get("description"));        

        mapBuilder.put("createTaskKey", BROKER_KEY);        
        return mapBuilder.build();
    }

}
