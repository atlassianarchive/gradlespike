package com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions;


import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.RecipientMapping;

import java.util.Map;

public class ResponsibleRecipientMapping implements RecipientMapping
{
    @Override
    public String getRecipientKey()
    {
        return "com.atlassian.bamboo.brokenbuildtracker:recipient.responsible";
    }

    @Override
    @SuppressWarnings("rawtypes")
    public String getRecipientData(Map notificationMap)
    {
        return "";
    }
}
