package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.AbstractTaskMapping;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import java.util.Map;

public class CopyToUnicornTaskMapping extends AbstractTaskMapping
{
    private static final ImmutableSet<String> TASK_FIELDS = ImmutableSet.of("unicornInstance", "pathSource", "destination", "artifactSource");

    protected String taskKey()
    {
        return TaskMappingKey.BAMBOONICORN_COPY_TO_UNICORN.createTaskMappingKey();
    }

    @Override
    public Map<String, String> convert(@SuppressWarnings("rawtypes") Map task)
    {
        ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.builder();
        final String defaultSourceType = "PATH_SOURCE_TYPE";
        final String defaultCheckBox = "false";

        mapBuilder.put("userDescription", (String) task.get("description"));
        copyFromDslToBambooProperty(task, mapBuilder, "taskDisabled", "taskDisabled", defaultCheckBox);
        mapBuilder.putAll(buildMap(task, TASK_FIELDS));
        copyFromDslToBambooProperty(task, mapBuilder, "sourceType", "sourceType", defaultSourceType);
        mapBuilder.put("createTaskKey", taskKey());

        return mapBuilder.build();
    }
}
