package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.chains.ChainStage;
import com.atlassian.bamboo.deletion.DeletionService;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.branch.ChainBranchManager;
import com.atlassian.bamboo.plan.branch.ChainPropagatingFacade;
import com.atlassian.bamboo.plan.branch.ChainStagePropagatingFacade;

import org.apache.log4j.Logger;

public class StageService
{
    private static final Logger log = Logger.getLogger(StageService.class);

    private JobService jobService;
    private PlanManager planManager;
    private ChainBranchManager chainBranchManager;
    private DeletionService deletionService;

    
    public StageService(JobService jobService, 
                        PlanManager planManager, 
                        ChainBranchManager chainBranchManager,
                        DeletionService deletionService)
    {
        this.jobService = jobService;
        this.planManager = planManager;
        this.chainBranchManager = chainBranchManager;
        this.deletionService = deletionService;
    }

    @SuppressWarnings("rawtypes")
    public Chain performStageChanges(Object stageRepresentation, Chain chain)
    {
        if(stageRepresentation == null){
            return chain;
        }
        
        List stageList = DslTagHelper.getDslTagList(stageRepresentation);
        return performStageListChanges(stageList, chain);
        
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private Chain performStageListChanges(List stageRepresentation, Chain plan)
    {
        if (stageRepresentation == null){
            return plan;
        }
        
        List<ChainStage> existingStages = plan.getStages();
        
        int deletions = existingStages.size() - stageRepresentation.size();
        
        for (int i = 0; i < stageRepresentation.size(); i++)
        {
            // create
            if (existingStages.size() <= i) {                
                Map<String, Object> stage = (Map<String, Object>) stageRepresentation.get(i);
                ChainStage createdStage = createStage(plan, stage); 
                planManager.savePlan(plan);
                plan = planManager.getPlanById(plan.getId(), Chain.class);
                jobService.performJobChanges(stage.get("job"), createdStage);
            }
            // update
            else{
                Map<String, String> stage = (Map<String, String>) stageRepresentation.get(i);
                ChainStage chainStage = updateStage(plan, i, stage);
                jobService.performJobChanges(stage.get("job"), chainStage);
                
            }
            //Read the newly modified chain object back out of the DB
            plan = planManager.getPlanById(plan.getId(), Chain.class);
            
        }
        
        for (int i = 0; i < deletions ; i++){
            deleteLastStage(plan);
        }
        
       
        return plan;
    }

    private ChainStage createStage(Chain plan, Map<String, Object> stage)
    {
        final ChainPropagatingFacade chainAndBranches = new ChainPropagatingFacade(chainBranchManager, plan);
        log.info("PlanTemplates: Creating stage: '" + stage.get("name") + "'");
        ChainStage createdStage = chainAndBranches.addNewStage((String) stage.get("name"), (String) stage.get("description"), Boolean.parseBoolean((String) stage.get("manual")));
        return createdStage;
    }

    private ChainStage updateStage(Chain plan, int index, Map<String, String> stage)
    {
        log.info("PlanTemplates: Updating stage: '" + stage.get("name") + "'");
        
        // we need to load the object again due to Hibernate "non-unique object" issue
        Chain chain = planManager.getPlanByKey(plan.getPlanKey(), Chain.class );
        ChainStage chainStage = chain.getStages().get(index);
        
        ChainStagePropagatingFacade stages = new ChainStagePropagatingFacade(chainBranchManager, chainStage);

        stages.setName(stage.get("name"));
        stages.setDescription(stage.get("description"));
        stages.setManual(Boolean.parseBoolean(stage.get("manual")));

        stages.savePlan(planManager);
        return chainStage;
    }

    private void deleteLastStage(Chain plan)
    {
        Chain chain = planManager.getPlanByKey(plan.getPlanKey(), Chain.class );
        List<ChainStage> stages = chain.getStages();
        ChainStage chainStage = stages.get(stages.size() - 1);
        log.info("PlanTemplates: Deleting stage: '" + chainStage.getName() + "'" );
        deletionService.deleteStage(chainStage);
    }

}
