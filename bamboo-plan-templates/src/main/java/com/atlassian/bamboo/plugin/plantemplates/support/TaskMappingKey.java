package com.atlassian.bamboo.plugin.plantemplates.support;

public enum TaskMappingKey
{
    BAMBOONICORN_PROVISION_UNICORN("com.atlassian.bamboo.plugins.bamboonicorn", "provisionunicorn"),
    BAMBOONICORN_STOP_UNICORN("com.atlassian.bamboo.plugins.bamboonicorn", "stopunicorn"),
    BAMBOONICORN_INSTALL_ONDEMAND("com.atlassian.bamboo.plugins.bamboonicorn", "installOnDemand"),
    BAMBOONICORN_PROMOTE_PLUGIN_VERSION("com.atlassian.bamboo.plugins.bamboonicorn", "promoteManifesto"),
    BAMBOONICORN_PROMOTE_PRODUCT_VERSION("com.atlassian.bamboo.plugins.bamboonicorn", "promoteProductVersion"),
    BAMBOONICORN_PROMOTE_MULTIPLE_VERSION("com.atlassian.bamboo.plugins.bamboonicorn", "promoteManifestoMultiple", "promoteManifestoArtifacts"),
    BAMBOONICORN_REMOVE_MULTIPLE_VERSION("com.atlassian.bamboo.plugins.bamboonicorn", "removeManifestoMultiple", "removeManifestoArtifacts"),
    BAMBOONICORN_START_UNICORN("com.atlassian.bamboo.plugins.bamboonicorn", "startunicorn"),
    BAMBOONICORN_EXECUTE_ON_UNICORN("com.atlassian.bamboo.plugins.bamboonicorn", "execOnUnicorn"),
    BAMBOONICORN_UPDATE_LICENSE("com.atlassian.bamboo.plugins.bamboonicorn", "updatelicense"),
    BAMBOONICORN_SETUP_UNICORN("com.atlassian.bamboo.plugins.bamboonicorn", "setupunicorn"),
    BAMBOONICORN_COPY_TO_UNICORN("com.atlassian.bamboo.plugins.bamboonicorn", "copyToUnicorn"),
    BAMBOONICORN_RAISE_DEPLOYMENT_TICKET("com.atlassian.bamboo.plugins.bamboonicorn", "raiseDeploymentTicket");

    private String pluginKey;
    private String bambooTaskKey;
    private String planTemplateTaskKey;

    private TaskMappingKey(String pluginKey, String bambooTaskKey)
    {
        this(pluginKey, bambooTaskKey, bambooTaskKey);
    }

    private TaskMappingKey(String pluginKey, String bambooTaskKey, String planTemplateTaskKey)
    {
        this.pluginKey = pluginKey;
        this.bambooTaskKey = bambooTaskKey;
        this.planTemplateTaskKey = planTemplateTaskKey;
    }

    public String createTaskMappingKey()
    {
        return this.pluginKey + ":" + this.bambooTaskKey;
    }

    public String getPluginKey()
    {
        return this.pluginKey;
    }

    public String getTaskKey()
    {
        return this.planTemplateTaskKey;
    }
}
