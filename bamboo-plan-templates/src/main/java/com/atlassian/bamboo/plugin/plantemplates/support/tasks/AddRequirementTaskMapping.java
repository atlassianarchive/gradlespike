package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

public class AddRequirementTaskMapping implements TaskMapping
{

    private static final String REQUIREMENTTASK_KEY = "com.atlassian.bamboo.plugin.requirementtask:task.requirements";

    @SuppressWarnings("rawtypes")
    public Map<String, String> convert(Map task)
    {
        Builder<String, String> mapBuilder = ImmutableMap.<String, String>builder();
        
        String taskDisabled = (String) task.get("taskDisabled");
        if(Strings.isNullOrEmpty(taskDisabled)){
            mapBuilder.put("taskDisabled", "false");
        }else{
            mapBuilder.put("taskDisabled", taskDisabled);
        }

        mapBuilder.put("userDescription", (String) task.get("description"));        
        mapBuilder.put("createTaskKey", REQUIREMENTTASK_KEY);

        Map requirementMap = null;
        try {
            requirementMap = (Map) task.get("requirement");
        } catch( ClassCastException e){
            throw new IllegalArgumentException("PlanTemplates: Error on task type AddRequirement " + task.get("description") + " can only have one requirement" );
        }
        String key = (String) requirementMap.get("key");
        String value = (String) requirementMap.get("value");
        String condition = (String) requirementMap.get("condition");

        String regexMatchValue = "";
        if(condition.equals("exists")){
            condition = "exist";  
            value = "";
        }
        else if(condition.equals("equals")){
            condition = "equal";  
            
        }
        else if(condition.equals("matches")){
            regexMatchValue = value;
            condition = "match";
            value = "";
        }
        
        mapBuilder.put("requirementKey", key);
        mapBuilder.put("requirementMatchType", condition);
        mapBuilder.put("requirementMatchValue", value);
        mapBuilder.put("regexMatchValue", regexMatchValue);
        
        return mapBuilder.build();
    }

}
