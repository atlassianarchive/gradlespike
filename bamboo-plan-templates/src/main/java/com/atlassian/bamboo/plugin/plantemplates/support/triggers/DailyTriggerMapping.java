package com.atlassian.bamboo.plugin.plantemplates.support.triggers;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

public class DailyTriggerMapping implements TriggerMapping
{

    @SuppressWarnings("rawtypes")
    @Override
    public Map<String, String> convert(Map trigger)
    {
        Builder<String, String> mapBuilder = ImmutableMap.<String, String> builder();

        mapBuilder.put("selectedBuildStrategy", "daily")
                  .put("repository.change.daily.buildTime", (String) trigger.get("time"));
        
        return mapBuilder.build();
    }

}
