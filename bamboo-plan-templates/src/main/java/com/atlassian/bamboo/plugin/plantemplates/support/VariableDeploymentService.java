package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.variable.VariableConfigurationService;
import com.atlassian.bamboo.variable.VariableDefinition;
import com.atlassian.bamboo.variable.VariableDefinitionManager;

import org.apache.log4j.Logger;

public class VariableDeploymentService
{
    
    private static final Logger log = Logger.getLogger(VariableDeploymentService.class);
    
    private VariableConfigurationService variableConfigurationService;
    private VariableDefinitionManager variableDefinitionManager;
    
    public VariableDeploymentService(VariableConfigurationService variableConfigurationService, VariableDefinitionManager variableDefinitionManager)
    {
        this.variableConfigurationService = variableConfigurationService;
        this.variableDefinitionManager = variableDefinitionManager;
    }

    @SuppressWarnings("rawtypes")
    public void performVariableChanges(Object variableRepresentation, Environment environment)
    {
        deleteExistingVariables(environment);
        
        List variablesRepresentation = DslTagHelper.getDslTagList(variableRepresentation);
        
        for (Object variable : variablesRepresentation)
        {
            createVariable((Map) variable, environment);
        }
        
    }

    private void deleteExistingVariables(Environment environment)
    {
        List<VariableDefinition> variables = variableDefinitionManager.getDeploymentEnvironmentVariables(environment.getId());
        
        if(variables == null){
            return;
        }
        
        for (VariableDefinition variableDefinition : variables)
        {
            variableConfigurationService.deleteVariableDefinition(variableDefinition);
        }
        
    }

    @SuppressWarnings("rawtypes")
    private void createVariable(Map variable, Environment environment)
    {
        String key  = (String) variable.get("key");
        String value  = (String) variable.get("value");
        log.info("PlanTemplates: Adding VariableDefinition for deployment environment with key: '" + key + "'");        
        variableConfigurationService.createVariableForEnvironment(environment.getId(), key, value);        
    }

}
