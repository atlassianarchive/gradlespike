package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.Map;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanManager;

import org.apache.log4j.Logger;

public class MiscellaneousJobService
{
    private static final Logger log = Logger.getLogger(MiscellaneousJobService.class);
    
    private BuildDefinitionManager buildDefinitionManager;
    private PlanManager planManager;

    public MiscellaneousJobService(BuildDefinitionManager buildDefinitionManager, PlanManager planManager)
    {
        this.buildDefinitionManager = buildDefinitionManager;
        this.planManager = planManager;
    }

    @SuppressWarnings("unchecked")
    public Job performChanges(Object miscellaneousConfig, Job job)
    {
        log.info("PlanTemplates: Setting miscellaneous configuration for job: '" + job.getPlanKey().toString() + "'");

        Plan chain = planManager.getPlanByKey(job.getPlanKey());
        final BuildDefinition buildDefinition = chain.getBuildDefinition();
        final Map<String, String> customConfiguration = buildDefinition.getCustomConfiguration();

        setDefaultConfig(buildDefinition, customConfiguration);
        
        if ( ! ( miscellaneousConfig instanceof Map ) ) {
            buildDefinitionManager.savePlanAndDefinition(chain, buildDefinition);
            log.info("PlanTemplates: Updated miscellaneous configuration to defaults value, for job: '" + job.getPlanKey().toString() + "'");
            return job;
        }
        
        final Map<String,Object> config = (Map<String, Object>) miscellaneousConfig;

        if ( config.containsKey("cleanupWorkdirAfterBuild") )
        {
            buildDefinition.setCleanWorkingDirectory(Boolean.parseBoolean((String) config.get( "cleanupWorkdirAfterBuild" ) ));
        }
        if ( config.containsKey( "patternMatchLabelling" ) )
        {
            final Object patternMatchLabellingData = config.get("patternMatchLabelling");

            if ( ! ( patternMatchLabellingData instanceof Map ) )
            {
                throw new IllegalArgumentException ( String.format( "Must be only one patternMatchLabelling element specified for job: %s", job.getPlanKey().toString() ) );
            }
            Map<String,String> patternMatchingMap = (Map<String,String>) patternMatchLabellingData;

            String regex = patternMatchingMap.get("regex");
            customConfiguration.put("custom.auto.regex", regex);

            String labels = patternMatchingMap.get("labels");
            customConfiguration.put("custom.auto.label", labels);
           
        }
        buildDefinitionManager.savePlanAndDefinition(chain, buildDefinition);
        log.info("PlanTemplates: Updated miscellaneous configuration for job: '" + job.getPlanKey().toString() + "'");
        return job;
    }

    private void setDefaultConfig(final BuildDefinition buildDefinition, final Map<String, String> customConfiguration)
    {
        buildDefinition.setCleanWorkingDirectory(false);
        customConfiguration.remove("custom.auto.regex");
        customConfiguration.remove("custom.auto.label");
    }

}
