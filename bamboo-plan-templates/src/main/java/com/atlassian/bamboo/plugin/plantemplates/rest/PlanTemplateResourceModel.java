package com.atlassian.bamboo.plugin.plantemplates.rest;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "planTemplate")
@XmlAccessorType(XmlAccessType.FIELD)
public class PlanTemplateResourceModel
{

    @XmlElement(name = "dsl")
    private String dsl;

    private String shortcuts;

    public PlanTemplateResourceModel()
    {
    }

    public PlanTemplateResourceModel(String dsl, String shortcuts)
    {
        this.dsl = dsl;
        this.shortcuts = shortcuts;
    }

    public void setDsl(String dsl)
    {
        this.dsl = dsl;
    }

    public String getDsl()
    {
        return dsl;
    }

    public String getShortcuts()
    {
        return shortcuts;
    }

    public void setShortcuts(String shortcuts)
    {
        this.shortcuts = shortcuts;
    }
}