package com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients;

import java.util.Map;

public class UserRecipientMapping implements RecipientMapping
{
    @Override
    public String getRecipientKey()
    {
        return "com.atlassian.bamboo.plugin.system.notifications:recipient.user";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public String getRecipientData(Map notificationMap)
    {
        return (String) notificationMap.get("user");
    }
}
