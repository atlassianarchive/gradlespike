package com.atlassian.bamboo.plugin.plantemplates.support.triggers;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

public class AfterSuccessfulPlanTriggerMapping implements TriggerMapping
{
    @Override
    @SuppressWarnings("rawtypes")
    public Map<String, String> convert(Map trigger)
    {
        Builder<String, String> mapBuilder = ImmutableMap.<String, String> builder();

        mapBuilder
            .put("selectedBuildStrategy", "afterSuccessfulPlan")
            .put("triggeringPlan", (String) trigger.get("planKey"));

        return mapBuilder.build();
    }

}
