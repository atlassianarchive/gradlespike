package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

public class DeployBambooPluginTaskMapping extends AbstractDeployPluginTaskMapping
{

    public static final String DEPLOY_BAMBOO_PLUGIN_TASK_PLUGIN_KEY = "com.atlassian.bamboo.plugins.deploy.continuous-plugin-deployment:bamboodeploy";

    
    protected String taskKey(){
        return DEPLOY_BAMBOO_PLUGIN_TASK_PLUGIN_KEY;
    }

}
