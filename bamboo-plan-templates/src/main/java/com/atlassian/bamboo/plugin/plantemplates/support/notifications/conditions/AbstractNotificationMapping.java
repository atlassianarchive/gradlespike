package com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions;


import com.atlassian.bamboo.notification.NotificationManager;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.RecipientMapping;

public abstract class AbstractNotificationMapping implements NotificationMapping
{
    protected RecipientMapping recipientMapping;
    protected NotificationManager notificationManager;

    public AbstractNotificationMapping(RecipientMapping recipientMapping, NotificationManager notificationManager) {
        this.recipientMapping = recipientMapping;
        this.notificationManager = notificationManager;
    }

    public RecipientMapping getRecipientMapping() {
        return recipientMapping;
    }
}
