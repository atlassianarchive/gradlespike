package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.Map;

import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.project.ProjectManager;

public class ProjectService
{
    
    private ProjectManager projectManager;

    public ProjectService(ProjectManager projectManager)
    {
        this.projectManager = projectManager;
    }

    public Project createOrUpdate(Map<String, String> projectRepresentation)
    {
        String key = projectRepresentation.get("key");
        String name = projectRepresentation.get("name");
        String description = projectRepresentation.get("description");
        Project project;
        if (projectManager.isExistingProjectKey(key))
        {
            project = projectManager.getProjectByKey(key);
            project.setName(name);
            project.setDescription(description);
        }
        else
        {
            project = projectManager.createProject(key, name, description);
        }

        projectManager.saveProject(project);

        return project;
    }

}
