package com.atlassian.bamboo.plugin.plantemplates.task;

public class HttpPostException extends RuntimeException
{
    private static final long serialVersionUID = -5944003303734170581L;

    public HttpPostException()
    {
        super();
    }

    public HttpPostException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public HttpPostException(String message)
    {
        super(message);
    }

    public HttpPostException(Throwable cause)
    {
        super(cause);
    }
    
    

}
