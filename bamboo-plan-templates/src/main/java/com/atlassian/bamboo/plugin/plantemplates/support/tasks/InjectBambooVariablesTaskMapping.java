package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;

public class InjectBambooVariablesTaskMapping extends AbstractTaskMapping {

    @SuppressWarnings("rawtypes")
    public Map<String, String> convert(Map task)
    {
        ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.<String, String>builder();

        mapBuilder.put("userDescription", (String) task.get("description"));

        String taskDisabled = (String)task.get("taskDisabled");
        if(Strings.isNullOrEmpty(taskDisabled)){
            mapBuilder.put("taskDisabled", "false");
        }else{
            mapBuilder.put("taskDisabled", taskDisabled);
        }

        copyFromDslToBambooProperty(task, mapBuilder, "filePath");
        mapBuilder.put("createTaskKey", "com.atlassian.bamboo.plugins.bamboo-variable-inject-plugin:inject");

        return mapBuilder.build();

    }

}