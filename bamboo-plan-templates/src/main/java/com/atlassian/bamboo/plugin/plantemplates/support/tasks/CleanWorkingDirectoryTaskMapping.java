package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

public class CleanWorkingDirectoryTaskMapping implements TaskMapping
{

    @SuppressWarnings("rawtypes")
    public Map<String, String> convert( Map task)
    {
        Builder<String, String> mapBuilder = ImmutableMap.<String, String>builder();
        
        String taskDisabled = (String)task.get("taskDisabled");
        if(Strings.isNullOrEmpty(taskDisabled)){
            mapBuilder.put("taskDisabled", "false");
        }else{
            mapBuilder.put("taskDisabled", taskDisabled);
        }

        mapBuilder.put("userDescription", (String) task.get("description"));        
        mapBuilder.put("createTaskKey", "com.atlassian.bamboo.plugins.bamboo-artifact-downloader-plugin:cleanWorkingDirectoryTask");

        return mapBuilder.build();
    }

}
