package com.atlassian.bamboo.plugin.plantemplates.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plugin.plantemplates.AutoVariablesService;
import com.atlassian.bamboo.plugin.plantemplates.PlanTemplateService;
import com.atlassian.bamboo.plugin.plantemplates.PlanTemplateValidationRules;
import com.atlassian.bamboo.plugin.plantemplates.ValidationRulesPrinter;
import com.atlassian.bamboo.plugin.plantemplates.support.TemplateValidationException;
import com.atlassian.bamboo.variable.VariableDefinition;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;



@Path("/")
public class PlanTemplateResource {

    private PlanTemplateService planTemplateService;
    private final PlanManager planManager;

    public PlanTemplateResource(PlanTemplateService planTemplateService, PlanManager planManager)
    {
        this.planTemplateService = planTemplateService;
        this.planManager = planManager;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getMessage()
    {
       return Response.ok("keep calm and run the build").build();
    }
    
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public Response runDsl(String dsl){
        
        try {
            Map<String, Object> runPlanTemplate = planTemplateService.runPlanTemplate(dsl);
            return Response.ok(runPlanTemplate).build();
        }
        catch(TemplateValidationException e) {
            return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
        }catch(Exception e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(Throwables.getStackTraceAsString(e)).build();
        }
        
    }

    @Path("/json")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response runDslWithShortcuts(String jsonDoc)
    {
        ObjectMapper mapper = new ObjectMapper();
        try
        {
            @SuppressWarnings("unchecked")
            Map<String, String> map = mapper.readValue(jsonDoc, Map.class);
            String dsl = map.get("dsl");
            String shortcuts = map.get("shortcuts");
            Map<String, Object> runPlanTemplate = planTemplateService.runPlanTemplate(dsl, shortcuts);
            return Response.ok(runPlanTemplate).build();
        }
        catch(TemplateValidationException e) 
        {
            return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
        }
        catch(Exception e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(Throwables.getStackTraceAsString(e)).build();
        }
    }
    
    
    @Path("/expandDsl")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response expandDsl(String jsonDoc)
    {
        ObjectMapper mapper = new ObjectMapper();
        try
        {
            @SuppressWarnings("unchecked")
            Map<String, String> map = mapper.readValue(jsonDoc, Map.class);
            String dsl = map.get("dsl");
            String shortcuts = map.get("shortcuts");
            String expandDsl = planTemplateService.expandDsl(dsl, shortcuts);
            return Response.ok(expandDsl.toString()).build();
        }
        catch(TemplateValidationException e) 
        {
            return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
        }
        catch(Exception e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(Throwables.getStackTraceAsString(e)).build();
        }
    }


    @Path("/returnError500")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public Response returnError500()
    {
        return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Here you receive a pretty big stacktrace!\n").build();
    }

    @Path("/plan-template-variables/{planKey}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlanTemplateAdditionalVariables(@PathParam("planKey") String planKey) {
        PlanKey key;
        try {
            key = PlanKeys.getPlanKey(planKey);
            if(PlanKeys.isJobKey(key)){
                key = PlanKeys.getChainKeyFromJobKey(key);
            }
        } catch (IllegalArgumentException e){
            return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
        Plan plan = planManager.getPlanByKey(key);
        if (plan == null)
            return Response.status(Status.NOT_FOUND).build();

        Map<String, String> additionalVariables = new HashMap<String, String>();
        for (VariableDefinition variable : plan.getVariables()) {
            if (variable.getKey().startsWith(AutoVariablesService.PLANTEMPLATE_VARIABLE_PREFIX)) {
                additionalVariables.put(variable.getKey(), variable.getValue());
            }
        }

        return Response.status(Status.OK).entity(additionalVariables).build();
    }

    @Path("/validation-rules")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response validationRules(){
        return Response.ok(new PlanTemplateValidationRules().getRules()).build();
    }

    @Path("/pretty-printing-validation-rules")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response prettyPrintingValidationRules() {
        List<Map> rules = new PlanTemplateValidationRules().getRules();
        return Response.ok(new ValidationRulesPrinter().retrievePrettyPrintValidationRules(rules)).build();
    }
}
