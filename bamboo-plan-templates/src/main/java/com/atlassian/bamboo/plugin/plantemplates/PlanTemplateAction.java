package com.atlassian.bamboo.plugin.plantemplates;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.configuration.GlobalAdminAction;

import com.google.common.base.Throwables;

public class PlanTemplateAction extends GlobalAdminAction
{

    private static final long serialVersionUID = 1625963615731029419L;

    private PlanTemplateService planTemplateService;
    private String dsl;
    private String shortcuts;



    private String outputLog;

    @Override
    public String doDefault() {
        return SUCCESS;
    }

    @Override
    public void validate() {

    }

    public String doSave() {
        try{
            Map<String, Object> runPlanTemplate = planTemplateService.runPlanTemplate(dsl, shortcuts);
            addActionMessage("Success :D");
            
            List warnings = (List) runPlanTemplate.get("warnings");
            if (warnings !=null && !warnings.isEmpty()){
                for (Object warn : warnings)
                {
                    addActionWarning(warn.toString());
                }
            }
            
            return SUCCESS;
        } catch (Exception e){
            outputLog = Throwables.getStackTraceAsString(e);
            addActionError(e.getMessage());
            return ERROR;
        }

    }



    public void setPlanTemplateService(PlanTemplateService planTemplateService)
    {
        this.planTemplateService = planTemplateService;
    }

    public String getDsl()
    {
        return dsl;
    }

    public void setDsl(String dsl)
    {
        this.dsl = dsl;
    }

    public String getShortcuts()
    {
        return shortcuts;
    }

    public void setShortcuts(String shortcuts)
    {
        this.shortcuts = shortcuts;
    }

    public String getOutputLog() {
        return outputLog;
    }

    public void getOutputLog(String output) {
        this.outputLog = output;
    }


}
