package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import java.util.Map;

import com.atlassian.bamboo.plugin.plantemplates.support.tasks.AbstractTaskMapping;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_RAISE_DEPLOYMENT_TICKET;

public class RaiseDeploymentTicketTaskMapping extends AbstractTaskMapping
{
    private static final ImmutableSet<String> TASK_FIELDS = ImmutableSet.of("unicornInstance", "artifactName",
            "artifactVersion", "artifactKey", "pluginKey", "groupId", "deploymentEnvironment", "sdogUsername", "sdogPassword");

    protected String taskKey()
    {
        return BAMBOONICORN_RAISE_DEPLOYMENT_TICKET.createTaskMappingKey();
    }

    @Override
    public Map<String, String> convert(@SuppressWarnings("rawtypes") Map task)
    {
        ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.builder();
        final String defaultCheckBox = "false";

        mapBuilder.put("userDescription", (String) task.get("description"));
        copyFromDslToBambooProperty(task, mapBuilder, "taskDisabled", "taskDisabled", defaultCheckBox);
        mapBuilder.putAll(buildMap(task, TASK_FIELDS));
        mapBuilder.put("createTaskKey", taskKey());

        return mapBuilder.build();
    }
}
