package com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients;

import java.util.Map;

public class HttpNotificationsRecipientMapping  implements RecipientMapping
{

    @Override
    public String getRecipientKey()
    {
        return "com.atlassian.bamboo.plugins.bamboo-http-notifications:recipient.http.notification";
    }

    @Override
    @SuppressWarnings("rawtypes")
    public String getRecipientData(Map notificationMap)
    {
        String baseUrl = (String) notificationMap.get("baseUrl");

        return baseUrl;
    }

}
