package com.atlassian.bamboo.plugin.plantemplates;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.build.PlanCreationDeniedException;
import com.atlassian.bamboo.caching.DashboardCachingManager;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.deletion.DeletionService;
import com.atlassian.bamboo.plugin.plantemplates.support.TemplateValidationException;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.apache.log4j.Logger;


public class PlanTemplateServiceImpl implements PlanTemplateService
{
    private static final Logger log = Logger.getLogger(PlanTemplateService.class);

    private DashboardCachingManager dashboardCachingManager;
    private PerformPlanChanges performPlanChanges;
    private PerformDeploymentChanges performDeploymentChanges;
    private DeletionService deletionService;
    
    private SetupDslEvaluator setupDslEvaluator = new SetupDslEvaluator();


    public PlanTemplateServiceImpl(PerformPlanChanges performPlanChanges,
                                   PerformDeploymentChanges performDeploymentChanges,
                                   DashboardCachingManager dashboardCachingManager,
                                   DeletionService deletionService)
    {
        this.performPlanChanges = performPlanChanges;
        this.performDeploymentChanges = performDeploymentChanges;
        this.dashboardCachingManager = dashboardCachingManager;
        this.deletionService = deletionService;
    }
    
    public void setSetupDslEvaluator(SetupDslEvaluator setupDslEvaluator)
    {
        this.setupDslEvaluator = setupDslEvaluator;
    }

    @Override
    public Map<String, Object> runPlanTemplate(String template)
    {
        return runPlanTemplate(template, "");
    }


    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public Map<String, Object> runPlanTemplate(String template, String shortcuts)
    {
        log.info("PlanTemplates: Start of the evaluation of a DSL");
        DslEvaluator dslEvaluator = setupDslEvaluator.setup(shortcuts);
        Map result = dslEvaluator.run(template);
        
        List<String> errors = (List) result.get("errors");
        if(errors != null && !errors.isEmpty()){
            throw new TemplateValidationException("There were some errors validating the template", errors);
        }
        
        List<Chain> resultingPlans = null;
        try
        {
            resultingPlans = performPlanChanges.performChanges((Map) result.get("dslMap"));
            log.info("PlanTemplates: Finished performChanges on the Plans, waiting for reset the cache...");

            //FIXME find a better place for deployments
            performDeploymentChanges.performChanges((Map) result.get("dslMap"));
            
            log.info("PlanTemplates: Forcing to run deletion service");
            deletionService.executeDelayedDeletion();
            log.info("PlanTemplates: Deletion service to invoked");
            
            for (Chain chain : resultingPlans)
            {
                dashboardCachingManager.updatePlanCache(chain.getPlanKey());
            }

            log.info("PlanTemplates: Cache is reset, performChanges SUCCESS");
            
            Object warnings = result.get("warnings");
            return ImmutableMap.of("warnings", warnings == null? Lists.newArrayList() : warnings);
        }
        catch (PlanCreationDeniedException e)
        {
            throw new RuntimeException("Invalid permission to create or update the plans", e);
        }
    }
    
    public String expandDsl(String template, String shortcuts){
        DslEvaluator dslEvaluator = setupDslEvaluator.setup(shortcuts);
        String expandDsl = dslEvaluator.expandDsl(template);
        List<String> errors = (List<String>) dslEvaluator.validate(expandDsl).get("errors");
        if(!errors.isEmpty()){
            throw new TemplateValidationException("There were some errors validating the template", errors, expandDsl);
        }
        return expandDsl;    
    }

   
  
}