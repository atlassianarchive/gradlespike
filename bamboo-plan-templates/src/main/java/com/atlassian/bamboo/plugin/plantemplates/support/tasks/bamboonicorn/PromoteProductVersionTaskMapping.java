package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.google.common.collect.ImmutableSet;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_PROMOTE_PRODUCT_VERSION;

public class PromoteProductVersionTaskMapping extends AbstractVersionTaskMapping
{
    private static final ImmutableSet<String> TASK_FIELDS = ImmutableSet.of("productName", "productVersion", "username", "passwordVariable");

    protected String taskKey()
    {
        return BAMBOONICORN_PROMOTE_PRODUCT_VERSION.createTaskMappingKey();
    }

    @Override
    protected ImmutableSet<String> getTaskFields()
    {
        return TASK_FIELDS;
    }
}