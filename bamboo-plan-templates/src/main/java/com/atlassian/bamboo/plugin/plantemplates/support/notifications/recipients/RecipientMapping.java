package com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients;

import java.util.Map;

public interface RecipientMapping
{
    public String getRecipientKey();

    @SuppressWarnings("rawtypes")
    public String getRecipientData(Map notificationMap);
}
