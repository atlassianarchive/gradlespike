package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinition;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionImpl;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionManager;
import com.atlassian.bamboo.plan.artifact.ArtifactSubscription;
import com.atlassian.bamboo.plan.artifact.ArtifactSubscriptionImpl;
import com.atlassian.bamboo.plan.artifact.ArtifactSubscriptionManager;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.apache.log4j.Logger;

public class ArtifactService
{
    private static final Logger log = Logger.getLogger(ArtifactService.class);
    
    private ArtifactDefinitionManager artifactDefinitionManager;
    private ArtifactSubscriptionManager artifactSubscriptionManager;

    public ArtifactService(ArtifactDefinitionManager artifactDefinitionManager, ArtifactSubscriptionManager artifactSubscriptionManager)
    {
        this.artifactDefinitionManager = artifactDefinitionManager;
        this.artifactSubscriptionManager = artifactSubscriptionManager;
    }

    @SuppressWarnings("rawtypes")
    public Job performDefinitionChanges(Object artifactRepresentation, Job job)
    {
        List artifactDefinitions;
        if (artifactRepresentation instanceof List) {
            artifactDefinitions = (List) artifactRepresentation;
        }
        else if (artifactRepresentation instanceof Map) {
            artifactDefinitions = Lists.newArrayList(artifactRepresentation);
        }
        else if (artifactRepresentation == null) {
            artifactDefinitions = Lists.newArrayList();
        }
        else {
            throw new IllegalArgumentException("Artifact definitions were neither a map nor a list");
        }


        List<ArtifactDefinition> toSave = Lists.newArrayList();
        List<ArtifactDefinition> existingDefinitions = job.getArtifactDefinitions();

        Set<ArtifactDefinition> toDelete = Sets.newHashSet(existingDefinitions);

        for (Object artifactDefinitionObject : artifactDefinitions)
        {
            Map artifactDefinition = (Map) artifactDefinitionObject;
            ArtifactDefinition newDefinition = createNewArtifactDefinition(artifactDefinition, job);
            ArtifactDefinition existingDefinition = locateDefinition(newDefinition.getName(), existingDefinitions);
            if (existingDefinition == null) {
                log.info("PlanTemplates: Creating artifact definition: '" + newDefinition.getName() + "'");        
                toSave.add(newDefinition);
            }
            else {
                log.info("PlanTemplates: Updating artifact definition: '" + newDefinition.getName() + "'");
                copyDefinitionParameters(newDefinition, existingDefinition);
                toSave.add(existingDefinition);
                //Don't delete this one since we just updated it
                toDelete.remove(existingDefinition);
            }
        }

        for (ArtifactDefinition definition : toDelete) {
            log.info("PlanTemplates: Removing artifact definition list of : '" + toSave.size() + "' definitions");
            artifactDefinitionManager.removeArtifactDefinition(definition);
        }

        if (!toSave.isEmpty()) {
            log.info("PlanTemplates: Updating artifact definition list of : '" + toSave.size() + "' definitions");
            artifactDefinitionManager.saveArtifactDefinitions(toSave);
        }

        return job;
    }

    private ArtifactDefinition locateDefinition(String name, List<ArtifactDefinition> definitions)
    {
        ArtifactDefinition toReturn = null;
        for (ArtifactDefinition definition : definitions) {
            if (definition.getName().equals(name)) {
                toReturn = definition;
            }
        }
        return toReturn;
    }

    private void copyDefinitionParameters(ArtifactDefinition from, ArtifactDefinition to) {
        to.setLocation(from.getLocation());
        to.setCopyPattern(from.getCopyPattern());
        to.setSharedArtifact(from.isSharedArtifact());
        to.setProducerJob(from.getProducerJob());
    }

    @SuppressWarnings("rawtypes")
    public ArtifactDefinition createNewArtifactDefinition(Map definitionRepresentation, Job job)
    {
        String name = (String) definitionRepresentation.get("name");
        String location = (String) definitionRepresentation.get("location");
        String pattern = (String) definitionRepresentation.get("pattern");
        Boolean shared = Boolean.parseBoolean((String) definitionRepresentation.get("shared"));

        ArtifactDefinition newDefinition = new ArtifactDefinitionImpl(name, location, pattern);
        newDefinition.setProducerJob(job);
        newDefinition.setSharedArtifact(shared);

        return newDefinition;
    }

    @SuppressWarnings("rawtypes")
    public Job performSubscriptionChanges(Object artifactRepresentation, Job job)
    {

        List artifactSubscriptions = null;
        if(artifactRepresentation instanceof List) {
            artifactSubscriptions = (List) artifactRepresentation;
        }
        else if (artifactRepresentation instanceof Map) {
            artifactSubscriptions = Lists.newArrayList(artifactRepresentation);
        }
        else if (artifactRepresentation == null) {
            artifactSubscriptions = Lists.newArrayList();
        }
        else {
            throw new IllegalArgumentException("Artifact subscriptions for job " + job.getName() + " were neither a list or a map");
        }


        List<ArtifactSubscription> toSave = Lists.newArrayList();
        List<ArtifactSubscription> existingSubscriptions = job.getArtifactSubscriptions();
        Set<ArtifactSubscription> toDelete = Sets.newHashSet(existingSubscriptions);

        for (Object artifactSubscriptionObject : artifactSubscriptions)
        {
            Map artifactSubscriptionMap = (Map) artifactSubscriptionObject;
            String name = (String) artifactSubscriptionMap.get("name");
            String destination = (String) artifactSubscriptionMap.get("destination");

            ArtifactSubscription existing = locateSubscription(name, existingSubscriptions);
            Chain parentChain = job.getParent();
            ArtifactDefinition producer = artifactDefinitionManager.findArtifactDefinitionByChain(parentChain, name);

            if (producer == null) {
                throw new IllegalArgumentException("Couldn't find artifact '" + name + "' to subscribe to");
            }

            ArtifactSubscription newSubscription = new ArtifactSubscriptionImpl(producer, job, destination);

            if (existing == null) {
                log.info("PlanTemplates: Creating artifact subscription: '" + newSubscription.getName()  + "'");        
                toSave.add(newSubscription);
            }
            else {
                log.info("PlanTemplates: Updating artifact subscription: '" + newSubscription.getName()  + "'");        
                copySubscriptionParameters(newSubscription, existing);
                toSave.add(existing);
                toDelete.remove(existing);
            }
        }

        if (!toSave.isEmpty()) {
            log.info("PlanTemplates: Updating artifact subscription list of : '" + toSave.size() + "' definitions");
            artifactSubscriptionManager.saveArtifactSubscriptions(toSave);
        }

        if (!toDelete.isEmpty()) {
            log.info("PlanTemplates: Removing artifact subscription list of : '" + toSave.size() + "' definitions");
            artifactSubscriptionManager.removeArtifactSubscriptions(toDelete);
        }


        return job;
    }

    private ArtifactSubscription locateSubscription(String name, List<ArtifactSubscription> subscriptions)
    {
        ArtifactSubscription toReturn = null;
        for (ArtifactSubscription definition : subscriptions) {
            if (definition.getArtifactDefinition().getName().equals(name)) {
                toReturn = definition;
            }
        }
        return toReturn;
    }

    private void copySubscriptionParameters(ArtifactSubscription from, ArtifactSubscription to) {
        to.setArtifactDefinition(from.getArtifactDefinition());
        to.setDestinationDirectory(from.getDestinationDirectory());
    }
}
