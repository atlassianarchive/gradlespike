package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.atlassian.bamboo.plugin.plantemplates.support.tasks.AbstractTaskMapping;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import java.util.Map;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_PROVISION_UNICORN;

public class ProvisionUnicornTaskMapping extends AbstractTaskMapping
{
    private static final ImmutableSet<String> TASK_FIELDS = ImmutableSet.of("password", "gappsDomain", "unicornInstance", "version");

    protected String taskKey()
    {
        return BAMBOONICORN_PROVISION_UNICORN.createTaskMappingKey();
    }

    @Override
    public Map<String, String> convert(@SuppressWarnings("rawtypes") Map task)
    {
        ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.builder();
        final String defaultAdminUsername = "admin";
        final String defaultAdminEmail = "admin@atlassian.com";
        final String defaultCheckBox = "false";

        mapBuilder.put("userDescription", (String) task.get("description"));

        mapBuilder.putAll(buildMap(task, TASK_FIELDS));
        copyFromDslToBambooProperty(task, mapBuilder, "taskDisabled", "taskDisabled", defaultCheckBox);
        copyFromDslToBambooProperty(task, mapBuilder, "email", "email", defaultAdminEmail);
        copyFromDslToBambooProperty(task, mapBuilder, "username", "username", defaultAdminUsername);

        mapBuilder.put("createTaskKey", taskKey());

        return mapBuilder.build();
    }
}
