package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.google.common.collect.ImmutableSet;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_REMOVE_MULTIPLE_VERSION;

public class RemoveArtifactsVersionTaskMapping extends AbstractVersionTaskMapping
{
    private static final ImmutableSet<String> TASK_FIELDS = ImmutableSet.of("removalSet", "preconditionsSet", "username", "passwordVariable");

    protected String taskKey()
    {
        return BAMBOONICORN_REMOVE_MULTIPLE_VERSION.createTaskMappingKey();
    }

    @Override
    protected ImmutableSet<String> getTaskFields()
    {
        return TASK_FIELDS;
    }

}