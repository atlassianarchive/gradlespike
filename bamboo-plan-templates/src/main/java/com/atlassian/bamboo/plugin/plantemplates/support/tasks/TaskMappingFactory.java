package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionManager;
import com.atlassian.bamboo.plugin.plantemplates.support.RepositoryService;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.CopyToUnicornTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.ExecuteOnUnicornTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.InstallOnDemandTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.PromoteArtifactsVersionTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.PromotePluginVersionTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.PromoteProductVersionTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.ProvisionUnicornTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.RemoveArtifactsVersionTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.SetupUnicornTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.RaiseDeploymentTicketTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.StartUnicornTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.StopUnicornTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.UpdateLicenseTaskMapping;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_COPY_TO_UNICORN;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_EXECUTE_ON_UNICORN;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_INSTALL_ONDEMAND;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_PROMOTE_PLUGIN_VERSION;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_PROMOTE_PRODUCT_VERSION;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_PROMOTE_MULTIPLE_VERSION;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_REMOVE_MULTIPLE_VERSION;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_PROVISION_UNICORN;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_SETUP_UNICORN;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_RAISE_DEPLOYMENT_TICKET;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_START_UNICORN;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_STOP_UNICORN;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_UPDATE_LICENSE;

public class TaskMappingFactory
{
    private RepositoryService repositoryService;
    private ArtifactDefinitionManager artifactDefinitionManager;
    private PlanManager planManager;


    public TaskMappingFactory(RepositoryService repositoryService,
                              ArtifactDefinitionManager artifactDefinitionManager,
                              PlanManager planManager)
    {
        this.repositoryService = repositoryService;
        this.artifactDefinitionManager = artifactDefinitionManager;
        this.planManager = planManager;
    }



    public TaskMapping create(String type)
    {
        //TODO get the task types dynamic as a Groovy bean maybe?
        
        if("script".equalsIgnoreCase(type)){
            return new ScriptTaskMapping();
        }
        
        if("checkout".equalsIgnoreCase(type)){
            return new CheckoutTaskMapping(repositoryService);
        }
        
        if("maven3".equalsIgnoreCase(type)){
            return new Maven3TaskMapping();
        }
        
        if("maven2".equalsIgnoreCase(type)){
            return new Maven2TaskMapping();
        }
        
        if("deployBambooPlugin".equalsIgnoreCase(type)){
            return new DeployBambooPluginTaskMapping();
        }
        
        if("deployJiraPlugin".equalsIgnoreCase(type)){
            return new DeployJiraPluginTaskMapping();
        }

        if("deployConfluencePlugin".equalsIgnoreCase(type)){
            return new DeployConfluencePluginTaskMapping();
        }

        if ("jUnitParser".equalsIgnoreCase(type)) {
            return new JUnitParserTaskMapping();
        }
        
        if("brokerUrlDiscovery".equalsIgnoreCase(type)){
            return new BrokerUrlDiscoveryTaskMapping(); 
        }

        if("nodejs".equalsIgnoreCase(type)){
            return new NodeJsTaskMapping();
        }
        
        if("npm".equalsIgnoreCase(type)){
            return new NpmTaskMapping();
        }
        
        if ("cleanWorkingDirectory".equalsIgnoreCase(type)) {
            return new CleanWorkingDirectoryTaskMapping();
        }
        
        if ("artifactDownload".equalsIgnoreCase(type)) {
            return new ArtifactDownloadTaskMapping(artifactDefinitionManager, planManager);
        }
        
        if ("addRequirement".equalsIgnoreCase(type)) {
            return new AddRequirementTaskMapping();
        }
        
        if ("injectBambooVariables".equalsIgnoreCase(type)) {
            return new InjectBambooVariablesTaskMapping();
        }

        if ("ant".equalsIgnoreCase(type)) {
            return new AntTaskMapping();
        }

        if("mavenVersionVariableUpdater".equalsIgnoreCase(type))
        {
            return new MavenVersionUpdaterTaskMapping();
        }

        if("deployPlugin".equalsIgnoreCase(type))
        {
            return new DeployPluginTaskMapping();
        }

        if (BAMBOONICORN_PROVISION_UNICORN.getTaskKey().equalsIgnoreCase(type))
        {
            return new ProvisionUnicornTaskMapping();
        }

        if (BAMBOONICORN_PROMOTE_PLUGIN_VERSION.getTaskKey().equalsIgnoreCase(type))
        {
            return new PromotePluginVersionTaskMapping();
        }

        if (BAMBOONICORN_PROMOTE_PRODUCT_VERSION.getTaskKey().equalsIgnoreCase(type))
        {
            return new PromoteProductVersionTaskMapping();
        }

        if (BAMBOONICORN_PROMOTE_MULTIPLE_VERSION.getTaskKey().equalsIgnoreCase(type))
        {
            return new PromoteArtifactsVersionTaskMapping();
        }

        if (BAMBOONICORN_REMOVE_MULTIPLE_VERSION.getTaskKey().equalsIgnoreCase(type))
        {
            return new RemoveArtifactsVersionTaskMapping();
        }

        if (BAMBOONICORN_INSTALL_ONDEMAND.getTaskKey().equalsIgnoreCase(type))
        {
            return new InstallOnDemandTaskMapping();
        }

        if (BAMBOONICORN_START_UNICORN.getTaskKey().equalsIgnoreCase(type))
        {
            return new StartUnicornTaskMapping();
        }

        if (BAMBOONICORN_STOP_UNICORN.getTaskKey().equalsIgnoreCase(type))
        {
            return new StopUnicornTaskMapping();
        }

        if (BAMBOONICORN_UPDATE_LICENSE.getTaskKey().equalsIgnoreCase(type))
        {
            return new UpdateLicenseTaskMapping();
        }

        if (BAMBOONICORN_EXECUTE_ON_UNICORN.getTaskKey().equalsIgnoreCase(type))
        {
            return new ExecuteOnUnicornTaskMapping();
        }

        if (BAMBOONICORN_SETUP_UNICORN.getTaskKey().equalsIgnoreCase(type))
        {
            return new SetupUnicornTaskMapping();
        }

        if (BAMBOONICORN_COPY_TO_UNICORN.getTaskKey().equalsIgnoreCase(type))
        {
            return new CopyToUnicornTaskMapping();
        }

        if (BAMBOONICORN_RAISE_DEPLOYMENT_TICKET.getTaskKey().equalsIgnoreCase(type))
        {
            return new RaiseDeploymentTicketTaskMapping();
        }


        throw new IllegalArgumentException("The type of the task is not supported, type: " + type);
    }

}
