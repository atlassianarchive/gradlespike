package com.atlassian.bamboo.plugin.plantemplates.support.triggers;

import java.util.Map;

public interface TriggerMapping
{
    public Map<String, String> convert(@SuppressWarnings("rawtypes") Map trigger);

}
