package com.atlassian.bamboo.plugin.plantemplates;

import java.util.Map;

public interface PlanTemplateService
{
    Map<String, Object> runPlanTemplate(String template, String shortcuts);

    Map<String, Object> runPlanTemplate(String template);
    
    String expandDsl(String template, String shortcuts);

}