package com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions;

import java.util.Map;

import com.atlassian.bamboo.notification.NotificationManager;
import com.atlassian.bamboo.notification.NotificationRule;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.RecipientMapping;

public class DeploymentStartedAndFinishedMapping extends AbstractNotificationMapping
{

    public DeploymentStartedAndFinishedMapping(RecipientMapping recipientMapping,
                                               NotificationManager notificationManager)
    {
        super(recipientMapping, notificationManager);
    }

    @Override
    @SuppressWarnings("rawtypes") 
    public NotificationRule createNotificationRule(Map notificationMap)
    {
        String conditionKey = "bamboo.deployments:deploymentStartedFinished";
        String conditionData = "";
        String recipientData = recipientMapping.getRecipientData(notificationMap);
        String recipientKey = recipientMapping.getRecipientKey();

        NotificationRule notificationRule = notificationManager.createNotificationRule(conditionKey, conditionData, recipientData, recipientKey);
        return notificationRule;
    }

}
