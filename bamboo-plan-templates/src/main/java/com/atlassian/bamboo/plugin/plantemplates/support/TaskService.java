package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.TaskMappingFactory;
import com.atlassian.bamboo.task.TaskConfigurationService;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskManager;
import com.atlassian.bamboo.task.TaskModuleDescriptor;
import com.atlassian.bamboo.task.TaskRootDirectorySelector;
import com.atlassian.spring.container.ContainerManager;

import com.google.common.collect.Lists;

import org.apache.log4j.Logger;

public class TaskService
{
    private static final Logger log = Logger.getLogger(TaskService.class);
    
    private final TaskConfigurationService taskConfigurationService;
    private TaskManager taskManager;
    private TaskMappingFactory taskMappingFactory;
    private PlanManager planManager;
    private BuildDefinitionManager buildDefinitionManager;

    public TaskService(TaskConfigurationService taskConfigurationService, TaskMappingFactory taskMappingFactory, PlanManager planManager, BuildDefinitionManager buildDefinitionManager)
    {
        this.taskConfigurationService = taskConfigurationService;
        this.taskMappingFactory = taskMappingFactory;
        this.planManager = planManager;
        this.buildDefinitionManager = buildDefinitionManager;
        this.taskManager = null;
    }
    
    public void setTaskManager(TaskManager taskManager)
    {
        this.taskManager = taskManager;
    }
    
    public TaskManager getTaskManager()
    {
        if (this.taskManager == null)
            taskManager = (TaskManager) ContainerManager.getComponent("taskManager");
        return taskManager;
    }
    
    
    @SuppressWarnings("rawtypes")
    public Job performTasksChanges(Object tasksRepresentation, Job job)
    {
        if (tasksRepresentation == null)
        {
            return job;
        }

        if (tasksRepresentation instanceof Map)
        {
            return performTasksChangesList(Lists.newArrayList(tasksRepresentation), job);
        }

        if (tasksRepresentation instanceof List)
        {
            return performTasksChangesList((List) tasksRepresentation, job);
        }
        throw new IllegalArgumentException("tasksRepresentation is not a Map or a List is "
                                           + tasksRepresentation.getClass());
    }


    @SuppressWarnings("rawtypes")
    private Job performTasksChangesList(List tasksRepresentation, Job job)
    {
        Job workingJob = job;
        workingJob = deleteExistingTasks(workingJob);
        
        for (Object taskInfo : tasksRepresentation)
        {
            Map taskRepresentation = (Map) taskInfo;
            workingJob = createTask(taskRepresentation, workingJob);
        }
        return workingJob;
    }

    private Job deleteExistingTasks(Job job)
    {
        if(job.getBuildDefinition() == null){
            return job;
        }
        List<TaskDefinition> taskDefinitions = job.getBuildDefinition().getTaskDefinitions();
        for (TaskDefinition taskDefinition : taskDefinitions)
        {
            taskConfigurationService.deleteTask(job.getPlanKey(), taskDefinition.getId());
        }
        return planManager.getPlanByKey(job.getPlanKey(), Job.class);
    }

    @SuppressWarnings("rawtypes")
    private Job createTask(Map taskRepresentation, Job job)
    {
        String taskType = (String) taskRepresentation.get("type");
        String isFinal = (String) taskRepresentation.get("final");
        Map<String, String> taskParams = taskMappingFactory.create(taskType).convert(taskRepresentation);
        final TaskModuleDescriptor taskDescriptor = getTaskManager().getTaskDescriptor(taskParams.get("createTaskKey"));
        if(taskDescriptor == null){
            String error = "PlanTemplates: Could not get the taskDescriptor of the task '" + taskParams.get("userDescription") + "' with type: '" + taskType  + "' bamboo key '" +  taskParams.get("createTaskKey") + "'";
            log.error(error);
            throw new RuntimeException(error);
        }

        log.info("PlanTemplates: Creating task: '" + taskParams.get("userDescription") + "' with type: '" + taskType  + "'");        
        TaskDefinition taskDefinition = taskConfigurationService.createTask(job.getPlanKey(), taskDescriptor, taskParams.get("userDescription"), !Boolean.parseBoolean(taskParams.get("taskDisabled")), taskParams, TaskRootDirectorySelector.DEFAULT);
        
        if (Boolean.parseBoolean(isFinal)) {
            Job resultJob = planManager.getPlanByKey(job.getPlanKey(), Job.class);

            BuildDefinition buildDefinition = resultJob.getBuildDefinition();
            List<TaskDefinition> taskDefinitions = buildDefinition.getTaskDefinitions();

            for (TaskDefinition definition : taskDefinitions)
            {
                if(definition.getId() == taskDefinition.getId()) {
                    definition.setFinalising(true);
                }
            }

            buildDefinition.setTaskDefinitions(taskDefinitions);

            buildDefinitionManager.savePlanAndDefinition(resultJob, buildDefinition);
        }
        return planManager.getPlanByKey(job.getPlanKey(), Job.class);
    }

}
