package com.atlassian.bamboo.plugin.plantemplates.support.triggers;


public class TriggerMappingFactory
{

    public TriggerMapping create(String type)
    {
        if(type.equalsIgnoreCase("cron")) {            
            return new CronTriggerMapping();
        }
        
        if(type.equalsIgnoreCase("polling")) {            
            return new PollingTriggerMapping();
        }
        
        if(type.equalsIgnoreCase("daily")) {            
            return new DailyTriggerMapping();
        }
        
        if(type.equalsIgnoreCase("push")) {            
            return new PushTriggerMapping();
        }
        
        if(type.equalsIgnoreCase("afterSuccessfulPlan")) {            
            return new AfterSuccessfulPlanTriggerMapping();
        }
        
        throw new IllegalArgumentException("trigger type not recognized: "+ type);
        
    }

}
