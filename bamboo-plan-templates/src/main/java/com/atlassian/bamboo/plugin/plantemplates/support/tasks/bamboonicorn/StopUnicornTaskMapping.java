package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.atlassian.bamboo.plugin.plantemplates.support.tasks.AbstractTaskMapping;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_STOP_UNICORN;

public class StopUnicornTaskMapping extends AbstractTaskMapping
{
    public String taskKey()
    {
        return BAMBOONICORN_STOP_UNICORN.createTaskMappingKey();
    }

    @Override
    public Map<String, String> convert(@SuppressWarnings("rawtypes") Map task)
    {
        ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.builder();
        final String defaultStopMode = "STOP";
        final String defaultCheckBox = "false";

        mapBuilder.put("userDescription", (String) task.get("description"));
        copyFromDslToBambooProperty(task, mapBuilder, "taskDisabled", "taskDisabled", defaultCheckBox);
        copyFromDslToBambooProperty(task, mapBuilder, "unicornInstance");
        copyFromDslToBambooProperty(task, mapBuilder, "mode", "mode", defaultStopMode);
        mapBuilder.put("createTaskKey", taskKey());

        return mapBuilder.build();
    }
}
