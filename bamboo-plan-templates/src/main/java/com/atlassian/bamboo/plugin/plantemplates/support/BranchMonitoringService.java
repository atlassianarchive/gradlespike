package com.atlassian.bamboo.plugin.plantemplates.support;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.core.ScopedExclusionService;
import com.atlassian.bamboo.core.ScopedExclusionServiceHelper;
import com.atlassian.bamboo.event.BuildConfigurationUpdatedEvent;
import com.atlassian.bamboo.plan.branch.BranchDetectionService;
import com.atlassian.bamboo.plan.branch.BranchMonitoringConfiguration;
import com.atlassian.bamboo.plugin.plantemplates.support.branches.BranchMonitoringMapping;
import com.atlassian.event.api.EventPublisher;

import java.util.Map;

/**
 * Apply branch monitoring configuration to an existing chain.
 *
 * Much of this class was copied from {@link com.atlassian.bamboo.ww2.actions.branch.ConfigureBranches}.
 * It is unfortunate that Bamboo does not provide an API for this.
 */
public class BranchMonitoringService
{
    private final BranchMonitoringMapping branchMonitoringMapping;
    private final BuildDefinitionManager buildDefinitionManager;
    private final ScopedExclusionService scopedExclusionService;
    private final BranchDetectionService branchDetectionService;
    private final EventPublisher eventPublisher;

    public BranchMonitoringService(BranchMonitoringMapping branchMonitoringMapping,
                                   BuildDefinitionManager buildDefinitionManager,
                                   ScopedExclusionService scopedExclusionService,
                                   BranchDetectionService branchDetectionService,
                                   EventPublisher eventPublisher)
    {
        this.branchMonitoringMapping = branchMonitoringMapping;
        this.buildDefinitionManager = buildDefinitionManager;
        this.scopedExclusionService = scopedExclusionService;
        this.branchDetectionService = branchDetectionService;
        this.eventPublisher = eventPublisher;
    }

    @SuppressWarnings("rawtypes")
    public void applyAndSaveBranchMonitoringConfiguration(Map map, Chain chain)
    {
        BuildDefinition buildDefinition = buildDefinitionManager.getBuildDefinition(chain);
        BranchMonitoringConfiguration oldConfiguration = buildDefinition.getBranchMonitoringConfiguration();
        BranchMonitoringConfiguration newConfiguration = branchMonitoringMapping.apply(map);
        boolean shouldScheduleBranchListInitialisation = !oldConfiguration.isMonitoringEnabled() && newConfiguration.isMonitoringEnabled();

        oldConfiguration.populateFromConfig(newConfiguration.toConfiguration());

        // Use the exclusion service to make sure branch detection is not run while the plan is saved.
        scopedExclusionService.withLock(ScopedExclusionService.ExclusionScopeType.REPOSITORY_DATA, chain.getPlanKey(),
                                        ScopedExclusionServiceHelper.adapt(saveChainAndScheduleBranchInitialisation(chain, buildDefinition, shouldScheduleBranchListInitialisation)));
    }

    private Runnable saveChainAndScheduleBranchInitialisation(final Chain chain, final BuildDefinition buildDefinition, final boolean shouldScheduleBranchListInitialisation)
    {
        return new Runnable()
        {
            @Override
            public void run()
            {
                buildDefinitionManager.savePlanAndDefinition(chain, buildDefinition);
                if (shouldScheduleBranchListInitialisation)
                {
                    branchDetectionService.scheduleBranchListInitialisation(chain);
                }
                eventPublisher.publish(new BuildConfigurationUpdatedEvent(this, chain.getPlanKey()));
            }
        };
    }
}
