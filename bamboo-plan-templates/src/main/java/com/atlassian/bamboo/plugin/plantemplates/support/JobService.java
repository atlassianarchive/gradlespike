package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.build.PlanCreationDeniedException;
import com.atlassian.bamboo.build.PlanCreationException;
import com.atlassian.bamboo.build.creation.JobCreationService;
import com.atlassian.bamboo.build.creation.PlanCreationService;
import com.atlassian.bamboo.build.creation.PlanCreationService.EnablePlan;
import com.atlassian.bamboo.chains.ChainStage;
import com.atlassian.bamboo.deletion.DeletionService;
import com.atlassian.bamboo.fieldvalue.BuildDefinitionConverter;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.branch.ChainBranchManager;
import com.atlassian.bamboo.plan.branch.JobPropagatingFacade;
import com.atlassian.bamboo.webwork.util.ActionParametersMapImpl;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.atlassian.event.api.EventPublisher;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class JobService
{
    private static final Logger log = Logger.getLogger(JobService.class);

    private JobCreationService jobCreationService;
    private DeletionService deletionService;
    private ChainBranchManager chainBranchManager;
    private EventPublisher eventPublisher;
    private PlanManager planManager;
    private BuildDefinitionManager buildDefinitionManager;
    private TaskService taskService;
    private RequirementsService requirementsService;
    private ArtifactService artifactService;
    private MiscellaneousJobService miscellaneousJobService;

    public JobService(JobCreationService jobCreationService,
                      DeletionService deletionService,
                      ChainBranchManager chainBranchManager,
                      EventPublisher eventPublisher,
                      PlanManager planManager,
                      BuildDefinitionManager buildDefinitionManager,
                      TaskService taskService,
                      RequirementsService requirementsService,
                      ArtifactService artifactService,
                      MiscellaneousJobService miscellaneousJobService)
    {
        this.jobCreationService = jobCreationService;
        this.deletionService = deletionService;
        this.chainBranchManager = chainBranchManager;
        this.eventPublisher = eventPublisher;
        this.planManager = planManager;
        this.buildDefinitionManager = buildDefinitionManager;
        this.taskService = taskService;
        this.requirementsService = requirementsService;
        this.artifactService = artifactService;
        this.miscellaneousJobService = miscellaneousJobService;
    }

    @SuppressWarnings("rawtypes")
    public ChainStage performJobChanges(Object jobRepresentation, ChainStage chainStage)
    {
        if (jobRepresentation == null)
        {
            return chainStage;
        }
        if (jobRepresentation instanceof Map)
        {
            return performJobListChanges(Lists.newArrayList(jobRepresentation), chainStage);
        }

        if (jobRepresentation instanceof List)
        {
            return performJobListChanges((List) jobRepresentation, chainStage);
        }
        throw new IllegalArgumentException("jobRepresentation is not a Map or a List is "
                                           + jobRepresentation.getClass());
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private ChainStage performJobListChanges(final List jobRepresentation, ChainStage chainStage)
    {
        ChainStage workingStage = chainStage;
        Map<String, Job> currentJobs = getExistingJobs(chainStage);
        
        Set<String> processedJobKeys = Sets.newHashSet();
        
        for (Object jobInfo : jobRepresentation)
        {

            Map<String, Object> jobMap = (Map<String, Object>) jobInfo;
            String jobKey = ((String) jobMap.get("key")).toUpperCase();
            Job job = currentJobs.get(jobKey);
            try{
                if(job == null){
                    
                    String globalJobKey = chainStage.getChain().getKey() + "-" + jobKey;
                    Plan globalJob = planManager.getPlanByKey(PlanKeys.getPlanKey(globalJobKey));
                    if(globalJob != null){
                        String errorMsg = "Is not possible to use the key '"  + jobKey +  "' because it was use before in another stage, " +
                        		"please use different key for the job, or make sure the deletion of the job was performed";
                        log.error(errorMsg);
                        throw new IllegalArgumentException(errorMsg);
                    }                        
                    
                    log.info("PlanTemplates: Creating job: '" + jobKey + "'");
                    job = createJob(workingStage, jobMap);
                }else{
                    log.info("PlanTemplates: Updating job: '" + jobKey + "'");
                    job = updateJob(workingStage, jobMap, planManager.getPlanByKey(job.getPlanKey(), Job.class));
                }
                processedJobKeys.add(jobKey);
            }catch(Exception e){
            	String errorMsg = "PlanTemplates: Error processing the job with key: " + jobKey + " for the plan " + chainStage.getChain().getKey() + " with the message: " + e.getMessage();
            	log.error(errorMsg, e);
            	throw new RuntimeException(errorMsg, e);
            }

            //Requirements
            job = requirementsService.performChanges(jobMap.get("requirement"), job);

            //Artifacts
            job = artifactService.performDefinitionChanges(jobMap.get("artifactDefinition"), job);

            job = artifactService.performSubscriptionChanges(jobMap.get("artifactSubscription"), job);

            //Miscellaneous
            job = miscellaneousJobService.performChanges(jobMap.get("miscellaneousConfiguration"), job);

            //Tasks
            if (jobMap.containsKey("task")) {
                job = taskService.performTasksChanges(jobMap.get("task"), job);
            }

            workingStage = job.getStage();
            currentJobs = getExistingJobs(workingStage);
        }
        
        SetView<String> jobsKeysToDelete = Sets.difference(currentJobs.keySet(), processedJobKeys);
        for (String jobKeyToDelete : jobsKeysToDelete)
        {
            log.info("PlanTemplates: Deleting job " + jobKeyToDelete);
            deletionService.deletePlan(currentJobs.get(jobKeyToDelete));
        }
        deletionService.executeDelayedDeletion();
        
        return chainStage;
    }

    private Map<String, Job> getExistingJobs(ChainStage chainStage)
    {
        return Maps.uniqueIndex(chainStage.getJobs(), new Function<Job, String>()
        {
            @Override
            public String apply(@Nullable Job job)
            {
                return job.getBuildKey();
            }
        }
        );
    }

    private Job updateJob(ChainStage chainStage, Map<String, Object> jobMap, final Job job)
    {
        String buildName = (String) jobMap.get("name");
        String buildDescription = (String) jobMap.get("description");
        if (buildDescription == null) {
            buildDescription = "";
        }
        
        boolean enabled = true;
        if (jobMap.get("enabled") != null && !Boolean.parseBoolean((String) jobMap.get("enabled")))
        {
            enabled = false;
        }

        final JobPropagatingFacade buildableFacade = new JobPropagatingFacade(chainBranchManager, job);
        final boolean hasSuspensionStatusChanged = enabled == job.isSuspendedFromBuilding();

        boolean isTheSame = job.getBuildName().equals(buildName) &&
                            StringUtils.equals(job.getDescription(), buildDescription) &&
                            !hasSuspensionStatusChanged;

        if (isTheSame)
        {
            // no changes
            return job;
        }
        
        // set name and description and propagate to all branches
        buildableFacade.setBuildName(buildName);
        buildableFacade.setDescription(buildDescription);
        buildableFacade.setPlanSuspendedState(planManager, false);
        buildableFacade.savePlan(planManager);

        // repo id definition working directory only needs to be set on the master job
        BuildDefinition buildDefinition = job.getBuildDefinition();
        // buildDefinition.setRepositoryIdDefiningWorkingDir(repositoryDefiningWorkingDirectory);
        buildDefinitionManager.savePlanAndDefinition(job, buildDefinition);

        if (hasSuspensionStatusChanged)
        {
            buildableFacade.setPlanSuspendedState(planManager, !enabled);
        }

        buildableFacade.publishBuildConfigurationUpdatedEvent(eventPublisher, this);
        return planManager.getPlanByKey(job.getPlanKey(), Job.class);
        
    }

    private Job createJob(ChainStage chainStage, Map<String, Object> job)
    {

        Map<String, Object> jobContext = Maps.newHashMap();
        jobContext.put(JobCreationService.BUILD_KEY, chainStage.getChain().getKey());
        jobContext.put(JobCreationService.BUILD_NAME, job.get("name"));
        jobContext.put(JobCreationService.SUB_BUILD_KEY, job.get("key"));
        jobContext.put(JobCreationService.EXISTING_STAGE, chainStage.getName());
        jobContext.put(JobCreationService.BUILD_DESCRIPTION, job.get("description"));
        jobContext.put(JobCreationService.CHAIN, chainStage.getChain());

        EnablePlan enabledJob = PlanCreationService.EnablePlan.ENABLED;

        if (job.get("enabled") != null && !Boolean.parseBoolean((String) job.get("enabled")))
        {
            enabledJob = PlanCreationService.EnablePlan.DISABLED;
        }

        BuildConfiguration jobBuildConfiguration = new BuildConfiguration();
        jobBuildConfiguration.setProperty(BuildDefinitionConverter.INHERIT_REPOSITORY, "true");

        try
        {
            
           String jobFullKey = chainStage.getChain().getKey() + '-' + job.get("key").toString().toUpperCase();
            
           jobCreationService.createJobAndBranches(jobBuildConfiguration,
                                                         new ActionParametersMapImpl(jobContext),
                                                         enabledJob);
                     
           return planManager.getPlanByKey(PlanKeys.getPlanKey(jobFullKey), Job.class);
            
        }
        catch (PlanCreationException e)
        {
            throw new RuntimeException(e);
        }
        catch (PlanCreationDeniedException e)
        {
            throw new RuntimeException(e);
        }
    }


}
