package com.atlassian.bamboo.plugin.plantemplates;

import java.util.List;

public class SetupDslEvaluator
{
    public DslEvaluator setup(String shortcuts)
    {
        ShortcutEvaluator shortcutBuilderEvaluator = new ShortcutEvaluator();
        List<TemplateShortcut> shortcutList = shortcutBuilderEvaluator.parseShortcuts(shortcuts);

        return DslEvaluator.builder()
                    .withShortcuts(shortcutList)
                    .withValidationRules(new PlanTemplateValidationRules().getRules())
                    .build();
    }

}
