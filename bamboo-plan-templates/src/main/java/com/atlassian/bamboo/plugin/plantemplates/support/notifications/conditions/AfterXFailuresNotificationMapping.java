package com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions;

import com.atlassian.bamboo.notification.NotificationManager;
import com.atlassian.bamboo.notification.NotificationRule;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.RecipientMapping;

import java.util.Map;

public class AfterXFailuresNotificationMapping extends AbstractNotificationMapping
{
    public AfterXFailuresNotificationMapping(RecipientMapping recipientMapping, NotificationManager notificationManager)
    {
        super(recipientMapping, notificationManager);
    }

    @Override
    @SuppressWarnings("rawtypes")
    public NotificationRule createNotificationRule(Map notificationMap)
    {
        String conditionKey = "com.atlassian.bamboo.plugin.system.notifications:chainCompleted.XFailedChains";
        String conditionData = (String) notificationMap.get("X");
        String recipientData = recipientMapping.getRecipientData(notificationMap);
        String recipientKey = recipientMapping.getRecipientKey();

        NotificationRule notificationRule = notificationManager.createNotificationRule(conditionKey, conditionData, recipientData, recipientKey);
        return notificationRule;
    }
}
