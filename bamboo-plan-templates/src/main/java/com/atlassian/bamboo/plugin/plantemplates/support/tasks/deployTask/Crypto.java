package com.atlassian.bamboo.plugin.plantemplates.support.tasks.deployTask;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;

import org.apache.commons.codec.binary.Base64;

public class Crypto
{
    // FIXME this is borrowed from com.atlassian.bamboo.plugins.confdeploy.Crypto in 
    // bamboo-continuous-plugin-deployment
    // the reason is because you can not access to the class on the plugin, and the task
    // does a transformation of the password to an encrypt password
    
    private static final String ALGORITHM = "AES";
    
    private String generateKey()
    {
        final KeyGenerator keyGenerator;
        try
        {
            keyGenerator = KeyGenerator.getInstance(ALGORITHM);
        }
        catch (NoSuchAlgorithmException e)
        {
            final String msg = String.format("Unable to create KeyGenerator for %s algorithm: %s", ALGORITHM, e.getMessage());
            throw new RuntimeException(msg, e);
        }
        keyGenerator.init(128);

        SecretKey key = keyGenerator.generateKey();
        return Base64.encodeBase64String(key.getEncoded());
    }
    
    public Map<String, String> encrypt(String value)
    {
        if (Strings.isNullOrEmpty(value))
            throw new RuntimeException("Cannot encrypt an empty value");

        // Rebuild the key object from the key string
        final String key = generateKey();
        SecretKeySpec keySpec = new SecretKeySpec(Base64.decodeBase64(key), ALGORITHM);

        Cipher cipher;
        try
        {
            cipher = Cipher.getInstance(ALGORITHM);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException(e);
        }
        catch (NoSuchPaddingException e)
        {
            throw new RuntimeException(e);
        }

        try
        {
            cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        }
        catch (InvalidKeyException e)
        {
            throw new RuntimeException(e);
        }
        try
        {
            final String encrypted = Base64.encodeBase64String(cipher.doFinal(value.getBytes()));
            return ImmutableMap.of("key", key, "encrypted", encrypted);
        }
        catch (IllegalBlockSizeException e)
        {
            throw new RuntimeException(e);
        }
        catch (BadPaddingException e)
        {
            throw new RuntimeException(e);
        }
    }

}
