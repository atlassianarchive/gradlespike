package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.bamboo.deployments.projects.DeploymentProject;
import com.atlassian.bamboo.deployments.projects.service.DeploymentProjectService;
import com.atlassian.bamboo.exception.WebValidationException;

import com.google.common.collect.Sets;

import org.apache.log4j.Logger;

public class DeploymentVersioningService
{
    private static final Logger log = Logger.getLogger(DeploymentVersioningService.class);

    private DeploymentProjectService deploymentProjectService;
    
    public DeploymentVersioningService(DeploymentProjectService deploymentProjectService)
    {
        this.deploymentProjectService = deploymentProjectService;
    }

    @SuppressWarnings("rawtypes")
    public void performChanges(Object versioningObject, DeploymentProject deploymentProject)
    {
        Map versioning = (Map) versioningObject;
        if (versioning == null){
            return;
        }
        
        String version = (String) versioning.get("version");
        String autoIncrementNumber = (String) versioning.get("autoIncrementNumber");
        List variablesList = DslTagHelper.getDslTagList(versioning.get("variableToAutoIncrement"));
        
        Set<String> variableKeySet = Sets.newHashSet();
        for (Object variableObject : variablesList)
        {
            Map variable = (Map) variableObject;
            String key = (String) variable.get("key");
            if (key != null)
            {
                variableKeySet.add(key);
            }
        }
                
        try
        {
            log.info("PlanTemplates: Performing changes on deployment versioning with name: " 
                        + deploymentProject.getName() + " version '" + version +"' variableKeySet: " + variableKeySet);
            deploymentProjectService.updateVersionNamingScheme(deploymentProject.getId(), version, Boolean.valueOf(autoIncrementNumber), variableKeySet);
        }
        catch (WebValidationException e)
        {
            String errorMsg = "PlanTemplates: Error performing changes on deployment versioning with name: " + deploymentProject.getName();
            log.error(errorMsg);
            throw new IllegalArgumentException(errorMsg, e);
        }
    }

}
