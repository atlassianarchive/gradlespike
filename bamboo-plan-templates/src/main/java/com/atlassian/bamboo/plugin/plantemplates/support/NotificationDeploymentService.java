package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentService;
import com.atlassian.bamboo.exception.WebValidationException;
import com.atlassian.bamboo.notification.NotificationRule;
import com.atlassian.bamboo.notification.NotificationSet;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.NotificationMappingFactory;

import org.apache.log4j.Logger;

public class NotificationDeploymentService
{
    private static final Logger log = Logger.getLogger(NotificationDeploymentService.class);

    private NotificationMappingFactory notificationMappingFactory;

    private EnvironmentService environmentService;
    
    
    public NotificationDeploymentService(NotificationMappingFactory notificationMappingFactory,
                                         EnvironmentService environmentService)
    {
        this.notificationMappingFactory = notificationMappingFactory;
        this.environmentService = environmentService;
    }


    @SuppressWarnings("rawtypes")
    public void performNotificationChanges(Object notificationRepresentation, Environment environment)
    {
        List notifications = DslTagHelper.getDslTagList(notificationRepresentation);
        
        deleteExistingNotifications(environment);
        
        for (Object notification : notifications)
        {
            createNotifications((Map) notification, environment);
        }
    }

    private void deleteExistingNotifications(Environment environment)
    {
        NotificationSet existing = environmentService.getNotificationSet(environment.getId());
        if (existing == null)
        {
            return;
        }
        
        for(NotificationRule rule : existing.getNotificationRules())
        {
            try
            {
                environmentService.deleteNotification(environment.getId(), rule.getId());
            }
            catch (WebValidationException e)
            {
                String errorMsg = "PlanTemplates: Error deleting existing Notification for deployment environment: '" + environment.getName() + "'";
                log.error(errorMsg,e);
                throw new IllegalArgumentException(errorMsg, e);
            }
        }
        
    }


    @SuppressWarnings("rawtypes")
    private void createNotifications(Map notification, Environment environment)
    {
        String notificationType = (String) notification.get("type");
        String recipientType = (String) notification.get("recipient");

        NotificationRule notificationRule = notificationMappingFactory.create(notificationType, recipientType).createNotificationRule(notification);

        log.info("PlanTemplates: Adding Notification for Deployments: '" + notificationType + "'");        

        try
        {
            environmentService.addNotification(environment.getId(), notificationRule);
        }
        catch (WebValidationException e)
        {
            String errorMsg = "PlanTemplates: Error adding Notification for Deployments: '" + notificationType + "'";
            log.error(errorMsg,e);
            throw new IllegalArgumentException(errorMsg, e);
        }
    }
}
