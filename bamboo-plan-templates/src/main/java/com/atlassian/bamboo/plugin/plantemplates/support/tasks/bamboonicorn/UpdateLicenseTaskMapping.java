package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.atlassian.bamboo.plugin.plantemplates.support.tasks.AbstractTaskMapping;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import java.util.Map;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_UPDATE_LICENSE;

public class UpdateLicenseTaskMapping extends AbstractTaskMapping
{
    private static final ImmutableSet<String> TASK_FIELDS = ImmutableSet.of("license", "unicornInstance");

    protected String taskKey()
    {
        return BAMBOONICORN_UPDATE_LICENSE.createTaskMappingKey();
    }

    @Override
    public Map<String, String> convert(@SuppressWarnings("rawtypes") Map task)
    {
        ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.builder();
        final String defaultCheckBox = "false";

        copyFromDslToBambooProperty(task, mapBuilder, "taskDisabled", "taskDisabled", defaultCheckBox);
        mapBuilder.put("userDescription", (String) task.get("description"));
        mapBuilder.putAll(buildMap(task, TASK_FIELDS));
        mapBuilder.put("createTaskKey", taskKey());

        return mapBuilder.build();
    }
}
