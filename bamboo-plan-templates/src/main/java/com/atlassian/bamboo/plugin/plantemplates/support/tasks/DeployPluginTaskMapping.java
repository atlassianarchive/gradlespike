package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 *  Task mapping for the new continuous deployment plugin task
 */
public class DeployPluginTaskMapping extends AbstractDeployPluginTaskMapping
{
    public static final String DEPLOY_PLUGIN_TASK_PLUGIN_KEY = "com.atlassian.bamboo.plugins.deploy.continuous-plugin-deployment:deploy-task";

    @Override
    protected String taskKey() {
        return DEPLOY_PLUGIN_TASK_PLUGIN_KEY;
    }

    @Override
    public Map<String, String> convert(@SuppressWarnings("rawtypes") Map task)
    {
        ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
        builder.putAll(super.convert(task));

        String product = (String)task.get("product");
        builder.put("bcpd.config.productType", "bcpd.product." + product);
        return builder.build();
    }
}
