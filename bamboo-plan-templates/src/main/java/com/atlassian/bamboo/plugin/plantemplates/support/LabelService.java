package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.labels.Label;
import com.atlassian.bamboo.labels.LabelManager;
import com.atlassian.bamboo.labels.LabelParser;

import org.apache.log4j.Logger;

public class LabelService
{
    private static final Logger log = Logger.getLogger(LabelService.class);
    private static final String PLAN_TEMPLATES_LABEL = "plan-templates";

    private LabelManager labelManager;

    public LabelService(LabelManager labelManager)
    {
        this.labelManager = labelManager;
    }

    @SuppressWarnings("unchecked")
    public void performLabelChanges(Chain chain, Object labelRepresentation)
    {
        List<Map<String, String>> labels = DslTagHelper.getDslTagList(labelRepresentation);
        
        Set<Label> planLabels = labelManager.getPlanLabels(chain);
        for (Label label : planLabels)
        {
            labelManager.removeLabel(label.getName(), chain.getPlanKey(), null);
        }
        
        for (Map<String, String> labelMap : labels)
        {
            addLabel(chain, labelMap);
        }

        addDefaultLabel(chain);

    }

    private void addDefaultLabel(Chain chain) {
        labelManager.addLabel(PLAN_TEMPLATES_LABEL, chain.getPlanKey(), null);
    }

    private void addLabel(Chain chain, Map<String, String> labelMap) {
        String labelName = labelMap.get("name");
        if(labelName == null) {
            return;
        }
        if(!LabelParser.isValidLabelName(labelName)){
            throw new IllegalArgumentException("Label name contains invalid characters: " + labelName);
        }
        log.info("PlanTemplates: Adding Label: '" + labelName + "'");
        labelManager.addLabel(labelName, chain.getPlanKey(), null);
    }

}
