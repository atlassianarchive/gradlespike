package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.build.PlanDependency;
import com.atlassian.bamboo.build.PlanDependencyManager;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.v2.build.trigger.DependencyBlockingStrategy;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;

import org.apache.log4j.Logger;

public class PlanDependenciesService
{
    private static final Logger log = Logger.getLogger(PlanDependenciesService.class);

    
    private PlanDependencyManager planDependencyManager;
    private BuildDefinitionManager buildDefinitionManager;

    public PlanDependenciesService(PlanDependencyManager planDependencyManager,
                                   BuildDefinitionManager buildDefinitionManager)
    {
        this.planDependencyManager = planDependencyManager;
        this.buildDefinitionManager = buildDefinitionManager;
    }

    @SuppressWarnings("rawtypes")
    public void performChanges(Chain chain,  Map dependencies)
    {
        if (dependencies == null)
            return;
        
        log.info("PlanTemplates: Updating plan dependencies for plan: '" + chain.getBuildKey() + "'");
        
        List childPlans = DslTagHelper.getDslTagList(dependencies.get("childPlan"));

        Set<PlanKey> planKeys = Sets.newHashSet(); 
        for (Object child : childPlans)
        {
          Map childPlan = (Map) child;
          PlanKey planKey = PlanKeys.getPlanKey((String) childPlan.get("planKey"));
          planKeys.add(planKey);
        }
        
        
        planDependencyManager.adjustChildDependencyList(PlanDependency.DEFAULT_DEP_KEY, chain, planKeys, true);
        
        
        String blockingStrategy = (String) dependencies.get("blockingStrategy");
        String triggerOnlyAfterAllStagesGreen = (String) dependencies.get("triggerOnlyAfterAllStagesGreen");
        String automaticDependency = (String) dependencies.get("automaticDependency");
        String triggerForBranches = (String) dependencies.get("triggerForBranches");
        
        BuildDefinition buildDefinition = chain.getBuildDefinition();
        Map<String, String> customConfiguration = buildDefinition.getCustomConfiguration();
        
        if(!Strings.isNullOrEmpty(blockingStrategy))
            customConfiguration.put(DependencyBlockingStrategy.DEPENDENCY_BLOCKING_STRATEGY_CONFIG_KEY, blockingStrategyMap(blockingStrategy));
        
        customConfiguration.put("custom.dependencies.triggerOnlyAfterAllStages", triggerOnlyAfterAllStagesGreen);
        customConfiguration.put("custom.dependencies.triggerForBranches", triggerForBranches);
        customConfiguration.put("custom.dependency.automaticManagement.enabled", automaticDependency);
        
        buildDefinitionManager.savePlanAndDefinition(chain, buildDefinition);
        
        log.info("PlanTemplates: Finish updating plan dependencies for plan: '" + chain.getBuildKey() + "'");
        
    }
        
    
        
    private String blockingStrategyMap(String blockingStrategy) {
        //  blockingStrategy: 'notBlock' 'blockIfPendingBuilds' 'blockIfUnbuildChanges'        

        if ("notBlock".equals(blockingStrategy))
            return DependencyBlockingStrategy.None.toString();
        
        if ("blockIfPendingBuilds".equals(blockingStrategy))
            return DependencyBlockingStrategy.DontBuildIfParentInQueue.toString();
        
        if ("blockIfUnbuildChanges".equals(blockingStrategy))
            return DependencyBlockingStrategy.BuildParentIfChangesDetected.toString();
        
        return "";
    }

}
