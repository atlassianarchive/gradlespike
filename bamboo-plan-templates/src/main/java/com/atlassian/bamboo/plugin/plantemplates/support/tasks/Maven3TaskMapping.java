package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

public class Maven3TaskMapping extends AbstractTaskMapping
{
    private static final String MAVEN_3_TASK_PLUGIN_KEY = "com.atlassian.bamboo.plugins.maven:task.builder.mvn3";
    
    protected String taskKey(){
        return MAVEN_3_TASK_PLUGIN_KEY;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Map<String, String> convert(Map task)
    {
        Builder<String, String> mapBuilder = ImmutableMap.<String, String> builder();
        String defaultTestDirectory = "**/target/surefire-reports/*.xml";
        String defaultTestDirectoryOption = "standardTestDirectory";

        mapBuilder.put("userDescription", (String) task.get("description"));

        String taskDisabled = (String) task.get("taskDisabled");
        if (Strings.isNullOrEmpty(taskDisabled))
        {
            mapBuilder.put("taskDisabled", "false");
        }
        else
        {
            mapBuilder.put("taskDisabled", taskDisabled);
        }

        String goal = (String) task.get("goal");
        mapBuilder.put("goal", goal);
        
        copyFromDslToBambooProperty(task, mapBuilder, "useMavenReturnCode");
        
        String hasTests = (String) task.get("hasTests");
        if(!Strings.isNullOrEmpty(hasTests)){
            mapBuilder.put("testChecked", hasTests);
            
            String testDirectory = (String) task.get("testDirectory");
            if(!Strings.isNullOrEmpty(testDirectory)){
                defaultTestDirectory = testDirectory;
                defaultTestDirectoryOption = "customTestDirectory";
            } 
        }
            
        mapBuilder.put("testDirectoryOption", defaultTestDirectoryOption);
        mapBuilder.put("testResultsDirectory", defaultTestDirectory);
        
        copyFromDslToBambooProperty(task, mapBuilder, "mavenExecutable", "label");
        copyFromDslToBambooProperty(task, mapBuilder, "buildJdk");
        copyFromDslToBambooProperty(task, mapBuilder, "projectFile");
        copyFromDslToBambooProperty(task, mapBuilder, "environmentVariables");
        copyFromDslToBambooProperty(task, mapBuilder, "workingSubDirectory");

        mapBuilder.put("createTaskKey", taskKey());

        return mapBuilder.build();
    }

}
