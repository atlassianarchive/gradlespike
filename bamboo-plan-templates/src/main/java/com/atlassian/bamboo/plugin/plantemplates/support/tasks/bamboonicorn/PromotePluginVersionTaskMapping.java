package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.atlassian.bamboo.plugin.plantemplates.support.tasks.AbstractTaskMapping;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_PROMOTE_PLUGIN_VERSION;

public class PromotePluginVersionTaskMapping extends AbstractVersionTaskMapping
{
    private static final ImmutableSet<String> TASK_FIELDS = ImmutableSet.of("artifactName", "artifactVersion", "application", "username", "passwordVariable");

    protected String taskKey()
    {
        return BAMBOONICORN_PROMOTE_PLUGIN_VERSION.createTaskMappingKey();
    }

    @Override
    protected ImmutableSet<String> getTaskFields()
    {
        return TASK_FIELDS;
    }

}