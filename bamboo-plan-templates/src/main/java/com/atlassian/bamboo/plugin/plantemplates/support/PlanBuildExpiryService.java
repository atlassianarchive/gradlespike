package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.Map;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.chains.Chain;

import com.google.common.base.Strings;

import org.apache.log4j.Logger;

public class PlanBuildExpiryService
{
    
    private static final Logger log = Logger.getLogger(PlanBuildExpiryService.class);

    private BuildDefinitionManager buildDefinitionManager;

    public PlanBuildExpiryService(BuildDefinitionManager buildDefinitionManager)
    {
        this.buildDefinitionManager = buildDefinitionManager;
    }

    public void performChanges(Chain chain, @SuppressWarnings("rawtypes") Map buildExpiry)
    {
        // DSL
        // buildExpiry(notExpire: 'true', buildResults: 'true', artifacts: 'true', 
        // buildLogs: 'true', duration: '3', period: 'days', minimumBuildsToKeep: '3', labelsToKeep: 'label1 label2')

        // MAPPING
        // custom.buildExpiryConfig.enabled  -> true / false
        // custom.buildExpiryConfig.expiryTypeNothing  -> true / false
        // custom.buildExpiryConfig.expiryTypeResult  -> true / false
        // custom.buildExpiryConfig.expiryTypeArtifact  -> true / false
        // custom.buildExpiryConfig.expiryTypeBuildLog  -> true / false
        // custom.buildExpiryConfig.duration   -> number 
        // custom.buildExpiryConfig.period -> "days" "weeks" "months"
        // custom.buildExpiryConfig.buildsToKeep -> number
        // custom.buildExpiryConfig.excludeLabels  -> true / false
        // custom.buildExpiryConfig.labelsToKeep -> "label1 label2 label3"
        
        if(buildExpiry == null)
            return;
        
        log.info("PlanTemplates: Updating Build Expiry for plan: '" + chain.getBuildKey() + "'");

        String notExpire = (String) buildExpiry.get("notExpire");
        String buildResults = (String) buildExpiry.get("buildResults");
        String artifacts = (String) buildExpiry.get("artifacts");
        String buildLogs = (String) buildExpiry.get("buildLogs");
        String duration = (String) buildExpiry.get("duration");
        String period = (String) buildExpiry.get("period");
        String minimumBuildsToKeep = (String) buildExpiry.get("minimumBuildsToKeep");
        String labelsToKeep = (String) buildExpiry.get("labelsToKeep");
                    
        BuildDefinition buildDefinition = chain.getBuildDefinition();
        Map<String, String> customConfiguration = buildDefinition.getCustomConfiguration();

        customConfiguration.put("custom.buildExpiryConfig.enabled", "true");
        
        if(!Strings.isNullOrEmpty(notExpire)){
            customConfiguration.put("custom.buildExpiryConfig.expiryTypeNothing", notExpire);
        }
        if(!Strings.isNullOrEmpty(buildResults)){
            customConfiguration.put("custom.buildExpiryConfig.expiryTypeResult", buildResults);
            customConfiguration.put("custom.buildExpiryConfig.expiryTypeArtifact", buildResults);
            customConfiguration.put("custom.buildExpiryConfig.expiryTypeBuildLog", buildResults);
        }
        
        if(!Strings.isNullOrEmpty(artifacts)){
            customConfiguration.put("custom.buildExpiryConfig.expiryTypeResult", "false");
            customConfiguration.put("custom.buildExpiryConfig.expiryTypeArtifact", artifacts);
        }
        
        if(!Strings.isNullOrEmpty(buildLogs)){
            customConfiguration.put("custom.buildExpiryConfig.expiryTypeResult", "false");
            customConfiguration.put("custom.buildExpiryConfig.expiryTypeBuildLog", buildLogs);
        }
        
        if(!Strings.isNullOrEmpty(duration)){
            customConfiguration.put("custom.buildExpiryConfig.duration", duration);
        }
        if(!Strings.isNullOrEmpty(period)){
            customConfiguration.put("custom.buildExpiryConfig.period", period);
        }
        if(!Strings.isNullOrEmpty(minimumBuildsToKeep)){
            customConfiguration.put("custom.buildExpiryConfig.buildsToKeep", minimumBuildsToKeep);
        }
        
        if(!Strings.isNullOrEmpty(labelsToKeep)){
            customConfiguration.put("custom.buildExpiryConfig.excludeLabels", "true");
            customConfiguration.put("custom.buildExpiryConfig.labelsToKeep", labelsToKeep);
        }
        
        buildDefinitionManager.savePlanAndDefinition(chain, buildDefinition);
        
        log.info("PlanTemplates: Finish updating Build Expiry for plan: '" + chain.getBuildKey() + "'");
        
    }
    
    
    
}
