package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import com.atlassian.bamboo.deployments.environments.ConfigurationState;
import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentDeletionService;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentService;
import com.atlassian.bamboo.deployments.projects.DeploymentProject;
import com.atlassian.bamboo.exception.WebValidationException;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import org.apache.log4j.Logger;

public class EnvironmentDeploymentService
{
    private static final Logger log = Logger.getLogger(EnvironmentDeploymentService.class);
    
    private EnvironmentService environmentService;
    private EnvironmentDeletionService environmentDeletionService;
    private TaskDeploymentService taskDeploymentService;
    private TriggerDeploymentService triggerDeploymentService;
    private VariableDeploymentService variableDeploymentService;
    private NotificationDeploymentService notificationDeploymentService;

    public EnvironmentDeploymentService(EnvironmentService environmentService, EnvironmentDeletionService environmentDeletionService, 
                                        TaskDeploymentService taskDeploymentService, TriggerDeploymentService triggerDeploymentService,
                                        VariableDeploymentService variableDeploymentService, NotificationDeploymentService notificationDeploymentService)
    {
        this.environmentService = environmentService;
        this.environmentDeletionService = environmentDeletionService;
        this.taskDeploymentService = taskDeploymentService;
        this.triggerDeploymentService = triggerDeploymentService;
        this.variableDeploymentService = variableDeploymentService;
        this.notificationDeploymentService = notificationDeploymentService;
    }

    @SuppressWarnings("rawtypes")
    public void performChanges(Object environmentRepresentation, DeploymentProject deploymentProject)
    {
        
        List environmentRepresentations = DslTagHelper.getDslTagList(environmentRepresentation);
        List<Environment> existingEnvironments = environmentService.getEnvironmentsForDeploymentProject(deploymentProject.getId());
        
        List<Long> existingIds = Lists.transform(existingEnvironments,new Function<Environment, Long>()
                                                {
                                                    @Override
                                                    public Long apply(@Nullable Environment input)
                                                    {
                                                        return input.getId();
                                                    }
                                                });
        
        List<Long> updatedIds = Lists.newArrayList();
        
        for (Object representation : environmentRepresentations)
        {
            Map environment = (Map) representation;            
            if (environment != null)
            {
                Environment processed = createOrUpdateEnvironment(environment, deploymentProject, existingEnvironments);
                updatedIds.add(processed.getId());
            }
        }
        
        existingIds.removeAll(updatedIds);
        for (Long id : existingIds)
        {
            environmentDeletionService.delete(id);
        }
    }

    @SuppressWarnings("rawtypes") 
    private Environment createOrUpdateEnvironment(Map environmentMap, DeploymentProject deploymentProject, List<Environment> existingEnvironments)
    {
        String environmentName = (String) environmentMap.get("name");
        String environmentDescription = (String) environmentMap.get("description");        
        
        Environment environment = findEnvironmentByName(environmentName, deploymentProject, existingEnvironments);
        Environment environmentProcessed = null;
        log.info("PlanTemplates: Performing changes on deployment:environment with name: " + deploymentProject.getName() + " -> " + environmentName);

        try
        {
            if (environment != null) {
                environmentProcessed = environmentService.editEnvironment(environment.getId(), environmentName, environmentDescription);
            }else{
                environmentProcessed = environmentService.addEnvironment(deploymentProject.getId(), environmentName, environmentDescription);
            }
               
        } catch (WebValidationException e)
        {
            log.error("Error performing changes on deployment:environment with name: " + deploymentProject.getName() + " -> " + environmentName, e);
            throw new IllegalArgumentException(e);
        }
        
        taskDeploymentService.performTasksChanges(environmentMap.get("task"), environmentProcessed);
        environmentService.updateEnvironmentConfigurationState(environmentProcessed.getId(), ConfigurationState.TASKED);
        triggerDeploymentService.performTriggersChanges(environmentMap.get("trigger"), environmentProcessed);
        variableDeploymentService.performVariableChanges(environmentMap.get("variable"), environmentProcessed);
        notificationDeploymentService.performNotificationChanges(environmentMap.get("notification"), environmentProcessed);
        
        log.info("PlanTemplates: Finished to perform changes on deployment:environment with name: " + deploymentProject.getName() + " -> " + environmentName);
        
        return environmentProcessed;
    }



    private Environment findEnvironmentByName(String environmentName, DeploymentProject deploymentProject, List<Environment> environments)
    {
        for (Environment environment : environments)
        {
            if (environmentName.equals(environment.getName())){
                return environment;
            }
        }
        return null;
    }
}
