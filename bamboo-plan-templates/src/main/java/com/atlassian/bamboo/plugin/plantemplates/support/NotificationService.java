package com.atlassian.bamboo.plugin.plantemplates.support;

import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.notification.NotificationRule;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.NotificationMappingFactory;
import com.google.common.collect.Lists;

import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

public class NotificationService
{
    private static final Logger log = Logger.getLogger(NotificationService.class);

    private NotificationMappingFactory notificationMappingFactory;
    private PlanManager planManager;

    public NotificationService(NotificationMappingFactory notificationMappingFactory, PlanManager planManager)
    {
        this.notificationMappingFactory = notificationMappingFactory;
        this.planManager = planManager;
    }

    @SuppressWarnings("rawtypes")
    public  Chain performNotificationChanges(Chain chain, Object notifications)
    {
        List notificationList;
        if (notifications instanceof List) {
            notificationList = (List) notifications;
        }
        else if (notifications instanceof Map) {
            notificationList = Lists.newArrayList(notifications);
        }
        else if (notifications == null) {
            return chain;
        }
        else {
            throw new IllegalArgumentException("Notification object was neither a list nor a map");
        }

        for(Object notificationObject : notificationList) {
            chain = preformChanges(chain, (Map) notificationObject);
        }

        return chain;
    }

    @SuppressWarnings("rawtypes")
    private Chain preformChanges(Chain chain, Map notificationMap)
    {
        String notificationType = (String) notificationMap.get("type");
        String recipientType = (String) notificationMap.get("recipient");

        NotificationRule notificationRule = notificationMappingFactory.create(notificationType, recipientType).createNotificationRule(notificationMap);

        log.info("PlanTemplates: Adding Notification: '" + notificationType + "'");        
        chain.getNotificationSet().addNotification(notificationRule);

        planManager.savePlan(chain);

        return planManager.getPlanByKey(chain.getPlanKey(), Chain.class);
    }
}
