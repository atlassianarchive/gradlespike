package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.bamboo.build.PlanCreationDeniedException;
import com.atlassian.bamboo.build.creation.ChainCreationService;
import com.atlassian.bamboo.build.creation.PlanCreationService.EnablePlan;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.deletion.DeletionService;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.branch.ChainBranchManager;
import com.atlassian.bamboo.repository.Repository;
import com.atlassian.bamboo.repository.nullrepository.NullRepository;
import com.atlassian.bamboo.webwork.util.ActionParametersMapImpl;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;

import org.apache.log4j.Logger;

public class PlanService
{
    
    private static final Logger log = Logger.getLogger(PlanService.class);

    private PlanManager planManager;
    private ChainCreationService chainCreationService;
    private RepositoryService repositoryService;
    private TriggerService triggerService;
    private NotificationService notificationService;
    private LabelService labelService;
    private VariableService variableService;
    private MiscellaneousPlanService miscellaneousPlanService;

    public PlanService(PlanManager planManager,
                       ChainCreationService chainCreationService,
                       RepositoryService repositoryService,
                       TriggerService triggerService, 
                       NotificationService notificationService,
                       LabelService labelService,
                       VariableService variableService,
                       ChainBranchManager chainBranchManager,
                       DeletionService deletionService,
                       MiscellaneousPlanService miscellaneousPlanService)
    {
        this.planManager = planManager;
        this.chainCreationService = chainCreationService;
        this.repositoryService = repositoryService;
        this.triggerService = triggerService;
        this.notificationService = notificationService;
        this.labelService = labelService;
        this.variableService = variableService;
        this.miscellaneousPlanService = miscellaneousPlanService;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public Chain createOrUpdate(Map planRepresentation) throws PlanCreationDeniedException
    {
        String planKeyString = (String) planRepresentation.get("key");
        String planName = (String) planRepresentation.get("name");
        String description = (String) planRepresentation.get("description");
        String projectKey = ((Map<String, String> )planRepresentation.get("project")).get("key");
        PlanKey planKey = PlanKeys.getPlanKey(projectKey.toUpperCase() + "-" + planKeyString.toUpperCase());
        
        Chain chain;
        if (planManager.isPlanKeyConflicting(planKey))
        {
            log.info("PlanTemplates: Updating plan: '" + projectKey + "-" +planKeyString + "'");
            chain = updatePlan(planName, description, planKey);
        }
        else
        {
            log.info("PlanTemplates: Creating plan: '" + projectKey + "-" +planKeyString + "'");
            chain = createPlan(planKeyString, planName, description, projectKey);
        }

        repositoryService.updateRespository(chain, planRepresentation.get("repository"));
        triggerService.performTriggerChanges(chain, planRepresentation.get("trigger"));
        notificationService.performNotificationChanges(chain, planRepresentation.get("notification"));
        labelService.performLabelChanges(chain, planRepresentation.get("label"));
        variableService.performVariableChanges(chain, planRepresentation.get("variable"));
        miscellaneousPlanService.performChanges(chain, (Map<String, Object>)planRepresentation.get("planMiscellaneous"));

        save(chain);
        return chain;
    }

    private Chain updatePlan(String planName, String description, PlanKey planKey)
    {
        Chain chain;
        chain = planManager.getPlanByKey(planKey, Chain.class);
        chain.setSuspendedFromBuilding(true);
        chain.setBuildName(planName);
        chain.setDescription(description);
        return chain;
    }

    private Chain createPlan(String planKeyString,
                             String planName,
                             String description,
                             String projectKey) throws PlanCreationDeniedException
    {
        PlanKey planKey;
        Chain chain;
        ActionParametersMapImpl actionParametersMap = new ActionParametersMapImpl(new HashMap<String, Object>());
        actionParametersMap.put(ChainCreationService.CHAIN_NAME, planName);
        actionParametersMap.put(ChainCreationService.CHAIN_KEY, planKeyString);
        actionParametersMap.put("chainDescription", description);
        actionParametersMap.put("existingProjectKey", projectKey);

        BuildConfiguration buildConfiguration = new BuildConfiguration();
        buildConfiguration.addProperty(Repository.SELECTED_REPOSITORY, NullRepository.KEY);
        
        planKey = PlanKeys.getPlanKey(chainCreationService.createPlan(buildConfiguration, actionParametersMap, EnablePlan.DISABLED));
        chain = planManager.getPlanByKey(planKey, Chain.class);
        return chain;
    }
    
    public void save(Plan chain){
        planManager.savePlan(chain);        
    }

}
