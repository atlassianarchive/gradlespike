package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.AbstractTaskMapping;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import java.util.Map;

public class SetupUnicornTaskMapping extends AbstractTaskMapping
{
    private static final ImmutableSet<String> TASK_FIELDS = ImmutableSet.of("gappsDomain", "unicornInstance", "version", "localYaml", "source", "destination", "license", "halTimeout");

    protected String taskKey() {
        return TaskMappingKey.BAMBOONICORN_SETUP_UNICORN.createTaskMappingKey();
    }

    @Override
    public Map<String, String> convert(@SuppressWarnings("rawtypes") Map task)
    {
        ImmutableMap.Builder<String, String> mapBuilder = ImmutableMap.builder();
        final String defaultCheckBox = "false";
        final String defaultAdminUsername = "admin";
        final String defaultAdminPassword = "admin";
        final String defaultAdminEmail = "admin@atlassian.com";

        mapBuilder.put("userDescription", (String) task.get("description"));
        copyFromDslToBambooProperty(task, mapBuilder, "taskDisabled", "taskDisabled", defaultCheckBox);
        copyFromDslToBambooProperty(task, mapBuilder, "username", "username", defaultAdminUsername);
        copyFromDslToBambooProperty(task, mapBuilder, "password", "password", defaultAdminPassword);
        copyFromDslToBambooProperty(task, mapBuilder, "email", "email", defaultAdminEmail);
        copyFromDslToBambooProperty(task, mapBuilder, "cleanBeforeInstall", "cleanBeforeInstall", defaultCheckBox);
        mapBuilder.putAll(buildMap(task, TASK_FIELDS));
        mapBuilder.put("createTaskKey", taskKey());

        return mapBuilder.build();
    }
}
