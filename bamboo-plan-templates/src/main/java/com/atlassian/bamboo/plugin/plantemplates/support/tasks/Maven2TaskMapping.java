package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

public class Maven2TaskMapping extends Maven3TaskMapping
{
    private static final String MAVEN_2_TASK_PLUGIN_KEY = "com.atlassian.bamboo.plugins.maven:task.builder.mvn2";

    @Override
    protected String taskKey()
    {
        return MAVEN_2_TASK_PLUGIN_KEY;
    }

}
