package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;
import com.google.common.collect.Lists;

public class DslTagHelper
{
    @SuppressWarnings("rawtypes")
    public static List getDslTagList(Object entityRepresentation)
    {
        List entityRepresentations;
        Object entities = entityRepresentation;
        if(entities instanceof List) {
            entityRepresentations = (List) entities;
        }
        else if (entities instanceof Map) {
            entityRepresentations = Lists.newArrayList(entities);
        }
        else {
            return Lists.newArrayList();
        }
        return entityRepresentations;
    }
}