package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

import com.atlassian.bamboo.plugin.BambooPluginUtils;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

public class ScriptTaskMapping extends AbstractTaskMapping implements TaskMapping
{
    private static final String SCRIPT_TASK_PLUGIN_KEY = BambooPluginUtils.SCRIPT_BUILDER_TASK_PLUGIN_KEY;

    // userDescription
    // taskDisabled (false, true)
    // scriptLocation (INLINE, FILE)
    // script
    // scriptBody
    // argument
    // environmentVariables
    // workingSubDirectory
    // createTaskKey -> com.atlassian.bamboo.plugins.scripttask:task.builder.script
    
    @SuppressWarnings("rawtypes") 
    public Map<String, String> convert(Map task)
    {
        Builder<String, String> mapBuilder = ImmutableMap.<String, String>builder();
        
        mapBuilder.put("userDescription", (String) task.get("description"));        

        String taskDisabled = (String)task.get("taskDisabled");
        if(Strings.isNullOrEmpty(taskDisabled)){
            mapBuilder.put("taskDisabled", "false");
        }else{
            mapBuilder.put("taskDisabled", taskDisabled);
        }
        
        String script = (String) task.get("script");
        if(!Strings.isNullOrEmpty(script)){
            mapBuilder.put("scriptLocation", "FILE");
            mapBuilder.put("script", script);
        }

        String scriptBody = (String) task.get("scriptBody");
        if(!Strings.isNullOrEmpty(scriptBody)){
            mapBuilder.put("scriptLocation", "INLINE");
            mapBuilder.put("scriptBody", scriptBody);
        }
        
        copyFromDslToBambooProperty(task, mapBuilder, "argument");
        copyFromDslToBambooProperty(task, mapBuilder, "environmentVariables");
        copyFromDslToBambooProperty(task, mapBuilder, "workingSubDirectory");
        mapBuilder.put("createTaskKey", SCRIPT_TASK_PLUGIN_KEY);

        return mapBuilder.build();
        
    }
    
    
}
