plan(key:'HN14', name:'Hostnode 14 test', description:'JIRA Start testcase on hn14', planEnabled:'true') {
    project(key:'AGENTPERF', name:'Agent Performance Testing', description:'Build Engineerings Agent Performance Testing')
    stage(name:'Default Stage', description:'', manual:'false') {
        job(key:'HN14X1', name:'Internal Agent on HN14 - 1', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 0 30', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X2', name:'Internal Agent on HN14 - 2', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 2 28', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X3', name:'Internal Agent on HN14 - 3', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 4 26', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X4', name:'Internal Agent on HN14 - 4', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 6 24', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X5', name:'Internal Agent on HN14 - 5', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 8 22', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X6', name:'Internal Agent on HN14 - 6', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 10 20', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X7', name:'Internal Agent on HN14 - 7', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 12 18', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X8', name:'Internal Agent on HN14 - 8', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 14 16', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X9', name:'Internal Agent on HN14 - 9', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 16 14', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X10', name:'Internal Agent on HN14 - 10', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 18 12', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X11', name:'Internal Agent on HN14 - 11', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 20 10', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X12', name:'Internal Agent on HN14 - 12', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 22 8', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X13', name:'Internal Agent on HN14 - 13', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 24 6', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X14', name:'Internal Agent on HN14 - 14', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 26 4', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
    }
}
plan(key:'HN15', name:'Hostnode 15 test', description:'JIRA Start testcase on hn15', planEnabled:'true') {
    project(key:'AGENTPERF', name:'Agent Performance Testing', description:'Build Engineerings Agent Performance Testing')
    stage(name:'Default Stage', description:'', manual:'false') {
        job(key:'HN15X1', name:'Internal Agent on HN15 - 1', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 0 30', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'15')
        }
        job(key:'HN15X2', name:'Internal Agent on HN15 - 2', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 2 28', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'15')
        }
        job(key:'HN15X3', name:'Internal Agent on HN15 - 3', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 4 26', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'15')
        }
        job(key:'HN15X4', name:'Internal Agent on HN15 - 4', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 6 24', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'15')
        }
        job(key:'HN15X5', name:'Internal Agent on HN15 - 5', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 8 22', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'15')
        }
        job(key:'HN15X6', name:'Internal Agent on HN15 - 6', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 10 20', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'15')
        }
        job(key:'HN15X7', name:'Internal Agent on HN15 - 7', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 12 18', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'15')
        }
        job(key:'HN15X8', name:'Internal Agent on HN15 - 8', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 14 16', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'15')
        }
        job(key:'HN15X9', name:'Internal Agent on HN15 - 9', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 16 14', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'15')
        }
        job(key:'HN15X10', name:'Internal Agent on HN15 - 10', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 18 12', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'15')
        }
        job(key:'HN15X11', name:'Internal Agent on HN15 - 11', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 20 10', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'15')
        }
        job(key:'HN15X12', name:'Internal Agent on HN15 - 12', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 22 8', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'15')
        }
        job(key:'HN15X13', name:'Internal Agent on HN15 - 13', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 24 6', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'15')
        }
        job(key:'HN15X14', name:'Internal Agent on HN15 - 14', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 26 4', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'15')
        }
    }
}
plan(key:'HN16', name:'Hostnode 16 test', description:'JIRA Start testcase on hn16', planEnabled:'true') {
    project(key:'AGENTPERF', name:'Agent Performance Testing', description:'Build Engineerings Agent Performance Testing')
    stage(name:'Default Stage', description:'', manual:'false') {
        job(key:'HN16X1', name:'Internal Agent on HN16 - 1', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 0 30', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'16')
        }
        job(key:'HN16X2', name:'Internal Agent on HN16 - 2', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 2 28', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'16')
        }
        job(key:'HN16X3', name:'Internal Agent on HN16 - 3', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 4 26', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'16')
        }
        job(key:'HN16X4', name:'Internal Agent on HN16 - 4', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 6 24', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'16')
        }
        job(key:'HN16X5', name:'Internal Agent on HN16 - 5', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 8 22', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'16')
        }
        job(key:'HN16X6', name:'Internal Agent on HN16 - 6', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 10 20', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'16')
        }
        job(key:'HN16X7', name:'Internal Agent on HN16 - 7', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 12 18', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'16')
        }
        job(key:'HN16X8', name:'Internal Agent on HN16 - 8', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 14 16', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'16')
        }
        job(key:'HN16X9', name:'Internal Agent on HN16 - 9', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 16 14', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'16')
        }
        job(key:'HN16X10', name:'Internal Agent on HN16 - 10', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 18 12', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'16')
        }
        job(key:'HN16X11', name:'Internal Agent on HN16 - 11', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 20 10', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'16')
        }
        job(key:'HN16X12', name:'Internal Agent on HN16 - 12', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 22 8', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'16')
        }
        job(key:'HN16X13', name:'Internal Agent on HN16 - 13', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 24 6', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'16')
        }
        job(key:'HN16X14', name:'Internal Agent on HN16 - 14', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 26 4', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'16')
        }
    }
}
plan(key:'HN17', name:'Hostnode 17 test', description:'JIRA Start testcase on hn17', planEnabled:'true') {
    project(key:'AGENTPERF', name:'Agent Performance Testing', description:'Build Engineerings Agent Performance Testing')
    stage(name:'Default Stage', description:'', manual:'false') {
        job(key:'HN17X1', name:'Internal Agent on HN17 - 1', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 0 30', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'17')
        }
        job(key:'HN17X2', name:'Internal Agent on HN17 - 2', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 2 28', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'17')
        }
        job(key:'HN17X3', name:'Internal Agent on HN17 - 3', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 4 26', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'17')
        }
        job(key:'HN17X4', name:'Internal Agent on HN17 - 4', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 6 24', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'17')
        }
        job(key:'HN17X5', name:'Internal Agent on HN17 - 5', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 8 22', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'17')
        }
        job(key:'HN17X6', name:'Internal Agent on HN17 - 6', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 10 20', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'17')
        }
        job(key:'HN17X7', name:'Internal Agent on HN17 - 7', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 12 18', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'17')
        }
        job(key:'HN17X8', name:'Internal Agent on HN17 - 8', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 14 16', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'17')
        }
        job(key:'HN17X9', name:'Internal Agent on HN17 - 9', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 16 14', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'17')
        }
        job(key:'HN17X10', name:'Internal Agent on HN17 - 10', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 18 12', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'17')
        }
        job(key:'HN17X11', name:'Internal Agent on HN17 - 11', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 20 10', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'17')
        }
        job(key:'HN17X12', name:'Internal Agent on HN17 - 12', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 22 8', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'17')
        }
        job(key:'HN17X13', name:'Internal Agent on HN17 - 13', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 24 6', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'17')
        }
        job(key:'HN17X14', name:'Internal Agent on HN17 - 14', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 26 4', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'17')
        }
    }
}
plan(key:'HN18', name:'Hostnode 18 test', description:'JIRA Start testcase on hn18', planEnabled:'true') {
    project(key:'AGENTPERF', name:'Agent Performance Testing', description:'Build Engineerings Agent Performance Testing')
    stage(name:'Default Stage', description:'', manual:'false') {
        job(key:'HN18X1', name:'Internal Agent on HN18 - 1', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 0 30', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'18')
        }
        job(key:'HN18X2', name:'Internal Agent on HN18 - 2', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 2 28', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'18')
        }
        job(key:'HN18X3', name:'Internal Agent on HN18 - 3', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 4 26', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'18')
        }
        job(key:'HN18X4', name:'Internal Agent on HN18 - 4', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 6 24', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'18')
        }
        job(key:'HN18X5', name:'Internal Agent on HN18 - 5', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 8 22', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'18')
        }
        job(key:'HN18X6', name:'Internal Agent on HN18 - 6', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 10 20', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'18')
        }
        job(key:'HN18X7', name:'Internal Agent on HN18 - 7', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 12 18', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'18')
        }
        job(key:'HN18X8', name:'Internal Agent on HN18 - 8', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 14 16', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'18')
        }
        job(key:'HN18X9', name:'Internal Agent on HN18 - 9', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 16 14', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'18')
        }
        job(key:'HN18X10', name:'Internal Agent on HN18 - 10', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 18 12', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'18')
        }
        job(key:'HN18X11', name:'Internal Agent on HN18 - 11', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 20 10', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'18')
        }
        job(key:'HN18X12', name:'Internal Agent on HN18 - 12', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 22 8', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'18')
        }
        job(key:'HN18X13', name:'Internal Agent on HN18 - 13', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 24 6', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'18')
        }
        job(key:'HN18X14', name:'Internal Agent on HN18 - 14', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 26 4', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'18')
        }
    }
}
plan(key:'ALLHNS', name:'All hostnodes test', description:'JIRA Start testcase on hn$hostnode', planEnabled:'true') {
    project(key:'AGENTPERF', name:'Agent Performance Testing', description:'Build Engineerings Agent Performance Testing')
    stage(name:'Default Stage', description:'', manual:'false') {
        job(key:'HN14X1', name:'Internal Agent on HN14 - 1', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 0 30', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X2', name:'Internal Agent on HN14 - 2', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 2 28', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X3', name:'Internal Agent on HN14 - 3', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 4 26', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X4', name:'Internal Agent on HN14 - 4', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 6 24', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X5', name:'Internal Agent on HN14 - 5', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 8 22', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X6', name:'Internal Agent on HN14 - 6', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 10 20', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X7', name:'Internal Agent on HN14 - 7', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 12 18', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X8', name:'Internal Agent on HN14 - 8', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 14 16', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X9', name:'Internal Agent on HN14 - 9', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 16 14', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X10', name:'Internal Agent on HN14 - 10', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 18 12', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X11', name:'Internal Agent on HN14 - 11', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 20 10', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X12', name:'Internal Agent on HN14 - 12', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 22 8', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X13', name:'Internal Agent on HN14 - 13', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 24 6', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
        job(key:'HN14X14', name:'Internal Agent on HN14 - 14', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Voodoo')
            task(type:'script_task', description:'Start JIRA. Many times', script_body:'./run-jira-start.sh 26 4', script_working_sub_directory:'agent-perf-testing')
            requirement(key:'hostnode', value:'14')
        }
    }
}
plan(key:'MAIN', name:'Main', description:'BuildEng Puppet CI Pipeline', planEnabled:'true') {
    project(key:'PUPPETCI', name:'Puppet CI', description:'Build Engineerings Puppet CI')
    stage(name:'Create Package', description:'Build puppet tree & perform sanity checks', manual:'false')
    stage(name:'Vagrant', description:'Provision vagrant instances from scratch', manual:'false') {
        job(key:'VAGRANTBASE', name:'Base Infra - statsd install4j graphite', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Puppet')
            task(type:'script_task', description:'statsd install4j graphite', script_body:'/utils/ci/vagrant-provision-from-scratch statsd install4j graphite', script_working_sub_directory:'extra stuff')
            requirement(key:'virtualbox', value:'true')
        }
        job(key:'VAGRANTMAVEN', name:'Maven Infra - m2proxy sandbox sandboxpromoter maven', enabled:'true') {
            task(type:'scm_checkout', description:'Source Checkout', scm_repository:'BuildEng Puppet')
            task(type:'script_task', description:'m2proxy sandbox sandboxpromoter maven', script_body:'/utils/ci/vagrant-provision-from-scratch m2proxy sandbox sandboxpromoter maven', script_working_sub_directory:'extra stuff')
            requirement(key:'virtualbox', value:'true')
        }
    }
    stage(name:'Staging', description:'Roll out to Staging Environments', manual:'false')
    stage(name:'Production', description:'Roll out to production', manual:'true')
}
