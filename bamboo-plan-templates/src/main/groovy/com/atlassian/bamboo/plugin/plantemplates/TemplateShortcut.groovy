package com.atlassian.bamboo.plugin.plantemplates

class TemplateShortcut {

    private final Object identifier
    private final String dsl
    private final List<String> args

    TemplateShortcut(Object identifier, String dsl, List<String> args) {
        this.identifier = identifier
        this.dsl = dsl
        this.args = args
    }

    public boolean matches(Object name) {
        return (name.equals(identifier))
    }

    public Object apply(Map attributes, List shortcuts) {        
        DslEvaluator dslEvaluator = DslEvaluator.builder().withShortcuts(shortcuts).build()
        return dslEvaluator.evaluate(substituteArgs(attributes))
    }

    private String substituteArgs(Map<String, String> attributes) {
        def result = dsl
        if (args != null) {
            validateArgs(attributes)
            attributes.each {key, value ->
                if (args.contains(key)) {
                    def replaceValue = value.replace(/'/, /\'/).replace(/"/, /\"/)
                    result = result.replace('#' + key, replaceValue)
                }
            }
        }
        return result
    }

    private void validateArgs(Map<String, String> attributes) {
        def notFound = []
        args.each { arg ->
            if (!attributes.containsKey(arg)) {
                notFound.add(arg)
            }
        }
        if (notFound.size() > 0)
            throw new IllegalArgumentException("No value supplied for arguments " + notFound.toListString() + " when processing shortcut " + identifier)
    }
    
    public String toString() {
        return "Shortcut[identifier:${identifier}, args: ${args}, dsl: ${dsl}]"
    }

    public boolean equals(Object o) {
        if (! (o instanceof TemplateShortcut)) {
            return false;
        }
        def obj = o as TemplateShortcut;
        if (obj.identifier == this.identifier && obj.dsl == this.dsl && obj.args == this.args) {
            return true;
        }
        else {
            return false;
        }
    }
}
