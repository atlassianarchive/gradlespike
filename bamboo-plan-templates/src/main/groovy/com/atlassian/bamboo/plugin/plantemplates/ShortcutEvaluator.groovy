package com.atlassian.bamboo.plugin.plantemplates

class ShortcutEvaluator {

    def List<TemplateShortcut> parseShortcuts(String dslBody) {
        if (!dslBody || dslBody.isEmpty()) {
            return []
        }

        def result = []
        def parsedShortcuts = evaluate(dslBody)
        parsedShortcuts.each {shortcut ->
            result.add(new TemplateShortcut(shortcut.name, shortcut.value, shortcut.args))
        }
        return result
    }

    def List<Map> evaluate(String dslBody) {
        ShortcutBuilder builder = new ShortcutBuilder()
        BuilderBinding binding = new BuilderBinding()

        binding.builder = builder

        def dslScript = new GroovyShell().parse(dslBody)
        dslScript.binding = binding
        dslScript.run()

        return builder.result
    }
}
