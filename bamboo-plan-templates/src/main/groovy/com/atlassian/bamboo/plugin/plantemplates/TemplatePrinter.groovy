package com.atlassian.bamboo.plugin.plantemplates

class TemplatePrinter {

    def String buildDslFromMap(Map map) {
        def topNode = new TemplateNode()
        topNode.name = "dummy"
        populate(topNode, map)

        return nodeToString(topNode, 0).trim()
    }

    def populate(TemplateNode node, Map<String, Object> attributes) {
        attributes.each {entry ->
            if (entry.value instanceof String) {
                node.attributes[entry.key] = entry.value
            }
            else if (entry.value instanceof Map) {
                node.children.add(processChild(entry.key, entry.value as Map))
            }
            else if (entry.value instanceof List) {
                entry.value.each { item ->
                    node.children.add(processChild(entry.key, item))
                }
            }
            else {
                throw new IllegalArgumentException("Processing node '${node.name}' with attributes '${attributes}' the attribute ${entry?.key} is a not handle type, entry class: ${entry?.value?.class} with toString ${entry}")
            }
        }
    }

    def TemplateNode processChild(String name, Map attributes) {
        def childNode = new TemplateNode()
        childNode.name = name
        populate(childNode, attributes)
        return childNode
    }

    def nodeToString(TemplateNode node, int indent) {
        def result = addIndent("", indent)

        if (node.name == "dummy") {
            node.children.each { child ->
                result += nodeToString(child, 0)
            }
            return result
        }

        result += node.name + "("
        def flag = 0
        node.attributes.each {entry ->
            if(entry.value && entry.value instanceof String && entry.value.startsWith('\n')){
                result += ", "*flag + entry.key + ":'''" + entry.value + "'''"
            }else{
                result += ", "*flag + entry.key + ":'" + entry.value + "'"
            }      
            flag = 1
        }
        result += ")"

        if (!node.children.isEmpty()) {
            result += " {\n"
            node.children.each { child ->
                result += nodeToString(child, indent+1)
            }
            result += addIndent("}", indent)
        }
        result += "\n"
        return result
    }

    static def String addIndent(String message, int indent) {
        indent = indent < 0 ? 0 : indent
        return " "*(4*indent) + message
    }

    class TemplateNode {
        def String name;
        def Map attributes = [:]
        def List<TemplateNode> children = []
    }
}
