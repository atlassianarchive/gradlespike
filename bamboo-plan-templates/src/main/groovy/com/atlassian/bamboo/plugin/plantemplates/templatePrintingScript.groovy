package com.atlassian.bamboo.plugin.plantemplates


def template = new File('template.txt').text
def shortcutFileContents = new File('shortcuts.txt').text

ShortcutEvaluator shortcutBuilderEvaluator = new ShortcutEvaluator()
List<TemplateShortcut> shortcuts = shortcutBuilderEvaluator.parseShortcuts(shortcutFileContents)

DslEvaluator planBuilderEvaluator = DslEvaluator.builder().withShortcuts(shortcuts);

Map expected = planBuilderEvaluator.evaluate(template);

TemplatePrinter printer = new TemplatePrinter()
String printedDsl = printer.buildDslFromMap(expected)

println printedDsl
