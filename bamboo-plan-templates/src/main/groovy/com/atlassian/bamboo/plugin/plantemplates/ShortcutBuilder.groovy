package com.atlassian.bamboo.plugin.plantemplates

class ShortcutBuilder extends BuilderSupport {

    public List<Map> result = []
    def debug = false 
    def currentShortcut = null;

    @Override
    protected void setParent(Object parent, Object child) {}

    @Override
    protected Object createNode(Object name){
        return createNode(name, [:]);
    }

    @Override
    protected Object createNode(Object name, Object value){
        //This method is called when parsing a shortcut with a list as the second item, rather than a map
        //That is, a shortcut definition with arguments
        if(debug) {println "createNode(Name: ${name}, Object: ${value})"}
        if(currentShortcut == null) {
            currentShortcut = name
        }
        return [name:name,
                attributes: null,
                args: value,
                value: "",
                children:[]]
    }

    @Override
    protected Object createNode(Object name, Map attributes) {
        //This method is called when parsing a regular dsl statement for the form (name map closure)
        if(debug) {println "createNode(Name: ${name}, Map: ${attributes})"}
        if(currentShortcut == null) {
            currentShortcut = name
        }
        
        
        attributes.each { key, value ->
            if (! (value instanceof String)) {
                throw new IllegalArgumentException("Inside shortcut definition: '${currentShortcut}', error while processing node: '${name}', found attribute: '${key}' with non-string value")
            }
        }
        
        return [name:name,
                attributes: {
                    def result = "("
                    def num = 0
                    attributes.each {key, value ->
                        def quote = "'"
                        def replaceValue = value.replace(/'/, /\'/).replace(/"/, /\"/)
                        if (value.startsWith('\n')) {
                            quote = "'''"
                        }
                        result = result + ","*num
                        result = result + key + ":" + quote + replaceValue + quote;
                        num = 1
                    }
                    result = result + ")"
                    return result
                },
                value: "",
                args:  null,
                children:[]]
    }

    @Override
    protected Object createNode(Object name, Map attributes, Object value) {
    }

    @Override
    protected void nodeCompleted(Object parent, Object node) {

        if (parent != null) {
            if (node?.children?.size > 0) {
                node.value = node.value + " {\n"
                processChildren(node)
                node.value = node.value + "\n}"
            }
            if(node != null){
                node.value = node.name + node.attributes.call() + node.value
                parent.children.add(node)
            }
        }
        else {
            processChildren(node)
            if(!node.value){
                throw new IllegalArgumentException("Inside shortcut definition: '${currentShortcut}', shortcut has no body")
            }
            def shortcut = [name:node.name, value: node.value, args: node.args]
            result.add(shortcut)
            currentShortcut = null
        }
        if(debug) {println "node completed parent: ${parent}, node: ${node}"}
    }

    protected void processChildren(node) {
        node.children.each { child ->
            node.value = node.value + child.value + "\n"
        }
    }
}
