package com.atlassian.bamboo.plugin.plantemplates

class ShortcutExpander {

    private List<TemplateShortcut> shortcuts;

    ShortcutExpander(List<TemplateShortcut> shortcuts) {
        this.shortcuts = shortcuts;
    }

    public Object handleShortcuts(Object name, Map attributes) {
        def result = null;
        //TODO: assumes that only ever matches one shortcut
        shortcuts.each {shortcut ->
            if (shortcut.matches(name)) {
                result = shortcut.apply(attributes, shortcuts)
            }
        }
        return result
    }
}
