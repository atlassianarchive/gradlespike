package com.atlassian.bamboo.plugin.plantemplates

class DslEvaluator {

    protected List<TemplateShortcut> shortcuts
    protected List<Map> validationRules
    protected TemplatePrinter templatePrinter = new TemplatePrinter()
    
    private DslEvaluator(List<TemplateShortcut> shortcuts, List<Map> rules)
    {
        this.shortcuts = shortcuts
        this.validationRules = rules
    }
  
    def Map evaluate(String dslBody) {
        PlanBuilder builder = new PlanBuilder(new ShortcutExpander(shortcuts))
        parse(builder, dslBody)
        return builder.result
    }
    
    def Map validate(String dslBody) {
        PlanBuilderValidator builder = new PlanBuilderValidator(new ShortcutExpander(shortcuts), validationRules)
        parse(builder, dslBody)
        return ["errors": builder.errors, "warnings": builder.warnings]
    }
    
    def Map run(String dslBody) {
        def dslMap = evaluate(dslBody)
        def validation = []
        try{
            // validate the expandedDsl, but failback to the original dsl if an exception it's raised
            def expandedDsl = templatePrinter.buildDslFromMap(dslMap)
            validation = validate(expandedDsl)
        }catch(Exception e){
            // TODO log the error to improve possible error on the printer
            validation = validate(dslBody)
        }
        return [dslMap: dslMap , errors: validation.errors, warnings: validation.warnings]
    }
    
    def String expandDsl(String dslBody){
        def dslMap = evaluate(dslBody)
        try{
            return templatePrinter.buildDslFromMap(dslMap)
        }catch(Exception e){
            return "Error trying to print the DSL, please check the syntax"
        }
    }

	private parse(PlanBuilder builder, String dslBody) {
		BuilderBinding binding = new BuilderBinding()
		binding.builder = builder

        def dslScript = new GroovyShell().parse(dslBody)
		dslScript.binding = binding
		dslScript.run()
	}

    
    public static Builder builder(){
        return new Builder();
    }
    
    public static class Builder {
         protected List<TemplateShortcut> shortcuts = []
         protected List<Map> validationRules = []
        
         public Builder withShortcuts(List<TemplateShortcut> shortcuts){
             this.shortcuts = shortcuts
             return this
         }
         
         public Builder withValidationRules(List<Map> validationRules){
             this.validationRules = validationRules
             return this
         }
         
         public DslEvaluator build(){
             return new DslEvaluator(shortcuts, validationRules)
         }
    } 
    
}