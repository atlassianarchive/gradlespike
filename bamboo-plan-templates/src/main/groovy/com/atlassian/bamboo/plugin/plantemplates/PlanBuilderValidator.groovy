package com.atlassian.bamboo.plugin.plantemplates

import java.util.List;
import java.util.Map;

import com.google.common.base.Strings;

class PlanBuilderValidator extends PlanBuilder
{
    def debug = false
    
    List<String> errors = []
    List<String> warnings = []
    List<Map> rules = []
        
    TemplatePrinter printer = new TemplatePrinter()
    
    public PlanBuilderValidator(ShortcutExpander shortcutExpander, List<Map> rules)
    {
        super(shortcutExpander)
        this.rules = rules
    }
    
    @Override
    protected Object postNodeCompletion(Object parent, Object node)
    {
        if(debug) {println "postNodeCompletion\n Node: ${node}\n Parent: ${parent}\n"}

        def rulesNodes = rules.collect{it.node}.unique()
        def nodes = node.collect{it.key}.unique()

        if (!rulesNodes.containsAll(nodes)){
            warnings.add("There is no rule defined for node(s) ${nodes - rulesNodes}. They will probably be ignored.")
        }

        rules.each { rule ->
            def nodeContent = node[rule.node]
            if (nodeContent !=null){
                applyRule(rule, parent, nodeContent, node)
            } 
        }
        return super.postNodeCompletion(parent, node);
    }
    
    def applyRule(rule, parent, nodeContent, node){
        applyRequired(rule, parent, nodeContent, node)
        applyParent(rule, parent, nodeContent, node)
        applyRoot(rule, parent, nodeContent, node)
        applyAllowValues(rule, parent, nodeContent, node)
        applyAllowAttributes(rule, parent, nodeContent, node)
        applyAttributeMatches(rule, parent, nodeContent, node)
        applyConditionalAllow(rule, parent, nodeContent, node)
    }
    
    def applyParent(rule, parent, nodeContent, node){
        if(!rule.parent){
            return
        }
        if(parent == null){
            errors.add("'${rule.node}' need to be a child node of '${rule.parent}' for: " + printer.buildDslFromMap(node))
            return
        }
        
        if(rule.parent instanceof String) {
            if(parent[rule.parent] == null){
                errors.add("'${rule.node}' need to be a child node of '${rule.parent}' for: " + printer.buildDslFromMap(node))
                return
            }
        }
        else if(rule.parent instanceof List) {
            for (String nodeName : rule.parent) {
                if (parent[nodeName] != null) {
                    return
                }
            }
            errors.add("'${rule.node}' need to be a child node of one of '${rule.parent}' for: " + printer.buildDslFromMap(node))
            return
        }
    }
    
    def applyRequired(rule, parent, nodeContent, node){
        rule.required.each { attr ->
            if(!nodeContent[attr])
                errors.add("'${rule.node}' required a non empty attribute or child node '${attr}' for: " + printer.buildDslFromMap(node))
        }
    }
    
    def applyRoot(rule, parent, nodeContent, node){
        if(rule.root){
            if(parent != null){
                errors.add("'${rule.node}' need to be a root node for: " + printer.buildDslFromMap(node))
                return
            }
        }
    }
    
    def applyAllowValues(rule, parent, nodeContent, node){
        if(!rule.allowValues)
            return
        
        rule.allowValues.each { attr, values ->
            if(nodeContent[attr]!= null && !(nodeContent[attr] in values)){
                errors.add("'${rule.node}:${attr}' need to be one of these ${values} for: " + printer.buildDslFromMap(node))
            } 
        }
        
    }
    
    def applyAllowAttributes(rule, parent, nodeContent, node) {
        if (!rule.allowAttributes)
            return
            
        nodeContent.each { attribute, value ->
            if ((value instanceof String) && !(attribute in rule.allowAttributes)) {
                errors.add("'${rule.node}:${attribute}' is not an allowed attribute. Possible attributes are ${rule.allowAttributes} for: " + printer.buildDslFromMap(node))
            }
        }
    }

    def applyAttributeMatches(rule, parent, nodeContent, node) {
        if (!rule.attributeMatches)
            return

        nodeContent.each { attribute, value ->
            def pattern = rule.attributeMatches[attribute]
            if ((pattern != null) && (value instanceof String) && !(value ==~ pattern)) {
                errors.add("'${rule.node}:${attribute}' did not match the pattern of allowed values. The value must match the pattern ${pattern} for: " + printer.buildDslFromMap(node))
            }
        }
    }
    
    def applyConditionalAllow(rule, parent, nodeContent, node) {
        if (!rule.conditionalAllow)
            return

        def allowedAttributes =  rule.conditionalAllow.findAll{
            nodeContent[it.discriminator] == it.value
        }.collectMany{it.attributes}.unique()


        nodeContent.each { attribute, value ->
            if ((value instanceof String) && !(attribute in allowedAttributes)) {
                errors.add("'${rule.node}:${attribute}' is not an allowed attribute. Possible conditional attributes are ${allowedAttributes} for: " + printer.buildDslFromMap(node))
            }
        }
    }
    

}
