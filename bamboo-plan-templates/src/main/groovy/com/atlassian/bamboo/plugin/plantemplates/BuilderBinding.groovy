package com.atlassian.bamboo.plugin.plantemplates

class BuilderBinding extends Binding{

    def builder

    Object getVariable(String name) {
        return { Object... args -> builder.invokeMethod(name,args) }
    }
}
