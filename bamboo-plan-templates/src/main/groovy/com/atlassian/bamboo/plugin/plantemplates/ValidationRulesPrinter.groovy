package com.atlassian.bamboo.plugin.plantemplates

class ValidationRulesPrinter {

    String retrievePrettyPrintValidationRules(rules) {
        def nodesByParent = calculateNodesByParent(rules)
        def rootNodesRules = rules.findAll{it.root == true}

        return rootNodesRules.collect{ printableVersionRule (it, 0, nodesByParent) }.join("\n")
    }

    private String printableVersionRule (rule, Number depthLevel, nodesByParent){
        def childNodeRules = nodesByParent.get(rule.node)

        // Attributes
        def expandedRule = "".padLeft(depthLevel) + rule.node + "("
        expandedRule += rule.allowAttributes.collect().join(", ")
        expandedRule += ")"

        // child nodes
        if (childNodeRules != null && !childNodeRules.isEmpty()){
            expandedRule += "{\n"
            expandedRule += childNodeRules.collect{ printableVersionRule (it, depthLevel+1, nodesByParent) }.join("")
            expandedRule += "}".padLeft(depthLevel+1)
        }
        expandedRule += "\n"
        return expandedRule
    }

    private Map calculateNodesByParent(rules) {
        def nodesByParent = [:].withDefault {[]}

        rules.each { rule ->
            if (rule.parent instanceof String) {
                nodesByParent[rule.parent].add(rule)
            } else {
                rule.parent.each { nodesByParent[it].add(rule) }
            }
        }
        return nodesByParent
    }
}
