package com.atlassian.bamboo.plugin.plantemplates

public class PlanBuilder extends BuilderSupport {

    def debug = false
    def result = null
    private ShortcutExpander shortcutExpander

    PlanBuilder(ShortcutExpander shortcutExpander) {
        this.shortcutExpander = shortcutExpander
    }
    
    protected void setParent(Object parent, Object child) {}

    protected Object createNode(Object name){
        return createNode(name, [:]);
    }
    protected Object createNode(Object name, Object value){
        throw new Exception("Invalid object found with name: ${name} and value: '${value}' and value type: '${value.class}'")
    }
    protected Object createNode(Object name, Map attributes){
        if(debug) {println "createNode(${name}, ${attributes})"}
        
        attributes.each { key, value ->
            if (! (value instanceof String)) {
                throw new IllegalArgumentException("Error while processing node: '${name}', found attribute: '${key}' with non-string value, attributes= ${attributes}")
            }
        }

        def handled = shortcutExpander.handleShortcuts(name, attributes)
        if (handled != null) {
            if(debug) {println "* shortcuts handled: ${handled}"}            
            return handled
        }

        def result = [:]
        result[name] = attributes
        return result
    }
    protected Object createNode(Object name, Map attributes, Object value){
        throw new Exception()
    }
    protected void nodeCompleted(Object parent, Object node) {
        if(debug) {println "+ node completed parent: ${parent}, node: ${node}"}        
        //Rolls up a node into it's parent. The end result should look like:
        // [parent.type : [parent.attribute1:value, parent.attribute2:value ... node.type: [node.attribute1:value ...]]
        if(parent != null) {
            node.each { entry ->
                //There should only ever be a single value in parent at this time.
                parent.each { parentEntry ->
                    addOrExtendMapEntry(parentEntry.value, entry.key, entry.value)
                }
                
            }
        } else {
            node.each { entry ->
                if (result == null) {        
                    if(node instanceof Map){                                                
                        result = [:]
                        result[entry.key] = entry.value
                    }else{
                        result = node
                    }
                }
                else {
                    addOrExtendMapEntry(result, entry.key, entry.value)
                }
                
            }
        }
        if(debug) {println "- node completed parent: ${parent}, node: ${node}"}
    }

    /**
     * Adds and entry to a map. If the key already exists, if converts the existing value to a list and append the new value
     * @param map
     * @param key
     * @param value
     */
    protected void addOrExtendMapEntry(Map map, Object key, Object value) {
        def existing = map[key]
        if (existing == null) {
            map[key] = value
        }
        else {
            if (! (existing instanceof List)) {
                map[key] = [existing]
            }

            if(value instanceof List){
                map[key].addAll(value)

            }else{
                map[key].add(value)
            }
        }
    }

    protected void prettyPrint(Map node, Integer level) {
        def prefix = "  "*level + "-"
        for(key in node.keySet()) {
            if(node[key] instanceof Map) {
                println prefix + "${key}:"
                prettyprint(node[key], level+1)
            }
            else if (node[key] instanceof List) {
                for(item in node[key]) {
                    def m = [:]
                    m[key] = item
                    prettyprint(m, level)
                }
            }
            else {
                println prefix + "${key}:${node[key]}"
            }
        }
    }

}