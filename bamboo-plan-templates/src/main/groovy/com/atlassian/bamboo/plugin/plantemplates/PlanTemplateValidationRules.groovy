package com.atlassian.bamboo.plugin.plantemplates


class PlanTemplateValidationRules
{
    public List<Map> getRules(){
        return this.rules
    }
    
    def List<Map> rules = [
        //[node: 'xxx', 
        //  parent: 'yyy', 
        //  root: true, 
        //  required: ['key', 'description'], 
        //  allowAttributes: ['name', 'key'],  OR conditionalAllow: [ [discriminator: "type", value:'maven', attributes: ['executable', 'type'] ] ]
        /// allowValues: [
        //                'type': ['one', 'two'] 
        //  ]
        //],
        [node: 'deployment',
            root: true,
            required: ['name', 'planKey', 'environment'],
            allowAttributes: ['name', 'planKey', 'description']
        ],
        [node: 'plan', 
            root: true, 
            required: ['key', 'project', 'stage'],
            attributeMatches: [key:/[0-9a-zA-Z]+/], 
            allowAttributes: ['key', 'name', 'description', 'enabled']
        ],
        [node: 'planMiscellaneous',
            parent: 'plan',
        ],
        [node: 'sandbox',
            parent: 'planMiscellaneous',
            required: ['enabled'],
            allowAttributes: ['enabled', 'automaticPromotion'],
            allowValues: [
                    'enabled': ['true', 'false'],
                    'automaticPromotion': ['true', 'false']
            ]
        ],
        [node: 'dependencies',
                parent: 'plan',
                allowAttributes: ['triggerOnlyAfterAllStagesGreen', 'automaticDependency', 'triggerForBranches', 'blockingStrategy'],
                allowValues: [
                        'blockingStrategy': ['notBlock', 'blockIfPendingBuilds', 'blockIfUnbuildChanges'],
                ],
        ],
        [node: 'childPlan',
                parent: 'dependencies',
                required: ['planKey'],
                allowAttributes: ['planKey'],
                attributeMatches: [planKey:/[0-9a-zA-Z]+-[0-9a-zA-Z]+/],
        ],
        [node: 'templateRepository',
            root: true,
            required: ['url'],
            allowAttributes: ['url']
        ],
        [node: 'project', 
            parent: 'plan', 
            required: ['key', 'name'],
            attributeMatches: [key:/[0-9a-zA-Z]+/],
            allowAttributes: ['key', 'name', 'description']
        ],
        [node: 'label', 
            parent: 'plan', 
            required: ['name'],
            allowAttributes: ['name']
        ],
        [node: 'variable', 
            parent: ['plan', 'environment'],
            required: ['key', 'value'],
            allowAttributes: ['key', 'value']
        ],
        [node: 'branchMonitoring',
            parent: 'plan',
            required: ['enabled'], // This is required because the default is false, which is non-intuitive.
            allowValues: [
                'notificationStrategy': ['NOTIFY_COMMITTERS', 'INHERIT', 'NONE']
            ], 
            allowAttributes: ['enabled', 'matchingPattern', 'timeOfInactivityInDays', 'notificationStrategy', 'remoteJiraBranchLinkingEnabled']
        ],
        [node: 'repository', 
            required: ['name'],
            parent: ['trigger', 'plan', 'task'],
            allowAttributes: ['name', 'checkoutDirectory']
        ],
        [node: 'trigger', 
            required: ['type', 'description'],
            parent: ['plan', 'environment'],
            allowValues: [
                'type': ['polling', 'push', 'daily', 'cron', 'afterSuccessfulPlan'],
                'strategy': ['periodically', 'scheduled'],
                'onlyBranchesWithChanges': ['true', 'false']
            ],
            conditionalAllow: [
                    [discriminator: "type", value:'polling', attributes: ['type', 'description', 'strategy', 'frequency', 'cronExpression']],
                    [discriminator: "type", value:'cron', attributes: ['type', 'description', 'cronExpression', 'onlyBranchesWithChanges']],
                    [discriminator: "type", value:'daily', attributes: ['type', 'description','time']],
                    [discriminator: "type", value:'push', attributes: ['type','description']],
                    [discriminator: "type", value:'afterSuccessfulPlan', attributes: ['type', 'description', 'planKey']]
            ]
        ],
    
        [node: 'server', 
            parent: 'trigger', 
            required: ['ip'],
            allowAttributes: ['ip']
        ],
        
        [node: 'stage', 
            parent: 'plan', 
            required: ['name', 'job'],
            allowAttributes: ['name', 'description', 'manual']
        ],
        
        [node: 'job', 
            parent: 'stage', 
            required: [ 'key', 'name', 'task'],
            attributeMatches: [key:/[0-9a-zA-Z]+/],
            allowAttributes: ['key', 'name', 'description', 'enabled']
        ],
        
        [node: 'requirement', 
            parent: ['job', 'task'],
            required: ['key', 'condition'], 
            allowAttributes: ['key', 'condition', 'value'], 
            allowValues: [
                'condition': ['equals', 'exists', 'matches', 'cron'] 
            ]
        ],
        [node: 'buildExpiry',
            parent: 'plan',
            allowAttributes: ['notExpire', 'buildResults', 'artifacts', 'buildLogs', 'duration', 'period', 'minimumBuildsToKeep', 'labelsToKeep'],
            allowValues: [
                'period': ['days', 'weeks', 'months'],
            ]
        ],    
        [node: 'notification',
            parent: ['environment', 'plan'],
            required: ['type', 'recipient'],
            allowValues: [
                'recipient':   [
                                "email",
                                "user",
                                "group",
                                "responsible",
                                "committers",
                                "watchers",
                                "IM",
                                "hipchat",
                                "stash",
                                "httpNotification"
                                ],
                'type':  [
                        'All Builds Completed',
                        'Change of Build Status',
                        'Failed Builds and First Successful',
                        'After X Build Failures',
                        'Comment Added',
                        'Change of Responsibilities',
                        'All Jobs Completed',
                        'Change of Job Status',
                        'Failed Jobs and First Successful',
                        'First Failed Job for Plan',
                        'Job Error',
                        'Job Hung',
                        'Job Queue Timeout',
                        'Job Queued without Capable Agents',
                        'Deployment Started and Finished',
                        'Deployment Finished',

                ]
           ],
            conditionalAllow: [
                    [discriminator: "recipient", value:'email', attributes: ['type', 'recipient', 'email']],
                    [discriminator: "recipient", value:'user', attributes: ['type', 'recipient', 'user']],
                    [discriminator: "recipient", value:'group', attributes: ['type', 'recipient', 'group']],
                    [discriminator: "recipient", value:'responsible', attributes: ['type', 'recipient', ]],
                    [discriminator: "recipient", value:'committers', attributes: ['type', 'recipient']],
                    [discriminator: "recipient", value:'watchers', attributes: ['type', 'recipient']],
                    [discriminator: "recipient", value:'IM', attributes: ['type', 'recipient', 'address']],
                    [discriminator: "recipient", value:'hipchat', attributes: ['type', 'recipient', 'room', 'apiKey', 'notify']],
                    [discriminator: "recipient", value:'stash', attributes: ['type', 'recipient']],
                    [discriminator: "recipient", value:'httpNotification', attributes: ['type', 'recipient', 'baseUrl']],
            ]
        ],
        [node: 'versioning',
            parent: 'deployment',
            required: ['version'],
            allowAttributes: ['version', 'autoIncrementNumber'],
        ],
        [node: 'variableToAutoIncrement',
            parent: 'versioning',
            required: ['key'],
            allowAttributes: ['key']
        ],
        [node: 'environment',
            parent: 'deployment',
            required: ['name', 'task'],
            allowAttributes: ['name', 'description']
        ],
        [node: 'artifactDefinition',
            parent: 'job',
            required: ['name', 'pattern'],
            allowAttributes: ['name', 'location', 'pattern', 'shared']
        ],
        [node: 'artifactSubscription',
            parent: 'job',
            required: ['name'],
            allowAttributes: ['name', 'destination']
        ],
        [node: 'miscellaneousConfiguration',
                parent: 'job',
                allowAttributes: ['cleanupWorkdirAfterBuild'],
                allowValues: [
                        'cleanupWorkdirAfterBuild': [ 'false', 'true' ]
                ],
                allowAttributes: ['cleanupWorkdirAfterBuild']
        ],
        [node: 'patternMatchLabelling',
                parent: 'miscellaneousConfiguration',
                required: [ 'regex', 'labels' ],
                allowAttributes: ['regex', 'labels']
        ],
        [node: 'deployer',
            root: true,
            required: [ 'planKey'],
            allowAttributes: ['planKey']
        ],
        [node: 'task',
            parent: ['job', 'environment'],
            required: ['type', 'description'],
            allowValues: [
                'type': ['script', 'maven2', 'maven3', 'checkout', 'deployBambooPlugin','deployConfluencePlugin',
                        'deployJiraPlugin', 'jUnitParser', 'nodejs', 'npm', 'brokerUrlDiscovery', 'cleanWorkingDirectory',
                        'artifactDownload', 'addRequirement', 'installOnDemand', 'provisionUnicorn', 'promoteManifesto',
                        'promoteProductVersion', 'promoteManifestoArtifacts', 'removeManifestoArtifacts',
                        'stopUnicorn', 'startUnicorn', 'execOnUnicorn', 'updateLicense', 'raiseDeploymentTicket', 
                        'setupUnicorn', 'copyToUnicorn', 'injectBambooVariables', 'ant', 'mavenVersionVariableUpdater',
                        'deployPlugin'],
                'taskDisabled': ['true', 'false'],
                'enableTrafficLogging': ['true', 'false'],
                'certificateCheckDisabled': ['true', 'false'],
                'includeGlobals': ['true', 'false'],
                'useAtlassianId': ['true', 'false'],
                'useAtlassianIdWebSudo':['true', 'false'],
                'forcePromote':['true','false'],
                'product':['jira','confluence','bamboo','fecru', 'stash'],
            ],
            attributeMatches: [pluginInstallationTimeout:/[0-9]*/],
            conditionalAllow: [
                    [discriminator: "type", value:'script', attributes: ['type', 'description', 'final', 'environmentVariables', 'taskDisabled',
                            'scriptBody', 'argument', 'workingSubDirectory', 'script']],
                    [discriminator: "type", value:'maven2', attributes: ['type', 'description', 'final', 'environmentVariables', 'taskDisabled',
                            'workingSubDirectory', 'projectFile', 'buildJdk', 'mavenExecutable', 'hasTests', 'testDirectory', 'useMavenReturnCode',
                            'goal']],
                    [discriminator: "type", value:'maven3', attributes: ['type', 'description', 'final', 'environmentVariables', 'taskDisabled',
                            'workingSubDirectory', 'projectFile', 'buildJdk', 'mavenExecutable', 'hasTests', 'testDirectory', 'useMavenReturnCode',
                            'goal']],
                    [discriminator: "type", value:'checkout', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'cleanCheckout']],
                    [discriminator: "type", value:'deployBambooPlugin', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'url', 'username', 'password', 'artifact', 'passwordVariable', 'runOnBranch',
                            'enableTrafficLogging', 'certificateCheckDisabled', 'pluginInstallationTimeout',
                            'useAtlassianId', 'atlassianIdBaseURL', 'atlassianIdUsername', 'atlassianIdPassword',
                            'atlassianIdPasswordVariable', 'useAtlassianIdWebSudo', 'atlassianIdAppName']],
                    [discriminator: "type", value:'deployConfluencePlugin', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'url', 'username', 'password', 'artifact', 'passwordVariable', 'runOnBranch',
                            'enableTrafficLogging', 'certificateCheckDisabled', 'pluginInstallationTimeout',
                            'useAtlassianId', 'atlassianIdBaseURL', 'atlassianIdUsername', 'atlassianIdPassword',
                            'atlassianIdPasswordVariable', 'useAtlassianIdWebSudo', 'atlassianIdAppName']],
                    [discriminator: "type", value:'deployJiraPlugin', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'url', 'username', 'password', 'artifact', 'passwordVariable', 'runOnBranch',
                            'enableTrafficLogging', 'certificateCheckDisabled', 'pluginInstallationTimeout',
                            'useAtlassianId', 'atlassianIdBaseURL', 'atlassianIdUsername', 'atlassianIdPassword',
                            'atlassianIdPasswordVariable', 'useAtlassianIdWebSudo', 'atlassianIdAppName']],
                    [discriminator: "type", value:'deployPlugin', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'url', 'username', 'password', 'artifact', 'passwordVariable', 'runOnBranch',
                            'enableTrafficLogging', 'certificateCheckDisabled', 'pluginInstallationTimeout',
                            'useAtlassianId', 'atlassianIdBaseURL', 'atlassianIdUsername', 'atlassianIdPassword',
                            'atlassianIdPasswordVariable', 'useAtlassianIdWebSudo', 'atlassianIdAppName', 'product']],
                    [discriminator: "type", value:'jUnitParser', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'resultsDirectory']],
                    [discriminator: "type", value:'nodejs', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'executable', 'script', 'workingSubDirectory', 'arguments']],
                    [discriminator: "type", value:'npm', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'executable', 'command', 'workingSubDirectory']],
                    [discriminator: "type", value:'brokerUrlDiscovery', attributes: ['type', 'description', 'final']],
                    [discriminator: "type", value:'cleanWorkingDirectory', attributes: ['type', 'description', 'final', 'taskDisabled']],
                    [discriminator: "type", value:'artifactDownload', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'planKey']],
                    [discriminator: "type", value:'addRequirement', attributes: ['type', 'description', 'final', 'taskDisabled']],
                    [discriminator: "type", value:'installOnDemand', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'unicornInstance', 'version', 'cleanBeforeInstall', 'localYaml']],
                    [discriminator: "type", value:'provisionUnicorn', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'unicornInstance', 'version', 'username', 'email', 'password', 'gappsDomain']],
                    [discriminator: "type", value:'promoteManifesto', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'artifactName', 'artifactVersion', 'application', 'username', 'passwordVariable', 'deploymentEnvironment']],
                    [discriminator: "type", value:'promoteProductVersion', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'productName', 'productVersion', 'username', 'passwordVariable', 'deploymentEnvironment']],
                    [discriminator: "type", value:'promoteManifestoArtifacts', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'promotionSet', 'preconditionsSet', 'forcePromote', 'username', 'passwordVariable', 'deploymentEnvironment']],
                    [discriminator: "type", value:'removeManifestoArtifacts', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'removalSet', 'preconditionsSet', 'username', 'passwordVariable', 'deploymentEnvironment']],
                    [discriminator: "type", value:'stopUnicorn', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'unicornInstance', 'mode']],
                    [discriminator: "type", value:'startUnicorn', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'unicornInstance', 'halTimeout']],
                    [discriminator: "type", value:'execOnUnicorn', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'unicornInstance', 'scriptBody', 'arguments', 'scriptFile', 'scriptLocation']],
                    [discriminator: "type", value:'updateLicense', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'unicornInstance', 'license']],
                    [discriminator: "type", value:'raiseDeploymentTicket', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'unicornInstance', 'artifactName', 'artifactVersion', 'artifactKey', 'pluginKey', 'groupId',
                            'deploymentEnvironment', 'sdogUsername', 'sdogPassword']],
                    [discriminator: "type", value:'setupUnicorn', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'gappsDomain', 'unicornInstance', 'version', 'localYaml', 'source', 'destination', 'license', 'halTimeout',
                            'email', 'username', 'password', 'cleanBeforeInstall']],
                    [discriminator: "type", value:'copyToUnicorn', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'unicornInstance', 'pathSource', 'destination', 'artifactSource', 'sourceType']],
                    [discriminator: "type", value:'injectBambooVariables', attributes: ['type', 'description', 'final', 'taskDisabled',
                            'filePath']],
                    [discriminator: "type", value:'ant', attributes: ['type', 'description', 'final', 'environmentVariables', 'taskDisabled',
                            'workingSubDirectory', 'buildFile', 'buildJdk', 'executable', 'hasTests', 'testResultsDirectory', 'target']],
                    [discriminator: "type", value: 'mavenVersionVariableUpdater', attributes: ['type', 'description', 'taskDisabled', 'final',
                            'variablePattern', 'versionPattern', 'includeGlobals']],
            ]
        ],
        [node: 'artifact',
            parent: 'task',
            required: ['name', 'localPath'],
            allowAttributes: ['name', 'localPath']
        ]
    ]
}
