package it.com.atlassian.bamboo.plugins;

import com.atlassian.bamboo.plugin.plantemplates.PerformPlanChanges;
import com.atlassian.bamboo.plugin.plantemplates.PlanTemplateService;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.sal.api.ApplicationProperties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(AtlassianPluginsTestRunner.class)
public class MyComponentWiredTest
{
    private final ApplicationProperties applicationProperties;
    private final PlanTemplateService planTemplateService;

    public MyComponentWiredTest(ApplicationProperties applicationProperties,PlanTemplateService planTemplateService)
    {
        this.applicationProperties = applicationProperties;
        this.planTemplateService = planTemplateService;
    }

    @Test
    public void testMyName()
    {
//        assertEquals("names do not match!", "performPlanChanges:" + applicationProperties.getDisplayName(),myPluginComponent.);
        System.out.println(applicationProperties.getDisplayName());
        assertNotNull(applicationProperties.getDisplayName());
        assertNotNull(planTemplateService);
    }
}