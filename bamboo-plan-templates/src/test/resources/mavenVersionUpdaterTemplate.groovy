templateRepository(url: "http://url.to.the.repository/template.groovy")

plan(key:"SAMPLE", name:"Sample Plan", description:"Plan created from plan templates") {
    project(key: "TSTPLANTEMPLATES", name:"testing plan templates", description:"a project description")
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: "job11Key", description: "basic" ){
            task(type:"mavenVersionVariableUpdater", description:"maven version update", versionPattern: '.updateVersion', includeGlobals:'true', variablePattern: '.variablePattern')
        }
    }
}
