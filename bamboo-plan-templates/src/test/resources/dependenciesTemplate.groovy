
plan(key:'REST', name:'rest test', description:'A sensible description goes here...') {
    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: 'job11Key', description: 'hellojob' ){
            task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
        }        
    }
}

plan(key:'REST2', name:'rest test2', description:'A sensible description goes here...') {
    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')
    
    dependencies(triggerOnlyAfterAllStagesGreen: 'true', automaticDependency: 'true', triggerForBranches: 'true', blockingStrategy: 'notBlock'){
        childPlan(planKey: 'PERSONAL-REST')
    }
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: 'job11Key', description: 'hellojob' ){
            task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
        }
    }
}

plan(key:'REST3', name:'rest test3', description:'A sensible description goes here...') {
    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')
    
    //  blockingStrategy: 'notBlock' 'blockIfPendingBuilds' 'blockIfUnbuildChanges'
    dependencies(triggerOnlyAfterAllStagesGreen: 'true', automaticDependency: 'true', triggerForBranches: 'true', blockingStrategy: 'blockIfPendingBuilds'){
        childPlan(planKey: 'PERSONAL-REST')
        childPlan(planKey: 'PERSONAL-REST2')
    }
    
    
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: 'job11Key', description: 'hellojob' ){
            task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
        }
    }
}