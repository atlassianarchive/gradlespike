plan(key:'BAMBOONICORN', name:'Bamboonicorn Plugin', description:'Smoke test for Bamboonicorn Plan Templates')
{
    project(key:'UPGRADE', name:'Smoke Tests - Bamboo Upgrade Prerequisites', description:'')

    repository(name:'bamboonicorn')

    planVariable(key:'unicorn.instance2', value:'bamcorn-pt-test-instance.jira-dev.com')

    stage(name:'Default Stage', description:'')
    {
        job(name:'Unicorn Tasks (PT)', key:'UTP', enabled:'true')
        {
            task(type:'provisionUnicorn', description:'Create a unicorn instance for icebat-dev.79.0',
                    unicornInstance:'${bamboo.unicorn.instance2}', version:'icebat-dev.79.0',
                    email:'awang@atlassian.com', username:'admin', password:'', gappsDomain:'')
        }
    }
}