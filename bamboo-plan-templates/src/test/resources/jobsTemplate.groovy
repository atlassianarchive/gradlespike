plan(key:'REST', name:'rest test', description:'A sensible description goes here...') {
    label(name:"label1")
    label(name:"label2")
    variable(key:"key1", value:"value1")
    variable(key:"key2", value:"value2")
    repository(name:'test')
    repository(name:'test2')
    trigger(description:'trigger1', type:'polling', strategy:'periodically', frequency:'180'){
        repository(name:'test')
        repository(name:'test2')
    }
    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: 'job11Key', description: 'hellojob' ){
            requirement(key:"os", condition:"equals", value:"windows")
            requirement(key:"awesome", condition:"exists")
            requirement(key:"mssql", condition:"matches", value:"200.")

            task(type:'checkout', description:'task05'){
                repository(name:'test', checkoutDirectory:'./test1directory')
                repository(name:'test2', checkoutDirectory:'./test2directory')
            }
            task(type:'script', description:'task1', scriptBody:'''
                    echo hello1o
                    echo hello2oo
                    echo hello3ooo
                '''
            )
            task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
            task(type:'maven3', description:'task3Maven', goal:'clean install', hasTests:'true', useMavenReturnCode:'true')
            task(type:'maven3', description:'task3MavenWithJdk', goal:'clean install', buildJdk:'jdk1.7', mavenExecutable:'maven 3')
            task(type:'jUnitParser', description:'parse cucumber results', final:'true', resultsDirectory:'**/TEST-*.xml')
        }
        job(name: "job2", key: 'job2Key', description: 'job21description' ){
            requirement(key:"mssql", condition:"matches", value:"200.")
            task(type:'script', description:'task1', scriptBody:'echo hello1oooooo')
            task(type:'script', description:'task2', scriptBody:'echo h2')
            task(type:'maven2', description:'task2Maven', goal:'clean install', hasTests:'true', useMavenReturnCode:'true')
        }
        
    }
}
