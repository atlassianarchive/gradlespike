

plan(key:'TEMPLATETEST', name:'Fedora15 OpenLDAP', description:'A sensible description goes here...') {
    repository(name:'test')
    repository(name:'test2')
    trigger(description:'trigger1', type:'polling', strategy:'periodically', frequency:'180'){
        //repository(name:'test')
        repository(name:'test2')
    }
    trigger(description:'trigger2', type:'polling', strategy:'scheduled', cronExpression:'0 0 0 ? * *'){
        repository(name:'test')
        repository(name:'test2')
    }
    trigger(description:'trigger3', type:'push'){
        repository(name:'test')
        repository(name:'test2')
        server(ip:'127.0.0.1') 
        server(ip:'127.0.0.3')
    }
    trigger(description:'trigger4', type:'daily', time:'17:30'){
        planGreenCondition(planKey:'some-key')
        planGreenCondition(planKey:'blah-blah')
    }
    trigger(description:'trigger5', type:'cron', cronExpression:'8 0 0 ? * *')
    trigger(description:'trigger with branch changes', type:'cron', cronExpression:'1 0 0 ? * *', onlyBranchesWithChanges: 'true')
    trigger(description:'trigger6', type:'daily', time:'17:30')
    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: 'job11Key', description: 'job1description' ){
            task(type:'checkout', description:'task05'){
                repository(name:'test', checkoutDirectory:'./test1directory')
                repository(name:'test2', checkoutDirectory:'./test2directory')
            }
        }
    }
}
