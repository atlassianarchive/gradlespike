def projectKey = "PERFAGENTSTEST"
def projectName = "Agents Performance Test"
def projectDescription = "Agents Performance Test"

// plugin to install

// Dummy plugin https://maven.atlassian.com/public/com/atlassian/bamboo/plugin/bamboo-agents-performance-test-plugin/1.4/bamboo-agents-performance-test-plugin-1.4.jar


plan(key:"BIGPLUGIN", name:"install a big plugin", description:"") {
    project(key: projectKey, name: projectName, description: projectDescription)
    label(name:"plan-templates")
        
    stage(name:"Download", description: "Download", manual: "false"){
        job(name: "Download", key: "DW", description: "" ){
            artifactDefinition(name:'plugin', location:'./', pattern:'*.jar', shared:'true')

            task(type:"script", description:"download plugin", argument: "", scriptBody:'''
                wget https://maven.atlassian.com/public/com/atlassian/bamboo/plugin/bamboo-agents-performance-test-plugin/1.4/bamboo-agents-performance-test-plugin-1.4.jar                        
            ''')

        }
    }
}


deployment(planKey:"PERFAGENTSTEST-BIGPLUGIN", name: "deploy big plugin", description: "a better deployment project"){
        environment(name:"localhost", description:"") {
            task(type:'cleanWorkingDirectory', description:'clean')
            task(type:'artifactDownload', planKey: 'PERFAGENTSTEST-BIGPLUGIN', description: 'description') {
                artifact(name:'plugin', localPath:'./plugin')
            }
            
            task(type:'deployBambooPlugin', description:'Upload the plugin', artifact:'plugin', url:'http://localhost:6990/bamboo/', username:'admin', password:'admin', enableTrafficLogging: 'true', certificateCheckDisabled: 'true', pluginInstallationTimeout: '700')
        }
}