plan(key:'BAMBOONICORN', name:'Bamboonicorn Plugin', description:'Smoke test for Bamboonicorn Plan Templates')
{
    project(key:'UPGRADE', name:'Smoke Tests - Bamboo Upgrade Prerequisites', description:'')

    repository(name:'bamboonicorn')

    planVariable(key:'unicorn.instance2', value:'bamcorn-pt-test-instance.jira-dev.com')

    stage(name:'Default Stage', description:'')
    {
        job(name:'Unicorn Tasks (PT)', key:'UTP', enabled:'true')
        {
            task(type:'raiseDeploymentTicket', description:'Raising a deployment ticket to OnDemand', artifactName:'fake',
                    artifactVersion:'1.0', artifactKey:'fake-key', pluginKey:'plugin:key', groupId:'groups',
                    deploymentEnvironment:'DOG', sdogUsername:'sdog-user', sdogPassword:'sdog-password')
        }
    }
}