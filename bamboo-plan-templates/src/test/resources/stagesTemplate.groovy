plan(key:'TEMPLATETEST', name:'Fedora15 OpenLDAP', description:'A sensible description goes here...') {
    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')
    stage(name:"stage1", description: "stage1Description", manual: "true")   {
        job(name: "job1", key: 'job1Key', description: 'hellojob' ){
            task(type:'script', description:'task1', scriptBody:'echo hello1o')
        }
    }
    stage(name:"stage2.3", description: "stage2.2Description", manual: "true")  {
        job(name: "job2", key: 'job2Key', description: 'another job' ){
            task(type:'script', description:'task2', scriptBody:'echo hello1o')
        }
    }
    
}
