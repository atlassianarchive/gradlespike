templateRepository(url: "http://url.to.the.repository/template.groovy")

plan(key:"SAMPLE", name:"Sample Plan", description:"Plan created from plan templates") {
    project(key: "TSTPLANTEMPLATES", name:"testing plan templates", description:"a project description")
    planMiscellaneous(){
        sandbox(enabled:"true", automaticPromotion:"false")
    }
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: "job11Key", description: "basic" ){
            task(type:"script", description:"description", scriptBody:"echo hello")
        }
    }
}
