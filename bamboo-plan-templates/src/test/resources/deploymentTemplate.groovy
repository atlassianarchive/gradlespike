

plan(key:'REST', name:'rest test', description:'A sensible description goes here...') {
    label(name:"label1")
    label(name:"label2")
    variable(key:"key1", value:"value1")
    variable(key:"variable1", value:"1")
    variable(key:"variable2", value:"22")
    
    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: 'job11Key', description: 'hellojob' ){
            requirement(key:"awesome", condition:"exists")
            artifactDefinition(name:'cargo logs', location:'./logs', pattern:'*.txt', shared:'true')
            artifactDefinition(name:'test2 logs', location:'./logs2', pattern:'*.txt', shared:'true')
            task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
        }
        
        job(name: "job2", key: 'job21Key', description: 'hellojo2b' ){
            task(type:'artifactDownload', description: 'description', planKey: 'PERSONAL-REST') {
                artifact(name:'cargo logs', localPath:'./logs')
                artifact(name:'test2 logs', localPath:'./logs2')
            }
        }
    }
}


deployment(planKey:"PERSONAL-REST", name: "deploymentTest", description: "a better deployment project"){

    // permissions -> leave the defaults
    
    versioning(version:'release-1-${bamboo.variable1}-${bamboo.variable2}', autoIncrementNumber: 'true') {
        variableToAutoIncrement(key: 'variable1')
        variableToAutoIncrement(key: 'variable2')
    }
    
	environment(name:"Staging", description:"") {
		
        trigger(description:'triggerCron', type:'cron', cronExpression:'8 0 0 ? * *')
        trigger(description:'triggerAfter1', type:'afterSuccessfulPlan', planKey: 'PERSONAL-REST')

        variable(key:"key1", value:"value1")
        variable(key:"key2", value:"value2")
        
		
		// permissions (not implement)
		
		// agents  (not implement)
   
        task(type:'cleanWorkingDirectory', description:'clean')
        
        task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
        
        task(type:'artifactDownload', planKey: 'PERSONAL-REST', description: 'description') {
            artifact(name:'cargo logs', localPath:'./logs')
            //artifact(name:'test2 logs', localPath:'./logs2')
            
        }
        
        task(type:'addRequirement', description:'description') {
            requirement(key:"req_key", condition:"equals", value:"blah blah")
        }
        
		// notifications
        
        notification(type:'Deployment Started and Finished', recipient:'email', email:'test@test.com')
        notification(type:'Deployment Finished', recipient:'email', email:'test2@test.com')
        
		
				  
		
	}
	environment(name:"QA", description:"") {
        task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
    }
	environment(name:"Production", description:"") {
        trigger(description:'triggerAfter', type:'afterSuccessfulPlan', planKey: 'PERSONAL-REST')
        variable(key:"key3", value:"value3")
        
        task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
        
	
	}
			
}


deployment(planKey:"PERSONAL-REST", name: "deploymentTest2", description: "a sample for deployments 2"){
    environment(name:"QA", description:"") {
        task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
    }
}
