plan(key: 'T1ALLBRANCH', name: 'My test - All Branches', description: 'JIRA 5.2, 6.2-SNAPSHOT. All branches.') {
    label(name: 'plan-templates')
    project(key: 'GH', name: 'GreenHopper')

    repository(name: 'test')

    trigger(description: 'gh repo trigger', type: 'polling', strategy: 'periodically', frequency: '180') {
        repository(name: 'test')
    }

    branchMonitoring(enabled: 'true', timeOfInactivityInDays: '30', notificationStrategy: 'INHERIT', remoteJiraBranchLinkingEnabled: 'true')

   
    stage(name: 'Build and Unit Testsss', description: 'Builds plugin artifacts and runs unit tests', manual: 'false') {
        
        job(name: 'Build aJIRA 1111111', key: 'JOB1', description: 'Buildssss the plugin against JIRA #jiraVersionNumber and runs unit tests') {
            task(type: 'checkout', description: 'Ch2eckout greenhopper') {
                repository(name: 'test', cleanCheckout: 'true')
            }
            task(type:'script', description:'task1', scriptBody:'''
                        echo hello1o
                        echo hello2oo
                        echo hello3ooo
                    '''
                )
            task(type:'script', description:'task2', scriptBody:'''
                        echo hello1o
                        echo hello2oo
                        echo hello3ooo
                    '''
                )
        }


        job(name: 'Build aJIRA 22222', key: 'JOB2', description: 'xzcvzxvcxzxcvzxcvzx') {
            task(type: 'checkout', description: 'Ch2eckout greenhopper') {
                repository(name: 'test', cleanCheckout: 'true')
            }
            task(type:'script', description:'task3', scriptBody:'''
                        echo hello1o
                        echo hello2oo
                        echo hello3ooo
                    '''
                )
            task(type:'script', description:'task4', scriptBody:'''
                        echo hello1o
                        echo hello2oo
                        echo hello3ooo
                    '''
                )
        }

        job(name: 'Build aJIRA 33333', key: 'JOB3', description: 'asdfasdfasdfasdf') {
            task(type: 'checkout', description: 'Ch2eckout greenhopper') {
                repository(name: 'test', cleanCheckout: 'true')
            }
            task(type:'script', description:'task4', scriptBody:'''
                        echo hello1o
                        echo hello2oo
                        echo hello3ooo
                    '''
                )
        }

  
   
    }
    
    
    stage(name: 'asdfasdfadsf', description: 'zxcvzxcvzcx', manual: 'false') {
        
        job(name: 'Build aJIRA 44444', key: 'JOB4', description: 'Buildssss the plugin against JIRA #jiraVersionNumber and runs unit tests') {
            task(type: 'checkout', description: 'Ch2eckout greenhopper') {
                repository(name: 'test', cleanCheckout: 'true')
            }
            task(type:'script', description:'task1', scriptBody:'''
                        echo hello1o
                        echo hello2oo
                        echo hello3ooo
                    '''
                )
            task(type:'script', description:'task2', scriptBody:'''
                        echo hello1o
                        echo hello2oo
                        echo hello3ooo
                    '''
                )
        }

    }

}