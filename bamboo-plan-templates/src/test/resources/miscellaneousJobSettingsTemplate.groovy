plan(key:'REST87', name:'test misc', description:'A sensible description goes here...') {
    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: 'job12', description: 'hellojob' ){
            miscellaneousConfiguration(){
                patternMatchLabelling(regex: 'bla*', labels: 'label1 label2' )
            }

            task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
        }
        job(name: "job2", key: 'job123', description: 'hellojob' ){

            miscellaneousConfiguration( cleanupWorkdirAfterBuild: 'true' ) {
                patternMatchLabelling(regex: 'bla2*', labels: 'label1 label2 label3' )

            }

            task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
        }
    }

}