plan(key:'BAMBOONICORN', name:'Bamboonicorn Plugin', description:'Smoke test for Bamboonicorn Plan Templates')
{
    project(key:'UPGRADE', name:'Smoke Tests - Bamboo Upgrade Prerequisites', description:'')

    repository(name:'bamboonicorn')

    planVariable(key:'unicorn.instance2', value:'bamcorn-pt-test-instance.jira-dev.com')

    stage(name:'Default Stage', description:'')
    {
        job(name:'Unicorn Tasks (PT)', key:'UTP', enabled:'true')
        {
            task(type:'copyToUnicorn', description:'Copy a file to unicorn',
                    unicornInstance:'${bamboo.unicorn.instance2}', sourceType:'PATH_SOURCE_TYPE',
                    pathSource:'fake/file/path', destination:'fake/destination')
        }
    }
}