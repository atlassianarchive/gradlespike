plan(key: 'BRANCHESTEST', name: 'Branches Test', description: '...') {
    repository(name: 'test')
    project(key: 'PERSONAL', name: 'Z Personal', description: '...')

    branchMonitoring(enabled: 'true',
                     matchingPattern: 'issue/[^/]*',
                     timeOfInactivityInDays: '10',
                     notificationStrategy: 'INHERIT',
                     remoteJiraBranchLinkingEnabled: 'true')
    stage(name:"stage1", description: "stage1Description", manual: "true")   {
        job(name: "job1", key: 'job1Key', description: 'hellojob' ){
            task(type:'script', description:'task1', scriptBody:'echo hello1o')
        }
    }
}
