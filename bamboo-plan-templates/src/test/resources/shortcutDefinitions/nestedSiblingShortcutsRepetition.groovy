thirdLevel(['index', 'node']) {
    job(key:'#node #index') {
        requirement(key:'node', value:'#node')
    }
}

secondLevel(['arg']) {
    thirdLevel(node:'#arg', index:'1')
    thirdLevel(node:'#arg', index:'2')
}

bigShortcut() {
    plan(key:'key') {
        stage(name:'stageName') {
            secondLevel(arg:'14')
            secondLevel(arg:'15')
        }
    }
}
