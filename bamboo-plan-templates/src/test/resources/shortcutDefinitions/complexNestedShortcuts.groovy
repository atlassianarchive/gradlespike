burn_and_deploy_ami_plan(['planKey', 'ami', 'configName', 'configScript', 'repositoryName', 'username', 'password', 'pageTitle', 'ec2User']) {
    plan(key:'#planKey', name:'#configName burn and deploy', description:'Burn an AMI and deploy it to all bamboo servers') {
        label(name:'plan-templates')
        repository(name:'#repositoryName')
        repository(name:'Bellatrix')

        variable(key:'burn.instance', value:'1')
        variable(key:'deployment.package', value:'buildeng-puppet')
        variable(key:'ec2.user', value:'#ec2User')
        variable(key:'stop.instance.on.finish', value:'1')

        project(key: 'AMIFACTORY', name:'AMI factory', description:'AMI factory')

        stage(name:'start, provision, burn', description: 'Start the instance, provision it, burn it and set perms', manual: 'false'){
            job(name: 'Burn AMI', key: 'burn', description: 'burn' ){
                requirement(key:'system.git.executable', condition:'exists')
                requirement(key:'elastic', condition:'equals', value:'false')
                requirement(key:'os', condition:'equals', value:'Linux')
                artifactDefinition(name:'burn_instance_out', location:'buildeng-agents-puppet', pattern:'burn_instance_out', shared:'true')
                artifactDefinition(name:'start_instance_out', location:'buildeng-agents-puppet', pattern:'start_instance_out', shared:'true')

                task(type:'checkout', description:'checkout buildeng puppet'){
                    repository(name:'#repositoryName', checkoutDirectory:'./buildeng-agents-puppet')
                }

                task(type:'checkout', description:'checkout bellatrix'){
                    repository(name:'Bellatrix', checkoutDirectory:'./bellatrix')
                }

                task(type:'script', description:'Create Artifact', workingSubDirectory: 'buildeng-agents-puppet', scriptBody:'''
	                    #!/bin/bash

	                    ./utils/ci/create_puppet_deployment_package.sh -a "${bamboo.deployment.package}" || exit 1
	                    mv tmp/*.tar.bz2 .
	                    rm -rf tmp
	                '''
                )

                task(type:'script', description:'Start ec2 instance', scriptBody:'''
	                    #!/bin/bash

	                    # Create virtual environment and install bellatrix.
	                    rm -rf env
	                    virtualenv --no-site-packages env
	                    source env/bin/activate

	                    cd bellatrix
	                    python setup.py install
	                    #pip install bellatrix
	                    bellatrix --version

	                    pushd ../buildeng-agents-puppet
	                    git_subj=`git --no-pager log -1 --format="%s"`
	                    git_auth=`git --no-pager log -1 --format="%an"`
	                    git_emai=`git --no-pager log -1 --format="%ae"`
	                    popd

	                    # Check if the manualbuildtriggerreason username variable has not been replaced by bamboo
	                    if [[ '${bamboo.ManualBuildTriggerReason.userName}' =~ bamboo.ManualBuildTriggerReason.userName ]]
	                    then
	                        bellatrix start #ami elasticbamboo --new_size 40 --type m1.large --scm_subject "$git_subj" --scm_author "$git_auth" --scm_email "$git_emai" \
	                            --build_info "${bamboo.buildKey}  #${bamboo.buildNumber}" --triggered_by "an automated process" --security_groups elasticbamboo
	                    else # If it has, then add the username as a triggered by argument.
	                        bellatrix start #ami elasticbamboo --new_size 40 --type m1.large --scm_subject "$git_subj" --scm_author "$git_auth" --scm_email "$git_emai" \
	                            --build_info "${bamboo.buildKey}  #${bamboo.buildNumber}" --triggered_by "${bamboo.ManualBuildTriggerReason.userName}" --security_groups elasticbamboo
	                    fi

	                '''
                )

                task(type:'script', description:'provision', workingSubDirectory: 'buildeng-agents-puppet', scriptBody:'''
	                    #!/bin/bash
	                    source ../env/bin/activate

	                    mv ../bellatrix/start_instance_out .
	                    python utils/provision.py ec2configs/#configScript ${bamboo.ec2.user} ~/.bellatrix/ec2.pk
	                    ret=$?
	                    echo Provision finished with exit code: $ret

	                    exit $ret
	                '''
                )

                task(type:'script', description:'burn', workingSubDirectory: 'buildeng-agents-puppet', scriptBody:'''
	                    #!/bin/bash
	                    source ../env/bin/activate
	                    ./utils/burn.py #configName
	                '''
                )

                task(type:'script', description:'set permissions', workingSubDirectory: 'buildeng-agents-puppet', scriptBody:'''
	                    #!/bin/bash

	                    source ../env/bin/activate

	                    # Retrieve the start_instance_out from parent dir.
	                    mv ../burn_instance_out .

	                    #applying permissions
	                    ./utils/permissions.py ~/.bellatrix/account_permissions
	                '''
                )

                task(type:'script', description:'stop instance', final:'true', workingSubDirectory: 'buildeng-agents-puppet', scriptBody:'''
	                    #!/bin/bash

	                    # Create virtual environment and install bellatrix.
	                    rm -rf env
	                    virtualenv --no-site-packages env
	                    source env/bin/activate
	                    pip install bellatrix
	                    bellatrix --version

	                    #applying permissions
	                    ./utils/stop_if_necessary.py ${bamboo.stop.instance.on.finish} start_instance_out
	                    exit 0
	                '''
                )

                task(type:'jUnitParser', description:'parse cucumber results', final:'true', resultsDirectory:'**/TEST-*.xml')
            }

        }

        deploy_ami_and_update_register(configName:'#configName', repositoryName:'#repositoryName', username:'#username', password:'#password', pageTitle:'#pageTitle', ami:'#ami', another:"what's the problem?")

    }
}

deploy_ami_and_update_register(['configName', 'repositoryName', 'username', 'password', 'pageTitle', 'ami', 'another']) {
    deploy_ami_to_all_servers(configName:'#configName', repositoryName:'#repositoryName', username:'#username', password:'#password')

    stage(name:'Register', description:'Update the register on EAC #another', manual:'false'){
        job(name:'Update Register on EAC', key:'UPDATEREGISTER', description:'updates the register on EAC'){
            artifactSubscription(name:'beac_names', destination:'./')
            artifactSubscription(name:'xbac_names', destination:'./')
            artifactSubscription(name:'cbac_names', destination:'./')
            artifactSubscription(name:'jbac_names', destination:'./')
            task(type:'checkout', description:'checkout buildeng puppet'){
                repository(name:'Build Agents Puppet')
            }
            task(type:'script', description:'upload', scriptBody:'''

                    ./utils/ec2_deployment/update_ami_register.py https://extranet.atlassian.com #username #password "RELENG" #pageTitle #ami BEAC_names XBAC_names CBAC_names JBAC_names

                    echo "updated register on EAC"
                '''
            )
        }
    }

}

deploy_ami_to_all_servers(['configName', 'repositoryName', 'username', 'password']) {
    stage(name:'Deploy', description: 'Deploy to bamboo servers', manual: 'false'){

        deploy_ami_to_bamboo_server(bambooUrl:'https://staging-bamboo.atlassian.com', bambooAlias:'XBAC', configName:'#configName', repositoryName:'#repositoryName', username:'#username', password:'#password')
        deploy_ami_to_bamboo_server(bambooUrl:'https://confuence-bamboo.atlassian.com', bambooAlias:'CBAC', configName:'#configName', repositoryName:'#repositoryName', username:'#username', password:'#password')
        deploy_ami_to_bamboo_server(bambooUrl:'https://bamboo.extranet.atlassian.com', bambooAlias:'BEAC', configName:'#configName', repositoryName:'#repositoryName', username:'#username', password:'#password')
        deploy_ami_to_bamboo_server(bambooUrl:'https://jira.bamboo.atlassian.com', bambooAlias:'JBAC', configName:'#configName', repositoryName:'#repositoryName', username:'#username', password:'#password')
    }
}

deploy_ami_to_bamboo_server(['bambooUrl', 'bambooAlias', 'configName', 'repositoryName', 'username', 'password']) {
    job(name: "Deploy to #bambooAlias", key: 'DEPLOY#bambooAlias', description: 'Deploys the AMI to #bambooAlias' ){
        artifactDefinition(name:'#bambooAlias_names', pattern:'*elasticAMI.log', shared:'true')
        artifactSubscription(name:'burn_instance_out', destination:'./bellatrix_out')
        artifactSubscription(name:'start_instance_out', destination:'./bellatrix_out')

        task(type:'checkout', description:'checkout buildeng puppet'){
            repository(name:'#repositoryName')
        }
        task(type:'script', description:'upload', scriptBody:'''
                ami=`python -c "print open('./bellatrix_out/burn_instance_out', 'rU').readline().split(',')[0]"`

                echo "using ami: $ami"
                echo "ami name: #configName"

                ./utils/ec2_deployment/autoUpdate.py #bambooUrl #username #password #configName $ami

                echo "uploaded to #bambooAlias"
            '''
        )
    }
}