defaultPlan() {
    plan(key:'TEMPLATETEST') {
        project(key: 'TEMPLATETEST')
        defaultVariables()
    }
}

defaultVariables() {
    planVariable(key:'ami', value:'ami-7aa60314')
    planVariable(key:'ec2.config', value:'fedora15x64ldap.py')
    planVariable(key:'ec2-user', value: 'ec2-user')
}

buildengRequirements(['os']) {
    requirement(key:'container', type:'equals', value:'true')
    requirement(key:'ciagent', type:'equals', value:'true')
    requirement(key:'os', type:'equals', value:'#os')
}