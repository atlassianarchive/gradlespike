jobInternalAgentPerfTestJiraStart([
    'index',
    'hostnode',
    'delayBeforeStart',
    'lengthOfTesting'
]) {
    job(key:'HN#hostnodeX#index', name:'AgentHN#hostnode#index', enabled:'true') {
        requirement(key:'hostnode', value:'#hostnode')
    }
}

jobsInternalAgentsPerfTestJiraStartStaggedStartForHostnode(['hostnode']) {
    jobInternalAgentPerfTestJiraStart(hostnode:'#hostnode', index:'1',  delayBeforeStart:'0',  lengthOfTesting:'30')
    jobInternalAgentPerfTestJiraStart(hostnode:'#hostnode', index:'2',  delayBeforeStart:'2',  lengthOfTesting:'28')
}