templateRepository(url: "http://url.to.the.repository/template.groovy")

plan(key:"SAMPLE", name:"Sample Plan", description:"Plan created from plan templates") {
    project(key: "TSTPLANTEMPLATES", name:"testing plan templates", description:"a project description")
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: "job11Key", description: "basic" ){
            task(type:"deployPlugin", description:"continuous deploy plugin", taskDisabled:"false",
                    product:'fecru', url:'http://localhost', passwordVariable: '${bamboo.password}',
                    artifact:'a.jar', runOnBranch:'true', enableTrafficLogging: 'false', certificateCheckDisabled:'false',
                    pluginInstallationTimeout:'90', useAtlassianId:'true', atlassianIdBaseURL:'http://atlassian',
                    atlassianIdUsername:'atlassianUser', atlassianIdPassword:'pwd', useAtlassianIdWebSudo:'false')
        }
    }
}
