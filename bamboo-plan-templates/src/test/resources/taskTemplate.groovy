plan(key:'REST', name:'rest test', description:'A sensible description goes here...') {
    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')
    repository(name:'test')
    repository(name:'test2')
    
    stage(name:"stage1", description: "stage1", manual: "false"){
        job(name: "job1", key: 'job11Key', description: 'hellojob' ){
            
            // checkout --------------------------------------------------------
            task(type:'checkout', description:'checkout 1'){
                repository(name:'test')
            }
            
            task(type:'checkout', description:'checkout 2', cleanCheckout: 'true'){
                repository(name:'test', checkoutDirectory:'./test1directory')
                repository(name:'test2', checkoutDirectory:'./test2directory')
            }
            
            // script  --------------------------------------------------------
            task(type:'script', description:'task1', scriptBody:'''
                    echo hello1
                    echo hello2
                '''
            )
                        
            task(type:'script', description:'task2', script:'test1.sh', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
            
            // Maven 2 and 3 --------------------------------------------------------
            task(type:'maven3', description:'task3 Simple Maven', goal:'clean install')
            task(type:'maven3', description:'task3Maven', goal:'clean install', hasTests:'true', useMavenReturnCode:'true')
            task(type:'maven3', description:'task3Maven more Args', goal:'clean install', hasTests:'true', useMavenReturnCode:'true', 
                environmentVariables: 'one', workingSubDirectory: '/one', testDirectory: '**/target/my_custom/*.xml')
            task(type:'maven3', description:'task3MavenWithJdk', goal:'clean install', buildJdk:'jdk1.7', mavenExecutable:'maven 3')
            task(type:'maven2', description:'task2Maven', goal:'clean install', hasTests:'true', useMavenReturnCode:'true')
            
            
            
            // junit parser --------------------------------------------------------
            task(type:'jUnitParser', description:'parse test')
            task(type:'jUnitParser', description:'parse cucumber results', final:'true', resultsDirectory:'**/TEST-*.xml')

            
            // deploy plugin --------------------------------------------------------
            task(type:'deployBambooPlugin', description:'deploy plugin', url:'http://mybamboo:1990/bamboo', 
                username:'${bamboo.username}', passwordVariable:'${bamboo.password}', artifact:'artifactName')
            
            task(type:'deployBambooPlugin', description:'deploy plugin2', url:'http://mybamboo:1990/bamboo', 
                username:'${bamboo.username}', passwordVariable:'${bamboo.password}', artifact:'artifactName',
                runOnBranch: 'true')

            task(type:'deployConfluencePlugin', description:'deploy plugin2', url:'http://mybamboo:1990/bamboo',
                    username:'${bamboo.username}', passwordVariable:'${bamboo.password}', artifact:'artifactName',
                    runOnBranch: 'true')

            task(type:'deployJiraPlugin', description:'deploy Jira Plugin', url:'http://jira:1990/',
                    username:'${bamboo.username}', passwordVariable:'${bamboo.password}', artifact:'artifactName',
                    runOnBranch: 'true', useAtlassianId: 'true', atlassianIdUsername: 'myatlassianId',
                    atlassianIdPasswordVariable: '${bamboo.password}', useAtlassianIdWebSudo: 'true', atlassianIdAppName:'appName')
            
            // brokerUrlDiscovery plugin for Hallelujah
            task(type:'brokerUrlDiscovery', description:'task brokerUrlDiscovery')
            
            // make a task final with -> final: 'true' --------------------------------------------------------
            task(type:'deployBambooPlugin', description:'deploy plugin', url:'http://mybamboo:1990/bamboo',
                username:'${bamboo.username}', passwordVariable:'${bamboo.password}', artifact:'artifactName', final: 'true')
            
            task(type:'jUnitParser', description:'parse cucumber results', resultsDirectory:'**/TEST-*.xml', final:'true')
            
            // task node types
            task(type:'npm', description:'npm task', executable:'Node 0.8.23', command: 'install something', workingSubDirectory: './newSub')
            task(type:'nodejs', description:'nodejs task', executable:'Node 0.8.23', script: 'server.js', workingSubDirectory: './newSub', arguments: 'argument1')

            //ant types
            task(type:'ant', description:'an ant task', buildFile: 'antBuildFile',testResultsDirectory:'**/TEST-*.xml', executable: "ANT",
                    hasTests: 'false',final:'true', target: 'install', buildJdk: 'JDK 7')
            
        }
    }
}
