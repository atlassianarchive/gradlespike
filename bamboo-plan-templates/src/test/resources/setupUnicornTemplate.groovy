plan(key:'BAMBOONICORN', name:'Bamboonicorn Plugin', description:'Smoke test for Bamboonicorn Plan Templates')
{
    project(key:'UPGRADE', name:'Smoke Tests - Bamboo Upgrade Prerequisites', description:'')

    repository(name:'bamboonicorn')

    planVariable(key:'unicorn.instance2', value:'bamcorn-pt-test-instance.jira-dev.com')

    stage(name:'Default Stage', description:'')
    {
        job(name:'Unicorn Tasks (PT)', key:'UTP', enabled:'true')
        {
            task(type:'setupUnicorn', description:'Set up a unicorn instance for latest prod',
                    unicornInstance:'${bamboo.unicorn.instance2}', version:'lastest-prod',
                    email:'admin@atlassian.com', username:'admin', password:'admin', gappsDomain:'',
                    license:'insert valid license', halTimeout:'12', source:"fake/path", destination:"fake/destination")

        }
    }
}