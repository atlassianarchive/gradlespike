plan(key:'REST22', name:'test 1', description:'A sensible description goes here...') {
    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: 'job12', description: 'hellojob' ){
            task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
        }
    }
    buildExpiry(notExpire: 'true')
}


plan(key:'REST23', name:'test 2', description:'A sensible description goes here...') {
    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: 'job12', description: 'hellojob' ){
            task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
        }
    }
    
    buildExpiry(buildResults: 'true')
}

plan(key:'REST24', name:'test 3', description:'A sensible description goes here...') {
    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: 'job12', description: 'hellojob' ){
            task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
        }
    }
    buildExpiry(artifacts: 'true')
    
}

plan(key:'REST25', name:'test 4', description:'A sensible description goes here...') {
    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: 'job12', description: 'hellojob' ){
            task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
        }
    }
    buildExpiry(buildResults: 'true', duration: '3', period: 'days', minimumBuildsToKeep: '4')
}


plan(key:'REST26', name:'test 5', description:'A sensible description goes here...') {
    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: 'job12', description: 'hellojob' ){
            task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
        }
    }
    buildExpiry(artifacts: 'true', buildLogs: 'true', duration: '2', period: 'weeks')
}

plan(key:'REST27', name:'test 6', description:'A sensible description goes here...') {
    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: 'job12', description: 'hellojob' ){
            task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
        }
    }
    buildExpiry(labelsToKeep: 'label1 label2')
}


