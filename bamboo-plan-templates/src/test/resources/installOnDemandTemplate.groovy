plan(key:'BAMBOONICORN', name:'Bamboonicorn Plugin', description:'Smoke test for Bamboonicorn Plan Templates')
{
    project(key:'UPGRADE', name:'Smoke Tests - Bamboo Upgrade Prerequisites', description:'')

    repository(name:'bamboonicorn')

    planVariable(key:'unicorn.instance2', value:'bamcorn-pt-test-instance.jira-dev.com')

    stage(name:'Default Stage', description:'')
    {
        job(name:'Unicorn Tasks (PT)', key:'UTP', enabled:'true')
        {
            task(type:'installOnDemand', description:'Install OnDemand onto provisioned unicorn instance',
                    unicornInstance:'${bamboo.unicorn.instance2}', version:'latest', localYaml:'daemontools::j2ee_jira::enabled: false')
        }
    }
}