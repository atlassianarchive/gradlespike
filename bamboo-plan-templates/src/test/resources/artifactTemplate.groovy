plan(key:'HELLO9', name:'hehe', description:'A sensible description goes here...') {
    project(key: 'TESTPROJ', name:'Z Personal', description:'a project description')

    stage(name:'some-stage', description:'stage description', manual:'false') {
        job(name:'job name', key:'jobkey', enabled:'true') {
            artifactDefinition(name:'cargo logs', location:'./logs', pattern:'*.txt', shared:'true')
            task(type:'script', description:'task2', script:'test1', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
        }
    }

    stage(name:'some-second-stage', description:'stage description', manual:'false') {
        job(name:'consumer', key:'second', description:'consumes the artifact', enabled:'true') {
            artifactSubscription(name:'cargo logs', destination:'./dump')
            task(type:'script', description:'task2', script:'test3', argument: 'testArg', environmentVariables: 'one', workingSubDirectory: '/one')
        }
    }
}