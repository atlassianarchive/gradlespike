plan(key:'TEMPLATETEST', name:'Fedora15 OpenLDAP', description:'A sensible description goes here...') {

    notification(type:'All Builds Completed', recipient:'email', email:'fake@fake.com')
    notification(type:'All Builds Completed', recipient:'user', user:'bsmith')

    notification(type:'Job Hung', recipient:'hipchat', apiKey:'api_key', room:'our_room', notify:'false')
    
    notification(type:'Job Hung', recipient:'httpNotification', baseUrl:'http://mydomain.com:3000')
    
    notification(type:'Failed Jobs and First Successful', recipient:'committers')
    
    
    notification(type:'After X Build Failures', recipient:'group', group:'build-admins')
    project(key: 'PERSONAL', name:'Z Personal', description:'a project description')
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: 'job11Key', description: 'job1description' ){
            task(type:'script', description:'task05', scriptBody:'echo blah'){
            }
        }
    }
}
