plan(key:'BAMBOONICORN', name:'Bamboonicorn Plugin', description:'Smoke test for Bamboonicorn Plan Templates')
{
    project(key:'UPGRADE', name:'Smoke Tests - Bamboo Upgrade Prerequisites', description:'')

    repository(name:'bamboonicorn')

    planVariable(key:'unicorn.instance2', value:'bamcorn-pt-test-instance.jira-dev.com')

    stage(name:'Default Stage', description:'')
    {
        job(name:'Unicorn Tasks (PT)', key:'UTP', enabled:'true')
        {
            task(type:'promoteManifesto', description:'Promote artifact', artifactName:'placeholder',
                    artifactVersion:'placeholder', application:'jira', deploymentEnvironment:'jirastudio-dev',
                    username:'username', passwordVariable:'${bamboo.password}')

            task(type:'promoteProductVersion', description:'Promote product', productName:'placeholder',
                    productVersion:'placeholder', deploymentEnvironment:'jirastudio-dev',
                    username:'username', passwordVariable:'${bamboo.password}')

            task(type:'promoteManifestoArtifacts', description:'Promote artifacts', promotionSet:'placeholder',
                    preconditionsSet:'placeholder', forcePromote:'false', deploymentEnvironment:'jirastudio-dev',
                    username:'username', passwordVariable:'${bamboo.password}')

            task(type:'removeManifestoArtifacts', description:'Remove artifacst', removalSet:'placeholder',
                    preconditionsSet:'placeholder', deploymentEnvironment:'jirastudio-dev',
                    username:'username', passwordVariable:'${bamboo.password}')
        }
    }
}