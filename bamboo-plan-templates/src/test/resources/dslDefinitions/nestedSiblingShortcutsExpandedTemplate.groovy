plan(key:'HN18', name:'Hostnode 18 test', description:'', planEnabled:'true') {
    stage(name:'Default Stage', description:'', manual:'false') {
        job(key:'HN18X1', name:'AgentHN181', enabled:'true') {
            requirement(key:'hostnode', value:'18')
        }
        job(key:'HN18X2', name:'AgentHN182', enabled:'true') {
            requirement(key:'hostnode', value:'18')
        }
    }
}