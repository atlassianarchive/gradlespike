plan(key:"RUN2", name:"Run sample.groovy", description:"Runs a plan template...") {
    repository(name:"plan templates")
    trigger(description:'polling trigger', type:'polling', strategy:'periodically', frequency:'15'){
        repository(name:"plan templates")
    }
    project(key: "PLANTEMPLATES", name:"plan templates", description:"a project description")
    stage(name:"stage1", description: "stage11111Description", manual: "false"){
        job(name: "job1", key: "job11Key", description: "basic" ){

            task(type:"nodejs", description:"description aaaaa", executable: "Node.js",
                    script:"server.js",
                    arguments:"argument1")
        }
    }
}
