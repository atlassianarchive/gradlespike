package com.atlassian.bamboo.plugin.plantemplates.support.tasks;


import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class MavenVersionUpdaterTaskMappingTest
{
    @InjectMocks
    private MavenVersionUpdaterTaskMapping taskMapping;

    @Test
    public void convertATaskOfMavenVersionUpdater()
    {

        Map<String, String> map = ImmutableMap.<String, String>builder()
                                    .put("description", "desc")
                                    .put("type", "mavenVersionUpdater")
                                    .put("versionPattern", ".autoUpdate")
                                    .build();

        Map<String, String> result = taskMapping.convert(map);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                                        .put("taskDisabled", "false")
                                        .put("userDescription", "desc")
                                        .put("versionPattern", ".autoUpdate")
                                        .put("includeGlobals", "false")
                                        .put("createTaskKey", MavenVersionUpdaterTaskMapping.MAVEN_VERSION_VARIABLE_UPDATER_TASK_PLUGIN_KEY)
                                        .build();

        assertEquals(expected, result);
    }

    @Test
    public void convertATaskOfMavenVersionUpdaterWithExtraAttrs()
    {
        Map<String, String> map = ImmutableMap.<String, String>builder()
                .put("description", "desc")
                .put("taskDisabled", "true")
                .put("type", "mavenVersionUpdater")
                .put("versionPattern", ".versionPt")
                .put("includeGlobals", "true")
                .put("variablePattern", ".varUpdate")
                .build();

        Map<String, String> result = taskMapping.convert(map);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "true")
                .put("userDescription", "desc")
                .put("versionPattern", ".versionPt")
                .put("includeGlobals", "true")
                .put("variablePattern", ".varUpdate")
                .put("createTaskKey", MavenVersionUpdaterTaskMapping.MAVEN_VERSION_VARIABLE_UPDATER_TASK_PLUGIN_KEY)
                .build();

        assertEquals(expected, result);
    }
}
