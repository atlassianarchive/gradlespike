package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.Map;

import com.atlassian.bamboo.deployments.projects.DeploymentProject;
import com.atlassian.bamboo.deployments.projects.service.DeploymentProjectService;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("rawtypes")
@RunWith(MockitoJUnitRunner.class)
public class DeploymentVersioningServiceTest
{

    @InjectMocks
    private DeploymentVersioningService deploymentVersioningService;
    
    @Mock
    private DeploymentProjectService deploymentProjectService;
    
    @Test
    public void runTemplatePerformsACreationOfVersioningForADeployment() throws Exception
    {
    
        // versioning(version:'release-1-${bamboo.variable1}-${bamboo.variable2}', autoIncrementNumber: true) 
        
        Map versioning = ImmutableMap.builder()
                 .put("version", "release-1-${bamboo.variable1}-${bamboo.variable2}")
                 .put("autoIncrementNumber", "true")
                 .build();
        
        DeploymentProject deployment = mock(DeploymentProject.class);
        when(deployment.getName()).thenReturn("deploymentName");
        when(deployment.getId()).thenReturn(5L);
        
        deploymentVersioningService.performChanges(versioning, deployment);
        
        verify(deploymentProjectService, times(1)).updateVersionNamingScheme(5L, "release-1-${bamboo.variable1}-${bamboo.variable2}", true, Sets.<String>newHashSet());
        
    }
    
    
    @Test
    public void runTemplatePerformsACreationOfVersioningForADeploymentWithVersioningVariables() throws Exception
    {
    
        // versioning(version:'release-1-${bamboo.variable1}-${bamboo.variable2}', autoIncrementNumber: true) {
        //     variableToAutoIncrement(key: 'variable1')
        //     variableToAutoIncrement(key: 'variable2')
        // }
        
        Map variable1 = ImmutableMap.builder()
            .put("key", "variable1")
            .build();
   
        Map variable2 = ImmutableMap.builder()
            .put("key", "variable2")
            .build();
        
        Map versioning = ImmutableMap.builder()
                 .put("version", "release-1-${bamboo.variable1}-${bamboo.variable2}")
                 .put("autoIncrementNumber", "true")
                 .put("variableToAutoIncrement", Lists.newArrayList(variable1, variable2))
                 .build();
        
        DeploymentProject deployment = mock(DeploymentProject.class);
        when(deployment.getName()).thenReturn("deploymentName");
        when(deployment.getId()).thenReturn(5L);
        
        deploymentVersioningService.performChanges(versioning, deployment);
        
        verify(deploymentProjectService, times(1)).updateVersionNamingScheme(5L, "release-1-${bamboo.variable1}-${bamboo.variable2}", true, Sets.newHashSet("variable1", "variable2"));
        
    }
}
