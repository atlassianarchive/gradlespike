package com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions;

import com.atlassian.bamboo.notification.NotificationManager;
import com.atlassian.bamboo.notification.NotificationRule;
import com.atlassian.bamboo.notification.NotificationRuleImpl;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.AfterXFailuresNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.RecipientMapping;
import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Map;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AfterXFailuresNotificationTest
{
    @InjectMocks
    private AfterXFailuresNotificationMapping afterXFailuresNotificationMapping;

    @Mock
    private NotificationManager notificationManager;

    @Mock
    private RecipientMapping recipientMapping;

    private Answer<NotificationRule> notificationRuleAnswer = new Answer<NotificationRule>() {
        @Override
        public NotificationRule answer(InvocationOnMock invocation) throws Throwable {
            Object[] args = invocation.getArguments();
            return createNewNotificationRule((String) args[0], (String) args[1], (String) args[2], (String) args[3]);
        }
    };

    @Test
    @SuppressWarnings("rawtypes")
    public void buildCompleteNotificationMappingReturnsCorrectRule() {
        Map notification = ImmutableMap.builder()
                .put("recipient", "email")
                .put("type", "after X failures")
                .put("X", "3")
                .put("email", "fake@fake.com")
                .build();


        when(notificationManager.createNotificationRule(anyString(), anyString(), anyString(), anyString())).thenAnswer(notificationRuleAnswer);

        NotificationRule notificationRule = afterXFailuresNotificationMapping.createNotificationRule(notification);
        Assert.assertNotNull(notificationRule);
        Assert.assertEquals(notificationRule.getConditionKey(), "com.atlassian.bamboo.plugin.system.notifications:chainCompleted.XFailedChains");
        Assert.assertEquals(notificationRule.getConditionData(), "3");

    }

    private NotificationRule createNewNotificationRule(String conditionKey, String conditionData, String recipientString, String recipientType) {
        NotificationRule newNR = new NotificationRuleImpl();
        newNR.setNotificationManager(notificationManager);
        newNR.setConditionData(conditionData);
        newNR.setConditionKey(conditionKey);
        newNR.setRecipient(recipientString);
        newNR.setRecipientType(recipientType);
        return newNR;
    }
}
