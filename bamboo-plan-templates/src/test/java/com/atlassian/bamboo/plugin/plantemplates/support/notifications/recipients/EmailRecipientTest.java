package com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients;

import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.EmailRecipientMapping;
import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class EmailRecipientTest
{
    @InjectMocks
    private EmailRecipientMapping emailRecipientMapping;

    @SuppressWarnings("rawtypes")
    @Test
    public void emailMappingReturnsCorrectData(){
        Map notification = ImmutableMap.builder()
                .put("recipient", "email")
                .put("type", "Build Complete")
                .put("email", "fake@fake.com")
                .build();

        String recipientKey = emailRecipientMapping.getRecipientKey();
        String recipientData = emailRecipientMapping.getRecipientData(notification);

        Assert.assertEquals(recipientKey, "com.atlassian.bamboo.plugin.system.notifications:recipient.email");
        Assert.assertEquals(recipientData, "fake@fake.com");
    }
}
