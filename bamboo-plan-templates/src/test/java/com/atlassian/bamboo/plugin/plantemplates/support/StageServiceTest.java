package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.build.creation.ChainCreationService;
import com.atlassian.bamboo.caching.DashboardCachingManager;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.chains.ChainStage;
import com.atlassian.bamboo.chains.ChainStageImpl;
import com.atlassian.bamboo.chains.DefaultChain;
import com.atlassian.bamboo.deletion.DeletionService;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.branch.ChainBranchManager;
import com.atlassian.bamboo.project.ProjectManager;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings({ "rawtypes", "unchecked"})
public class StageServiceTest
{
    @InjectMocks
    private StageService stageService;
    
    @Mock
    private ProjectManager projectManager;
    @Mock
    private PlanManager planManager;
    @Mock
    private ChainCreationService chainCreationService;
    @Mock
    private DashboardCachingManager dashboardCachingManager;
    @Mock
    private JobService jobService;
    @Mock
    private ChainBranchManager chainBranchManager;
    @Mock
    private DeletionService deletionService;

    
    @Test
    public void performChangesCreatesOneStage() throws Exception
    {
        Map stageRepresentation = ImmutableMap.builder()
                            .put("name", "stage1")
                            .put("description", "stage1Description")
                            .put("manual", "false").build();

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");

        when(planManager.getPlanById(anyLong(), any(Class.class))).thenReturn(createdChain);

        Chain builtPlan = stageService.performStageChanges(stageRepresentation, createdChain);
        
        Assert.assertNotNull(builtPlan);
        Assert.assertEquals(Lists.newArrayList(new ChainStageImpl(createdChain, "stage1", "stage1Description", false)), builtPlan.getStages());
    }

    @Test
    public void performChangesCreatesMultipleStages() throws Exception
    {
        
        List stageRepresentation = ImmutableList.builder()
                         .add(ImmutableMap.builder()
                                 .put("name", "stage1")
                                 .put("description", "stage1Description")
                                 .put("manual", "false").build())
                         .add(ImmutableMap.builder()
                                 .put("name", "stage2")
                                 .put("description", "stage2Description")
                                 .put("manual", "true").build())
            .build();

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");

        when(planManager.getPlanById(anyLong(), any(Class.class))).thenReturn(createdChain);
        
        Chain builtPlan = stageService.performStageChanges(stageRepresentation, createdChain);
        
        Assert.assertNotNull(builtPlan);
        Assert.assertEquals(Lists.newArrayList(new ChainStageImpl(createdChain, "stage1", "stage1Description", false), 
                                               new ChainStageImpl(createdChain, "stage2", "stage2Description", true)), builtPlan.getStages());
        
    }
    
    @Test
    public void performChangesUpdatesOneStage() throws Exception
    {

        Map stageRepresentation = ImmutableMap.builder()
                            .put("name", "stage1.1")
                            .put("description", "stage1Description")
                            .put("manual", "false").build();



        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");
        createdChain.addNewStage("stage1", "stage1Description", false);

        when(planManager.getPlanById(anyLong(), any(Class.class))).thenReturn(createdChain);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(createdChain);


        
        Chain builtPlan = stageService.performStageChanges(stageRepresentation, createdChain);
        
        Assert.assertNotNull(builtPlan);
        Assert.assertEquals(Lists.newArrayList(new ChainStageImpl(createdChain, "stage1.1", "stage1Description", false)), builtPlan.getStages());
        
    }
    
    @Test
    public void performChangesUpdatesMultipleStage() throws Exception
    {
        
        List stageRepresentation = ImmutableList.builder()
                                 .add(ImmutableMap.builder()
                                         .put("name", "stage1.1")
                                         .put("description", "stage1Description")
                                         .put("manual", "false").build())
                                 .add(ImmutableMap.builder()
                                         .put("name", "stage2.2")
                                         .put("description", "stage2Description")
                                         .put("manual", "true").build())
                                 .build();
        


        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");
        createdChain.addNewStage("stage1", "stage1Description", false);
        createdChain.addNewStage("stage2", "stage2Description", false);

        when(planManager.getPlanById(anyLong(), any(Class.class))).thenReturn(createdChain);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(createdChain);
        
        Chain builtPlan = stageService.performStageChanges(stageRepresentation, createdChain);
        
        Assert.assertNotNull(builtPlan);
        Assert.assertEquals(Lists.newArrayList(new ChainStageImpl(createdChain, "stage1.1", "stage1Description", false),
                                               new ChainStageImpl(createdChain, "stage2.2", "stage2Description", true)), builtPlan.getStages());
        
    }
    
    @Test
    public void performChangesDeletesCorrectStage() throws Exception
    {
        
        Map stageRepresentation = ImmutableMap.builder()
                            .put("name", "stage1")
                            .put("description", "stage1Description")
                            .put("manual", "false").build();

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");
        createdChain.addNewStage("stage1", "stage1Description", false);
        createdChain.addNewStage("stage2", "stage2Description", false);

        when(planManager.getPlanById(anyLong(), any(Class.class))).thenReturn(createdChain);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(createdChain);

        
        Chain builtPlan = stageService.performStageChanges(stageRepresentation, createdChain);
        
        Assert.assertNotNull(builtPlan);
        
        verify(deletionService, times(1)).deleteStage(any(ChainStage.class));
        
    }
    
    
    @Test
    public void performChangesDeletesAndUpdatesCorrectStage() throws Exception
    {
        
        List stageRepresentation = ImmutableList.builder()
                             .add(ImmutableMap.builder()
                                 .put("name", "stage1.1")
                                 .put("description", "stage1Description")
                                 .put("manual", "false").build())
                             .add(ImmutableMap.builder()
                                  .put("name", "stage2.2")
                                  .put("description", "stage2Description")
                                  .put("manual", "true").build())
                             .build();

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");
        createdChain.addNewStage("stage1", "stage1Description", false);
        createdChain.addNewStage("stage2", "stage2Description", false);
        createdChain.addNewStage("stage3", "stage3Description", false);
        createdChain.addNewStage("stage4", "stage4Description", false);

        when(planManager.getPlanById(anyLong(), any(Class.class))).thenReturn(createdChain);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(createdChain);


        Chain builtPlan = stageService.performStageChanges(stageRepresentation, createdChain);
        
        Assert.assertNotNull(builtPlan);
        
        verify(deletionService, times(2)).deleteStage(any(ChainStage.class));
        
    }
    
    
    
    @Test
    public void performChangesAddsAndUpdatesCorrectStage() throws Exception
    {
        
        List stageRepresentation = ImmutableList.builder()
                             .add(ImmutableMap.builder()
                                 .put("name", "stage1.1")
                                 .put("description", "stage1Description")
                                 .put("manual", "false").build())
                             .add(ImmutableMap.builder()
                                  .put("name", "stage2.2")
                                  .put("description", "stage2Description")
                                  .put("manual", "true").build())
                             .add(ImmutableMap.builder()
                                  .put("name", "stage3")
                                  .put("description", "stage3Description")
                                  .put("manual", "true").build())
                            .build();

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");
        createdChain.addNewStage("stage1", "stage1Description", false);
        createdChain.addNewStage("stage2", "stage2Description", false);

        when(planManager.getPlanById(anyLong(), any(Class.class))).thenReturn(createdChain);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(createdChain);
        
        Chain builtPlan = stageService.performStageChanges(stageRepresentation, createdChain);
        
        Assert.assertNotNull(builtPlan);
        Assert.assertEquals(Lists.newArrayList(new ChainStageImpl(createdChain, "stage1.1", "stage1Description", false),
                                               new ChainStageImpl(createdChain, "stage2.2", "stage2Description", true),
                                               new ChainStageImpl(createdChain, "stage3", "stage3Description", true)), builtPlan.getStages());
        
    }
    
    
    
        
    
    
    @Test
    public void performChangesPropagateChangesToTheNestedJobs() throws Exception
    {
        Map stage1job = ImmutableMap.builder()
                .put("name", "job1Name")
                .put("key", "job1Key")
                .put("description", "job1description")
                .put("enabled", "true")
                .build();

        Map job2 = ImmutableMap.builder()
                .put("name", "job2Name")
                .put("key", "job2Key")
                .put("description", "job2description")
                .put("enabled", "true")
                .build();

        Map job3 = ImmutableMap.builder()
                .put("name", "job3Name")
                .put("key", "job3Key")
                .put("description", "job3description")
                .put("enabled", "true")
                .build();

        List stage2Jobs = ImmutableList.builder()
                .add(job2)
                .add(job3)
                .build();


        List stageRepresentation = ImmutableList.builder()
                                 .add(ImmutableMap.builder()
                                         .put("name", "stage1")
                                         .put("description", "stage1Description")
                                         .put("manual", "false")
                                         .put("job", stage1job)
                                         .build()
                                 )
                                 .add(ImmutableMap.builder()
                                         .put("name", "stage2")
                                         .put("description", "stage2Description")
                                         .put("manual", "false")
                                         .put("job", stage2Jobs)
                                         .build()

                                 ).build();

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");
        createdChain.addNewStage("stage1", "stage1Description", false);

        when(planManager.getPlanById(anyLong(), any(Class.class))).thenReturn(createdChain);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(createdChain);


        Chain builtPlan = stageService.performStageChanges(stageRepresentation, createdChain);
        
        Assert.assertNotNull(builtPlan);
        verify(jobService, times(1)).performJobChanges(stage1job, createdChain.getStages().get(0));
        verify(jobService, times(1)).performJobChanges(stage2Jobs, createdChain.getStages().get(1));
    }


}
