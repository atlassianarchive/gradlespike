package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_EXECUTE_ON_UNICORN;

@RunWith(MockitoJUnitRunner.class)
public class ExecuteOnUnicornTaskMappingTest
{
    @InjectMocks
    private ExecuteOnUnicornTaskMapping executeOnUnicornTaskMapping;

    @Test
    @SuppressWarnings("rawtypes")
    public void convertAExecuteOnUnicornTaskWithMinimalParamsReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_EXECUTE_ON_UNICORN.getTaskKey())
                .put("description", "task description")
                .build();

        Map<String,String> taskActionParams = executeOnUnicornTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_EXECUTE_ON_UNICORN.createTaskMappingKey())
                .put("scriptLocation", "INLINE")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }

    @Test
    @SuppressWarnings("rawtypes")
    public void convertAExecuteOnUnicornTaskWithInlineReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_EXECUTE_ON_UNICORN.getTaskKey())
                .put("description", "task description")
                .put("unicornInstance", "fake-instance.jira-dev.com")
                .put("scriptBody", "fakescript")
                .put("arguments", "rawr")
                .build();

        Map<String,String> taskActionParams = executeOnUnicornTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("unicornInstance", "fake-instance.jira-dev.com")
                .put("arguments", "rawr")
                .put("scriptBody", "fakescript")
                .put("scriptLocation", "INLINE")
                .put("createTaskKey", BAMBOONICORN_EXECUTE_ON_UNICORN.createTaskMappingKey())
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }

    @Test
    @SuppressWarnings("rawtypes")
    public void convertAExecuteOnUnicornTaskWithFileReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_EXECUTE_ON_UNICORN.getTaskKey())
                .put("description", "task description")
                .put("unicornInstance", "fake-instance.jira-dev.com")
                .put("scriptBody", "fakescript")
                .put("arguments", "rawr")
                .put("scriptLocation", "FILE")
                .put("scriptFile", "derpderp.sh")
                .build();

        Map<String,String> taskActionParams = executeOnUnicornTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("unicornInstance", "fake-instance.jira-dev.com")
                .put("scriptBody", "fakescript")
                .put("arguments", "rawr")
                .put("scriptLocation", "FILE")
                .put("scriptFile", "derpderp.sh")
                .put("createTaskKey", BAMBOONICORN_EXECUTE_ON_UNICORN.createTaskMappingKey())
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }
}
