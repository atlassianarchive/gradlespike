package com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients;

import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.UserRecipientMapping;
import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class UserRecipientTest
{
    @InjectMocks
    private UserRecipientMapping userRecipientMapping;

    @SuppressWarnings("rawtypes")
    @Test
    public void userMappingReturnsCorrectData(){
        Map notification = ImmutableMap.builder()
                .put("recipient", "user")
                .put("type", "Build Complete")
                .put("user", "bsmith")
                .build();

        String recipientKey = userRecipientMapping.getRecipientKey();
        String recipientData = userRecipientMapping.getRecipientData(notification);

        Assert.assertEquals("com.atlassian.bamboo.plugin.system.notifications:recipient.user", recipientKey);
        Assert.assertEquals("bsmith", recipientData);
    }
}