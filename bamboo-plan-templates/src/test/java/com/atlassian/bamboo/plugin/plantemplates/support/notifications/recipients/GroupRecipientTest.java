package com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients;

import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.GroupRecipientMapping;
import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class GroupRecipientTest
{

    @InjectMocks
    private GroupRecipientMapping groupRecipientMapping;

    @SuppressWarnings("rawtypes")
    @Test
    public void groupMappingReturnsCorrectData(){
        Map notification = ImmutableMap.builder()
                .put("recipient", "group")
                .put("type", "Build Complete")
                .put("group", "build-admins")
                .build();

        String recipientKey = groupRecipientMapping.getRecipientKey();
        String recipientData = groupRecipientMapping.getRecipientData(notification);

        Assert.assertEquals("com.atlassian.bamboo.plugin.system.notifications:recipient.group", recipientKey);
        Assert.assertEquals("build-admins", recipientData);
    }
}
