package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_START_UNICORN;

@RunWith(MockitoJUnitRunner.class)
public class StartUnicornTaskMappingTest
{
    @InjectMocks
    private StartUnicornTaskMapping startUnicornTaskMapping;

    @Test
    @SuppressWarnings("rawtypes")
    public void convertAStartUnicornTaskWithMinimalParamsReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_START_UNICORN.getTaskKey())
                .put("description", "task description")
                .build();

        Map<String,String> taskActionParams = startUnicornTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_START_UNICORN.createTaskMappingKey())
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }

    @Test
    @SuppressWarnings("rawtypes")
    public void convertAStartUnicornTaskReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_START_UNICORN.getTaskKey())
                .put("description", "task description")
                .put("unicornInstance", "test-unicorn-instance.jira-dev.com")
                .put("halTimeout", "17")
                .build();

        Map<String,String> taskActionParams = startUnicornTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_START_UNICORN.createTaskMappingKey())
                .put("unicornInstance", "test-unicorn-instance.jira-dev.com")
                .put("halTimeout", "17")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }

}
