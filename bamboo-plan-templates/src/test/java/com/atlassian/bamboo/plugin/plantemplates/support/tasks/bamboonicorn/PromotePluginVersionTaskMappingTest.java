package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_PROMOTE_PLUGIN_VERSION;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("UnusedDeclaration")
public class PromotePluginVersionTaskMappingTest
{
    @InjectMocks
    private PromotePluginVersionTaskMapping promotePluginVersionTaskMapping;

    @Test
    public void convertWillHandleAllFields()
    {
        Map<String, String> task = ImmutableMap.<String, String>builder()
                .put("type", BAMBOONICORN_PROMOTE_PLUGIN_VERSION.getTaskKey())
                .put("description", "task description")
                .put("artifactName", "fakeArtifact")
                .put("artifactVersion", "1.1.1")
                .put("application", "horde")
                .put("deploymentEnvironment", "jirastudio-prd")
                .put("username", "username")
                .put("passwordVariable", "${bamboo.password}")
                .build();

        Map<String,String> taskActionParams = promotePluginVersionTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_PROMOTE_PLUGIN_VERSION.createTaskMappingKey())
                .put("artifactName", "fakeArtifact")
                .put("artifactVersion", "1.1.1")
                .put("application", "horde")
                .put("deploymentEnvironment", "PROD")
                .put("username", "username")
                .put("passwordVariable", "${bamboo.password}")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }

    @Test
    public void missingDeploymentEnvironmentWillBeReplacedWithDefault()
    {
        Map<String, String> task = ImmutableMap.<String, String>builder()
                .put("type", BAMBOONICORN_PROMOTE_PLUGIN_VERSION.getTaskKey())
                .put("description", "task description")
                .put("artifactName", "fakeArtifact")
                .put("artifactVersion", "1.1.1")
                .put("application", "horde")
                .put("username", "username")
                .put("passwordVariable", "${bamboo.password}")
                .build();

        Map<String,String> taskActionParams = promotePluginVersionTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_PROMOTE_PLUGIN_VERSION.createTaskMappingKey())
                .put("artifactName", "fakeArtifact")
                .put("artifactVersion", "1.1.1")
                .put("application", "horde")
                .put("deploymentEnvironment", "DEV")
                .put("username", "username")
                .put("passwordVariable", "${bamboo.password}")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }
}
