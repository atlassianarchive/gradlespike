package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.build.DefaultJob;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plugin.BambooPluginUtils;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.ScriptTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.TaskMappingFactory;
import com.atlassian.bamboo.task.TaskConfigurationService;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskDefinitionImpl;
import com.atlassian.bamboo.task.TaskManager;
import com.atlassian.bamboo.task.TaskModuleDescriptor;
import com.atlassian.bamboo.task.TaskRootDirectorySelector;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import com.google.common.collect.Maps;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TaskServiceTest
{
    @InjectMocks
    private TaskService taskService;

    @Mock
    private TaskConfigurationService taskConfigurationService;
    @Mock
    private TaskManager taskManager;
    @Mock 
    private BuildDefinitionManager buildDefinitionManager;
    @Mock 
    private TaskMappingFactory taskMappingFactory;
    @Mock
    private PlanManager planManager;

    @Before
    public void setUpTaskManager(){
        taskService.setTaskManager(taskManager);
    }
    
    @Test
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void performChangesCreatesOneTask()
    {
        // task(type:'scriptTask', description:'$description', scriptBody:'echo hellooooooo')
        Map tasksRepresentation = ImmutableMap.builder()
             .put("type", "scriptTask")
             .put("description", "task1Script")
             .put("scriptBody", "echo hellooooo")
             .build();
       
        DefaultJob job = new DefaultJob();
        job.setBuildName("job1Name");
        job.setBuildKey("JOB1KEY");
        job.setDescription("job1Description");
        job.setKey("projectkey-plankey1");
        job.setBuildDefinitionManager(buildDefinitionManager);
        
        TaskModuleDescriptor taskDescriptor = mock(TaskModuleDescriptor.class);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(job);
        when(taskManager.getTaskDescriptor(BambooPluginUtils.SCRIPT_BUILDER_TASK_PLUGIN_KEY)).thenReturn(taskDescriptor);
        when(taskMappingFactory.create("scriptTask")).thenReturn(new ScriptTaskMapping());
        
        // method to test
        taskService.performTasksChanges(tasksRepresentation, job);
        
        Map<String, String> taskParams = new ScriptTaskMapping().convert(tasksRepresentation);   
        
        verify(taskConfigurationService, times(1)).createTask(job.getPlanKey(), taskDescriptor, "task1Script", true, taskParams, TaskRootDirectorySelector.DEFAULT); 

    }

    
    @Test
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void performChangesCreatesMultipleTask()
    {
        // task(type:'scriptTask', description:'$description', scriptBody:'echo hellooooooo')
        
        Map task1 = ImmutableMap.builder()
            .put("type", "scriptTask")
            .put("description", "task1Script")
            .put("scriptBody", "echo hellooooo")
            .build();

        Map task2 = ImmutableMap.builder()
            .put("type", "scriptTask")
            .put("description", "task2Script")
            .put("scriptBody", "echo hellooooo")
            .build();
        
        List tasksRepresentation = ImmutableList.builder()
             .add(task1)
             .add(task2)
             .build();
       
        DefaultJob job = new DefaultJob();
        job.setBuildName("job1Name");
        job.setBuildKey("JOB1KEY");
        job.setDescription("job1Description");
        job.setKey("projectkey-plankey1");
        job.setBuildDefinitionManager(buildDefinitionManager);
        
        TaskModuleDescriptor taskDescriptor = mock(TaskModuleDescriptor.class);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(job);
        when(taskManager.getTaskDescriptor(BambooPluginUtils.SCRIPT_BUILDER_TASK_PLUGIN_KEY)).thenReturn(taskDescriptor);
        when(taskMappingFactory.create("scriptTask")).thenReturn(new ScriptTaskMapping());
        
        // method to test
        taskService.performTasksChanges(tasksRepresentation, job);
        
        Map<String, String> taskParams1 = new ScriptTaskMapping().convert(task1);
        Map<String, String> taskParams2 = new ScriptTaskMapping().convert(task2);
        
        verify(taskConfigurationService, times(1)).createTask(job.getPlanKey(), taskDescriptor, "task1Script", true, taskParams1, TaskRootDirectorySelector.DEFAULT);
        verify(taskConfigurationService, times(1)).createTask(job.getPlanKey(), taskDescriptor, "task2Script", true, taskParams2, TaskRootDirectorySelector.DEFAULT);

    }
    
    
    @Test
    @SuppressWarnings("rawtypes")    
    public void performChangesDeletesTheExistingTaskForTheJob()
    {
        // task(type:'scriptTask', description:'$description', scriptBody:'echo hellooooooo')
        Map tasksRepresentation = ImmutableMap.builder()
             .put("type", "scriptTask")
             .put("description", "task1Script")
             .put("scriptBody", "echo hellooooo")
             .build();
       
        DefaultJob job = mock(DefaultJob.class, RETURNS_DEEP_STUBS);

        TaskDefinition taskDef1 = mock(TaskDefinition.class);
        TaskDefinition taskDef2 = mock(TaskDefinition.class);
        when(taskDef1.getId()).thenReturn(2L);
        when(taskDef2.getId()).thenReturn(8L);
        when(job.getBuildDefinition().getTaskDefinitions()).thenReturn(Lists.newArrayList(taskDef1, taskDef2));
        PlanKey planKey = PlanKeys.getPlanKey("project1-job1");
        when(job.getPlanKey()).thenReturn(planKey);
        when(planManager.getPlanByKey(planKey, Job.class)).thenReturn(job);
        when(taskMappingFactory.create("scriptTask")).thenReturn(new ScriptTaskMapping());
        
        TaskModuleDescriptor taskDescriptor = mock(TaskModuleDescriptor.class);
        when(taskManager.getTaskDescriptor(BambooPluginUtils.SCRIPT_BUILDER_TASK_PLUGIN_KEY)).thenReturn(taskDescriptor);
        
        // method to test
        taskService.performTasksChanges(tasksRepresentation, job);
        
        verify(taskConfigurationService, times(1)).deleteTask(eq(planKey), eq(2L));
        verify(taskConfigurationService, times(1)).deleteTask(eq(planKey), eq(8L));

    }

    @Test
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void performChangesCreatesAFinalTask()
    {
        Map tasksRepresentation = ImmutableMap.builder()
                .put("type", "scriptTask")
                .put("description", "task1Script")
                .put("final", "true")
                .put("scriptBody", "echo hellooooo")
                .build();

        DefaultJob job = mock(DefaultJob.class, RETURNS_DEEP_STUBS);

        BuildDefinition buildDefinition = mock(BuildDefinition.class);
        TaskDefinition taskDefinition = new TaskDefinitionImpl(0, "pluginkey", "task1Script", true, Maps.<String, String>newHashMap());

        Map<String, String> taskParams = new ScriptTaskMapping().convert(tasksRepresentation);

        TaskModuleDescriptor taskDescriptor = mock(TaskModuleDescriptor.class);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(job);
        when(taskManager.getTaskDescriptor(BambooPluginUtils.SCRIPT_BUILDER_TASK_PLUGIN_KEY)).thenReturn(taskDescriptor);
        when(taskMappingFactory.create("scriptTask")).thenReturn(new ScriptTaskMapping());
        when(buildDefinition.getTaskDefinitions()).thenReturn(Lists.newArrayList(taskDefinition));
        when(job.getBuildDefinition()).thenReturn(buildDefinition);
        when(taskConfigurationService.createTask(job.getPlanKey(), taskDescriptor, "task1Script", true, taskParams, TaskRootDirectorySelector.DEFAULT)).thenReturn(taskDefinition);

        // method to test
        taskService.performTasksChanges(tasksRepresentation, job);

        verify(taskConfigurationService, times(1)).createTask(job.getPlanKey(), taskDescriptor, "task1Script", true, taskParams, TaskRootDirectorySelector.DEFAULT);

        verify(buildDefinitionManager, times(1)).savePlanAndDefinition(eq(job), any(BuildDefinition.class));

        Assert.assertEquals(true, taskDefinition.isFinalising());

    }

}
