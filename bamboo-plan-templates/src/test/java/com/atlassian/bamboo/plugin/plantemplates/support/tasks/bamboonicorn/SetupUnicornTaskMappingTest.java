package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_SETUP_UNICORN;

@RunWith(MockitoJUnitRunner.class)
public class SetupUnicornTaskMappingTest
{
    @InjectMocks
    private SetupUnicornTaskMapping setupUnicornTaskMapping;

    @Test
    @SuppressWarnings("rawtypes")
    public void convertASetupUnicornTaskWithMinimalParamsReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_SETUP_UNICORN.getTaskKey())
                .put("description", "task description")
                .build();

        Map<String,String> taskActionParams = setupUnicornTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_SETUP_UNICORN.createTaskMappingKey())
                .put("cleanBeforeInstall", "false")
                .put("username", "admin")
                .put("password", "admin")
                .put("email", "admin@atlassian.com")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }

    @Test
    @SuppressWarnings("rawtypes")
    public void convertASetupUnicornTaskReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_SETUP_UNICORN.getTaskKey())
                .put("description", "task description")
                .put("cleanBeforeInstall", "true")
                .put("username", "ondemand-user")
                .put("password", "sesame")
                .put("email", "user@atlassian.com")
                .put("unicornInstance", "test-unicorn-instance.jira-dev.com")
                .put("version", "any-icebat")
                .put("localYaml", "anyyamls")
                .put("source", "fake/source")
                .put("destination", "unknown")
                .put("gappsDomain", "random-gapps")
                .put("license", "no")
                .put("halTimeout", "123")
                .build();

        Map<String,String> taskActionParams = setupUnicornTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_SETUP_UNICORN.createTaskMappingKey())
                .put("cleanBeforeInstall", "true")
                .put("username", "ondemand-user")
                .put("password", "sesame")
                .put("email", "user@atlassian.com")
                .put("unicornInstance", "test-unicorn-instance.jira-dev.com")
                .put("version", "any-icebat")
                .put("localYaml", "anyyamls")
                .put("source", "fake/source")
                .put("destination", "unknown")
                .put("gappsDomain", "random-gapps")
                .put("license", "no")
                .put("halTimeout", "123")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }
}
