package com.atlassian.bamboo.plugin.plantemplates.support;


import com.atlassian.bamboo.build.DefaultJob;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementImpl;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementSetImpl;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Map;


@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("rawtypes")
public class RequirementsServiceTest
{
    @InjectMocks
    private RequirementsService requirementsService;

    @Test
    public void performChangesAddsOneRequirement() {
        Map requirement = ImmutableMap.builder()
                .put("key", "ciagent")
                .put("value", "true")
                .put("condition", "equals")
                .build();

        Job job = new DefaultJob();

        requirementsService.performChanges(requirement, job);

        RequirementImpl expectedRequirement = new RequirementImpl("ciagent", false, "true");

        Assert.assertTrue(job.getRequirementSet().getRequirements().contains(expectedRequirement));
    }


    @Test
    public void performChangesAddsMultipleRequirements() {
        Map requirement = ImmutableMap.builder()
                .put("key", "ciagent")
                .put("value", "true")
                .put("condition", "equals")
                .build();

        Map requirement2 = ImmutableMap.builder()
                .put("key", "os")
                .put("value", "linux*")
                .put("condition", "match")
                .build();

        List requirements = ImmutableList.builder()
                .add(requirement)
                .add(requirement2)
                .build();

        Job job = new DefaultJob();

        requirementsService.performChanges(requirements, job);

        RequirementImpl expectedRequirement = new RequirementImpl("ciagent", false, "true");
        RequirementImpl expectedRequirement2 = new RequirementImpl("os", true, "linux*");

        Assert.assertTrue(job.getRequirementSet().getRequirements().contains(expectedRequirement));
        Assert.assertTrue(job.getRequirementSet().getRequirements().contains(expectedRequirement2));
    }

    @Test
    public void performChangesAddsExistConditionRequirement() {
        Map requirement = ImmutableMap.builder()
                .put("key", "ciagent")
                .put("condition", "exists")
                .build();

        Job job = new DefaultJob();

        requirementsService.performChanges(requirement, job);

        RequirementImpl expectedRequirement = new RequirementImpl("ciagent", true, ".*");

        Assert.assertTrue(job.getRequirementSet().getRequirements().contains(expectedRequirement));
    }

    @Test
    public void performChangesHandlesEmptyRequirements() {

        RequirementSetImpl requirementSet = new RequirementSetImpl();
        requirementSet.addRequirement(new RequirementImpl("ciagent", true, ".*"));

        Job job = new DefaultJob();
        job.setRequirementSet(requirementSet);

        requirementsService.performChanges(null, job);

        Assert.assertTrue(job.getRequirementSet().getRequirements().isEmpty());
    }

}
