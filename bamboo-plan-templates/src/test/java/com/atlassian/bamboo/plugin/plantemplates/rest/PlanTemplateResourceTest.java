package com.atlassian.bamboo.plugin.plantemplates.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plugin.plantemplates.PlanTemplateService;
import com.atlassian.bamboo.plugin.plantemplates.support.TemplateValidationException;
import com.atlassian.bamboo.variable.VariableDefinition;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlanTemplateResourceTest {
    
    @InjectMocks
    private PlanTemplateResource planTemplateResource;
    
    @Mock
    private PlanTemplateService service;
    @Mock
    private PlanManager planManager;
    
    
    @Test
    public void runDslReturnsA200WhenItsSuccessfull() {
        when(service.runPlanTemplate("TEST")).thenReturn(Maps.<String, Object>newHashMap());
        
        Response response = planTemplateResource.runDsl("TEST");
        
        assertEquals(200, response.getStatus()); 
        verify(service, times(1)).runPlanTemplate("TEST");
    }
    

    @Test
    public void runDslReturnsA400WhenAValidationErrorIsThrow() {
        when(service.runPlanTemplate("TEST")).thenThrow(new TemplateValidationException("message", Lists.newArrayList("error1", "error2")));
        
        Response response = planTemplateResource.runDsl("TEST");
        
        assertEquals(400, response.getStatus()); 
        assertEquals("TemplateValidationException: message\r\n\terror1\r\n\terror2\r\n",response.getEntity());
        verify(service, times(1)).runPlanTemplate("TEST");
    }
    
    
    @Test
    public void runDslReturnsTheWarningsAndTheStatus() {
        
        Map<String, Object> execution = Maps.newHashMap();

        ArrayList<String> warnings = Lists.newArrayList("warn1", "warn2");
        execution.put("warnings", warnings);
        
        when(service.runPlanTemplate("TEST")).thenReturn(execution);
        
        Response response = planTemplateResource.runDsl("TEST");
        
        assertEquals(200, response.getStatus()); 
        
        Map<String, Object> entity = (Map<String, Object>) response.getEntity();
        assertEquals(warnings, entity.get("warnings"));
        verify(service, times(1)).runPlanTemplate("TEST");
    }
    
    @Test
    public void runDslWithShortcutsReturnsTheWarningsAndTheStatus() {
        
        Map<String, Object> execution = Maps.newHashMap();

        ArrayList<String> warnings = Lists.newArrayList("warn1", "warn2");
        execution.put("warnings", warnings);
        
        when(service.runPlanTemplate("TEST1","TEST2")).thenReturn(execution);
        
        Response response = planTemplateResource.runDslWithShortcuts("{\"dsl\": \"TEST1\", \"shortcuts\": \"TEST2\"}");
        
        assertEquals(200, response.getStatus()); 
        
        
        Map<String, Object> entity = (Map<String, Object>) response.getEntity();
        assertEquals(warnings, entity.get("warnings"));
        verify(service, times(1)).runPlanTemplate("TEST1","TEST2");
    }
    
    
    
    @Test
    public void runDslWithShortcutsReturnsA200WhenItsSuccessfull() {
        when(service.runPlanTemplate("TEST1","TEST2")).thenReturn(Maps.<String, Object>newHashMap());
        
        Response response = planTemplateResource.runDslWithShortcuts("{\"dsl\": \"TEST1\", \"shortcuts\": \"TEST2\"}");
        
        assertEquals(200, response.getStatus()); 
        verify(service, times(1)).runPlanTemplate("TEST1","TEST2");
    }
    
    
    
    @Test
    public void runDslWithShortcutsReturnsA400WhenAValidationErrorIsThrow() {
        when(service.runPlanTemplate("TEST1","TEST2")).thenThrow(new TemplateValidationException("message", Lists.newArrayList("error1", "error2")));
        
        Response response = planTemplateResource.runDslWithShortcuts("{\"dsl\": \"TEST1\", \"shortcuts\": \"TEST2\"}");
        
        assertEquals(400, response.getStatus()); 
        assertEquals("TemplateValidationException: message\r\n\terror1\r\n\terror2\r\n",response.getEntity());
    }


    @Test
    public void expandDslReturnsA200WhenItsSuccessfull() {
        when(service.expandDsl("TEST1","TEST2")).thenReturn("ok");

        Response response = planTemplateResource.expandDsl("{\"dsl\": \"TEST1\", \"shortcuts\": \"TEST2\"}");

        assertEquals(200, response.getStatus());
        verify(service, times(1)).expandDsl("TEST1", "TEST2");
    }

    @SuppressWarnings("unchecked")
    @Test
      public void planTemplateVariablesReturnsPlanTemplateVariables() throws Exception {
        final String planKey = "projectkey-plankey";
        final String expectedVariableName = "plantemplate.repository.url";
        final String expectedVariableValue = "http://www.gitrepo.com/repo.git";

        VariableDefinition variableDefinition = mock(VariableDefinition.class);
        Chain chain = mock(Chain.class);
        when(variableDefinition.getKey()).thenReturn(expectedVariableName);
        when(variableDefinition.getValue()).thenReturn(expectedVariableValue);
        when(chain.getVariables()).thenReturn(Collections.singletonList(variableDefinition));
        when(planManager.getPlanByKey(PlanKeys.getPlanKey(planKey))).thenReturn(chain);

        Response response = planTemplateResource.getPlanTemplateAdditionalVariables(planKey);

        assertEquals(200, response.getStatus());
        assertThat(response.getEntity(), instanceOf(Map.class));
        assertThat((Map<String, String>) response.getEntity(), hasEntry(expectedVariableName, expectedVariableValue));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void planTemplateVariablesReturnsOnlyPlanTemplateVariables() throws Exception {
        final String planKey = "projectkey-plankey";
        final String expectedVariableName = "plantemplate.expected.variable.name";
        final String expectedVariableValue = "expectedValue";
        final String notExpectedVariableName = "not.expected.variable.name";
        final String notExpectedVariableValue = "notExpectedValue";

        VariableDefinition expectedVariableDefinition = mock(VariableDefinition.class);
        VariableDefinition notExpectedVariableDefinition = mock(VariableDefinition.class);
        Chain chain = mock(Chain.class);
        when(expectedVariableDefinition.getKey()).thenReturn(expectedVariableName);
        when(expectedVariableDefinition.getValue()).thenReturn(expectedVariableValue);
        when(notExpectedVariableDefinition.getKey()).thenReturn(notExpectedVariableName);
        when(notExpectedVariableDefinition.getValue()).thenReturn(notExpectedVariableValue);
        when(chain.getVariables()).thenReturn(Arrays.asList(expectedVariableDefinition, notExpectedVariableDefinition));
        when(planManager.getPlanByKey(PlanKeys.getPlanKey(planKey))).thenReturn(chain);

        Response response = planTemplateResource.getPlanTemplateAdditionalVariables(planKey);

        assertEquals(200, response.getStatus());
        assertThat(response.getEntity(), is(instanceOf(Map.class)));
        Map<String, String> variables = (Map<String, String>) response.getEntity();
        assertThat(variables.entrySet(), hasSize(1));
        assertThat(variables, hasEntry(expectedVariableName, expectedVariableValue));
    }
    @Test
    public void planTemplateVariablesReturns404IfPlanNotFound() throws Exception {
        Response response = planTemplateResource.getPlanTemplateAdditionalVariables("fakeProjectKey-fakePlanKey");
        assertEquals(404, response.getStatus());
    }

    @Test
    public void planTemplateVariablesReturns400ifPlanKeyIsMalformed() throws Exception {
        final String planKey = "projectkey";

        Response response = planTemplateResource.getPlanTemplateAdditionalVariables(planKey);

        assertEquals(400, response.getStatus());
    }
    
    
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void validationRulesHasTheProperEntries() throws Exception {
        Response response = planTemplateResource.validationRules();

        assertEquals(200, response.getStatus());
        assertThat(response.getEntity(), is(instanceOf(List.class)));
        
        List<Map> rules = (List<Map>) response.getEntity();
        Assert.assertFalse(rules.isEmpty());
        
        Map node = (Map) rules.get(0);
        Assert.assertNotNull(node);
        
    }


    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void prettyPrintingForValidationRulesHasTheProperEntries() throws Exception {
        Response response = planTemplateResource.prettyPrintingValidationRules();

        assertEquals(200, response.getStatus());
        assertThat(response.getEntity(), is(instanceOf(String.class)));

        String rules = (String) response.getEntity();
        Assert.assertTrue(rules.contains(" stage(name, description, manual){\n" +
                "  job(key, name, description, enabled){"));

        System.out.print(rules);

    }
    
}
