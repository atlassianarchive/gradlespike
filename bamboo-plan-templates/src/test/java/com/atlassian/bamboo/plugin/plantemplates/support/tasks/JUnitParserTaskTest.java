package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;

@RunWith(MockitoJUnitRunner.class)
public class JUnitParserTaskTest
{
    @InjectMocks
    private JUnitParserTaskMapping jUnitParserTaskMapping;

    @Test
    @SuppressWarnings("rawtypes")
    public void convertAJUnitTaskReturnsCorrectMap(){
        Map task = ImmutableMap.builder()
                .put("type", "jUnitParser")
                .put("description", "task description")
                .put("resultsDirectory", "**/*blah.xml")
                .build();

        Map<String,String> taskActionParams = jUnitParserTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("userDescription", "task description")
                .put("taskDisabled", "false")
                .put("createTaskKey", "com.atlassian.bamboo.plugins.testresultparser:task.testresultparser.junit")
                .put("testResultsDirectory", "**/*blah.xml")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }

    @Test
    @SuppressWarnings("rawtypes")
    public void convertAJUnitTaskWithDefaultLocationReturnsCorrectMap(){
        Map task = ImmutableMap.builder()
                .put("type", "jUnitParser")
                .put("description", "task description")
                .build();

        Map<String,String> taskActionParams = jUnitParserTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("userDescription", "task description")
                .put("taskDisabled", "false")
                .put("createTaskKey", "com.atlassian.bamboo.plugins.testresultparser:task.testresultparser.junit")
                .put("testResultsDirectory", "**/test-reports/*.xml")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }
}
