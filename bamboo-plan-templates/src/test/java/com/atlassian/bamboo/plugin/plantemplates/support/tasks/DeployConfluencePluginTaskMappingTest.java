package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import com.atlassian.bamboo.plugin.plantemplates.support.tasks.deployTask.Crypto;
import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DeployConfluencePluginTaskMappingTest
{
    
    @Mock
    private Crypto crypto;
    
    @InjectMocks
    private DeployConfluencePluginTaskMapping confluencePluginTaskMapping;

//    userDescription:testDeployTask
//    checkBoxFields:taskDisabled
//    confDeployJar:Plugin Jar
//    selectFields:confDeployJar
//    confDeployURL:http://localhost:1990/confluence
//    confDeployUsername:usernameValue
//    confDeployPassword:passValue  <- This is encrypt before created!!!!
//    deployBranchEnabled:true
//    checkBoxFields:deployBranchEnabled
//    createTaskKey:com.atlassian.bamboo.plugins.deploy.continuous-plugin-deployment:confdeploy
//    confDeployPasswordVariable
//    confDeployPasswordVariableCheck


    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaskWithMinimalDataReturnsCorrectMap(){

        confluencePluginTaskMapping.setCrypto(crypto);
        when(crypto.encrypt("passValue")).thenReturn(ImmutableMap.of("key", "cryptKey", "encrypted", "cryptPass"));

        Map task = ImmutableMap.builder()
            .put("type", "deployConfluencePlugin")
            .put("description", "testDeployPlugin")
            .put("url", "http://mybamboo:1990/bamboo")
            .put("username", "usernameValue")
            .put("password", "passValue")
            .put("artifact", "artifactName")
            .build();

        Map<String,String> taskActionParams = confluencePluginTaskMapping.convert(task);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("userDescription", "testDeployPlugin")
            .put("taskDisabled", "false")
            .put("confDeployUsername", "usernameValue")
            .put("confDeployPassword", "cryptPass")
            .put("confDeployKey", "cryptKey")
            .put("confDeployURL", "http://mybamboo:1990/bamboo")
            .put("confDeployJar", "artifactName")
            .put("pluginInstallationTimeout", "90")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.deploy.continuous-plugin-deployment:confdeploy")
            .build(); 
        
        Assert.assertEquals(expected, taskActionParams);
    }
    
    
    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaskWithPasswordVariableDataReturnsCorrectMap(){

        confluencePluginTaskMapping.setCrypto(crypto);
        when(crypto.encrypt("passValue")).thenReturn(ImmutableMap.of("key", "cryptKey", "encrypted", "cryptPass"));

        Map task = ImmutableMap.builder()
            .put("type", "deployConfluencePlugin")
            .put("description", "testDeployPlugin")
            .put("url", "http://mybamboo:1990/bamboo")
            .put("username", "usernameValue")
            .put("passwordVariable", "${bamboo.password}")
            .put("artifact", "artifactName")
            .build();

        Map<String,String> taskActionParams = confluencePluginTaskMapping.convert(task);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("userDescription", "testDeployPlugin")
            .put("taskDisabled", "false")
            .put("confDeployUsername", "usernameValue")
            .put("confDeployURL", "http://mybamboo:1990/bamboo")
            .put("confDeployJar", "artifactName")
            .put("confDeployPasswordVariable", "${bamboo.password}")
            .put("confDeployPasswordVariableCheck", "true")
            .put("pluginInstallationTimeout", "90")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.deploy.continuous-plugin-deployment:confdeploy")
            .build(); 
        
        Assert.assertEquals(expected, taskActionParams);
    }
    
    
    
    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaskWithOptionalDataReturnsCorrectMap(){
        confluencePluginTaskMapping.setCrypto(crypto);
        when(crypto.encrypt("passValue")).thenReturn(ImmutableMap.of("key", "cryptKey", "encrypted", "cryptPass"));

        Map task = ImmutableMap.builder()
            .put("type", "deployConfluencePlugin")
            .put("description", "testDeployPlugin")
            .put("url", "http://mybamboo:1990/bamboo")
            .put("username", "usernameValue")
            .put("password", "passValue")
            .put("artifact", "artifactName")
            .put("runOnBranch", "true")
            .build();

        Map<String,String> taskActionParams = confluencePluginTaskMapping.convert(task);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("userDescription", "testDeployPlugin")
            .put("taskDisabled", "false")
            .put("confDeployUsername", "usernameValue")
            .put("confDeployPassword", "cryptPass")
            .put("confDeployKey", "cryptKey")
            .put("confDeployURL", "http://mybamboo:1990/bamboo")
            .put("confDeployJar", "artifactName")
            .put("deployBranchEnabled", "true")
            .put("pluginInstallationTimeout", "90")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.deploy.continuous-plugin-deployment:confdeploy")
            .build(); 
        Assert.assertEquals(expected, taskActionParams);
        
    }
    
}
