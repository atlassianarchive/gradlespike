package com.atlassian.bamboo.plugin.plantemplates.support.triggers;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;

@RunWith(MockitoJUnitRunner.class)
public class AfterSuccessfulPlanTriggerMappingTest
{
    @InjectMocks
    private AfterSuccessfulPlanTriggerMapping afterSuccessfulPlanTriggerMapping;
    
    @SuppressWarnings("rawtypes")
    @Test
    public void convertACronTriggerReturnsCorrectMap() {
        //  trigger(description:'triggerAfter', type:'afterSuccessfulPlan', planKey: 'PERSONAL-REST')

        Map trigger = ImmutableMap.builder()
            .put("description", "trigger1")
            .put("type", "afterSuccessfulPlan")
            .put("planKey", "PERSONAL-REST")
            .build();  

        Map<String, String> result = afterSuccessfulPlanTriggerMapping.convert(trigger);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("selectedBuildStrategy", "afterSuccessfulPlan")
            .put("triggeringPlan", "PERSONAL-REST")
            .build();
        
        Assert.assertEquals(expected, result);
        
    }
}
