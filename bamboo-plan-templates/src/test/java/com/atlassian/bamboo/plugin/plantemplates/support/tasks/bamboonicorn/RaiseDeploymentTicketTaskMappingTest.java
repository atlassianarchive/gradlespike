package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_RAISE_DEPLOYMENT_TICKET;

@RunWith(MockitoJUnitRunner.class)
public class RaiseDeploymentTicketTaskMappingTest
{
    @InjectMocks
    private RaiseDeploymentTicketTaskMapping raiseDeploymentTicketTaskMapping;

    @Test
    @SuppressWarnings("rawtypes")
    public void convertARaiseDeploymentTicketTaskWithMinimalParamsReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_RAISE_DEPLOYMENT_TICKET.getTaskKey())
                .put("description", "task description")
                .build();

        Map<String,String> taskActionParams = raiseDeploymentTicketTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_RAISE_DEPLOYMENT_TICKET.createTaskMappingKey())
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }

    @Test
    @SuppressWarnings("rawtypes")
    public void convertARaiseDeploymentTicketTaskReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_RAISE_DEPLOYMENT_TICKET.getTaskKey())
                .put("description", "task description")
                .put("artifactName", "fakeArtifact")
                .put("artifactVersion", "2.1")
                .put("artifactKey", "artifact-key")
                .put("pluginKey", "plugin:key")
                .put("groupId", "fake-group")
                .put("deploymentEnvironment", "PROD")
                .put("sdogUsername", "awang")
                .put("sdogPassword", "nicetry")
                .build();

        Map<String,String> taskActionParams = raiseDeploymentTicketTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_RAISE_DEPLOYMENT_TICKET.createTaskMappingKey())
                .put("artifactName", "fakeArtifact")
                .put("artifactVersion", "2.1")
                .put("artifactKey", "artifact-key")
                .put("pluginKey", "plugin:key")
                .put("groupId", "fake-group")
                .put("deploymentEnvironment", "PROD")
                .put("sdogUsername", "awang")
                .put("sdogPassword", "nicetry")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }
}
