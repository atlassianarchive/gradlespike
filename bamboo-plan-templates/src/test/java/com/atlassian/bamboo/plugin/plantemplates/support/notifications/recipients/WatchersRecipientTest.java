package com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients;

import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.WatcherRecipientMapping;
import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class WatchersRecipientTest
{
    @InjectMocks
    private WatcherRecipientMapping watcherRecipientMapping;

    @SuppressWarnings("rawtypes")
    @Test
    public void userMappingReturnsCorrectData(){
        Map notification = ImmutableMap.builder()
                .put("recipient", "watchers")
                .put("type", "Build Complete")
                .build();

        String recipientKey = watcherRecipientMapping.getRecipientKey();
        String recipientData = watcherRecipientMapping.getRecipientData(notification);

        Assert.assertEquals("com.atlassian.bamboo.plugin.system.notifications:recipient.watcher", recipientKey);
        Assert.assertEquals("", recipientData);
    }
}