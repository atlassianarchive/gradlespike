package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.variable.VariableConfigurationService;
import com.atlassian.bamboo.variable.VariableDefinition;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("rawtypes")
public class VariableServiceTest
{
    
    @InjectMocks
    private VariableService variableService;
    @Mock
    private VariableConfigurationService variableConfigurationService;
    
    @Test
    public void performVariableChangesCreatesAPlanVariable(){
        Map variable1 = ImmutableMap.builder()
            .put("key", "key1")
            .put("value", "value1")
            .build();

        Chain chain = mock(Chain.class);
        when(chain.getVariables()).thenReturn(Lists.<VariableDefinition>newArrayList());
        
        variableService.performVariableChanges(chain, variable1);
        
        verify(variableConfigurationService, times(1)).createPlanVariable(chain, "key1", "value1");
    }

    @Test
    public void performVariableChangesCreatesMultiplePlanVariables(){
        Map variable1 = ImmutableMap.builder()
            .put("key", "key1")
            .put("value", "value1")
            .build();

        Map variable2 = ImmutableMap.builder()
            .put("key", "key2")
            .put("value", "value2")
            .build();
        
        List variables = Lists.newArrayList(variable1, variable2);
        
        Chain chain = mock(Chain.class);
        when(chain.getVariables()).thenReturn(Lists.<VariableDefinition>newArrayList());
        
        variableService.performVariableChanges(chain, variables);
        
        verify(variableConfigurationService, times(1)).createPlanVariable(chain, "key1", "value1");
        verify(variableConfigurationService, times(1)).createPlanVariable(chain, "key2", "value2");
    }
    
    
    @Test
    public void performVariableChangesDeletesExistingPlanVariables(){
        Map variable1 = ImmutableMap.builder()
            .put("key", "key1")
            .put("value", "value1")
            .build();

        Map variable2 = ImmutableMap.builder()
            .put("key", "key2")
            .put("value", "value2")
            .build();
        
        List variables = Lists.newArrayList(variable1, variable2);
        
        Chain chain = mock(Chain.class);
        
        VariableDefinition variableDefinition1 = mock(VariableDefinition.class);
        VariableDefinition variableDefinition2 = mock(VariableDefinition.class);
        
        when(chain.getVariables()).thenReturn(Lists.newArrayList(variableDefinition1, variableDefinition2));
        
        variableService.performVariableChanges(chain, variables);
        
        verify(variableConfigurationService, times(1)).deleteVariableDefinition(variableDefinition1);
        verify(variableConfigurationService, times(1)).deleteVariableDefinition(variableDefinition2);
        
        verify(variableConfigurationService, times(1)).createPlanVariable(chain, "key1", "value1");
        verify(variableConfigurationService, times(1)).createPlanVariable(chain, "key2", "value2");
    }


    
    
    
    
}
