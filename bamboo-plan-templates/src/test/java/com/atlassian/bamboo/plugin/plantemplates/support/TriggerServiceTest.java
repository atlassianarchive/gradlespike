package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.build.strategy.BuildStrategy;
import com.atlassian.bamboo.build.strategy.BuildStrategyConfigurationService;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plugin.plantemplates.support.triggers.TriggerMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.triggers.TriggerMappingFactory;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class TriggerServiceTest
{
    
    @InjectMocks
    private TriggerService triggerService;

    @Mock
    private BuildStrategyConfigurationService buildStrategyConfigurationService;
    @Mock
    private RepositoryService repositoryService;
    @Mock
    private TriggerMappingFactory triggerMappingFactory;

    @Test
    @SuppressWarnings("rawtypes")
    public void performTriggerChangesCreatesSingleTriggers(){
        
       Map trigger1 = ImmutableMap.builder()
           .put("description", "trigger1")
           .put("type", "cron")
           .build();
       
       Chain plan = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
       PlanKey planKey = PlanKeys.getPlanKey("projectKey-planKey");
       when(plan.getPlanKey()).thenReturn(planKey);
       when(triggerMappingFactory.create(anyString())).thenReturn(mock(TriggerMapping.class));
        
       triggerService.performTriggerChanges(plan, trigger1);

       verify(buildStrategyConfigurationService, times(1)).createBuildStrategy(eq(plan.getPlanKey()), eq("trigger1"), eq(Sets.<Long>newHashSet()), any(BuildConfiguration.class));
    }
    
    @Test
    @SuppressWarnings("rawtypes")
    public void performTriggerChangesCreatesMultipleTriggers(){
       Map trigger1 = ImmutableMap.builder()
           .put("description", "trigger1")
           .put("type", "cron")
           .build();
       Map trigger2 = ImmutableMap.builder()
           .put("description", "trigger2")
           .put("type", "daily")
           .build();
       
       List triggers = ImmutableList.builder()
           .add(trigger1)
           .add(trigger2)
           .build();
       
       Chain plan = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
       PlanKey planKey = PlanKeys.getPlanKey("projectKey-planKey");
       when(plan.getPlanKey()).thenReturn(planKey);
       when(triggerMappingFactory.create(anyString())).thenReturn(mock(TriggerMapping.class));
        
       triggerService.performTriggerChanges(plan, triggers);
       
       verify(buildStrategyConfigurationService, times(1)).createBuildStrategy(eq(plan.getPlanKey()), eq("trigger1"), eq(Sets.<Long>newHashSet()), any(BuildConfiguration.class));
       verify(buildStrategyConfigurationService, times(1)).createBuildStrategy(eq(plan.getPlanKey()), eq("trigger2"), eq(Sets.<Long>newHashSet()), any(BuildConfiguration.class));
        
    }
    
    @Test
    @SuppressWarnings("rawtypes")
    public void performTriggerChangesDeletesExistingTriggers()
    {
        Map trigger1 = ImmutableMap.builder().put("description", "trigger1").put("type", "cron").build();
        Map trigger2 = ImmutableMap.builder().put("description", "trigger2").put("type", "daily").build();
        List triggers = ImmutableList.builder().add(trigger1).add(trigger2).build();
        
        //Existing Triggers
        BuildStrategy oldTrigger1 = mock(BuildStrategy.class);
        BuildStrategy oldTrigger2 = mock(BuildStrategy.class);
        when(oldTrigger1.getId()).thenReturn(1L);
        when(oldTrigger2.getId()).thenReturn(2L);
        List<BuildStrategy> oldTriggers = Lists.newArrayList(oldTrigger1, oldTrigger2);
       
        Chain plan = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
        PlanKey planKey = PlanKeys.getPlanKey("projectKey-planKey");
        when(plan.getPlanKey()).thenReturn(planKey);
        when(plan.getBuildDefinition().getBuildStrategies()).thenReturn(oldTriggers);
        when(triggerMappingFactory.create(anyString())).thenReturn(mock(TriggerMapping.class));

        triggerService.performTriggerChanges(plan, triggers);

        verify(buildStrategyConfigurationService, times(1)).deleteBuildStrategy(planKey, 1L);
        verify(buildStrategyConfigurationService, times(1)).deleteBuildStrategy(planKey, 2L);

    }

    @Test
    @SuppressWarnings("rawtypes")
    public void performTriggerChangesShouldUseTheRightRepositories(){
        Map repository1 = ImmutableMap.builder()
             .put("name", "repositoryName1").build();
        
        Map repository2 = ImmutableMap.builder()
            .put("name", "repositoryName2").build();
         
        Map trigger1 = ImmutableMap.builder()
            .put("description", "trigger1")
            .put("type", "cron")
            .put("repository", Lists.newArrayList(repository1, repository2))
            .build();
        Map trigger2 = ImmutableMap.builder()
            .put("description", "trigger2")
            .put("type", "daily")
            .put("repository", repository1)
            .build();
        
        List triggers = ImmutableList.builder()
            .add(trigger1)
            .add(trigger2)
            .build();
        
        Chain plan = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
        PlanKey planKey = PlanKeys.getPlanKey("projectKey-planKey");
        when(plan.getPlanKey()).thenReturn(planKey);
        
        when(repositoryService.findRespositoryIdByName("repositoryName1")).thenReturn(1L);
        when(repositoryService.findRespositoryIdByName("repositoryName2")).thenReturn(2L);
        when(triggerMappingFactory.create(anyString())).thenReturn(mock(TriggerMapping.class));
         
        triggerService.performTriggerChanges(plan, triggers);
        
        verify(buildStrategyConfigurationService, times(1)).createBuildStrategy(
                                                    eq(plan.getPlanKey()), 
                                                    eq("trigger1"), 
                                                    eq(Sets.<Long>newHashSet(1L, 2L)), 
                                                    any(BuildConfiguration. class)
                                                );
        
        verify(buildStrategyConfigurationService, times(1)).createBuildStrategy(
                                                    eq(plan.getPlanKey()), 
                                                    eq("trigger2"), 
                                                    eq(Sets.<Long>newHashSet(1L)), 
                                                    any(BuildConfiguration. class)
                                                );
    }    
    
    
    
    @Test
    @SuppressWarnings("rawtypes")
    public void performTriggerChangesShouldUseTheRightTriggerType(){
        Map trigger1 = ImmutableMap.builder()
            .put("description", "trigger1")
            .put("type", "cron")
            .build();
        Map trigger2 = ImmutableMap.builder()
            .put("description", "trigger2")
            .put("type", "daily")
            .build();
        
        List triggers = ImmutableList.builder()
            .add(trigger1)
            .add(trigger2)
            .build();
        
        Chain plan = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
        PlanKey planKey = PlanKeys.getPlanKey("projectKey-planKey");
        when(plan.getPlanKey()).thenReturn(planKey);
        
        TriggerMapping triggerMapping1 = mock(TriggerMapping.class);
        TriggerMapping triggerMapping2 = mock(TriggerMapping.class);
        
        when(triggerMappingFactory.create("cron")).thenReturn(triggerMapping1);
        when(triggerMappingFactory.create("daily")).thenReturn(triggerMapping2);
        
        triggerService.performTriggerChanges(plan, triggers);
        
        verify(triggerMapping1, times(1)).convert(trigger1);
        verify(triggerMapping2, times(1)).convert(trigger2);
    }    
    
    
    
    @Test
    public void performTriggerChangesDeletesExistingTriggersIfNotTriggerRepresentationIsPassed()
    {
        //Existing Triggers
        BuildStrategy oldTrigger1 = mock(BuildStrategy.class);
        BuildStrategy oldTrigger2 = mock(BuildStrategy.class);
        when(oldTrigger1.getId()).thenReturn(1L);
        when(oldTrigger2.getId()).thenReturn(2L);
        List<BuildStrategy> oldTriggers = Lists.newArrayList(oldTrigger1, oldTrigger2);
       
        Chain plan = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
        PlanKey planKey = PlanKeys.getPlanKey("projectKey-planKey");
        when(plan.getPlanKey()).thenReturn(planKey);
        when(plan.getBuildDefinition().getBuildStrategies()).thenReturn(oldTriggers);
        when(triggerMappingFactory.create(anyString())).thenReturn(mock(TriggerMapping.class));

        triggerService.performTriggerChanges(plan, null);

        verify(buildStrategyConfigurationService, times(1)).deleteBuildStrategy(planKey, 1L);
        verify(buildStrategyConfigurationService, times(1)).deleteBuildStrategy(planKey, 2L);

    }

    
}
