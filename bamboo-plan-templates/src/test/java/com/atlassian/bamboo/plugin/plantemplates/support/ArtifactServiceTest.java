package com.atlassian.bamboo.plugin.plantemplates.support;


import com.atlassian.bamboo.build.DefaultJob;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.chains.DefaultChain;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinition;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionImpl;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionManager;
import com.atlassian.bamboo.plan.artifact.ArtifactSubscription;
import com.atlassian.bamboo.plan.artifact.ArtifactSubscriptionImpl;
import com.atlassian.bamboo.plan.artifact.ArtifactSubscriptionManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings({ "rawtypes", "unchecked" })
public class  ArtifactServiceTest
{
    @InjectMocks
    private ArtifactService artifactService;

    @Mock
    private ArtifactDefinitionManager artifactDefinitionManager;

    @Mock
    private ArtifactSubscriptionManager artifactSubscriptionManager;

    @Test
    public void performDefinitionChangesAddsOneDefinition() {

        Map artifactDefinition = ImmutableMap.builder()
                .put("name", "artifactName")
                .put("pattern", "**/*.txt")
                .put("location", "./")
                .put("shared", "false")
                .build();

        Job job = mock(DefaultJob.class);
        when(job.getArtifactDefinitions()).thenReturn(Lists.<ArtifactDefinition>newArrayList());

        artifactService.performDefinitionChanges(artifactDefinition, job);

        verify(artifactDefinitionManager, times(1)).saveArtifactDefinitions(any(List.class));
        verify(artifactDefinitionManager, times(0)).removeArtifactDefinition(any(ArtifactDefinition.class));
    }

    @Test
    public void performDefinitionChangesAddsManyDefinitions() {

        Map artifactDefinition1 = ImmutableMap.builder()
                .put("name", "artifactName")
                .put("pattern", "**/*.txt")
                .put("location", "./")
                .put("shared", "false")
                .build();

        Map artifactDefinition2 = ImmutableMap.builder()
                .put("name", "artifactName2")
                .put("pattern", "**/*.txt")
                .put("location", "./")
                .put("shared", "false")
                .build();

        Map artifactDefinition3 = ImmutableMap.builder()
                .put("name", "artifactName3")
                .put("pattern", "**/*.txt")
                .put("location", "./")
                .put("shared", "true")
                .build();

        List definition = ImmutableList.builder()
                .add(artifactDefinition1)
                .add(artifactDefinition2)
                .add(artifactDefinition3)
                .build();

        Job job = mock(DefaultJob.class);
        when(job.getArtifactDefinitions()).thenReturn(Lists.<ArtifactDefinition>newArrayList());

        artifactService.performDefinitionChanges(definition, job);

        verify(artifactDefinitionManager, times(1)).saveArtifactDefinitions(any(List.class));
        verify(artifactDefinitionManager, times(0)).removeArtifactDefinition(any(ArtifactDefinition.class));
    }

    @Test
    public void performDefinitionChangesUpdatesOneDefinition() {

        Map artifactDefinition = ImmutableMap.builder()
                .put("name", "artifactName")
                .put("pattern", "**/*.jar")
                .put("location", "./")
                .put("shared", "false")
                .build();

        Job job = mock(DefaultJob.class);
        ArtifactDefinition existing = new ArtifactDefinitionImpl("artifactName", "./", "**/*.txt");
        existing.setProducerJob(job);
        existing.setSharedArtifact(false);
        when(job.getArtifactDefinitions()).thenReturn(Lists.newArrayList(existing));

        artifactService.performDefinitionChanges(artifactDefinition, job);

        verify(artifactDefinitionManager, times(1)).saveArtifactDefinitions(any(List.class));
        verify(artifactDefinitionManager, times(0)).removeArtifactDefinition(any(ArtifactDefinition.class));
    }

    @Test
    public void performDefinitionChangesUpdatesManyDefinitions() {

        Map artifactDefinition1 = ImmutableMap.builder()
                .put("name", "artifactName")
                .put("pattern", "**/*.jar")
                .put("location", "./")
                .put("shared", "false")
                .build();

        Map artifactDefinition2 = ImmutableMap.builder()
                .put("name", "secondArtifact")
                .put("pattern", "**/*.jar")
                .put("location", "./")
                .put("shared", "false")
                .build();

        List definitionMaps = ImmutableList.builder()
                .add(artifactDefinition1)
                .add(artifactDefinition2)
                .build();

        Job job = mock(DefaultJob.class);
        ArtifactDefinition existing1 = new ArtifactDefinitionImpl("artifactName", "./", "**/*.txt");
        existing1.setProducerJob(job);
        existing1.setSharedArtifact(false);

        ArtifactDefinition existing2 = new ArtifactDefinitionImpl("secondArtifact", "./", "**/*.txt");
        existing2.setProducerJob(job);
        existing2.setSharedArtifact(false);
        when(job.getArtifactDefinitions()).thenReturn(Lists.newArrayList(existing1, existing2));

        artifactService.performDefinitionChanges(definitionMaps, job);

        verify(artifactDefinitionManager, times(1)).saveArtifactDefinitions(any(List.class));
        verify(artifactDefinitionManager, times(0)).removeArtifactDefinition(any(ArtifactDefinition.class));
    }

    @Test
    public void performDefinitionChangesDeletesAllExistingOnEmptyInputDefinition() {

        Job job = mock(DefaultJob.class);
        ArtifactDefinition existing = new ArtifactDefinitionImpl("artifactName", "./", "**/*.txt");
        existing.setProducerJob(job);
        existing.setSharedArtifact(false);
        when(job.getArtifactDefinitions()).thenReturn(Lists.newArrayList(existing));

        artifactService.performDefinitionChanges(null, job);

        verify(artifactDefinitionManager, times(0)).saveArtifactDefinitions(any(List.class));
        verify(artifactDefinitionManager, times(1)).removeArtifactDefinition(existing);
    }

    @Test
    public void performDefinitionChangesHandlesEmptyInputDefinition() {

        Job job = mock(DefaultJob.class);

        artifactService.performDefinitionChanges(null, job);

        verify(artifactDefinitionManager, times(0)).saveArtifactDefinitions(any(List.class));
        verify(artifactDefinitionManager, times(0)).removeArtifactDefinition(any(ArtifactDefinition.class));
    }

    @Test
    public void performSubscriptionChangesAddsOneSubscription() {

        Map artifactDefinition = ImmutableMap.builder()
                .put("name", "artifactName")
                .put("destination", "./")
                .build();

        Chain chain = mock(DefaultChain.class);
        Job job = mock(DefaultJob.class);

        ArtifactDefinition existing = new ArtifactDefinitionImpl("artifactName", "./", "**/*.txt");
        existing.setProducerJob(job);
        existing.setSharedArtifact(false);

        when(job.getArtifactDefinitions()).thenReturn(Lists.<ArtifactDefinition>newArrayList());
        when(job.getParent()).thenReturn(chain);
        when(artifactDefinitionManager.findArtifactDefinitionByChain(chain, "artifactName")).thenReturn(existing);

        artifactService.performSubscriptionChanges(artifactDefinition, job);

        verify(artifactSubscriptionManager, times(1)).saveArtifactSubscriptions(any(List.class));
        verify(artifactSubscriptionManager, times(0)).removeArtifactSubscriptions(any(Collection.class));
    }

    @Test
    public void performSubscriptionChangesAddsManySubscription() {

        Map artifactSubscription1 = ImmutableMap.builder()
                .put("name", "artifactName")
                .put("destination", "./")
                .build();

        Map artifactSubscription2 = ImmutableMap.builder()
                .put("name", "secondArtifact")
                .put("destination", "./")
                .build();

        List subscriptions = ImmutableList.builder()
                .add(artifactSubscription1)
                .add(artifactSubscription2)
                .build();

        Chain chain = mock(DefaultChain.class);
        Job job = mock(DefaultJob.class);

        ArtifactDefinition producer1 = new ArtifactDefinitionImpl("artifactName", "./", "**/*.txt");
        producer1.setProducerJob(job);
        producer1.setSharedArtifact(false);

        ArtifactDefinition producer2 = new ArtifactDefinitionImpl("secondArtifact", "./", "**/*.txt");
        producer2.setProducerJob(job);
        producer2.setSharedArtifact(false);

        when(job.getArtifactDefinitions()).thenReturn(Lists.<ArtifactDefinition>newArrayList());
        when(job.getParent()).thenReturn(chain);
        when(artifactDefinitionManager.findArtifactDefinitionByChain(chain, "artifactName")).thenReturn(producer1);
        when(artifactDefinitionManager.findArtifactDefinitionByChain(chain, "secondArtifact")).thenReturn(producer2);

        artifactService.performSubscriptionChanges(subscriptions, job);

        verify(artifactSubscriptionManager, times(1)).saveArtifactSubscriptions(any(List.class));
        verify(artifactSubscriptionManager, times(0)).removeArtifactSubscriptions(any(Collection.class));
    }

    @Test
    public void performSubscriptionChangesUpdatesOneSubscription() {

        Map artifactDefinition = ImmutableMap.builder()
                .put("name", "artifactName")
                .put("destination", "./subDir/")
                .build();

        Chain chain = mock(DefaultChain.class);
        Job job = mock(DefaultJob.class);

        ArtifactDefinition producer = new ArtifactDefinitionImpl("artifactName", "./", "**/*.txt");
        producer.setProducerJob(job);
        producer.setSharedArtifact(false);

        ArtifactSubscription existing = new ArtifactSubscriptionImpl(producer, job, "./");

        when(job.getArtifactDefinitions()).thenReturn(Lists.<ArtifactDefinition>newArrayList());
        when(job.getParent()).thenReturn(chain);
        when(artifactDefinitionManager.findArtifactDefinitionByChain(chain, "artifactName")).thenReturn(producer);
        when(job.getArtifactSubscriptions()).thenReturn(Lists.newArrayList(existing));

        artifactService.performSubscriptionChanges(artifactDefinition, job);

        verify(artifactSubscriptionManager, times(1)).saveArtifactSubscriptions(any(List.class));
        verify(artifactSubscriptionManager, times(0)).removeArtifactSubscriptions(any(Collection.class));
    }

    @Test
    public void performSubscriptionChangesHandlesEmptyInputDefinition() {

        Job job = mock(DefaultJob.class);

        artifactService.performSubscriptionChanges(null, job);

        verify(artifactDefinitionManager, times(0)).saveArtifactDefinitions(any(List.class));
        verify(artifactDefinitionManager, times(0)).removeArtifactDefinition(any(ArtifactDefinition.class));
    }

    @Test
    public void performSubscriptionChangesDeletesAllExistingOnEmptyInputSubscription() {

        Job job = mock(DefaultJob.class);
        ArtifactDefinition producer = new ArtifactDefinitionImpl("artifactName", "./", "**/*.txt");
        producer.setProducerJob(job);
        producer.setSharedArtifact(false);

        ArtifactSubscription existing = new ArtifactSubscriptionImpl(producer, job, "./");
        when(job.getArtifactDefinitions()).thenReturn(Lists.newArrayList(producer));
        when(job.getArtifactSubscriptions()).thenReturn(Lists.newArrayList(existing));

        artifactService.performSubscriptionChanges(null, job);

        verify(artifactSubscriptionManager, times(0)).saveArtifactSubscriptions(any(List.class));
        verify(artifactSubscriptionManager, times(1)).removeArtifactSubscriptions(any(List.class));
    }
}
