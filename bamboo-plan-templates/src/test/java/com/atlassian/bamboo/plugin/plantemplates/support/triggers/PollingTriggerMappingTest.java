package com.atlassian.bamboo.plugin.plantemplates.support.triggers;

import java.util.Map;
import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;

@RunWith(MockitoJUnitRunner.class)
public class PollingTriggerMappingTest
{
    @InjectMocks
    private PollingTriggerMapping pollingTriggerMapping;
    
    @SuppressWarnings("rawtypes")
    @Test
    public void convertForPeriodicallyReturnsCorrectMap() {
        
        Map trigger = ImmutableMap.builder()
            .put("description", "trigger1")
            .put("type", "polling")
            .put("strategy", "periodically")
            .put("frequency", "180")
            .build();  

        Map<String, String> result = pollingTriggerMapping.convert(trigger);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("selectedBuildStrategy", "poll")
            .put("repository.change.poll.type", "PERIOD")
            .put("repository.change.poll.pollingPeriod", "180")
            .build();
        
        Assert.assertEquals(expected, result);
        
    }
    
    
    @SuppressWarnings("rawtypes")
    @Test
    public void convertForCronReturnsCorrectMap() {
        
        Map trigger = ImmutableMap.builder()
            .put("description", "trigger1")
            .put("type", "polling")
            .put("strategy", "scheduled")
            .put("cronExpression", "0 0 0 ? * *")
            .build();  

        Map<String, String> result = pollingTriggerMapping.convert(trigger);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("selectedBuildStrategy", "poll")
            .put("repository.change.poll.type", "CRON")
            .put("repository.change.poll.cronExpression", "0 0 0 ? * *")
            .build();
        
        Assert.assertEquals(expected, result);
        
    }
}
