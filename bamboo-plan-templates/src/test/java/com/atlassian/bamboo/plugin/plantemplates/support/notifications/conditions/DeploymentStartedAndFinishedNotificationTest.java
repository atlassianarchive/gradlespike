package com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions;

import java.util.Map;

import com.atlassian.bamboo.notification.NotificationManager;
import com.atlassian.bamboo.notification.NotificationRule;
import com.atlassian.bamboo.notification.NotificationRuleImpl;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.RecipientMapping;

import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DeploymentStartedAndFinishedNotificationTest
{
    @InjectMocks
    private DeploymentStartedAndFinishedMapping deploymentStartedAndFinishedMapping;

    @Mock
    private NotificationManager notificationManager;

    @Mock
    private RecipientMapping recipientMapping;

    @SuppressWarnings("rawtypes")
    @Test
    public void buildCompleteNotificationMappingReturnsCorrectRule() {
        Map notification = ImmutableMap.builder()
                .put("recipient", "email")
                .put("type", "Deployment Started and Finished")
                .put("email", "fake@fake.com")
                .build();

        
        NotificationRule newNR = new NotificationRuleImpl();
        newNR.setNotificationManager(notificationManager);
        newNR.setConditionKey("bamboo.deployments:deploymentStartedFinished");
        newNR.setRecipient("fake@fake.com");
        newNR.setRecipientType("recipientkey");
        
        when(recipientMapping.getRecipientKey()).thenReturn("recipientkey");
        when(recipientMapping.getRecipientData(notification)).thenReturn("fake@fake.com");

        when(notificationManager.createNotificationRule("bamboo.deployments:deploymentStartedFinished", "", "fake@fake.com", "recipientkey")).thenReturn(newNR);

        NotificationRule notificationRule = deploymentStartedAndFinishedMapping.createNotificationRule(notification);
        Assert.assertNotNull(notificationRule);
        Assert.assertEquals(notificationRule.getConditionKey(), "bamboo.deployments:deploymentStartedFinished");

    }
}
