package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentTaskService;
import com.atlassian.bamboo.plugin.BambooPluginUtils;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.ScriptTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.TaskMappingFactory;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskManager;
import com.atlassian.bamboo.task.TaskModuleDescriptor;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TaskDeploymentServiceTest
{
    @InjectMocks
    private TaskDeploymentService taskDeploymentService;
    
    @Mock
    private EnvironmentTaskService environmentTaskService;
    
    @Mock 
    private TaskMappingFactory taskMappingFactory;
    @Mock
    private TaskManager taskManager;
    
    @Mock
    private PerformChangesForHacksOnContinuousDeploymentPlugin performChangesForHacksOnContinuousDeploymentPlugin;
    
    @Before
    public void setUpTaskManager(){
        taskDeploymentService.setTaskManager(taskManager);
    }

    
    @Test
    @SuppressWarnings({ "rawtypes"})
    public void performChangesCreatesOneTask()
    {
        Map tasksRepresentation = ImmutableMap.builder()
             .put("type", "scriptTask")
             .put("description", "task1Script")
             .put("scriptBody", "echo hellooooo")
             .build();
             
        TaskModuleDescriptor taskDescriptor = mock(TaskModuleDescriptor.class);
        when(taskManager.getTaskDescriptor(BambooPluginUtils.SCRIPT_BUILDER_TASK_PLUGIN_KEY)).thenReturn(taskDescriptor);
        when(taskMappingFactory.create("scriptTask")).thenReturn(new ScriptTaskMapping());
        
        Environment environment = mock(Environment.class);
        when(environment.getId()).thenReturn(8L);
        when(environment.getName()).thenReturn("environmentNameExisting");

        // method to test
        taskDeploymentService.performTasksChanges(tasksRepresentation, environment);
        
        Map<String, String> taskParams = new ScriptTaskMapping().convert(tasksRepresentation);   
        
        verify(environmentTaskService, times(1)).createTask(environment.getId(), taskDescriptor, "task1Script", true, taskParams);
    }
    
    
    @Test
    @SuppressWarnings({ "rawtypes"})
    public void performChangesCreatesMultipleTask()
    {
        // task(type:'scriptTask', description:'$description', scriptBody:'echo hellooooooo')
        
        Map task1 = ImmutableMap.builder()
            .put("type", "scriptTask")
            .put("description", "task1Script")
            .put("scriptBody", "echo hellooooo")
            .build();

        Map task2 = ImmutableMap.builder()
            .put("type", "scriptTask")
            .put("description", "task2Script")
            .put("scriptBody", "echo hellooooo")
            .build();
        
        List tasksRepresentation = ImmutableList.builder()
             .add(task1)
             .add(task2)
             .build();
       
        Environment environment = mock(Environment.class);
        when(environment.getId()).thenReturn(8L);
        when(environment.getName()).thenReturn("environmentNameExisting");

      
        TaskModuleDescriptor taskDescriptor = mock(TaskModuleDescriptor.class);
        when(taskManager.getTaskDescriptor(BambooPluginUtils.SCRIPT_BUILDER_TASK_PLUGIN_KEY)).thenReturn(taskDescriptor);
        when(taskMappingFactory.create("scriptTask")).thenReturn(new ScriptTaskMapping());
        
        // method to test
        taskDeploymentService.performTasksChanges(tasksRepresentation, environment);
        
        Map<String, String> taskParams1 = new ScriptTaskMapping().convert(task1);
        Map<String, String> taskParams2 = new ScriptTaskMapping().convert(task2);
        
        verify(environmentTaskService, times(1)).createTask(environment.getId(), taskDescriptor, "task1Script", true, taskParams1);
        verify(environmentTaskService, times(1)).createTask(environment.getId(), taskDescriptor, "task2Script", true, taskParams2);

    }
    
    @Test
    @SuppressWarnings("rawtypes")    
    public void performChangesDeletesTheExistingTask()
    {
        // task(type:'scriptTask', description:'$description', scriptBody:'echo hellooooooo')
        Map tasksRepresentation = ImmutableMap.builder()
             .put("type", "scriptTask")
             .put("description", "task1Script")
             .put("scriptBody", "echo hellooooo")
             .build();
       

        TaskDefinition taskDef1 = mock(TaskDefinition.class);
        TaskDefinition taskDef2 = mock(TaskDefinition.class);
        when(taskDef1.getId()).thenReturn(2L);
        when(taskDef2.getId()).thenReturn(3L);
        
        when(taskMappingFactory.create("scriptTask")).thenReturn(new ScriptTaskMapping());
        
        Environment environment = mock(Environment.class);
        when(environment.getId()).thenReturn(8L);
        when(environment.getName()).thenReturn("environmentNameExisting");
        when(environment.getTaskDefinitions()).thenReturn(Lists.newArrayList(taskDef1, taskDef2));
        
        TaskModuleDescriptor taskDescriptor = mock(TaskModuleDescriptor.class);
        when(taskManager.getTaskDescriptor(BambooPluginUtils.SCRIPT_BUILDER_TASK_PLUGIN_KEY)).thenReturn(taskDescriptor);
        
        // method to test
        taskDeploymentService.performTasksChanges(tasksRepresentation, environment);
        
        // environmentTaskService.deleteTask(long environmentId, long taskId);
        verify(environmentTaskService, times(1)).deleteTask(eq(8L), eq(2L));
        verify(environmentTaskService, times(1)).deleteTask(eq(8L), eq(3L));

    }
    
    
    @Test
    @SuppressWarnings({ "rawtypes", "unchecked"})
    public void performChangesDoTheHackyStuffForFixTheDeployBambooPluginTask()
    {
        Map tasksRepresentation = ImmutableMap.builder()
            .put("type", "scriptTask")
            .put("description", "testDeployBambooPlugin")
            .put("url", "http://mybamboo:1990/bamboo")
            .put("username", "usernameValue")
            .put("passwordVariable", "${bamboo.password}")
            .put("artifact", "artifactName")
            .build();

             
        TaskModuleDescriptor taskDescriptor = mock(TaskModuleDescriptor.class);
        when(taskManager.getTaskDescriptor(BambooPluginUtils.SCRIPT_BUILDER_TASK_PLUGIN_KEY)).thenReturn(taskDescriptor);
        when(taskMappingFactory.create("scriptTask")).thenReturn(new ScriptTaskMapping());
        
        Environment environment = mock(Environment.class);
        when(environment.getId()).thenReturn(8L);
        when(environment.getName()).thenReturn("environmentNameExisting");

        // method to test
        taskDeploymentService.performTasksChanges(tasksRepresentation, environment);
        
        verify(performChangesForHacksOnContinuousDeploymentPlugin, times(1)).perform(eq(environment), any(List.class));
    }


}
