package com.atlassian.bamboo.plugin.plantemplates.support;


import com.atlassian.bamboo.build.BuildDefinition;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MiscellaneousPlanServiceTest {

    @InjectMocks
    MiscellaneousPlanService miscellaneousPlanService;

    @Test
    public void updateSandboxForAutomaticValue() {

        Map input = ImmutableMap.builder()
                .put("enabled", "true")
                .put("automaticPromotion", "true")
                .build();

        Map<String, String> customConfig = Maps.newHashMap();
        BuildDefinition buildDefinition = mockBuildDefinition(customConfig);
        miscellaneousPlanService.createOrUpdateSandbox(buildDefinition, input);

        Map result = ImmutableMap.builder()
                .put("custom.sandbox.enabled", "true")
                .put("custom.sandbox.promotion.strategy", "sandbox.promotion.automatic")
                .build();

        assertEquals(result, customConfig);

    }

    @Test
    public void updateSandboxIfDisabled() {

        Map input = ImmutableMap.builder()
                .put("enabled", "false")
                .build();

        Map<String, String> customConfig = Maps.newHashMap();
        BuildDefinition buildDefinition = mockBuildDefinition(customConfig);
        miscellaneousPlanService.createOrUpdateSandbox(buildDefinition, input);

        Map result = ImmutableMap.builder()
                .put("custom.sandbox.enabled", "false")
                .build();

        assertEquals(result, customConfig);
    }

    @Test
    public void enableSandboxWithDefaultStrategy() {

        Map input = ImmutableMap.builder()
                .put("enabled", "true")
                .build();

        Map<String, String> customConfig = Maps.newHashMap();
        BuildDefinition buildDefinition = mockBuildDefinition(customConfig);
        miscellaneousPlanService.createOrUpdateSandbox(buildDefinition, input);

        Map result = ImmutableMap.builder()
                .put("custom.sandbox.enabled", "true")
                .put("custom.sandbox.promotion.strategy", "sandbox.promotion.manual")
                .build();

        assertEquals(result, customConfig);
    }



    @Test
    public void doNotDoAnythingToSandboxIfNotPresent() {

        Map<String, String> customConfig = Maps.newHashMap();
        BuildDefinition buildDefinition = mockBuildDefinition(customConfig);
        miscellaneousPlanService.createOrUpdateSandbox(buildDefinition, null);

        Map result = ImmutableMap.builder()
                .build();

        assertEquals(result, customConfig);
    }

    private BuildDefinition mockBuildDefinition(Map customConfig) {
        BuildDefinition buildDefinition = mock(BuildDefinition.class, Mockito.RETURNS_DEEP_STUBS);
        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfig);
        return buildDefinition;
    }


}
