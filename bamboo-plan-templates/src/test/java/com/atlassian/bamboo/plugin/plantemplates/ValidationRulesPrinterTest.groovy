package com.atlassian.bamboo.plugin.plantemplates

import org.junit.Test

class ValidationRulesPrinterTest {
    @Test
    public void prettyPrintRulesInformation() throws Exception{
        List<Map> rules = [
                [node: 'plan',
                        root: true,
                        allowAttributes: ['key', 'name', 'description', 'enabled']
                ],
                [node: 'project',
                        parent: 'plan',
                        allowAttributes: ['key', 'name', 'description']
                ]
        ]

        String result = new ValidationRulesPrinter().retrievePrettyPrintValidationRules(rules);
        assert result == '''plan(key, name, description, enabled){
 project(key, name, description)
}
'''
    }

    @Test
    public void prettyPrintRulesInformationIfNodeHasMultipleParents() throws Exception{
        List<Map> rules = [
                [node: 'plan',
                        root: true,
                        allowAttributes: ['key', 'name', 'description', 'enabled']
                ],
                [node: 'project',
                        parent: ['plan','othernode'],
                        allowAttributes: ['key', 'name', 'description']
                ],
                [node: 'label',
                        parent: 'plan',
                        allowAttributes: ['key']
                ]
        ]

        String result = new ValidationRulesPrinter().retrievePrettyPrintValidationRules(rules);
        assert result == '''plan(key, name, description, enabled){
 project(key, name, description)
 label(key)
}
'''
    }

    @Test
    public void prettyPrintRulesInformationWithThreeLevels() throws Exception{
        List<Map> rules = [
                [node: 'plan',
                        root: true,
                        allowAttributes: ['key', 'description', 'enabled']
                ],
                [node: 'stage',
                        parent: 'plan',
                        allowAttributes: ['key', 'name']
                ],
                [node: 'job',
                        parent: 'stage',
                        allowAttributes: ['key', 'otherattribute']
                ]
        ]
        String result = new ValidationRulesPrinter().retrievePrettyPrintValidationRules(rules);
        assert result == '''plan(key, description, enabled){
 stage(key, name){
  job(key, otherattribute)
 }
}
'''
    }

    /*@Test
    public void prettyPrintRulesInformationForConditionalAttributes() throws Exception{
        List<Map> rules = [
                [node: 'job',
                        root: true,
                        allowAttributes: ['key', 'name']
                ],
                [node: 'task',
                        parent: 'job',
                        conditionalAllow: [
                                [discriminator: "type", value:'maven', attributes: ['executable', 'type']],
                                [discriminator: "type", value:'script', attributes: ['argument', 'type']]
                        ]
                ]
        ]

        String result = new ValidationRulesPrinter().retrievePrettyPrintValidationRules(rules);
        assert result == '''job(key: '...', name: '...'){
 task(type: 'maven', executable: '...')
 task(type: 'script', argument: '...')
}
'''
    }   */

}
