package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.plugin.plantemplates.support.RepositoryService;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;

@RunWith(MockitoJUnitRunner.class)
public class CheckoutTaskMappingTest
{
    @InjectMocks
    private CheckoutTaskMapping checkoutTaskMapping;
    
    @Mock
    private RepositoryService repositoryService; 

//minimal--------    
//    userDescription:blablaa
//    checkBoxFields:taskDisabled
//    selectedRepository_0:defaultRepository
//    selectFields:selectedRepository_0
//    checkoutDir_0:
//    checkBoxFields:cleanCheckout
//    createTaskKey:com.atlassian.bamboo.plugins.vcs:task.vcs.checkout
//    taskId:0
//    planKey:PERSONAL-TEMPLATETEST-JOB2KEY
//    bamboo.successReturnMode:json
//    decorator:nothing
//    confirm:true
    
    
    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaskWithMinimalDataReturnsCorrectMap(){
        
        when(repositoryService.findRespositoryIdByName("test")).thenReturn(45678L);
        
        Map repo = ImmutableMap.builder()
            .put("name", "test")
            .build();
        
        Map task = ImmutableMap.builder()
            .put("type", "checkout")
            .put("description", "testCheckoutTaskDescription")
            .put("repository", repo)
            .build();

        Map<String,String> taskActionParams = checkoutTaskMapping.convert(task);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("userDescription", "testCheckoutTaskDescription")
            .put("taskDisabled", "false")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.vcs:task.vcs.checkout")
            .put("selectedRepository_0", "45678")
            .put("checkoutDir_0", "")
            .build(); 
        
        Assert.assertEquals(expected, taskActionParams);
        
    }

//optional----    
//  userDescription:testCheckoutTaskDescrip
//  checkBoxFields:taskDisabled
//  selectedRepository_0:defaultRepository
//  selectFields:selectedRepository_0
//  checkoutDir_0:testCheckOutDir
//  cleanCheckout:true
//  checkBoxFields:cleanCheckout
//  createTaskKey:com.atlassian.bamboo.plugins.vcs:task.vcs.checkout
//  taskId:0
//  planKey:PERSONAL-TEMPLATETEST-JOB11KEY
//  bamboo.successReturnMode:json
//  decorator:nothing
//  confirm:true

    
    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaskWithOptionalDataReturnsCorrectMap(){

        Map task = ImmutableMap.builder()
            .put("type", "checkout")
            .put("description", "testCheckoutTaskDescription")
            .put("cleanCheckout", "true")
            .build();

        Map<String,String> taskActionParams = checkoutTaskMapping.convert(task);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("userDescription", "testCheckoutTaskDescription")
            .put("taskDisabled", "false")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.vcs:task.vcs.checkout")
            .put("selectedRepository_0", "defaultRepository")
            .put("checkoutDir_0", "")
            .put("cleanCheckout", "true")
            .build(); 
        
        Assert.assertEquals(expected, taskActionParams);
    }
    
    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaskWithMultipleCheckoutsReturnsCorrectMap(){
        
        when(repositoryService.findRespositoryIdByName("test")).thenReturn(11111L);
        when(repositoryService.findRespositoryIdByName("test2")).thenReturn(22222L);
        
        
        Map repo1 = ImmutableMap.builder()
            .put("name", "test")
            .put("checkoutDirectory", "")
            .build();
        
        Map repo2 = ImmutableMap.builder()
            .put("name", "test2")
            .put("checkoutDirectory", "someDirectory/path")
            .build();
                 
        List repositories = ImmutableList.builder()
            .add(repo1)
            .add(repo2)
            .build();
        
        Map task = ImmutableMap.builder()
            .put("type", "checkout")
            .put("description", "testCheckoutTaskDescription")
            .put("repository", repositories)
            .put("cleanCheckout", "true")
            .build();

        Map<String,String> taskActionParams = checkoutTaskMapping.convert(task);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("userDescription", "testCheckoutTaskDescription")
            .put("taskDisabled", "false")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.vcs:task.vcs.checkout")
            .put("selectedRepository_0", "11111")
            .put("checkoutDir_0", "")
            .put("selectedRepository_1", "22222")
            .put("checkoutDir_1", "someDirectory/path")
            .put("cleanCheckout", "true")
            .build(); 
        
        Assert.assertEquals(expected, taskActionParams);
    }
    
    
}
