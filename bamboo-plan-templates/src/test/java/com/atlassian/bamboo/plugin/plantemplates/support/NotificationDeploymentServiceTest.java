package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentService;
import com.atlassian.bamboo.notification.NotificationRuleImpl;
import com.atlassian.bamboo.notification.NotificationSetImpl;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.NotificationMappingFactory;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.NotificationMapping;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings({"rawtypes"})
public class NotificationDeploymentServiceTest
{
    @InjectMocks
    private NotificationDeploymentService notificationDeploymentService;

    @Mock
    private NotificationMappingFactory notificationMappingFactory;

    @Mock
    private EnvironmentService environmentService;
    
    
    @Test
    public void performChangesCreatesOneNotification() throws Exception {
        Map notification = ImmutableMap.builder()
                .put("recipient", "email")
                .put("type", "Deployment Started and Finished")
                .put("email", "fake@fake.com")
                .build();

        NotificationMapping mapping = mock(NotificationMapping.class);
        when(notificationMappingFactory.create(anyString(), anyString())).thenReturn(mapping);
        NotificationRuleImpl notificationRule = new NotificationRuleImpl();
        when(mapping.createNotificationRule(eq(notification))).thenReturn(notificationRule);
        
        Environment environment = mock(Environment.class);
        when(environment.getId()).thenReturn(13L);

        notificationDeploymentService.performNotificationChanges(notification, environment);

        verify(mapping, times(1)).createNotificationRule(notification);
        verify(environmentService, times(1)).addNotification(13L, notificationRule);
        
    }
    
    
    @Test
    public void performChangesCreatesManyNotifications() throws Exception {
        Map notification1 = ImmutableMap.builder()
                .put("recipient", "email")
                .put("type", "Build Complete")
                .put("email", "fake@fake.com")
                .build();

        Map notification2 = ImmutableMap.builder()
                .put("recipient", "user")
                .put("type", "Build Complete")
                .put("user", "bsmith")
                .build();

        List notifications = ImmutableList.builder()
                .add(notification1)
                .add(notification2)
                .build();

        NotificationMapping mapping = mock(NotificationMapping.class);
        when(notificationMappingFactory.create(anyString(), anyString())).thenReturn(mapping);
        
        NotificationRuleImpl notificationRule1 = new NotificationRuleImpl();
        notificationRule1.setConditionData("condition1");
        when(mapping.createNotificationRule(notification1)).thenReturn(notificationRule1);
        
        NotificationRuleImpl notificationRule2 = new NotificationRuleImpl();
        notificationRule2.setConditionData("condition2");
        when(mapping.createNotificationRule(notification2)).thenReturn(notificationRule2);

        Environment environment = mock(Environment.class);
        when(environment.getId()).thenReturn(13L);
        
        notificationDeploymentService.performNotificationChanges(notifications, environment);

        verify(mapping, times(1)).createNotificationRule(notification1);
        verify(mapping, times(1)).createNotificationRule(notification2);
        verify(environmentService, times(1)).addNotification(13L, notificationRule1);
        verify(environmentService, times(1)).addNotification(13L, notificationRule2);

    }
    
    
    @Test
    public void performChangesDeletesExistingNotifications() throws Exception {
        Map notification1 = ImmutableMap.builder()
            .put("recipient", "email")
            .put("type", "Build Complete")
            .put("email", "fake@fake.com")
            .build();

        Map notification2 = ImmutableMap.builder()
            .put("recipient", "user")
            .put("type", "Build Complete")
            .put("user", "bsmith")
            .build();

        List notifications = ImmutableList.builder()
            .add(notification1)
            .add(notification2)
            .build();
        
        NotificationMapping mapping = mock(NotificationMapping.class);
        when(notificationMappingFactory.create(anyString(), anyString())).thenReturn(mapping);
        
        NotificationRuleImpl notificationRule1 = new NotificationRuleImpl();
        notificationRule1.setId(1L);
        notificationRule1.setConditionData("condition1");
        when(mapping.createNotificationRule(notification1)).thenReturn(notificationRule1);
        
        NotificationRuleImpl notificationRule2 = new NotificationRuleImpl();
        notificationRule2.setId(2L);
        notificationRule2.setConditionData("condition2");
        when(mapping.createNotificationRule(notification2)).thenReturn(notificationRule2);
        
        Environment environment = mock(Environment.class);
        when(environment.getId()).thenReturn(13L);
        
        NotificationSetImpl notificationSet = new NotificationSetImpl();
        notificationSet.addNotification(notificationRule1);
        notificationSet.addNotification(notificationRule2);

        when(environmentService.getNotificationSet(13L)).thenReturn(notificationSet);

        notificationDeploymentService.performNotificationChanges(notifications, environment);
        
        verify(environmentService, times(1)).deleteNotification(13L, 1L);
        verify(environmentService, times(1)).deleteNotification(13L, 2L);
        
    }

}
