package com.atlassian.bamboo.plugin.plantemplates.support.triggers;

import java.util.Map;
import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;

@RunWith(MockitoJUnitRunner.class)
public class DailyTriggerMappingTest
{
    @InjectMocks
    private DailyTriggerMapping dailyTriggetMapping;
    
    @SuppressWarnings("rawtypes")
    @Test
    public void convertACronTriggerReturnsCorrectMap() {
        
        Map trigger = ImmutableMap.builder()
            .put("description", "trigger1")
            .put("type", "daily")
            .put("time", "17:30")
            .build();  

        Map<String, String> result = dailyTriggetMapping.convert(trigger);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("selectedBuildStrategy", "daily")
            .put("repository.change.daily.buildTime", "17:30")
            .build();
        
        Assert.assertEquals(expected, result);
        
    }
}
