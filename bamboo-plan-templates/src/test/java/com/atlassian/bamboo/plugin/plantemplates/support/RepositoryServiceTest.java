package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.repository.RepositoryConfigurationService;
import com.atlassian.bamboo.repository.RepositoryData;
import com.atlassian.bamboo.repository.RepositoryDefinitionManager;
import com.atlassian.bamboo.repository.nullrepository.NullRepository;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RepositoryServiceTest
{
    @InjectMocks
    private RepositoryService repositoryService;
    
    @Mock
    private RepositoryConfigurationService repositoryConfigurationService;
    @Mock
    private RepositoryDefinitionManager repositoryDefinitionManager;
    
    @Test
    @SuppressWarnings("rawtypes")
    public void findRespositoryReturnsCorrectRepositoryKey(){
        
        Map repoRepresentation1 = ImmutableMap.builder()
            .put("name", "name1").build();
        Map repoRepresentation2 = ImmutableMap.builder()
            .put("name", "name2").build();
        Map repoRepresentation3 = ImmutableMap.builder()
            .put("name", "name3").build();
        Map repoRepresentation4 = ImmutableMap.builder()
            .put("name", "name4").build();
        
        RepositoryData rep1 = mock(RepositoryData.class);
        RepositoryData rep2 = mock(RepositoryData.class);
        RepositoryData rep3 = mock(RepositoryData.class);
        
        when(rep1.getName()).thenReturn("name1");
        when(rep2.getName()).thenReturn("name2");
        when(rep3.getName()).thenReturn("name3");
        
        when(rep1.getPluginKey()).thenReturn("key1");
        when(rep2.getPluginKey()).thenReturn("key2");
        when(rep3.getPluginKey()).thenReturn("key3");
        
        when(repositoryDefinitionManager.getGlobalRepositoryDefinitions()).thenReturn(Lists.newArrayList(rep1, rep2, rep3));        
        
        String key;
        key = repositoryService.findRespository(repoRepresentation1);
        assertEquals("key1", key);
        
        key = repositoryService.findRespository(repoRepresentation2);
        assertEquals("key2", key);
        
        key = repositoryService.findRespository(repoRepresentation3);
        assertEquals("key3", key);
        
        key = repositoryService.findRespository(repoRepresentation4);
        assertEquals(NullRepository.KEY, key);
    }
    
    
    @Test
    @SuppressWarnings("rawtypes")
    public void updateRespositoryMakesCorrectCalls(){
        
        Map repoRepresentation1 = ImmutableMap.builder()
            .put("name", "name1").build();

        Plan plan = mock(Plan.class);
        RepositoryData rep1 = mock(RepositoryData.class);
        
        when(rep1.getName()).thenReturn("name1");
        when(rep1.getId()).thenReturn(1L);
        when(repositoryDefinitionManager.getGlobalRepositoryDefinitions()).thenReturn(Lists.newArrayList(rep1));        
        
        repositoryService.updateRespository(plan, repoRepresentation1);
        verify(repositoryConfigurationService, times(1)).attachGlobalRepositoryToPlan(plan , 1L, true);
        
    }
    
    @Test
    @SuppressWarnings("rawtypes")
    public void updateRespositoryMakesCorrectCalls2(){
        Map repoRepresentation4 = ImmutableMap.builder()
            .put("name", "name4").build();

        Plan plan = mock(Plan.class);
        RepositoryData rep1 = mock(RepositoryData.class);
        RepositoryData rep2 = mock(RepositoryData.class);
        
        when(rep1.getName()).thenReturn("name1");
        when(rep2.getName()).thenReturn("name2");
        
        when(rep1.getId()).thenReturn(1L);
        when(rep2.getId()).thenReturn(2L);
        
        when(repositoryDefinitionManager.getGlobalRepositoryDefinitions()).thenReturn(Lists.newArrayList(rep1, rep2));        
        
        repositoryService.updateRespository(plan, repoRepresentation4);
        verify(repositoryConfigurationService, times(0)).attachGlobalRepositoryToPlan(any(Plan.class), anyLong(), anyBoolean());
        
    }
    
    @Test
    @SuppressWarnings("rawtypes")
    public void updateMultipleRespositoriesMakesCorrectCalls(){
        
        Map repoRepresentation1 = ImmutableMap.builder()
            .put("name", "name1").build();
        
        Map repoRepresentation2 = ImmutableMap.builder()
            .put("name", "name2").build();
        
        Map repoRepresentation3 = ImmutableMap.builder()
            .put("name", "name3").build();
        
        List repositories = ImmutableList.builder().add(repoRepresentation1)
            .add(repoRepresentation2)
            .add(repoRepresentation3)
            .build();
        
        Plan plan = mock(Plan.class);
        RepositoryData rep1 = mock(RepositoryData.class);
        RepositoryData rep2 = mock(RepositoryData.class);
        RepositoryData rep3 = mock(RepositoryData.class);
        
        when(rep1.getName()).thenReturn("name1");
        when(rep1.getId()).thenReturn(1L);
        
        when(rep2.getName()).thenReturn("name2");
        when(rep2.getId()).thenReturn(2L);
        
        when(rep3.getName()).thenReturn("name3");
        when(rep3.getId()).thenReturn(3L);
        
        when(repositoryDefinitionManager.getGlobalRepositoryDefinitions()).thenReturn(Lists.newArrayList(rep1, rep2, rep3));        
        
        repositoryService.updateRespository(plan, repositories);
        verify(repositoryConfigurationService, times(1)).attachGlobalRepositoryToPlan(plan , 1L, true);
        verify(repositoryConfigurationService, times(1)).attachGlobalRepositoryToPlan(plan , 2L, true);
        verify(repositoryConfigurationService, times(1)).attachGlobalRepositoryToPlan(plan , 3L, true);
        
    }
    
    @Test
    @SuppressWarnings("rawtypes")
    public void updateRespositoryDoesNotFailIfTheRepositoryIsAlreadyAttachedToThePlan(){
        Map repoRepresentation1 = ImmutableMap.builder()
            .put("name", "name1").build();

        Plan plan = mock(Plan.class);
        RepositoryData rep1 = mock(RepositoryData.class);
        
        when(rep1.getName()).thenReturn("name1");
        when(rep1.getId()).thenReturn(1L);
        when(repositoryDefinitionManager.getGlobalRepositoryDefinitions()).thenReturn(Lists.newArrayList(rep1));    
        when(repositoryConfigurationService.attachGlobalRepositoryToPlan(any(Plan.class), anyLong(), anyBoolean()))
                .thenThrow(new IllegalArgumentException("Repository 458753 is already attached to the Plan"));        

        repositoryService.updateRespository(plan, repoRepresentation1);
        verify(repositoryConfigurationService, times(1)).attachGlobalRepositoryToPlan(any(Plan.class), anyLong(), anyBoolean());
        
    }
    
    @Test
    public void findRespositoryByNameReturnsCorrectRepositoryId(){
        
        String name1 = "name1";
        String name2 = "name2";
        String name3 = "name3";
        String name4 = "name4";
        
        
        RepositoryData rep1 = mock(RepositoryData.class);
        RepositoryData rep2 = mock(RepositoryData.class);
        RepositoryData rep3 = mock(RepositoryData.class);
        
        when(rep1.getName()).thenReturn("name1");
        when(rep2.getName()).thenReturn("name2");
        when(rep3.getName()).thenReturn("name3");
        
        when(rep1.getId()).thenReturn(1L);
        when(rep2.getId()).thenReturn(2L);
        when(rep3.getId()).thenReturn(3L);
        
        when(repositoryDefinitionManager.getGlobalRepositoryDefinitions()).thenReturn(Lists.newArrayList(rep1, rep2, rep3));        
        
        Long id;
        id = repositoryService.findRespositoryIdByName(name1);
        assertEquals(1L, id.longValue());
        
        id = repositoryService.findRespositoryIdByName(name2);
        assertEquals(2L, id.longValue());
        
        id = repositoryService.findRespositoryIdByName(name3);
        assertEquals(3L, id.longValue());
        
        try{
            repositoryService.findRespositoryIdByName(name4);
            Assert.fail();
        }catch(IllegalArgumentException e){
            assertEquals("Shared repository 'name4' is not found, please ensure that the shared repository exists", e.getMessage());
        }
    }

}
