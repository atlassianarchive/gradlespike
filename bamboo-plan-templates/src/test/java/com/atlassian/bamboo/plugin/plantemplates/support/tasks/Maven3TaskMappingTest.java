package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;


@RunWith(MockitoJUnitRunner.class)
public class Maven3TaskMappingTest
{

    @InjectMocks
    private Maven3TaskMapping maven3TaskMapping;
    
//maven3 minimal
//    userDescription:task descript minimal test
//    checkBoxFields:taskDisabled
//    label:maven 3
//    goal:clean test
//    checkBoxFields:useMavenReturnCode
//    buildJdk:jdk18
//    selectFields:buildJdk
//    testDirectoryOption:standardTestDirectory
//    testResultsDirectory:**/target/surefire-reports/*.xml
//    createTaskKey:com.atlassian.bamboo.plugins.maven:task.builder.mvn3
    

    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaksWithMinimalMaven3OptionsReturnsCorrectMap(){
        Map task = ImmutableMap.builder()
            .put("type", "maven3")
            .put("description", "task1Maven3")
            .put("goal", "clean install")
            .build();

        Map<String,String> taskActionParams = maven3TaskMapping.convert(task);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("userDescription", "task1Maven3")
            .put("taskDisabled", "false")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.maven:task.builder.mvn3")
            .put("goal", "clean install")
            .put("testDirectoryOption", "standardTestDirectory")
            .put("testResultsDirectory", "**/target/surefire-reports/*.xml")
            .build(); 
        
        Assert.assertEquals(expected, taskActionParams);
    }
    
    
    
//maven3 with optional    
//    userDescription:maven 3 test
//    checkBoxFields:taskDisabled
//    label:maven 3
//    selectFields:label
//    goal:clean test
//    useMavenReturnCode:true
//    checkBoxFields:useMavenReturnCode
//    buildJdk:jdk18
//    selectFields:buildJdk
//    projectFile:/Override Project File
//    environmentVariables:env var test
//    workingSubDirectory:work dir test
//    testChecked:true
//    checkBoxFields:testChecked
//    testDirectoryOption:customTestDirectory
//    testResultsDirectory:**/target/surefire-reports/*.xml
//    createTaskKey:com.atlassian.bamboo.plugins.maven:task.builder.mvn3
    
    
    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaksWithMaven3OptionsReturnsCorrectMap(){
        Map task = ImmutableMap.builder()
            .put("type", "maven3")
            .put("description", "task1Maven3")
            .put("goal", "clean install")
            .put("environmentVariables", "environmentVariables1")
            .put("workingSubDirectory", "workingSubDirectory1")
            .put("projectFile", "projectFileTest")
            .put("buildJdk", "jdk18")
            .put("mavenExecutable", "maven 3")
            .put("hasTests", "true")
            .put("testDirectory", "**/target/my_custom/*.xml")
            .put("useMavenReturnCode", "true")
            .build();

        Map<String,String> taskActionParams = maven3TaskMapping.convert(task);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("userDescription", "task1Maven3")
            .put("taskDisabled", "false")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.maven:task.builder.mvn3")
            .put("environmentVariables", "environmentVariables1")
            .put("workingSubDirectory", "workingSubDirectory1")
            .put("projectFile", "projectFileTest")
            .put("label", "maven 3")
            .put("goal", "clean install")
            .put("buildJdk", "jdk18")
            .put("testChecked", "true")
            .put("testDirectoryOption", "customTestDirectory")
            .put("testResultsDirectory", "**/target/my_custom/*.xml")
            .put("useMavenReturnCode", "true")

            //TODO deal with selected  checkBoxFields and selectFields
            
            .build(); 
        
        Assert.assertEquals(expected, taskActionParams);
    }
    
}
