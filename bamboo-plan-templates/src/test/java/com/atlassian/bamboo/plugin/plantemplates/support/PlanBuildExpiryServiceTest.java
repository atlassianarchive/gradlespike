package com.atlassian.bamboo.plugin.plantemplates.support;


import java.util.Map;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.chains.Chain;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlanBuildExpiryServiceTest
{
    @InjectMocks
    private PlanBuildExpiryService planBuildExpiryService;
    
    @Mock 
    private BuildDefinitionManager buildDefinitionManager;

    
    // ALL OPTIONS    
    //  buildExpiry(notExpire: 'true', buildResults: 'true', artifacts: 'true', 
    //  buildLogs: 'true', duration: '3', period: 'days', minimumBuildsToKeep: '3', labelsToKeep: 'label1 label2')

    // ALL PROPERTIES
    // custom.buildExpiryConfig.enabled  -> true / false
    // custom.buildExpiryConfig.expiryTypeNothing  -> true / false
    // custom.buildExpiryConfig.expiryTypeResult  -> true / false
    // custom.buildExpiryConfig.expiryTypeArtifact  -> true / false
    // custom.buildExpiryConfig.expiryTypeBuildLog  -> true / false
    // custom.buildExpiryConfig.duration   -> number 
    // custom.buildExpiryConfig.period -> "days" "weeks" "months"
    // custom.buildExpiryConfig.excludeLabels  -> true / false
    // custom.buildExpiryConfig.labelsToKeep -> "label1 label2 label3"
    
    @SuppressWarnings("rawtypes")
    @Test
    public void buildExpirySet_expiryTypeNothing_ConfigProperties() {
        // buildExpiry(notExpire: 'true') 
        
        Map planDependencies = ImmutableMap.builder()
            .put("buildExpiry", ImmutableMap.builder()
                    .put("notExpire", "true")
                    .build())
            .build();

        // custom.buildExpiryConfig.enabled  -> true / false
        // custom.buildExpiryConfig.expiryTypeNothing  -> true / false
        
        Chain chain = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
        BuildDefinition buildDefinition = mock(BuildDefinition.class, Mockito.RETURNS_DEEP_STUBS);
        when(chain.getBuildDefinition()).thenReturn(buildDefinition);
        Map<String, String> customConfig = Maps.newHashMap();
        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfig);
        
        planBuildExpiryService.performChanges(chain, (Map) planDependencies.get("buildExpiry"));

        assertEquals(customConfig.get("custom.buildExpiryConfig.enabled"), "true");
        assertEquals(customConfig.get("custom.buildExpiryConfig.expiryTypeNothing"), "true");
    }
 
    @SuppressWarnings("rawtypes")
    @Test
    public void buildExpirySet_buildResults_ConfigProperties() {
          //      buildExpiry(buildResults: 'true') 
          
          
          Map planDependencies = ImmutableMap.builder()
              .put("buildExpiry", ImmutableMap.builder()
                      .put("buildResults", "true")
                      .build())
              .build();
    
          // custom.buildExpiryConfig.enabled  -> true 
          // custom.buildExpiryConfig.expiryTypeResult  -> true 
          // custom.buildExpiryConfig.expiryTypeArtifact  -> true 
          // custom.buildExpiryConfig.expiryTypeBuildLog  -> true 
          
          
          Chain chain = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
          BuildDefinition buildDefinition = mock(BuildDefinition.class, Mockito.RETURNS_DEEP_STUBS);
          when(chain.getBuildDefinition()).thenReturn(buildDefinition);
          Map<String, String> customConfig = Maps.newHashMap();
          when(buildDefinition.getCustomConfiguration()).thenReturn(customConfig);
          
          planBuildExpiryService.performChanges(chain, (Map) planDependencies.get("buildExpiry"));
    
          assertEquals(customConfig.get("custom.buildExpiryConfig.enabled"), "true");
          assertEquals(customConfig.get("custom.buildExpiryConfig.expiryTypeResult"), "true");
          assertEquals(customConfig.get("custom.buildExpiryConfig.expiryTypeArtifact"), "true");
          assertEquals(customConfig.get("custom.buildExpiryConfig.expiryTypeBuildLog"), "true");
          
      }
    
    @SuppressWarnings("rawtypes")
    @Test
    public void buildExpirySet_duration_ConfigProperties() {
        //  buildExpiry(buildResults: 'true', duration: '3', period: 'days', minimumBuildsToKeep: '4')
          
          Map planDependencies = ImmutableMap.builder()
              .put("buildExpiry", ImmutableMap.builder()
                      .put("buildResults", "true")
                      .put("duration", "3")
                      .put("period", "days")
                      .put("minimumBuildsToKeep", "4")
                      .build())
              .build();
    
          // custom.buildExpiryConfig.enabled  -> true
          // custom.buildExpiryConfig.expiryTypeResult  -> true 
          // custom.buildExpiryConfig.duration   -> 3 
          // custom.buildExpiryConfig.period -> "days" 
          // custom.buildExpiryConfig.buildsToKeep -> 4
          
          
          Chain chain = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
          BuildDefinition buildDefinition = mock(BuildDefinition.class, Mockito.RETURNS_DEEP_STUBS);
          when(chain.getBuildDefinition()).thenReturn(buildDefinition);
          Map<String, String> customConfig = Maps.newHashMap();
          when(buildDefinition.getCustomConfiguration()).thenReturn(customConfig);
          
          planBuildExpiryService.performChanges(chain, (Map) planDependencies.get("buildExpiry"));
    
          assertEquals(customConfig.get("custom.buildExpiryConfig.enabled"), "true");
          assertEquals(customConfig.get("custom.buildExpiryConfig.expiryTypeResult"), "true");
          assertEquals(customConfig.get("custom.buildExpiryConfig.duration"), "3");
          assertEquals(customConfig.get("custom.buildExpiryConfig.period"), "days");
          assertEquals(customConfig.get("custom.buildExpiryConfig.buildsToKeep"), "4");
          
      }

    
    @SuppressWarnings("rawtypes")
    @Test
    public void buildExpirySet_artifacts_ConfigProperties() {
          //      buildExpiry(artifacts: 'true', buildLogs: 'true')
          
          Map planDependencies = ImmutableMap.builder()
              .put("buildExpiry", ImmutableMap.builder()
                      .put("artifacts", "true")
                      .put("buildLogs", "true")
                      .build())
              .build();
    
          // custom.buildExpiryConfig.enabled  -> true 
          // custom.buildExpiryConfig.expiryTypeResult  -> false 
          // custom.buildExpiryConfig.expiryTypeArtifact  -> true 
          // custom.buildExpiryConfig.expiryTypeBuildLog  -> true 
          
          
          Chain chain = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
          BuildDefinition buildDefinition = mock(BuildDefinition.class, Mockito.RETURNS_DEEP_STUBS);
          when(chain.getBuildDefinition()).thenReturn(buildDefinition);
          Map<String, String> customConfig = Maps.newHashMap();
          when(buildDefinition.getCustomConfiguration()).thenReturn(customConfig);
          
          planBuildExpiryService.performChanges(chain, (Map) planDependencies.get("buildExpiry"));
    
          assertEquals(customConfig.get("custom.buildExpiryConfig.enabled"), "true");
          assertEquals(customConfig.get("custom.buildExpiryConfig.expiryTypeResult"), "false");
          assertEquals(customConfig.get("custom.buildExpiryConfig.expiryTypeArtifact"), "true");
          assertEquals(customConfig.get("custom.buildExpiryConfig.expiryTypeBuildLog"), "true");
          
      }
    
    
    @SuppressWarnings("rawtypes")
    @Test
    public void buildExpirySet_labelsToKeep_ConfigProperties() {
        //  buildExpiry(labelsToKeep: 'label1 label2')
          
          Map planDependencies = ImmutableMap.builder()
              .put("buildExpiry", ImmutableMap.builder()
                      .put("labelsToKeep", "label1 label2")
                      .build())
              .build();
    
          // custom.buildExpiryConfig.enabled  -> true
          // custom.buildExpiryConfig.excludeLabels  -> true
          // custom.buildExpiryConfig.labelsToKeep -> "label1 label2"
          
          Chain chain = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
          BuildDefinition buildDefinition = mock(BuildDefinition.class, Mockito.RETURNS_DEEP_STUBS);
          when(chain.getBuildDefinition()).thenReturn(buildDefinition);
          Map<String, String> customConfig = Maps.newHashMap();
          when(buildDefinition.getCustomConfiguration()).thenReturn(customConfig);
          
          planBuildExpiryService.performChanges(chain, (Map) planDependencies.get("buildExpiry"));
    
          assertEquals(customConfig.get("custom.buildExpiryConfig.enabled"), "true");
          assertEquals(customConfig.get("custom.buildExpiryConfig.excludeLabels"), "true");
          assertEquals(customConfig.get("custom.buildExpiryConfig.labelsToKeep"), "label1 label2");
          
      }


                                                
    
}