package com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients;

import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.HipChatRecipientMapping;
import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("rawtypes")
public class HipChatRecipientTest
{
    @InjectMocks
    private HipChatRecipientMapping hipChatRecipientMapping;

    @Test
    public void hipChatMappingReturnsCorrectData(){
        Map notification = ImmutableMap.builder()
                .put("recipient", "hipchat")
                .put("type", "Build Complete")
                .put("room", "room_name")
                .put("apiKey", "api_key")
                .put("notify", "true")
                .build();

        String recipientKey = hipChatRecipientMapping.getRecipientKey();
        String recipientData = hipChatRecipientMapping.getRecipientData(notification);

        Assert.assertEquals(recipientKey, "com.atlassian.bamboo.plugins.bamboo-hipchat:recipient.hipchat");
        Assert.assertEquals(recipientData, "api_key|true|room_name");
    }

    @Test
    public void hipChatMappingWithoutNotifyReturnsCorrectData(){
        Map notification = ImmutableMap.builder()
                .put("recipient", "hipchat")
                .put("type", "Build Complete")
                .put("room", "room_name")
                .put("apiKey", "api_key")
                .build();

        String recipientKey = hipChatRecipientMapping.getRecipientKey();
        String recipientData = hipChatRecipientMapping.getRecipientData(notification);

        Assert.assertEquals(recipientKey, "com.atlassian.bamboo.plugins.bamboo-hipchat:recipient.hipchat");
        Assert.assertEquals(recipientData, "api_key|false|room_name");
    }
}
