package com.atlassian.bamboo.plugin.plantemplates;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.DefaultBuildDefinition;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.chains.DefaultChain;
import com.atlassian.bamboo.plan.branch.BranchMonitoringConfigurationImpl;
import com.atlassian.bamboo.plugin.plantemplates.support.BranchMonitoringService;
import com.atlassian.bamboo.plugin.plantemplates.support.PlanBuildExpiryService;
import com.atlassian.bamboo.plugin.plantemplates.support.PlanDependenciesService;
import com.atlassian.bamboo.plugin.plantemplates.support.PlanService;
import com.atlassian.bamboo.plugin.plantemplates.support.ProjectService;
import com.atlassian.bamboo.plugin.plantemplates.support.StageService;
import com.atlassian.bamboo.project.DefaultProject;
import com.atlassian.bamboo.variable.VariableConfigurationService;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("rawtypes")
public class PerformPlanChangesTest
{
    @InjectMocks
    private PerformPlanChanges performPlanChanges;
    
    @Mock
    private PlanService planService;
    @Mock
    private ProjectService projectService;
    @Mock
    private StageService stageService;
    @Mock
    private BranchMonitoringService branchMonitoringService;
    @Mock
    private VariableConfigurationService variableConfigurationService;
    @Mock
    private AutoVariablesService autoVariablesService;
    @Mock
    private PlanDependenciesService planDependenciesService;
    @Mock
    private PlanBuildExpiryService planBuildExpiryService;

    
    @Test
    public void performChangesCreatesNewPlan() throws Exception
    {
        Map<String, String> projectRepresentation = ImmutableMap.<String, String>builder()
            .put("name", "projectName")
            .put("key", "projectKey")
            .put("description", "").build();
        
        Map planRepresentation = ImmutableMap.builder()
                .put("plan", ImmutableMap.builder()
                        .put("key", "planKey")
                        .put("name", "planName")
                        .put("description", "")
                        .put("project", projectRepresentation)
                        .build())
                .build();
        


        DefaultProject createdProject = new DefaultProject("projectKey", "projectName", "");

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");

        when(planService.createOrUpdate((Map)planRepresentation.get("plan"))).thenReturn(createdChain);
        when(stageService.performStageChanges(null, createdChain)).thenReturn(createdChain);
        when(projectService.createOrUpdate(projectRepresentation)).thenReturn(createdProject);

        performPlanChanges.performChanges(planRepresentation);

        verify(projectService, times(1)).createOrUpdate(projectRepresentation);
        verify(planService, times(1)).createOrUpdate((Map) planRepresentation.get("plan"));

        assertThat("Plan should be enabled following update", createdChain.isSuspendedFromBuilding(), is(false));
    }

    @Test
    public void planIsNotEnabledIfConfiguredToBeDisabled() throws Exception
    {
        Map<String, String> projectRepresentation = ImmutableMap.<String, String>builder()
                .put("name", "projectName")
                .put("key", "projectKey")
                .put("description", "").build();

        Map planRepresentation = ImmutableMap.builder()
                .put("plan", ImmutableMap.builder()
                        .put("key", "planKey")
                        .put("name", "planName")
                        .put("description", "")
                        .put("enabled", "false")
                        .put("project", projectRepresentation)
                        .build())
                .build();

        DefaultProject createdProject = new DefaultProject("projectKey", "projectName", "");

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");

        when(planService.createOrUpdate((Map)planRepresentation.get("plan"))).thenReturn(createdChain);
        when(stageService.performStageChanges(null, createdChain)).thenReturn(createdChain);
        when(projectService.createOrUpdate(projectRepresentation)).thenReturn(createdProject);

        performPlanChanges.performChanges(planRepresentation);

        verify(projectService, times(1)).createOrUpdate(projectRepresentation);
        verify(planService, times(1)).createOrUpdate((Map) planRepresentation.get("plan"));

        assertThat("Plan should be disabled following update", createdChain.isSuspendedFromBuilding(), is(true));
    }

    @Test
    @SuppressWarnings({"unchecked" })
    public void performChangesAppliesChangesToStages() throws Exception
    {
        List stageRepresentation = ImmutableList.builder()
        .add(ImmutableMap.builder()
            .put("name", "stage1")
            .put("description", "stage1Description")
            .put("manual", "false").build())
        .add(ImmutableMap.builder()
             .put("name", "stage2")
             .put("description", "stage2Description")
             .put("manual", "true").build())
        .build();
        
        Map planRepresentation = ImmutableMap.builder()
            .put("plan", ImmutableMap.builder()
                    .put("key", "planKey")
                    .put("name", "newName!")
                    .put("description", "new!")
                    .put("project", ImmutableMap.builder()
                            .put("name", "newName!")
                            .put("key", "projectKey")
                            .put("description", "new!").build())
                    .put("stage",  stageRepresentation)
                    .build())
            .build();
        

        DefaultProject createdProject = new DefaultProject("projectKey", "projectName", "");

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");
        
        when(planService.createOrUpdate((Map)planRepresentation.get("plan"))).thenReturn(createdChain);
        when(projectService.createOrUpdate(any(Map.class))).thenReturn(createdProject);
        when(stageService.performStageChanges(stageRepresentation, createdChain)).thenReturn(createdChain);

        List<Chain> builtPlans = performPlanChanges.performChanges(planRepresentation);
        
        verify(stageService, times(1)).performStageChanges(stageRepresentation, builtPlans.get(0));
    }
    
    
    @Test
    public void performChangesAppliesBranchMonitoringConfiguration() throws Exception
    {
        Map<String, String> branchMonitoringMap = ImmutableMap.of("enabled", "true",
                                                                  "matchingPattern", "issue/[^/]*",
                                                                  "timeOfInactivityInDays", "10",
                                                                  "notificationStrategy", "INHERIT",
                                                                  "remoteJiraBranchLinkingEnabled", "true");

        Map<String, String> projectMap = ImmutableMap.of("name", "projectName",
                                                         "key", "projectKey",
                                                         "description", "");

        Map<String, Object> planMap = ImmutableMap.of("key", "planKey",
                                                      "name", "planName",
                                                      "description", "",
                                                      "project", projectMap,
                                                      "branchMonitoring", branchMonitoringMap);
        
        Map planRepresentation = ImmutableMap.of("plan", planMap);

        DefaultProject createdProject = new DefaultProject("projectKey", "projectName", "");

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");

        BuildDefinition mockBuildDefinition = mock(DefaultBuildDefinition.class);

        when(planService.createOrUpdate(planMap)).thenReturn(createdChain);
        when(stageService.performStageChanges(null, createdChain)).thenReturn(createdChain);
        when(projectService.createOrUpdate(projectMap)).thenReturn(createdProject);
        when(mockBuildDefinition.getBranchMonitoringConfiguration()).thenReturn(new BranchMonitoringConfigurationImpl());

        performPlanChanges.performChanges(planRepresentation);

        verify(branchMonitoringService).applyAndSaveBranchMonitoringConfiguration(branchMonitoringMap, createdChain);
    }


    @SuppressWarnings("unchecked")
    @Test
    public void performChangesAddsAutomaticVariables() throws Exception
    {
        final String variableKey = "variableKey";
        final String variableValue = "variableValue";
        Map<String, String> projectMap = ImmutableMap.of("name", "projectName",
                "key", "projectKey",
                "description", "");
        Map<String, Object> planMap = ImmutableMap.of("key", "planKey",
                "name", "planName",
                "description", "",
                "project", projectMap
        );
        Map planRepresentation = ImmutableMap.of(
                "plan", planMap
        );
        DefaultProject createdProject = new DefaultProject("projectKey", "projectName", "");
        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");

        when(autoVariablesService.getAdditionalVariables(planRepresentation))
                .thenReturn(Collections.singletonMap(variableKey, variableValue));
        when(planService.createOrUpdate(planMap)).thenReturn(createdChain);
        when(stageService.performStageChanges(null, createdChain)).thenReturn(createdChain);
        when(projectService.createOrUpdate(projectMap)).thenReturn(createdProject);

        performPlanChanges.performChanges(planRepresentation);

        verify(variableConfigurationService).createPlanVariable(createdChain, variableKey, variableValue);
    }


}
