package com.atlassian.bamboo.plugin.plantemplates.support;


import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.build.creation.ChainCreationService;
import com.atlassian.bamboo.build.creation.PlanCreationService;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.chains.DefaultChain;
import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.deletion.DeletionService;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.branch.ChainBranchManager;
import com.atlassian.bamboo.project.DefaultProject;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlanServiceTest
{

    @InjectMocks
    private PlanService planService;
    @Mock
    private PlanManager planManager;
    @Mock
    private ChainCreationService chainCreationService;
    @Mock
    private RepositoryService repositoryService;
    @Mock
    private TriggerService triggerService;
    @Mock
    private NotificationService notificationService;
    @Mock
    private LabelService labelService;
    @Mock
    private VariableService variableService;
    @Mock
    private DeletionService deletionService;
    @Mock
    private ChainBranchManager chainBranchManager;
    @Mock
    private MiscellaneousPlanService miscellaneousPlanService;

    @Test
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void createOrUpdateCreatesNewPlan() throws Exception
    {
        Map repository = ImmutableMap.builder()
            .put("name", "repositoryName").build();
        
        Map dslResult = ImmutableMap.builder()
                .put("plan", ImmutableMap.builder()
                        .put("key", "planKey")
                        .put("name", "planName")
                        .put("description", "description")
                        .put("repository", repository)
                        .put("project", ImmutableMap.builder()
                                .put("name", "projectName")
                                .put("key", "projectKey")
                                .put("description", "").build())
                        .build())
                .build();

        Map planRepresentation = (Map) dslResult.get("plan");

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");
        
        when(chainCreationService.createPlan(any(BuildConfiguration.class), any(ActionParametersMap.class), eq(ChainCreationService.EnablePlan.DISABLED))).thenReturn("PROJECTKEY-PLANKEY");
        when(planManager.isPlanKeyConflicting(any(PlanKey.class))).thenReturn(false);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(createdChain);
        
        Chain plan = planService.createOrUpdate(planRepresentation);
        
        Assert.assertEquals("projectKey-planKey", plan.getKey());

        verify(repositoryService, times(1)).updateRespository(plan, repository);
        verify(chainCreationService, times(1)).createPlan(any(BuildConfiguration.class), any(ActionParametersMap.class), eq(ChainCreationService.EnablePlan.DISABLED));
    }
    
    @Test
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void createOrUpdateCreatesADisabledNewPlan() throws Exception
    {
        Map repository = ImmutableMap.builder()
            .put("name", "repositoryName").build();
        
        Map dslResult = ImmutableMap.builder()
                .put("plan", ImmutableMap.builder()
                        .put("key", "planKey")
                        .put("name", "planName")
                        .put("description", "description")
                        .put("enabled", "false")
                        .put("repository", repository)
                        .put("project", ImmutableMap.builder()
                                .put("name", "projectName")
                                .put("key", "projectKey")
                                .put("description", "").build())
                        .build())
                .build();

        Map planRepresentation = (Map) dslResult.get("plan");

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");
        
        when(chainCreationService.createPlan(any(BuildConfiguration.class), any(ActionParametersMap.class), eq(ChainCreationService.EnablePlan.DISABLED))).thenReturn("PROJECTKEY-PLANKEY");
        when(planManager.isPlanKeyConflicting(any(PlanKey.class))).thenReturn(false);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(createdChain);
        
        Chain plan = planService.createOrUpdate(planRepresentation);
        
        Assert.assertEquals("projectKey-planKey", plan.getKey());

        verify(repositoryService, times(1)).updateRespository(plan, repository);
        verify(chainCreationService, times(1)).createPlan(any(BuildConfiguration.class), any(ActionParametersMap.class), eq(ChainCreationService.EnablePlan.DISABLED));
    }
    
    @Test
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void createOrUpdateUpdatesPlan() throws Exception
    {
        Map dslResult = ImmutableMap.builder()
                .put("plan", ImmutableMap.builder()
                        .put("key", "planKey")
                        .put("name", "newName!")
                        .put("description", "new!")
                        .put("enabled", "true")
                        .put("project", ImmutableMap.builder()
                                .put("name", "newName!")
                                .put("key", "projectKey")
                                .put("description", "new!").build())
                        .build())
                .build();

        DefaultProject existingProject = new DefaultProject("projectKey", "old project name", "odl project description");

        Map planRepresentation = (Map) dslResult.get("plan");

        Chain existingChain = new DefaultChain();
        existingChain.setKey("projectKey-planKey");
        existingChain.setDescription("old description");
        existingChain.setBuildName("old name");
        existingChain.setProject(existingProject);
        existingChain.setSuspendedFromBuilding(false);
        
        when(planManager.isPlanKeyConflicting(any(PlanKey.class))).thenReturn(true);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(existingChain);

        Chain plan = planService.createOrUpdate(planRepresentation);
        
        Assert.assertEquals("projectKey-planKey", plan.getKey());
        Assert.assertEquals("newName!", plan.getBuildName());
        Assert.assertEquals("new!", plan.getDescription());
        Assert.assertEquals(true, plan.isSuspendedFromBuilding());

        verify(chainCreationService, times(0)).createPlan(any(BuildConfiguration.class), any(ActionParametersMap.class), any(ChainCreationService.EnablePlan.class));
    }
    
    
   
    @Test
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void createOrUpdateCreatePlanWithCorrectSharedRepository() throws Exception
    {
        Map repositoryRepre = ImmutableMap.builder()
             .put("name", "repositoryName").build();
        Map dslResult = ImmutableMap.builder()
                .put("plan", ImmutableMap.builder()
                        .put("key", "planKey")
                        .put("name", "planName")
                        .put("description", "description")
                        .put("repository", repositoryRepre)
                        .put("project", ImmutableMap.builder()
                                .put("name", "projectName")
                                .put("key", "projectKey")
                                .put("description", "").build())
                        .build())
                .build();
        

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");

        Map planRepresentation = (Map) dslResult.get("plan");
        
        when(chainCreationService.createPlan(any(BuildConfiguration.class), any(ActionParametersMap.class), any(ChainCreationService.EnablePlan.class))).thenReturn("PROJECTKEY-PLANKEY");
        when(planManager.isPlanKeyConflicting(any(PlanKey.class))).thenReturn(false);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(createdChain);
        when(repositoryService.findRespository(repositoryRepre)).thenReturn("testKey");
        
        Chain plan = planService.createOrUpdate(planRepresentation);
        
        Assert.assertEquals("projectKey-planKey", plan.getKey());
        
        verify(repositoryService, times(1)).updateRespository(createdChain, repositoryRepre);
        verify(chainCreationService, times(1)).createPlan(any(BuildConfiguration.class), any(ActionParametersMap.class), eq(ChainCreationService.EnablePlan.DISABLED));
    }
    
    
    @Test
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void createOrUpdateCreatePlanWithCorrectSharedRepositories() throws Exception
    {
        Map repository1 = ImmutableMap.builder()
             .put("name", "repositoryName1").build();
        Map repository2 = ImmutableMap.builder()
            .put("name", "repositoryName2").build();
        
        List repositories = ImmutableList.builder()
            .add(repository1)
            .add(repository2)
            .build();
        
        Map dslResult = ImmutableMap.builder()
                .put("plan", ImmutableMap.builder()
                        .put("key", "planKey")
                        .put("name", "planName")
                        .put("description", "description")
                        .put("repository", repositories)
                        .put("project", ImmutableMap.builder()
                                .put("name", "projectName")
                                .put("key", "projectKey")
                                .put("description", "").build())
                        .build())
                .build();

        Map planRepresentation = (Map) dslResult.get("plan");

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");
        
        when(chainCreationService.createPlan(any(BuildConfiguration.class), any(ActionParametersMap.class), any(ChainCreationService.EnablePlan.class))).thenReturn("PROJECTKEY-PLANKEY");
        when(planManager.isPlanKeyConflicting(any(PlanKey.class))).thenReturn(false);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(createdChain);
        when(repositoryService.findRespository(repository1)).thenReturn("testKey1");
        when(repositoryService.findRespository(repository1)).thenReturn("testKey2");

        
        Chain plan = planService.createOrUpdate(planRepresentation);
        
        Assert.assertEquals("projectKey-planKey", plan.getKey());
        
        verify(repositoryService, times(1)).updateRespository(createdChain, repositories);
        verify(chainCreationService, times(1)).createPlan(any(BuildConfiguration.class), any(ActionParametersMap.class), eq(ChainCreationService.EnablePlan.DISABLED));
    }
    
    
    @Test
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void createOrUpdateCreatePlanWithCorrectTriggers() throws Exception
    {
        Map trigger1 = ImmutableMap.builder()
             .put("description", "trigger1").build();
        Map trigger2 = ImmutableMap.builder()
            .put("description", "trigger2").build();
        
        List triggers = ImmutableList.builder()
            .add(trigger1)
            .add(trigger2)
            .build();
        
        Map dslResult = ImmutableMap.builder()
                .put("plan", ImmutableMap.builder()
                        .put("key", "planKey")
                        .put("name", "planName")
                        .put("description", "description")
                        .put("trigger", triggers)
                        .put("project", ImmutableMap.builder()
                                .put("name", "projectName")
                                .put("key", "projectKey")
                                .put("description", "").build())
                        .build())
                .build();

        Map planRepresentation = (Map) dslResult.get("plan");

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");
        
        when(chainCreationService.createPlan(any(BuildConfiguration.class), any(ActionParametersMap.class), any(ChainCreationService.EnablePlan.class))).thenReturn("PROJECTKEY-PLANKEY");
        when(planManager.isPlanKeyConflicting(any(PlanKey.class))).thenReturn(false);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(createdChain);
        
        Chain plan = planService.createOrUpdate(planRepresentation);
        
        Assert.assertEquals("projectKey-planKey", plan.getKey());
        
        verify(triggerService, times(1)).performTriggerChanges(createdChain, triggers);
        verify(chainCreationService, times(1)).createPlan(any(BuildConfiguration.class), any(ActionParametersMap.class), eq(ChainCreationService.EnablePlan.DISABLED));
    }

    @Test
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void createOrUpdateCreatePlanWithCorrectNotifications() throws Exception
    {
        Map notification = ImmutableMap.builder()
                .put("recipient", "email")
                .put("type", "Build Complete")
                .put("email", "fake@fake.com")
                .build();

        Map dslResult = ImmutableMap.builder()
                .put("plan", ImmutableMap.builder()
                        .put("key", "planKey")
                        .put("name", "planName")
                        .put("description", "description")
                        .put("notification", notification)
                        .put("project", ImmutableMap.builder()
                                .put("name", "projectName")
                                .put("key", "projectKey")
                                .put("description", "").build())
                        .build())
                .build();

        Map planRepresentation = (Map) dslResult.get("plan");

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");

        when(chainCreationService.createPlan(any(BuildConfiguration.class), any(ActionParametersMap.class), any(ChainCreationService.EnablePlan.class))).thenReturn("PROJECTKEY-PLANKEY");
        when(planManager.isPlanKeyConflicting(any(PlanKey.class))).thenReturn(false);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(createdChain);

        Chain plan = planService.createOrUpdate(planRepresentation);

        Assert.assertEquals("projectKey-planKey", plan.getKey());


        verify(notificationService, times(1)).performNotificationChanges(createdChain, notification);
        verify(chainCreationService, times(1)).createPlan(any(BuildConfiguration.class), any(ActionParametersMap.class), eq(ChainCreationService.EnablePlan.DISABLED));
    }
    
    
    @Test
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void createOrUpdateCreatePlanWithCorrectPlanLabels() throws Exception
    {
        
        Map label1 = ImmutableMap.builder()
            .put("name", "label1")
            .build();
        
        Map label2 = ImmutableMap.builder()
            .put("name", "label2")
            .build();

        
        List labels = Lists.newArrayList(label1, label2);
        
        Map dslResult = ImmutableMap.builder()
                .put("plan", ImmutableMap.builder()
                        .put("key", "planKey")
                        .put("name", "planName")
                        .put("description", "description")
                        .put("label", labels)
                        .put("project", ImmutableMap.builder()
                                .put("name", "projectName")
                                .put("key", "projectKey")
                                .put("description", "").build())
                        .build())
                .build();

        Map planRepresentation = (Map) dslResult.get("plan");

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");

        when(chainCreationService.createPlan(any(BuildConfiguration.class), any(ActionParametersMap.class), any(ChainCreationService.EnablePlan.class))).thenReturn("PROJECTKEY-PLANKEY");
        when(planManager.isPlanKeyConflicting(any(PlanKey.class))).thenReturn(false);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(createdChain);

        Chain plan = planService.createOrUpdate(planRepresentation);

        Assert.assertEquals("projectKey-planKey", plan.getKey());

        verify(labelService, times(1)).performLabelChanges(createdChain, labels);
    }
    
    @Test
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void createOrUpdateCreatePlanWithCorrectPlanVariables() throws Exception
    {
        
        Map variable1 = ImmutableMap.builder()
            .put("key", "key1")
            .put("value", "value1")
            .build();
        
        Map variable2 = ImmutableMap.builder()
            .put("key", "key2")
            .put("value", "value2")
            .build();

        
        List variables = Lists.newArrayList(variable1, variable2);
        
        Map dslResult = ImmutableMap.builder()
                .put("plan", ImmutableMap.builder()
                        .put("key", "planKey")
                        .put("name", "planName")
                        .put("description", "description")
                        .put("variable", variables)
                        .put("project", ImmutableMap.builder()
                                .put("name", "projectName")
                                .put("key", "projectKey")
                                .put("description", "").build())
                        .build())
                .build();

        Map planRepresentation = (Map) dslResult.get("plan");

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");

        when(chainCreationService.createPlan(any(BuildConfiguration.class), any(ActionParametersMap.class), any(ChainCreationService.EnablePlan.class))).thenReturn("PROJECTKEY-PLANKEY");
        when(planManager.isPlanKeyConflicting(any(PlanKey.class))).thenReturn(false);
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(createdChain);

        Chain plan = planService.createOrUpdate(planRepresentation);

        Assert.assertEquals("projectKey-planKey", plan.getKey());

        verify(variableService, times(1)).performVariableChanges(createdChain, variables);
    }


}
