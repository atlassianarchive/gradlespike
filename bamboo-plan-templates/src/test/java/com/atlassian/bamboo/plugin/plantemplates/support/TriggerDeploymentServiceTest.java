package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.build.strategy.BuildStrategy;
import com.atlassian.bamboo.build.strategy.BuildStrategyConfigurationService;
import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentTriggerService;
import com.atlassian.bamboo.plugin.plantemplates.support.triggers.TriggerMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.triggers.TriggerMappingFactory;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TriggerDeploymentServiceTest
{
    
    @InjectMocks
    private TriggerDeploymentService triggerService;

    @Mock
    private BuildStrategyConfigurationService buildStrategyConfigurationService;
    @Mock
    private RepositoryService repositoryService;
    @Mock
    private TriggerMappingFactory triggerMappingFactory;
    @Mock
    private EnvironmentTriggerService environmentTriggerService;
    

    @Test
    @SuppressWarnings("rawtypes")
    public void performTriggerChangesCreatesSingleTrigger(){
       Map trigger1 = ImmutableMap.builder()
           .put("description", "trigger1")
           .put("type", "cron")
           .build();
       
       Environment environment = mock(Environment.class);
       when(environment.getId()).thenReturn(13L);
       
       when(triggerMappingFactory.create(anyString())).thenReturn(mock(TriggerMapping.class));
        
       triggerService.performTriggersChanges(trigger1, environment);
       
       verify(environmentTriggerService, times(1)).createEnvironmentTrigger(eq(13L), eq("trigger1"), eq(Sets.<Long>newHashSet()), any(BuildConfiguration.class));
    }
    
    @Test
    @SuppressWarnings("rawtypes")
    public void performTriggerChangesCreatesMultipleTriggers(){
       Map trigger1 = ImmutableMap.builder()
           .put("description", "trigger1")
           .put("type", "cron")
           .build();
       Map trigger2 = ImmutableMap.builder()
           .put("description", "trigger2")
           .put("type", "afterSuccessfulPlan")
           .build();
       
       List triggers = ImmutableList.builder()
           .add(trigger1)
           .add(trigger2)
           .build();
       
       Environment environment = mock(Environment.class);
       when(environment.getId()).thenReturn(13L);
       
       when(triggerMappingFactory.create(anyString())).thenReturn(mock(TriggerMapping.class));
        
       triggerService.performTriggersChanges(triggers, environment);

       verify(environmentTriggerService, times(1)).createEnvironmentTrigger(eq(13L), eq("trigger1"), eq(Sets.<Long>newHashSet()), any(BuildConfiguration.class));
       verify(environmentTriggerService, times(1)).createEnvironmentTrigger(eq(13L), eq("trigger2"), eq(Sets.<Long>newHashSet()), any(BuildConfiguration.class));
        
    }
    
    @Test
    @SuppressWarnings("rawtypes")
    public void performTriggerChangesDeletesExistingTriggers()
    {
        Map trigger1 = ImmutableMap.builder().put("description", "trigger1").put("type", "cron").build();
        Map trigger2 = ImmutableMap.builder().put("description", "trigger2").put("type", "afterSuccessfulPlan").build();
        List triggers = ImmutableList.builder().add(trigger1).add(trigger2).build();
        
        //Existing Triggers
        BuildStrategy oldTrigger1 = mock(BuildStrategy.class);
        BuildStrategy oldTrigger2 = mock(BuildStrategy.class);
        when(oldTrigger1.getId()).thenReturn(1L);
        when(oldTrigger2.getId()).thenReturn(2L);
        List<BuildStrategy> oldTriggers = Lists.newArrayList(oldTrigger1, oldTrigger2);
       
        Environment environment = mock(Environment.class);
        when(environment.getId()).thenReturn(13L);
        when(environment.getTriggers()).thenReturn(oldTriggers);
        
        when(triggerMappingFactory.create(anyString())).thenReturn(mock(TriggerMapping.class));

        triggerService.performTriggersChanges(triggers, environment);

        
        verify(environmentTriggerService, times(1)).deleteEnvironmentTrigger(eq(13L), eq(1L));
        verify(environmentTriggerService, times(1)).deleteEnvironmentTrigger(eq(13L), eq(2L));


    }

    
}
