package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.chains.DefaultChain;
import com.atlassian.bamboo.notification.NotificationRuleImpl;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.NotificationMappingFactory;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.NotificationMapping;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings({"rawtypes", "unchecked"})
public class NotificationServiceTest
{
    @InjectMocks
    private NotificationService notificationService;

    @Mock
    private NotificationMappingFactory notificationMappingFactory;

    @Mock
    private PlanManager planManager;

    @Test
    public void performChangesCreatesOneNotification() {
        Map notification = ImmutableMap.builder()
                .put("recipient", "email")
                .put("type", "Build Complete")
                .put("email", "fake@fake.com")
                .build();

        Chain createdChain = new DefaultChain();

        NotificationMapping mapping = mock(NotificationMapping.class);
        when(notificationMappingFactory.create(anyString(), anyString())).thenReturn(mapping);
        when(mapping.createNotificationRule(eq(notification))).thenReturn(new NotificationRuleImpl());
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(createdChain);

        Chain updatedChain = notificationService.performNotificationChanges(createdChain, notification);

        verify(mapping, times(1)).createNotificationRule(notification);
        Assert.assertEquals(updatedChain.getNotificationSet().getNotificationRules().size(), 1);
    }

    @Test
    public void performChangesCreatesManyNotifications() {
        Map notification1 = ImmutableMap.builder()
                .put("recipient", "email")
                .put("type", "Build Complete")
                .put("email", "fake@fake.com")
                .build();

        Map notification2 = ImmutableMap.builder()
                .put("recipient", "user")
                .put("type", "Build Complete")
                .put("user", "bsmith")
                .build();

        List notifications = ImmutableList.builder()
                .add(notification1)
                .add(notification2)
                .build();

        Chain createdChain = new DefaultChain();

        NotificationMapping mapping = mock(NotificationMapping.class);
        when(notificationMappingFactory.create(anyString(), anyString())).thenReturn(mapping);
        when(mapping.createNotificationRule(anyMap())).thenReturn(new NotificationRuleImpl());
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(createdChain);

        Chain updatedChain = notificationService.performNotificationChanges(createdChain, notifications);

        verify(mapping, times(1)).createNotificationRule(notification1);
        verify(mapping, times(1)).createNotificationRule(notification2);
        Assert.assertEquals(updatedChain.getNotificationSet().getNotificationRules().size(), 1);
    }

    @Test
    public void performChangesHandlesEmptyNotifications() {

        Chain createdChain = new DefaultChain();

        NotificationMapping mapping = mock(NotificationMapping.class);
        when(notificationMappingFactory.create(anyString(), anyString())).thenReturn(mapping);
        when(mapping.createNotificationRule(anyMap())).thenReturn(new NotificationRuleImpl());
        when(planManager.getPlanByKey(any(PlanKey.class), any(Class.class))).thenReturn(createdChain);

        Chain updatedChain = notificationService.performNotificationChanges(createdChain, null);

        verify(mapping, times(0)).createNotificationRule(anyMap());
        Assert.assertEquals(updatedChain.getNotificationSet().getNotificationRules().size(), 0);
    }


}
