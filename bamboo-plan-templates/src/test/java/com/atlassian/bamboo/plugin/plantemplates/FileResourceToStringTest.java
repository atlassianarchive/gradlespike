package com.atlassian.bamboo.plugin.plantemplates;

import com.atlassian.bamboo.plugin.plantemplates.task.FileResourceToString;
import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static de.regnis.q.sequence.core.QSequenceAssert.assertTrue;
import static org.junit.Assert.assertEquals;

public class FileResourceToStringTest
{
    @Rule
    public TemporaryFolder tmpDir = new TemporaryFolder();

    @Test
    public void testMultiShortcuts() throws Exception
    {
        String shortCutStr1 = "shortCut1";
        String shortCutStr2 = "shortCut2";

        createTmpShortCutFile(shortCutStr1, "short1.groovy");
        createTmpShortCutFile(shortCutStr2, "short2.groovy");

        FileResourceToString frs = new FileResourceToString();
        String shortCut = frs.convert(tmpDir.getRoot(), "short1.groovy,short2.groovy");
        assertTrue(shortCut.contains(shortCutStr1));
        assertTrue(shortCut.contains(shortCutStr2));
    }

    private void createTmpShortCutFile(String shortCutStr, String fileName) throws IOException {
        File shortCut1File1 = tmpDir.newFile(fileName);
        FileOutputStream fos1 = new FileOutputStream(shortCut1File1);
        fos1.write(shortCutStr.getBytes());
        fos1.close();
    }
}
