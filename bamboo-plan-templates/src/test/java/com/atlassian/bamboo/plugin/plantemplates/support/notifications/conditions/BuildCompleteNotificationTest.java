package com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions;

import java.util.Map;

import com.atlassian.bamboo.notification.NotificationManager;
import com.atlassian.bamboo.notification.NotificationRule;
import com.atlassian.bamboo.notification.NotificationRuleImpl;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.RecipientMapping;

import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import org.junit.Assert;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BuildCompleteNotificationTest
{
    @InjectMocks
    private BuildCompleteNotificationMapping buildCompleteNotificationMapping;

    @Mock
    private NotificationManager notificationManager;

    @Mock
    private RecipientMapping recipientMapping;

    private Answer<NotificationRule> notificationRuleAnswer = new Answer<NotificationRule>() {
        @Override
        public NotificationRule answer(InvocationOnMock invocation) throws Throwable {
            Object[] args = invocation.getArguments();
            return createNewNotificationRule((String) args[0], (String) args[1], (String) args[2], (String) args[3]);
        }
    };

    @SuppressWarnings("rawtypes")
    @Test
    public void buildCompleteNotificationMappingReturnsCorrectRule() {
        Map notification = ImmutableMap.builder()
                .put("recipient", "email")
                .put("type", "Build Complete")
                .put("email", "fake@fake.com")
                .build();


        when(notificationManager.createNotificationRule(anyString(), anyString(), anyString(), anyString())).thenAnswer(notificationRuleAnswer);

        NotificationRule notificationRule = buildCompleteNotificationMapping.createNotificationRule(notification);
        Assert.assertNotNull(notificationRule);
        Assert.assertEquals(notificationRule.getConditionKey(), "com.atlassian.bamboo.plugin.system.notifications:chainCompleted.allBuilds");

    }

    private NotificationRule createNewNotificationRule(String conditionKey, String conditionData, String recipientString, String recipientType) {
        NotificationRule newNR = new NotificationRuleImpl();
        newNR.setNotificationManager(notificationManager);
        newNR.setConditionData(conditionData);
        newNR.setConditionKey(conditionKey);
        newNR.setRecipient(recipientString);
        newNR.setRecipientType(recipientType);
        return newNR;
    }
}
