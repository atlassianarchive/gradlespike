package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;


@RunWith(MockitoJUnitRunner.class)
public class Maven2TaskMappingTest
{

    @InjectMocks
    private Maven2TaskMapping maven2TaskMapping;
    
    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaksWithMaven2_OptionsReturnsCorrectMap(){
        Map task = ImmutableMap.builder()
            .put("type", "maven3")
            .put("description", "task1Maven3")
            .put("goal", "clean install")
            .put("environmentVariables", "environmentVariables1")
            .put("workingSubDirectory", "workingSubDirectory1")
            .put("projectFile", "projectFileTest")
            .put("buildJdk", "jdk18")
            .put("mavenExecutable", "maven 3")
            .put("hasTests", "true")
            .put("testDirectory", "**/target/my_custom/*.xml")
            .put("useMavenReturnCode", "true")
            .build();

        Map<String,String> taskActionParams = maven2TaskMapping.convert(task);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("userDescription", "task1Maven3")
            .put("taskDisabled", "false")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.maven:task.builder.mvn2")
            .put("environmentVariables", "environmentVariables1")
            .put("workingSubDirectory", "workingSubDirectory1")
            .put("projectFile", "projectFileTest")
            .put("label", "maven 3")
            .put("goal", "clean install")
            .put("buildJdk", "jdk18")
            .put("testChecked", "true")
            .put("testDirectoryOption", "customTestDirectory")
            .put("testResultsDirectory", "**/target/my_custom/*.xml")
            .put("useMavenReturnCode", "true")
            .build(); 
        
        Assert.assertEquals(expected, taskActionParams);
    }
    
}
