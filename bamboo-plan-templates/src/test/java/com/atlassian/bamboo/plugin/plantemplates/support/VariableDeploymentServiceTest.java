package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.variable.VariableConfigurationService;
import com.atlassian.bamboo.variable.VariableDefinition;
import com.atlassian.bamboo.variable.VariableDefinitionManager;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("rawtypes")
public class VariableDeploymentServiceTest
{
    
    @InjectMocks
    private VariableDeploymentService variableService;
    @Mock
    private VariableConfigurationService variableConfigurationService;
    @Mock
    private VariableDefinitionManager variableDefinitionManager;
    
    @Test
    public void performVariableChangesCreatesVariable(){
        Map variable1 = ImmutableMap.builder()
            .put("key", "key1")
            .put("value", "value1")
            .build();

        Environment environment = mock(Environment.class);
        when(environment.getId()).thenReturn(8L);
        
        variableService.performVariableChanges(variable1, environment);
        
        verify(variableConfigurationService, times(1)).createVariableForEnvironment(8L, "key1", "value1");
    }
    
    
    @Test
    public void performVariableChangesCreatesMultipleVariables(){
        Map variable1 = ImmutableMap.builder()
            .put("key", "key1")
            .put("value", "value1")
            .build();

        Map variable2 = ImmutableMap.builder()
            .put("key", "key2")
            .put("value", "value2")
            .build();
        
        List variables = Lists.newArrayList(variable1, variable2);
        
        Environment environment = mock(Environment.class);
        when(environment.getId()).thenReturn(8L);
        
        variableService.performVariableChanges(variables, environment);
        
        verify(variableConfigurationService, times(1)).createVariableForEnvironment(8L, "key1", "value1");
        verify(variableConfigurationService, times(1)).createVariableForEnvironment(8L, "key2", "value2");
    }
    
    
    @Test
    public void performVariableChangesDeletesExistingVariables(){
        Map variable1 = ImmutableMap.builder()
            .put("key", "key1")
            .put("value", "value1")
            .build();

        Map variable2 = ImmutableMap.builder()
            .put("key", "key2")
            .put("value", "value2")
            .build();
        
        List variables = Lists.newArrayList(variable1, variable2);

        Environment environment = mock(Environment.class);
        when(environment.getId()).thenReturn(8L);
        
        VariableDefinition variableDefinition1 = mock(VariableDefinition.class);
        VariableDefinition variableDefinition2 = mock(VariableDefinition.class);
        
        when(variableDefinitionManager.getDeploymentEnvironmentVariables(8L)).thenReturn(Lists.newArrayList(variableDefinition1, variableDefinition2));
        
        variableService.performVariableChanges(variables, environment);
        
        verify(variableConfigurationService, times(1)).deleteVariableDefinition(variableDefinition1);
        verify(variableConfigurationService, times(1)).deleteVariableDefinition(variableDefinition2);
    }

    


}
