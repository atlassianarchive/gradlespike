package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;

@RunWith(MockitoJUnitRunner.class)
public class ScriptTaskMappingTest
{
    @InjectMocks
    private ScriptTaskMapping scriptTaskMapper;
    
    
    // userDescription
    // taskDisabled (false, true)
    // scriptLocation (INLINE, FILE)
    // script
    // scriptBody
    // argument
    // environmentVariables
    // workingSubDirectory
    // createTaskKey -> com.atlassian.bamboo.plugins.scripttask:task.builder.script
    
    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaksWithAInlineScriptReturnsCorrectMap(){
        Map task = ImmutableMap.builder()
            .put("type", "script")
            .put("description", "task1Script")
            .put("scriptBody", "echo helloo")
            .put("argument", "argument1")
            .put("environmentVariables", "environmentVariables1")
            .put("workingSubDirectory", "workingSubDirectory1")
            .build();

        Map<String,String> taskActionParams = scriptTaskMapper.convert(task);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("userDescription", "task1Script")
            .put("taskDisabled", "false")
            .put("scriptLocation", "INLINE")
            .put("scriptBody", "echo helloo")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.scripttask:task.builder.script")
            .put("argument", "argument1")
            .put("environmentVariables", "environmentVariables1")
            .put("workingSubDirectory", "workingSubDirectory1")
            .build(); 
        
        Assert.assertEquals(expected, taskActionParams);
    }
    
    
    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaksWithAFileScriptReturnsCorrectMap(){
        Map task = ImmutableMap.builder()
            .put("type", "script")
            .put("description", "task2Script")
            .put("script", "mylocation")
            .put("taskDisabled", "true")
            .build();

        Map<String,String> taskActionParams = scriptTaskMapper.convert(task);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("userDescription", "task2Script")
            .put("taskDisabled", "true")
            .put("scriptLocation", "FILE")
            .put("script", "mylocation")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.scripttask:task.builder.script")
            .build(); 
        
        Assert.assertEquals(expected, taskActionParams);
    }


}
