package com.atlassian.bamboo.plugin.plantemplates.support;


import java.util.Map;
import java.util.Set;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.build.PlanDependency;
import com.atlassian.bamboo.build.PlanDependencyManager;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.v2.build.trigger.DependencyBlockingStrategy;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlanDependenciesServiceTest
{
    @InjectMocks
    private PlanDependenciesService planDependenciesService;
    
    @Mock
    private PlanDependencyManager planDependencyManager;
    
    @Mock 
    private BuildDefinitionManager buildDefinitionManager;
    
    @SuppressWarnings("rawtypes")
    @Test
    public void aPlanWithDependenciesMakesTheRightMethodCall() {
//        dependencies(triggerOnlyAfterAllStagesGreen: 'true', automaticDependency: 'true', triggerForBranches: 'true', blockingStrategy: 'notBlock'){
//            childPlan(planKey: 'PERSONAL-REST')
//        }
        
        Map childPlan1 = ImmutableMap.builder()
            .put("planKey", "PERSONAL-REST")
            .build();
        
        Map childPlan2 = ImmutableMap.builder()
            .put("planKey", "PERSONAL-REST2")
            .build();
        
        Map planDependencies = ImmutableMap.builder()
            .put("dependencies", ImmutableMap.builder()
                    .put("triggerOnlyAfterAllStagesGreen", "true")
                    .put("automaticDependency", "true")
                    .put("triggerForBranches", "true")
                    .put("blockingStrategy", "notBlock")                    
                    .put("childPlan", ImmutableList.of(childPlan1, childPlan2))
                    .build())
            .build();

        Chain chain = mock(Chain.class);
        when(chain.getBuildDefinition()).thenReturn(mock(BuildDefinition.class));
        
        planDependenciesService.performChanges(chain, (Map) planDependencies.get("dependencies"));
        
        Set<PlanKey> planKeys = Sets.newHashSet(PlanKeys.getPlanKey("PERSONAL-REST"), PlanKeys.getPlanKey("PERSONAL-REST2"));
        
        verify(planDependencyManager).adjustChildDependencyList(PlanDependency.DEFAULT_DEP_KEY, chain, planKeys, true);
                                                
    }
    
    @SuppressWarnings("rawtypes")
    @Test
    public void aPlanWithBlockingStrategyNotBlockHasTheRightValue() {
        //  blockingStrategy: 'notBlock' 'blockIfPendingBuilds' 'blockIfUnbuildChanges'        
        Map childPlan1 = ImmutableMap.builder()
            .put("planKey", "PERSONAL-REST")
            .build();
        
        Map planDependencies = ImmutableMap.builder()
            .put("dependencies", ImmutableMap.builder()
                    .put("triggerOnlyAfterAllStagesGreen", "true")
                    .put("automaticDependency", "true")
                    .put("triggerForBranches", "true")
                    .put("blockingStrategy", "notBlock")                    
                    .put("childPlan", ImmutableList.of(childPlan1))
                    .build())
            .build();

        Chain chain = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
        BuildDefinition buildDefinition = mock(BuildDefinition.class, Mockito.RETURNS_DEEP_STUBS);
        when(chain.getBuildDefinition()).thenReturn(buildDefinition);
        Map<String, String> customConfig = Maps.newHashMap();
        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfig);
        
        planDependenciesService.performChanges(chain, (Map) planDependencies.get("dependencies"));
        
        Assert.assertEquals(DependencyBlockingStrategy.None.toString(), customConfig.get(DependencyBlockingStrategy.DEPENDENCY_BLOCKING_STRATEGY_CONFIG_KEY));
                                                        
    }
    
    @SuppressWarnings("rawtypes")
    @Test
    public void aPlanWithBlockingStrategy_blockIfPendingBuilds_HasTheRightValue() {
        //  blockingStrategy: 'notBlock' 'blockIfPendingBuilds' 'blockIfUnbuildChanges'        
        Map childPlan1 = ImmutableMap.builder()
            .put("planKey", "PERSONAL-REST")
            .build();
        
        Map planDependencies = ImmutableMap.builder()
            .put("dependencies", ImmutableMap.builder()
                    .put("triggerOnlyAfterAllStagesGreen", "true")
                    .put("automaticDependency", "true")
                    .put("triggerForBranches", "true")
                    .put("blockingStrategy", "blockIfPendingBuilds")                    
                    .put("childPlan", ImmutableList.of(childPlan1))
                    .build())
            .build();

        Chain chain = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
        BuildDefinition buildDefinition = mock(BuildDefinition.class, Mockito.RETURNS_DEEP_STUBS);
        when(chain.getBuildDefinition()).thenReturn(buildDefinition);
        Map<String, String> customConfig = Maps.newHashMap();
        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfig);
        
        planDependenciesService.performChanges(chain, (Map) planDependencies.get("dependencies"));
        
        Assert.assertEquals(DependencyBlockingStrategy.DontBuildIfParentInQueue.toString(), customConfig.get(DependencyBlockingStrategy.DEPENDENCY_BLOCKING_STRATEGY_CONFIG_KEY));
                                                        
    }

    
    @SuppressWarnings("rawtypes")
    @Test
    public void aPlanWithBlockingStrategy_blockIfUnbuildChanges_HasTheRightValue() {
        //  blockingStrategy: 'notBlock' 'blockIfPendingBuilds' 'blockIfUnbuildChanges'        
        Map childPlan1 = ImmutableMap.builder()
            .put("planKey", "PERSONAL-REST")
            .build();
        
        Map planDependencies = ImmutableMap.builder()
            .put("dependencies", ImmutableMap.builder()
                    .put("triggerOnlyAfterAllStagesGreen", "true")
                    .put("automaticDependency", "true")
                    .put("triggerForBranches", "true")
                    .put("blockingStrategy", "blockIfUnbuildChanges")                    
                    .put("childPlan", ImmutableList.of(childPlan1))
                    .build())
            .build();

        Chain chain = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
        BuildDefinition buildDefinition = mock(BuildDefinition.class, Mockito.RETURNS_DEEP_STUBS);
        when(chain.getBuildDefinition()).thenReturn(buildDefinition);
        Map<String, String> customConfig = Maps.newHashMap();
        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfig);
        
        planDependenciesService.performChanges(chain, (Map) planDependencies.get("dependencies"));

        Assert.assertEquals(DependencyBlockingStrategy.BuildParentIfChangesDetected.toString(), customConfig.get(DependencyBlockingStrategy.DEPENDENCY_BLOCKING_STRATEGY_CONFIG_KEY));
    }
    
    @SuppressWarnings("rawtypes")
    @Test
    public void aPlanWithout_BlockingStrategy_HasNullValue() {
        //  blockingStrategy: 'notBlock' 'blockIfPendingBuilds' 'blockIfUnbuildChanges'        
        Map childPlan1 = ImmutableMap.builder()
            .put("planKey", "PERSONAL-REST")
            .build();
        
        Map planDependencies = ImmutableMap.builder()
            .put("dependencies", ImmutableMap.builder()
                    .put("triggerOnlyAfterAllStagesGreen", "true")
                    .put("automaticDependency", "true")
                    .put("triggerForBranches", "true")
                    .put("childPlan", ImmutableList.of(childPlan1))
                    .build())
            .build();

        Chain chain = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
        BuildDefinition buildDefinition = mock(BuildDefinition.class, Mockito.RETURNS_DEEP_STUBS);
        when(chain.getBuildDefinition()).thenReturn(buildDefinition);
        Map<String, String> customConfig = Maps.newHashMap();
        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfig);
        
        planDependenciesService.performChanges(chain, (Map) planDependencies.get("dependencies"));

        Assert.assertNull(customConfig.get(DependencyBlockingStrategy.DEPENDENCY_BLOCKING_STRATEGY_CONFIG_KEY));
    }
    
    
    
    
    @SuppressWarnings("rawtypes")
    @Test
    public void aPlan_dependenciesProperties_true_HasTheRightValues() {
        Map childPlan1 = ImmutableMap.builder()
            .put("planKey", "PERSONAL-REST")
            .build();
        
        Map planDependencies = ImmutableMap.builder()
            .put("dependencies", ImmutableMap.builder()
                    .put("triggerOnlyAfterAllStagesGreen", "true")
                    .put("automaticDependency", "true")
                    .put("triggerForBranches", "true")
                    .put("blockingStrategy", "blockIfUnbuildChanges")                    
                    .put("childPlan", ImmutableList.of(childPlan1))
                    .build())
            .build();

        Chain chain = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
        BuildDefinition buildDefinition = mock(BuildDefinition.class, Mockito.RETURNS_DEEP_STUBS);
        when(chain.getBuildDefinition()).thenReturn(buildDefinition);
        Map<String, String> customConfig = Maps.newHashMap();
        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfig);
        
        planDependenciesService.performChanges(chain, (Map) planDependencies.get("dependencies"));

        Assert.assertEquals("true", customConfig.get("custom.dependencies.triggerOnlyAfterAllStages"));
        Assert.assertEquals("true", customConfig.get("custom.dependencies.triggerForBranches"));
        Assert.assertEquals("true", customConfig.get("custom.dependency.automaticManagement.enabled"));
    }
    
    
    
    @SuppressWarnings("rawtypes")
    @Test
    public void aPlan_dependenciesProperties_false_HasTheRightValues() {
        Map childPlan1 = ImmutableMap.builder()
            .put("planKey", "PERSONAL-REST")
            .build();
        
        Map planDependencies = ImmutableMap.builder()
            .put("dependencies", ImmutableMap.builder()
                    .put("triggerOnlyAfterAllStagesGreen", "false")
                    .put("automaticDependency", "false")
                    .put("triggerForBranches", "false")
                    .put("blockingStrategy", "blockIfUnbuildChanges")                    
                    .put("childPlan", ImmutableList.of(childPlan1))
                    .build())
            .build();

        Chain chain = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
        BuildDefinition buildDefinition = mock(BuildDefinition.class, Mockito.RETURNS_DEEP_STUBS);
        when(chain.getBuildDefinition()).thenReturn(buildDefinition);
        Map<String, String> customConfig = Maps.newHashMap();
        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfig);
        
        planDependenciesService.performChanges(chain, (Map) planDependencies.get("dependencies"));

        Assert.assertEquals("false", customConfig.get("custom.dependencies.triggerOnlyAfterAllStages"));
        Assert.assertEquals("false", customConfig.get("custom.dependencies.triggerForBranches"));
        Assert.assertEquals("false", customConfig.get("custom.dependency.automaticManagement.enabled"));
    }
    
    @SuppressWarnings("rawtypes")
    @Test
    public void aPlan_dependenciesProperties_empty_HasTheRightValues() {
        Map childPlan1 = ImmutableMap.builder()
            .put("planKey", "PERSONAL-REST")
            .build();
        
        Map planDependencies = ImmutableMap.builder()
            .put("dependencies", ImmutableMap.builder()
                    .put("childPlan", ImmutableList.of(childPlan1))
                    .build())
            .build();

        Chain chain = mock(Chain.class, Mockito.RETURNS_DEEP_STUBS);
        BuildDefinition buildDefinition = mock(BuildDefinition.class, Mockito.RETURNS_DEEP_STUBS);
        when(chain.getBuildDefinition()).thenReturn(buildDefinition);
        Map<String, String> customConfig = Maps.newHashMap();
        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfig);
        
        planDependenciesService.performChanges(chain, (Map) planDependencies.get("dependencies"));

        Assert.assertNull(customConfig.get("custom.dependencies.triggerOnlyAfterAllStages"));
        Assert.assertNull(customConfig.get("custom.dependencies.triggerForBranches"));
        Assert.assertNull(customConfig.get("custom.dependency.automaticManagement.enabled"));
    }
    
    
}
