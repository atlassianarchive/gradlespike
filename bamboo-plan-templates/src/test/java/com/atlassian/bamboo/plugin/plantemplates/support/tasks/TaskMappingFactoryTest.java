package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.CopyToUnicornTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.ExecuteOnUnicornTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.InstallOnDemandTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.PromoteArtifactsVersionTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.PromotePluginVersionTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.PromoteProductVersionTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.ProvisionUnicornTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.RemoveArtifactsVersionTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.SetupUnicornTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.RaiseDeploymentTicketTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.StartUnicornTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.StopUnicornTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn.UpdateLicenseTaskMapping;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_COPY_TO_UNICORN;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_EXECUTE_ON_UNICORN;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_INSTALL_ONDEMAND;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_PROMOTE_PLUGIN_VERSION;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_PROMOTE_PRODUCT_VERSION;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_PROMOTE_MULTIPLE_VERSION;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_REMOVE_MULTIPLE_VERSION;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_PROVISION_UNICORN;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_SETUP_UNICORN;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_RAISE_DEPLOYMENT_TICKET;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_START_UNICORN;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_STOP_UNICORN;
import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_UPDATE_LICENSE;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class TaskMappingFactoryTest
{

    @InjectMocks
    private TaskMappingFactory taskMappingFactory;

    @Test
    public void factoryReturnsAScriptTask()
    {
        TaskMapping taskMapping = taskMappingFactory.create("script");
        assertThat(taskMapping, instanceOf(ScriptTaskMapping.class));
    }
    
    @Test
    public void factoryReturnsACheckoutTask()
    {
        TaskMapping taskMapping = taskMappingFactory.create("checkout");
        assertThat(taskMapping, instanceOf(CheckoutTaskMapping.class));
    }
    
    @Test
    public void factoryReturnsAMaven3Task()
    {
        TaskMapping taskMapping = taskMappingFactory.create("maven3");
        assertThat(taskMapping, instanceOf(Maven3TaskMapping.class));
    }

    @Test
    public void factoryReturnsAMaven2Task()
    {
        TaskMapping taskMapping = taskMappingFactory.create("maven2");
        assertThat(taskMapping, instanceOf(Maven2TaskMapping.class));
    }
    
    @Test
    public void factoryReturnsADeployBambooPluginTask()
    {
        TaskMapping taskMapping = taskMappingFactory.create("deployBambooPlugin");
        assertThat(taskMapping, instanceOf(DeployBambooPluginTaskMapping.class));
    }

    @Test
    public void factoryReturnsADeployConfluencePluginTask()
    {
        TaskMapping taskMapping = taskMappingFactory.create("deployConfluencePlugin");
        assertThat(taskMapping, instanceOf(DeployConfluencePluginTaskMapping.class));
    }
    
    @Test
    public void factoryReturnsADeployJiraPluginTask()
    {
        TaskMapping taskMapping = taskMappingFactory.create("deployJiraPlugin");
        assertThat(taskMapping, instanceOf(DeployJiraPluginTaskMapping.class));
    }

    @Test
    public void factoryReturnsAJUnitParserTask()
    {
        TaskMapping taskMapping = taskMappingFactory.create("jUnitParser");
        assertThat(taskMapping, instanceOf(JUnitParserTaskMapping.class));
    }
    
    @Test
    public void factoryReturnsBrokerUrlDiscoveryTask()
    {
        TaskMapping taskMapping = taskMappingFactory.create("brokerUrlDiscovery");
        assertThat(taskMapping, instanceOf(BrokerUrlDiscoveryTaskMapping.class));
    }

    @Test
    public void factoryReturnsNodejsTask()
    {
        TaskMapping taskMapping = taskMappingFactory.create("nodejs");
        assertThat(taskMapping, instanceOf(NodeJsTaskMapping.class));
    }
    
    @Test
    public void factoryReturnsNpmTask()
    {
        TaskMapping taskMapping = taskMappingFactory.create("npm");
        assertThat(taskMapping, instanceOf(NpmTaskMapping.class));
    }
    
    @Test
    public void factoryReturnsCleanWorkingDirectoryTask()
    {
        TaskMapping taskMapping = taskMappingFactory.create("cleanWorkingDirectory");
        assertThat(taskMapping, instanceOf(CleanWorkingDirectoryTaskMapping.class));
    }
    
    @Test
    public void factoryReturnsArtifactDownloadTaskMapping()
    {
        TaskMapping taskMapping = taskMappingFactory.create("artifactDownload");
        assertThat(taskMapping, instanceOf(ArtifactDownloadTaskMapping.class));
    }
    
    @Test
    public void factoryReturnsAddRequirementTaskMapping()
    {
        TaskMapping taskMapping = taskMappingFactory.create("addRequirement");
        assertThat(taskMapping, instanceOf(AddRequirementTaskMapping.class));
    }
    
    @Test
    public void factoryReturnsInjectBambooVariablesTaskMapping()
    {
        TaskMapping taskMapping = taskMappingFactory.create("injectBambooVariables");
        assertThat(taskMapping, instanceOf(InjectBambooVariablesTaskMapping.class));
    }

    @Test
    public void factoryReturnsProvisionUnicornTaskMapping()
    {
        TaskMapping taskMapping = taskMappingFactory.create(BAMBOONICORN_PROVISION_UNICORN.getTaskKey());
        assertThat(taskMapping, instanceOf(ProvisionUnicornTaskMapping.class));
    }

    @Test
    public void factoryReturnsInstallOnDemandTaskMapping()
    {
        TaskMapping taskMapping = taskMappingFactory.create(BAMBOONICORN_INSTALL_ONDEMAND.getTaskKey());
        assertThat(taskMapping, instanceOf(InstallOnDemandTaskMapping.class));
    }

    @Test
    public void factoryReturnsPromotePluginVersionTaskMapping()
    {
        TaskMapping taskMapping = taskMappingFactory.create(BAMBOONICORN_PROMOTE_PLUGIN_VERSION.getTaskKey());
        assertThat(taskMapping, instanceOf(PromotePluginVersionTaskMapping.class));
    }

    @Test
    public void factoryReturnsPromoteProductVersionTaskMapping()
    {
        TaskMapping taskMapping = taskMappingFactory.create(BAMBOONICORN_PROMOTE_PRODUCT_VERSION.getTaskKey());
        assertThat(taskMapping, instanceOf(PromoteProductVersionTaskMapping.class));
    }

    @Test
    public void factoryReturnsPromoteArtifactsVersionTaskMapping()
    {
        TaskMapping taskMapping = taskMappingFactory.create(BAMBOONICORN_PROMOTE_MULTIPLE_VERSION.getTaskKey());
        assertThat(taskMapping, instanceOf(PromoteArtifactsVersionTaskMapping.class));
    }

    @Test
    public void factoryReturnsRemoveArtifactsVersionTaskMapping()
    {
        TaskMapping taskMapping = taskMappingFactory.create(BAMBOONICORN_REMOVE_MULTIPLE_VERSION.getTaskKey());
        assertThat(taskMapping, instanceOf(RemoveArtifactsVersionTaskMapping.class));
    }

    @Test
    public void factoryReturnsStopUnicornTaskMapping()
    {
        TaskMapping taskMapping = taskMappingFactory.create(BAMBOONICORN_STOP_UNICORN.getTaskKey());
        assertThat(taskMapping, instanceOf(StopUnicornTaskMapping.class));
    }

    @Test
    public void factoryReturnsUpdateLicenseTaskMapping()
    {
        TaskMapping taskMapping = taskMappingFactory.create(BAMBOONICORN_UPDATE_LICENSE.getTaskKey());
        assertThat(taskMapping, instanceOf(UpdateLicenseTaskMapping.class));
    }

    @Test
    public void factoryReturnsStartUnicornTaskMapping()
    {
        TaskMapping taskMapping = taskMappingFactory.create(BAMBOONICORN_START_UNICORN.getTaskKey());
        assertThat(taskMapping, instanceOf(StartUnicornTaskMapping.class));
    }

    @Test
    public void factoryReturnsExecuteOnUnicornTaskMapping()
    {
        TaskMapping taskMapping = taskMappingFactory.create(BAMBOONICORN_EXECUTE_ON_UNICORN.getTaskKey());
        assertThat(taskMapping, instanceOf(ExecuteOnUnicornTaskMapping.class));
    }

    @Test
    public void factoryReturnsSetupUnicornTaskMapping()
    {
        TaskMapping taskMapping = taskMappingFactory.create(BAMBOONICORN_SETUP_UNICORN.getTaskKey());
        assertThat(taskMapping, instanceOf(SetupUnicornTaskMapping.class));
    }

    @Test
    public void factoryReturnsCopyToUnicornTaskMapping()
    {
        TaskMapping taskMapping = taskMappingFactory.create(BAMBOONICORN_COPY_TO_UNICORN.getTaskKey());
        assertThat(taskMapping, instanceOf(CopyToUnicornTaskMapping.class));
    }

    @Test
    public void factoryReturnsRaiseDeploymentTicketTaskMapping()
    {
        TaskMapping taskMapping = taskMappingFactory.create(BAMBOONICORN_RAISE_DEPLOYMENT_TICKET.getTaskKey());
        assertThat(taskMapping, instanceOf(RaiseDeploymentTicketTaskMapping.class));
    }

    @Test
    public void factoryReturnsRaiseDAntTaskMapping()
    {
        TaskMapping taskMapping = taskMappingFactory.create("ant");
        assertThat(taskMapping, instanceOf(AntTaskMapping.class));
    }
}
