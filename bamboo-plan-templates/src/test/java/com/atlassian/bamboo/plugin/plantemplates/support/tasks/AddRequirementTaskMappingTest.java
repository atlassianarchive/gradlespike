package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;

@RunWith(MockitoJUnitRunner.class)
public class AddRequirementTaskMappingTest
{
    @InjectMocks
    private AddRequirementTaskMapping addRequirementTaskMapping;
    
    @Test
    @SuppressWarnings("rawtypes")
    public void convertReturnsCorrectMap(){
        Map requirementMap = ImmutableMap.builder()
            .put("key", "key")
            .put("condition", "equals")
            .put("value", "asdf")
            .build();
        
        Map task = ImmutableMap.builder()
                .put("type", "addRequirement")
                .put("description", "task description")
                .put("requirement", requirementMap)
                .build();

        Map<String,String> taskActionParams = addRequirementTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("userDescription", "task description")
                .put("taskDisabled", "false")
                .put("createTaskKey", "com.atlassian.bamboo.plugin.requirementtask:task.requirements")
                .put("requirementKey", "key")
                .put("requirementMatchType", "equal")
                .put("requirementMatchValue", "asdf")
                .put("regexMatchValue", "")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }
    
    
    @Test
    @SuppressWarnings("rawtypes")
    public void convertReturnsCorrectMapForARegexp(){
        Map requirementMap = ImmutableMap.builder()
            .put("key", "key")
            .put("condition", "matches")
            .put("value", "asdf")
            .build();
        
        Map task = ImmutableMap.builder()
                .put("type", "addRequirement")
                .put("description", "task description")
                .put("requirement", requirementMap)
                .build();

        Map<String,String> taskActionParams = addRequirementTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("userDescription", "task description")
                .put("taskDisabled", "false")
                .put("createTaskKey", "com.atlassian.bamboo.plugin.requirementtask:task.requirements")
                .put("requirementKey", "key")
                .put("requirementMatchType", "match")
                .put("requirementMatchValue", "")
                .put("regexMatchValue", "asdf")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }
    
    @Test
    @SuppressWarnings("rawtypes")
    public void convertReturnsCorrectMapForExists(){
        Map requirementMap = ImmutableMap.builder()
            .put("key", "key")
            .put("condition", "exists")
            .build();
        
        Map task = ImmutableMap.builder()
                .put("type", "addRequirement")
                .put("description", "task description")
                .put("requirement", requirementMap)
                .build();

        Map<String,String> taskActionParams = addRequirementTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("userDescription", "task description")
                .put("taskDisabled", "false")
                .put("createTaskKey", "com.atlassian.bamboo.plugin.requirementtask:task.requirements")
                .put("requirementKey", "key")
                .put("requirementMatchType", "exist")
                .put("requirementMatchValue", "")
                .put("regexMatchValue", "")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }
}
