package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_PROVISION_UNICORN;

@RunWith(MockitoJUnitRunner.class)
public class ProvisionUnicornTaskMappingTest
{
    @InjectMocks
    private ProvisionUnicornTaskMapping provisionUnicornTaskMapping;

    @Test
    @SuppressWarnings("rawtypes")
    public void convertAProvisionUnicornTaskWithMinimalParamsReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_PROVISION_UNICORN.getTaskKey())
                .put("description", "task description")
                .build();

        Map<String,String> taskActionParams = provisionUnicornTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_PROVISION_UNICORN.createTaskMappingKey())
                .put("username", "admin")
                .put("email", "admin@atlassian.com")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }

    @Test
    @SuppressWarnings("rawtypes")
    public void convertAProvisionUnicornTaskReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_PROVISION_UNICORN.getTaskKey())
                .put("description", "task description")
                .put("username", "awang")
                .put("email", "awang@atlassian.com")
                .put("password", "nicetry")
                .put("unicornInstance", "test-unicorn-instance.jira-dev.com")
                .put("gappsDomain", "gapps-link.com")
                .put("version", "random-icebat.dev.12")
                .build();

        Map<String,String> taskActionParams = provisionUnicornTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_PROVISION_UNICORN.createTaskMappingKey())
                .put("username", "awang")
                .put("email", "awang@atlassian.com")
                .put("password", "nicetry")
                .put("unicornInstance", "test-unicorn-instance.jira-dev.com")
                .put("gappsDomain", "gapps-link.com")
                .put("version", "random-icebat.dev.12")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }
}
