package com.atlassian.bamboo.plugin.plantemplates.support.triggers;

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class CronTriggerMappingTest
{
    @InjectMocks
    private CronTriggerMapping cronTriggetMapping;
    
    @SuppressWarnings("rawtypes")
    @Test
    public void convertACronTriggerReturnsCorrectMap() {
        
        Map trigger = ImmutableMap.builder()
            .put("description", "trigger1")
            .put("type", "cron")
            .put("cronExpression", "0 0 0 ? * *")
            .build();  

        Map<String, String> result = cronTriggetMapping.convert(trigger);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("selectedBuildStrategy", "schedule")
            .put("repository.change.schedule.cronExpression", "0 0 0 ? * *")
            .put("custom.rejectBranchBuildWithoutChange.enabled", "false")
            .build();
        
        Assert.assertEquals(expected, result);
        
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void convertACronTriggerWithOnlyBranchesWithChangesReturnsCorrectMap() {

        Map trigger = ImmutableMap.builder()
                .put("description", "trigger1")
                .put("type", "cron")
                .put("cronExpression", "0 0 0 ? * *")
                .put("onlyBranchesWithChanges", "true")
                .build();

        Map<String, String> result = cronTriggetMapping.convert(trigger);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("selectedBuildStrategy", "schedule")
                .put("repository.change.schedule.cronExpression", "0 0 0 ? * *")
                .put("custom.rejectBranchBuildWithoutChange.enabled", "true")
                .build();

        Assert.assertEquals(expected, result);

    }
}
