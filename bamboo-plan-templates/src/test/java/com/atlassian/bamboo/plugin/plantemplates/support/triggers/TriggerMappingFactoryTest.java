package com.atlassian.bamboo.plugin.plantemplates.support.triggers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;

@RunWith(MockitoJUnitRunner.class)
public class TriggerMappingFactoryTest
{

    @InjectMocks
    private TriggerMappingFactory triggerMappingFactory;
    
    @Test
    public void factoryReturnsACronTrigger(){
        TriggerMapping triggerMapping;
        
        triggerMapping = triggerMappingFactory.create("cron");
        Assert.assertTrue(triggerMapping instanceof CronTriggerMapping);
    }
    
    
    @Test
    public void factoryReturnsADailyTrigger(){
        TriggerMapping triggerMapping;
        
        triggerMapping = triggerMappingFactory.create("daily");
        Assert.assertTrue(triggerMapping instanceof DailyTriggerMapping);
    }
    
    @Test
    public void factoryReturnsAPollingTrigger(){
        TriggerMapping triggerMapping;
        
        triggerMapping = triggerMappingFactory.create("polling");
        Assert.assertTrue(triggerMapping instanceof PollingTriggerMapping);
    }
    
    @Test
    public void factoryReturnsAPushTrigger(){
        TriggerMapping triggerMapping;
        
        triggerMapping = triggerMappingFactory.create("push");
        Assert.assertTrue(triggerMapping instanceof PushTriggerMapping);
    }
    
    @Test
    public void factoryReturnsAafterSuccessfulPlanTrigger(){
        TriggerMapping triggerMapping;
        
        triggerMapping = triggerMappingFactory.create("afterSuccessfulPlan");
        Assert.assertTrue(triggerMapping instanceof AfterSuccessfulPlanTriggerMapping);
    }
}
    
