package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.build.DefaultJob;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.build.creation.JobCreationService;
import com.atlassian.bamboo.build.creation.PlanCreationService.EnablePlan;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.chains.ChainStage;
import com.atlassian.bamboo.chains.DefaultChain;
import com.atlassian.bamboo.deletion.DeletionService;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.branch.ChainBranchManager;
import com.atlassian.bamboo.webwork.util.ActionParametersMapImpl;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.atlassian.event.api.EventPublisher;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("rawtypes")
public class JobServiceTest
{
    @InjectMocks
    private JobService jobService;
 
    @Mock
    private JobCreationService jobCreationService;
    @Mock
    private DeletionService deletionService;
    @Mock
    private ChainBranchManager chainBranchManager;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private PlanManager planManager;
    @Mock
    private BuildDefinitionManager buildDefinitionManager;
    @Mock
    private TaskService taskService;
    @Mock
    private RequirementsService requirementsService;
    @Mock
    private ArtifactService artifactService;
    @Mock
    private MiscellaneousJobService miscellaneousJobService; 
    

    @Test
    public void performChangesCreatesOneJob() throws Exception{

        Map jobRepresentation = ImmutableMap.builder()
                .put("name", "job1Name")
                .put("key", "JOB1KEY")
                .put("description", "job1description")
                .put("enabled", "true")
                .build();

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectkey-plankey1");
        ChainStage stage = createdChain.addNewStage("defaultStage", "default", false);
        Job job = mock(DefaultJob.class);
        when(jobCreationService.createSingleJob(any(BuildConfiguration.class), any(ActionParametersMapImpl.class), any(EnablePlan.class))).thenReturn("projectkey-plankey1-JOB1KEY");
        when(planManager.getPlanByKey(PlanKeys.getPlanKey("projectkey-plankey1-JOB1KEY"), Job.class)).thenReturn(job);
        when(requirementsService.performChanges(any(), any(Job.class))).thenReturn(job);
        when(artifactService.performDefinitionChanges(any(), any(Job.class))).thenReturn(job);
        when(artifactService.performSubscriptionChanges(any(), any(Job.class))).thenReturn(job);
        when(miscellaneousJobService.performChanges(anyObject(), any(Job.class))).thenReturn(job);
        when(job.getStage()).thenReturn(stage);
       

        stage = jobService.performJobChanges(jobRepresentation, stage);
        
        Map<String, Object> jobContext = Maps.newHashMap();
        jobContext.put(JobCreationService.BUILD_KEY, "projectkey-plankey1");
        jobContext.put(JobCreationService.BUILD_NAME, "job1Name");
        jobContext.put(JobCreationService.SUB_BUILD_KEY, "JOB1KEY");
        jobContext.put(JobCreationService.EXISTING_STAGE, "defaultStage");
        jobContext.put(JobCreationService.BUILD_DESCRIPTION, "job1description");
        jobContext.put(JobCreationService.CHAIN, createdChain);
        ActionParametersMapImpl actionParams = new ActionParametersMapImpl(jobContext);
        assertNotNull(stage);
        verify(jobCreationService, times(1)).createJobAndBranches(any(BuildConfiguration.class), eq(actionParams), eq(EnablePlan.ENABLED));
    }
    
    
    @Test
    public void performChangesCreatesMultipleJobs() throws Exception{

        Map jobRepresentation1 = ImmutableMap.builder()
                .put("name", "job1Name")
                .put("key", "JOB1KEY")
                .put("description", "job1description")
                .put("enabled", "true")
                .build();
        
        Map jobRepresentation2 = ImmutableMap.builder()
            .put("name", "job2Name")
            .put("key", "job2Key")
            .put("description", "job2description")
            .put("enabled", "true")
            .build();
        
        List jobsRepresentation = ImmutableList.builder()
            .add(jobRepresentation1)
            .add(jobRepresentation2)
            .build();

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectkey-plankey1");
        ChainStage stage = createdChain.addNewStage("defaultStage", "default", false);
        Job job = mock(DefaultJob.class);
        when(jobCreationService.createSingleJob(any(BuildConfiguration.class), any(ActionParametersMapImpl.class), any(EnablePlan.class))).thenReturn("projectkey-plankey1-JOB1KEY");
        when(planManager.getPlanByKey(PlanKeys.getPlanKey("projectkey-plankey1-JOB1KEY"), Job.class)).thenReturn(job);
        when(requirementsService.performChanges(any(), any(Job.class))).thenReturn(job);
        when(artifactService.performDefinitionChanges(any(), any(Job.class))).thenReturn(job);
        when(artifactService.performSubscriptionChanges(any(), any(Job.class))).thenReturn(job);
        when(miscellaneousJobService.performChanges(anyObject(), any(Job.class))).thenReturn(job);

        
        when(job.getStage()).thenReturn(stage);

        stage = jobService.performJobChanges(jobsRepresentation, stage);
        
        Map<String, Object> jobContext1 = Maps.newHashMap();
        jobContext1.put(JobCreationService.BUILD_KEY, "projectkey-plankey1");
        jobContext1.put(JobCreationService.BUILD_NAME, "job1Name");
        jobContext1.put(JobCreationService.SUB_BUILD_KEY, "JOB1KEY");
        jobContext1.put(JobCreationService.EXISTING_STAGE, "defaultStage");
        jobContext1.put(JobCreationService.BUILD_DESCRIPTION, "job1description");
        jobContext1.put(JobCreationService.CHAIN, createdChain);
        ActionParametersMapImpl actionParams1 = new ActionParametersMapImpl(jobContext1);
        
        Map<String, Object> jobContext2 = Maps.newHashMap();
        jobContext2.put(JobCreationService.BUILD_KEY, "projectkey-plankey1");
        jobContext2.put(JobCreationService.BUILD_NAME, "job2Name");
        jobContext2.put(JobCreationService.SUB_BUILD_KEY, "job2Key");
        jobContext2.put(JobCreationService.EXISTING_STAGE, "defaultStage");
        jobContext2.put(JobCreationService.BUILD_DESCRIPTION, "job2description");
        jobContext2.put(JobCreationService.CHAIN, createdChain);
        ActionParametersMapImpl actionParams2 = new ActionParametersMapImpl(jobContext2);
        
        assertNotNull(stage);
        verify(jobCreationService, times(1)).createJobAndBranches(any(BuildConfiguration.class), eq(actionParams1), eq(EnablePlan.ENABLED));
        verify(jobCreationService, times(1)).createJobAndBranches(any(BuildConfiguration.class), eq(actionParams2), eq(EnablePlan.ENABLED));
    }
    
    @Test
    public void performChangesUpdateJobs() throws Exception{

        // XXX not TDD style, because C&P from the xWork Action form Bamboo :S
        
        Map jobRepresentation1 = ImmutableMap.builder()
            .put("name", "job1.1Name")
            .put("key", "JOB1KEY")
            .put("description", "job1.1description")
            .put("enabled", "true")
            .build();
        
        Map jobRepresentation2 = ImmutableMap.builder()
            .put("name", "job2Name")
            .put("key", "job2Key")
            .put("description", "job2description")
            .put("enabled", "true")
            .build();
        
        List jobsRepresentation = ImmutableList.builder()
            .add(jobRepresentation1)
            .add(jobRepresentation2)
            .build();

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectkey-plankey1");
        ChainStage stage = createdChain.addNewStage("defaultStage", "default", false);
        DefaultJob existingJob = spy(new DefaultJob());
        existingJob.setDescription("OLD_DESCRIPTION");
        existingJob.setBuildName("job1Name");
        existingJob.setBuildKey("JOB1KEY");
        existingJob.setDescription("job1Description");
        existingJob.setBuildDefinitionManager(buildDefinitionManager);
        stage.addJob(existingJob);
        existingJob.setStage(stage);


        Job newJob = mock(Job.class);

        when(jobCreationService.createSingleJob(any(BuildConfiguration.class), any(ActionParametersMapImpl.class), any(EnablePlan.class))).thenReturn("projectkey-plankey1-job2Key");
        when(planManager.getPlanByKey(PlanKeys.getPlanKey("projectkey-plankey1-job2Key"), Job.class)).thenReturn(newJob);
        when(planManager.getPlanByKey(PlanKeys.getPlanKey("projectkey-plankey1-JOB1KEY"), Job.class)).thenReturn(existingJob);
        when(requirementsService.performChanges(any(), any(Job.class))).thenReturn(existingJob);
        when(artifactService.performDefinitionChanges(any(), any(Job.class))).thenReturn(existingJob);
        when(artifactService.performSubscriptionChanges(any(), any(Job.class))).thenReturn(existingJob);
        when(miscellaneousJobService.performChanges(anyObject(), any(Job.class))).thenReturn(existingJob);

        when(newJob.getStage()).thenReturn(stage);
        when(existingJob.getPlanKey()).thenReturn(PlanKeys.getPlanKey("projectkey-plankey1-JOB1KEY"));

        stage = jobService.performJobChanges(jobsRepresentation, stage);
       
        Map<String, Object> jobContext = Maps.newHashMap();
        jobContext.put(JobCreationService.BUILD_KEY, "projectkey-plankey1");
        jobContext.put(JobCreationService.BUILD_NAME, "job2Name");
        jobContext.put(JobCreationService.SUB_BUILD_KEY, "job2Key");
        jobContext.put(JobCreationService.EXISTING_STAGE, "defaultStage");
        jobContext.put(JobCreationService.BUILD_DESCRIPTION, "job2description");
        jobContext.put(JobCreationService.CHAIN, createdChain);
        ActionParametersMapImpl actionParams2 = new ActionParametersMapImpl(jobContext);
        
        assertNotNull(stage);
        verify(jobCreationService, times(1)).createJobAndBranches(any(BuildConfiguration.class), eq(actionParams2), eq(EnablePlan.ENABLED));
        
        Job result1 = Iterables.get(stage.getJobs(), 0);
        assertEquals("job1.1Name", result1.getBuildName());
        assertEquals("job1.1description", result1.getDescription());
        assertEquals("JOB1KEY", result1.getBuildKey());
        
    }
    
    
    @Test
    public void performChangesDeleteJobs() throws Exception{

        Map jobRepresentation1 = ImmutableMap.builder()
                .put("name", "job1.1Name")
                .put("key", "JOB1KEY")
                .put("description", "job1.1description")
                .put("enabled", "true")
                .build();
        
        List jobsRepresentation = ImmutableList.builder()
            .add(jobRepresentation1)
            .build();

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectkey-plankey1");
        ChainStage stage = createdChain.addNewStage("defaultStage", "default", false);
       
        // job1
        DefaultJob job1 = new DefaultJob();
        job1.setBuildName("job1Name");
        job1.setBuildKey("JOB1KEY");
        job1.setDescription("job1Description");
        
        //XXX needs to setup PlayKey because DefaultJob doesn't have equals and hashcode and 
        // extends AbstractPlan that use PlanKey for equals and hashcode
        // so stage.addJob(defaultJob) doesn't add if the plan key it's not different
        // BUG in Bamboo ??????????????????????????????????
        job1.setPlanKey(PlanKeys.getPlanKey("projectkey-plankey1"));
        job1.setBuildDefinitionManager(buildDefinitionManager);
        job1.setStage(stage);
        stage.addJob(job1);

        // job2
        DefaultJob job2 = new DefaultJob();
        job2.setBuildName("job2Name");
        job2.setBuildKey("JOB2KEY");
        job2.setDescription("job2Description");
        job2.setPlanKey(PlanKeys.getPlanKey("projectkey-plankey2"));
        job1.setBuildDefinitionManager(buildDefinitionManager);
        job2.setStage(stage);
        stage.addJob(job2);
        
        // job3
        DefaultJob job3 = new DefaultJob();
        job3.setBuildName("job3Name");
        job3.setBuildKey("JOB3KEY");
        job3.setDescription("job3Description");
        job3.setPlanKey(PlanKeys.getPlanKey("projectkey-plankey3"));
        job3.setStage(stage);
        stage.addJob(job3);

        when(jobCreationService.createSingleJob(any(BuildConfiguration.class), any(ActionParametersMapImpl.class), any(EnablePlan.class))).thenReturn("projectkey-plankey1-JOB1KEY");
        when(planManager.getPlanByKey(PlanKeys.getPlanKey("projectkey-plankey1"), Job.class)).thenReturn(job1);
        when(requirementsService.performChanges(any(), any(Job.class))).thenReturn(job1);
        when(artifactService.performDefinitionChanges(any(), any(Job.class))).thenReturn(job1);
        when(artifactService.performSubscriptionChanges(any(), any(Job.class))).thenReturn(job1);
        when(miscellaneousJobService.performChanges(anyObject(), any(Job.class))).thenReturn(job1);

        stage = jobService.performJobChanges(jobsRepresentation, stage);
       
        assertNotNull(stage);
        verify(jobCreationService, times(0)).createSingleJob(any(BuildConfiguration.class), any(ActionParametersMapImpl.class), any(EnablePlan.class));

        
        verify(deletionService, times(1)).deletePlan(job2);
        verify(deletionService, times(1)).deletePlan(job3);
        //assertEquals(1, stage.getJobs().size());
        
        
    }
    
    
    @Test
    public void performChangesAppliesTheChangesToTheNestedTask() throws Exception{
        
        Map tasksRepresentation1 = ImmutableMap.builder()
            .put("type", "script_task")
            .put("description", "task1Script")
            .put("script_body", "echo hellooooo")
            .build();
        
        Map tasksRepresentation2 = ImmutableMap.builder()
            .put("type", "script_task")
            .put("description", "task2Script")
            .put("script_body", "echo hellooooo")
            .build();
        
     
        Map jobRepresentation1 = ImmutableMap.builder()
            .put("name", "job1.1Name")
            .put("key", "JOB1KEY")
            .put("description", "job1.1description")
            .put("enabled", "true")
            .put("task", tasksRepresentation1)
            .build();
    
        Map jobRepresentation2 = ImmutableMap.builder()
            .put("name", "job2Name")
            .put("key", "job2Key")
            .put("description", "job2description")
            .put("enabled", "true")
            .put("task", tasksRepresentation2)
            .build();
        
        List jobsRepresentation = ImmutableList.builder()
            .add(jobRepresentation1)
            .add(jobRepresentation2)
            .build();
        
        
        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectkey-plankey1");
        ChainStage stage = createdChain.addNewStage("defaultStage", "default", false);
        Job job1 = mock(DefaultJob.class);
        Job job2 = mock(DefaultJob.class);
        when(jobCreationService.createSingleJob(any(BuildConfiguration.class), any(ActionParametersMapImpl.class), any(EnablePlan.class))).thenReturn("projectkey-plankey1-JOB1KEY");
        when(planManager.getPlanByKey(PlanKeys.getPlanKey("projectkey-plankey1-JOB1KEY"), Job.class)).thenReturn(job1);
        when(planManager.getPlanByKey(PlanKeys.getPlanKey("projectkey-plankey1-job2Key"), Job.class)).thenReturn(job2);
        when(requirementsService.performChanges(null, job1)).thenReturn(job1);
        when(requirementsService.performChanges(null, job2)).thenReturn(job2);
        when(taskService.performTasksChanges(eq(tasksRepresentation1), any(Job.class))).thenReturn(job1);
        when(taskService.performTasksChanges(eq(tasksRepresentation2), any(Job.class))).thenReturn(job2);
        when(job1.getStage()).thenReturn(stage);
        when(job2.getStage()).thenReturn(stage);

        stage = jobService.performJobChanges(jobsRepresentation, stage);
        
        verify(taskService, times(1)).performTasksChanges(eq(tasksRepresentation1), any(Job.class));
        verify(taskService, times(1)).performTasksChanges(eq(tasksRepresentation2), any(Job.class));

    }

    @Test
    public void performChangesAppliesChangesToRequirements() throws Exception{
        Map requirement = ImmutableMap.builder()
                .put("key", "ciagent")
                .put("value", "true")
                .put("condition", "equals")
                .build();

        Map tasksRepresentation = ImmutableMap.builder()
                .put("type", "script_task")
                .put("description", "task1Script")
                .put("script_body", "echo hellooooo")
                .build();

        Map jobRepresentation = ImmutableMap.builder()
                .put("name", "job1.1Name")
                .put("key", "JOB1KEY")
                .put("description", "job1.1description")
                .put("enabled", "true")
                .put("requirement", requirement)
                .put("task", tasksRepresentation)
                .build();

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectkey-plankey1");
        ChainStage stage = createdChain.addNewStage("defaultStage", "default", false);
        Job job = mock(DefaultJob.class);
        when(jobCreationService.createSingleJob(any(BuildConfiguration.class), any(ActionParametersMapImpl.class), any(EnablePlan.class))).thenReturn("projectkey-plankey1-JOB1KEY");
        when(requirementsService.performChanges(any(), any(Job.class))).thenReturn(job);
        when(taskService.performTasksChanges(eq(tasksRepresentation), any(Job.class))).thenReturn(job);
        when(job.getStage()).thenReturn(stage);

        jobService.performJobChanges(jobRepresentation, stage);

        verify(requirementsService, times(1)).performChanges(eq(requirement), any(Job.class));
    }

    @Test
    public void performChangesAppliesChangesToArtifacts() throws Exception{
        Map artifactSubscription = ImmutableMap.builder()
                .put("name", "toConsume")
                .put("destination", "./dest")
                .build();

        Map artifactDefinition = ImmutableMap.builder()
                .put("name", "artifactName")
                .put("pattern", "**/*.txt")
                .put("location", "./")
                .put("shared", "false")
                .build();

        Map jobRepresentation = ImmutableMap.builder()
                .put("name", "job1.1Name")
                .put("key", "JOB1KEY")
                .put("description", "job1.1description")
                .put("enabled", "true")
                .put("artifactDefinition", artifactDefinition)
                .put("artifactSubscription", artifactSubscription)
                .build();

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectkey-plankey1");
        ChainStage stage = createdChain.addNewStage("defaultStage", "default", false);
        Job job = mock(DefaultJob.class);
        when(jobCreationService.createSingleJob(any(BuildConfiguration.class), any(ActionParametersMapImpl.class), any(EnablePlan.class))).thenReturn("projectkey-plankey1-JOB1KEY");
        when(requirementsService.performChanges(any(), any(Job.class))).thenReturn(job);
        when(artifactService.performDefinitionChanges(any(), any(Job.class))).thenReturn(job);
        when(artifactService.performSubscriptionChanges(any(), any(Job.class))).thenReturn(job);
        when(miscellaneousJobService.performChanges(anyObject(), any(Job.class))).thenReturn(job);
        when(job.getStage()).thenReturn(stage);

        jobService.performJobChanges(jobRepresentation, stage);

        verify(artifactService, times(1)).performDefinitionChanges(artifactDefinition, job);
        verify(artifactService, times(1)).performSubscriptionChanges(artifactSubscription, job);
    }
    
    
    @Test
    public void performChangesAppliesChangesMiscellaneousJobServices() throws Exception{
        Map patternMatchLabellingData = ImmutableMap.builder()
                .put("regex", "bla*")
                .put("labels", "label1 label2")
                .build();

        Map miscConfigData = ImmutableMap.builder()
                .put("patternMatchLabelling", patternMatchLabellingData )
                .put("cleanupWorkdirAfterBuild", "true" )
                .build();

        Map jobRepresentation = ImmutableMap.builder()
                .put("name", "job1.1Name")
                .put("key", "JOB1KEY")
                .put("description", "job1.1description")
                .put("enabled", "true")
                .put("miscellaneousConfiguration", miscConfigData)
                .build();

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectkey-plankey1");
        ChainStage stage = createdChain.addNewStage("defaultStage", "default", false);
        Job job = mock(DefaultJob.class);
        when(jobCreationService.createSingleJob(any(BuildConfiguration.class), any(ActionParametersMapImpl.class), any(EnablePlan.class))).thenReturn("projectkey-plankey1-JOB1KEY");
        when(artifactService.performSubscriptionChanges(any(Map.class), any(Job.class))).thenReturn(job);
        when(miscellaneousJobService.performChanges(anyObject(), any(Job.class))).thenReturn(job);
        
        when(job.getStage()).thenReturn(stage);

        jobService.performJobChanges(jobRepresentation, stage);

        verify(miscellaneousJobService, times(1)).performChanges(eq(miscConfigData), any(Job.class));
    }

}
