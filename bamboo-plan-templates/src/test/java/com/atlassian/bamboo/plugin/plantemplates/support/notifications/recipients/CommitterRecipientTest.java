package com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients;

import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.CommitterRecipientMapping;
import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class CommitterRecipientTest
{
    @InjectMocks
    private CommitterRecipientMapping committerRecipientMapping;

    @SuppressWarnings("rawtypes")
    @Test
    public void userMappingReturnsCorrectData(){
        Map notification = ImmutableMap.builder()
                .put("recipient", "committers")
                .put("type", "Build Complete")
                .build();

        String recipientKey = committerRecipientMapping.getRecipientKey();
        String recipientData = committerRecipientMapping.getRecipientData(notification);

        Assert.assertEquals("com.atlassian.bamboo.plugin.system.notifications:recipient.committer", recipientKey);
        Assert.assertEquals("", recipientData);
    }
}