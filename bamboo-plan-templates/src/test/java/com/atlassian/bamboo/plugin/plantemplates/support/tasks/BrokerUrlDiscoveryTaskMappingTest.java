package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;
@RunWith(MockitoJUnitRunner.class)
public class BrokerUrlDiscoveryTaskMappingTest
{
    @InjectMocks
    private BrokerUrlDiscoveryTaskMapping brokerUrlDiscoveryTask;
    
    // userDescription:taskHello
    // checkBoxFields:taskDisabled
    // createTaskKey:com.atlassian.bamboo.buildeng.brokertask.brokerurldiscovery:brokerKey
    
    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaskReturnsCorrectMap(){
        
        Map task = ImmutableMap.builder()
            .put("type", "brokerUrlDiscovery")
            .put("description", "testBrokerUrlDiscoverDescription")
            .build();

        Map<String,String> taskActionParams = brokerUrlDiscoveryTask.convert(task);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("userDescription", "testBrokerUrlDiscoverDescription")
            .put("createTaskKey", "com.atlassian.bamboo.buildeng.brokertask.brokerurldiscovery:brokerKey")
            .build(); 
        
        Assert.assertEquals(expected, taskActionParams);
        
    }

    
}
