package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.build.DefaultJob;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MiscellaneousJobServiceTest
{
    
    @InjectMocks
    private MiscellaneousJobService miscellaneousJobService;
    
    @Mock 
    private BuildDefinitionManager buildDefinitionManager;
    @Mock
    private PlanManager planManager;


    @Test
    @SuppressWarnings("rawtypes")
    public void miscConfigurationAttributesSettingCorrectValues() {
       //  miscellaneousConfiguration( cleanupWorkdirAfterBuild: true )

        Map miscConfigData = ImmutableMap.builder()
                .put("cleanupWorkdirAfterBuild", "true" )
                .build();
        
        PlanKey planKey = PlanKeys.getPlanKey("PROJECT-PLAN-JOB");
        Job job = mockJob(planKey);
        BuildDefinition buildDefinition = mockBuildDefinition(planKey);
        
        miscellaneousJobService.performChanges( miscConfigData, job );
        
        verify(buildDefinition).setCleanWorkingDirectory(true);

    }

    @Test
    @SuppressWarnings("rawtypes")
    public void patternMatchLabellingDetectedUnderMiscConfiguration() {
        // job (...) {
        //     miscellaneousConfiguration(...) {
        //          patternMatchLabelling(regex: 'bla*', labels: 'label1 label2' )
        //     }
        // }

        Map patternMatchLabellingData = ImmutableMap.builder()
                .put("regex", "bla*")
                .put("labels", "label1 label2")
                .build();

        Map miscConfigData = ImmutableMap.builder()
                .put("patternMatchLabelling", patternMatchLabellingData )
                .build();

        PlanKey planKey = PlanKeys.getPlanKey("PROJECT-PLAN-JOB");
        Job job = mockJob(planKey);
        BuildDefinition buildDefinition = mockBuildDefinition(planKey);
        Map<String, String> customConfig = buildDefinition.getCustomConfiguration();
        
        miscellaneousJobService.performChanges( miscConfigData, job );

        assertEquals("bla*", customConfig.get("custom.auto.regex") );
        assertEquals("label1 label2", customConfig.get("custom.auto.label") );

    }

    
    @SuppressWarnings("rawtypes")
    @Test(expected = IllegalArgumentException.class)
    public void multiplePatternMatchLabelingPerJobRaisesException()
    {
        Map patternMatchLabellingElement = ImmutableMap.builder()
                .put("regex", "bla*")
                .put("labels", "label1 label2")
                .build();

        List patternMatchLabellingList = ImmutableList.builder()
                .add(patternMatchLabellingElement)
                .add(patternMatchLabellingElement)
                .build();

        Map miscConfigData = ImmutableMap.builder()
                .put("patternMatchLabelling", patternMatchLabellingList )
                .build();

        PlanKey planKey = PlanKeys.getPlanKey("PROJECT-PLAN-JOB");
        Job job = mockJob(planKey);
        mockBuildDefinition(planKey);
        
        miscellaneousJobService.performChanges( miscConfigData, job );

    }
    
    @Test
    public void defaultValuesAreSetWhenDefinitionNotProvided() {

        PlanKey planKey = PlanKeys.getPlanKey("PROJECT-PLAN-JOB");
        Job job = mockJob(planKey);
        BuildDefinition buildDefinition = mockBuildDefinition(planKey);        
        Map<String, String> customConfig = buildDefinition.getCustomConfiguration();
        
        customConfig.put("custom.auto.label", "test label" );    
        customConfig.put("custom.auto.regex", "test regex" );
        
        miscellaneousJobService.performChanges( null, job );
        
        assertEquals(false, customConfig.containsKey("custom.auto.label") );
        assertEquals(false, customConfig.containsKey("custom.auto.regex") );
        verify(buildDefinition).setCleanWorkingDirectory(false);        
    }
    
    
    private Job mockJob(PlanKey planKey)
    {
        Job job = mock(DefaultJob.class);
        when(job.getPlanKey()).thenReturn(planKey);
        return job;
    }

    private BuildDefinition mockBuildDefinition(PlanKey planKey)
    {
        Plan chain = mock(Plan.class);
        when(planManager.getPlanByKey(planKey)).thenReturn(chain);

        BuildDefinition buildDefinition = mock(BuildDefinition.class, Mockito.RETURNS_DEEP_STUBS);        
        when(chain.getBuildDefinition()).thenReturn(buildDefinition);
        Map<String, String> customConfig = Maps.newHashMap();
        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfig);
        return buildDefinition;
    }

    
    

    
}
