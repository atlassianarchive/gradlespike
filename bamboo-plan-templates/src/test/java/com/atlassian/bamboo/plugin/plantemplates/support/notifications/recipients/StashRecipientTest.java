package com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients;

import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.StashRecipientMapping;
import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class StashRecipientTest
{
    @InjectMocks
    private StashRecipientMapping stashRecipientMapping;

    @SuppressWarnings("rawtypes")
    @Test
    public void stashMappingReturnsCorrectData(){
        Map notification = ImmutableMap.builder()
                .put("recipient", "stash")
                .put("type", "Build Complete")
                .build();

        String recipientKey = stashRecipientMapping.getRecipientKey();
        String recipientData = stashRecipientMapping.getRecipientData(notification);

        Assert.assertEquals("com.atlassian.bamboo.plugins.bamboo-stash-plugin:recipient.stash", recipientKey);
        Assert.assertEquals("", recipientData);
    }
}