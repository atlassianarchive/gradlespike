package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_INSTALL_ONDEMAND;

@RunWith(MockitoJUnitRunner.class)
public class InstallOnDemandTaskMappingTest
{
    @InjectMocks
    private InstallOnDemandTaskMapping installOnDemandTaskMapping;

    @Test
    @SuppressWarnings("rawtypes")
    public void convertAInstallOnDemandTaskWithMinimalParamsReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_INSTALL_ONDEMAND.getTaskKey())
                .put("description", "task description")
                .build();

        Map<String,String> taskActionParams = installOnDemandTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_INSTALL_ONDEMAND.createTaskMappingKey())
                .put("cleanBeforeInstall", "false")
                .put("icebatVersion", "latest")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }

    @Test
    @SuppressWarnings("rawtypes")
    public void convertAInstallOnDemandTaskReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_INSTALL_ONDEMAND.getTaskKey())
                .put("description", "task description")
                .put("unicornInstance", "fake-instance.jira-dev.com")
                .put("version", "random-icebat.dev.12")
                .put("cleanBeforeInstall", "true")
                .put("localYaml", "fakescript")
                .build();

        Map<String,String> taskActionParams = installOnDemandTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("unicornInstance", "fake-instance.jira-dev.com")
                .put("icebatVersion", "random-icebat.dev.12")
                .put("cleanBeforeInstall", "true")
                .put("localYaml", "fakescript")
                .put("createTaskKey", BAMBOONICORN_INSTALL_ONDEMAND.createTaskMappingKey())
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }
}
