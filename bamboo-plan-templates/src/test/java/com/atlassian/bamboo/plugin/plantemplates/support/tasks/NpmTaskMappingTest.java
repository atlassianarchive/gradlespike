package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.junit.Assert;


@RunWith(MockitoJUnitRunner.class)
public class NpmTaskMappingTest {
    

    @InjectMocks
    private NpmTaskMapping npmTaskMapping;

    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaskReturnsCorrectMap(){
        
    //  userDescription:npm test
    //  checkBoxFields:taskDisabled
    //  runtime:Node 0.8.23
    //  command:test npm
    //  workingSubDirectory:./test
    //  createTaskKey:com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.builder.npm
    // FIXME do we need this property???
    //  planKey:PERFAGENTSTEST-PERFTESTARTISUBS1-AS0 
      
        
        Map task = ImmutableMap.builder()
                .put("type", "npm")
                .put("description", "new npm task")
                .put("executable", "Node 0.8.23")
                .put("command", "install something")
                .put("workingSubDirectory", "./newSub")
                .build();

        Map<String,String> taskActionParams = npmTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("userDescription", "new npm task")
                .put("taskDisabled", "false")
                .put("command", "install something")
                .put("runtime","Node 0.8.23")
                .put("workingSubDirectory", "./newSub")
                .put("createTaskKey", "com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.builder.npm")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }



}


    
    

