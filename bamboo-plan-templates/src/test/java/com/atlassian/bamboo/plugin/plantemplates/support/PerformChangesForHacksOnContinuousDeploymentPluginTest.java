package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentTaskService;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.ArtifactDownloadTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.DeployBambooPluginTaskMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.tasks.DeployConfluencePluginTaskMapping;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskManager;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings({"unchecked"})
public class PerformChangesForHacksOnContinuousDeploymentPluginTest
{
    @InjectMocks
    private PerformChangesForHacksOnContinuousDeploymentPlugin changesForHacksOnContinuousDeploymentPlugin;
    
    @Mock
    private EnvironmentTaskService environmentTaskService;
    
    @Mock
    private TaskManager taskManager;
    
    // For deploys the "confDeployJar" is dodgy and we have to do hack 
    // confDeployJar = v2:142966847:2:0:plugin
    // confDeployJar = v2:{artifactId}:{artifactDownloaderTaskId}:{artifactDownloaderTransferId}:{artifactName}
        
    @Test
    public void performChangesCallEditTaskWithFixedArtifactDetails()    
    {
        
        Map<String, String> downloadTaskMap = ImmutableMap.<String, String>builder()
            .put("userDescription", "artifactDownload description")
            .put("taskDisabled", "false")
            .put("artifactId_0", "12345")
            .put("localPath_0", "./test/artifact/")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.bamboo-artifact-downloader-plugin:artifactdownloadertask")
            .put("sourcePlanKey", "test-test")
            .build(); 
        
        TaskDefinition downloadTask = mock(TaskDefinition.class);
        when(downloadTask.getId()).thenReturn(4L);
        when(downloadTask.getPluginKey()).thenReturn(ArtifactDownloadTaskMapping.ARTIFACT_DOWNLOADER_KEY);
        when(downloadTask.getConfiguration()).thenReturn(downloadTaskMap);
                
        
        Map<String, String> deployTaskMap = ImmutableMap.<String, String>builder()
            .put("userDescription", "testDeployBambooPlugin")
            .put("taskDisabled", "false")
            .put("confDeployUsername", "usernameValue")
            .put("confDeployURL", "http://mybamboo:1990/bamboo")
            .put("confDeployJar", "artifactName")
            .put("confDeployPasswordVariable", "${bamboo.password}")
            .put("confDeployPasswordVariableCheck", "true")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.deploy.continuous-plugin-deployment:bamboodeploy")
            .build();
        
        Map<String, String> deployTaskMapExpected = Maps.newHashMap(deployTaskMap);
        
        TaskDefinition deployTask = mock(TaskDefinition.class);
        when(deployTask.getId()).thenReturn(6L);
        when(deployTask.getPluginKey()).thenReturn(DeployBambooPluginTaskMapping.DEPLOY_BAMBOO_PLUGIN_TASK_PLUGIN_KEY);
        when(deployTask.getConfiguration()).thenReturn(deployTaskMap);
        when(deployTask.isEnabled()).thenReturn(true);
        when(deployTask.getUserDescription()).thenReturn("testDeployBambooPlugin");
        
        List<TaskDefinition> tasks = Lists.newArrayList(downloadTask, deployTask);
        
        Environment environment = mock(Environment.class);
        when(environment.getId()).thenReturn(8L);
        when(environment.getName()).thenReturn("environmentNameExisting");
        when(environment.getTaskDefinitions()).thenReturn(tasks);
                
        changesForHacksOnContinuousDeploymentPlugin.perform(environment, tasks);

        // confDeployJar = v2:12345:4:0:artifactName
        // confDeployJar = v2:{artifactId}:{artifactDownloaderTaskId}:{artifactDownloaderTransferId}:{artifactName}      
        deployTaskMapExpected.put("confDeployJar", "v2:12345:4:0:artifactName");
        
        verify(environmentTaskService, times(1)).editTask(environment.getId(), deployTask.getId(), deployTask.getUserDescription(), true, deployTaskMapExpected);
       
    }
    
    
    
    @Test
    public void performChangesReApplyTheHack()    
    {
        
        Map<String, String> downloadTaskMap = ImmutableMap.<String, String>builder()
            .put("userDescription", "artifactDownload description")
            .put("taskDisabled", "false")
            .put("artifactId_0", "12345")
            .put("localPath_0", "./test/artifact/")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.bamboo-artifact-downloader-plugin:artifactdownloadertask")
            .put("sourcePlanKey", "test-test")
            .build(); 
        
        TaskDefinition downloadTask = mock(TaskDefinition.class);
        when(downloadTask.getId()).thenReturn(4L);
        when(downloadTask.getPluginKey()).thenReturn(ArtifactDownloadTaskMapping.ARTIFACT_DOWNLOADER_KEY);
        when(downloadTask.getConfiguration()).thenReturn(downloadTaskMap);
                
        
        Map<String, String> deployTaskMap = ImmutableMap.<String, String>builder()
            .put("userDescription", "testDeployBambooPlugin")
            .put("taskDisabled", "false")
            .put("confDeployUsername", "usernameValue")
            .put("confDeployURL", "http://mybamboo:1990/bamboo")
            .put("confDeployJar", "v2:88888:4:0:artifactName")
            .put("confDeployPasswordVariable", "${bamboo.password}")
            .put("confDeployPasswordVariableCheck", "true")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.deploy.continuous-plugin-deployment:bamboodeploy")
            .build();
        
        Map<String, String> deployTaskMapExpected = Maps.newHashMap(deployTaskMap);
        
        TaskDefinition deployTask = mock(TaskDefinition.class);
        when(deployTask.getId()).thenReturn(6L);
        when(deployTask.getPluginKey()).thenReturn(DeployBambooPluginTaskMapping.DEPLOY_BAMBOO_PLUGIN_TASK_PLUGIN_KEY);
        when(deployTask.getConfiguration()).thenReturn(deployTaskMap);
        when(deployTask.isEnabled()).thenReturn(true);
        when(deployTask.getUserDescription()).thenReturn("testDeployBambooPlugin");
        
        List<TaskDefinition> tasks = Lists.newArrayList(downloadTask, deployTask);
        
        Environment environment = mock(Environment.class);
        when(environment.getId()).thenReturn(8L);
        when(environment.getName()).thenReturn("environmentNameExisting");
        when(environment.getTaskDefinitions()).thenReturn(tasks);
                
        changesForHacksOnContinuousDeploymentPlugin.perform(environment, tasks);

        // confDeployJar = v2:12345:4:0:artifactName
        // confDeployJar = v2:{artifactId}:{artifactDownloaderTaskId}:{artifactDownloaderTransferId}:{artifactName}      
        deployTaskMapExpected.put("confDeployJar", "v2:12345:4:0:artifactName");
        
        verify(environmentTaskService, times(1)).editTask(environment.getId(), deployTask.getId(), deployTask.getUserDescription(), true, deployTaskMapExpected);
       
    }
    
    
   
    
    @Test
    public void performChangesNotCalledIfNoDownloadTaskPresent()
    {
        TaskDefinition deployTask = mock(TaskDefinition.class);
        when(deployTask.getId()).thenReturn(6L);
        when(deployTask.getPluginKey()).thenReturn(DeployBambooPluginTaskMapping.DEPLOY_BAMBOO_PLUGIN_TASK_PLUGIN_KEY);
        when(deployTask.isEnabled()).thenReturn(true);
        when(deployTask.getUserDescription()).thenReturn("testDeployBambooPlugin");
        
        
        List<TaskDefinition> tasks = Lists.newArrayList(deployTask);
        
        Environment environment = mock(Environment.class);
        when(environment.getId()).thenReturn(8L);
        when(environment.getName()).thenReturn("environmentNameExisting");
        when(environment.getTaskDefinitions()).thenReturn(tasks);
        
        changesForHacksOnContinuousDeploymentPlugin.perform(environment, tasks);
        
        verify(environmentTaskService, times(0)).editTask(anyLong(), anyLong(), anyString(), anyBoolean(), anyMap());
    }
    
    
    @Test
    public void performChangesNotCalledIfNoDeployTaskIsPresent()
    {
        TaskDefinition downloadTask = mock(TaskDefinition.class);
        when(downloadTask.getId()).thenReturn(4L);
        when(downloadTask.getPluginKey()).thenReturn(ArtifactDownloadTaskMapping.ARTIFACT_DOWNLOADER_KEY);
                
        List<TaskDefinition> tasks = Lists.newArrayList(downloadTask);
        
        Environment environment = mock(Environment.class);
        when(environment.getId()).thenReturn(8L);
        when(environment.getName()).thenReturn("environmentNameExisting");
        when(environment.getTaskDefinitions()).thenReturn(tasks);
        
        changesForHacksOnContinuousDeploymentPlugin.perform(environment, tasks);
        
        verify(environmentTaskService, times(0)).editTask(anyLong(), anyLong(), anyString(), anyBoolean(), anyMap());
    }
    
    @Test
    public void performChangesCalledOnAnyDeployTask()
    {
        
        Map<String, String> downloadTaskMap = ImmutableMap.<String, String>builder()
            .put("userDescription", "artifactDownload description")
            .put("taskDisabled", "false")
            .put("artifactId_0", "12345")
            .put("localPath_0", "./test/artifact/")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.bamboo-artifact-downloader-plugin:artifactdownloadertask")
            .put("sourcePlanKey", "test-test")
            .build(); 
        
        TaskDefinition downloadTask = mock(TaskDefinition.class);
        when(downloadTask.getId()).thenReturn(4L);
        when(downloadTask.getPluginKey()).thenReturn(ArtifactDownloadTaskMapping.ARTIFACT_DOWNLOADER_KEY);
        when(downloadTask.getConfiguration()).thenReturn(downloadTaskMap);
                
        
        Map<String, String> deployTaskMap = ImmutableMap.<String, String>builder()
            .put("userDescription", "testDeployBambooPlugin")
            .put("taskDisabled", "false")
            .put("confDeployUsername", "usernameValue")
            .put("confDeployURL", "http://mybamboo:1990/bamboo")
            .put("confDeployJar", "artifactName")
            .put("confDeployPasswordVariable", "${bamboo.password}")
            .put("confDeployPasswordVariableCheck", "true")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.deploy.continuous-plugin-deployment:bamboodeploy")
            .build();
        
        TaskDefinition deployTask = mock(TaskDefinition.class);
        when(deployTask.getId()).thenReturn(6L);
        when(deployTask.getPluginKey()).thenReturn(DeployConfluencePluginTaskMapping.DEPLOY_CONF_PLUGIN_TASK_PLUGIN_KEY);
        when(deployTask.isEnabled()).thenReturn(true);
        when(deployTask.getUserDescription()).thenReturn("testConfluencePlugin");
        when(deployTask.getConfiguration()).thenReturn(deployTaskMap);
                
        List<TaskDefinition> tasks = Lists.newArrayList(downloadTask, deployTask);
        
        Environment environment = mock(Environment.class);
        when(environment.getId()).thenReturn(8L);
        when(environment.getName()).thenReturn("environmentNameExisting");
        when(environment.getTaskDefinitions()).thenReturn(tasks);
        
        changesForHacksOnContinuousDeploymentPlugin.perform(environment, tasks);
        
        verify(environmentTaskService, times(1)).editTask(anyLong(), anyLong(), anyString(), anyBoolean(), anyMap());
    }
    
}
