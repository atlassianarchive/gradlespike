package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import com.atlassian.bamboo.plugin.plantemplates.support.tasks.deployTask.Crypto;
import com.google.common.collect.ImmutableMap;
import com.opensymphony.xwork2.interceptor.annotations.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DeployJiraPluginTaskMappingTest
{

    @Mock
    private Crypto crypto;

    @InjectMocks
    private DeployJiraPluginTaskMapping jiraPluginTaskMapping;

    @Test
    public void convertATaskWithNormalValues()
    {
        when(crypto.encrypt("passValue")).thenReturn(ImmutableMap.of("key", "cryptKey", "encrypted", "cryptPass"));
        Map map = ImmutableMap.<String, String>builder()
                    .put("type", "deployJiraPlugin")
                    .put("description", "DeployJiraPluginDesc")
                    .put("url", "http://atlassian.com")
                    .put("username", "testUser")
                    .put("password", "passValue")
                    .put("artifact", "artifactName")
                    .build();
        Map result = jiraPluginTaskMapping.convert(map);

        Map expected = ImmutableMap.builder()
                        .put("taskDisabled", "false")
                        .put("userDescription", "DeployJiraPluginDesc")
                        .put("confDeployUsername", "testUser")
                        .put("confDeployPassword", "cryptPass")
                        .put("confDeployKey", "cryptKey")
                        .put("confDeployURL", "http://atlassian.com")
                        .put("confDeployJar", "artifactName")
                        .put("pluginInstallationTimeout", "90")
                        .put("createTaskKey", DeployJiraPluginTaskMapping.DEPLOY_JIRA_PLUGIN_TASK_PLUGIN_KEY)
                        .build();
        assertEquals(expected, result);
    }

    @Test
    public void convertAtaskWithAtlassianId()
    {
        when(crypto.encrypt("passValue")).thenReturn(ImmutableMap.of("key", "cryptKey", "encrypted", "cryptPass"));
        Map map = ImmutableMap.<String, String>builder()
                    .put("type", "deployJiraPlugin")
                    .put("description", "DeployJiraPluginDesc")
                    .put("url", "http://atlassian.com")
                    .put("username", "testUser")
                    .put("password", "passValue")
                    .put("artifact", "artifactName")
                    .put("useAtlassianId", "true")
                    .put("atlassianIdUsername", "atlasUser")
                    .put("atlassianIdPassword", "passValue")
                    .build();
        Map result = jiraPluginTaskMapping.convert(map);

        Map expected = ImmutableMap.<String, String>builder()
                        .put("taskDisabled", "false")
                        .put("userDescription", "DeployJiraPluginDesc")
                        .put("confDeployUsername", "testUser")
                        .put("confDeployPassword", "cryptPass")
                        .put("confDeployKey", "cryptKey")
                        .put("confDeployURL", "http://atlassian.com")
                        .put("confDeployJar", "artifactName")
                        .put("pluginInstallationTimeout", "90")
                        .put("useAtlassianId", "true")
                        .put("atlassianIdUsername", "atlasUser")
                        .put("atlassianIdPassword", "cryptPass")
                        .put("atlassianIdKey", "cryptKey")
                        .put("useAtlassianIdWebSudo", "false")
                        .put("createTaskKey", DeployJiraPluginTaskMapping.DEPLOY_JIRA_PLUGIN_TASK_PLUGIN_KEY)
                        .build();
        assertEquals(expected, result);
    }

    @Test
    public void convertAtaskWithAtlassianIdAndPasswordVariable()
    {
        when(crypto.encrypt("passValue")).thenReturn(ImmutableMap.of("key", "cryptKey", "encrypted", "cryptPass"));
        Map map = ImmutableMap.<String, String>builder()
                .put("type", "deployJiraPlugin")
                .put("description", "DeployJiraPluginDesc")
                .put("url", "http://atlassian.com")
                .put("username", "testUser")
                .put("password", "passValue")
                .put("artifact", "artifactName")
                .put("useAtlassianId", "true")
                .put("atlassianIdUsername", "atlasUser")
                .put("atlassianIdPasswordVariable", "${bamboo.atlassian.password}")
                .build();
        Map result = jiraPluginTaskMapping.convert(map);

        Map expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "DeployJiraPluginDesc")
                .put("confDeployUsername", "testUser")
                .put("confDeployPassword", "cryptPass")
                .put("confDeployKey", "cryptKey")
                .put("confDeployURL", "http://atlassian.com")
                .put("confDeployJar", "artifactName")
                .put("pluginInstallationTimeout", "90")
                .put("useAtlassianId", "true")
                .put("atlassianIdUsername", "atlasUser")
                .put("atlassianIdPasswordVariable", "${bamboo.atlassian.password}")
                .put("atlassianIdPasswordVariableCheck", "true")
                .put("useAtlassianIdWebSudo", "false")
                .put("createTaskKey", DeployJiraPluginTaskMapping.DEPLOY_JIRA_PLUGIN_TASK_PLUGIN_KEY)
                .build();
        assertEquals(expected, result);
    }

    @Test
    public void convertAtaskWithExtraAtlassianIdAttrs()
    {
        when(crypto.encrypt("passValue")).thenReturn(ImmutableMap.of("key", "cryptKey", "encrypted", "cryptPass"));
        Map map = ImmutableMap.<String, String>builder()
                .put("type", "deployJiraPlugin")
                .put("description", "DeployJiraPluginDesc")
                .put("url", "http://atlassian.com")
                .put("username", "testUser")
                .put("password", "passValue")
                .put("artifact", "artifactName")
                .put("useAtlassianId", "true")
                .put("atlassianIdUsername", "atlasUser")
                .put("atlassianIdPasswordVariable", "${bamboo.atlassian.password}")
                .put("useAtlassianIdWebSudo", "true")
                .put("atlassianIdAppName", "myapp")
                .build();
        Map result = jiraPluginTaskMapping.convert(map);

        Map expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "DeployJiraPluginDesc")
                .put("confDeployUsername", "testUser")
                .put("confDeployPassword", "cryptPass")
                .put("confDeployKey", "cryptKey")
                .put("confDeployURL", "http://atlassian.com")
                .put("confDeployJar", "artifactName")
                .put("pluginInstallationTimeout", "90")
                .put("useAtlassianId", "true")
                .put("atlassianIdUsername", "atlasUser")
                .put("atlassianIdPasswordVariable", "${bamboo.atlassian.password}")
                .put("atlassianIdPasswordVariableCheck", "true")
                .put("atlassianIdAppName", "myapp")
                .put("useAtlassianIdWebSudo", "true")
                .put("createTaskKey", DeployJiraPluginTaskMapping.DEPLOY_JIRA_PLUGIN_TASK_PLUGIN_KEY)
                .build();
        assertEquals(expected, result);
    }
}
