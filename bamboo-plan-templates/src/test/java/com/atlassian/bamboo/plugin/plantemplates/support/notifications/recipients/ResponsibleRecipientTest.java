package com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients;

import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.ResponsibleRecipientMapping;
import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class ResponsibleRecipientTest
{
    @InjectMocks
    private ResponsibleRecipientMapping responsibleRecipientMapping;

    @Test
    @SuppressWarnings("rawtypes")
    public void userMappingReturnsCorrectData(){
        Map notification = ImmutableMap.builder()
                .put("recipient", "responsible")
                .put("type", "Build Complete")
                .build();

        String recipientKey = responsibleRecipientMapping.getRecipientKey();
        String recipientData = responsibleRecipientMapping.getRecipientData(notification);

        Assert.assertEquals("com.atlassian.bamboo.brokenbuildtracker:recipient.responsible", recipientKey);
        Assert.assertEquals("", recipientData);
    }
}