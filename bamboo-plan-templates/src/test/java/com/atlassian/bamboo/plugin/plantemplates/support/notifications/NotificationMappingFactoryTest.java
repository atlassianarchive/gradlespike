package com.atlassian.bamboo.plugin.plantemplates.support.notifications;

import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.AfterXFailuresNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.BuildCompleteNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.ChangeOfJobStatusNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.ChangeOfStatusNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.CommentAddedNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.DeploymentFinishedMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.DeploymentStartedAndFinishedMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.FailedJobNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.FailedNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.FirstFailedJobForPlanNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.JobCompleteNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.JobErrorNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.JobHungNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.JobQueueTimeoutNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.NoAgentNotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.NotificationMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.ResponsibilitiesChangedMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.conditions.ResponsibleRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.CommitterRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.EmailRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.GroupRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.HipChatRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.HttpNotificationsRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.IMRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.StashRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.UserRecipientMapping;
import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.WatcherRecipientMapping;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class NotificationMappingFactoryTest
{
    @InjectMocks
    private NotificationMappingFactory notificationMappingFactory;

    @Test
    public void notificationMappingFactoryReturnBuildCompleteMapping(){
        NotificationMapping notificationMapping = notificationMappingFactory.create("all builds completed", "email");
        Assert.assertTrue(notificationMapping instanceof BuildCompleteNotificationMapping);
    }

    @Test
    public void notificationMappingFactoryWorksCaseInsensitive(){
        NotificationMapping notificationMapping = notificationMappingFactory.create("AlL bUiLdS cOmPlEtEd", "email");
        Assert.assertTrue(notificationMapping instanceof BuildCompleteNotificationMapping);
    }

    @Test
    public void notificationMappingFactoryReturnResponsibilitiesChangedMapping(){
        NotificationMapping notificationMapping = notificationMappingFactory.create("change of responsibilities", "email");
        Assert.assertTrue(notificationMapping instanceof ResponsibilitiesChangedMapping);
    }

    @Test
    public void notificationMappingFactoryReturnCommentAddedMapping(){
        NotificationMapping notificationMapping = notificationMappingFactory.create("comment added", "email");
        Assert.assertTrue(notificationMapping instanceof CommentAddedNotificationMapping);
    }

    @Test
    public void notificationMappingFactoryReturnChangeOfStatusMapping(){
        NotificationMapping notificationMapping = notificationMappingFactory.create("change of build status", "email");
        Assert.assertTrue(notificationMapping instanceof ChangeOfStatusNotificationMapping);
    }

    @Test
    public void notificationMappingFactoryReturnFailedThenSuccessfulMapping(){
        NotificationMapping notificationMapping = notificationMappingFactory.create("failed builds and first successful", "email");
        Assert.assertTrue(notificationMapping instanceof FailedNotificationMapping);
    }

    @Test
    public void notificationMappingFactoryReturnAfterXFailuresMapping(){
        NotificationMapping notificationMapping = notificationMappingFactory.create("after x build failures", "email");
        Assert.assertTrue(notificationMapping instanceof AfterXFailuresNotificationMapping);
    }

    @Test
    public void notificationMappingFactoryReturnJobCompleteMapping(){
        NotificationMapping notificationMapping = notificationMappingFactory.create("all jobs completed", "email");
        Assert.assertTrue(notificationMapping instanceof JobCompleteNotificationMapping);
    }

    @Test
    public void notificationMappingFactoryReturnChangeOfJobStatusMapping(){
        NotificationMapping notificationMapping = notificationMappingFactory.create("change of job status", "email");
        Assert.assertTrue(notificationMapping instanceof ChangeOfJobStatusNotificationMapping);
    }

    @Test
    public void notificationMappingFactoryReturnFailedJobMapping(){
        NotificationMapping notificationMapping = notificationMappingFactory.create("failed jobs and first successful", "email");
        Assert.assertTrue(notificationMapping instanceof FailedJobNotificationMapping);
    }

    @Test
    public void notificationMappingFactoryReturnFirstFailedJobForPlanMapping(){
        NotificationMapping notificationMapping = notificationMappingFactory.create("first failed job for plan", "email");
        Assert.assertTrue(notificationMapping instanceof FirstFailedJobForPlanNotificationMapping);
    }

    @Test
    public void notificationMappingFactoryReturnJobErrorMapping(){
        NotificationMapping notificationMapping = notificationMappingFactory.create("job error", "email");
        Assert.assertTrue(notificationMapping instanceof JobErrorNotificationMapping);
    }

    @Test
    public void notificationMappingFactoryReturnJobHungMapping(){
        NotificationMapping notificationMapping = notificationMappingFactory.create("job hung", "email");
        Assert.assertTrue(notificationMapping instanceof JobHungNotificationMapping);
    }

    @Test
    public void notificationMappingFactoryReturnJobQueueTimeoutMapping(){
        NotificationMapping notificationMapping = notificationMappingFactory.create("job queue timeout", "email");
        Assert.assertTrue(notificationMapping instanceof JobQueueTimeoutNotificationMapping);
    }
    
    @Test
    public void notificationMappingFactoryDeploymentStartedAndFinishedMapping(){
        NotificationMapping notificationMapping = notificationMappingFactory.create("Deployment Started and Finished", "email");
        Assert.assertTrue(notificationMapping instanceof DeploymentStartedAndFinishedMapping);
    }
    
    @Test
    public void notificationMappingFactoryDeploymentFinishedMapping(){
        NotificationMapping notificationMapping = notificationMappingFactory.create("Deployment Finished", "email");
        Assert.assertTrue(notificationMapping instanceof DeploymentFinishedMapping);
    }


    @Test
    public void notificationMappingFactoryReturnNoAgentMapping(){
        NotificationMapping notificationMapping = notificationMappingFactory.create("job queued without capable agents", "email");
        Assert.assertTrue(notificationMapping instanceof NoAgentNotificationMapping);
    }

    @Test
    public void notificationMappingFactoryHandlesEmailRecipient(){
        Assert.assertTrue(notificationMappingFactory.getRecipientMapping("email") instanceof EmailRecipientMapping);
    }

    @Test
    public void notificationMappingFactoryHandlesUserRecipient(){
        Assert.assertTrue(notificationMappingFactory.getRecipientMapping("user") instanceof UserRecipientMapping);
    }

    @Test
    public void notificationMappingFactoryHandlesGroupRecipient(){
        Assert.assertTrue(notificationMappingFactory.getRecipientMapping("group") instanceof GroupRecipientMapping);
    }

    @Test
    public void notificationMappingFactoryHandlesResponsibleRecipient(){
        Assert.assertTrue(notificationMappingFactory.getRecipientMapping("responsible") instanceof ResponsibleRecipientMapping);
    }

    @Test
    public void notificationMappingFactoryHandlesCommitersRecipient(){
        Assert.assertTrue(notificationMappingFactory.getRecipientMapping("committers") instanceof CommitterRecipientMapping);
    }

    @Test
    public void notificationMappingFactoryHandlesWatchersRecipient(){
        Assert.assertTrue(notificationMappingFactory.getRecipientMapping("watchers") instanceof WatcherRecipientMapping);
    }

    @Test
    public void notificationMappingFactoryHandlesIMRecipient(){
        Assert.assertTrue(notificationMappingFactory.getRecipientMapping("IM") instanceof IMRecipientMapping);
    }

    @Test
    public void notificationMappingFactoryHandlesHipChatRecipient(){
        Assert.assertTrue(notificationMappingFactory.getRecipientMapping("hipchat") instanceof HipChatRecipientMapping);
    }

    @Test
    public void notificationMappingFactoryHandlesStashRecipient(){
        Assert.assertTrue(notificationMappingFactory.getRecipientMapping("stash") instanceof StashRecipientMapping);
    }

    @Test
    public void notificationMappingFactoryHandlesHttpNotificationRecipient(){
        Assert.assertTrue(notificationMappingFactory.getRecipientMapping("httpNotification") instanceof HttpNotificationsRecipientMapping);
    }
}
