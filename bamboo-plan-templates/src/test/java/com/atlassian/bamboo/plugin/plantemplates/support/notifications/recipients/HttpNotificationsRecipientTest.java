package com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("rawtypes")
public class HttpNotificationsRecipientTest
{
    @InjectMocks
    private HttpNotificationsRecipientMapping httpNotificationsRecipientMapping;

    @Test
    public void httpNotificationsMappingReturnsCorrectData(){
        Map notification = ImmutableMap.builder()
                .put("recipient", "httpNotification")
                .put("type", "Build Complete")
                .put("baseUrl", "http://mydomain.com:3000")
                .build();
        
        String recipientKey = httpNotificationsRecipientMapping.getRecipientKey();
        String recipientData = httpNotificationsRecipientMapping.getRecipientData(notification);

        Assert.assertEquals(recipientKey, "com.atlassian.bamboo.plugins.bamboo-http-notifications:recipient.http.notification");
        Assert.assertEquals(recipientData, "http://mydomain.com:3000");
    }
}
