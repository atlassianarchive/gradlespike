package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.deployments.environments.ConfigurationState;
import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentDeletionService;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentService;
import com.atlassian.bamboo.deployments.projects.DeploymentProject;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.*;

@SuppressWarnings("rawtypes")
@RunWith(MockitoJUnitRunner.class)
public class EnvironmentDeploymentServiceTest
{
   @InjectMocks
   private EnvironmentDeploymentService environmentDeploymentService;
   
   @Mock
   private EnvironmentService environmentService;
   @Mock
   private EnvironmentDeletionService environmentDeletionService;
   @Mock
   private TaskDeploymentService taskDeploymentService;
   @Mock
   private TriggerDeploymentService triggerDeploymentService;
   @Mock
   private VariableDeploymentService variableDeploymentService;
   @Mock   
   private NotificationDeploymentService notificationDeploymentService;
    
   @Test
   public void runTemplatePerformsACreationOfEnvironmentsForADeployment() throws Exception
   {
       
       Map environmentRepresentation = ImmutableMap.builder()
             .put("name", "environmentName")
             .put("description", "environmentDescription")
             .build();
   
          
       DeploymentProject deployment = mock(DeploymentProject.class);
       when(deployment.getName()).thenReturn("deploymentName");
       when(deployment.getId()).thenReturn(5L);
       
       Environment environment = mock(Environment.class);
       when(environment.getId()).thenReturn(13L);
       
       when(environmentService.addEnvironment(5L,"environmentName", "environmentDescription")).thenReturn(environment);
           
       environmentDeploymentService.performChanges(environmentRepresentation, deployment);
        
       verify(environmentService, times(1)).addEnvironment(5L,"environmentName", "environmentDescription");
   }
   
   @Test
   public void runTemplatePerformsACreationOfMultipleDeployments() throws Exception
   {
        Map environmentMap = ImmutableMap.builder()
            .put("name", "environmentName1")
            .put("description", "environmentDescription1")
            .build();

        Map environmentMap2 = ImmutableMap.builder()
            .put("name", "environmentName2")
            .put("description", "environmentDescription2")
            .build();

        List environmentRepresentation = Lists.newArrayList(environmentMap, environmentMap2);
          
        DeploymentProject deployment = mock(DeploymentProject.class);
        when(deployment.getName()).thenReturn("deploymentName");
        when(deployment.getId()).thenReturn(5L);
        
        Environment environment = mock(Environment.class);
        when(environment.getId()).thenReturn(13L);
        Environment environment2 = mock(Environment.class);
        when(environment2.getId()).thenReturn(14L);
        
        when(environmentService.addEnvironment(5L,"environmentName1", "environmentDescription1")).thenReturn(environment);
        when(environmentService.addEnvironment(5L,"environmentName2", "environmentDescription2")).thenReturn(environment2);
           
        environmentDeploymentService.performChanges(environmentRepresentation, deployment);
        
        verify(environmentService, times(1)).addEnvironment(5L,"environmentName1", "environmentDescription1");
        verify(environmentService, times(1)).addEnvironment(5L,"environmentName2", "environmentDescription2");
   }
   
   @Test
   public void runTemplatePerformsAnUpdateOfEnvironmentsForADeployment() throws Exception
   {
       
       Map environmentRepresentation = ImmutableMap.builder()
             .put("name", "environmentName")
             .put("description", "environmentDescription")
             .build();
   
          
       DeploymentProject deployment = mock(DeploymentProject.class);
       when(deployment.getName()).thenReturn("deploymentName");
       when(deployment.getId()).thenReturn(5L);
       
       Environment environment = mock(Environment.class);
       when(environment.getId()).thenReturn(13L);
       when(environment.getName()).thenReturn("environmentName");
       
       when(environmentService.editEnvironment(13L,"environmentName", "environmentDescription")).thenReturn(environment);
       
       when(environmentService.getEnvironmentsForDeploymentProject(5L)).thenReturn(Lists.newArrayList(environment));
           
       environmentDeploymentService.performChanges(environmentRepresentation, deployment);
        
       verify(environmentService, times(1)).editEnvironment(13L, "environmentName", "environmentDescription");
   }
   
   
   @Test
   public void runTemplatePerformsADeletionOfEnvironments() throws Exception
   {
        Map environmentMap = ImmutableMap.builder()
            .put("name", "environmentName1")
            .put("description", "environmentDescription1")
            .build();

       List environmentRepresentation = Lists.newArrayList(environmentMap);
          
        DeploymentProject deployment = mock(DeploymentProject.class);        
        when(deployment.getName()).thenReturn("deploymentName");
        when(deployment.getId()).thenReturn(5L);
        
        Environment existingEnvironment = mock(Environment.class);
        when(existingEnvironment.getId()).thenReturn(8L);
        when(existingEnvironment.getName()).thenReturn("environmentNameExisting");
        
        Environment newEnvironment = mock(Environment.class);
        when(newEnvironment.getId()).thenReturn(19L);
        
        when(environmentService.addEnvironment(5L,"environmentName1", "environmentDescription1")).thenReturn(newEnvironment);
        when(environmentService.getEnvironmentsForDeploymentProject(5L)).thenReturn(Lists.newArrayList(existingEnvironment));

        environmentDeploymentService.performChanges(environmentRepresentation, deployment);
        
        verify(environmentDeletionService, times(1)).delete(8L);
        verify(environmentService, times(1)).addEnvironment(5L,"environmentName1", "environmentDescription1");
   }
   
   
   @Test
   public void runTemplatePerformsChangesOnTasks() throws Exception
   {
       
       Map tasksRepresentation1 = ImmutableMap.builder()
           .put("type", "script_task")
           .put("description", "task1Script")
           .put("script_body", "echo hellooooo")
           .build();
       
       Map tasksRepresentation2 = ImmutableMap.builder()
           .put("type", "script_task")
           .put("description", "task2Script")
           .put("script_body", "echo hellooooo")
           .build();
       
       
       
       Map environmentRepresentation1 = ImmutableMap.builder()
             .put("name", "environmentName1")
             .put("description", "environmentDescription1")
             .put("task", tasksRepresentation1)
             .build();
    
       
       Map environmentRepresentation2 = ImmutableMap.builder()
           .put("name", "environmentName2")
           .put("description", "environmentDescription2")
           .put("task", tasksRepresentation2)
           .build();
   
       List environmentRepresentation = Lists.newArrayList(environmentRepresentation1, environmentRepresentation2);
          
       DeploymentProject deployment = mock(DeploymentProject.class);
       when(deployment.getName()).thenReturn("deploymentName");
       when(deployment.getId()).thenReturn(5L);
       
       Environment environment1 = mock(Environment.class);
       when(environment1.getId()).thenReturn(13L);
       
       Environment environment2 = mock(Environment.class);
       when(environment2.getId()).thenReturn(14L);
       
       
       when(environmentService.addEnvironment(eq(5L), anyString(), anyString()))
                   .thenReturn(environment1)
                   .thenReturn(environment2);
           
       environmentDeploymentService.performChanges(environmentRepresentation, deployment);
        
       verify(taskDeploymentService, times(1)).performTasksChanges(tasksRepresentation1, environment1);
       verify(taskDeploymentService, times(1)).performTasksChanges(tasksRepresentation2, environment2);

       // verify that the environment is in the right state
       // environmentService.updateEnvironmentConfigurationState(environmentId, ConfigurationState.TASKED);
       verify(environmentService, times(1)).updateEnvironmentConfigurationState(environment1.getId(), ConfigurationState.TASKED);
       verify(environmentService, times(1)).updateEnvironmentConfigurationState(environment2.getId(), ConfigurationState.TASKED);

   }
   
   @Test
   public void runTemplatePerformsChangesOnTriggers() throws Exception
   {
       
       Map triggerRepresentation1 = ImmutableMap.builder()
           .put("type", "afterSuccessfulPlan")
           .put("description", "triggerAfter1")
           .put("planKey", "PERSONAL-REST")
           .build();

       
       Map triggerRepresentation2 = ImmutableMap.builder()
           .put("type", "afterSuccessfulPlan")
           .put("description", "triggerAfter2")
           .put("planKey", "PERSONAL-REST")
           .build();
       
       Map environmentRepresentation1 = ImmutableMap.builder()
           .put("name", "environmentName1")
           .put("description", "environmentDescription1")
           .put("trigger", triggerRepresentation1)
           .build();
       
       Map environmentRepresentation2 = ImmutableMap.builder()
           .put("name", "environmentName2")
           .put("description", "environmentDescription2")
           .put("trigger", triggerRepresentation2)
           .build();
   
       List environmentRepresentation = Lists.newArrayList(environmentRepresentation1, environmentRepresentation2);
          
       DeploymentProject deployment = mock(DeploymentProject.class);
       when(deployment.getName()).thenReturn("deploymentName");
       when(deployment.getId()).thenReturn(5L);
       
       Environment environment1 = mock(Environment.class);
       when(environment1.getId()).thenReturn(13L);
       
       Environment environment2 = mock(Environment.class);
       when(environment2.getId()).thenReturn(14L);
       
       
       when(environmentService.addEnvironment(eq(5L), anyString(), anyString()))
                   .thenReturn(environment1)
                   .thenReturn(environment2);
           
       environmentDeploymentService.performChanges(environmentRepresentation, deployment);
        
       verify(triggerDeploymentService, times(1)).performTriggersChanges(triggerRepresentation1, environment1);
       verify(triggerDeploymentService, times(1)).performTriggersChanges(triggerRepresentation2, environment2);


   }   
   
   
   @Test
   public void runTemplatePerformsChangesOnVariables() throws Exception
   {
       
       Map variableRepresentation1 = ImmutableMap.builder()
           .put("key", "key1")
           .put("value", "value1")
           .build();

       
       Map variableRepresentation2 = ImmutableMap.builder()
           .put("key", "key2")
           .put("value", "value2")
           .build();
       
       Map environmentRepresentation1 = ImmutableMap.builder()
           .put("name", "environmentName1")
           .put("description", "environmentDescription1")
           .put("variable", variableRepresentation1)
           .build();
       
       Map environmentRepresentation2 = ImmutableMap.builder()
           .put("name", "environmentName2")
           .put("description", "environmentDescription2")
           .put("variable", variableRepresentation2)
           .build();
   
       List environmentRepresentation = Lists.newArrayList(environmentRepresentation1, environmentRepresentation2);
          
       DeploymentProject deployment = mock(DeploymentProject.class);
       when(deployment.getName()).thenReturn("deploymentName");
       when(deployment.getId()).thenReturn(5L);
       
       Environment environment1 = mock(Environment.class);
       when(environment1.getId()).thenReturn(13L);
       
       Environment environment2 = mock(Environment.class);
       when(environment2.getId()).thenReturn(14L);
       
       
       when(environmentService.addEnvironment(eq(5L), anyString(), anyString()))
                   .thenReturn(environment1)
                   .thenReturn(environment2);
           
       environmentDeploymentService.performChanges(environmentRepresentation, deployment);
        
       verify(variableDeploymentService, times(1)).performVariableChanges(variableRepresentation1, environment1);
       verify(variableDeploymentService, times(1)).performVariableChanges(variableRepresentation2, environment2);


   }   
   
   
   @Test
   public void runTemplatePerformsChangesOnNotifications() throws Exception
   {
       // notification(type:'Deployment Started and Finished', recipient:'email', email:'fake@fake.com')
       
       Map notificationRepresentation1 = ImmutableMap.builder()
           .put("type", "Deployment Started and Finished")
           .put("recipient", "email")
           .put("email", "fake1@fake.com")
           .build();

       
       Map notificationRepresentation2 = ImmutableMap.builder()
           .put("type", "Deployment Started and Finished")
           .put("recipient", "email")
           .put("email", "fake2@fake.com")
           .build();
       
       Map environmentRepresentation1 = ImmutableMap.builder()
           .put("name", "environmentName1")
           .put("description", "environmentDescription1")
           .put("notification", notificationRepresentation1)
           .build();
       
       Map environmentRepresentation2 = ImmutableMap.builder()
           .put("name", "environmentName2")
           .put("description", "environmentDescription2")
           .put("notification", notificationRepresentation2)
           .build();
   
       List environmentRepresentation = Lists.newArrayList(environmentRepresentation1, environmentRepresentation2);
          
       DeploymentProject deployment = mock(DeploymentProject.class);
       when(deployment.getName()).thenReturn("deploymentName");
       when(deployment.getId()).thenReturn(5L);
       
       Environment environment1 = mock(Environment.class);
       when(environment1.getId()).thenReturn(13L);
       
       Environment environment2 = mock(Environment.class);
       when(environment2.getId()).thenReturn(14L);
       
       
       when(environmentService.addEnvironment(eq(5L), anyString(), anyString()))
                   .thenReturn(environment1)
                   .thenReturn(environment2);
           
       environmentDeploymentService.performChanges(environmentRepresentation, deployment);
        
       verify(notificationDeploymentService, times(1)).performNotificationChanges(notificationRepresentation1, environment1);
       verify(notificationDeploymentService, times(1)).performNotificationChanges(notificationRepresentation2, environment2);


   }   
   
}
