package com.atlassian.bamboo.plugin.plantemplates;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(MockitoJUnitRunner.class)
public class AutoVariablesServiceTest {
    @InjectMocks
    private AutoVariablesService autoVariablesService;

    @Test
    public void noAdditionalVariablesWorks() {
        Map<String, String> autoVariables = autoVariablesService.getAdditionalVariables(Collections.<String, Object>emptyMap());

        assertThat(autoVariables.entrySet(), hasSize(1));
    }

    @Test
    public void repositoryUrlDetectedInDsl() {
        final String urlValue = "urlValue";
        Map<String, String> templateRepositoryMap = Collections.singletonMap("url", urlValue);
        Map<String, Object> dsl = Collections.<String, Object>singletonMap("templateRepository", templateRepositoryMap);

        Map<String, String> autoVariables = autoVariablesService.getAdditionalVariables(dsl);

        assertThat(autoVariables.entrySet(), hasSize(2));
        assertThat(autoVariables, hasEntry("plantemplate.repository.url", urlValue));
    }

    @Test
    public void templateRepositoryWithoutUrlWorks() {
        Map<String, String> templateRepositoryMap = Collections.singletonMap("randomVariable", "randomValue");
        Map<String, Object> dsl = Collections.<String, Object>singletonMap("templateRepository", templateRepositoryMap);

        Map<String, String> autoVariables = autoVariablesService.getAdditionalVariables(dsl);

        assertThat(autoVariables.entrySet(), hasSize(1));
    }

    @Test
    public void deployerPlanKeyDetectedInDsl() {
        final String deployerPlanKey = "projectKey-planKey";
        Map<String, String> deployerMap = Collections.singletonMap("planKey", deployerPlanKey);
        Map<String, Object> dsl = Collections.<String, Object>singletonMap("deployer", deployerMap);

        Map<String, String> autoVariables = autoVariablesService.getAdditionalVariables(dsl);

        assertThat(autoVariables.entrySet(), hasSize(2));
        assertThat(autoVariables, hasEntry("plantemplate.deployer.planKey", deployerPlanKey));
    }

    @Test
    public void deployerWithoutPlanKeyWorks() {
        Map<String, String> deployerMap = Collections.singletonMap("randomVariable", "randomValue");
        Map<String, Object> dsl = Collections.<String, Object>singletonMap("deployer", deployerMap);

        Map<String, String> autoVariables = autoVariablesService.getAdditionalVariables(dsl);

        assertThat(autoVariables.entrySet(), hasSize(1));
    }

    @Test
    public void ensureThatPlanTemplateIsAlwaysSet() {
        Map<String, String> autoVariables = autoVariablesService.getAdditionalVariables(Collections.<String, Object>emptyMap());

        assertThat(autoVariables.entrySet(), is(not(empty())));
        assertThat(autoVariables, hasEntry("plantemplate.enabled", "true"));
    }
}
