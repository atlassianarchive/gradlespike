package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.chains.DefaultChain;
import com.atlassian.bamboo.labels.Label;
import com.atlassian.bamboo.labels.LabelManager;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.user.User;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("rawtypes")
public class LabelServiceTest
{
    
    @InjectMocks
    private LabelService labelService;
    @Mock
    private LabelManager labelManager;

    @Test
    public void performLabelChangesAddsOneLabels(){
        Map label1 = ImmutableMap.builder()
                .put("name", "label1")
                .build();

        Chain chain = new DefaultChain();
        chain.setKey("plan-key");
        PlanKey planKey = chain.getPlanKey();

        labelService.performLabelChanges(chain, label1);

        verify(labelManager, times(1)).addLabel("label1", planKey , null);
    }

    @Test
    public void ensurePlanTemplateLabelAlwaysPresent(){
        Chain chain = new DefaultChain();
        chain.setKey("plan-key");
        PlanKey planKey = chain.getPlanKey();

        labelService.performLabelChanges(chain, ImmutableMap.of());

        verify(labelManager).addLabel("plan-templates", planKey , null);
    }

    @Test
    public void performLabelChangesAddsTwoLabels(){
        Map label1 = ImmutableMap.builder()
            .put("name", "label1")
            .build();
        
        Map label2 = ImmutableMap.builder()
            .put("name", "label2")
            .build();
        
        Chain chain = new DefaultChain();
        chain.setKey("plan-key");
        List labels = Lists.newArrayList(label1, label2);
        
        PlanKey planKey = chain.getPlanKey();

        labelService.performLabelChanges(chain, labels);

        verify(labelManager, times(1)).addLabel("label1", planKey , null);
        verify(labelManager, times(1)).addLabel("label2", planKey , null);

    }
    
    @Test
    public void performLabelChangesDeletesAllLabels(){
        Chain chain = new DefaultChain();
        chain.setKey("plan-key");

        Label existingLabel = mock(Label.class);
        when(existingLabel.getName()).thenReturn("labelName");
        when(labelManager.getPlanLabels(chain)).thenReturn(Sets.<Label>newHashSet(existingLabel));
        
        PlanKey planKey = chain.getPlanKey();

        labelService.performLabelChanges(chain, null);

        verify(labelManager, times(1)).addLabel(anyString(), any(PlanKey.class) , any(User.class));
        verify(labelManager, times(1)).removeLabel("labelName", planKey, null);
    }
    
    
    @Test
    public void performLabelChangesDeleteAllExistingLabelsAndCreatesNewOnes(){
        Map label1 = ImmutableMap.builder()
            .put("name", "label1")
            .build();
        
        Map label2 = ImmutableMap.builder()
            .put("name", "label2")
            .build();
        
        Chain chain = new DefaultChain();
        chain.setKey("plan-key");
        List labels = Lists.newArrayList(label1, label2);
        
        PlanKey planKey = chain.getPlanKey();
       
        Label existingLabel1 = mock(Label.class);
        when(existingLabel1.getName()).thenReturn("existingLabel1");
        Label existingLabel2 = mock(Label.class);
        when(existingLabel2.getName()).thenReturn("label2");
        when(labelManager.getPlanLabels(chain)).thenReturn(Sets.<Label>newHashSet(existingLabel1, existingLabel2));

        labelService.performLabelChanges(chain, labels);
        
        verify(labelManager, times(1)).removeLabel("existingLabel1", planKey, null);
        verify(labelManager, times(1)).removeLabel("label2", planKey, null);
        
        verify(labelManager, times(1)).addLabel("label1", planKey , null);
        verify(labelManager, times(1)).addLabel("label2", planKey , null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void performLabelChangesThrowsExcpetionOnBadLabel(){
        Map label1 = ImmutableMap.builder()
            .put("name", "bad:label")
            .build();
        
        Chain chain = new DefaultChain();
        chain.setKey("plan-key");
        
        when(labelManager.getPlanLabels(chain)).thenReturn(Sets.<Label>newHashSet());

        labelService.performLabelChanges(chain, label1);
        
        verify(labelManager, times(0)).addLabel(anyString(), any(PlanKey.class) , any(User.class));
    }

}
