package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class InjectBambooVariablesTaskMappingTest {

    @InjectMocks
    private InjectBambooVariablesTaskMapping injectBambooVariablesTaskMapping;
    
//  userDescription:test2
//  filePath:pathfile
//  createTaskKey:com.atlassian.bamboo.plugins.bamboo-variable-inject-plugin:inject


    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaskReturnsCorrectMap(){
        Map task = ImmutableMap.builder()
                .put("type", "injectBambooVariables")
                .put("description", "new inject task")
                .put("filePath", "path to the file")
                .build();

        Map<String,String> taskActionParams = injectBambooVariablesTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("userDescription", "new inject task")
                .put("taskDisabled", "false")
                .put("filePath", "path to the file")
                .put("createTaskKey", "com.atlassian.bamboo.plugins.bamboo-variable-inject-plugin:inject")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }



}
