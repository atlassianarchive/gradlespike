package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;

@RunWith(MockitoJUnitRunner.class)
public class CleanWorkingDirectoryTaskMappingTest
{

    @InjectMocks
    private CleanWorkingDirectoryTaskMapping cleanWorkingDirectoryTaskMapping;
    
    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaskWithMinimalDataReturnsCorrectMap(){
        
        Map task = ImmutableMap.builder()
            .put("type", "cleanWorkingDirectory")
            .put("description", "testCleanUpTaskDescription")
            .build();

        Map<String,String> taskActionParams = cleanWorkingDirectoryTaskMapping.convert(task);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("userDescription", "testCleanUpTaskDescription")
            .put("taskDisabled", "false")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.bamboo-artifact-downloader-plugin:cleanWorkingDirectoryTask")
            .build(); 
        
        Assert.assertEquals(expected, taskActionParams);
        
    }

}
