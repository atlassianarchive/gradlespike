package com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients;

import com.atlassian.bamboo.plugin.plantemplates.support.notifications.recipients.IMRecipientMapping;
import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class IMRecipientTest
{
    @InjectMocks
    private IMRecipientMapping imRecipientMapping;

    @Test
    @SuppressWarnings("rawtypes")
    public void emailMappingReturnsCorrectData(){
        Map notification = ImmutableMap.builder()
                .put("recipient", "IM")
                .put("type", "Build Complete")
                .put("address", "bob@chat.com")
                .build();

        String recipientKey = imRecipientMapping.getRecipientKey();
        String recipientData = imRecipientMapping.getRecipientData(notification);

        Assert.assertEquals(recipientKey, "com.atlassian.bamboo.plugin.system.notifications:recipient.im");
        Assert.assertEquals(recipientData, "bob@chat.com");
    }
}
