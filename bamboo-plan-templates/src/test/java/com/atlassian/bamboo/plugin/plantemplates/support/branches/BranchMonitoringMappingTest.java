package com.atlassian.bamboo.plugin.plantemplates.support.branches;

import com.atlassian.bamboo.chains.branches.BranchNotificationStrategy;
import com.atlassian.bamboo.plan.branch.BranchMonitoringConfiguration;
import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

@SuppressWarnings("rawtypes")
public class BranchMonitoringMappingTest
{
    @Test
    public void convertSetsAllGivenValues1() throws Exception
    {
        Map map = ImmutableMap.of("enabled", "true",
                                  "matchingPattern", "issue/[^/]*",
                                  "timeOfInactivityInDays", "10",
                                  "notificationStrategy", "INHERIT",
                                  "remoteJiraBranchLinkingEnabled", "true");
        BranchMonitoringConfiguration configuration = branchMonitoringMapping().apply(map);
        assertConfiguration(configuration, true, "issue/[^/]*", true, 10, BranchNotificationStrategy.INHERIT, true);
    }

    @Test
    public void convertSetsAllGivenValues2() throws Exception
    {
        Map map = ImmutableMap.of("enabled", "false",
                                  "matchingPattern", "",
                                  "timeOfInactivityInDays", "0",
                                  "notificationStrategy", "NONE",
                                  "remoteJiraBranchLinkingEnabled", "false");
        BranchMonitoringConfiguration configuration = branchMonitoringMapping().apply(map);
        assertConfiguration(configuration, false, "", false, 0, BranchNotificationStrategy.NONE, false);
    }

    @Test
    public void convertSetsAllGivenValuesAndUsesDefaultsForTheRest() throws Exception
    {
        Map map = ImmutableMap.of("enabled", "true",
                                  "notificationStrategy", "INHERIT",
                                  "remoteJiraBranchLinkingEnabled", "true");
        BranchMonitoringConfiguration configuration = branchMonitoringMapping().apply(map);
        assertConfiguration(configuration, true, "", true, 30, BranchNotificationStrategy.INHERIT, true);
    }

    @Test
    public void convertWithEmptyMapUsesDefaults() throws Exception
    {
        BranchMonitoringConfiguration configuration = branchMonitoringMapping().apply(ImmutableMap.of());
        assertDefaultConfiguration(configuration);
    }

    @Test
    public void convertWithNullMapUsesDefaults() throws Exception
    {
        BranchMonitoringConfiguration configuration = branchMonitoringMapping().apply(null);
        assertDefaultConfiguration(configuration);
    }

    private void assertDefaultConfiguration(BranchMonitoringConfiguration configuration)
    {
        assertConfiguration(configuration, false, "", false, 30, BranchNotificationStrategy.NOTIFY_COMMITTERS, true);
    }

    private void assertConfiguration(BranchMonitoringConfiguration configuration,
                                     boolean monitoringEnabled,
                                     String matchingPattern,
                                     boolean cleanupEnabled,
                                     int timeOfInactivityInDays,
                                     BranchNotificationStrategy notificationStrategy,
                                     boolean remoteJiraBranchLinkingEnabled)
    {
        Assert.assertEquals(monitoringEnabled, configuration.isMonitoringEnabled());
        Assert.assertEquals(matchingPattern, configuration.getMatchingPattern());
        Assert.assertEquals(cleanupEnabled, configuration.isCleanupEnabled());
        Assert.assertEquals(timeOfInactivityInDays, configuration.getTimeOfInactivityInDays());
        Assert.assertEquals(notificationStrategy, configuration.getDefaultBranchNotificationStrategy());
        Assert.assertEquals(remoteJiraBranchLinkingEnabled, configuration.isRemoteJiraBranchLinkingEnabled());
    }

    private BranchMonitoringMapping branchMonitoringMapping()
    {
        return new BranchMonitoringMapping();
    }
}
