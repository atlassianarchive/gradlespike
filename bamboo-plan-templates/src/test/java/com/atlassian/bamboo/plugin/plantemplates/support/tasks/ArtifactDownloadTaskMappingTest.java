package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.chains.DefaultChain;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinition;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionManager;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ArtifactDownloadTaskMappingTest
{
    @InjectMocks
    private ArtifactDownloadTaskMapping artifactDownloadTaskMapping;
    
    @Mock
    private ArtifactDefinitionManager artifactDefinitionManager;
    
    @Mock
    private PlanManager planManager;
    
    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaskWithMinimalDataReturnsCorrectMap(){
        
        Map artifact = ImmutableMap.builder()
            .put("name", "name")
            .put("localPath", "./test/artifact/")
            .build();
        
        Map task = ImmutableMap.builder()
            .put("type", "artifactDownload")
            .put("description", "artifactDownload description")
            .put("artifact", artifact)
            .put("planKey", "test-test")
            .build();

               
        Chain chain = new DefaultChain();
        
        ArtifactDefinition artifactDefinition = mock(ArtifactDefinition.class);
        when(artifactDefinition.getName()).thenReturn("name");
        when(artifactDefinition.getId()).thenReturn(12345L);
        when(artifactDefinitionManager.findSharedArtifactsByChain(chain)).thenReturn(Lists.newArrayList(artifactDefinition));
        
        when(planManager.getPlanByKey(PlanKeys.getPlanKey("test-test"), Chain.class)).thenReturn(chain);
        
        Map<String,String> taskActionParams = artifactDownloadTaskMapping.convert(task);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("userDescription", "artifactDownload description")
            .put("taskDisabled", "false")
            .put("artifactId_0", "12345")
            .put("localPath_0", "./test/artifact/")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.bamboo-artifact-downloader-plugin:artifactdownloadertask")
            .put("sourcePlanKey", "test-test")
            .build(); 
        
        Assert.assertEquals(expected, taskActionParams);
        
    }
    
    @Test(expected=IllegalArgumentException.class)
    @SuppressWarnings("rawtypes")
    public void convertATaskThrowsAnExceptionIfThePlanIsNotFound(){
        
        Map artifact = ImmutableMap.builder()
            .put("name", "name")
            .put("localPath", "./test/artifact/")
            .build();
        
        Map task = ImmutableMap.builder()
            .put("type", "artifactDownload")
            .put("planKey", "test-test")
            .put("description", "artifactDownload description")
            .put("artifact", artifact)
            .build();

        when(planManager.getPlanByKey(PlanKeys.getPlanKey("test-test"), Chain.class)).thenReturn(null);
        
        artifactDownloadTaskMapping.convert(task);
        
    }
    
       
    
    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaskWithMultipleArtifactsReturnsCorrectMap(){
        
        Map artifact1 = ImmutableMap.builder()
            .put("name", "name")
            .put("localPath", "./test/artifact/")
            .build();
        
        Map artifact2 = ImmutableMap.builder()
            .put("name", "name2")
            .put("localPath", "./test/artifact2/")
            .build();
        
        List artifacts = Lists.newArrayList(artifact1, artifact2);
        
        Map task = ImmutableMap.builder()
            .put("type", "artifactDownload")
            .put("description", "artifactDownload description")
            .put("artifact", artifacts)
            .put("planKey", "test-test")
            .build();
               
        Chain chain = new DefaultChain();
        
        ArtifactDefinition artifactDefinition1 = mock(ArtifactDefinition.class);
        when(artifactDefinition1.getName()).thenReturn("name");
        when(artifactDefinition1.getId()).thenReturn(12345L);
        
        ArtifactDefinition artifactDefinition2 = mock(ArtifactDefinition.class);
        when(artifactDefinition2.getName()).thenReturn("name2");
        when(artifactDefinition2.getId()).thenReturn(12L);
        
        when(artifactDefinitionManager.findSharedArtifactsByChain(chain)).thenReturn(Lists.newArrayList(artifactDefinition1, artifactDefinition2));
        
        when(planManager.getPlanByKey(PlanKeys.getPlanKey("test-test"), Chain.class)).thenReturn(chain);
        
        Map<String,String> taskActionParams = artifactDownloadTaskMapping.convert(task);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("userDescription", "artifactDownload description")
            .put("taskDisabled", "false")
            .put("artifactId_0", "12345")
            .put("localPath_0", "./test/artifact/")
            .put("artifactId_1", "12")
            .put("localPath_1", "./test/artifact2/")
            .put("sourcePlanKey", "test-test")
            .put("createTaskKey", "com.atlassian.bamboo.plugins.bamboo-artifact-downloader-plugin:artifactdownloadertask")
            .build(); 
        
        Assert.assertEquals(expected, taskActionParams);
        
    }
}
