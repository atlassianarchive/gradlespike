package com.atlassian.bamboo.plugin.plantemplates;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.caching.DashboardCachingManager;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.chains.DefaultChain;
import com.atlassian.bamboo.deletion.DeletionService;
import com.atlassian.bamboo.plugin.plantemplates.support.TemplateValidationException;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mchange.util.AssertException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("rawtypes")
public class PlanTemplateServiceTest
{
    @InjectMocks
    private PlanTemplateServiceImpl planTemplateServiceImpl;
  
    @Mock
    private DashboardCachingManager dashboardCachingManager;
    
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private DslEvaluator dslEvaluator;
    
    @Mock
    private SetupDslEvaluator setupDslEvaluator;
    
    @Mock
    private PerformPlanChanges performPlanChanges;
    
    @Mock
    private PerformDeploymentChanges performDeploymentChanges;
    
    @Mock
    private DeletionService deletionService;


    @Rule
    public ExpectedException exception = ExpectedException.none();
    
    
    @Before
    public void setUp(){
        planTemplateServiceImpl.setSetupDslEvaluator(setupDslEvaluator);
        when(setupDslEvaluator.setup(anyString())).thenReturn(dslEvaluator);
    }
    
    @Test
    public void runTemplatePerformsChangesOnAPlan() throws Exception
    {
        Map<String, String> projectRepresentation = ImmutableMap.<String, String>builder()
            .put("name", "projectName")
            .put("key", "projectKey")
            .put("description", "").build();
        
        Map planRepresentation = ImmutableMap.builder()
                .put("plan", ImmutableMap.builder()
                        .put("key", "planKey")
                        .put("name", "planName")
                        .put("description", "")
                        .put("project", projectRepresentation)
                        .build())
                .build();
        

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");

        when(dslEvaluator.run("TEST")).thenReturn(ImmutableMap.of("dslMap", planRepresentation, "errors", Lists.newArrayList()));       
        
        Assert.assertNotNull(planTemplateServiceImpl.runPlanTemplate("TEST"));
        verify(performPlanChanges, times(1)).performChanges(planRepresentation);
        
    }
    
   
    
    @Test
    public void runTemplateReturnsErrorsOnBadTemplate() throws Exception
    {

        Map planRepresentation = Maps.newHashMap();

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");
        
        List<String> errors = Lists.newArrayList("error1", "error2");
        
        when(dslEvaluator.run("TEST")).thenReturn(ImmutableMap.of("dslMap", planRepresentation, "errors", errors));
        
        try {
            planTemplateServiceImpl.runPlanTemplate("TEST");
            Assert.fail("TemplateValidationException was not thrown");
        }catch(TemplateValidationException e) {
            Assert.assertEquals("There were some errors validating the template", e.getMessage());
            Assert.assertEquals(errors, e.getErrors());
            Assert.assertEquals("TemplateValidationException: There were some errors validating the template\r\n\terror1\r\n\terror2\r\n", e.toString());
        }
        
        verify(performPlanChanges, times(0)).performChanges(any(Map.class));
    }
    
    
    
    @Test
    public void runTemplateReturnsTheMapWithWarnings() throws Exception
    {

        Map planRepresentation = Maps.newHashMap();

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");
        
        List<String> warnings = Lists.newArrayList("warn1", "warn2");
        
        when(dslEvaluator.run("TEST")).thenReturn(ImmutableMap.of("dslMap", planRepresentation, "warnings", warnings));
        
        Map<String, Object> result = planTemplateServiceImpl.runPlanTemplate("TEST");
        
        Assert.assertEquals(warnings, result.get("warnings"));
        
    }
    
    
    
    @Test
    public void expandDslReturnsAExpandedAndPrettyPrinttedVersionOfTheDsl() throws Exception
    {
        when(dslEvaluator.expandDsl("templateTest")).thenReturn("expandTest");
        when(dslEvaluator.validate("expandTest")).thenReturn(ImmutableMap.of("errors", Lists.newArrayList()));
        
        String expandDsl = planTemplateServiceImpl.expandDsl("templateTest", "shortcutsTest");
        
        Assert.assertEquals("expandTest", expandDsl);
    
    }


    @Test
    public void runTemplateClearTheCacheForThePlanCreated() throws Exception
    {
        Map planRepresentation = ImmutableMap.builder()
                .put("plan", ImmutableMap.builder()
                        .put("key", "planKey")
                        .put("name", "planName")
                        .put("description", "")
                        .put("project", Maps.newHashMap())
                        .build())
                .build();

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");

        when(dslEvaluator.run("TEST")).thenReturn(ImmutableMap.of("dslMap", planRepresentation, "errors", Lists.newArrayList()));
        when(performPlanChanges.performChanges(anyMap())).thenReturn(Lists.newArrayList(createdChain));

        planTemplateServiceImpl.runPlanTemplate("TEST");

        verify(dashboardCachingManager, times(1)).updatePlanCache(createdChain.getPlanKey());
    }

    @Test
    public void runTemplateClearTheCacheForMultiplePlans() throws Exception
    {
        Map plan1 = ImmutableMap.builder()
                        .put("key", "planKey1")
                        .put("name", "planName1")
                        .put("description", "")
                        .put("project", Maps.newHashMap())
                .build();

        Map plan2 = ImmutableMap.builder()
                        .put("key", "planKey2")
                        .put("name", "planName2")
                        .put("description", "2")
                        .put("project", Maps.newHashMap())
                        .build();
        
        Map planRepresentation = ImmutableMap.builder()
                .put("plan", ImmutableList.builder()
                    .add(plan1)
                    .add(plan2)
                    .build())
                .build();

        Chain createdChain1 = new DefaultChain();
        createdChain1.setKey("projectKey-planKey1");

        Chain createdChain2 = new DefaultChain();
        createdChain2.setKey("projectKey-planKey2");

        when(dslEvaluator.run("TEST")).thenReturn(ImmutableMap.of("dslMap", planRepresentation, "errors", Lists.newArrayList()));
        when(performPlanChanges.performChanges(anyMap())).thenReturn(Lists.newArrayList(createdChain1, createdChain2));

        planTemplateServiceImpl.runPlanTemplate("TEST");

        verify(dashboardCachingManager, times(1)).updatePlanCache(createdChain1.getPlanKey());
        verify(dashboardCachingManager, times(1)).updatePlanCache(createdChain2.getPlanKey());
    }
    
    
    
    @Test
    public void runTemplatePerformsChangesOnDeployments() throws Exception
    {
        
        Map deploymentRepresentation = ImmutableMap.builder()
                .put("deployment", ImmutableMap.builder()
                        .put("planKey", "planKey")
                        .put("name", "deploymentName")
                        .put("description", "")
                        .build())
                .build();
        

        Chain createdChain = new DefaultChain();
        createdChain.setKey("projectKey-planKey");

        when(dslEvaluator.run("TEST")).thenReturn(ImmutableMap.of("dslMap", deploymentRepresentation, "errors", Lists.newArrayList()));       
        
        planTemplateServiceImpl.runPlanTemplate("TEST");
        
        verify(performDeploymentChanges, times(1)).performChanges(deploymentRepresentation);
        
    }
    

   
}
