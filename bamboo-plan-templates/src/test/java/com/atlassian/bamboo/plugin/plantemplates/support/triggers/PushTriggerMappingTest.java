package com.atlassian.bamboo.plugin.plantemplates.support.triggers;

import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;

@RunWith(MockitoJUnitRunner.class)
public class PushTriggerMappingTest
{
    @InjectMocks
    private PushTriggerMapping pushTriggetMapping;
    
    @SuppressWarnings("rawtypes")
    @Test
    public void convertAPushTriggerReturnsCorrectMap() {
        Map trigger = ImmutableMap.builder()
            .put("description", "trigger1")
            .put("type", "push")
            .build();  

        Map<String, String> result = pushTriggetMapping.convert(trigger);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("selectedBuildStrategy", "trigger")
            .put("repository.change.trigger.triggerIpAddress", "")
            .build();
        
        Assert.assertEquals(expected, result);
        
    }
    
    @SuppressWarnings("rawtypes")
    @Test
    public void convertAPushTriggerWithIpsReturnsCorrectMap() {
        Map server1 = ImmutableMap.builder()
            .put("ip", "127.0.0.1")
            .build();
        
        Map server2 = ImmutableMap.builder()
            .put("ip", "127.0.0.2")
            .build();
        
        List servers = ImmutableList.builder()
            .add(server1)
            .add(server2)
            .build();
        
        Map trigger = ImmutableMap.builder()
            .put("description", "trigger1")
            .put("type", "push")
            .put("server", servers)
            .build();  

        Map<String, String> result = pushTriggetMapping.convert(trigger);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("selectedBuildStrategy", "trigger")
            .put("repository.change.trigger.triggerIpAddress", "127.0.0.1,127.0.0.2")
            .build();
        
        Assert.assertEquals(expected, result);
        
    }
    
    @SuppressWarnings("rawtypes")
    @Test
    public void convertAPushTriggerWithASingleIpReturnsCorrectMap() {
        Map server1 = ImmutableMap.builder()
            .put("ip", "127.0.0.1")
            .build();

        Map trigger = ImmutableMap.builder()
            .put("description", "trigger1")
            .put("type", "push")
            .put("server", server1)
            .build();  

        Map<String, String> result = pushTriggetMapping.convert(trigger);
        
        Map<String, String> expected = ImmutableMap.<String, String>builder()
            .put("selectedBuildStrategy", "trigger")
            .put("repository.change.trigger.triggerIpAddress", "127.0.0.1")
            .build();
        
        Assert.assertEquals(expected, result);
        
    }
}
