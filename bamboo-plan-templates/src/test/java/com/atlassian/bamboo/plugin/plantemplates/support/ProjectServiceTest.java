package com.atlassian.bamboo.plugin.plantemplates.support;

import java.util.Map;

import com.atlassian.bamboo.project.DefaultProject;
import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.project.ProjectManager;

import com.google.common.collect.ImmutableMap;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProjectServiceTest
{
    
    @InjectMocks
    public ProjectService projectService;
    
    @Mock
    private ProjectManager projectManager;
    
    @SuppressWarnings({"unchecked","rawtypes"})
    @Test
    public void createOrUpdateCreatesNewProject() throws Exception
    {
        Map projectRepresentation = ImmutableMap.builder()
                                .put("name", "projectName")
                                .put("key", "projectKey")
                                .put("description", "").build();

        DefaultProject createdProject = new DefaultProject("projectKey", "projectName", "");

        when(projectManager.isExistingProjectKey(anyString())).thenReturn(false);
        when(projectManager.createProject("projectKey", "projectName", "")).thenReturn(createdProject);

        Project project = projectService.createOrUpdate(projectRepresentation);
        
        Assert.assertNotNull(project);
        Assert.assertEquals("projectName", project.getName());
        Assert.assertEquals("projectKey", project.getKey());
        Assert.assertEquals("", project.getDescription());
        
        verify(projectManager, times(1)).saveProject(createdProject);
    }
    
    @Test
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void createOrUpdateUpdatesNewProject() throws Exception
    {
        Map projectRepresentation = ImmutableMap.builder()
                                .put("name", "projectName")
                                .put("key", "oldkey")
                                .put("description", "").build();

        DefaultProject existingProject = new DefaultProject("oldkey", "oldname", "oldDescription");
        
        when(projectManager.isExistingProjectKey(anyString())).thenReturn(true);
        when(projectManager.getProjectByKey("oldkey")).thenReturn(existingProject);

        Project project = projectService.createOrUpdate(projectRepresentation);
        
        Assert.assertNotNull(project);
        Assert.assertEquals("projectName", project.getName());
        Assert.assertEquals("oldkey", project.getKey());
        Assert.assertEquals("", project.getDescription());
        
        verify(projectManager, times(1)).saveProject(existingProject);
    }


}
