package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_PROMOTE_MULTIPLE_VERSION;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("UnusedDeclaration")
public class PromoteArtifactsVersionTaskMappingTest
{
    @InjectMocks
    private PromoteArtifactsVersionTaskMapping promoteArtifactsVersionTaskMapping;

    private final static String PROMOTION_SET = "artifact=\"a\",version=\"2.9\",product=\"bamboo\"\n" +
            "artifact=\"b\",version=\"1.9\"";
    private final static String PRECONDITIONS_SET = "artifact=\"a\",version=\"2.5\",product=\"bamboo\"\n" +
            "artifact=\"b\",version=\"1.4\"";

    @Test
    public void convertWillHandleAllFields()
    {
        Map<String, String> task = ImmutableMap.<String, String>builder()
                .put("type", BAMBOONICORN_PROMOTE_MULTIPLE_VERSION.getTaskKey())
                .put("description", "task description")
                .put("promotionSet", PROMOTION_SET)
                .put("preconditionsSet", PRECONDITIONS_SET)
                .put("forcePromote", "false")
                .put("deploymentEnvironment", "jirastudio-prd")
                .put("username", "username")
                .put("passwordVariable", "${bamboo.password}")
                .build();

        Map<String,String> taskActionParams = promoteArtifactsVersionTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_PROMOTE_MULTIPLE_VERSION.createTaskMappingKey())
                .put("promotionSet", PROMOTION_SET)
                .put("preconditionsSet", PRECONDITIONS_SET)
                .put("forcePromote", "false")
                .put("deploymentEnvironment", "PROD")
                .put("username", "username")
                .put("passwordVariable", "${bamboo.password}")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }
}