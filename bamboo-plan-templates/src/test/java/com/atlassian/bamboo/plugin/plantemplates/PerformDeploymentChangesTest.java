package com.atlassian.bamboo.plugin.plantemplates;
import java.util.Map;

import com.atlassian.bamboo.deployments.projects.DeploymentProject;
import com.atlassian.bamboo.deployments.projects.service.DeploymentProjectService;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plugin.plantemplates.support.DeploymentVersioningService;
import com.atlassian.bamboo.plugin.plantemplates.support.EnvironmentDeploymentService;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("rawtypes")
@RunWith(MockitoJUnitRunner.class)
public class PerformDeploymentChangesTest
{
    
    @InjectMocks
    private PerformDeploymentChanges performDeploymentChanges;
    
    @Mock
    private DeploymentProjectService deploymentProjectService;
    @Mock
    private EnvironmentDeploymentService environmentDeploymentService;
    @Mock
    private DeploymentVersioningService deploymentVersioningService;
    
    
    @Test
    public void runTemplatePerformsChangesOnDeployment() throws Exception
    {
        
        Map deploymentRepresentation = ImmutableMap.builder()
                .put("deployment", ImmutableMap.builder()
                        .put("planKey", "projectkey-planKey")
                        .put("name", "deploymentName")
                        .put("description", "deployDescription")
                        .build())
                .build();
        
        performDeploymentChanges.performChanges(deploymentRepresentation);
        verify(deploymentProjectService, times(1)).addDeploymentProject("deploymentName", "deployDescription", "projectkey-planKey");
    }
    
    @Test
    public void runTemplatePerformsUpdateOnDeployment() throws Exception
    {
        
        Map deploymentRepresentation = ImmutableMap.builder()
                .put("deployment", ImmutableMap.builder()
                        .put("planKey", "projectkey-planKey")
                        .put("name", "deploymentName")
                        .put("description", "newDescription")
                        .build())
                .build();
        
        DeploymentProject deployment = mock(DeploymentProject.class);
        when(deployment.getName()).thenReturn("deploymentName");
        when(deployment.getId()).thenReturn(5L);
        when(deploymentProjectService.getDeploymentProjectsRelatedToPlan(PlanKeys.getPlanKey("projectkey-planKey"))).thenReturn(Lists.newArrayList(deployment));
        
        performDeploymentChanges.performChanges(deploymentRepresentation);
        
        verify(deploymentProjectService, times(0)).addDeploymentProject(anyString(), anyString(), anyString());
        verify(deploymentProjectService, times(1)).editDeploymentProject(5L, "deploymentName", "newDescription", "projectkey-planKey");
    }
    
    
    @Test
    public void runTemplatePerformsChangesOnMultipleDeployments() throws Exception
    {
        
        Map deployment1 = ImmutableMap.builder()
                    .put("planKey", "projectkey-planKey1")
                    .put("name", "deploymentName1")
                    .put("description", "deployDescription1")
                    .build();
        
        Map deployment2 = ImmutableMap.builder()
                    .put("planKey", "projectkey-planKey2")
                    .put("name", "deploymentName2")
                    .put("description", "deployDescription2")
                    .build();
            
        Map deploymentRepresentation = ImmutableMap.builder()
            .put("deployment", ImmutableList.builder()
                .add(deployment1)
                .add(deployment2)
                .build())
            .build();
        
        performDeploymentChanges.performChanges(deploymentRepresentation);
        verify(deploymentProjectService, times(1)).addDeploymentProject("deploymentName1", "deployDescription1", "projectkey-planKey1");
        verify(deploymentProjectService, times(1)).addDeploymentProject("deploymentName2", "deployDescription2", "projectkey-planKey2");
    }
    
    
    @Test
    public void runTemplatePerformsTheChangesOnEnvironments() throws Exception
    {
        Map environment1 = ImmutableMap.builder()
                 .put("name", "environmentName1")
                 .put("description", "environmentDescription2")
                 .build();       
        Map environment2 = ImmutableMap.builder()
            .put("name", "environmentName1")
            .put("description", "environmentDescription2")
            .build();
             
        Map deployment1 = ImmutableMap.builder()
            .put("planKey", "projectkey-planKey1")
            .put("name", "deploymentName1")
            .put("description", "deployDescription1")
            .put("environment", environment1)
            .build();

        Map deployment2 = ImmutableMap.builder()
                    .put("planKey", "projectkey-planKey2")
                    .put("name", "deploymentName2")
                    .put("description", "deployDescription2")
                    .put("environment", environment2)
                    .build();
            
        Map deploymentRepresentation = ImmutableMap.builder()
            .put("deployment", ImmutableList.builder()
                .add(deployment1)
                .add(deployment2)
                .build())
            .build();
        
        
        DeploymentProject deploymentProject1 = mock(DeploymentProject.class);
        when(deploymentProject1.getName()).thenReturn("deploymentName1");
        when(deploymentProject1.getId()).thenReturn(5L);
        
        DeploymentProject deploymentProject2 = mock(DeploymentProject.class);
        when(deploymentProject2.getName()).thenReturn("deploymentName2");
        when(deploymentProject2.getId()).thenReturn(6L);
        
        when(deploymentProjectService.addDeploymentProject(anyString(), anyString(), anyString())).thenReturn(deploymentProject1);        
        when(deploymentProjectService.editDeploymentProject(anyLong(), anyString(), anyString(), anyString())).thenReturn(deploymentProject2);
        
        when(deploymentProjectService.getDeploymentProjectsRelatedToPlan(PlanKeys.getPlanKey("projectkey-planKey1"))).thenReturn(Lists.newArrayList(deploymentProject1));

        performDeploymentChanges.performChanges(deploymentRepresentation);
        
        verify(environmentDeploymentService, times(1)).performChanges(environment1, deploymentProject1);
        verify(environmentDeploymentService, times(1)).performChanges(environment2, deploymentProject2);
        
    }
    
    
    
    @Test
    public void runTemplatePerformsTheChangesOnVersioning() throws Exception
    {        
        // versioning(version:'release-1-${bamboo.variable1}-${bamboo.variable2}', autoIncrementNumber: true) 
        
        Map versioning = ImmutableMap.builder()
                 .put("version", "release-1-${bamboo.variable1}-${bamboo.variable2}")
                 .put("autoIncrementNumber", "true")
                 .build();       
             
        Map deployment1 = ImmutableMap.builder()
            .put("planKey", "projectkey-planKey1")
            .put("name", "deploymentName1")
            .put("description", "deployDescription1")
            .put("versioning", versioning)
            .build();
        
        Map deploymentRepresentation = ImmutableMap.builder()
            .put("deployment", ImmutableList.builder()
                .add(deployment1)
                .build())
            .build();
        
        DeploymentProject deploymentProject = mock(DeploymentProject.class);
        when(deploymentProject.getName()).thenReturn("deploymentName1");
        when(deploymentProject.getId()).thenReturn(5L);
  
        when(deploymentProjectService.addDeploymentProject(anyString(), anyString(), anyString())).thenReturn(deploymentProject);        

        performDeploymentChanges.performChanges(deploymentRepresentation);
        
        verify(deploymentVersioningService, times(1)).performChanges(versioning, deploymentProject);
        
    }
    
    

}
