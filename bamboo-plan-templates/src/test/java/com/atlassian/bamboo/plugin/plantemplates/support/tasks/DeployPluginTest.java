package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import com.atlassian.bamboo.plugin.plantemplates.support.tasks.deployTask.Crypto;
import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DeployPluginTest
{
    @Mock
    private Crypto crypto;

    @InjectMocks
    private DeployPluginTaskMapping continuousDeployPluginTaskMapping;

    @Test
    public void testDeployToJira()
    {
        when(crypto.encrypt("passValue")).thenReturn(ImmutableMap.of("key", "cryptKey", "encrypted", "cryptPass"));

        Map task = ImmutableMap.builder()
                .put("type", "deployPlugin")
                .put("description", "testContinouousDeployPlugin")
                .put("url", "http://mybamboo:1990/bamboo")
                .put("username", "usernameValue")
                .put("password", "passValue")
                .put("artifact", "artifactName")
                .put("product", "jira")
                .build();

        Map<String,String> taskActionParams = continuousDeployPluginTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("userDescription", "testContinouousDeployPlugin")
                .put("taskDisabled", "false")
                .put("confDeployUsername", "usernameValue")
                .put("confDeployPassword", "cryptPass")
                .put("confDeployKey", "cryptKey")
                .put("confDeployURL", "http://mybamboo:1990/bamboo")
                .put("confDeployJar", "artifactName")
                .put("pluginInstallationTimeout", "90")
                .put("createTaskKey", "com.atlassian.bamboo.plugins.deploy.continuous-plugin-deployment:deploy-task")
                .put("bcpd.config.productType", "bcpd.product.jira")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }

    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaskWithOptionalDataReturnsCorrectMap(){
        when(crypto.encrypt("passValue")).thenReturn(ImmutableMap.of("key", "cryptKey", "encrypted", "cryptPass"));

        Map task = ImmutableMap.builder()
                .put("type", "deployPlugin")
                .put("description", "testDeployBambooPlugin")
                .put("url", "http://mybamboo:1990/bamboo")
                .put("passwordVariable", "passValue")
                .put("artifact", "artifactName")
                .put("runOnBranch", "true")
                .put("useAtlassianId", "true")
                .put("atlassianIdUsername", "atlassianIdUser")
                .put("atlassianIdPassword", "passValue")
                .put("atlassianIdBaseURL", "http://atlassian")
                .put("useAtlassianIdWebSudo", "true")
                .put("atlassianIdAppName", "atlassianId")
                .put("product", "jira")
                .build();

        Map<String,String> taskActionParams = continuousDeployPluginTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("userDescription", "testDeployBambooPlugin")
                .put("taskDisabled", "false")
                .put("confDeployPasswordVariable", "passValue")
                .put("confDeployURL", "http://mybamboo:1990/bamboo")
                .put("confDeployJar", "artifactName")
                .put("deployBranchEnabled", "true")
                .put("pluginInstallationTimeout", "90")
                .put("createTaskKey", "com.atlassian.bamboo.plugins.deploy.continuous-plugin-deployment:deploy-task")
                .put("useAtlassianId", "true")
                .put("atlassianIdUsername", "atlassianIdUser")
                .put("atlassianIdPassword", "cryptPass")
                .put("atlassianIdKey", "cryptKey")
                .put("atlassianIdBaseURL", "http://atlassian")
                .put("useAtlassianIdWebSudo", "true")
                .put("atlassianIdAppName", "atlassianId")
                .put("bcpd.config.productType", "bcpd.product.jira")
                .put("confDeployPasswordVariableCheck", "true")
                .build();
        Assert.assertEquals(expected, taskActionParams);

    }
}
