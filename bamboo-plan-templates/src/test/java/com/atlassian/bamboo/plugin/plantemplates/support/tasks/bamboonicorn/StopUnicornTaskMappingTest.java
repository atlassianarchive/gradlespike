package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_STOP_UNICORN;

@RunWith(MockitoJUnitRunner.class)
public class StopUnicornTaskMappingTest
{
    @InjectMocks
    private StopUnicornTaskMapping stopUnicornTaskMapping;

    @Test
    @SuppressWarnings("rawtypes")
    public void convertAStopUnicornTaskWithMinimalParamsReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_STOP_UNICORN.getTaskKey())
                .put("description", "task description")
                .build();

        Map<String,String> taskActionParams = stopUnicornTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_STOP_UNICORN.createTaskMappingKey())
                .put("mode", "STOP")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }

    @Test
    @SuppressWarnings("rawtypes")
    public void convertAStopUnicornTaskReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_STOP_UNICORN.getTaskKey())
                .put("description", "task description")
                .put("unicornInstance", "test-unicorn-instance.jira-dev.com")
                .put("mode", "DESTROY")
                .build();

        Map<String,String> taskActionParams = stopUnicornTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_STOP_UNICORN.createTaskMappingKey())
                .put("unicornInstance", "test-unicorn-instance.jira-dev.com")
                .put("mode", "DESTROY")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }
}
