package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_UPDATE_LICENSE;

@RunWith(MockitoJUnitRunner.class)
public class UpdateLicenseTaskMappingTest
{
    private static final String TEST_LICENSE = "AAAB6w0ODAoPeNp9VE1vozAUvPtXIO0ZxEdToUpIm2aRttqQREl278Y86NsaGxnTbv79Oia0BdIcO\n" +
            "PjjzbyZN+ZbJoWzbJTjx04QPkT+Qxg76eHohH4QkTUyEC0cTw1saA3Japtl6X71tFyTnNa5lN6y0\n" +
            "3IPAt6SkvIWyCHdJOZzF75PmBSlR5nGV0i06oAw1THMOYw2LxzpvwbV6QfVkHSCY40aioHkETlHU\n" +
            "e1AoSySICR/UdERyK5T7JmOUIyAO9dfuH5MNl2dg9qWv1tQbeIGBFtPKqykZ4E01I10G95VKEaov\n" +
            "G/tj6lCKZKQpK+Ud1SfF73cjKLQIKhgY+o4cv27M/VKgb0/tDTsr2mrM1lgiWAURfeLKI7DMI7vQ\n" +
            "yJFATUVRd/DxYJBwqNdriWjfFmB0FaONfq6SaKrf8rWeNlrD/wBcLC9QCsn3RzT/W7/dEg/pjSvX\n" +
            "UmhjUGpkc0Txql68V5ME98Lsz41nArQHpM1KbF9hhPMe7qqZg+11HCRY1hs47NSpuRbMRrPwDLN4\n" +
            "LuAGcZQMVdmOb/2xB5PeW6kaE4wUH8WcANgynXVuJ1x3Abg0OUtU9jYti30VlVUYNtn9dN4PsyZu\n" +
            "XYO0ZdxGT3j881p+fACp0G/oXE2nku8+l/NOV3OL5MJ8h/v3KR2MCwCFCriT/QddDSF8RYypr6bR\n" +
            "XsnQNLkAhRLGdsepMXIeHH8juyNlVsvgNL5Uw==X02nb";

    @InjectMocks
    private UpdateLicenseTaskMapping updateLicenseTaskMapping;

    @Test
    @SuppressWarnings("rawtypes")
    public void convertAUpdateLicenseTaskWithMinimalParamsReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_UPDATE_LICENSE.getTaskKey())
                .put("description", "task description")
                .build();

        Map<String,String> taskActionParams = updateLicenseTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_UPDATE_LICENSE.createTaskMappingKey())
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }

    @Test
    @SuppressWarnings("rawtypes")
    public void convertAUpdateLicenseTaskReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_UPDATE_LICENSE.getTaskKey())
                .put("description", "task description")
                .put("unicornInstance", "test-unicorn-instance.jira-dev.com")
                .put("license", TEST_LICENSE)
                .build();

        Map<String,String> taskActionParams = updateLicenseTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_UPDATE_LICENSE.createTaskMappingKey())
                .put("unicornInstance", "test-unicorn-instance.jira-dev.com")
                .put("license", TEST_LICENSE)
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }
}
