package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class AntTaskMappingTest {
    @InjectMocks
    private AntTaskMapping antTaskMapper;


    // userDescription
    // taskDisabled (false, true)
    // label
    // buildFile
    // target
    // buildJdk:JDK 1.7.0_40
    // environmentVariables
    // workingSubDirectory:my-sub/
    // testChecked:true
    // testResultsDirectory:**/test-reports/*.xml
    // createTaskKey -> com.atlassian.bamboo.plugins.ant:task.builder.ant

    @Test
    @SuppressWarnings("rawtypes")
    public void convertMinimalFieldsToAnAntTask() {
        Map task = ImmutableMap.builder()
                .put("type", "ant")
                .put("description", "task1Ant")
                .put("executable", "ANT")
                .put("target", "install")
                .put("buildJdk", "JDK 7")
                .put("hasTests", "false")
                .build();

        Map<String, String> taskActionParams = antTaskMapper.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("userDescription", "task1Ant")
                .put("taskDisabled", "false")
                .put("label", "ANT")
                .put("target", "install")
                .put("createTaskKey", "com.atlassian.bamboo.plugins.ant:task.builder.ant")
                .put("buildJdk", "JDK 7")
                .put("testChecked", "false")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }

    @Test
    @SuppressWarnings("rawtypes")
    public void convertCompleteFieldsToAnAntTask() {
        Map task = ImmutableMap.builder()
                .put("type", "ant")
                .put("description", "task1Ant")
                .put("buildFile", "antBuildFile")
                .put("taskDisabled", "true")
                .put("executable", "ANT")
                .put("target", "install")
                .put("buildJdk", "JDK 7")
                .put("environmentVariables", "env=\"bla\"")
                .put("workingSubDirectory", "myDirectory/test")
                .put("hasTests", "true")
                .put("testResultsDirectory", "myTestDirectory/test")
                .build();

        Map<String, String> taskActionParams = antTaskMapper.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("userDescription", "task1Ant")
                .put("buildFile", "antBuildFile")
                .put("taskDisabled", "true")
                .put("label", "ANT")
                .put("target", "install")
                .put("createTaskKey", "com.atlassian.bamboo.plugins.ant:task.builder.ant")
                .put("buildJdk", "JDK 7")
                .put("environmentVariables", "env=\"bla\"")
                .put("workingSubDirectory", "myDirectory/test")
                .put("testChecked", "true")
                .put("testResultsDirectory", "myTestDirectory/test")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }
}
