package com.atlassian.bamboo.plugin.plantemplates.support.tasks.bamboonicorn;

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.atlassian.bamboo.plugin.plantemplates.support.TaskMappingKey.BAMBOONICORN_COPY_TO_UNICORN;

@RunWith(MockitoJUnitRunner.class)
public class CopyToUnicornTaskMappingTest
{
    @InjectMocks
    private CopyToUnicornTaskMapping copyToUnicornTaskMapping;

    @Test
    @SuppressWarnings("rawtypes")
    public void convertACopyToUnicornTaskWithMinimalParamsReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_COPY_TO_UNICORN.getTaskKey())
                .put("description", "task description")
                .build();

        Map<String,String> taskActionParams = copyToUnicornTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("createTaskKey", BAMBOONICORN_COPY_TO_UNICORN.createTaskMappingKey())
                .put("sourceType", "PATH_SOURCE_TYPE")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }

    @Test
    @SuppressWarnings("rawtypes")
    public void convertACopyToUnicornTaskWithArtifactReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_COPY_TO_UNICORN.getTaskKey())
                .put("description", "task description")
                .put("unicornInstance", "fake-instance.jira-dev.com")
                .put("sourceType", "ARTIFACT_SOURCE_TYPE")
                .put("artifactSource", "jira")
                .put("destination", "fake/destination/path")
                .build();

        Map<String,String> taskActionParams = copyToUnicornTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("unicornInstance", "fake-instance.jira-dev.com")
                .put("sourceType", "ARTIFACT_SOURCE_TYPE")
                .put("artifactSource", "jira")
                .put("destination", "fake/destination/path")
                .put("createTaskKey", BAMBOONICORN_COPY_TO_UNICORN.createTaskMappingKey())
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }

    @Test
    @SuppressWarnings("rawtypes")
    public void convertACopyToUnicornTaskWithPathReturnsCorrectMap()
    {
        Map task = ImmutableMap.builder()
                .put("type", BAMBOONICORN_COPY_TO_UNICORN.getTaskKey())
                .put("description", "task description")
                .put("unicornInstance", "fake-instance.jira-dev.com")
                .put("pathSource", "local/fake/path")
                .put("destination", "fake/destination/path")
                .build();

        Map<String,String> taskActionParams = copyToUnicornTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("taskDisabled", "false")
                .put("userDescription", "task description")
                .put("unicornInstance", "fake-instance.jira-dev.com")
                .put("sourceType", "PATH_SOURCE_TYPE")
                .put("pathSource", "local/fake/path")
                .put("destination", "fake/destination/path")
                .put("createTaskKey", BAMBOONICORN_COPY_TO_UNICORN.createTaskMappingKey())
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }
}
