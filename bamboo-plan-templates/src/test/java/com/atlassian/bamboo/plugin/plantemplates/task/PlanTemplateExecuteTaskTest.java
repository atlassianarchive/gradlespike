package com.atlassian.bamboo.plugin.plantemplates.task;

import java.io.File;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskState;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import org.junit.Assert;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlanTemplateExecuteTaskTest
{
    
    @InjectMocks
    private PlanTemplateExecuteTask planTemplateExecuteTask;
    
    @Mock
    private FileResourceToString fileResourceToString;
    
    @Mock
    private HttpPostRequest httpPostRequest;
    
    @Test
    public void taskExecutionReturnsSuccessAndCallTheRestEndpoint() throws Exception{
        planTemplateExecuteTask.setFileResourceToString(fileResourceToString);
        planTemplateExecuteTask.setHttpPostRequest(httpPostRequest);
        
        when(fileResourceToString.convert(any(File.class), eq("templateValue"))).thenReturn("templateBody");
        when(fileResourceToString.convert(any(File.class), eq("shortcutsValue"))).thenReturn("shortcutsBody");
        TaskContext taskContext = mock(TaskContext.class, Mockito.RETURNS_DEEP_STUBS);
        when(taskContext.getBuildContext().getPlanKey()).thenReturn("PROJECT-PLAN");
        
        ConfigurationMap configurationMap = mock(ConfigurationMap.class);
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);
        when(configurationMap.get("template")).thenReturn("templateValue");
        when(configurationMap.get("shortcuts")).thenReturn("shortcutsValue");
        when(configurationMap.get("bambooServer")).thenReturn("bambooServerValue");
        when(configurationMap.get("username")).thenReturn("usernameValue");
        when(configurationMap.get("password")).thenReturn("passwordValue");
        when(httpPostRequest.expandDsl(eq("bambooServerValue"), startsWith("templateBody"), eq("shortcutsBody"), eq("usernameValue"), eq("passwordValue"))).thenReturn("TEST");        
                
        TaskResult taskResult = planTemplateExecuteTask.execute(taskContext);
        
        verify(httpPostRequest, times(1)).postDsl(eq("bambooServerValue"), startsWith("templateBody"), eq("shortcutsBody"), eq("usernameValue"), eq("passwordValue"));
        Assert.assertEquals(TaskState.SUCCESS, taskResult.getTaskState());
    }
    
    
    @Test
    public void taskExecutionReturnsFailAndCallTheRestEndpoint() throws Exception{
        planTemplateExecuteTask.setFileResourceToString(fileResourceToString);
        planTemplateExecuteTask.setHttpPostRequest(httpPostRequest);

        when(fileResourceToString.convert(any(File.class), eq("templateValue"))).thenReturn("templateBody");
        when(fileResourceToString.convert(any(File.class), eq("shortcutsValue"))).thenReturn("shortcutsBody");
        TaskContext taskContext = mock(TaskContext.class, Mockito.RETURNS_DEEP_STUBS);
        
        ConfigurationMap configurationMap = mock(ConfigurationMap.class);
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);
        when(httpPostRequest.postDsl(eq("bambooServerValue"), startsWith("templateBody"), eq("shortcutsBody"), eq("usernameValue"), eq("passwordValue"))).thenThrow(new HttpPostException("test"));
        
        when(configurationMap.get("template")).thenReturn("templateValue");
        when(configurationMap.get("shortcuts")).thenReturn("shortcutsValue");
        when(configurationMap.get("bambooServer")).thenReturn("bambooServerValue");
        when(configurationMap.get("username")).thenReturn("usernameValue");
        when(configurationMap.get("password")).thenReturn("passwordValue");
        when(httpPostRequest.expandDsl(eq("bambooServerValue"), startsWith("templateBody"), eq("shortcutsBody"), eq("usernameValue"), eq("passwordValue"))).thenReturn("TEST");
        
        TaskResult taskResult = null;

        taskResult = planTemplateExecuteTask.execute(taskContext);
        
        verify(httpPostRequest, times(1)).postDsl(eq("bambooServerValue"), startsWith("templateBody"), eq("shortcutsBody"), eq("usernameValue"), eq("passwordValue"));
        Assert.assertEquals(TaskState.FAILED, taskResult.getTaskState());
    }
    
    
    @Test
    public void taskExecutionReturnsFailAndCallTheRestEndpointIfExpandFails() throws Exception{
        planTemplateExecuteTask.setFileResourceToString(fileResourceToString);
        planTemplateExecuteTask.setHttpPostRequest(httpPostRequest);

        when(fileResourceToString.convert(any(File.class), eq("templateValue"))).thenReturn("templateBody");
        when(fileResourceToString.convert(any(File.class), eq("shortcutsValue"))).thenReturn("shortcutsBody");
        TaskContext taskContext = mock(TaskContext.class, Mockito.RETURNS_DEEP_STUBS);
        
        ConfigurationMap configurationMap = mock(ConfigurationMap.class);
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);
        when(httpPostRequest.expandDsl(eq("bambooServerValue"), startsWith("templateBody"), eq("shortcutsBody"), eq("usernameValue"), eq("passwordValue"))).thenThrow(new HttpPostException("test"));
        
        when(configurationMap.get("template")).thenReturn("templateValue");
        when(configurationMap.get("shortcuts")).thenReturn("shortcutsValue");
        when(configurationMap.get("bambooServer")).thenReturn("bambooServerValue");
        when(configurationMap.get("username")).thenReturn("usernameValue");
        when(configurationMap.get("password")).thenReturn("passwordValue");
        
        TaskResult taskResult = null;

        taskResult = planTemplateExecuteTask.execute(taskContext);
        
        verify(httpPostRequest, times(0)).postDsl(eq("bambooServerValue"), startsWith("templateBody"), eq("shortcutsBody"), eq("usernameValue"), eq("passwordValue"));
        Assert.assertEquals(TaskState.FAILED, taskResult.getTaskState());
    }


    @Test
    public void taskExecutionReturnsSuccessUsingBambooVariableAndCallTheRestEndpoint() throws Exception{
        planTemplateExecuteTask.setFileResourceToString(fileResourceToString);
        planTemplateExecuteTask.setHttpPostRequest(httpPostRequest);

        when(fileResourceToString.convert(any(File.class), eq("templateValue"))).thenReturn("templateBody");
        when(fileResourceToString.convert(any(File.class), eq("shortcutsValue"))).thenReturn("shortcutsBody");
        TaskContext taskContext = mock(TaskContext.class, Mockito.RETURNS_DEEP_STUBS);

        ConfigurationMap configurationMap = mock(ConfigurationMap.class);
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);
        when(configurationMap.get("template")).thenReturn("templateValue");
        when(configurationMap.get("shortcuts")).thenReturn("shortcutsValue");
        when(configurationMap.get("bambooServer")).thenReturn("bambooServerValue");
        when(configurationMap.get("username")).thenReturn("usernameValue");
        when(configurationMap.get("password")).thenReturn("passwordValue");
        when(configurationMap.get("passwordVariableCheck")).thenReturn("true");
        when(configurationMap.get("passwordVariable")).thenReturn("passwordVariableValue");
        when(httpPostRequest.expandDsl(eq("bambooServerValue"), startsWith("templateBody"), eq("shortcutsBody"), eq("usernameValue"), eq("passwordVariableValue"))).thenReturn("TEST");
        when(httpPostRequest.expandDsl(eq("bambooServerValue"), startsWith("templateBody"), eq("shortcutsBody"), eq("usernameValue"), eq("passwordValue"))).thenReturn("FAIL");

        TaskResult taskResult = planTemplateExecuteTask.execute(taskContext);

        verify(httpPostRequest, times(1)).postDsl(eq("bambooServerValue"), startsWith("templateBody"), eq("shortcutsBody"), eq("usernameValue"), eq("passwordVariableValue"));
        Assert.assertEquals(TaskState.SUCCESS, taskResult.getTaskState());
    }


    @Test
    public void taskExecutionAddsDeployerSectionToTheTemplate() throws Exception{
        planTemplateExecuteTask.setFileResourceToString(fileResourceToString);
        planTemplateExecuteTask.setHttpPostRequest(httpPostRequest);
        
        when(fileResourceToString.convert(any(File.class), anyString())).thenReturn("templateBody");
        TaskContext taskContext = mock(TaskContext.class, Mockito.RETURNS_DEEP_STUBS);
        
        when(taskContext.getBuildContext().getPlanKey()).thenReturn("PROJECT-PLAN");
        
        
        ConfigurationMap configurationMap = mock(ConfigurationMap.class);
        when(configurationMap.get("template")).thenReturn("templateValue");
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);
        when(httpPostRequest.expandDsl(anyString(), anyString(), anyString(), anyString(), anyString())).thenReturn("blah");

        planTemplateExecuteTask.execute(taskContext);
        
        String templateBodyWithDeployerSection = "templateBody\ndeployer(planKey:'PROJECT-PLAN')";
        verify(httpPostRequest, times(1)).postDsl(anyString(), eq(templateBodyWithDeployerSection), anyString(), anyString(), anyString());
    }

    @Test
    public void taskExecutionWithStrategyValidateOnly() throws Exception
    {
        planTemplateExecuteTask.setFileResourceToString(fileResourceToString);
        planTemplateExecuteTask.setHttpPostRequest(httpPostRequest);

        when(fileResourceToString.convert(any(File.class), anyString())).thenReturn("templateBody");
        TaskContext taskContext = mock(TaskContext.class, Mockito.RETURNS_DEEP_STUBS);
        when(taskContext.getBuildContext().getPlanKey()).thenReturn("PROJECT-PLAN");
        ConfigurationMap configurationMap = mock(ConfigurationMap.class);
        when(configurationMap.get("template")).thenReturn("templateValue");
        when(configurationMap.get("executionStrategy")).thenReturn(PlanTemplateExecuteTaskConfigurator.VALIDATE_ONLY);
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);
        when(taskContext.getBuildContext().isBranch()).thenReturn(false);
        when(httpPostRequest.expandDsl(anyString(), anyString(), anyString(), anyString(), anyString())).thenReturn("test return");

        planTemplateExecuteTask.execute(taskContext);

        String templateBodyWithDeployerSection = "templateBody\ndeployer(planKey:'PROJECT-PLAN')";
        verify(httpPostRequest, times(1)).expandDsl(anyString(), eq(templateBodyWithDeployerSection), anyString(), anyString(), anyString());
        verify(httpPostRequest, times(0)).postDsl(anyString(), anyString(), anyString(), anyString(), anyString());
    }

    @Test
    public void testExecutionWithStrategyRunOnMaster() throws Exception
    {
        planTemplateExecuteTask.setFileResourceToString(fileResourceToString);
        planTemplateExecuteTask.setHttpPostRequest(httpPostRequest);

        when(fileResourceToString.convert(any(File.class), anyString())).thenReturn("templateBody");
        TaskContext taskContext = mock(TaskContext.class, Mockito.RETURNS_DEEP_STUBS);
        when(taskContext.getBuildContext().getPlanKey()).thenReturn("PROJECT-PLAN");
        when(taskContext.getBuildContext().isBranch()).thenReturn(false);
        ConfigurationMap configurationMap = mock(ConfigurationMap.class);
        when(configurationMap.get("template")).thenReturn("templateValue");
        when(configurationMap.get("executionStrategy")).thenReturn("executionStrategy.executeOnMaster");
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);

        when(httpPostRequest.expandDsl(anyString(), anyString(), anyString(), anyString(), anyString())).thenReturn("test return");

        planTemplateExecuteTask.execute(taskContext);

        String templateBodyWithDeployerSection = "templateBody\ndeployer(planKey:'PROJECT-PLAN')";
        verify(httpPostRequest, times(1)).expandDsl(anyString(), eq(templateBodyWithDeployerSection), anyString(), anyString(), anyString());
        verify(httpPostRequest, times(1)).postDsl(anyString(), eq(templateBodyWithDeployerSection), anyString(), anyString(), anyString());

    }

    @Test
    public void testBranchExecutionWithStrategyRunOnMaster() throws Exception
    {
        planTemplateExecuteTask.setFileResourceToString(fileResourceToString);
        planTemplateExecuteTask.setHttpPostRequest(httpPostRequest);

        when(fileResourceToString.convert(any(File.class), anyString())).thenReturn("templateBody");
        TaskContext taskContext = mock(TaskContext.class, Mockito.RETURNS_DEEP_STUBS);
        when(taskContext.getBuildContext().getPlanKey()).thenReturn("PROJECT-PLAN");
        when(taskContext.getBuildContext().isBranch()).thenReturn(true);
        ConfigurationMap configurationMap = mock(ConfigurationMap.class);
        when(configurationMap.get("template")).thenReturn("templateValue");
        when(configurationMap.get("executionStrategy")).thenReturn("executionStrategy.executeOnMaster");
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);

        when(httpPostRequest.expandDsl(anyString(), anyString(), anyString(), anyString(), anyString())).thenReturn("test return");

        planTemplateExecuteTask.execute(taskContext);

        String templateBodyWithDeployerSection = "templateBody\ndeployer(planKey:'PROJECT-PLAN')";
        verify(httpPostRequest, times(1)).expandDsl(anyString(), eq(templateBodyWithDeployerSection), anyString(), anyString(), anyString());
        verify(httpPostRequest, times(0)).postDsl(anyString(), eq(templateBodyWithDeployerSection), anyString(), anyString(), anyString());

    }

    @Test
    public void testBranchExecutionWithStrategyRunOnMasterAndBranches() throws Exception
    {
        planTemplateExecuteTask.setFileResourceToString(fileResourceToString);
        planTemplateExecuteTask.setHttpPostRequest(httpPostRequest);

        when(fileResourceToString.convert(any(File.class), anyString())).thenReturn("templateBody");
        TaskContext taskContext = mock(TaskContext.class, Mockito.RETURNS_DEEP_STUBS);
        when(taskContext.getBuildContext().getPlanKey()).thenReturn("PROJECT-PLAN");
        when(taskContext.getBuildContext().isBranch()).thenReturn(true);
        ConfigurationMap configurationMap = mock(ConfigurationMap.class);
        when(configurationMap.get("template")).thenReturn("templateValue");
        when(configurationMap.get("executionStrategy")).thenReturn("executionStrategy.executeOnMasterAndBranches");
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);

        when(httpPostRequest.expandDsl(anyString(), anyString(), anyString(), anyString(), anyString())).thenReturn("test return");

        planTemplateExecuteTask.execute(taskContext);

        String templateBodyWithDeployerSection = "templateBody\ndeployer(planKey:'PROJECT-PLAN')";
        verify(httpPostRequest, times(1)).expandDsl(anyString(), eq(templateBodyWithDeployerSection), anyString(), anyString(), anyString());
        verify(httpPostRequest, times(1)).postDsl(anyString(), eq(templateBodyWithDeployerSection), anyString(), anyString(), anyString());

    }
    
    
}
