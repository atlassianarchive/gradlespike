package com.atlassian.bamboo.plugin.plantemplates.support.tasks;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class NodeJsTaskMappingTest {

    @InjectMocks
    private NodeJsTaskMapping nodeJsTaskMapping;

    @Test
    @SuppressWarnings("rawtypes")
    public void convertATaskReturnsCorrectMap(){
        Map task = ImmutableMap.builder()
                .put("type", "nodejs")
                .put("description", "new nodejs task")
                .put("executable", "Node.js")
                .put("script", "server.js")
                .put("workingSubDirectory", "./newSub")
                .put("arguments", "argument1")
                .build();

        Map<String,String> taskActionParams = nodeJsTaskMapping.convert(task);

        Map<String, String> expected = ImmutableMap.<String, String>builder()
                .put("userDescription", "new nodejs task")
                .put("taskDisabled", "false")
                .put("command", "server.js")
                .put("runtime","Node.js")
                .put("workingSubDirectory", "./newSub")
                .put("createTaskKey", "com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.builder.node")
                .put("arguments", "argument1")
                .build();

        Assert.assertEquals(expected, taskActionParams);
    }



}
