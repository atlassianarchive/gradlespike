package com.atlassian.bamboo.plugin.plantemplates;import com.google.common.base.Charsets
import com.google.common.collect.ImmutableMap
import com.google.common.io.Resources

import org.junit.Test

import org.junit.Assert;


public class PlanBuilderValidatorTest extends GroovyTestCase
{
    private static String getResourceAsString(String template) throws IOException
    {
        return Resources.toString(Resources.getResource(template), Charsets.UTF_8);
    }

    @Test
    public void testValidateWithoutRulesReturnsEmptyErrors() throws Exception
    {
        String template = """
            plan(key: "key")
        """
        DslEvaluator evaluator = DslEvaluator.builder().build();
        List<String> errors = evaluator.validate(template).get("errors");
        assertNotNull(errors);

        assert [] == errors
    }
    
    
    @Test
    public void testValidateShouldReturnAnErrorOnAttributeRequired() throws Exception
    {
        String template = """
            plan(key: "key1", description: 'description1'){
                stage(name: 'name1')
                stage(key: 'fail')
            }
            plan(key: "key2")
        """
        List<Map> rules = [
                [node: 'plan', required: ['key', 'description']],
                [node: 'stage', required: ['name']]
            ]
        
        DslEvaluator evaluator = DslEvaluator.builder().withValidationRules(rules).build();
        List<String> errors = evaluator.validate(template).get("errors");
        assertNotNull(errors);

        println errors
        List<String> errorsExpected = [
            "'stage' required a non empty attribute or child node 'name' for: stage(key:'fail')",
            "'plan' required a non empty attribute or child node 'description' for: plan(key:'key2')",
            ]
        assert errorsExpected == errors
    }
    
    
    @Test
    public void testValidateShouldReturnNoErrorsWhenYouUseAShortcut() throws Exception
    {
        String template = """
            plan(key: "key1", description: 'description1'){
                stage(name: 'name1')
                defaultVariables()
            }
            plan(key: "key2", description: 'description2')
        """
        List<Map> rules = [
                [node: 'plan', required: ['key', 'description']],
                [node: 'stage', required: ['name']]
            ]
        
        DslEvaluator evaluator = DslEvaluator.builder().withValidationRules(rules).build();
        List<String> errors = evaluator.validate(template).get("errors");
        assertNotNull(errors);

        println errors
        List<String> errorsExpected = [
            ]
        assert errorsExpected == errors
    }

    
    @Test
    public void testValidateShouldReturnAnErrorWhenTheParentIsNotSet() throws Exception
    {
        String template = """
            plan(key: "key1", description: 'description1'){
                stage(name: 'name1')
            }
            plan(key: "key2")
            stage(key: 'fail')
        """
        List<Map> rules = [
                [node: 'stage', parent: 'plan']
            ]
        
        DslEvaluator evaluator = DslEvaluator.builder().withValidationRules(rules).build();
        List<String> errors = evaluator.validate(template).get("errors");
        assertNotNull(errors);

        List<String> errorsExpected = [
            "'stage' need to be a child node of 'plan' for: stage(key:'fail')",
            ]
        
        assert errorsExpected == errors
    }
    
    
    @Test
    public void testValidateShouldReturnAnErrorWhenTheParentIsWrong() throws Exception
    {
        String template = """
            job(key: "key1", description: 'description1'){
                stage(key:'fail')
            }
        """
        List<Map> rules = [
                [node: 'stage', parent: 'plan']
            ]
        
        DslEvaluator evaluator = DslEvaluator.builder().withValidationRules(rules).build();
        List<String> errors = evaluator.validate(template).get("errors");
        assertNotNull(errors);

        List<String> errorsExpected = [
            "'stage' need to be a child node of 'plan' for: stage(key:'fail')",
            ]
        
        assert errorsExpected == errors
    }
    
    @Test
    public void testValidateShouldReturnNoErrorsForMultipleParentRule() throws Exception
    {
        String template = """
            job(key: "key1", description: 'description1'){
                task(type:'type1')
            }
            environment(key: "key2") {
                task(type:'type1')
            }
        """
        List<Map> rules = [
                [node: 'task', parent: ['job', 'environment'] ]
            ]
        
        DslEvaluator evaluator = DslEvaluator.builder().withValidationRules(rules).build();
        List<String> errors = evaluator.validate(template).get("errors");
        assertNotNull(errors);
        
        assert [] == errors
    }
    
    @Test
    public void testValidateShouldReturnAnErrorWhenTheRootIsWrong() throws Exception
    {
        String template = """
            plan(key: "key1", description: 'description1'){
                plan(key:'fail')
            }
        """
        List<Map> rules = [
                [node: 'plan', root: true]
            ]
        
        DslEvaluator evaluator = DslEvaluator.builder().withValidationRules(rules).build();
        List<String> errors = evaluator.validate(template).get("errors");
        assertNotNull(errors);

        List<String> errorsExpected = [
            "'plan' need to be a root node for: plan(key:'fail')",
            ]
        
        assert errorsExpected == errors
    }
    
    
    @Test
    public void testValidateShouldReturnAnErrorWhenAttributeIsNotInAValueList() throws Exception
    {
        String template = """
            plan(key: "key1", description: 'description1'){
                task(type:'one')
                task(type:'two')
                task(type:'fail')
                task(ok: 'ok')
            }
        """
        List<Map> rules = [
                [node: 'task', allowValues: ["type": ["one", "two"] ] ]
            ]
        
    
        DslEvaluator evaluator = DslEvaluator.builder().withValidationRules(rules).build();
        List<String> errors = evaluator.validate(template).get("errors");
        assertNotNull(errors);

        List<String> errorsExpected = [
            "'task:type' need to be one of these [one, two] for: task(type:'fail')",
            ]
        
        assert errorsExpected == errors
    }
    
    
    @Test
    public void testValidateShouldReturnAnErrorWhenAnAttributeItsNotAllowed() throws Exception
    {
        String template = """
            plan(key: "key1", description: 'description1'){
                task(name: 'task1', description: 'blabla') {
                    hello(name: "hello")
                }
                task(name: 'task2', fail: 'value')
            }
        """
        List<Map> rules = [
                [node: 'task', allowAttributes: ["name", "description"] ]
            ]
        
        DslEvaluator evaluator = DslEvaluator.builder().withValidationRules(rules).build();
        List<String> errors = evaluator.validate(template).get("errors");
        assertNotNull(errors);

        List<String> errorsExpected = [
            "'task:fail' is not an allowed attribute. Possible attributes are [name, description] for: task(name:'task2', fail:'value')",
            ]
        
        assert errorsExpected == errors
    }

    @Test
    public void testValidateShouldReturnAnErrorWhenAnAttributeDoesNotMatchPattern() throws Exception
    {
        String template = """
            plan(key:'key1 a', description:'description1')
        """
        List<Map> rules = [
                [node: 'plan', attributeMatches: [key:/[^ ]*/] ]
        ]

        DslEvaluator evaluator = DslEvaluator.builder().withValidationRules(rules).build();
        List<String> errors = evaluator.validate(template).get("errors");
        assertNotNull(errors);

        List<String> errorsExpected = [
                "'plan:key' did not match the pattern of allowed values. The value must match the pattern [^ ]* for: plan(key:'key1 a', description:'description1')",
        ]

        assert errorsExpected == errors
    }

    @Test
    public void testValidateGivesWarningIfARuleIsMissing() throws Exception
    {
        String template = """
            plan(key: "key"){
               stage(name: "name") 
            }
        """

        List<Map> rules = [
                [node: 'other', attributeMatches: [key:/[^ ]*/] ]
        ]


        DslEvaluator evaluator = DslEvaluator.builder().withValidationRules(rules).build();
        def validation = evaluator.validate(template)
        List<String> errors = validation.get("errors");
        List<String> warnings = validation.get("warnings");

        assert ["There is no rule defined for node(s) [stage]. They will probably be ignored.", 
            "There is no rule defined for node(s) [plan]. They will probably be ignored."] == warnings
        assert [] == errors
    }


    @Test
    public void testValidateShouldReturnAnErrorOnChildNodeRequired() throws Exception
    {
        String template = """
            plan(key: "key1")
        """
        List<Map> rules = [
                [node: 'plan', required: ['key', 'stage']]
        ]

        DslEvaluator evaluator = DslEvaluator.builder().withValidationRules(rules).build();
        List<String> errors = evaluator.validate(template).get("errors");
        assertNotNull(errors);

        println errors
        List<String> errorsExpected = [
                "'plan' required a non empty attribute or child node 'stage' for: plan(key:'key1')",
        ]
        assert errorsExpected == errors
    }
    
    
    @Test
    public void testConditionalAllowAttributesGivesAnErrorWhenTheAttributeIsNotPresent() throws Exception
    {
        String template = """
            task(type: "script", argument:"ok")
            task(type: "maven",  argument:"ok", executable: "mvn")
        """
        List<Map> rules = [
                [node: 'task',                     
                    conditionalAllow: [ 
                        [discriminator: "type", value:'maven', attributes: ['executable', 'type']],
                        [discriminator: "type", value:'script', attributes: ['argument', 'type']]
                    ]
                ]
        ]


        DslEvaluator evaluator = DslEvaluator.builder().withValidationRules(rules).build();
        List<String> errors = evaluator.validate(template).get("errors");
        assertNotNull(errors);

        println errors
        List<String> errorsExpected = [
                "'task:argument' is not an allowed attribute. Possible conditional attributes are [executable, type] for: task(type:'maven', argument:'ok', executable:'mvn')"
        ]
        assert errorsExpected == errors
    }



    @Test
    public void testConditionalAllowAttributesIfTheDiscriminatorDoesntMatchAnyValue() throws Exception
    {
        String template = """
            task(type: "script", argument:"ok")
            task(type: "maven",  executable: "mvn")
            task(type: "other")
        """
        List<Map> rules = [
                [node: 'task',
                        conditionalAllow: [
                                [discriminator: "type", value:'maven', attributes: ['executable', 'type']],
                                [discriminator: "type", value:'script', attributes: ['argument', 'type']]
                        ]
                ]
        ]


        DslEvaluator evaluator = DslEvaluator.builder().withValidationRules(rules).build();
        List<String> errors = evaluator.validate(template).get("errors");

        assert errors == ["'task:type' is not an allowed attribute. Possible conditional attributes are [] for: task(type:'other')"]
    }



    @Test
    public void testConditionalAllowAttributesWithDifferentDiscriminators() throws Exception
    {
        String template = """
            task(type: "script", argument:"ok")
            task(type: "maven",  argument:"ok", executable: "mvn")
        """
        List<Map> rules = [
                [node: 'task',
                        conditionalAllow: [
                                [discriminator: "type", value:'maven', attributes: ['executable', 'type']],
                                [discriminator: "argument", value:'ok', attributes: ['argument', 'type']]
                        ]
                ]
        ]


        DslEvaluator evaluator = DslEvaluator.builder().withValidationRules(rules).build();
        List<String> errors = evaluator.validate(template).get("errors");
        assert [] == errors
    }
}
