package com.atlassian.bamboo.plugin.plantemplates

import com.google.common.base.Charsets
import com.google.common.io.Resources
import org.junit.Test

class ValidationRulesTest  extends GroovyTestCase{
    private static String getResourceAsString(String template) throws IOException
    {
        return Resources.toString(Resources.getResource(template), Charsets.UTF_8);
    }

    @Test
    public void testBasicRuleWorksFine() throws Exception
    {
        String dsl = """
        plan(key: 'key1', description: 'desc1'){
            plan(key: 'fail')
        }
        """
        DslEvaluator evaluator = DslEvaluator.builder().withValidationRules(new PlanTemplateValidationRules().rules).build();
        
        def result = evaluator.run(dsl);
        
        assert result != null;
        assert result.dslMap != null;
        assert result.errors != null;

        assert result.errors.size > 0
    }
    
    @Test
    public void testBranchMonitoringTemplateIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('branchMonitoringTemplate.groovy'))
    }

    @Test
    public void testJobTemplateIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('jobsTemplate.groovy'))
    }
    
    @Test
    public void testNotificationsTemplateIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('notificationsTemplate.groovy'))
    }
    
    @Test
    public void testStagesTemplateIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('stagesTemplate.groovy'))
    }
    
    @Test
    public void testTriggersTemplateIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('triggersTemplate.groovy'))
    }
    
    
    @Test
    public void testTaskTemplateIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('taskTemplate.groovy'))
    }
    
    @Test
    public void testDeploymentTemplateIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('deploymentTemplate.groovy'))
    }
    
    @Test
    public void testArtifactTemplateIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('artifactTemplate.groovy'))
    }

    @Test
    public void testProvisionUnicornTemplateIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('provisionUnicornTemplate.groovy'))
    }

    @Test
    public void testInstallOnDemandTemplateIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('installOnDemandTemplate.groovy'));
    }

    @Test
    public void testPromoteManifestoTemplateIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('promoteManifestoTemplate.groovy'));
    }

    @Test
    public void testStartUnicornTemplateIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('startUnicornTemplate.groovy'));
    }

    @Test
    public void testStopUnicornIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('stopUnicornTemplate.groovy'));
    }
    
    @Test
    public void testDependenciesTemplateIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('dependenciesTemplate.groovy'));
    }

    @Test
    public void testExecuteOnUnicornIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('executeOnUnicornTemplate.groovy'));
    }

    @Test
    public void testUpdateLicenseIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('updateLicenseTemplate.groovy'));
    }

    @Test
    public void testCopyToUnicornIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('copyToUnicornTemplate.groovy'));
    }

    @Test
    public void testSetupUnicornIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('setupUnicornTemplate.groovy'));
    }

    @Test
    public void testRaiseDeploymentTicketIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('raiseDeploymentTicketTemplate.groovy'))
    }
    
    @Test
    public void testBuildExpirytIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('buildexpiryTemplate.groovy'))
    }

    @Test
    public void testPlanMiscellaneousIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('planMiscellaneousTemplate.groovy'))
    }
    
    @Test
    public void testMiscellaneousJobIsValid() throws Exception
    {
        testTemplateIsValid(getResourceAsString('miscellaneousJobSettingsTemplate.groovy'))
    }

    @Test void testMavenVersionUpdaterTaskIsValud() throws Exception
    {
        testTemplateIsValid(getResourceAsString('mavenVersionUpdaterTemplate.groovy'))
    }

    @Test void testDeployPluginTaskIsValud() throws Exception
    {
        testTemplateIsValid(getResourceAsString('deployPluginTaskTemplate.groovy'))
    }

    private void testTemplateIsValid(String dsl) throws Exception
    {
        DslEvaluator evaluator = DslEvaluator.builder().withValidationRules(new PlanTemplateValidationRules().rules).build();
        
        def result = evaluator.run(dsl);
        
        assert result != null;
        assert result.dslMap != null;
        assert result.errors != null;
        assert result.errors == []
    }
    
    
    
    @Test
    public void testATemplateWithShortcutsRunsTheValidationOnTheExpandedVersion() throws Exception
    {
        String dsl = """
            defaultPlan()
        """
        String shortcuts = """
            defaultPlan() {
                plan(fail: 'fail')
            }
        """
        
        ShortcutEvaluator shortcutBuilder = new ShortcutEvaluator()
        List<TemplateShortcut> shorcutsList = shortcutBuilder.parseShortcuts(shortcuts)

        DslEvaluator evaluator = DslEvaluator.builder().withShortcuts(shorcutsList).withValidationRules(new PlanTemplateValidationRules().rules).build();
        
        def result = evaluator.run(dsl);
        
        assert result != null;
        assert result.dslMap != null;
        assert result.errors != null;
        assert result.errors == [ "'plan' required a non empty attribute or child node 'key' for: plan(fail:'fail')",
                "'plan' required a non empty attribute or child node 'project' for: plan(fail:'fail')",
                "'plan' required a non empty attribute or child node 'stage' for: plan(fail:'fail')",
                "'plan:fail' is not an allowed attribute. Possible attributes are [key, name, description, enabled] for: plan(fail:'fail')"]
    }

    @Test
    public void testParamsOnNestedShortcuts() throws Exception
    {
        String dsl = getResourceAsString('dslDefinitions/complexNested.groovy')
        String shortcutDsl = getResourceAsString('shortcutDefinitions/complexNestedShortcuts.groovy')
        ShortcutEvaluator shortcutBuilder = new ShortcutEvaluator()
        List<TemplateShortcut> shortcutsList = shortcutBuilder.parseShortcuts(shortcutDsl)

        DslEvaluator evaluator = DslEvaluator.builder().withShortcuts(shortcutsList).withValidationRules(new PlanTemplateValidationRules().rules).build();

        def result = evaluator.run(dsl);

        assert result != null;
        assert result.dslMap != null;
        assert result.errors != null;
        assert result.errors == []
    }

    @Test
    public void testNodejsTaskIsValidated() throws Exception
    {
        String dsl = getResourceAsString('dslDefinitions/nodejsTaskTemplate.groovy')
        List<TemplateShortcut> shortcutsList = Collections.emptyList();

        DslEvaluator evaluator = DslEvaluator.builder().withShortcuts(shortcutsList).withValidationRules(new PlanTemplateValidationRules().rules).build();

        def result = evaluator.run(dsl);

        assert result != null;
        assert result.dslMap != null;
        assert result.errors != null;
        assert result.errors == []
    }



}
