package com.atlassian.bamboo.plugin.plantemplates

import com.google.common.io.Resources
import org.apache.commons.compress.utils.Charsets
import org.junit.Test

class ShortcutBuilderDslTest extends GroovyTestCase{
    private static String getResourceAsString(String template) throws IOException
    {
        return Resources.toString(Resources.getResource(template), Charsets.UTF_8);
    }

    @Test
    public void testShortcutReturnsExpectedShortcutList() throws Exception
    {
        String shortcutDsl = getResourceAsString("shortcutDefinitions/minimalShortcut.groovy");
        ShortcutEvaluator evaluator = new ShortcutEvaluator();
        
        List<TemplateShortcut> result = evaluator.parseShortcuts(shortcutDsl);

        assert result != null;
        List<TemplateShortcut> expected = [new TemplateShortcut("minimalShortcut",'mytag(key:\'ami\',value:\'ami-7aa60314\',something:\'someValue\')\n', null)]
        assert expected == result
    }
    
    @Test
    public void testShortcutWithUnquotedAttributeRaiseAnException() throws Exception
    {
        String shortcutDsl = getResourceAsString("shortcutDefinitions/syntaxErrorShortcut.groovy");
        ShortcutEvaluator evaluator = new ShortcutEvaluator();
        
        def message = shouldFail(IllegalArgumentException){
            List<TemplateShortcut> result = evaluator.parseShortcuts(shortcutDsl);
        }
        
        assert message == "Inside shortcut definition: 'defaultVariables', error while processing node: 'planVariable', found attribute: 'key' with non-string value";
    }
    
    @Test
    public void testEmptyShortcutDefinitionRaiseAnExceptionWithCorrectEmptyBodyMessage() throws Exception
    {
        String shortcutDsl = '''emptyShortcut(){}''';
        ShortcutEvaluator evaluator = new ShortcutEvaluator();
        
        def message = shouldFail(IllegalArgumentException){
            List<TemplateShortcut> result = evaluator.parseShortcuts(shortcutDsl);
        }
        
        assert message == "Inside shortcut definition: 'emptyShortcut', shortcut has no body"
    }

    @Test
    public void testShortcutWithRegexCharactersIsEscaped() {
        String shortcutDsl = '''
        awesome(['param']) {
            variable(key:'key', value:'#param')
        }
        '''
        ShortcutEvaluator evaluator = new ShortcutEvaluator();

        List<TemplateShortcut> result = evaluator.parseShortcuts(shortcutDsl);

        assert result != null;
        assert result.size() == 1;
        TemplateShortcut created = result[0];

        assert [variable:[key:'key', value:'${hello}']] == created.apply([param:'${hello}'], [])


    }

    @Test
    public void testShortcutWithMultiLineString() {
        String shortcutDsl = '''
        awesome() {
            variable(key:'key', value:"""
            hello
            """
            )
        }
        '''
        ShortcutEvaluator evaluator = new ShortcutEvaluator();

        List<TemplateShortcut> result = evaluator.parseShortcuts(shortcutDsl);

        assert result != null;
        assert result.size() == 1;
        TemplateShortcut created = result[0];
        def value = """
            hello
            """

        assert [variable:[key:'key', value:value]] == created.apply([param:'${hello}'], [])


    }
}