package com.atlassian.bamboo.plugin.plantemplates;

import com.google.common.base.Charsets
import com.google.common.collect.ImmutableMap
import com.google.common.collect.Lists
import com.google.common.io.Resources

import org.junit.Test

import static junit.framework.Assert.assertEquals
import static junit.framework.Assert.assertNotNull


public class PlanBuilderDslTest extends GroovyTestCase
{
    private static String getResourceAsString(String template) throws IOException
    {
        return Resources.toString(Resources.getResource(template), Charsets.UTF_8);
    }

    @Test
    public void testMinimalTemplateReturnsExpectedMapData() throws Exception
    {
        String template = getResourceAsString("dslDefinitions/minimalTemplate.groovy");
        DslEvaluator evaluator = DslEvaluator.builder().build();
        Map result = evaluator.evaluate(template);
        assertNotNull(result);

        Map expected = ImmutableMap.builder()
                .put("plan", ImmutableMap.builder()
                        .put("key", "TEMPLATETEST")
                        .put("name", "Fedora15 OpenLDAP")
                        .put("description", "A sensible description goes here...")
                        .put("project", ImmutableMap.builder()
                                .put("key", "PERSONAL")
                                .put("name", "Z Personal")
                                .put("description", "a project description").build())
                        .build())
                .build();

        assert expected == result
    }

    @Test
    public void testTemplateWithVariablesReturnsThemInAnArray() throws IOException
    {
        String template = getResourceAsString("dslDefinitions/templateWithVariables.groovy");
        DslEvaluator evaluator = DslEvaluator.builder().build();
        Map<String, Map> result = evaluator.evaluate(template);
        assertNotNull(result);
 
        List expected = Lists.newArrayList(
                ImmutableMap.of("key", "ami", "value", "ami-7aa60314"),
                ImmutableMap.of("key", "ec2.config","value","fedora15x64ldap.py"),
                ImmutableMap.of("key", "ec2-user","value","ec2-user")
        );

        assert expected == result.get("plan").get("planVariable")
    }

    @Test
    public void testPlanVariableShortcutCorrectlyPopulatesMap() throws IOException
    {
        String shortcutTemplate = getResourceAsString("dslDefinitions/shortcutTemplateWithVariables.groovy");
        String fullTemplate = getResourceAsString("dslDefinitions/templateWithVariables.groovy");
        
        //setup the shortcuts
        
        //defaultVariables
        def shortcuts = []
        def defaultVariables = """
            planVariable(key:'ami', value:'ami-7aa60314')
            planVariable(key:'ec2.config', value:'fedora15x64ldap.py')
            planVariable(key:'ec2-user', value: 'ec2-user')
        """
        shortcuts.add(new TemplateShortcut("defaultVariables", defaultVariables, []))

        DslEvaluator planBuilderEval = DslEvaluator.builder().withShortcuts(shortcuts).build();
        
        Map expected = planBuilderEval.evaluate(fullTemplate);
        Map current = planBuilderEval.evaluate(shortcutTemplate);

        assert expected == current
    }

    @Test
    public void testShortCutsCanBeLoadedFromAFile() throws IOException
    {
        String shortcutTemplate = getResourceAsString("dslDefinitions/shortcutTemplateWithVariables.groovy");
        String fullTemplate = getResourceAsString("dslDefinitions/templateWithVariables.groovy");

        //load the shortcuts
        String shortcutFileContents = getResourceAsString("shortcutDefinitions/shortcuts.groovy")

        //Parse and build the shortcut objects
        ShortcutEvaluator shortcutBuilderEvaluator = new ShortcutEvaluator()
        List<TemplateShortcut> shortcuts = shortcutBuilderEvaluator.parseShortcuts(shortcutFileContents)

        DslEvaluator planBuilderEval = DslEvaluator.builder().withShortcuts(shortcuts).build();

        Map expected = planBuilderEval.evaluate(fullTemplate);
        Map current = planBuilderEval.evaluate(shortcutTemplate);

        assert expected == current
    }

    @Test
    public void testNestedShortcutsInTheShortcutDefinitionCorrectlyPopulatesMap() throws IOException
    {
        String shortcutTemplate = getResourceAsString("dslDefinitions/nestedShorcutsTemplate.groovy");
        String fullTemplate = getResourceAsString("dslDefinitions/nestedShorcutsExpandedTemplate.groovy");

        //load the shortcuts
        String shortcutFileContents = getResourceAsString("shortcutDefinitions/shortcuts.groovy")

        //Parse and build the shortcut objects
        ShortcutEvaluator shortcutBuilderEvaluator = new ShortcutEvaluator()
        List<TemplateShortcut> shortcuts = shortcutBuilderEvaluator.parseShortcuts(shortcutFileContents)

        DslEvaluator planBuilderEvaluator = DslEvaluator.builder().withShortcuts(shortcuts).build();
        
        Map expected = planBuilderEvaluator.evaluate(fullTemplate);
        Map current = planBuilderEvaluator.evaluate(shortcutTemplate);

        assert expected == current
    }

    @Test
    public void testShortcutsWithArgumentsCorrectlySubstitutesValues() throws IOException
    {
        String shortcutTemplate = getResourceAsString("dslDefinitions/shortcutWithArgumentsTemplate.groovy");
        String fullTemplate = getResourceAsString("dslDefinitions/shortcutWithArgumentsExpanded.groovy");

        //load the shortcuts
        String shortcutFileContents = getResourceAsString("shortcutDefinitions/shortcuts.groovy")

        //Parse and build the shortcut objects
        ShortcutEvaluator shortcutBuilderEvaluator = new ShortcutEvaluator()
        List<TemplateShortcut> shortcuts = shortcutBuilderEvaluator.parseShortcuts(shortcutFileContents)

        DslEvaluator planBuilderEvaluator = DslEvaluator.builder().withShortcuts(shortcuts).build();

        Map expected = planBuilderEvaluator.evaluate(fullTemplate);
        Map current = planBuilderEvaluator.evaluate(shortcutTemplate);

        assert expected == current
    }

    @Test
    public void testShortcutsWithoutAllArgsThrowsException() throws IOException
    {
        String shortcutTemplate = getResourceAsString("dslDefinitions/shortcutWithoutAllArgsTemplate.groovy");

        //load the shortcuts
        String shortcutFileContents = getResourceAsString("shortcutDefinitions/shortcuts.groovy")

        //Parse and build the shortcut objects
        ShortcutEvaluator shortcutBuilderEvaluator = new ShortcutEvaluator()
        List<TemplateShortcut> shortcuts = shortcutBuilderEvaluator.parseShortcuts(shortcutFileContents)

        DslEvaluator planBuilderEvaluator = DslEvaluator.builder().withShortcuts(shortcuts).build();

        shouldFail(IllegalArgumentException) {
            planBuilderEvaluator.evaluate(shortcutTemplate);
        }
    }

    @Test
    public void testTemplatePrinterCorrectlyPrintsTemplate() throws IOException
    {
        String shortcutTemplate = getResourceAsString("dslDefinitions/shortcutWithArgumentsTemplate.groovy");
        String fullTemplate = getResourceAsString("dslDefinitions/shortcutWithArgumentsExpanded.groovy");

        //load the shortcuts
        String shortcutFileContents = getResourceAsString("shortcutDefinitions/shortcuts.groovy")

        //Parse and build the shortcut objects
        ShortcutEvaluator shortcutBuilderEvaluator = new ShortcutEvaluator()
        List<TemplateShortcut> shortcuts = shortcutBuilderEvaluator.parseShortcuts(shortcutFileContents)

        DslEvaluator planBuilderEvaluator = DslEvaluator.builder().withShortcuts(shortcuts).build();

        Map expected = planBuilderEvaluator.evaluate(fullTemplate);
        Map current = planBuilderEvaluator.evaluate(shortcutTemplate);

        TemplatePrinter printer = new TemplatePrinter()
        String printedDsl = printer.buildDslFromMap(expected)
        String printedShortcutDsl = printer.buildDslFromMap(current)

        assertEquals(fullTemplate, printedDsl)
        assertEquals(fullTemplate, printedShortcutDsl)
    }
    
    @Test
    public void testSyntaxErrorNotUsingQuotesForAnAttribute() throws IOException
    {
        String fullTemplate = getResourceAsString("dslDefinitions/simpleTemplateWithSyntaxError.groovy");

        DslEvaluator planBuilderEvaluator = DslEvaluator.builder().build();
        
        def message = shouldFail(IllegalArgumentException) {
            Map expected = planBuilderEvaluator.evaluate(fullTemplate);
            print expected
        }
        
        assert message.startsWith("Error while processing node: 'plan', found attribute: 'key' with non-string value") == true
    }
    
    @Test
    public void testNestedShortcutsWithSiblings() throws IOException
    {
        String shortcutTemplate = getResourceAsString("dslDefinitions/nestedSiblingShortcutsTemplate.groovy");
        String fullTemplate = getResourceAsString("dslDefinitions/nestedSiblingShortcutsExpandedTemplate.groovy");

        //load the shortcuts
        String shortcutFileContents = getResourceAsString("shortcutDefinitions/nestedSiblingShortcuts.groovy")

        //Parse and build the shortcut objects
        ShortcutEvaluator shortcutBuilderEvaluator = new ShortcutEvaluator()
        List<TemplateShortcut> shortcuts = shortcutBuilderEvaluator.parseShortcuts(shortcutFileContents)

        DslEvaluator planBuilderEvaluator = DslEvaluator.builder().withShortcuts(shortcuts).build();
        
        Map result = planBuilderEvaluator.evaluate(shortcutTemplate)
        Map expected = planBuilderEvaluator.evaluate(fullTemplate)
        
        assert expected == result
    }

    @Test
    public void testNestedShortcutsWithSiblingsAndRepetition() throws IOException
    {
        String shortcutTemplate = getResourceAsString("dslDefinitions/nestedSiblingShortcutsRepetitionTemplate.groovy");
        String fullTemplate = getResourceAsString("dslDefinitions/nestedSiblingShortcutsRepetitionExpandedTemplate.groovy");

        //load the shortcuts
        String shortcutFileContents = getResourceAsString("shortcutDefinitions/nestedSiblingShortcutsRepetition.groovy")

        //Parse and build the shortcut objects
        ShortcutEvaluator shortcutBuilderEvaluator = new ShortcutEvaluator()
        List<TemplateShortcut> shortcuts = shortcutBuilderEvaluator.parseShortcuts(shortcutFileContents)

        DslEvaluator planBuilderEvaluator = DslEvaluator.builder().withShortcuts(shortcuts).build();

        Map result = planBuilderEvaluator.evaluate(shortcutTemplate)
        Map expected = planBuilderEvaluator.evaluate(fullTemplate)

        assert expected == result
    }
    
    
    @Test
    public void testDeploymentsTemplateCorrectlyPopulatesMap() throws IOException
    {
        String shortcutTemplate = getResourceAsString("dslDefinitions/deploymentsPluginTemplate.groovy");
        String fullTemplate = getResourceAsString("dslDefinitions/deploymentsPluginExpandedTemplate.groovy");
        
        //load the shortcuts
        String shortcutFileContents = getResourceAsString("shortcutDefinitions/deploymentsShortcuts.groovy")

        //Parse and build the shortcut objects
        ShortcutEvaluator shortcutBuilderEvaluator = new ShortcutEvaluator()
        List<TemplateShortcut> shortcuts = shortcutBuilderEvaluator.parseShortcuts(shortcutFileContents)

        DslEvaluator planBuilderEvaluator = DslEvaluator.builder().withShortcuts(shortcuts).build();

        Map result = planBuilderEvaluator.evaluate(shortcutTemplate)
        Map expected = planBuilderEvaluator.evaluate(fullTemplate)

        assert expected == result
    }

}
