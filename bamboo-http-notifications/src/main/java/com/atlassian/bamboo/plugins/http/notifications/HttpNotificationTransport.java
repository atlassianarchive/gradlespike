package com.atlassian.bamboo.plugins.http.notifications;

import java.io.IOException;
import java.util.Map;

import com.atlassian.bamboo.notification.Notification;
import com.atlassian.bamboo.notification.NotificationTransport;
import com.atlassian.bamboo.resultsummary.ResultsSummary;

import com.google.common.collect.Maps;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class HttpNotificationTransport  implements NotificationTransport
{

    private static final Logger log = Logger.getLogger(HttpNotificationTransport.class);

    
    private String notificationBaseUrl;
    private ResultsSummary resultsSummary;
    
    public HttpNotificationTransport(String notificationBaseUrl, ResultsSummary resultsSummary)
    {
        this.notificationBaseUrl = notificationBaseUrl;
        this.resultsSummary = resultsSummary;
    }


    @Override
    public void sendNotification(@NotNull Notification notification)
    {
        Map<String, Object> jsonMap = Maps.newHashMap();
        jsonMap.put("IMContent", notification.getIMContent());
        jsonMap.put("description", notification.getDescription());
        
        String status = "unknown";
        if(resultsSummary != null && resultsSummary.getBuildState() != null)
            status = resultsSummary.getBuildState().toString();
        jsonMap.put("status", status);

        String jsonBody = new Gson().toJson(jsonMap);
        
        DefaultHttpClient client = new DefaultHttpClient();
        try
        {
            HttpPost httpPost = new HttpPost(notificationBaseUrl);
            httpPost.setEntity(new StringEntity(jsonBody, "application/json", "UTF-8"));
            HttpResponse response = client.execute(httpPost);
            
            if (response.getStatusLine().getStatusCode() == 200 || response.getStatusLine().getStatusCode() == 201){
            }else{
                log.error("Error " + response.getStatusLine().getStatusCode() + "  " + EntityUtils.toString(response.getEntity()));
            }
        }
        catch (ClientProtocolException e)
        {
            log.error("Error on POST to" + notificationBaseUrl, e);
        }
        catch (IOException e)
        {
            log.error("Error on POST to" + notificationBaseUrl, e);
        }
        finally
        {
            client.getConnectionManager().shutdown();
        }
    }
}
