package com.atlassian.bamboo.plugins.http.notifications;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.notification.NotificationRecipient;
import com.atlassian.bamboo.notification.NotificationTransport;
import com.atlassian.bamboo.notification.recipients.AbstractNotificationRecipient;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.plugin.descriptor.NotificationRecipientModuleDescriptor;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.template.TemplateRenderer;
import com.atlassian.event.Event;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class HttpNotificationRecipient extends AbstractNotificationRecipient
    implements NotificationRecipient.RequiresPlan, NotificationRecipient.RequiresResultSummary,
    NotificationRecipient.RequiresEvent
{
    
    private String notificationBaseUrl;
    
    private TemplateRenderer templateRenderer;

    private Event event;

    private ImmutablePlan plan;

    private ResultsSummary resultsSummary;
    
    public void setTemplateRenderer(TemplateRenderer templateRenderer)
    {
        this.templateRenderer = templateRenderer;
    }
    
    @Override
    public void init(@Nullable String configurationData)
    {
        if (Strings.isNullOrEmpty(configurationData)){
            this.notificationBaseUrl = "http://put_here_your_url";
            return;
        }
        this.notificationBaseUrl = configurationData;
    }
    
    @Override
    public void populate(@NotNull Map<String, String[]> params)
    {        
        this.notificationBaseUrl = params.get("notificationBaseUrl")[0];
    }
    
    @NotNull
    @Override
    public String getRecipientConfig()
    {
        return notificationBaseUrl;
    }
    
    @NotNull
    @Override
    public String getEditHtml()
    {
        String editTemplateLocation = ((NotificationRecipientModuleDescriptor)getModuleDescriptor()).getEditTemplate();
        return templateRenderer.render(editTemplateLocation, populateContext());
    }
    
    @NotNull
    @Override
    public String getViewHtml()
    {
        String editTemplateLocation = ((NotificationRecipientModuleDescriptor)getModuleDescriptor()).getViewTemplate();
        return templateRenderer.render(editTemplateLocation, populateContext());
    }
    
    private Map<String, Object> populateContext()
    {
        Map<String, Object> context = Maps.newHashMap();
        if (notificationBaseUrl != null)
        {
            context.put("notificationBaseUrl", notificationBaseUrl);
        }
        return context;
    }


    @NotNull
    @Override
    public List<NotificationTransport> getTransports()
    {
        List<NotificationTransport> list = Lists.newArrayList();
        list.add(new HttpNotificationTransport(notificationBaseUrl, resultsSummary));
        return list;
    }

    @Override
    public void setEvent(final Event event)
    {
        this.event = event;
    }

    @Override
    public void setPlan(final ImmutablePlan plan)
    {
        this.plan = plan;
    }

    @Override
    public void setResultsSummary(final ResultsSummary resultsSummary)
    {
        this.resultsSummary = resultsSummary;
    }

}
