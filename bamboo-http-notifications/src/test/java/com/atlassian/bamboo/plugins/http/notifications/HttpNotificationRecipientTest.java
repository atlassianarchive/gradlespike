package com.atlassian.bamboo.plugins.http.notifications;


import org.junit.Test;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class HttpNotificationRecipientTest {
    @MockitoAnnotations.Mock
    ImmutablePlan immutablePlan;

    @Test
    public void testSetPlan() {
        HttpNotificationRecipient recipient = new HttpNotificationRecipient();
        recipient.setPlan(immutablePlan);
    }
}
