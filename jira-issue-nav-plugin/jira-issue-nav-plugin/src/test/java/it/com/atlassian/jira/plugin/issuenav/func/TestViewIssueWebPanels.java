package it.com.atlassian.jira.plugin.issuenav.func;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.plugin.issuenav.client.IssueWebPanels;
import com.atlassian.jira.plugin.issuenav.client.IssueWebPanelsClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.List;

import javax.annotation.Nullable;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;

/**
 * Test security around the ViewIssueWebPanels resource
 *
 * @since v5.1
 */
@WebTest ({ Category.FUNC_TEST })
public class TestViewIssueWebPanels extends FuncTestCase
{

    private IssueWebPanelsClient client;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        client = new IssueWebPanelsClient(getEnvironmentData());
        administration.restoreData("TestViewIssueWebPanels.xml");
    }

    @Test
    public void testErrorConditions()
    {
        Response response = client.getResponseForIssue("99999");
        assertEquals(404, response.statusCode);

        response = client.loginAs("fry").getResponseForIssue("10000");
        assertEquals(403, response.statusCode);

        response = client.anonymous().getResponseForIssue("10000");
        assertEquals(401, response.statusCode);
    }

    @Test
    public void testSuccess()
    {
        IssueWebPanels panels = client.getPanelsForIssue("10000");

        final List<String> leftPanelKeys = getPanelKeys(panels.leftPanels);
        assertHasKeys(leftPanelKeys,
                "com.atlassian.jira.jira-view-issue-plugin:details-module",
                "com.atlassian.jira.jira-view-issue-plugin:descriptionmodule",
                "com.atlassian.jira.jira-view-issue-plugin:activitymodule");

        assertThat("Should also include the system's addcommentmodule",
                leftPanelKeys, anyOf(
                hasItem("com.atlassian.jira.plugin.system.comment-panel:addcommentmodule"),
                hasItem("com.atlassian.jira.jira-view-issue-plugin:addcommentmodule") // DESK-2029 - Circular dependency was unravelled and rewritten
        ));

        assertHasKeys(getPanelKeys(panels.rightPanels),
                "com.atlassian.jira.jira-view-issue-plugin:peoplemodule",
                "com.atlassian.jira.jira-view-issue-plugin:datesmodule");

        assertThat(panels.infoPanels, is(Matchers.<IssueWebPanels.IssueWebPanel>emptyIterable()));
    }

    private static List<String> getPanelKeys(List<IssueWebPanels.IssueWebPanel> panels)
    {
        return Lists.transform(panels, new Function<IssueWebPanels.IssueWebPanel, String>()
        {
            @Override
            public String apply(@Nullable final IssueWebPanels.IssueWebPanel input)
            {
                return (null != input) ? input.completeKey : null;
            }
        });
    }

    private static void assertHasKeys(List<String> actualKeys, String... expectedKeys)
    {
        assertThat("We should retrieve as many or more panels than we anticipate",
                actualKeys.size(), greaterThanOrEqualTo(expectedKeys.length));
        assertThat("Every panel we expect should be present in the full set retrieved",
                actualKeys, hasItems(expectedKeys));
    }
}