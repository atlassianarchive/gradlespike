package it.com.atlassian.jira.plugin.issuenav.webdriver.visualregressions;

import com.atlassian.jira.nimblefunctests.annotation.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailComponent;
import org.junit.Before;
import org.junit.Test;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.1
 */
@WebTest({ Category.WEBDRIVER_TEST, Category.VISUAL_REGRESSION })
@Restore("TestInlineEdit.xml")
public class TestInlineEdit extends VisualRegressionTest {
    protected IssueDetailComponent component;

    @Before
    public void openIssue()
    {
        logInToIssuesPageAsSysadmin();
        component = goToIssuePage("XSS-17");
    }

    @Test
    public void testLabels()
    {
        component.labels().switchToEdit();
        assertUIMatches("inline-edit-labels");
    }

    @Test
    public void testAffectsVersion()
    {
        component.affectsVersion().switchToEdit();
        assertUIMatches("inline-edit-affects-version");
    }

    @Test
    public void testDescription()
    {
        component.description().switchToEdit();
        assertUIMatches("inline-edit-description");
    }
}
