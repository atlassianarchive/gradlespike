package it.com.atlassian.jira.plugin.issuenav.func;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.plugin.issuenav.client.IssueTable;
import com.atlassian.jira.plugin.issuenav.client.IssueTableClient;
import com.atlassian.jira.plugin.issuenav.client.IssueTableServiceOutcome;
import com.atlassian.jira.plugin.issuenav.client.PreferredSearchLayoutClient;
import com.atlassian.jira.components.issueviewer.service.SystemFilter;
import com.atlassian.jira.plugin.issuenav.util.PreferredSearchLayoutService;
import com.google.common.collect.Sets;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.jira.issue.fields.layout.column.ColumnLayout.ColumnConfig;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Tests for {@link com.atlassian.jira.plugin.issuenav.rest.IssueTableResource}'s full screen mode.
 */
@WebTest ({ Category.FUNC_TEST, Category.ISSUES })
public class TestIssueTableResourceFullScreen extends FuncTestCase
{
    private IssueTableClient client;
    private PreferredSearchLayoutClient preferredSearchLayoutClient;

    @Override
    protected void setUpTest()
    {
        client = new IssueTableClient(environmentData);
        preferredSearchLayoutClient = new PreferredSearchLayoutClient(environmentData);
    }

    @Test
    public void testSystemFilters()
    {
        administration.restoreBlankInstance();
        backdoor.issues().createIssue("MKY", "i like monkeys");

        client.loginAs("admin");
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);

        IssueTableServiceOutcome response = client.getIssueTable(SystemFilter.MY_OPEN_ISSUES.getId(),
                "assignee = currentUser() AND resolution = Unresolved", null, null, ColumnConfig.USER);

        assertTrue(response.getWarnings().isEmpty());

        String table = (String)response.getIssueTable().getTable();
        Document doc = Jsoup.parse(table);
        Elements columnHeads = doc.select("#issuetable th");
        assertEquals(12, columnHeads.size());
    }

    @Test
    public void testJiraHasIssues()
    {
        administration.restoreBlankInstance();
        client.loginAs("admin");
        IssueTableServiceOutcome response = client.getIssueTable(null, "", null, null, ColumnConfig.USER);
        assertFalse(response.getIssueTable().getJiraHasIssues());

        backdoor.issues().createIssue("MKY", "i like monkeys");
        // Search returns no results, but jiraHasIssues should be true
        response = client.getIssueTable(null, "labels = not_there", null, null, ColumnConfig.USER);
        assertTrue(response.getIssueTable().getJiraHasIssues());
    }

    @Test
    public void testStableSearchReturnsIssueTableAndIssueIds()
    {
        administration.restoreBlankInstance();
        client.loginAs("admin");

        backdoor.issues().createIssue("MKY", "Monkey Issue 1");
        backdoor.issues().createIssue("MKY", "Monkey Issue 2");
        backdoor.issues().createIssue("HSP", "Homosapiens Issue 1");
        backdoor.issues().createIssue("HSP", "Homosapiens Issue 2");

        IssueTableServiceOutcome response = client.getIssueTable(null, "", null, null, ColumnConfig.USER);
        assertTrue(response.getIssueTable().getJiraHasIssues());
        assertTrue(response.getIssueTable().getIssueIds().indexOf(10001L) == 0);
    }

    @Test
    public void testInitialStableSearchRequestFromNonPushStateBrowserContainsStableIdsAndTableHtml()
    {
        administration.restoreBlankInstance();
        client.loginAs("admin");
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);

        backdoor.issues().createIssue("MKY", "Monkey Issue 1");
        backdoor.issues().createIssue("MKY", "Monkey Issue 2");
        backdoor.issues().createIssue("HSP", "Homosapiens Issue 1");
        backdoor.issues().createIssue("HSP", "Homosapiens Issue 2");

        IssueTableServiceOutcome response = client.getIssueTable(null, "project = HSP order by issue DESC", null, null, ColumnConfig.USER);
        assertTrue(response.getIssueTable().getJiraHasIssues());

        String table = (String)response.getIssueTable().getTable();
        assertTrue(table.contains("HSP-1"));
        assertTrue(table.contains("HSP-2"));

        assertFalse(table.contains("MKY-1"));
        assertFalse(table.contains("MKY-2"));

        assertTrue(response.getIssueTable().getIssueIds().indexOf(10003L) == 0);
        assertTrue(response.getIssueTable().getIssueIds().indexOf(10002L) == 1);
    }

    @Test
    public void testRequestWithStableListOfIssueIds()
    {
        administration.restoreBlankInstance();
        client.loginAs("admin");
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);

        backdoor.issues().createIssue("MKY", "Monkey Issue 1");
        backdoor.issues().createIssue("MKY", "Monkey Issue 2");
        backdoor.issues().createIssue("HSP", "Homosapiens Issue 1");
        backdoor.issues().createIssue("HSP", "Homosapiens Issue 2");

        IssueTableServiceOutcome response = client.getIssueTable(asList(10001L, 10002L));
        String table = (String)response.getIssueTable().getTable();
        assertTrue(table.contains("MKY-2"));
        assertTrue(table.contains("HSP-1"));

        assertFalse(table.contains("MKY-1"));
        assertFalse(table.contains("HSP-2"));

        assertNull(response.getIssueTable().getIssueIds());
    }

    @Test
    public void testNonExistentColumnsAreIgnored()
    {
        administration.restoreBlankInstance();
        client.loginAs("admin");
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);

        backdoor.issues().createIssue("MKY", "Monkey Issue 1");
        backdoor.issues().createIssue("MKY", "Monkey Issue 2");

        backdoor.columnControl().setUserColumns("admin", asList("issuekey", "summary", "blah"));

        IssueTableServiceOutcome response = client.getIssueTable(asList(10000L, 10001L));
        String table = (String)response.getIssueTable().getTable();
        assertTrue(table.contains("MKY-1"));
        assertTrue(table.contains("Monkey Issue 1"));
        assertTrue(table.contains("Monkey Issue 2"));
    }

    @Test
    public void testSpecifyingCustomFieldColumns()
    {
        administration.restoreBlankInstance();
        client.loginAs("admin");
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);

        backdoor.issues().createIssue("MKY", "Monkey Issue 1");
        backdoor.issues().createIssue("MKY", "Monkey Issue 2");

        String customField = backdoor.customFields().createCustomField("Scotts Field", "", "com.atlassian.jira.plugin.system.customfieldtypes:version", "com.atlassian.jira.plugin.system.customfieldtypes:versionsearcher");

        backdoor.columnControl().setUserColumns("admin", asList("issuekey", "summary", customField));

        IssueTableServiceOutcome response = client.getIssueTable(asList(10000L, 10001L));
        String table = (String) response.getIssueTable().getTable();
        assertTrue(table.contains("Scotts Field"));
    }

    @Test
    public void testRequestWithStableIdsPreservesOrder()
    {
        administration.restoreBlankInstance();
        client.loginAs("admin");
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);

        backdoor.issues().createIssue("MKY", "Monkey Issue 1");
        backdoor.issues().createIssue("MKY", "Monkey Issue 2");
        backdoor.issues().createIssue("HSP", "Homosapiens Issue 1");
        backdoor.issues().createIssue("HSP", "Homosapiens Issue 2");

        backdoor.columnControl().setUserColumns("admin", asList("summary"));

        IssueTableServiceOutcome response = client.getIssueTable(asList(10000L, 10001L));
        String tableHtml = (String)response.getIssueTable().getTable();
        assertTrue(tableHtml.indexOf("Monkey Issue 1") < tableHtml.indexOf("Monkey Issue 2"));

        response = client.getIssueTable(asList(10001L, 10000L));
        tableHtml = (String)response.getIssueTable().getTable();
        assertTrue(tableHtml.indexOf("Monkey Issue 2") < tableHtml.indexOf("Monkey Issue 1"));
    }

    @Test
    public void testInitialNonStableSearchResponseHasCorrectPaginationDetails()
    {
        administration.restoreData("TestClauseLozenges.xml");
        client.loginAs("admin");

        IssueTableServiceOutcome response = client.getIssueTable(null, "", null, null, ColumnConfig.USER);
        assertEquals(108, response.getIssueTable().getTotal());
        assertEquals(50, response.getIssueTable().getEnd());
    }

    @Test
    public void testInitialStableSearchResponseHasCorrectPaginationDetails()
    {
        administration.restoreData("TestClauseLozenges.xml");
        client.loginAs("admin");

        IssueTableServiceOutcome response = client.getIssueTable(null, "", null, null, ColumnConfig.USER);
        assertEquals(108, response.getIssueTable().getTotal());
        assertEquals(50, response.getIssueTable().getEnd());

        assertNotNull(response.getIssueTable().getIssueIds());
        assertEquals(response.getIssueTable().getIssueIds().size(), 108);
    }

    @Test
    public void testInitialStableSearchResponseRespondsWithCorrectStartIndex()
    {
        administration.restoreData("TestClauseLozenges.xml");
        client.loginAs("admin");

        IssueTableServiceOutcome response = client.getIssueTable(null, "", null, 20, ColumnConfig.USER);
        assertEquals(0, response.getIssueTable().getStartIndex());
        assertEquals(108, response.getIssueTable().getTotal());
        assertEquals(50, response.getIssueTable().getEnd());

        assertNotNull(response.getIssueTable().getIssueIds());
        assertEquals(response.getIssueTable().getIssueIds().size(), 108);
    }

    @Test
    public void testInitialNonStableSearchResponseRespondsWithCorrectStartIndex()
    {
        administration.restoreData("TestClauseLozenges.xml");
        client.loginAs("admin");

        IssueTableServiceOutcome response = client.getIssueTable(null, "", null, 20, ColumnConfig.USER);
        assertEquals(0, response.getIssueTable().getStartIndex());
        assertEquals(108, response.getIssueTable().getTotal());
        assertEquals(50, response.getIssueTable().getEnd());
    }

    @Test
    public void testIssueTableShouldContainStableSearchIssueIdsWhenStableSearchWasRequested()
    {
        administration.restoreData("TestClauseLozenges.xml");
        client.loginAs("admin");

        Set<String> expectedIssueKeys = Sets.newHashSet("QA-19", "QA-20");
        IssueTableServiceOutcome response = client.getIssueTable(null, "key IN (QA-19, QA-20)", null, 0, ColumnConfig.USER);

        assertTrue(response.getWarnings().isEmpty());
        assertThat(new HashSet<String>(response.getIssueTable().getIssueKeys()), equalTo(expectedIssueKeys));
        assertFalse(response.getIssueTable().getTable().toString().isEmpty());
    }

    @Test
    public void testColumnConfigForStableSearch()
    {
        administration.restoreData("TestColumnConfigWithSystem.xml");
        client.loginAs("admin");
        final List<String> userColumns = asList("issuetype", "issuekey", "summary");
        final List<String> filterColumns = asList("issuekey", "summary", "assignee", "priority");
        final List<String> systemColumns = asList("created", "updated", "duedate");

        List<Long> ids = asList(10000L, 10001L);
        final int expectedRowCount = ids.size();

        //Defaults to user columns
        IssueTableServiceOutcome response = client.getIssueTable(ids, null, null, null);
        assertColumnConfig("USER", userColumns, expectedRowCount, response);

        //Specified filter but fitlerid is null. Defaults to user
        response = client.getIssueTable(ids, null, null, ColumnConfig.FILTER);
        assertColumnConfig("USER", userColumns, expectedRowCount, response);

        //Proper call for filter
        response = client.getIssueTable(ids, "10000", null, ColumnConfig.FILTER);
        assertColumnConfig("FILTER", filterColumns, expectedRowCount, response);

        //Use explicit without specifying column config defaults to user
        final List<String> explicitColumns = asList("created", "issuetype", "summary");
        response = client.getIssueTable(ids, null, explicitColumns, null);
        assertColumnConfig("USER", userColumns, expectedRowCount, response);

        //Proper call for explicit
        response = client.getIssueTable(ids, null, explicitColumns, ColumnConfig.EXPLICIT);
        assertColumnConfig("EXPLICIT", explicitColumns, expectedRowCount, response);

        //Use system
        response = client.getIssueTable(ids, null, null, ColumnConfig.SYSTEM);
        assertColumnConfig("SYSTEM", systemColumns, expectedRowCount, response);
    }

    public void assertColumnConfig(String config, final List<String> columns, int total, IssueTableServiceOutcome response)
    {
        IssueTable table = response.getIssueTable();
        assertEquals(config, table.getColumnConfig());
        assertEquals(columns, table.getColumns());
        assertEquals(total, table.getTotal());
    }
}
