package it.com.atlassian.jira.plugin.issuenav.webdriver.issueactions;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.components.menu.IssueActions;
import com.atlassian.jira.pageobjects.dialogs.IssueActionsUtil;
import com.atlassian.jira.pageobjects.pages.viewissue.ActionTrigger;
import com.atlassian.jira.plugin.issuenav.client.SlomoClient;
import com.atlassian.jira.plugin.issuenav.client.SlomoPattern;
import com.atlassian.jira.components.issueviewer.config.IssueNavFeatures;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.ReadyForAction;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.SplitLayout;
import com.atlassian.pageobjects.PageBinder;
import it.com.atlassian.jira.plugin.issuenav.webdriver.KickassWebDriverTestCase;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@WebTest ({ Category.WEBDRIVER_TEST, Category.ISSUES })
public class TestDeferredIssueActions extends KickassWebDriverTestCase
{
    private IssueActionsUtil issueActionsUtil;
    private PageBinder pageBinder;
    private SlomoClient slomoClient;
    private SlomoPattern slomoPattern;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestInlineEdit.xml");
        issueActionsUtil = product.getPageBinder().bind(IssueActionsUtil.class);
        pageBinder = product.getPageBinder();
        slomoClient = new SlomoClient(environmentData);
        backdoor.darkFeatures().enableForSite(IssueNavFeatures.NO_PREFETCH.featureKey());
    }

    @After
    public void tearDown()
    {
        backdoor.darkFeatures().disableForSite(IssueNavFeatures.NO_PREFETCH.featureKey());
        slomoClient.removeDelay(slomoPattern.id);
    }

    @Test
    public void testInSplitMode()
    {
        IssuesPage issuesPage = logInToIssuesPageAsSysadmin("?jql=");
        SplitLayout splitLayout = issuesPage.getSplitLayout();
        slomoPattern = slomoClient.setDelay(".*AjaxIssueAction.*", 2000L);
        splitLayout.nextIssue(false);
        issueActionsUtil.invokeActionTrigger(IssueActions.LABELS, ActionTrigger.KEYBOARD_SHORTCUT);
        IssueDetailComponent updatedIssue = pageBinder.bind(IssueDetailComponent.class, "XSS-16");
        assertEquals("XSS-16", updatedIssue.getIssueKey());
        assertFalse("Expected labels dialog not to show if called when moving to another issue", issuesPage.hasVisibleDialog());
    }

    @Test
    public void testInFullScreenMode() throws InterruptedException
    {
        IssuesPage issuesPage = goToIssuesPage("?jql=").getListLayout();
        IssueDetailComponent issueDetailComponent = issuesPage.getResultsTable().navigateToIssueDetailPage("XSS-17");
        slomoPattern = slomoClient.setDelay(".*AjaxIssueAction.*", 2000L);
        issueDetailComponent.nextIssue(ReadyForAction.NONE);
        issueActionsUtil.invokeActionTrigger(IssueActions.LABELS, ActionTrigger.KEYBOARD_SHORTCUT);
        IssueDetailComponent updatedIssue = pageBinder.bind(IssueDetailComponent.class, "XSS-16");
        assertEquals("XSS-16", updatedIssue.getIssueKey());
        assertFalse("Expected labels dialog not to show if called when moving to another issue", issuesPage.hasVisibleDialog());
    }
}
