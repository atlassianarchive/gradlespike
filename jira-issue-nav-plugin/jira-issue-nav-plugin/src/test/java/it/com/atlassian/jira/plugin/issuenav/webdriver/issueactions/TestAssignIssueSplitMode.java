package it.com.atlassian.jira.plugin.issuenav.webdriver.issueactions;

import com.atlassian.jira.pageobjects.pages.viewissue.ActionTrigger;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.SplitViewSelectedIssue;

public class TestAssignIssueSplitMode extends TestAssignSplitMode
{
    @Override
    void assign(SplitViewSelectedIssue selectedIssue, ActionTrigger actionTrigger)
    {
        selectedIssue.assignIssue("admin", actionTrigger);
    }
}
