package it.com.atlassian.jira.plugin.issuenav.func;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.plugin.issuenav.client.IssueNavToolsClient;
import org.junit.Test;

import java.io.IOException;

/**
 * Smoke test for the KickAss Issue Nav Tools Action. The actual tools options content are checked by tests in JIRA core.
 */
@WebTest({ Category.FUNC_TEST })
public class TestIssueNavToolsAction extends FuncTestCase
{
    private IssueNavToolsClient issueNavToolsClient;

    @Override
    protected void setUpTest()
    {
        administration.restoreBlankInstance();
        issueNavToolsClient = new IssueNavToolsClient(getEnvironmentData());
    }

    @Test
    public void testIssueNavToolsSmokeTest() throws IOException
    {
        // Store a session search request
//        navigation.gotoPage("/issues/?jql=assignee=currentUser()");
//
//        Response response = issueNavToolsClient.getResponse(50, 50);
//        assertEquals(200, response.statusCode);
    }
}
