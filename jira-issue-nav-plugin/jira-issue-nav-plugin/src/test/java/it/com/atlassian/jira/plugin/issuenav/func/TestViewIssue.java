package it.com.atlassian.jira.plugin.issuenav.func;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import static org.hamcrest.CoreMatchers.startsWith;

/**
 * Testing the capabilities of server side rendered view issue page
 *
 * @since v6.0
 */
@WebTest({ Category.FUNC_TEST, Category.ISSUES })
public class TestViewIssue extends FuncTestCase
{
    @Override
    protected void setUpTest()
    {
        administration.restoreData("TestNavigation.xml");
    }

    @Test
    public void testServerSideRenderedViewIssue() throws SAXException
    {
        navigation.gotoPage("/browse/QA-31");

        String url = tester.getDialog().getResponse().getURL().toString();
        String[] metaValue = tester.getDialog().getResponse().getMetaTagContent("name", "ajs-serverRenderedViewIssue");

        Assert.assertThat(tester.getDialog().getResponsePageTitle(), startsWith("[QA-31] CLONE - Testing label stuff - Testing Bulk Move"));
        Assert.assertThat(url, Matchers.containsString("/browse/QA-31"));
        Assert.assertThat(metaValue.length, Matchers.equalTo(1));
        Assert.assertThat(metaValue[0], Matchers.equalTo("true"));
        tester.assertTextInElement("summary-val", "CLONE - Testing label stuff");
        tester.assertTextInElement("key-val", "QA-31");
    }

    @Test
    public void testGoToMovedIssueRedirectCorrectUrlAndIssue()
    {
        navigation.gotoPage("/browse/XSS-17");

        tester.clickLink("move-issue");
        tester.assertTextPresent("Move Issue: XSS-17 - asdasdasdasd 10:57am");
        tester.selectOption("pid", "Bulk Move 1");
        tester.submit("Next >>");
        tester.submit("Next >>");
        tester.submit("Move");

        navigation.gotoPage("/browse/XSS-17");

        tester.assertElementPresent("key-val");
        Assert.assertThat(tester.getDialog().getResponsePageTitle(), startsWith("[BULK-109] asdasdasdasd 10:57am - Testing Bulk Move"));
        String url = tester.getDialog().getResponse().getURL().toString();
        Assert.assertThat(url, Matchers.containsString("/browse/BULK-109"));
    }
}
