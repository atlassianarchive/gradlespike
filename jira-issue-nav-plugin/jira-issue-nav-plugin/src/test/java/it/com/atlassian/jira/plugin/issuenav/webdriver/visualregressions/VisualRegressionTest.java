package it.com.atlassian.jira.plugin.issuenav.webdriver.visualregressions;

import com.atlassian.jira.testkit.client.RestoreDataResources;
import com.atlassian.pageobjects.inject.InjectionContext;
import com.atlassian.selenium.visualcomparison.VisualComparableClient;
import com.atlassian.selenium.visualcomparison.VisualComparer;
import com.atlassian.selenium.visualcomparison.utils.ScreenResolution;
import com.google.inject.Inject;
import it.com.atlassian.jira.plugin.issuenav.webdriver.KickassWebDriverTestCase;
import org.junit.Before;
import org.openqa.selenium.Dimension;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * The base class for issue navigator visual regression tests.
 *
 * @since v6.1
 */
public abstract class VisualRegressionTest extends KickassWebDriverTestCase {

    @Inject private VisualComparableClient client;
    protected VisualComparer visualComparer;
    protected final Dimension DEFAULT_DIMENSION = new Dimension(1024, 768);
    protected final static String OUTPUT_DIRECTORY = "target/test-reports";

    @Before
    public void resetVisualComparer ()
    {
        backdoor.plugins().disablePlugin("com.atlassian.jira.jira-feedback-plugin"); // JRADEV-18286 - Has faulty check for webdriver test runs. Disabling manually.

        // Inject .client
        final InjectionContext context = (InjectionContext) product.getPageBinder();
        context.injectMembers(this);

        visualComparer = new VisualComparer(client);
        visualComparer.setScreenResolutions(new ScreenResolution[] {
                new ScreenResolution(DEFAULT_DIMENSION.width, DEFAULT_DIMENSION.height)
        });

        Map<String, String> uiStringReplacements = new HashMap<String, String>();
        uiStringReplacements.put("footer-build-information","(Build information removed)");
        visualComparer.setUIStringReplacements(uiStringReplacements);

        visualComparer.setWaitforJQueryTimeout(1000);
        visualComparer.enableReportGeneration(OUTPUT_DIRECTORY);
    }

    protected void assertUIMatches(String id)
    {
        removeUPMCount();
        visualComparer.assertUIMatches(id, getBaselineScreenshotFilePath());
    }

    /**
     * Removes the little yellow gem that appears on the UPM icon to indicate there are new updates to plugins.
     */
    protected void removeUPMCount()
    {
        Map<String,String> uiStringReplacements = new HashMap<String,String>(visualComparer.getUIStringReplacements());
        uiStringReplacements.put("upm-notifications-trigger","<span id='upm-notifications-icon'></span>");
        visualComparer.setUIStringReplacements(uiStringReplacements);
    }

    /**
     * The location of the baseline screenshots in the file system of the machine the tests are running on.
     */
    public String getBaselineScreenshotFilePath()
    {
        // The baseline screenshots live in https://bitbucket.org/atlassian/ka-baseline-images
        // This translates to "this baseline image folder and the JIRA repo checkout folder live within the same parent folder."
        // It's a little opaque, but unfortunately, this is the best way we could find to
        // deal with the different working directories when running from IDEA or Bamboo.
        return RestoreDataResources.getResourceUrl("").getPath() + "../../../../ka-baseline-images";
    }
}
