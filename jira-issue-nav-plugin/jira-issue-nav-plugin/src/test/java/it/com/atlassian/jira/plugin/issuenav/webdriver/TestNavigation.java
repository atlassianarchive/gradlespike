package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.pageobjects.dialogs.FormDialog;
import com.atlassian.jira.pageobjects.elements.GlobalMessage;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.components.issueviewer.config.IssueNavFeatures;
import com.atlassian.jira.plugin.issuenav.pageobjects.AdvancedQueryComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.ExecuteAdvancedPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.filter.Filters;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.KeyboardShortcutsDialog;
import com.atlassian.jira.plugin.issuenav.pageobjects.ResultsTableComponent;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.openqa.selenium.By;

import java.util.Map;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * This class tests that navigating in kickass updates the URL correctly vice versa that changing the URL (back/forward
 * buttons) will update the page correctly.
 * <p/>
 * Specifically this will test: <ul> <li>Go to entry page /issues/ -> Should have empty basic search</li> <li>Try
 * advanced search by clicking the toggle -> URL should be /issues/advanced/</li> <li>Go back to keywoard search by
 * clicking the toggle -> URL should be /issues/basic/</li> <li>Run a keywoard search with text -> URL should be
 * /issues/keywoard/?q=testing </li> <li>Select issues in search results -> URL should contain
 * &issue=&lt;ISSUEKEY>&gt;</li> <li>Click on an issue in search results -> Should show in detailed view and URL should
 * contain /issues/basic/?q=testing&issue=BULK-70&displayMode=detailed</li> <li>Return to search results view ->
 * displayMode param should disappear</li> <li>Switch to advanced mode -> URL should contain
 * /issues/advanced/?jql=&lt;query&gt;</li> <li>Select issues in ssearch results -> URL should contain
 * &issue=&lt;ISSUEKEY>&gt;</li> <li>Click on an issue in search results -> Should show in detailed view and URL should
 * contain /issues/advanced/?jql=text%20~%20"testing"&issue=BULK-70&displayMode=detailed</li> <li>Navigating back and
 * forward with browser buttons should update page contents correctly</li> </ul>
 */
public class TestNavigation extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;
    private TraceContext traceContext;
    private Map expectedCols = EasyMap.build(
            "summary", ResultsTableComponent.SORT_ORDER.desc,
            "issuekey", ResultsTableComponent.SORT_ORDER.desc,
            "issuetype", ResultsTableComponent.SORT_ORDER.asc,
            "status", ResultsTableComponent.SORT_ORDER.asc,
            "votes", ResultsTableComponent.SORT_ORDER.asc,
            "resolution", ResultsTableComponent.SORT_ORDER.desc,
            "reporter", ResultsTableComponent.SORT_ORDER.desc,
            "created", ResultsTableComponent.SORT_ORDER.asc,
            "updated", ResultsTableComponent.SORT_ORDER.asc,
            "priority", ResultsTableComponent.SORT_ORDER.asc,
            "duedate", ResultsTableComponent.SORT_ORDER.asc,
            "customfield_10010", ResultsTableComponent.SORT_ORDER.desc,
            "customfield_10071", ResultsTableComponent.SORT_ORDER.desc);

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestNavigation.xml");
        traceContext = product.getPageBinder().bind(TraceContext.class);
        issuesPage = logInToIssuesPageAsSysadmin("?jql=");
        issuesPage.getListLayout();
    }

    @Test
    public void testSwitchingBetweenBasicAndAdvanced()
    {
        issuesPage = issuesPage.getBasicQuery().searchAndWait("test hsql");
        assertEquals(2, issuesPage.getResultsTable().getResultCount());

        assertEquals("/issues/?jql=text%20~%20%22test%20hsql%22", issuesPage.getWindowLocation());
        assertEquals("QA-30", issuesPage.getResultsTable().getSelectedIssueKey());

        assertEquals(2, issuesPage.getResultsTable().getResultCount());
        assertEquals("text ~ \"test hsql\"", issuesPage.getAdvancedQuery().getCurrentQuery());
        assertEquals("/issues/?jql=text%20~%20%22test%20hsql%22", issuesPage.getWindowLocation());
        assertEquals("QA-30", issuesPage.getResultsTable().getSelectedIssueKey());

        issuesPage.getResultsTable().selectNextIssue();
        assertEquals("QA-36", issuesPage.getResultsTable().getSelectedIssueKey());
        assertEquals("/issues/?jql=text%20~%20%22test%20hsql%22", issuesPage.getWindowLocation());

        //switch back to basic
        issuesPage.getBasicQuery();
        assertEquals("QA-36", issuesPage.getResultsTable().getSelectedIssueKey());
        assertEquals("/issues/?jql=text%20~%20%22test%20hsql%22", issuesPage.getWindowLocation());

        //switch back to advanced mode
        issuesPage.getAdvancedQuery();
        assertEquals("QA-36", issuesPage.getResultsTable().getSelectedIssueKey());
        assertEquals("/issues/?jql=text%20~%20%22test%20hsql%22", issuesPage.getWindowLocation());

        //do another advanced query
        issuesPage = issuesPage.getAdvancedQuery().searchAndWait("text ~ \"test\" OR text ~ \"hsql\"");
        assertEquals("/issues/?jql=text%20~%20%22test%22%20OR%20text%20~%20%22hsql%22", issuesPage.getWindowLocation());
        assertEquals(11, issuesPage.getResultsTable().getResultCount());
        waitUntilFalse(issuesPage.isBasicSearch());

        //NOW GO BACK IN TIME!
        issuesPage = issuesPage.backAndWait(true, false);
        assertEquals("/issues/?jql=text%20~%20%22test%20hsql%22", issuesPage.getWindowLocation());
        assertEquals(2, issuesPage.getResultsTable().getResultCount());
        waitUntilFalse(issuesPage.isBasicSearch());

        issuesPage = issuesPage.backAndWait(true, true);
        assertEquals(50, issuesPage.getResultsTable().getResultCount());
        assertEquals("", issuesPage.getBasicQuery().getCurrentQuery());
    }

    @Test
    public void testSwitchingBetweenAdvancedAndBasic()
    {
        issuesPage = issuesPage.getAdvancedQuery().searchAndWait("project = BULK");
        assertEquals(50, issuesPage.getResultsTable().getResultCount());
        waitUntilFalse(issuesPage.isSwitcherDisabled());

        issuesPage = issuesPage.getAdvancedQuery().searchAndWait("project = \"Bulk Move 1\" and issuetype = Task");
        assertEquals(2, issuesPage.getResultsTable().getResultCount());
        waitUntilFalse(issuesPage.isSwitcherDisabled());

        issuesPage.getBasicQuery();
        waitUntilTrue(issuesPage.isBasicSearch());
        assertEquals("Bulk Move 1", issuesPage.getBasicQuery().project().getSelectedValue());
        assertEquals("Task", issuesPage.getBasicQuery().issueType().getSelectedValue());

        issuesPage.getAdvancedQuery();
        waitUntilFalse(issuesPage.isBasicSearch());
        assertEquals("project = \"Bulk Move 1\" and issuetype = Task", issuesPage.getAdvancedQuery().getCurrentQuery());

        assertEquals("Bulk Move 1", issuesPage.getBasicQuery().project().getSelectedValue());
        assertEquals("Task", issuesPage.getBasicQuery().issueType().getSelectedValue());

        issuesPage = issuesPage.backAndWait(true, true);
        assertEquals("Bulk Move 1", issuesPage.getBasicQuery().project().getSelectedValue());
        assertEquals("All", issuesPage.getBasicQuery().issueType().getSelectedValue());

        issuesPage = issuesPage.getAdvancedQuery().searchAndWait("brokenField = brokenOperand");
        waitUntilTrue(issuesPage.isSwitcherDisabled());
    }

    @Test
    public void testAdvancedNavigation()
    {
        issuesPage = issuesPage.getAdvancedQuery().searchAndWait("project = BULK");
        assertEquals(50, issuesPage.getResultsTable().getResultCount());
        assertEquals("/issues/?jql=project%20%3D%20BULK", issuesPage.getWindowLocation());

        issuesPage.getResultsTable().selectNextIssue();
        assertEquals("/issues/?jql=project%20%3D%20BULK", issuesPage.getWindowLocation());
        assertEquals("BULK-103", issuesPage.getResultsTable().getSelectedIssueKey());

        issuesPage.getResultsTable().selectPreviousIssue();
        assertEquals("/issues/?jql=project%20%3D%20BULK", issuesPage.getWindowLocation());
        assertEquals("BULK-108", issuesPage.getResultsTable().getSelectedIssueKey());

        //run another query.
        issuesPage = issuesPage.getAdvancedQuery().searchAndWait("project = BULK and assignee = admin and resolution is not empty");
        assertEquals(8, issuesPage.getResultsTable().getResultCount());
        assertEquals("/issues/?jql=project%20%3D%20BULK%20and%20assignee%20%3D%20admin%20and%20resolution%20is%20not%20empty", issuesPage.getWindowLocation());

        issuesPage = issuesPage.backAndWait(true, false);
        assertEquals("/issues/?jql=project%20%3D%20BULK", issuesPage.getWindowLocation());
        assertEquals("BULK-108", issuesPage.getResultsTable().getSelectedIssueKey());
        assertEquals(50, issuesPage.getResultsTable().getResultCount());
    }

    @Test
    public void testSortUrl()
    {
        issuesPage.getResultsTable().sortColumn("summary", ResultsTableComponent.SORT_ORDER.asc);
        assertEquals("/issues/?jql=ORDER%20BY%20summary%20ASC", issuesPage.getWindowLocation());
        issuesPage.getResultsTable().sortColumn("summary", ResultsTableComponent.SORT_ORDER.desc);
        assertEquals("/issues/?jql=ORDER%20BY%20summary%20DESC", issuesPage.getWindowLocation());
        issuesPage.getResultsTable().sortColumn("assignee", ResultsTableComponent.SORT_ORDER.asc);
        assertEquals("/issues/?jql=ORDER%20BY%20assignee%20ASC%2C%20summary%20DESC", issuesPage.getWindowLocation());
        Map<String,ResultsTableComponent.SORT_ORDER> sortedColumns = issuesPage.getResultsTable().getSortedColumns();
        assertEquals(expectedCols, sortedColumns);
        goToIssuesPage("?jql=ORDER%20BY%20assignee%20ASC%2C%20summary%20DESC");
        sortedColumns = issuesPage.getResultsTable().getSortedColumns();
        assertEquals(expectedCols, sortedColumns);
    }

    @Test
    public void testJqlUrlEscaping()
    {
        issuesPage = issuesPage.getAdvancedQuery().searchAndWait("text~'&'");
        assertTrue(issuesPage.getWindowLocation().equals("/issues/?jql=text~%27%26%27") || issuesPage.getWindowLocation().equals("/issues/?jql=text~'%26'"));
    }

    @Test
    public void testFilters()
    {
        issuesPage.getBasicQuery(); // Make basic mode the user's default.

        Filters filters = issuesPage.getFilters();
        assertEquals(Lists.newArrayList("Bugs", "Too Advanced"), filters.getFavouriteFilters());

        issuesPage = filters.selectFavourite("Bugs");
        checkState(issuesPage, "/issues/?filter=10016", "Bugs", null, 79, true, false);

        issuesPage.getResultsTable().selectNextIssue();
        checkState(issuesPage, "/issues/?filter=10016", "Bugs", null, 79, true, false);
        assertEquals("XSS-16", issuesPage.getResultsTable().getSelectedIssueKey());

        issuesPage.getResultsTable().selectPreviousIssue();
        checkState(issuesPage, "/issues/?filter=10016", "Bugs", null, 79, true, false);
        assertEquals("XSS-17", issuesPage.getResultsTable().getSelectedIssueKey());

        // The advanced mode input should be populated with the filter's JQL.
        AdvancedQueryComponent advancedQuery = issuesPage.getAdvancedQuery();
        assertEquals("issuetype = Bug", advancedQuery.getCurrentQuery());

        // The same goes for system filters. We should stay in advanced mode.
        filters.selectSystem("My Open Issues");
        checkState(issuesPage, "/issues/?filter=-1",
                "My Open Issues", "assignee = currentUser() AND resolution = Unresolved ORDER BY updatedDate DESC",
                102, false, false);

        // Switch to basic mode to set the user preference and select a filter
        // that can only be represented in JQL; advanced mode should be used.
        issuesPage.getBasicQuery();
        filters.selectFavourite("Too Advanced");
        checkState(issuesPage, "/issues/?filter=10020", "Too Advanced",
                "createdDate < endOfMonth()", 132, false, true);

        // Running simpler JQL should update the URL, but stay in advanced mode.
        issuesPage = advancedQuery.searchAndWait("project = BULK");
        checkState(issuesPage, "/issues/?filter=10020&jql=project%20%3D%20BULK",
                "Too Advanced", "project = BULK", 90, false, false);

        // Now start going back!
        issuesPage = issuesPage.backAndWait(true, false);
        checkState(issuesPage, "/issues/?filter=10020", "Too Advanced",
                "createdDate < endOfMonth()", 132, false, true);

        // This "My Open Issues" filter was in advanced mode before, but the
        // user's preference is basic; they're obviously not interacting with
        // the JQL/searchers, so here we can swap back to their preference.
        issuesPage = issuesPage.backAndWait(true, false);
        checkState(issuesPage, "/issues/?filter=-1",
                "My Open Issues", null, 102, true, false);

        // Reloading the page on a simple filter should use their preference.
        issuesPage = goToIssuesPage("?filter=10016");
        issuesPage.getFilters().waitForActiveFilter();
        checkState(issuesPage, "/issues/?filter=10016", "Bugs",
                "issuetype = Bug", 79, true, false);

        // Reloading the page on a complex filter should use advanced mode.
        issuesPage = goToIssuesPage("?filter=10020");
        issuesPage.getFilters().waitForActiveFilter();
        checkState(issuesPage, "/issues/?filter=10020", "Too Advanced",
                "createdDate < endOfMonth()", 132, false, true);

        // Navigating to a system filter should work.
        issuesPage = goToIssuesPage("?filter=-1");
        checkState(issuesPage, "/issues/?filter=-1",
                "My Open Issues", "assignee = currentUser() AND resolution = Unresolved ORDER BY updatedDate DESC",
                102, false, false);
    }

    @Test
    public void testRedirectsToEmptyJql()
    {
        issuesPage.waitForResultsTable();
        assertEquals("/issues/?jql=", issuesPage.getWindowLocation());
    }

    @Test
    public void testClauseLozenges()
    {
        // Selecting a project should update the issues table and change the URL.
        issuesPage = issuesPage.getBasicQuery().project().setValueAndSubmit("Bulk Move 1");
        assertEquals("Bulk Move 1", issuesPage.getBasicQuery().project().getSelectedValue());
        assertEquals(50, issuesPage.getResultsTable().getResultCount());
        assertEquals("/issues/?jql=project%20%3D%20BULK", issuesPage.getWindowLocation());
        assertEquals("BULK-108", issuesPage.getResultsTable().getSelectedIssueKey());

        issuesPage = issuesPage.backAndWait(true, true);
        assertEquals("/issues/?jql=", issuesPage.getWindowLocation());

        issuesPage = issuesPage.forwardAndWait(true, true);
        assertEquals("/issues/?jql=project%20%3D%20BULK", issuesPage.getWindowLocation());
        assertEquals("Bulk Move 1", issuesPage.getBasicQuery().project().getSelectedValue());
        assertEquals(50, issuesPage.getResultsTable().getResultCount());
        assertEquals("BULK-108", issuesPage.getResultsTable().getSelectedIssueKey());

        // The text query should be populated correctly (it shouldn't be HMTL).
        // We have to double encode spaces so they get past JiraLoginFilter.
        issuesPage = goToIssuesPage("?jql=text%20~%20Test");
        assertEquals("Test", issuesPage.getBasicQuery().getCurrentQuery());
    }

    @Test
    public void testClauseLozengeResetToAll()
    {
        issuesPage = issuesPage.getBasicQuery().project().setValueAndSubmit("Bulk Move 1");
        assertEquals("Bulk Move 1", issuesPage.getBasicQuery().project().getSelectedValue());

        issuesPage = issuesPage.getBasicQuery().project().clear();
        assertEquals("All", issuesPage.getBasicQuery().project().getSelectedValue());
    }

    @Test
    public void testAddFiltersButton()
    {
        assertTrue(issuesPage.getBasicQuery().canAddCriteria());

        issuesPage = issuesPage.getBasicQuery().project().setValueAndSubmit("Bulk Move 1");
        assertTrue(issuesPage.getBasicQuery().canAddCriteria());

        issuesPage = issuesPage.backAndWait(true, true);
        assertTrue(issuesPage.getBasicQuery().canAddCriteria());

        issuesPage = issuesPage.forwardAndWait(true, true);
        assertTrue(issuesPage.getBasicQuery().canAddCriteria());
    }

    /**
     * Test that JQL errors and warnings appear when they should.
     *
     * e.g. Malformed JQL, if an invalid project is referenced, etc.
     */
    @Test
    public void testJQLErrors()
    {
        final AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();

        // Invalid JQL should produce an error on normal requests...
        advancedQueryComponent.searchAndWait("lolinvalidjql");
        assertTrue(issuesPage.hasJQLErrors());
        assertFalse(issuesPage.hasJQLWarnings());
        assertThat(issuesPage.getJQLErrors(), Matchers.startsWith("Error in the JQL"));

        // ...and push-state requests (first make a valid search to clear errors).
        advancedQueryComponent.searchAndWait("");
        assertFalse(issuesPage.hasJQLErrors());
        assertFalse(issuesPage.hasJQLWarnings());
        advancedQueryComponent.searchAndWait("lolinvalidjql");
        assertTrue(issuesPage.hasJQLErrors());
        assertFalse(issuesPage.hasJQLWarnings());
        assertThat(issuesPage.getJQLErrors(), Matchers.startsWith("Error in the JQL Query:"));

        // Referencing an invalid project should produce an error on normal requests...
        advancedQueryComponent.searchAndWait("project = badproject");
        assertTrue(issuesPage.hasJQLErrors());
        assertThat(issuesPage.getJQLErrors(), Matchers.startsWith("The value 'badproject' does not"));
        assertFalse(issuesPage.hasJQLWarnings());

        // ...and push-state requests (first make a valid search to clear errors).
        advancedQueryComponent.searchAndWait("");
        assertFalse(issuesPage.hasJQLErrors());
        assertFalse(issuesPage.hasJQLWarnings());
        advancedQueryComponent.searchAndWait("project = badproject");
        assertTrue(issuesPage.hasJQLErrors());
        assertThat(issuesPage.getJQLErrors(), Matchers.startsWith("The value 'badproject' does not"));
        assertFalse(issuesPage.hasJQLWarnings());

        // Referencing invalid users should produce a warning on normal requests...
        advancedQueryComponent.searchAndWait("assignee = omgidontexist");
        assertFalse(issuesPage.hasJQLErrors());
        assertTrue(issuesPage.hasJQLWarnings());
        assertThat(issuesPage.getJQLWarnings(), Matchers.startsWith("The value 'omgidontexist' does not"));

        // ...and push-state requests (first make a valid search to clear warnings).
        advancedQueryComponent.searchAndWait("");
        assertFalse(issuesPage.hasJQLErrors());
        assertFalse(issuesPage.hasJQLWarnings());
        advancedQueryComponent.searchAndWait("assignee = omgidontexist");
        assertFalse(issuesPage.hasJQLErrors());
        assertTrue(issuesPage.hasJQLWarnings());
        assertThat(issuesPage.getJQLWarnings(), Matchers.startsWith("The value"));
    }

    @Test
    public void testIssueTablePagination()
    {
        ResultsTableComponent table = issuesPage.getResultsTable();
        assertEquals(1, table.getCurrentPageIndex());
        Tracer tracer = traceContext.checkpoint();
        table.goToNextPage();
        issuesPage.waitForResults(tracer);
        assertEquals(2, table.getCurrentPageIndex());

        issuesPage = goToIssuesPage("?jql=&startIndex=50");
        assertEquals(2, issuesPage.getResultsTable().getCurrentPageIndex());
    }

    @Test
    public void testPermalink()
    {
        String urlFragment =  "/issues/";

        issuesPage.getFilters().selectFavourite("Bugs");
        assertThat(issuesPage.getPermalinkHref(), Matchers.endsWith(urlFragment + "?filter=10016"));
        issuesPage.newSearch();
        assertThat(issuesPage.getPermalinkHref(), Matchers.endsWith(urlFragment + "?jql="));
    }

    @Test
    public void testBackNavigationClosesDialog()
    {
        issuesPage.getBasicQuery().project().setValueAndSubmit("Bulk Move 1");
        issuesPage.getBasicQuery().status().setValueAndSubmit("In Progress");
        issuesPage.getFilters().openSaveFilterDialog();
        issuesPage = issuesPage.backAndWait(true, true);
        assertFalse(issuesPage.getFilters().saveFilterDialogIsOpen());
    }

    @Test
    public void testBackNavigationClosesEditDialog()
    {
        goToIssuesPage();
        IssueDetailComponent issueDetail = issuesPage.getResultsTable().navigateToIssueDetailPage("XSS-15").waitForEditButton();
        FormDialog editDialog = issueDetail.openEditDialog();
        issuesPage = issuesPage.back();
        Poller.waitUntilFalse(editDialog.isOpen());
    }

    /**
     * Disabling keyboard shortcuts shouldn't show an "issue updated" message.
     * <p/>
     * This is because the dialog isn't marked as being "issue-related".
     */
    @Test
    public void testDisablingKeyboardShortcuts()
    {
        AtlassianWebDriver webDriver = product.getTester().getDriver();
        webDriver.findElement(By.cssSelector("#system-help-menu a.aui-nav-link")).click();
        webDriver.findElement(By.id("keyshortscuthelp")).click();

        Tracer checkpoint = traceContext.checkpoint();
        product.getPageBinder().bind(KeyboardShortcutsDialog.class)
                .toggleKeyboardShortcuts();

        // The page should refresh after disabling keyboard shortcuts; wait
        // until search results have loaded so we know the reload is complete.
        issuesPage.waitForResults(checkpoint, true);
        waitUntilFalse(product.getPageBinder().bind(GlobalMessage.class)
                .isPresent());
    }

    /**
     * The page information and result count should be correct after using the
     * j/k shortcuts to move between pages.
     */
    @Test
    public void testResultCountAcrossPages()
    {
        issuesPage = goToIssuesPage("?jql=&startIndex=50");
        ResultsTableComponent issueTable = issuesPage.getResultsTable();
        Poller.waitUntilEquals("51", issueTable.getPageStartIndexTimed());
        Poller.waitUntilEquals("100", issueTable.getPageEndIndexTimed());

        issueTable.selectPreviousIssue();
        issueTable = issuesPage.getResultsTable();
        Poller.waitUntilEquals("1", issueTable.getPageStartIndexTimed());
        Poller.waitUntilEquals("50", issueTable.getPageEndIndexTimed());
    }

    /*
     * Requesting <tt>/secure/IssueNavigator!executeAdvanced.jspa</tt> should
     * set the user's search mode to advanced and redirect to <tt>/issues/</tt>.
     */
    @Test
    public void testAdvancedRedirect()
    {
        // Set their search mode.
        Tracer checkpoint = traceContext.checkpoint();
        issuesPage.getBasicQuery();
        issuesPage.waitForSearchModeSet(checkpoint);

        // Navigate to /secure/IssueNavigator!executeAdvanced.jspa.
        product.goTo(ExecuteAdvancedPage.class);
        issuesPage.waitForResults(checkpoint);

        // The user's search mode should have been set to advanced.
        waitUntilFalse(issuesPage.isBasicSearch());
    }

    /**
     * Tests if a startIndex is larger than the actual number of issues it displays the last page.
     */
    @Test
    public void testStartIndexChangedToLastPageIfTooLarge()
    {
        issuesPage = goToIssuesPage("?jql=&startIndex=200");
        ResultsTableComponent issueTable = issuesPage.getResultsTable();
        Poller.waitUntilEquals("101", issueTable.getPageStartIndexTimed());
        assertTrue(product.getTester().getDriver().getCurrentUrl().endsWith("?jql=&startIndex=100"));
    }

    /**
     * Test that there is enough issues but the stable search limit restricts the total number of issues and whne you
     * put a startIndex between the two you get moved to the last page of the search.
     */
    @Test
    public void testStartIndexBetweenMaxStableSearchAndTotalIssues()
    {
        backdoor.applicationProperties().setString(APKeys.JIRA_STABLE_SEARCH_MAX_RESULTS, "100");
        issuesPage = goToIssuesPage("?jql=&startIndex=125");
        ResultsTableComponent issueTable = issuesPage.getResultsTable();
        Poller.waitUntilEquals("51", issueTable.getPageStartIndexTimed());
        assertTrue(product.getTester().getDriver().getCurrentUrl().endsWith("?jql=&startIndex=50"));
        backdoor.applicationProperties().setString(APKeys.JIRA_STABLE_SEARCH_MAX_RESULTS, "1000");
    }

    /**
     * Check that the page is in a particular state. Passing a JQL value will
     * enter advanced mode and may change the user's preference.
     */
    private void checkState(IssuesPage issuesPage, String url, String activeFilter, String jql, int resultsCountTotal, boolean isBasicSearch, boolean isSwitcherDisabled)
    {
        waitUntilEquals(isBasicSearch, issuesPage.isBasicSearch());
        waitUntilEquals(isSwitcherDisabled, issuesPage.isSwitcherDisabled());
        assertEquals(activeFilter, issuesPage.getFilters().getActiveFilterName());
        assertEquals(resultsCountTotal, issuesPage.getResultsTable().getResultsCountTotal());
        assertEquals(url, issuesPage.getWindowLocation());

        if (jql != null)
        {
            assertEquals(jql, issuesPage.getAdvancedQuery().getCurrentQuery());
        }
    }
}