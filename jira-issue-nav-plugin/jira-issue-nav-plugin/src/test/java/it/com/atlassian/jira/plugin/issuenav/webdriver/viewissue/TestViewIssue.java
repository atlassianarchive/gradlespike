package it.com.atlassian.jira.plugin.issuenav.webdriver.viewissue;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.nimblefunctests.annotation.Restore;
import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.components.menu.IssuesMenu;
import com.atlassian.jira.pageobjects.dialogs.FormDialog;
import com.atlassian.jira.pageobjects.model.DefaultIssueActions;
import com.atlassian.jira.pageobjects.pages.viewissue.IssueMenu;
import com.atlassian.jira.pageobjects.pages.viewissue.MoveIssuePage;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.client.SlomoClient;
import com.atlassian.jira.plugin.issuenav.client.SlomoPattern;
import com.atlassian.jira.plugin.issuenav.pageobjects.AuiMessage;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueCommentsPanelComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueErrorComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.ReadyForAction;
import com.atlassian.jira.plugin.issuenav.pageobjects.ResultsTableComponent;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.pageobjects.DelayedBinder;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.google.common.base.Function;
import it.com.atlassian.jira.plugin.issuenav.webdriver.KickassWebDriverTestCase;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Tests common to both the pushState and non-pushState variants of view issue.
 *
 * @since v6.0
 */
@Restore("TestInlineEdit.xml")
public abstract class TestViewIssue extends KickassWebDriverTestCase
{
    protected IssuesPage issuesPage;
    protected TraceContext traceContext;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        traceContext = product.getPageBinder().bind(TraceContext.class);
    }

    /**
     * JRADEV-16797 The assignee and reporter's avatars should be visible.
     */
    @Test
    public void testAssigneeReporterAvatars()
    {
        IssueDetailComponent issuePage = issuesPage.getResultsTable().navigateToIssueDetailPage("XSS-17");
        assertThat(issuePage.assignee().getAvatarURL(), containsString("/secure/useravatar?size=small&avatarId=10092"));
        assertThat(issuePage.reporter().getAvatarURL(), containsString("/secure/useravatar?size=small&ownerId=admin&avatarId=10060"));
    }

    /**
     * JRADEV-17425 Commenting should work.
     */
    @Test
    public void testComment()
    {
        // The bug only occurs if we navigate directly to the issue.

        product.getPageBinder().navigateToAndBind(IssueDetailPage.class, "XSS-17", "");

        IssueDetailComponent issuePage = product.getPageBinder().bind(IssueDetailComponent.class, "XSS-17");
        issuePage = issuePage.nextIssue();

        // This will time out if JRADEV-17425 isn't fixed.
        issuePage.addCommentUsingHeaderLink("OK, get a cup of tea.");

        issuePage.addCommentUsingFooterLink("OK, get a cup of coffee.");

        IssueCommentsPanelComponent commentsPanel = issuePage.openCommentsTabPanel();
        assertEquals(3, commentsPanel.getVisibleComments().size());

        assertEquals("OK, get a cup of tea.", commentsPanel.getVisibleComments().get(1).getContents());
        assertEquals("OK, get a cup of coffee.", commentsPanel.getVisibleComments().get(2).getContents());
    }

	@Test
	public void testViewIssueRedirectsToNewKeyAfterMove()
	{
		ViewIssuePage viewIssuePage = product.goToViewIssue("XSS-17");
		IssueMenu issueMenu = viewIssuePage.getIssueMenu();
		issueMenu.invoke(DefaultIssueActions.MOVE);
		final MoveIssuePage moveIssuePage = product.getPageBinder().bind(MoveIssuePage.class, "XSS-17");
		moveIssuePage.setNewProject("QA");
		assertThat(newArrayList(moveIssuePage.getIssueTypes()),
                equalTo(asList("Bug", "New Feature", "Task", "Improvement", "Wish")));
		final ViewIssuePage issuePage = moveIssuePage.next().next().move();
		assertEquals("Bug", issuePage.getDetailsSection().getIssueType());
		assertEquals("QA", issuePage.getProject());
		assertEquals("QA-37", issuePage.getIssueKey());

		// go to old URL
		product.getTester().getDriver().navigate().to(product.getProductInstance().getBaseUrl() + new ViewIssuePage("XSS-17").getUrl());
		ViewIssuePage newIssuePage = product.getPageBinder().bind(ViewIssuePage.class, "QA-37");
		assertEquals("QA-37", newIssuePage.getIssueKey());
		assertThat(product.getTester().getDriver().getCurrentUrl(), endsWith("/QA-37"));
	}

    @Test
    public void testEditComment()
    {
        IssueDetailComponent issuePage = chooseIssue("XSS-17");
        issuePage.editComment(10270, "edited comment");
        assertEquals("edited comment", issuePage.getComment(10270));
        int commentId = issuePage.addCommentUsingFooterLink("lalala");
        issuePage.editComment(commentId, "changed lalala");
        assertEquals("changed lalala", issuePage.getComment(commentId));
    }

    /**
     * You should be returned to issue search after deleting an issue.
     */
    @Test
    public void testDeleteIssue()
    {
        // Assumes we're not using stable search (uses a different trace key).
        IssueDetailComponent issuePage = chooseIssue("XSS-17");
        issuePage.deleteIssue("jira.search.finished");

        issuesPage = product.getPageBinder().bind(IssuesPage.class);
        assertFalse(issuesPage.getResultsTable().containsIssue("XSS-17"));

        // JRADEV-18917 Only occurs if we navigate directly to the issue with no search context.
        product.getPageBinder().navigateToAndBind(IssueDetailPage.class, "XSS-16");
        issuePage = product.getPageBinder().bind(IssueDetailComponent.class, "XSS-16");
        issuePage.deleteIssue("jira.search.finished");
        assertCurrentURLTimed("/issues/?jql=");
    }

    /**
     * JRADEV-17238 The quick edit dialog should refer to the correct issue.
     */
    @Test
    public void testEditDialog()
    {
        IssueDetailComponent issuePage = chooseIssue("XSS-17");
        FormDialog editDialog = issuePage.openEditDialog();
        assertTrue(editDialog.getTitle().contains("Edit Issue : XSS-17"));

        editDialog.close();
        issuesPage = issuePage.returnToSearch();

        issuePage = chooseIssue("XSS-16");
        editDialog = issuePage.openEditDialog();
        assertTrue(editDialog.getTitle().contains("Edit Issue : XSS-16"));
    }

    /**
     * Test that we can navigate to and from the view issue page.
     */
    @Test
    public void testNavToViewIssue()
    {
        IssueDetailComponent issuePage = chooseIssue("XSS-17");
        assertEquals(issuePage.getIssueKey(), "XSS-17");
        assertCurrentURL(getIssueURL("XSS-17", ""));

        // XSS-17 is still selected.
        issuesPage = issuePage.returnToSearch();
        assertEquals(issuesPage.getResultsTable().getSelectedIssueKey(), "XSS-17");
        issuePage = chooseIssue("XSS-16");
        assertEquals(issuePage.getIssueKey(), "XSS-16");
        assertCurrentURL(getIssueURL("XSS-16", ""));

        issuesPage = issuePage.returnToSearch();
        issuePage = goToIssuePageViaKey("o");
        assertEquals(issuePage.getIssueKey(), "XSS-16");
        assertCurrentURL(getIssueURL("XSS-16", ""));
    }

    /**
     * Regression test for JRADEV-17826
     */
    @Test
    public void testGoToActivityTab()
    {
        IssueDetailComponent issuePage = chooseIssue("XSS-17");
        assertTrue(issuePage.activityModuleDoesNotContainModules());
        String unselectedTab = issuePage.getHistoryIssueTabPanel().getAttribute("data-id");
        issuePage.clickIssueTabPanel(unselectedTab);
        assertTrue(issuePage.activityModuleDoesNotContainModules());
    }

    /**
     * JRADEV-17261 We should maintain the currently selected custom field tab after a successful inline edit.
     */
    @Test
    public void testCustomFieldTabs()
    {
        // Set a value in BULK-15's "Champ de texte" field so custom field tabs appear.
        Map<String, Object> fields = new HashMap<String, Object>();
        fields.put("customfield_10110", "Hello!");

        Map<String, Object> request = new HashMap<String, Object>();
        request.put("fields", fields);

        new IssueClient(environmentData).update("XSS-17", request);

        // Select the second custom field tab, then perform an inline edit. The tab should remain selected.
        IssueDetailComponent issuePage = issuesPage.getResultsTable().navigateToIssueDetailPage("XSS-17");
        issuePage.selectCustomFieldTab(1);
        issuePage.summary().editSaveWait("Aww yiss");
        assertEquals(1, issuePage.getSelectedCustomFieldTabIndex());
    }

    /**
     * Test that the browser's back button works from viewing an issue.
     */
    @Test
    public void testBrowserBackFromViewIssue()
    {
        chooseIssue("XSS-12");
        product.getTester().getDriver().navigate().back();
        issuesPage = product.getPageBinder().bind(IssuesPage.class);
        ResultsTableComponent resultTableComponent = issuesPage.getResultsTable();
        assertThat("Selected issue is XSS-12", resultTableComponent.getSelectedIssueKey(), containsString("XSS-12"));
    }

    @Test
    public void testReturnToSearchShowCorrectHighlightedIssue()
    {
        IssueDetailComponent issueDetailComponent = chooseIssue("XSS-5");
        issueDetailComponent.nextIssue();
        issueDetailComponent.nextIssue();
        issueDetailComponent.previousIssue();

        issuesPage = issueDetailComponent.returnToSearch();
        ResultsTableComponent resultTableComponent = issuesPage.getResultsTable();
        assertThat("Selected issue is XSS-4", "XSS-4", containsString(resultTableComponent.getSelectedIssueKey()));
        assertEquals("Paging should be on page one", "1–50 of 132", issuesPage.getResultsCount());

        issueDetailComponent = chooseIssue("BULK-84");
        issueDetailComponent.nextIssue(ReadyForAction.PSYCHO_READY).ready();
        issueDetailComponent.nextIssue(ReadyForAction.PSYCHO_READY).ready();

        issuesPage = issueDetailComponent.returnToSearch();
        resultTableComponent = issuesPage.getResultsTable();
        assertThat("Selected issue is BULK-82", "BULK-82", containsString(resultTableComponent.getSelectedIssueKey()));
        assertThat("Paging should be on page two", "51–100 of 132", containsString(issuesPage.getResultsCount()));
    }

    @Test
    public void testReturnSearchScrollsSelectedIssueIntoView()
    {
        ResultsTableComponent resultsTableComponent = issuesPage.getResultsTable();
        resultsTableComponent.goToNextPage();

        Tracer tracer = traceContext.checkpoint();
        resultsTableComponent.selectPreviousIssue();
        issuesPage.waitForStableUpdate(tracer);

        issuesPage.scrollToTop();
        assertEquals(false, resultsTableComponent.getSelectedIssue().isInView());

        String selectedIssueKey = resultsTableComponent.getSelectedIssueKey();
        issuesPage = chooseIssue(selectedIssueKey).returnToSearch();
        assertEquals(true, resultsTableComponent.getSelectedIssue().isInView());
    }

    @Test
    public void testReturnToSearchAfterTransitioning() throws InterruptedException
    {
        actuallyTestReturnToSearchAfterTransitioning("XSS-2", new Function<IssueDetailComponent, IssuesPage>()
        {
            @Override
            public IssuesPage apply(final IssueDetailComponent input)
            {
                return input.returnToSearch();
            }
        });

        actuallyTestReturnToSearchAfterTransitioning("XSS-9", new Function<IssueDetailComponent, IssuesPage>()
        {
            @Override
            public IssuesPage apply(final IssueDetailComponent input)
            {
                return input.returnToSearchUsingKeyboardShortcut();
            }
        });

        actuallyTestReturnToSearchAfterTransitioning("XSS-10", new Function<IssueDetailComponent, IssuesPage>()
        {
            @Override
            public IssuesPage apply(final IssueDetailComponent input)
            {
                return input.returnToSearchUsingIssuesMenu();
            }
        });
    }

    private void actuallyTestReturnToSearchAfterTransitioning(String issueKey, final Function<IssueDetailComponent, IssuesPage> function) throws InterruptedException
    {
        final long delay = TimeUnit.SECONDS.toMillis(1);
        final SlomoClient slomoClient = new SlomoClient(environmentData);

        IssueDetailComponent issuePage = chooseIssue(issueKey);
        SlomoPattern slomoPattern = slomoClient.setDelay(".*AjaxIssueAction!default\\.jspa.*", delay);

        try
        {
            issuePage.startProgress("jira.issue.update");
            function.apply(issuePage);
            Thread.sleep(2 * delay);
            assertFalse(issuePage.isAt());
        }
        finally
        {
            slomoClient.removeDelay(slomoPattern.id);
        }
    }

    /**
     * JRADEV-20326
     * XSS-12 is chosen as this is a non-cached issue, if XSS-17(cached) is chosen this test fails. See issue for description
     * why.
     */
    @Test
    public void testGoToViewInvalidIssueShowsError() throws IOException
    {
        backdoor.project().deleteProject("XSS");
        ResultsTableComponent resultsTableComponent = issuesPage.getResultsTable();
        AuiMessage issueDoesNotExistPage = resultsTableComponent.navigateAndExpectError("XSS-12", getIssueURL("XSS-12", ""));
        assertThat("Test issue does not exist", issueDoesNotExistPage.getText(), containsString(getErrorMessage()));
        /*
         With AJAX view issue on, the error will no longer be a full page error
         It will show an inline error and will not change the URL
         */
        assertCurrentURL(getInvalidIssueURL("XSS-12", ""));
    }

    /**
     * Test that the "next/previous issue" links work.
     */
    @Test
    public void testNextPreviousIssue()
    {
        issuesPage = jqlSearch("project = BLUK");
        IssueDetailComponent issuePage = chooseIssue("BLUK-6");
        assertTrue(issuePage.hasNextPreviousPager());
        assertTrue(issuePage.hasNextIssue());
        assertFalse(issuePage.hasPreviousIssue());
        assertEquals("1 of 3", issuePage.getPositionText());

        issuePage = issuePage.nextIssue();
        assertEquals("BLUK-5", issuePage.getIssueKey());
        assertTrue(issuePage.hasNextPreviousPager());
        assertTrue(issuePage.hasNextIssue());
        assertTrue(issuePage.hasPreviousIssue());
        assertEquals("2 of 3", issuePage.getPositionText());

        issuePage = issuePage.nextIssue();
        assertEquals("BLUK-4", issuePage.getIssueKey());
        assertTrue(issuePage.hasNextPreviousPager());
        assertFalse(issuePage.hasNextIssue());
        assertTrue(issuePage.hasPreviousIssue());
        assertEquals("3 of 3", issuePage.getPositionText());

        issuePage = issuePage.previousIssue();
        assertEquals("BLUK-5", issuePage.getIssueKey());
        assertTrue(issuePage.hasNextPreviousPager());
        assertTrue(issuePage.hasNextIssue());
        assertTrue(issuePage.hasPreviousIssue());
        assertEquals("2 of 3", issuePage.getPositionText());

        // The correct issue should be highlighted in the issue table.
        issuesPage = issuePage.returnToSearch();
        assertEquals("BLUK-5", issuesPage.getResultsTable().getSelectedIssue().getSelectedIssueKey());

        // The pager should still be shown for result sets of one issue.
        issuesPage = jqlSearch("key = XSS-17");
        issuePage = chooseIssue("XSS-17");
        assertTrue(issuePage.hasNextPreviousPager());
        assertFalse(issuePage.hasNextIssue());
        assertFalse(issuePage.hasPreviousIssue());
        assertEquals("1 of 1", issuePage.getPositionText());
    }

    @Test
    public void testViewIssueWithCommentPermaLinkRespectFocusedComment()
    {
        String frag = getIssueURL("XSS-17") + "?" + getUrlFragmentForCommentPermaLink("10270");
        IssueDetailComponent issueDetailComponent = directNavigateToViewIssueUrl(frag, "XSS-17");

        waitUntilTrue("The comment should be focused", issueDetailComponent.commentIsFocused("10270"));
        assertCurrentURL(this.getBrowserUrlAfterLoadingWithCommentPermaLink("XSS-17", "10270"));
    }


    @Test
    public void testViewIssueShouldRespectTwixiePreferences()
    {
        issuesPage.getResultsTable();
        IssueDetailComponent issueDetailComponent = chooseIssue("BULK-84");

        // Re-open it before starting this test
        // Note: the state of the twixie is saved on client's localStorage and it doesn't get reset by @Restore()
        // Sometimes the details twixie is already closed because another left it closed
        if (!issueDetailComponent.isDetailsTwixieOpen())
        {
            issueDetailComponent.clickDetailsTwixie();
        }

        assertEquals("Details block should be expanded", true, issueDetailComponent.isDetailsTwixieOpen());

        issueDetailComponent.clickDetailsTwixie();
        assertEquals("Details block should be collapsed", false, issueDetailComponent.isDetailsTwixieOpen());

        issueDetailComponent.returnToSearch().waitForResultsTable();
        issueDetailComponent = chooseIssue("BULK-84");
        assertEquals("Details block should be collapsed", false, issueDetailComponent.isDetailsTwixieOpen());

        String frag = getIssueURL("BULK-84");
        issueDetailComponent = directNavigateToViewIssueUrl(frag, "BULK-84");
        assertEquals("Details block should be collapsed", false, issueDetailComponent.isDetailsTwixieOpen());

        //Reset the Details Twixie to avoid problems with other tests
        issueDetailComponent.clickDetailsTwixie();
    }

    @Test
    public void testIssueActionsKeepTheCorrectURLOnStandaloneViewIssue()
    {
        String frag = getIssueURL("BULK-84");
        IssueDetailComponent issue = directNavigateToViewIssueUrl(frag, "BULK-84");
        assertCurrentURLTimed("The url should be " + frag + " on page load", frag);

        issue.addCommentUsingFooterLink("hahahaha");
        assertCurrentURLTimed("The url should still be " + frag + " after adding a comment", frag);

        issue.addCommentUsingHeaderLink("hahahaha");
        assertCurrentURLTimed("The url should still be " + frag + " after adding a comment", frag);

        issue.startProgress("jira.issue.update");
        assertCurrentURLTimed("The url should still be " + frag + " after doing a workflow transition", frag);

        issue.summary().editSaveWait("POKEMON! Gotta Catch'em Alllllll");
        assertCurrentURLTimed("The url should still be " + frag + " after doing an inline edit", frag);
    }

    @Test
    public void testDefaultCommentCollapsingAmount()
    {
        IssueDetailComponent issueDetailComponent = loadIssueFromTable("BULK-4");

        IssueCommentsPanelComponent commentsPanel = issueDetailComponent.openCommentsTabPanel();
        assertEquals("Number of comments has not been limited", 6, commentsPanel.getVisibleComments().size());
    }

    @Test
    public void testNumberOfCommentsCollapsedCorrectly()
    {
        backdoor.applicationProperties().setString(APKeys.COMMENT_COLLAPSING_MINIMUM_HIDDEN, "4");
        IssueDetailComponent issueDetailComponent = loadIssueFromTable("BULK-4");

        IssueCommentsPanelComponent commentsPanel = issueDetailComponent.openCommentsTabPanel();
        assertEquals("Number of comments is limited", 6, commentsPanel.getVisibleComments().size());
    }

    @Test
    public void testDisableCommentCollapsingDoesNotLimitComments()
    {
        backdoor.applicationProperties().setString(APKeys.COMMENT_COLLAPSING_MINIMUM_HIDDEN, "0");

        IssueDetailComponent issueDetailComponent = loadIssueFromTable("BULK-4");

        IssueCommentsPanelComponent commentsPanel = issueDetailComponent.openCommentsTabPanel();
        assertEquals("Number of comments has not been limited", 19, commentsPanel.getVisibleComments().size());
    }

    @Test
    public void testMinimumCommentsHiddenLargerThanPossibleHidden()
    {
        backdoor.applicationProperties().setString(APKeys.COMMENT_COLLAPSING_MINIMUM_HIDDEN, "15");
        backdoor.applicationProperties().setString("user.issues.per.page", "131");

        IssueDetailComponent issueDetailComponent = loadIssueFromTable("BULK-4");
        IssueCommentsPanelComponent commentsPanel = issueDetailComponent.openCommentsTabPanel();
        assertEquals("Number of comments has not been limited", 19, commentsPanel.getVisibleComments().size());
    }

    @Test
    public void testShowMoreCommentsShowsAllComments()
    {
        IssueCommentsPanelComponent commentsPanel = loadIssueFromTable("BULK-4").openCommentsTabPanel().clickShowMoreComments();
        assertEquals("Number of comments when showing all is correct", 19, commentsPanel.getVisibleComments().size());
    }

    @Test
    public void testLimitedCommentsSortedDescending()
    {
        assertLimitedCommentsAreInCorrectOrder(false);
    }

    @Test
    public void testLimitedCommentsSortedAscending()
    {
        assertLimitedCommentsAreInCorrectOrder(true);
    }

    private void assertLimitedCommentsAreInCorrectOrder(boolean ascending)
    {
        IssueDetailComponent issueDetailComponent = loadIssueFromTable("BULK-4");
        System.out.println("Openiing comment panel");
        IssueCommentsPanelComponent commentsPanel = issueDetailComponent.openCommentsTabPanel(ascending);

        List<Long> limitedComments = commentsPanel.getVisibleCommentsIds();
        List<Long> latestLimitedComments = ascending ?
                limitedComments.subList(0, limitedComments.size() - 1) :
                limitedComments.subList(1, limitedComments.size());

        commentsPanel = commentsPanel.clickShowMoreComments();

        List<Long> expandedComments = commentsPanel.getVisibleCommentsIds();
        List<Long> latestExpandedComments;
        if (ascending)
        {
            latestExpandedComments = expandedComments.subList(0, 5);
        }
        else
        {
            latestExpandedComments = expandedComments.subList(expandedComments.size() - 5, expandedComments.size());
        }
        assertEquals("Last 5 comments when limited must be the latest 5 comments", latestExpandedComments, latestLimitedComments);
        assertEquals("First comment when limited must be the first comment overall", expandedComments.get(0), limitedComments.get(0));
    }

    @Test
    public void testFocusedCommentInHiddenCommentsShowsAllComments()
    {
        String frag = getIssueURL("BULK-4") + "?" + getUrlFragmentForCommentPermaLink("10064");
        IssueDetailComponent issueDetailComponent = directNavigateToViewIssueUrl(frag, "BULK-4");
        IssueCommentsPanelComponent commentsPanel = issueDetailComponent.openCommentsTabPanel();

        assertTrue("Comment is not visible when it should be", commentsPanel.isCommentVisible(10064));
        assertFalse("Show more comments should not be showing", commentsPanel.isShowMoreCommentsVisible());
    }

    @Test
    public void testFocusedCommentWithinVisibleCommentsDoesNotShowAllComments()
    {
        String frag = getIssueURL("BULK-4") + "?" + getUrlFragmentForCommentPermaLink("10222");
        IssueDetailComponent issueDetailComponent = directNavigateToViewIssueUrl(frag, "BULK-4");
        IssueCommentsPanelComponent commentsPanel = issueDetailComponent.openCommentsTabPanel();

        assertTrue("Comment is not visible when it should be", commentsPanel.isCommentVisible(10222));
        assertTrue("Show more comments should be showing", commentsPanel.isShowMoreCommentsVisible());
    }

    @Test
    public void testIssueBelowCollapseThresholdShowsAllComments()
    {
        IssueDetailComponent issueDetailComponent = loadIssueFromTable("QA-21");
        IssueCommentsPanelComponent commentsPanel = issueDetailComponent.openCommentsTabPanel();

        assertEquals("Should be displaying all five comments", 5, commentsPanel.getVisibleComments().size());
        assertFalse("Show more comments should not be showing", commentsPanel.isShowMoreCommentsVisible());
    }

    @Test
    public void testAddingCommentDoesNotLosePager()
    {
        IssueDetailComponent issueDetailComponent = chooseIssue(issuesPage.getResultsTable().getSelectedIssueKey());

        issueDetailComponent.addCommentUsingFooterLink("Some generic comment comment");
        waitUntilTrue(issueDetailComponent.hasNextPreviousPagerTimed());

        issueDetailComponent.addCommentUsingHeaderLink("Some generic comment comment");
        waitUntilTrue(issueDetailComponent.hasNextPreviousPagerTimed());
    }

    @Test
    public void testIssueContentShowsShareButton()
    {
        IssueDetailComponent issue = chooseIssue(issuesPage.getResultsTable().getSelectedIssueKey());

        waitUntilTrue(issue.hasShareButton());

        issue.addCommentUsingFooterLink("test comment");
        waitUntilTrue(issue.hasShareButton());

        issue.addCommentUsingHeaderLink("test comment");
        waitUntilTrue(issue.hasShareButton());
    }

    @Test
    public void testPageTitle()
    {
        String issueTitle = "[XSS-17] asdasdasdasd 10:57am - Testing Bulk Move";
        String issueNavTitle = "Issue Navigator - Testing Bulk Move";

        // "normal" view issue
        {
            IssueDetailComponent issue = chooseIssue("XSS-17");
            waitUntil("View issue title should contain issue summary", pageTitle(), containsString(issueTitle));

            issue.returnToSearch();
            waitUntil("Title should be updated after returning to search", pageTitle(), containsString(issueNavTitle));
        }

        // server-rendered view issue
        {
            IssueDetailComponent issue = directNavigateToViewIssueUrl(getIssueURL("XSS-17", ""), "XSS-17");
            waitUntil("Server-rendered view issue title should contain issue summary", pageTitle(), containsString(issueTitle));

            issue.returnToSearch();
            waitUntil("Title should be updated after returning to search from server-rendered issue", pageTitle(), containsString(issueNavTitle));
        }
    }

    /**
     * JRADEV-19993 The tools menu should work after returning to search from a standalone issue.
     */
    @Test
    public void testToolsMenuAfterStandaloneIssue()
    {
        IssueDetailComponent issuePage = goToIssuePage("XSS-17");
        issuesPage = issuePage.returnToSearchUsingKeyboardShortcut();
    }

    @Test
    public void testMissingIssueReturnToSearch()
    {
        IssueErrorComponent errorPage = goToInvalidIssuePage("XSS-1234");
        errorPage.clickReturnToSearch();
    }

    @Test
    public void testWorkflowButtonMissingWhenLessThanThreeTransitions()
    {
        IssueDetailComponent issuePage = goToIssuePage("XSS-7");
        assertFalse(issuePage.hasMoreTransitions());

        issuePage = goToIssuePage("BULK-101");
        assertTrue(issuePage.hasMoreTransitions());

        issuePage.closeIssue("jira.issue.update");
        assertFalse(issuePage.hasMoreTransitions());

        issuePage = goToIssuePage("BULK-101");
        assertFalse(issuePage.hasMoreTransitions());
    }

    @Test
    public void testNonAsciiUsernameCanChangeIssueTabs()
    {
        String username = "тест +";
        backdoor.usersAndGroups().addUser(username);
        issuesPage = logInToIssuesPage(username, username);
        IssueDetailComponent issuePage = goToIssuePage("BULK-47");
        // Switch to history tab
        String historyTab = issuePage.getHistoryIssueTabPanel().getAttribute("data-id");
        issuePage = issuePage.clickIssueTabPanel(historyTab);
        assertEquals(historyTab, issuePage.getIssueTabPanel());
        // Switch back to comments tab and make sure comments are visible
        IssueCommentsPanelComponent commentsTabPanel = issuePage.openCommentsTabPanel();
        assertTrue(commentsTabPanel.getVisibleComments().size() > 0);
    }

    @Test
    public void testOpeningAnIssueUpdatesCurrentProject()
    {

        IssueDetailComponent issue = loadIssueFromSearch("BLUK-6", "project in (BULK, BLUK) ORDER BY key ASC");
        JiraHeader jiraHeader = product.getPageBinder().bind(JiraHeader.class);
        jiraHeader.getProjectsMenu().open();
        assertThat("The Current Project is Bulk Move 2 (BLUK)",
                jiraHeader.getProjectsMenu().getCurrentProject().getText(), is("Bulk Move 2 (BLUK)"));

        issue.nextIssue();
        jiraHeader = product.getPageBinder().bind(JiraHeader.class);
        jiraHeader.getProjectsMenu().open();
        assertThat("The Current Project is Bulk Move 1 (BULK)",
                jiraHeader.getProjectsMenu().getCurrentProject().getText(), is("Bulk Move 1 (BULK)"));

        goToIssuePage("XSS-7");
        jiraHeader = product.getPageBinder().bind(JiraHeader.class);
        jiraHeader.getProjectsMenu().open();
        assertThat("The Current Project is XSS",
                jiraHeader.getProjectsMenu().getCurrentProject().getText(), is("<iframe src=\"http://www.google.com\"></iframe> (XSS)"));
    }

    @Test
    public void testStalkerBarFollowsTheScroll()
    {
        IssueDetailComponent issuePage = goToIssuePage("QA-34");
        issuePage.bringCommentButtonIntoView();
        assertTrue("The stalker bar is still visible", issuePage.isStalkerPinnedToTopOfWindow());
    }

    private IssueDetailComponent loadIssueFromSearch(String issueKey, String search)
    {
        issuesPage = jqlSearch(search);
        issuesPage.getResultsTable();
        return chooseIssue(issueKey);
    }

    private IssueDetailComponent loadIssueFromTable(String issueKey)
    {
        return loadIssueFromSearch(issueKey, "issuekey = " + issueKey);
    }

    protected abstract String getBrowserUrlAfterLoadingWithCommentPermaLink(String issueKey, String commentId);

    protected abstract String getErrorMessage();

    /**
     * @return the trace key that signifies a filter has loaded after clicking
     *         it in the header's "issues" dropdown.
     */
    protected abstract String getHeaderFilterTraceKey();

    protected String getSearchFinishedTraceKey()
    {
        return "jira.search.finished";
    }

    /**
     * @return trace key sent when the issue search done via page reload.
     */
    protected String getPageReloadSearchFinishedTraceKey()
    {
        return "jira.search.finished.initial";
    }

    /**
     * @return trace key sent when the issue search was performed via AJAX.
     */
    protected String getAJAXSearchFinishedTraceKey()
    {
        return "jira.search.finished.secondary";
    }

    /**
     * @return the issues menu in the header (open).
     */
    protected IssuesMenu getIssuesMenu()
    {
        JiraHeader header = product.getPageBinder().bind(JiraHeader.class);
        return header.getIssuesMenu().open();
    }

    protected abstract String getIssueURL(String issueKey, String context);

    /**
     * This is the URL that will be returned when you try to view an invalid issue
     * <p/>
     * In the case of AJAX view issue, clicking on issue from issue nav will no longer show
     * a full page error. It will show an inline error instead. Thus the URL will not change.
     * <p/>
     * However, in the case of page pop view issue, the url will actually change.
     *
     * @param issueKey
     * @param context
     * @return
     */
    protected abstract String getInvalidIssueURL(String issueKey, String context);

    /**
     * @return the trace key that signifies we have returned to issue search.
     */
    protected abstract String getReturnToSearchTraceKey();

    /**
     * @return the trace key that signifies that view issue has loaded.
     */
    protected abstract String getViewIssueTraceKey();

    /**
     * Generates the URL fragment to use in order to link to a comment
     *
     * @param commentId CommentID to link
     * @return URL fragment
     */
    protected String getUrlFragmentForCommentPermaLink(String commentId)
    {
        return "focusedCommentId=" + commentId + "&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-" + commentId;
    }

    /**
     * Select an issue in the results table to view it.
     *
     * @param issueKey The issue's key.
     * @return the issue page object.
     */
    protected IssueDetailComponent chooseIssue(String issueKey)
    {
        Tracer checkpoint = traceContext.checkpoint();
        IssueDetailComponent issueDetailComponent = issuesPage.getResultsTable().navigateToIssueDetailPage(issueKey);

        traceContext.waitFor(checkpoint, getViewIssueTraceKey());
        return issueDetailComponent;
    }

    /**
     * Wait for the current issue to be refreshed.
     *
     * @param tracer The tracer to poll.
     */
    protected void waitForIssueRefresh(Tracer tracer)
    {
        traceContext.waitFor(tracer, getViewIssueTraceKey());
    }

    protected static class CanBind extends AbstractTimedCondition
    {
        private final DelayedBinder binder;

        public CanBind(DelayedBinder binder)
        {
            super(Timeouts.DEFAULT_INTERVAL, Timeouts.DEFAULT_INTERVAL);
            this.binder = binder;
        }

        @Override
        protected Boolean currentValue()
        {
            return binder.canBind();
        }
    }

    /**
     * View the currently highlighted issue by pressing a given key.
     *
     * @param key The key to press.
     */
    protected IssueDetailComponent goToIssuePageViaKey(CharSequence key)
    {
        Tracer checkpoint = traceContext.checkpoint();
        IssueDetailComponent issueDetailComponent = issuesPage.getResultsTable().selectIssueUsingKey(key);
        traceContext.waitFor(checkpoint, getViewIssueTraceKey());
        return issueDetailComponent;
    }

    /**
     * An utility function to help directly navigate to an URL, check whether it can navigate
     * and return the correct page object
     *
     * @param fragment fragment url without the base url and context path
     * @return IssueDetailComponent
     */
    protected IssueDetailComponent directNavigateToViewIssueUrl(String fragment, String issueKey)
    {
        String urlToGo = product.getProductInstance().getBaseUrl() + fragment;
        product.getTester().getDriver().navigate().to(urlToGo);
        final PageBinder binder = product.getPageBinder();
        final DelayedBinder<IssueDetailComponent> issueBinder = binder.delayedBind(IssueDetailComponent.class, issueKey);
        TimedQuery<Boolean> canBind = new CanBind(issueBinder);
        waitUntilTrue("Failed to bind to " + issueKey + " on navigating via url with search context", canBind);

        return issueBinder.get();
    }
}
