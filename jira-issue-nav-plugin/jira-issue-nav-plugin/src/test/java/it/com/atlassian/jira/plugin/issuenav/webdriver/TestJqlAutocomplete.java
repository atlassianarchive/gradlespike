package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.plugin.issuenav.pageobjects.AdvancedQueryComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.pageobjects.elements.PageElement;
import org.junit.Test;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.user.preferences.PreferenceKeys.USER_JQL_AUTOCOMPLETE_DISABLED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Danger, Will Robinson! This test requires focus of the window to pass.
 * <p/>
 * *flails arms dangerously*
 */
public class TestJqlAutocomplete extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;
    private static final List<String> FIELDS = Arrays.asList("project", "assignee", "createdDate", "filter", "affectedVersion");
    private static final List<String> RANGE_OPERATORS = Arrays.asList("<=", ">=", ">", "<");
    private static final List<String> EQUALITY_OPERATORS = Arrays.asList("=", "!=");
    private static final List<String> IS_EQUALITY_OPERATORS = Arrays.asList("is", "is not");
    private static final List<String> LIST_OPERATORS = Arrays.asList("in", "not in");
    private static final List<String> CHANGE_HISTORY_OPERATORS = Arrays.asList("was", "was not", "changed");
    private static final List<String> CHANGE_HISTORY_LIST_OPERATORS = Arrays.asList("was in", "was not in");

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreBlankInstance();
        issuesPage = logInToIssuesPageAsSysadmin();
    }

    /**
     * Please excuse this parent test method
     *
     * TODO Implement "RestoreOnce" annotation.
     */
    @Test
    public void testJqlAutoCompleteAndJqlValidity() {
        jqlAutoCompleteSuggestsFields();

        jqlAutoCompleteSuggestsCorrectAssigneeOperators();
        jqlAutoCompleteSuggestsCorrectDateOperators();
        jqlAutoCompleteSuggestsCorrectFilterOperators();

        jqlAutoCompleteOperandsForAssignee();
        jqlAutoCompleteOperandsForCreatedDate();
        jqlValidityIsDeterminedCorrectly();
    }

    @Test
    public void jqlAutocompleteShouldIgnorePerUserSetting()
    {
        // set the fred's preference to disable JQL autocomplete. this setting is no longer exposed in the UI but
        // users could have set it before upgrading to 5.2 and it gets carried forward
        backdoor.userProfile().preferences(FRED_USERNAME).set(USER_JQL_AUTOCOMPLETE_DISABLED, true);
        issuesPage = logInToIssuesPage(FRED_USERNAME, FRED_PASSWORD);

        final String field = "project";
        AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.typePartialQuery(field);
        PageElement suggestions = advancedQueryComponent.getAutocompleteSuggestions();
        assertEquals("auto complete should be enabled despite user preference (JRADEV-18221)", suggestions.find(By.cssSelector("b")).getText(), field);
    }

    @Test
    public void jqlEnterKeyShouldSubmitTheSearchIfAutocompleteIsOpened()
    {
        issuesPage = logInToIssuesPage(FRED_USERNAME, FRED_PASSWORD);

        final String field = "project = HSP";
        AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.typePartialQuery(field);
        PageElement suggestions = advancedQueryComponent.getAutocompleteSuggestions();
        assertTrue("Suggestions dropdown is present", suggestions.isVisible());

        issuesPage = advancedQueryComponent.submitAndWait();
        assertTrue("A new search has been performed", issuesPage.isQueryVisible());
    }


    private void jqlAutoCompleteSuggestsFields()
    {
        for (String field : FIELDS)
        {
            AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
            advancedQueryComponent.typePartialQuery(field);
            PageElement suggestions = advancedQueryComponent.getAutocompleteSuggestions();
            assertEquals(suggestions.find(By.cssSelector("b")).getText(), field);
        }
    }

    private void jqlAutoCompleteSuggestsCorrectAssigneeOperators()
    {
        AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.typePartialQuery("assignee ");
        PageElement suggestions = advancedQueryComponent.getAutocompleteSuggestions();
        List<String> suggestionsOperators = new ArrayList<String>();

        for (PageElement operator : suggestions.findAll(By.tagName("li")))
        {
            suggestionsOperators.add(operator.getText());
        }

        final List<String> operators = new ArrayList<String>();
        operators.addAll(EQUALITY_OPERATORS);
        operators.addAll(IS_EQUALITY_OPERATORS);
        operators.addAll(LIST_OPERATORS);
        operators.addAll(CHANGE_HISTORY_OPERATORS);
        operators.addAll(CHANGE_HISTORY_LIST_OPERATORS);

        for (String operator : operators)
        {
            assertTrue(suggestionsOperators.contains(operator));
        }
    }

    private void jqlAutoCompleteSuggestsCorrectDateOperators()
    {
        AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.typePartialQuery("createdDate ");
        PageElement suggestions = advancedQueryComponent.getAutocompleteSuggestions();
        List<String> suggestionsOperators = new ArrayList<String>();

        for (PageElement operator : suggestions.findAll(By.tagName("li")))
        {
            suggestionsOperators.add(operator.getText());
        }

        final List<String> operators = new ArrayList<String>();
        operators.addAll(EQUALITY_OPERATORS);
        operators.addAll(IS_EQUALITY_OPERATORS);
        operators.addAll(LIST_OPERATORS);
        operators.addAll(RANGE_OPERATORS);

        for (String operator : operators)
        {
            assertTrue(suggestionsOperators.contains(operator));
        }
    }

    private void jqlAutoCompleteSuggestsCorrectFilterOperators()
    {
        AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.typePartialQuery("filter ");
        PageElement suggestions = advancedQueryComponent.getAutocompleteSuggestions();
        List<String> suggestionsOperators = new ArrayList<String>();

        for (PageElement operator : suggestions.findAll(By.tagName("li")))
        {
            suggestionsOperators.add(operator.getText());
        }

        final List<String> operators = new ArrayList<String>();
        operators.addAll(EQUALITY_OPERATORS);
        operators.addAll(LIST_OPERATORS);

        for (String operator : operators)
        {
            assertTrue(suggestionsOperators.contains(operator));
        }
    }

    private void jqlAutoCompleteOperandsForAssignee()
    {
        final List<String> operands = Arrays.asList("Administrator - admin@example.com (admin)",
                "Fred Normal - fred@example.com (fred)",
                "JIRA Administrator - jiraadmin@example.com (jiraadmin)",
                "JIRA Developer - jiradev@example.com (jiradev)",
                "System Administrator - sysadmin@example.com (sysadmin)", "currentUser()");

        AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.typePartialQuery("assignee = ");
        PageElement suggestions = advancedQueryComponent.getAutocompleteSuggestions();
        List<String> suggestionsOperands = new ArrayList<String>();

        for (PageElement suggestion : suggestions.findAll(By.tagName("li")))
        {
            suggestionsOperands.add(suggestion.getText());
        }

        for (String operand : operands)
        {
            assertTrue("Failed for operand '" + operand + "'", suggestionsOperands.contains(operand));
        }
    }

    private void jqlAutoCompleteOperandsForCreatedDate()
    {
        final List<String> operands = Arrays.asList("currentLogin()", "endOfDay()", "endOfMonth()", "endOfWeek()",
                "endOfYear()", "lastLogin()", "now()", "startOfDay()", "startOfMonth()", "startOfWeek()",
                "startOfYear()");

        AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.typePartialQuery("createdDate < ");
        PageElement suggestions = advancedQueryComponent.getAutocompleteSuggestions();
        List<String> suggestionsOperands = new ArrayList<String>();

        for (PageElement suggestion : suggestions.findAll(By.tagName("li")))
        {
            suggestionsOperands.add(suggestion.getText());
        }

        for (String operand : operands)
        {
            assertTrue(suggestionsOperands.contains(operand));
        }
    }

    private void jqlValidityIsDeterminedCorrectly()
    {
        AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.typePartialQuery("something");
        assertFalse(advancedQueryComponent.jqlQueryIsValid());

        advancedQueryComponent.typePartialQuery("something = somethingelse");
        assertTrue(advancedQueryComponent.jqlQueryIsValid());

        advancedQueryComponent.typePartialQuery("something =");
        assertFalse(advancedQueryComponent.jqlQueryIsValid());

        // Change history validity
        advancedQueryComponent.typePartialQuery("something was somethingelse");
        assertTrue(advancedQueryComponent.jqlQueryIsValid());

        advancedQueryComponent.typePartialQuery("something in (a, list");
        assertFalse(advancedQueryComponent.jqlQueryIsValid());

        advancedQueryComponent.typePartialQuery("something was somethingelse before sometime");
        assertTrue(advancedQueryComponent.jqlQueryIsValid());

        // Reserved word marks query as invalid
        advancedQueryComponent.typePartialQuery("field = somethingelse");
        assertFalse(advancedQueryComponent.jqlQueryIsValid());

        advancedQueryComponent.typePartialQuery("something was somethingelse before now()");
        assertTrue(advancedQueryComponent.jqlQueryIsValid());

        advancedQueryComponent.typePartialQuery("something changed into a monkey");
        assertFalse(advancedQueryComponent.jqlQueryIsValid());

        advancedQueryComponent.typePartialQuery("something was in (a, list)");
        assertTrue(advancedQueryComponent.jqlQueryIsValid());

        advancedQueryComponent.typePartialQuery("something was not in something or filter = something before now()");
        assertFalse(advancedQueryComponent.jqlQueryIsValid());

        advancedQueryComponent.typePartialQuery("something was not in (a, list)");
        assertTrue(advancedQueryComponent.jqlQueryIsValid());

        // JRADEV-13215 Discarding changes should reset the parse indicator.
        String filterId = backdoor.searchRequests().createFilter("admin",
                "issuetype = Bug", "Bugs", "");

        issuesPage = goToIssuesPage("?filter=" + filterId);
        advancedQueryComponent = issuesPage.getAdvancedQuery();
        assertTrue(advancedQueryComponent.jqlQueryIsValid());
        advancedQueryComponent.searchAndWait("lol jql");
        assertFalse(advancedQueryComponent.jqlQueryIsValid());
        issuesPage.discardFilterChanges();
        assertTrue(advancedQueryComponent.jqlQueryIsValid());
    }
}