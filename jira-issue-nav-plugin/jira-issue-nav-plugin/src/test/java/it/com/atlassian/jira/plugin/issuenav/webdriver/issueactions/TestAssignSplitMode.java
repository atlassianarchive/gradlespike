package it.com.atlassian.jira.plugin.issuenav.webdriver.issueactions;

import com.atlassian.jira.pageobjects.elements.GlobalMessage;
import com.atlassian.jira.pageobjects.pages.viewissue.ActionTrigger;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.SplitLayout;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.SplitViewSelectedIssue;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.query.Poller;
import it.com.atlassian.jira.plugin.issuenav.webdriver.KickassWebDriverTestCase;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public abstract class TestAssignSplitMode extends KickassWebDriverTestCase
{
    String key = "SUM-1";

    private PageBinder pageBinder;
    private SplitLayout splitLayout;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestIssueActions.xml");

        pageBinder = product.getPageBinder();
        TraceContext traceContext = pageBinder.bind(TraceContext.class);
        Tracer tracer = traceContext.checkpoint();

        IssuesPage issuesPage = logInToIssuesPageAsSysadmin("?jql=");
        splitLayout = issuesPage.getSplitLayout(tracer);
    }

    @Test
    public void testFromIssueActionsDialog()
    {
        perform(ActionTrigger.ACTIONS_DIALOG);
    }

    @Test
    public void testFromKeyboardShortcut()
    {
        perform(ActionTrigger.KEYBOARD_SHORTCUT);
    }

    @Test
    public void testFromMenu()
    {
        perform(ActionTrigger.MENU);
    }

    private void perform(ActionTrigger actionTrigger)
    {
        final SplitViewSelectedIssue selectedIssue = splitLayout.getSelectedIssue();
        assign(selectedIssue, actionTrigger);
        Poller.waitUntil(product.getPageBinder().bind(GlobalMessage.class).getMessage(), Matchers.containsString(key + " has been assigned."));
        IssueDetailComponent issue = pageBinder.bind(IssueDetailComponent.class, "SUM-1");
        issue.waitUntilAssigneeIs("admin");
        assertEquals("admin", issue.assignee().getValue());
    }

    abstract void assign(SplitViewSelectedIssue selectedIssue, ActionTrigger actionTrigger);
}
