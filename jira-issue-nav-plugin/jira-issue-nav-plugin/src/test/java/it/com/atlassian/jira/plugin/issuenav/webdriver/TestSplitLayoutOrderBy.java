package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.nimblefunctests.annotation.Restore;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.OrderByDropDown;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.OrderByPanel;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.SplitLayout;
import com.atlassian.pageobjects.elements.Option;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.Test;

import java.util.List;

import static com.atlassian.pageobjects.elements.query.Conditions.and;
import static com.atlassian.pageobjects.elements.query.Conditions.isEqual;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

/**
 * Tests for the split layout (including swapping to/from it).
 *
 * @since v6.0
 */
@Restore("TestInlineEdit.xml")
public class TestSplitLayoutOrderBy extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;
    private SplitLayout splitLayout;
    private TraceContext traceContext;
    private Tracer tracer;

    @Override
    protected void setUpTest()
    {
        // add a few custom fields that we can search for later
        for (int i = 0; i < 15; i++)
        {
            backdoor.customFields().createCustomField("Ze Field " + i, "Test field" + i, "com.atlassian.jira.plugin.system.customfieldtypes:version", "com.atlassian.jira.plugin.system.customfieldtypes:versionsearcher");
        }

        traceContext = product.getPageBinder().bind(TraceContext.class);
        tracer = traceContext.checkpoint();

        issuesPage = logInToIssuesPageAsSysadmin("?jql=");
        splitLayout = issuesPage.getSplitLayout(tracer);
        super.setUpTest();

        // clean slate for tests
        tracer = traceContext.checkpoint();
    }

    @Test
    public void newFilterShouldNotHaveOrder() throws Exception
    {
        OrderByPanel orderBy = splitLayout.getOrderByPanel();
        waitUntilTrue("new filter should not have order", and(
                isEqual("Order by", orderBy.getOrderByText()),
                isEqual(null, orderBy.getOrderByFieldId()),
                isEqual(null, orderBy.getOrderByDirection())
        ));
    }

    @Test
    public void myOpenIssuesShouldBeOrderedByUpdatedAscending() throws Exception
    {
        issuesPage.getFilters().selectFilter(-1);

        // My Open Issues
        OrderByPanel orderBy = splitLayout.waitForIssueAndResults(tracer).getOrderByPanel();
        waitUntilTrue("results should be ordered by updated ASC", and(
                isEqual("Order by Updated", orderBy.getOrderByText()),
                isEqual("updated", orderBy.getOrderByFieldId()),
                isEqual("DESC", orderBy.getOrderByDirection())
        ));
    }

    @Test
    public void advancedSearchShouldBeOrderedByOrderByClause() throws Exception
    {
        // order by lastViewed DESC
        issuesPage.getAdvancedQuery().searchAndWait("order by lastViewed DESC");
        OrderByPanel orderBy = splitLayout.waitForIssueAndResults(tracer).getOrderByPanel();
        waitUntilTrue("results should be ordered by lastViewed DESC", and(
                isEqual("Order by Last Viewed", orderBy.getOrderByText()),
                isEqual("lastViewed", orderBy.getOrderByFieldId()),
                isEqual("DESC", orderBy.getOrderByDirection())
        ));

        // order by lastViewed ASC
        tracer = traceContext.checkpoint();
        issuesPage.getAdvancedQuery().searchAndWait("order by lastViewed ASC");
        orderBy = splitLayout.waitForIssueAndResults(tracer).getOrderByPanel();
        waitUntilTrue("results should be ordered by lastViewed ASC", and(
                isEqual("Order by Last Viewed", orderBy.getOrderByText()),
                isEqual("lastViewed", orderBy.getOrderByFieldId()),
                isEqual("ASC", orderBy.getOrderByDirection())
        ));
    }

    @Test
    public void clickingTheOrderByTextShouldInvertTheSortOrder() throws Exception
    {
        issuesPage.getAdvancedQuery().searchAndWait("order by updated ASC");
        OrderByPanel orderBy = splitLayout.waitForIssueAndResults(tracer).getOrderByPanel();
        tracer = traceContext.checkpoint();
        orderBy.clickOrderByText();
        orderBy = splitLayout.waitForIssueAndResults(tracer).getOrderByPanel();
        waitUntilTrue("clicking 'order by' text should invert the sort order", and(
                isEqual("Order by Updated", orderBy.getOrderByText()),
                isEqual("updated", orderBy.getOrderByFieldId()),
                isEqual("DESC", orderBy.getOrderByDirection())
        ));

        Poller.waitUntilEquals("the 1st issue should be highlighted after changing sort", "ARA-1", splitLayout.issueList().getHighlightedIssueKey());
    }

    @Test
    public void serverRenderedSplitViewRespectsClickingOrderByText() throws Exception
    {
        //Login again with a search context after split view has been set as the preferred layout
        //to enable server rendered split view
        issuesPage = logInToIssuesPageAsSysadmin("?jql=");
        clickingTheOrderByTextShouldInvertTheSortOrder();
    }

    @Test
    public void clickingItemsInTheOrderByDropDownChangesTheSortOrder() throws Exception
    {
        issuesPage.getFilters().selectFilter(-4); // All Issues = order by created ASC)

        // My Open Issues
        OrderByPanel orderBy = splitLayout.waitForIssueAndResults(tracer).getOrderByPanel();

        // open the drop-down and sanity-check the options that are in there
        OrderByDropDown dropDown = orderBy.openDropDown();
        waitUntilTrue(dropDown.isOpen());
        List<Option> options = dropDown.getOptions().now();

        assertThat("field 'created' should not be in the drop-down because it's already in the order by text", options, not(hasItem(Options.full("", "created", "Created"))));
        assertThat("field 'Assignee' should be in the drop-down", options, hasItem(Options.full("", "assignee", "Assignee")));

        // now change the sort order
        tracer = traceContext.checkpoint();
        dropDown.selectOption("Assignee");
        orderBy = splitLayout.waitForIssueAndResults(tracer).getOrderByPanel();

        waitUntilTrue("straight up clicking on 'Assignee' should change the sort order", and(
                isEqual("Order by Assignee", orderBy.getOrderByText()),
                isEqual("assignee", orderBy.getOrderByFieldId()),
                isEqual("ASC", orderBy.getOrderByDirection())
        ));

        dropDown = orderBy.openDropDown();
        waitUntilTrue(dropDown.isOpen());
        options = dropDown.getOptions().now();

        assertThat("field 'created' should be in the drop-down because we're now sorting by 'created'", options, hasItem(Options.full("", "created", "Created")));
        assertThat("field 'assignee' should have been removed from the drop-down", options, not(hasItem(Options.full("", "assignee", "Assignee"))));
    }

    @Test
    public void orderByDropDownShowsTheFirstTenMatchingFields() throws Exception
    {
        issuesPage.getFilters().selectFilter(-4); // All Issues = order by created ASC)

        // My Open Issues
        OrderByPanel orderBy = splitLayout.waitForIssueAndResults(tracer).getOrderByPanel();

        // open the drop-down and sanity-check the options that are in there
        OrderByDropDown dropDown = orderBy.openDropDown();
        waitUntilTrue(dropDown.isOpen());
        List<Option> options = dropDown.getOptions().now();

        assertThat("order by drop-down should show first 10 fields only", options.size(), equalTo(10));

        // search for fi*
        dropDown.typeSearchText("fi");
        options = dropDown.getOptions().now();
        assertThat("Fix Version should be in the suggestions", options, hasItem(Options.full("", "fixVersions", "Fix Version/s")));
        assertThat(options.size(), equalTo(1));
    }

    @Test
    public void dropDownFooterIsVisibleWheneverThereAreMoreFieldsToShow() throws Exception
    {
        issuesPage.getFilters().selectFilter(-4); // All Issues = order by created ASC)
        OrderByPanel orderBy = splitLayout.waitForIssueAndResults(tracer).getOrderByPanel();

        // open the drop-down and make sure the footer is shown
        OrderByDropDown dropDown = orderBy.openDropDown();
        waitUntilTrue(dropDown.isOpen());

        assertThat("footer should be visible on initial render", dropDown.getFooterText().now(), containsString("more fields..."));
        dropDown.typeSearchText("z");
        assertThat("footer should be visible after searching for fields", dropDown.getFooterText().now(), containsString("more fields..."));
    }

    @Test
    public void orderByDropdownOnlyIncludesOrderableFields() throws Exception
    {
        issuesPage.getFilters().selectFilter(-4); // All Issues = order by created ASC)
        OrderByPanel orderBy = splitLayout.waitForIssueAndResults(tracer).getOrderByPanel();

        // open the drop-down and make sure the footer is shown
        OrderByDropDown dropDown = orderBy.openDropDown();
        waitUntilTrue(dropDown.isOpen());

        dropDown.typeSearchText("Images");
        List<Option> options = dropDown.getOptions().now();
        assertThat("order by drop-down should show first 10 fields only", options.size(), equalTo(0));
    }

    @Test
    public void testOrderByDropDownUsingKeyboardShortcut()
    {
        issuesPage.getFilters().selectFilter(-4); // All Issues = order by created ASC)
        OrderByPanel orderBy = splitLayout.waitForIssueAndResults(tracer).getOrderByPanel();

        // open the drop-down via keyboard shortcut and make sure it is open
        OrderByDropDown dropDown = orderBy.openViaKeyboardShortcut();
        waitUntilTrue(dropDown.isOpen());
    }
}
