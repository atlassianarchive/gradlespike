package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.jira.plugin.issuenav.pageobjects.AdvancedQueryComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import org.junit.Test;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RestoreOnce("TestJqlQuickSearch.xml")
public class TestJqlQuickSearch extends KickassWebDriverTestCase
{

    private IssuesPage issuesPage;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        issuesPage = logInToIssuesPageAsSysadmin();
    }

    @Test
    public void testPrefixedQuickSearchHandling()
    {
        issuesPage.quickSearch("c:comp");
        final AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        assertThat(advancedQueryComponent.getCurrentQuery(), containsString("component = comp"));
        assertEquals("monkey", issuesPage.getBasicQuery().project().getSelectedValue());
        assertEquals("comp", issuesPage.getBasicQuery().component().getSelectedValue());
        issuesPage.quickSearch("r:admin");
        assertThat(issuesPage.getAdvancedQuery().getCurrentQuery(), containsString("reporter = admin"));
        assertEquals("Administrator", issuesPage.getBasicQuery().reporter().getSelectedValue());
        issuesPage.quickSearch("r:none");
        assertThat(issuesPage.getAdvancedQuery().getCurrentQuery(), containsString("reporter is EMPTY"));
        assertEquals("No Reporter", issuesPage.getBasicQuery().reporter().getSelectedValue());

        issuesPage.quickSearch("MKY");
        assertThat(issuesPage.getAdvancedQuery().getCurrentQuery(), containsString("project = MKY"));
        assertEquals("monkey", issuesPage.getBasicQuery().project().getSelectedValue());
    }
}
