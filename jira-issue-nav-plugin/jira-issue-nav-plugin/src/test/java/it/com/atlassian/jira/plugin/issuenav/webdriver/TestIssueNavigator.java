package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.plugin.issuenav.pageobjects.*;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * @since v6.1
 */
public class TestIssueNavigator extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;

    private void assertIssuesInTable(String... issues)
    {
        assertIssuesInTable(issuesPage, issues);
    }

    @Test
    public void testSearchAbsoluteDateRangeDueDate()
    {
        administration.restoreData("TestIssueNavigatorSearchParams.xml");
        issuesPage = logInToIssuesPageAsSysadmin().getListLayout();

        issuesPage.getBasicQuery().openAddCriteriaDialog().selectAndOpenDueDate();
        PrimeCriteriaDateComponent duedate = issuesPage.getBasicQuery().duedate();

        duedate.dialog().setDateBetween("12/Sep/06", "");
        assertIssuesInTable("HSP-2");

        duedate.open().setDateBetween("10/Sep/06", "");
        assertIssuesInTable("HSP-2", "HSP-1");

        duedate.open().setDateBetween("", "14/Sep/06");
        assertIssuesInTable("HSP-2", "HSP-1");

        duedate.open().setDateBetween("", "12/Sep/06");
        assertIssuesInTable("HSP-1");

        duedate.open().setDateBetween("10/Sep/06", "14/Sep/06");
        assertIssuesInTable("HSP-2", "HSP-1");

        duedate.open().setDateBetween("15/Sep/06", "17/Sep/06");
        assertIssuesInTable();
    }

    @Test
    public void testSearchRelativeDateRangeDueDate()
    {
        administration.restoreData("TestIssueNavigatorSearchParams.xml");
        issuesPage = logInToIssuesPageAsSysadmin().getListLayout();

        final Date today = new Date();

        // We need to update the due dates to todays date so that we can build a proper relative range
        IssueDetailComponent issue = goToIssuePage("HSP-1");
        final String todayString = new SimpleDateFormat("dd/MMM/yy").format(today);
        issue.dueDate().editSaveWait(todayString);

        issuesPage = goToIssuesPage();

        issuesPage.getBasicQuery().openAddCriteriaDialog().selectAndOpenDueDate();
        PrimeCriteriaDateComponent duedate = issuesPage.getBasicQuery().duedate();

        duedate.dialog().setInRange("-1d", "1d");
        assertIssuesInTable("HSP-1");

        duedate.open().setInRange("2d", "4d");
        assertIssuesInTable();

        duedate.open().setInRange("-1d", "");
        assertIssuesInTable("HSP-1");

        duedate.open().setInRange("", "1d");
        assertIssuesInTable("HSP-1", "HSP-2");
    }

    @Test
    public void testSearchNumberCustomFieldParam()
    {
        administration.restoreData("TestIssueNavigatorSearchParams.xml");
        issuesPage = logInToIssuesPageAsSysadmin().getListLayout();

        TextRangeClauseDialog field = issuesPage.getBasicQuery()
                .openAddCriteriaDialog()
                .selectAndOpenSearcher("Number Field", "customfield_10000", TextRangeClauseDialog.class,
                        "", "searcher-customfield_10000:greaterThan", "searcher-customfield_10000:lessThan");

        PrimeCriteriaDateComponent lozenge = issuesPage.getBasicQuery().getCriteria("customfield_10000", PrimeCriteriaDateComponent.class);

        field.setRangeAndWait("0", "3");
        assertIssuesInTable("HSP-1", "HSP-2");

        lozenge.open();
        field.setRangeAndWait("1", "3");
        assertIssuesInTable("HSP-1", "HSP-2");

        lozenge.open();
        field.setRangeAndWait("0", "2");
        assertIssuesInTable("HSP-1", "HSP-2");

        lozenge.open();
        field.setRangeAndWait("5", "9");
        assertIssuesInTable();

        lozenge.open();
        field.setRangeAndWait("2", "");
        assertIssuesInTable("HSP-2");

        lozenge.open();
        field.setRangeAndWait("", "1");
        assertIssuesInTable("HSP-1");
    }
}
