package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.plugin.issuenav.pageobjects.ExtendedCriteriaComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.pageobjects.elements.Option;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * @since v6.1
 */
public class TestIssueNavigatorEncoding extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;

    private static final String PROJECT_14_INCH_MONITORS = "14\" monitors";
    private static final String PROJECT_SCRIPT_HACK = "<script>alert(\"hack\")</script>";

    @Test
    public void testProjectNameIsEncoded() throws Exception
    {
        administration.restoreData("TestIssueNavigatorEncoding.xml");
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();

        String[] namesInOrder = {PROJECT_14_INCH_MONITORS, PROJECT_SCRIPT_HACK};

        int count = 0;
        for (Option option : issuesPage.getBasicQuery().project().getOptions())
        {
            assertThat(option.text(), containsString(namesInOrder[count]));
            count++;
        }
        assertEquals(namesInOrder.length, count);
    }

    @Test
    public void testHtmlCustomFieldValuesNotDoubleEncoded()
    {
        // data contains a filter called "Fields with HTML Values" that has the following criteria:
        // My Multi Checkbox: <b>My Option</b>
        // My Multi Select: <b>My Option 2</b>
        // My Radio Buttons: <b>My Option 3</b>
        // My Select List: <b>My Option</b>
        // since the values already contain HTML, the view should not HTML encode them
        administration.restoreData("TestXssCustomFields.xml");
        issuesPage = logInToIssuesPageAsSysadmin("?filter=10010").getListLayout();
        assertEquals("Fields with HTML Values", issuesPage.getCurrentSearchTitle());

        ExtendedCriteriaComponent extended = issuesPage.getBasicQuery().extendedCriteria();

        assertThat(extended.getClause("customfield_10010").getRawViewHtml(), containsString("<b>My Option</b>"));
        assertThat(extended.getClause("customfield_10010").getRawViewHtml(), not(containsString("&lt;b&gt;My Option&lt;/b&gt;")));

        assertThat(extended.getClause("customfield_10011").getRawViewHtml(), containsString("<b>My Option 2</b>"));
        assertThat(extended.getClause("customfield_10011").getRawViewHtml(), not(containsString("&lt;b&gt;My Option 2&lt;/b&gt;")));

        assertThat(extended.getClause("customfield_10020").getRawViewHtml(), containsString("<b>My Option 3</b>"));
        assertThat(extended.getClause("customfield_10020").getRawViewHtml(), not(containsString("&lt;b&gt;My Option 3&lt;/b&gt;")));

        assertThat(extended.getClause("customfield_10013").getRawViewHtml(), containsString("<b>My Option</b>"));
    }

    @Test
    public void testCustomFieldValuesThatShouldBeEncoded()
    {
        // data contains a filter called "Fields that should be encoded" that has the following criteria:
        // My Free Text: <xxx>freetext</xxx>
        // My Group: <xxx>delta</xxx>
        // My Multi Group: <xxx>gamma</xxx>
        // My Multi User: <xxx>alpha</xxx>
        // My Text: <xxx>smalltext</xxx>
        // My User: <xxx>beta</xxx>
        // these values come largely from user input, and so should be encoded by the view when displayed
        // e.g. currently <xxx>alpha</xxx> is a valid user name
        administration.restoreData("TestXssCustomFields.xml");
        issuesPage = logInToIssuesPageAsSysadmin("?filter=10020").getListLayout();
        assertEquals("Fields that should be encoded", issuesPage.getCurrentSearchTitle());

        ExtendedCriteriaComponent extended = issuesPage.getBasicQuery().extendedCriteria();

        assertThat(extended.getClause("customfield_10030").getRawViewHtml(), containsString("&lt;xxx&gt;freetext&lt;/xxx&gt;"));
        assertThat(extended.getClause("customfield_10030").getRawViewHtml(), not(containsString("<xxx>freetext</xxx>")));

        assertThat(extended.getClause("customfield_10000").getRawViewHtml(), containsString("&lt;xxx&gt;delta&lt;/xxx&gt;"));
        assertThat(extended.getClause("customfield_10000").getRawViewHtml(), not(containsString("<xxx>delta</xxx>")));

        assertThat(extended.getClause("customfield_10001").getRawViewHtml(), containsString("&lt;xxx&gt;gamma&lt;/xxx&gt;"));
        assertThat(extended.getClause("customfield_10001").getRawViewHtml(), not(containsString("<xxx>gamma</xxx>")));

        assertThat(extended.getClause("customfield_10002").getRawViewHtml(), containsString("Alpha"));
        assertThat(extended.getClause("customfield_10002").getRawViewHtml(), not(containsString("<xxx>alpha</xxx>")));

        assertThat(extended.getClause("customfield_10004").getRawViewHtml(), containsString("&lt;xxx&gt;smalltext&lt;/xxx&gt;"));
        assertThat(extended.getClause("customfield_10004").getRawViewHtml(), not(containsString("<xxx>smalltext</xxx>")));

        assertThat(extended.getClause("customfield_10003").getRawViewHtml(), containsString("Beta"));
        assertThat(extended.getClause("customfield_10003").getRawViewHtml(), not(containsString("<xxx>beta</xxx>")));
    }
}
