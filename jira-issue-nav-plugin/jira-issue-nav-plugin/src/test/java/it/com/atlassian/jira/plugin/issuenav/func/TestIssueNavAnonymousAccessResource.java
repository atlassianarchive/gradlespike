package it.com.atlassian.jira.plugin.issuenav.func;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.jira.nimblefunctests.framework.NimbleFuncTestCase;
import com.atlassian.jira.plugin.issuenav.client.IssueNavAnonymousAccessClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import org.junit.Before;
import org.junit.Test;

import static javax.ws.rs.core.Response.Status;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@WebTest({ Category.FUNC_TEST })
@RestoreOnce("TestNavigation.xml")
public class TestIssueNavAnonymousAccessResource extends NimbleFuncTestCase
{
    private IssueNavAnonymousAccessClient client;

    @Before
    public void setUp()
    {
        client = new IssueNavAnonymousAccessClient(environmentData);
    }

    @Test
    public void testReturnsAccessibleForIssueVisibleByAnonymousUsers()
    {
        assertTrue(client.getAccessible("XSS-17", null).accessible);
    }

    @Test
    public void testReturnsUnauthorisedErrorForIssueNotVisibleByAnonymousUsers()
    {
        Response response = client.getResponse("QA-36", null);
        assertEquals(Status.UNAUTHORIZED.getStatusCode(), response.statusCode);
    }

    @Test
    public void testReturnsBadRequestErrorForNonExistingIssue()
    {
        Response response = client.getResponse("ABC-1", null);
        assertEquals(Status.BAD_REQUEST.getStatusCode(), response.statusCode);
    }

    @Test
    public void testReturnsUnauthorisedErrorForSystemFilter()
    {
        Response response = client.getResponse("XSS-17", -1L);
        assertEquals(Status.UNAUTHORIZED.getStatusCode(), response.statusCode);
    }
}
