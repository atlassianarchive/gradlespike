package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.ShareDialog;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.SplitLayout;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.*;

@RestoreOnce ("TestSharingIssues.xml")
public class TestSharingIssues extends KickassWebDriverTestCase
{
    @Test
    public void shareDialogIsDismissedOnScrollWhenOpenedViaButton()
    {
        SplitLayout splitLayout = logInToIssuesPageAsSysadmin("SUM-1?jql=").getSplitLayout();
        IssueDetailComponent issueDetail = splitLayout.issue();
        ShareDialog shareDialog = issueDetail.getShareDialog().open();
        issueDetail.scrollTo(1000, true);
        waitUntilFalse("Should have been dismissed.", shareDialog.isOpenTimed());
    }

    @Test
    public void shareDialogIsDismissedOnScrollWhenOpenedViaKeyboardShortcut()
    {
        SplitLayout splitLayout = logInToIssuesPageAsSysadmin("SUM-1?jql=").getSplitLayout();
        IssueDetailComponent issueDetail = splitLayout.issue();
        ShareDialog shareDialog = issueDetail.getShareDialog().openViaKeyboardShortcut();
        issueDetail.scrollTo(1000, true);
        waitUntilFalse("Should have been dismissed.", shareDialog.isOpenTimed());
    }

    @Test
    public void shareDialogIsScrolledIntoViewWhenOpenedViaKeyboardShortcut()
    {
        SplitLayout splitLayout = logInToIssuesPageAsSysadmin("SUM-1?jql=").getSplitLayout();
        IssueDetailComponent issueDetail = splitLayout.issue();
        issueDetail.scrollTo(1000, true);
        ShareDialog shareDialog = issueDetail.getShareDialog().openViaKeyboardShortcut();
        waitUntilTrue("Should have opened.", shareDialog.isOpenTimed());
        assertTrue("Expected the top of the dialog to be within the view port.", shareDialog.getLocation().getY() > 0);
    }
}
