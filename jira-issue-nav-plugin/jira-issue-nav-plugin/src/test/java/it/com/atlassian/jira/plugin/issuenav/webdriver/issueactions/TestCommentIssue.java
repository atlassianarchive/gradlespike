package it.com.atlassian.jira.plugin.issuenav.webdriver.issueactions;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.elements.GlobalMessage;
import com.atlassian.jira.pageobjects.navigator.SelectedIssue;
import com.atlassian.jira.pageobjects.pages.viewissue.ActionTrigger;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.query.Poller;
import it.com.atlassian.jira.plugin.issuenav.webdriver.KickassWebDriverTestCase;
import org.hamcrest.Matchers;
import org.junit.Test;

@WebTest ({ Category.WEBDRIVER_TEST, Category.ISSUES })
public class TestCommentIssue extends KickassWebDriverTestCase
{
    String key = "SUM-1";

    private IssuesPage issuesPage;
    private PageBinder pageBinder;
    private TraceContext traceContext;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestIssueActions.xml");
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();
        pageBinder = product.getPageBinder();
        traceContext = pageBinder.bind(TraceContext.class);
    }

    @Test
    public void testFromIssueActionsDialog()
    {
        perform(ActionTrigger.ACTIONS_DIALOG);
    }

    @Test
    public void testFromKeyboardShortcut()
    {
        perform(ActionTrigger.KEYBOARD_SHORTCUT);
    }

    @Test
    public void testFromCog()
    {
        perform(ActionTrigger.MENU);
    }

    private void perform(ActionTrigger actionTrigger)
    {
        final SelectedIssue selectedIssue = issuesPage.getResultsTable().getSelectedIssue();
        Tracer tracer = traceContext.checkpoint();
        selectedIssue.addComment("Hello Mate!", actionTrigger);
        issuesPage.waitForStableUpdate(tracer);
        Poller.waitUntil(pageBinder.bind(GlobalMessage.class).getMessage(), Matchers.containsString(key + " has been updated with your comment."));
    }
}