package it.com.atlassian.jira.plugin.issuenav.func.util.funcobject;

import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.locator.TableLocator;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.meterware.httpunit.WebTable;
import net.sourceforge.jwebunit.WebTester;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class IssueNavigator
{
    private final Navigation navigation;
    private final WebTester tester;

    public IssueNavigator(Navigation navigation, WebTester tester)
    {
        this.navigation = navigation;
        this.tester = tester;
    }

    public IssueNavigator openSearchWithJql(String jql)
    {
        navigation.gotoPage("/issues/?jql=" + jql);
        return this;
    }

    public boolean isResultEmpty()
    {
        return getIssueTable() == null && isEmptyIgnoringWhiteSpaces(".navigator-content");
    }

    public ImmutableMap<String, String> getResultRow(int row)
    {
        final WebTable table = getIssueTable();
        final Node[] headerCells = new CssLocator(tester, "#issuetable thead tr.rowHeader th").getNodes();
        final ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
        for (int i = 0; i < headerCells.length; i++)
        {
            final Node dataIdAttr = headerCells[i].getAttributes().getNamedItem("data-id");
            if (dataIdAttr != null)
            {
                builder.put(dataIdAttr.getNodeValue(), table.getTableCell(row, i).asText().trim());
            }
        }
        return builder.build();
    }

    private WebTable getIssueTable()
    {
        final TableLocator tableLocator = new TableLocator(tester, "issuetable");
        return tableLocator.getTable();
    }

    private boolean isEmptyIgnoringWhiteSpaces(String cssLocatorString)
    {
        final CssLocator nodeLocator = new CssLocator(tester, cssLocatorString);
        final Node node = nodeLocator.getNode();
        final NodeList childNodes = node.getChildNodes();

        // a real empty node
        if (!node.hasChildNodes())
        {
            return true;
        }

        // expect only one child - text element
        if (childNodes.getLength() > 1)
        {
            return false;
        }
        final Node subNode = childNodes.item(0);
        final String nodeValue = subNode.getNodeValue();
        return nodeValue == null || Strings.isNullOrEmpty(nodeValue.replaceAll("\\s", ""));
    }
}
