package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.jira.plugin.issuenav.pageobjects.*;
import com.atlassian.pageobjects.elements.Option;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.timeout.DefaultTimeouts;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

@RestoreOnce("TestClauseLozenges.xml")
public class TestClauseLozenges extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        issuesPage = logInToIssuesPageAsSysadmin("?jql=");
        issuesPage.getListLayout();
    }

    @Test
    public void testChangingProjectUpdatesStatusChoices()
    {
        long now = System.currentTimeMillis();
        int optionSize = issuesPage.getBasicQuery().status().getAllOptionsCount();
        assertEquals(20, optionSize);

        issuesPage = issuesPage.getBasicQuery().project().setValueAndSubmit("QA");
        assertEquals("QA", issuesPage.getBasicQuery().project().getSelectedValue());
        optionSize = issuesPage.getBasicQuery().status().getAllOptionsCount();
        assertEquals(5, optionSize);
        issuesPage.getBasicQuery().project().deselectAllAndSubmit();
        issuesPage = issuesPage.getBasicQuery().project().setValueAndSubmit("Bulk Move 1");
        assertEquals("Bulk Move 1", issuesPage.getBasicQuery().project().getSelectedValue());
        optionSize = issuesPage.getBasicQuery().status().getAllOptionsCount();
        assertEquals(20, optionSize);
        long then = System.currentTimeMillis() - now;
        System.out.println("time taken: " + String.valueOf(then));
    }


    /*
     * Regression test for JRADEV-14543 in anticipation of webdriver running with IE8.
     */
    @Test
    public void testClearingAndTypingSingleCharacterTriggersAutoComplete()
    {
        issuesPage = issuesPage.getBasicQuery().issueType().setSearchText("n");
        int optionCount = issuesPage.getBasicQuery().issueType().getAllOptionsCount();
        assertEquals(1, optionCount);
        issuesPage = issuesPage.getBasicQuery().issueType().clearSearchText();
        issuesPage = issuesPage.getBasicQuery().issueType().setSearchText("n");
        optionCount = issuesPage.getBasicQuery().issueType().getAllOptionsCount();
        assertEquals(1, optionCount);
    }

    @Test
    public void testClickingOutsideUpdatesSearch()
    {
        issuesPage.getBasicQuery().project().setValueAndClickOutside("QA");
        assertEquals("QA", issuesPage.getBasicQuery().project().getSelectedValue());
    }

    @Test
    public void testChangingProjectShowsCustomFieldSearcher()
    {

        AddCriteria addCriteria = issuesPage.getBasicQuery().openAddCriteriaDialog();
        assertFalse(addCriteria.searcherExists("customfield_10071"));
        MultiSelectClauseDialog sparkler = addCriteria.getSparkler();
        sparkler.typeSearchText("<");
        assertTrue(addCriteria.searcherExists("customfield_10071"));
        assertTrue(addCriteria.searcherDisabled("customfield_10071"));

        issuesPage = issuesPage.getBasicQuery().project().setValueAndSubmit("QA");
        assertEquals("QA", issuesPage.getBasicQuery().project().getSelectedValue());
        assertTrue(issuesPage.getBasicQuery().openAddCriteriaDialog().searcherExists("customfield_10071"));

        issuesPage = issuesPage.getBasicQuery().project().clear();
        assertEquals("All", issuesPage.getBasicQuery().project().getSelectedValue());
        assertFalse(issuesPage.getBasicQuery().openAddCriteriaDialog().searcherExists("customfield_10071"));
    }

    @Test
    public void testClearSelectedValuesDoesNotRemoveTheSearcher()
    {
        BasicQueryComponent basicQuery = issuesPage.getBasicQuery();
        basicQuery.openAddCriteriaDialog().selectAndOpenComponent().selectValues("Bacon", "cheese");
        basicQuery.openAddCriteriaDialog().close();
        basicQuery.component().open().clearSelectedItems();
        issuesPage.waitForResultsTable();
        assertTrue("Components searcher should still be in the extended list",
                basicQuery.extendedCriteria().clauseExists("component"));
    }

    @Test
    public void testLoadingWithSimpleSearchShowsComponentSearcher()
    {
        issuesPage = jqlSearch("project=BULK");
        assertEquals("Bulk Move 1", issuesPage.getBasicQuery().project().getSelectedValue());
        assertTrue(issuesPage.getBasicQuery().openAddCriteriaDialog().searcherExists("component"));
    }

    @Test
    public void testLoadingWithSimpleSearchShowsCustomFieldSearcher()
    {
        issuesPage = jqlSearch("project=QA");
        assertEquals("QA", issuesPage.getBasicQuery().project().getSelectedValue());

        assertTrue(issuesPage.getBasicQuery().openAddCriteriaDialog().searcherExists("customfield_10071"));
    }

    @Test
    public void testLoadingWithSimpleSearchShowsFieldValues()
    {
        issuesPage = jqlSearch("project=QA");
        assertEquals("QA", issuesPage.getBasicQuery().project().getSelectedValue());

        assertEquals(5, issuesPage.getBasicQuery().status().getAllOptionsCount());
    }

    @Test
    public void testAddAndRemoveComponent()
    {
        issuesPage = jqlSearch("project=BULK");
        assertEquals("Bulk Move 1", issuesPage.getBasicQuery().project().getSelectedValue());

        issuesPage.getBasicQuery().openAddCriteriaDialog().selectAndOpenComponent().selectAndWait("Bacon");
        assertFalse(issuesPage.getBasicQuery().hasInvalidExtendedCriteria());
        ExtendedCriteriaComponent component = issuesPage.getBasicQuery().extendedCriteria();
        List<ExtendedCriteriaComponent.Clause> clauses = component.getClauses();

        assertEquals(clauses.size(), 1);
        ExtendedCriteriaComponent.Clause componentClause = clauses.get(0);

        assertEquals(componentClause.getId(), "component");
        assertTrue(componentClause.getViewHtml().contains("Bacon"));
        assertTrue(componentClause.isValidClause());

        Set<String> selectedCriteria = issuesPage.getBasicQuery().openAddCriteriaDialog().getSelectedCriteria();
        assertEquals(ImmutableSet.of("component"), selectedCriteria);
        assertEquals(1, issuesPage.getBasicQuery().numExtendedCriteria());

        componentClause.remove();
        clauses = component.getClauses();
        assertEquals(clauses.size(), 0);

        selectedCriteria = issuesPage.getBasicQuery().openAddCriteriaDialog().getSelectedCriteria();
        assertEquals(ImmutableSet.<String>of(), selectedCriteria);
    }

    @Test
    public void testLozengeisValidWithValidState()
    {
        issuesPage = jqlSearch("project = BULK AND component = Bacon");
        assertEquals("Bulk Move 1", issuesPage.getBasicQuery().project().getSelectedValue());

        assertFalse(issuesPage.getBasicQuery().hasInvalidExtendedCriteria());
        ExtendedCriteriaComponent component = issuesPage.getBasicQuery().extendedCriteria();
        List<ExtendedCriteriaComponent.Clause> clauses = component.getClauses();

        assertEquals(clauses.size(), 1);
        ExtendedCriteriaComponent.Clause componentClause = clauses.get(0);

        assertEquals(componentClause.getId(), "component");
        assertTrue(componentClause.isValidClause());
    }
    
    @Test
    public void testClauseIsSelectedInAddListWhenSelected()
    {
        issuesPage.getBasicQuery().openAddCriteriaDialog().selectAndOpenPriority().selectAndWait("Major");
        assertEquals(1, issuesPage.getBasicQuery().numExtendedCriteria());
        Set<String> selectedCriteria = issuesPage.getBasicQuery().openAddCriteriaDialog().getSelectedCriteria();
        assertEquals(ImmutableSet.of("priority"), selectedCriteria);
    }

    @Test
    public void testClauseIsSelectedInAddListWhenPageLoaded()
    {
        issuesPage = jqlSearch("priority=Major");
        assertEquals(1, issuesPage.getBasicQuery().numExtendedCriteria());
        Set<String> selectedCriteria = issuesPage.getBasicQuery().openAddCriteriaDialog().getSelectedCriteria();
        assertEquals(ImmutableSet.of("priority"), selectedCriteria);
    }

    @Test
    public void testSwitchingToAdvancedAndBackAndSelectingAClauseAddsIt()
    {
        BasicQueryComponent queryComponent = issuesPage.getBasicQuery();
        queryComponent.openAddCriteriaDialog().selectAndOpenPriority();
        AddCriteria addCriteria = queryComponent.openAddCriteriaDialog();
        Set<String> selectedCriteria = addCriteria.getSelectedCriteria();
        assertEquals(1, selectedCriteria.size());
        assertEquals(ImmutableSet.of("priority"), selectedCriteria);
        addCriteria.close();

        assertTrue(queryComponent.extendedCriteria().clauseExists("priority"));

        issuesPage.getAdvancedQuery();
        queryComponent = issuesPage.getBasicQuery();

        assertTrue(queryComponent.extendedCriteria().clauseExists("priority"));

        queryComponent.openAddCriteriaDialog().selectAndOpenReporter();
        selectedCriteria = queryComponent.openAddCriteriaDialog().getSelectedCriteria();
        assertEquals(2, selectedCriteria.size());
        assertEquals(ImmutableSet.of("priority", "reporter"), selectedCriteria);

        assertTrue(queryComponent.extendedCriteria().clauseExists("priority"));
        assertTrue(queryComponent.extendedCriteria().clauseExists("reporter"));
    }

    /**
     * Tests that a dialog opened from the criterion is not closed when an invalid value error returns from the server
     */
//    @Test
//    public void testInvalidAssignee()
//    {
//        issuesPage.getBasicQuery().assignee().selectUserAndWait("i dont even exist", IssuesPage.class);
//        assertEquals("i dont even exist", issuesPage.getBasicQuery().assignee().getSelectedValue());
//
//        assertTrue(issuesPage.getBasicQuery().assignee().isDialogOpen());
//        assertEquals("Could not find username: i dont even exist", issuesPage.getBasicQuery().assignee().open().errorText());
//    }

    /**
     * Remove an invalid clause from the variable list. Verify that it does not get added to the "add" list
     */
//    @Test
//    public void testRemovingInvalidClauseDoesNotAddToAddList()
//    {
//        // Verify using remove individual
//        issuesPage = jqlSearch("component=Bacon");
//        BasicQueryComponent basic = issuesPage.getBasicQuery();
//        assertTrue(basic.hasInvalidExtendedCriteria());
//
//        basic.extendedCriteria().getClause("component").remove();
//        assertFalse(basic.openAddCriteriaDialog().searcherExists("component"));
//
//        // Verify using remove all
//        issuesPage = jqlSearch("component=Bacon");
//        basic = issuesPage.getBasicQuery();
//        assertTrue(basic.hasInvalidExtendedCriteria());
//
//        basic.clearFilters();
//        assertFalse(basic.openAddCriteriaDialog().searcherExists("component"));
//    }

//    @Test
//    public void testEscapingInvalidLozengeName()
//    {
//        String searcherName = "<iframe src=\"http://www.atlassian.com\"></iframe>";
//        String searcherValue = "<iframe src=\"http://www.google.com\"></iframe>";
//
//        // Verify escaping when valid
//        issuesPage = jqlSearch("project=QA");
//        BasicQueryComponent basic = issuesPage.getBasicQuery();
//        AddCriteria addFilterComponent = basic.openAddCriteriaDialog();
//        addFilterComponent.getGroup("CUSTOM").expand();
//
//        TextClauseDialog field = addFilterComponent.selectSearcher("customfield_10071", TextClauseDialog.class, searcherName, "searcher-customfield_10071");
//        field.setTextAndWait(searcherValue);
//
//        assertEquals(1, issuesPage.getBasicQuery().numExtendedCriteria());
//
//        assertTrue(basic.hasVariableFilters());
//        String viewHtml = basic.extendedCriteria().getClause("customfield_10071").getViewHtml();
//        assertTrue(viewHtml.contains(searcherName));
//        assertTrue(viewHtml.contains(searcherValue));
//
//        // Verify escaping when invalid
//        basic.project().setValueAndWait("-1");
//
//        assertEquals("All", basic.project().getSelectedValue());
//
//        assertTrue(basic.hasInvalidExtendedCriteria());
//        viewHtml = basic.extendedCriteria().getClause("customfield_10071").getViewHtml();
//        assertTrue(viewHtml.contains(searcherName));
//        assertTrue(viewHtml.contains("Invalid"));
//    }

    @Test
    public void testSparklerPageObject()
    {
        BasicQueryComponent basic = issuesPage.getBasicQuery();
        //test search?
        //add to project
        basic.project().setValueAndSubmit("QA");
        assertEquals(1, basic.project().open().getNumSelected());
        basic.project().deselectAllAndSubmit();
        //add multiple
        basic.project().setValueAndSubmit("QA", "Bulk Move 1");
        assertEquals(2, basic.project().open().getNumSelected());
        //add an intersection
        basic.project().setValueAndSubmit("Bulk Move 1", "BLAH");
        assertEquals(3, basic.project().open().getNumSelected());
        //try deselect some
        basic.project().removeValuesAndSubmit("Bulk Move 1");
        assertEquals(2, basic.project().open().getNumSelected());

        //selectAll all
        basic.project().deselectAllAndSubmit();
        assertEquals(0, basic.project().open().getNumSelected());
        basic.project().open().cancelWithEsc();
        //try open twice, should stay open
        assertFalse(basic.project().isDialogOpen());
        basic.project().open();
        assertTrue(basic.project().isDialogOpen());
        basic.project().open();
        assertTrue(basic.project().isDialogOpen());
        //regressiontest for add none behaviour
        basic.project().setValueAndSubmit("QA", "Bulk Move 1");
        basic.project().setValueAndSubmit();
        assertEquals(2, basic.project().open().getNumSelected());
        basic.project().removeValuesAndSubmit();
        assertEquals(2, basic.project().open().getNumSelected());
    }

    /**
     * An anonymous user's preferred search mode should be stored for the
     * duration of their session.
     */
    @Test
    public void testAnonymousSearchMode()
    {
        product.logout();
        issuesPage = goToIssuesPage();
        issuesPage.getAdvancedQuery();
        issuesPage = goToIssuesPage();
        waitUntilFalse(issuesPage.isBasicSearch());
    }
    
    @Test
    public void testDisablingAutoupdateRendersAnUpdateButtonInTheSearcher()
    {
        backdoor.applicationProperties().setOption(APKeys.JIRA_ISSUENAV_CRITERIA_AUTOUPDATE, false);
        issuesPage = goToIssuesPage();
        issuesPage.waitForResultsTable();
        assertFalse(issuesPage.getBasicQuery().project().open().isAutoUpdate());
        backdoor.applicationProperties().setOption(APKeys.JIRA_ISSUENAV_CRITERIA_AUTOUPDATE, true);
        issuesPage = goToIssuesPage();
        issuesPage.waitForResultsTable();
        assertTrue(issuesPage.getBasicQuery().project().open().isAutoUpdate());
    }

    @Test
    public void testSearchIsNotUpdatedWhenAutoupdateIsDisabled() throws InterruptedException
    {
        backdoor.applicationProperties().setOption(APKeys.JIRA_ISSUENAV_CRITERIA_AUTOUPDATE, false);
        issuesPage = goToIssuesPage();
        issuesPage.waitForResultsTable();

        issuesPage.getBasicQuery().project().setValueAndClickOutside("QA", false);

        // If, after waiting DefaultTimeouts.DEFAULT ms, the results are the same, it means
        // we didn't run a new search, therefore autoupdate=false works
        String oldResultsCount = issuesPage.getResultsCount();
        Thread.sleep(DefaultTimeouts.DEFAULT);
        String newResultsCount = issuesPage.getResultsCount();
        assertThat(newResultsCount, is(oldResultsCount));
    }

    @Test
    public void testSearchIsUpdatedWhenTheUserClicksTheUpdateButton() throws InterruptedException
    {
        backdoor.applicationProperties().setOption(APKeys.JIRA_ISSUENAV_CRITERIA_AUTOUPDATE, false);
        issuesPage = goToIssuesPage();
        issuesPage.waitForResultsTable();

        issuesPage.getBasicQuery().project().setValueAndClickOutside("QA", false);
        issuesPage.getBasicQuery().project().open().submitAndWait();
        issuesPage.waitForResults();

        assertThat(issuesPage.getResultsTable().getResultCount(), is(18));
    }

    @Test
    public void testMultiSelectValidation()
    {
        BasicQueryComponent basicQueryComponent = issuesPage.getBasicQuery();
        basicQueryComponent.openAddCriteriaDialog().selectAndOpenSearcher(
                "Multi Select", "customfield_10140", MultiSelectClauseDialog.class,
                "", "searcher-customfield_10140", "");

        PrimeSelectCriteria multiSelect = basicQueryComponent.getCriteria(
                "customfield_10140", PrimeSelectCriteria.class, "", "searcher-customfield_10140", "");

        List<Option> options = multiSelect.getOptions();
        assertEquals(1, options.size());
        assertEquals("Global Option", options.get(0).text());

        multiSelect.setValueAndSubmit("Global Option");
        basicQueryComponent.project().setValueAndClickOutside("QA");

        options = multiSelect.getOptions();
        assertEquals(2, options.size());
        assertEquals("Global Option", options.get(0).text());
        assertEquals("QA Option", options.get(1).text());

        options = multiSelect.open().getInvalidOptions();
        assertEquals(1, options.size());
        assertEquals("Global Option", options.get(0).text());
    }

    @Test
    public void testSearchingAndThenSwitchingToAdvancedToCloseDialogUpdatesJql()
    {
        TextClauseDialog environmentDialog = issuesPage.getBasicQuery().openAddCriteriaDialog().selectAndOpenEnvironment();
        environmentDialog.setText("asdasd");
        issuesPage.getAdvancedQuery();
        issuesPage.waitForResultsTable();
        assertEquals("environment ~ asdasd", issuesPage.getAdvancedQuery().getCurrentQuery());
    }

    /**
     * JRADEV-15728 Deselecting the last value in a Sparkler should work.
     */
    @Test
    public void testDeselectingLastValue()
    {
        BasicQueryComponent basicQuery = issuesPage.getBasicQuery();
        basicQuery.status().setValueAndSubmit("Open");
        basicQuery.status().clear();
        basicQuery.status().dialog().cancelWithEsc();
        assertEquals(0, basicQuery.status().open().getSelectedOptions().size());

        basicQuery.assignee().setValueAndSubmit("Unassigned");
        basicQuery.assignee().clear();
        basicQuery.assignee().dialog().cancelWithEsc();
        assertEquals(0, basicQuery.assignee().open().getSelectedOptions().size());
    }

    /**
     * JRADEV-17493
     */
    @Test
    public void testRecentCriteria()
    {
        backdoor.restoreDataFromResource("TestClauseLozenges.xml");

        //Client-side
        BasicQueryComponent queryComponent = issuesPage.getBasicQuery();
        queryComponent.openAddCriteriaDialog().selectAndOpenResolutions();
        queryComponent.openAddCriteriaDialog().selectAndOpenPriority();
        queryComponent.openAddCriteriaDialog().selectAndOpenReporter();
        queryComponent.openAddCriteriaDialog().deselectResolutions().deselectPriority().deselectReporter().close();

        List<String> recentCriteria = queryComponent.openAddCriteriaDialog().getRecentCriteria();
        assertEquals(ImmutableList.<String>of("reporter", "priority", "resolution"), recentCriteria);

        //Server-side
        queryComponent.openAddCriteriaDialog().selectAndOpenResolutions().selectValues("Unresolved");
        queryComponent.openAddCriteriaDialog().selectAndOpenPriority().selectValues("Major");
        queryComponent.openAddCriteriaDialog().selectAndOpenReporter().selectValues("Current User");

        goToIssuesPage("?jql=");

        recentCriteria = queryComponent.openAddCriteriaDialog().getRecentCriteria();
        assertEquals(ImmutableList.<String>of("reporter", "priority", "resolution"), recentCriteria);
    }

    /**
     * JRADEV-17494
     */
    @Test
    public void testLabelsSparkler()
    {
        MultiSelectClauseDialog labelsDialog = issuesPage.getBasicQuery().openAddCriteriaDialog().selectAndOpenSearcher("Label", "labels", MultiSelectClauseDialog.class,
                "", "searcher-labels", "");

        final String[] selectedLabels = { "aewarwer", "world" };
        labelsDialog.selectValues(selectedLabels);
        assertEquals(Arrays.asList(selectedLabels), Lists.transform(labelsDialog.getSelectedOptions(), new Function<Option, String>()
        {
            @Override
            public String apply(Option input)
            {
                return input.value();
            }
        }));
    }

    /**
     * JRADEV-17906
     */
    @Test
    public void testIssueTypeDisplaysFallbackIcon()
    {
        //Create an IssueType with a bad URL
        backdoor.issueType().createIssueType("BadIcon","fake_url.png");

        //Refresh the page
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();

        PageElement elementWithBadIcon = issuesPage.getBasicQuery().issueType().open().getLabelByTitleAndImage("BadIcon", "images/icons/issuetypes/blank.png");
        waitUntilTrue("Icon should be blank.png", elementWithBadIcon.timed().isVisible() );
    }
}
