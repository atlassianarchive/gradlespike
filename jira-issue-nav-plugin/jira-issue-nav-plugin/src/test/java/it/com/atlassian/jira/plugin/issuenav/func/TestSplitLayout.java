package it.com.atlassian.jira.plugin.issuenav.func;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.meterware.httpunit.WebResponse;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Testing the capabilities of server side rendered split view
 *
 * @since v6.0
 */
@WebTest({ Category.FUNC_TEST, Category.ISSUE_NAVIGATOR })
public class TestSplitLayout extends FuncTestCase
{
    @Override
    protected void setUpTest()
    {
        administration.restoreData("TestNavigation.xml");
        setPreferredLayout("split-view");
    }

    @Test
    public void testServerSideRenderedListPanelForSplitView() throws IOException, SAXException {
        navigation.gotoPage("/issues/?jql=");
        WebResponse page = tester.getDialog().getResponse();

        String[] metaValue = page.getMetaTagContent("name", "ajs-jira.issues.preferred.layout.key");
        Assert.assertThat(metaValue.length, Matchers.equalTo(1));
        Assert.assertThat(metaValue[0], Matchers.equalTo("split-view"));

        checkListPanelForSplitView(page.getText());

        navigation.gotoPage("/issues");
        Assert.assertThat(tester.getDialog().getResponse().getText(),
                Matchers.not(Matchers.containsString("<div class=\"aui-group split-view\">")));

        navigation.gotoPage("/browse/XSS-17?jql=");
        checkListPanelForSplitView(tester.getDialog().getResponse().getText());

        navigation.gotoPage("/browse/XSS-17");
        Assert.assertThat(tester.getDialog().getResponse().getText(),
                Matchers.not(Matchers.containsString("<div class=\"aui-group split-view\">")));
    }

    private void checkListPanelForSplitView(String pageHtml)
    {
        Assert.assertThat(pageHtml, Matchers.containsString("<div class=\"aui-group split-view\">"));

        //Issue panel is not empty
        Assert.assertThat(pageHtml, Matchers.containsString("<div class=\"list-content\">"));
        Assert.assertThat(pageHtml,
                Matchers.not(Matchers.containsString("<div class=\"list-content\"></div>")));

        //Detail panel is empty
        Assert.assertThat(pageHtml,
                Matchers.containsString("<div class=\"aui-item detail-panel navigator-issue-only\"><div></div></div>"));

        //Pagination is not empty
        Assert.assertThat(pageHtml, Matchers.containsString("<div class=\"pagination-container aui-item\">"));
        Assert.assertThat(pageHtml, Matchers.not(Matchers.containsString("<div class=\"pagination-container aui-item\"></div>")));
    }

    private void setPreferredLayout(String layout)
    {
        final HttpClient client;
        try {
            client = createClientForUser("admin");
            HttpPost layoutPost = new HttpPost(getEnvironmentData().getBaseUrl() + "/rest/issueNav/latest/preferredSearchLayout");
            List<NameValuePair> formparams = new ArrayList<NameValuePair>();
            formparams.add(new BasicNameValuePair("layoutKey", layout));
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, "UTF-8");
            layoutPost.setEntity(entity);
            HttpResponse response = client.execute(layoutPost);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private HttpClient createClientForUser(@Nullable final String user) throws IOException
    {
        HttpClient client = new DefaultHttpClient();
        client.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BROWSER_COMPATIBILITY);

        if (user != null)
        {
            HttpPost method = new HttpPost(getEnvironmentData().getBaseUrl() + "/login.jsp");
            List<NameValuePair> formparams = new ArrayList<NameValuePair>();
            formparams.add(new BasicNameValuePair("os_username", user));
            formparams.add(new BasicNameValuePair("os_password", user));
            formparams.add(new BasicNameValuePair("os_cookie", String.valueOf(true)));
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, "UTF-8");
            method.setEntity(entity);


            final HttpResponse response = client.execute(method);
            assertEquals("OK", response.getHeaders("X-Seraph-LoginReason")[0].getValue());
            IOUtils.closeQuietly(response.getEntity().getContent());
        }

        return client;
    }
}
