package it.com.atlassian.jira.plugin.issuenav.webdriver;

/**
 * @since v6.1
 */

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.nimblefunctests.annotation.Restore;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.ToolsDropDown;
import com.atlassian.jira.webtests.Groups;
import com.atlassian.pageobjects.elements.query.Poller;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public class TestBulkEdit extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;

    @Test
    @Restore("TestBulkChangeInitialised.xml")
    public void testBulkDeleteIssueWithoutBulkChangePermission()
    {
        issuesPage = logInToIssuesPageAsSysadmin().getListLayout();
        administration.removeGlobalPermission(FunctTestConstants.BULK_CHANGE, Groups.USERS);
        Poller.waitUntilFalse(issuesPage.tools().isBulkEditAvailable());
    }

    @Test
    @Restore("TestBulkDeleteIssuesLimited.xml")
    public void testBulkDeleteIssuesLimited() throws Exception
    {
        // test for JRA-9828 OOME on bulk delete
        backdoor.applicationProperties().setString(APKeys.JIRA_BULK_EDIT_LIMIT_ISSUE_COUNT, "500");
        issuesPage = logInToIssuesPageAsSysadmin().getListLayout();
        ToolsDropDown tools = issuesPage.tools();
        waitUntilTrue(tools.isBulkEditAvailable());
        waitUntilEquals("Make sure user can only bulk edit first 500 issues, otherwise JIRA will OOME",
                "maximum 500 issues", tools.getBulkEditMaxText());
        tools.close();

        issuesPage.getAdvancedQuery().searchAndWait("id >= 11000 and id < 11500");
        Assert.assertThat("Precondition: query should have returned exactly 500 issues", issuesPage.getResultsCount(), Matchers.endsWith("of 500"));
        waitUntilTrue(tools.isBulkEditAvailable());
        waitUntilEquals("Make sure user can only bulk edit 500 issues, otherwise JIRA will OOME",
                "all 500 issue(s)", tools.getBulkEditAllText());
    }
}
