package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.nimblefunctests.annotation.Restore;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.MultiSelectClauseDialog;
import com.atlassian.jira.plugin.issuenav.pageobjects.TextClauseDialog;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;

@Restore ("TestCustomFields.xml")
public class TestCustomFields extends KickassWebDriverTestCase
{
    private TraceContext tracer;
    private IssuesPage issuesPage;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        tracer = product.getPageBinder().bind(TraceContext.class);
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();
    }

    @Test
    public void testVersionCustomField()
    {
        issuesPage.getBasicQuery().project().setValueAndSubmit("Bali");
        final MultiSelectClauseDialog multiSelect = issuesPage.getBasicQuery()
                .openAddCriteriaDialog()
                .selectAndOpenSearcher("My Single Version Picker", "customfield_10000", MultiSelectClauseDialog.class, "", "customfield_10000", "");
        Tracer checkpoint = tracer.checkpoint();
        multiSelect.selectValues("Late Arvo", "Crowded");
        issuesPage.waitForResults(checkpoint);
        assertEquals(1, issuesPage.getResultsTable().getResultCount());
    }

    @Test
    public void testSearchNumberCustomField()
    {
        String customField = backdoor.customFields().createCustomField("Some number field", "", "com.atlassian.jira.plugin.system.customfieldtypes:float",
                "com.atlassian.jira.plugin.system.customfieldtypes:exactnumber");

        issuesPage = goToIssuesPage();
        TextClauseDialog textDialog = issuesPage.getBasicQuery()
                .openAddCriteriaDialog()
                .selectAndOpenSearcher("Some number field", customField, TextClauseDialog.class, "", "searcher-" + customField);

        textDialog.setTextAndWait("SOMETEXT");
        assertEquals("'SOMETEXT' is an invalid number", textDialog.errorText());
    }

    @Test
    public void testFreeTextSearcherForCustomField()
    {
        String customField = backdoor.customFields().createCustomField("freestylin", "", "com.atlassian.jira.plugin.system.customfieldtypes:textfield",
                "com.atlassian.jira.plugin.system.customfieldtypes:textsearcher");

        issuesPage = goToIssuesPage();
        TextClauseDialog textDialog = issuesPage.getBasicQuery()
                .openAddCriteriaDialog()
                .selectAndOpenSearcher("freestylin", customField, TextClauseDialog.class, "", "searcher-" + customField);

        textDialog.setTextAndWait("invalid* *lucene freetext");
        assertEquals("The '*' and '?' are not allowed as first character in wildcard query.", textDialog.errorText());
    }

    @Test
    public void testFreeTextSearcherForCustomFieldInvalidFirstChar()
    {
        String customField = backdoor.customFields().createCustomField("freestylin", "", "com.atlassian.jira.plugin.system.customfieldtypes:textfield",
                "com.atlassian.jira.plugin.system.customfieldtypes:textsearcher");

        issuesPage = goToIssuesPage();
        TextClauseDialog textDialog = issuesPage.getBasicQuery()
                .openAddCriteriaDialog()
                .selectAndOpenSearcher("freestylin", customField, TextClauseDialog.class, "", "searcher-" + customField);

        textDialog.setTextAndWait("*invalid lucene freetext");
        assertEquals("The '*' and '?' are not allowed as first character in wildcard query.", textDialog.errorText());
    }

    @Test
    public void testProgressCustomField()
    {
        TextClauseDialog textDialog = issuesPage.getBasicQuery()
                .openAddCriteriaDialog()
                .selectAndOpenSearcher("Progress", "customfield_10100", TextClauseDialog.class, "Progress", "searcher-customfield_10100");
        textDialog.setText("Some value");
        textDialog.submitAndWait();

        assertEquals("cf[10100] ~ \"Some value\"", issuesPage.getAdvancedQuery().getCurrentQuery());
    }

    @Test
    public void testCascadingOptionUsingNoneDoesNotThrowException()
    {
        goToIssuesPage("?jql=Theme in cascadeOption('wiki', none)");
        assertFalse("Should be able to render this fine", issuesPage.hasJQLErrors());
    }

    @Test
    public void testCascadingOptionUsingInvalidOptionDoesNotThrowException()
    {
        goToIssuesPage("?jql=Theme in cascadeOption('wiki', 'invalid')");
        assertEquals("Should hav invalid option 'invalid'", "The option 'invalid' is not a child of option 'wiki' in function 'cascadeOption'.", issuesPage.getJQLErrors());
    }
}
