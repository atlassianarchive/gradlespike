package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.functest.framework.backdoor.NoAlertControl;
import com.atlassian.jira.nimblefunctests.framework.NimbleFuncTestCase;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.EnvironmentBasedProductInstance;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.*;
import com.atlassian.jira.plugin.issuenav.pageobjects.util.KickassLoginUtil;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.util.URLCodec;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;
import com.atlassian.pageobjects.elements.query.AbstractTimedQuery;
import com.atlassian.pageobjects.elements.query.ExpirationHandler;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.DefaultTimeouts;
import com.atlassian.webdriver.AtlassianWebDriver;
import org.hamcrest.Matchers;
import org.openqa.selenium.Dimension;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * The base class for Kickass Web Driver tests.
 *
 * @since v5.2
 */
public abstract class KickassWebDriverTestCase extends NimbleFuncTestCase
{
    private static final Dimension BROWSER_SIZE = new Dimension(1280, 1024);
    private static final String BROWSE_ROOT = "/browse/";
    private static final String ISSUES_ROOT = "/issues/";

    /**
     * Indicates whether the #setUpOnce method has run for this class yet.
     */
    private static boolean IS_SETUP_ONCE = false;

    protected Backdoor backdoor;
    protected KickassLoginUtil kickassLoginUtil;
    protected JiraTestedProduct product = new JiraTestedProduct(new EnvironmentBasedProductInstance());

    private NoAlertControl noAlert;

    @Override
    public void beforeMethod()
    {
        super.beforeMethod();
        backdoor = new Backdoor(environmentData);
        noAlert = new NoAlertControl(environmentData);

        // Clear onbeforeunload to ensure we don't get dirty form warnings.
        noAlert.enable();

        AtlassianWebDriver webDriver = product.getTester().getDriver();
        webDriver.manage().window().setSize(BROWSER_SIZE);
        kickassLoginUtil = product.getPageBinder().bind(KickassLoginUtil.class);
        if (!IS_SETUP_ONCE)
        {
            setUpOnce();
            IS_SETUP_ONCE = true;
        }
        setUpTest();
    }

    // Stub implementation, suitable for overriding
    protected void setUpTest() { }

    /**
     * Override this to run set up code ONCE for the given test case. This is best used in conjunction with
     * {@code @RestoreOnce} to write a test case where each of the methods is testing a different thing but they are all
     * using the same test scenario. This leads to tests that are easier to maintain and also quicker to run.
     * <p>
     * (JUnit's @BeforeClass is not suitable as that can only be used to annotate a static method that doesn't have
     * access to all the instance fields like the backdoor, etc).
     */
    protected void setUpOnce() { }

    @Override
    public void afterMethod()
    {
        noAlert.disable();
        super.afterMethod();
    }

    /**
     * Assert the current value of the URL.
     *
     * @param fragment The expected value of the URL (minus context path).
     */
    protected void assertCurrentURL(String fragment)
    {
        String expected = product.getProductInstance().getBaseUrl() + fragment;
        assertEquals(expected, product.getTester().getDriver().getCurrentUrl());
    }

    protected void assertCurrentURLTimed(String fragment) {
        assertCurrentURLTimed("Browser URL should be " + fragment, fragment);
    }

    protected void assertCurrentURLTimed(String message, String fragment)
    {
        final String expected = product.getProductInstance().getBaseUrl() + fragment;

        TimedCondition timedURL = new AbstractTimedCondition(DefaultTimeouts.DEFAULT, DefaultTimeouts.DEFAULT)
        {
            @Override
            protected Boolean currentValue()
            {
                return expected.equals(product.getTester().getDriver().getCurrentUrl());
            }
        };
        Poller.waitUntilTrue(message, timedURL);
    }

    /**
     * Navigate to an issue page, expecting a full-screen error message to appear.
     *
     * @param issueKey The issue key to use.
     * @return the visible error message.
     */
    protected IssueErrorComponent goToInvalidIssuePage(final String issueKey)
    {
        return product.getPageBinder().navigateToAndBind(IssueDetailPage.class, issueKey).error();
    }

    /**
     * Navigate to an issue page, expecting a full-screen error message to appear.
     *
     * @param issueKey The issue key to use.
     * @param jqlContext The JQL context.
     * @return the visible error message.
     */
    protected IssueErrorComponent goToInvalidIssuePage(final String issueKey, final String jqlContext)
    {
        return product.getPageBinder().navigateToAndBind(IssueDetailPage.class, issueKey, jqlContext).error();
    }

    protected IssueDetailComponent goToIssuePage(String issueKey)
    {
        return product.getPageBinder().navigateToAndBind(IssueDetailPage.class, issueKey).details();
    }

    protected IssueDetailComponent goToIssuePage(String issueKey, String jqlContext)
    {
        return product.getPageBinder().navigateToAndBind(IssueDetailPage.class, issueKey, jqlContext).details();
    }

    protected IssueDetailComponent goToIssuePage(String issueKey, String jqlContext, Long filterId)
    {
        return product.getPageBinder().navigateToAndBind(IssueDetailPage.class, issueKey, jqlContext, filterId).details();
    }

    /**
     * Navigate to the issues page with push state enabled.
     * <p/>
     * This method doesn't log you out/in; for that, see {@link #logInToIssuesPage(String, String, String)}.
     *
     * @return the issues page.
     */
    protected IssuesPage goToIssuesPage()
    {
        return goToIssuesPage("");
    }

    /**
     * Navigate to the issues page with push state enabled.
     * <p/>
     * This method doesn't log you out/in; for that, see {@link #logInToIssuesPage(String, String, String)}.
     *
     * @param queryString The query string to pass to the issues page.
     * @return The issues page.
     */
    protected IssuesPage goToIssuesPage(String queryString)
    {
        return goToIssuesPage(true, queryString);
    }

    /**
     * Navigate to the issues page.
     * <p/>
     * This method doesn't log you out/in; for that, see {@link #logInToIssuesPage(String, String, String)}.
     *
     * @param hasPushState Whether push state should be enabled.
     * @param queryString The query string to pass to the issues page.
     * @return The issues page.
     */
    protected IssuesPage goToIssuesPage(boolean hasPushState, String queryString)
    {
        return product.getPageBinder().navigateToAndBind(IssuesPage.class,
                hasPushState, queryString);
    }

    /**
     * Navigate to the split view issues page.
     * <p/>
     * This method assumes that split view is already your preferred layout.
     *
     * @param queryString The query string to pass to the issues page.
     * @return The issues page.
     */
    protected IssuesPage goToIssuesPageSplitView(final String queryString)
    {
        return goToIssuesPageSplitView(null, queryString);
    }

    /**
     * Navigate to the split view issues page.
     * <p/>
     * This method assumes that split view is already your preferred layout.
     *
     * @param issueKey The key of the selected issue.
     * @param queryString The query string to pass to the issues page.
     * @return The issues page.
     */
    protected IssuesPage goToIssuesPageSplitView(final String issueKey, final String queryString)
    {
        PageBinder pageBinder = product.getPageBinder();
        TraceContext traceContext = pageBinder.bind(TraceContext.class);

        Tracer tracer = traceContext.checkpoint();
        IssuesPage issuesPage = pageBinder.navigateToAndBind(IssuesPage.class, true, queryString, issueKey);
        issuesPage.getSplitLayout(tracer);
        return issuesPage;
    }

    protected IssuesPage jqlSearch(String jql)
    {
        try
        {
            return goToIssuesPage("?jql=" + URLCodec.encode(jql, "UTF-8", true));
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * Log out, log in as sysadmin, and navigate to the issues page.
     *
     * @return the issues page.
     */
    protected IssuesPage logInToIssuesPageAsSysadmin()
    {
        return logInToIssuesPageAsSysadmin("");
    }

    /**
     * Log out, log in as sysadmin, and navigate to the issues page with push state enabled.
     *
     * @param queryString The query string to pass to the issues page.
     * @return the issues page.
     */
    protected IssuesPage logInToIssuesPageAsSysadmin(String queryString)
    {
        return logInToIssuesPageAsSysadmin(true, queryString);
    }

    /**
     * Log out, log in as sysadmin, and navigate to the issues page.
     *
     * @param hasPushState Whether push state should be enabled.
     * @param queryString The query string to pass to the issues page.
     * @return the issues page.
     */
    protected IssuesPage logInToIssuesPageAsSysadmin(boolean hasPushState, String queryString)
    {
        return logInToIssuesPage(hasPushState, "admin", "admin", queryString);
    }

    /**
     * Log out, log in with the given details, and navigate to the issues page.
     *
     * @param username The username to use when logging in.
     * @param password The password to use when logging in.
     * @return the issues page.
     */
    protected IssuesPage logInToIssuesPage(String username, String password)
    {
        return logInToIssuesPage(username, password, "");
    }

    /**
     * Log out, log in with the given details, and navigate to the issues page.
     *
     * @param username The username to use when logging in.
     * @param password The password to use when logging in.
     * @param queryString The query string to pass to the issues page.
     * @return the issues page.
     */
    protected IssuesPage logInToIssuesPage(String username, String password, String queryString)
    {
        return logInToIssuesPage(true, username, password, queryString);
    }

    protected IssuesPage logInToIssuesPage(boolean hasPushState, String username, String password, String queryString)
    {
        return kickassLoginUtil.kickAssOnIssuesWith(product, username, password, hasPushState, queryString);
    }

    protected String getIssueURL(String issueKey) {
        return BROWSE_ROOT + issueKey;
    }

    /**
     * Get the view issue url under /browse
     *
     * @param issueKey
     * @param jqlContext If null, no search context will be appended to the url
     * @return
     */
    protected String getViewIssueUrl(String issueKey, String jqlContext)
    {
        return BROWSE_ROOT + issueKey + "?jql=" + jqlContext;
    }

    /**
     * Get the issue nav url under /issues
     *
     * @return
     */
    protected String getIssueNavUrl(String jqlContext)
    {
        return ISSUES_ROOT+ "?jql=" + jqlContext;
    }

    /**
     * @return A timed query that return the page title.
     */
    protected TimedQuery<String> pageTitle()
    {
        return new PageTitleQuery();
    }

    /**
     * Utility function that checks and asserts data that are being sent when initiating a share
     */
    protected void checkShareRequestData(Map<String,String> shareData, String fragment,
                                       String usernames, String emails, String message, String jql)
    {
        if (fragment != null)
        {
            String url = "/jira/rest/share/1.0/" + fragment;
            assertEquals("Share AJAX request URL should be " + url, url, shareData.get("url"));
        }
        if (usernames != null)
        {
            assertThat("Share AJAX request usernames should be " + usernames, shareData.get("usernames"),
                    Matchers.containsString(usernames));
        }
        if (emails != null)
        {
            assertThat("Share AJAX request usernames should be " + emails, shareData.get("emails"),
                    Matchers.containsString(emails));
        }
        if (message != null)
        {
            assertEquals("Share AJAX request message should be " + message, message, shareData.get("message"));
        }
        if (jql != null)
        {
            assertEquals("Share AJAX request jql should be " + jql, jql, shareData.get("jql"));
        }
    }

    protected void assertIssuesInTable(IssuesPage issuesPage, String... issues)
    {
        if (issues.length == 0)
        {
            assertTrue(issuesPage.emptySearchResultsMessageIsVisible());
            return;
        }
        ResultsTableComponent table = issuesPage.getResultsTable();
        for (String issue : issues)
        {
            table.containsIssue(issue);
        }
        assertEquals(issues.length, table.getResultsCountTotal());
    }

    private class PageTitleQuery extends AbstractTimedQuery<String>
    {
        PageTitleQuery()
        {
            super(3000L, 10, ExpirationHandler.RETURN_CURRENT);
        }

        @Override
        protected String currentValue() {
            return product.getTester().getDriver().getTitle();
        }

        @Override
        protected boolean shouldReturn(String currentEval)
        {
            return true;
        }
    }
}
