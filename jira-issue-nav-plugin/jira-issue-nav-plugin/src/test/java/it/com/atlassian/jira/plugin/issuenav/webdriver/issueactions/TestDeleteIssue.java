package it.com.atlassian.jira.plugin.issuenav.webdriver.issueactions;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.navigator.SelectedIssue;
import com.atlassian.jira.pageobjects.pages.viewissue.ActionTrigger;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.pageobjects.PageBinder;
import it.com.atlassian.jira.plugin.issuenav.webdriver.KickassWebDriverTestCase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

@WebTest ({ Category.WEBDRIVER_TEST, Category.ISSUES })
public class TestDeleteIssue extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;
    private PageBinder pageBinder;
    private TraceContext traceContext;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestIssueActions.xml");
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();
        pageBinder = product.getPageBinder();
        traceContext = pageBinder.bind(TraceContext.class);
    }

    @Test
    public void testFromIssueActionsDialog()
    {
        perform(ActionTrigger.ACTIONS_DIALOG);
    }

    @Test
    public void testFromCog()
    {
        perform(ActionTrigger.MENU);
    }

    private void perform(ActionTrigger actionTrigger)
    {
        final SelectedIssue selectedIssue = issuesPage.getResultsTable().getSelectedIssue();
        Tracer tracer = traceContext.checkpoint();
        selectedIssue.deleteIssue(actionTrigger);
        issuesPage.waitForResults(tracer);
        assertEquals("PERU-1", selectedIssue.getSelectedIssueKey());
    }
}