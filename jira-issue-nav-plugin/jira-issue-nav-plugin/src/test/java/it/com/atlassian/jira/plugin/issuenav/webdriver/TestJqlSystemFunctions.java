package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.components.issueviewer.config.IssueNavFeatures;
import com.atlassian.jira.plugin.issuenav.pageobjects.BasicQueryComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.MultiSelectClauseDialog;
import com.atlassian.jira.plugin.issuenav.pageobjects.ResultsTableComponent;
import com.atlassian.pageobjects.elements.query.Poller;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class TestJqlSystemFunctions extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        issuesPage = logInToIssuesPageAsSysadmin().getListLayout();

    }

    @Test
    public void testSwitchingBetweenSimpleAndAdvancedShowCorrectJQLForUnreleasedVersionFunction()
    {
        administration.restoreData("TestJqlReleasedVersionsFunctions.xml");
        issuesPage = logInToIssuesPageAsSysadmin().getListLayout();

        // verify that going from simple to advanced results in generating the unreleasedVersions function
        BasicQueryComponent basic = issuesPage.newSearch().getBasicQuery();
        basic.project().setValueAndSubmit("10000");
        basic.openAddCriteriaDialog().selectAndOpenSearcher(
                "Fix Version", "fixfor", MultiSelectClauseDialog.class, "", "searcher-fixfor", ""
        ).selectValues("Unreleased Versions");
        assertThat("Selecting Released Versions in Fix Version should automatically convert it to use unreleasedVersion() in JQL",
                issuesPage.getAdvancedQuery().getCurrentQuery(),
                Matchers.containsString("project = \"NUMBER\" AND fixVersion in unreleasedVersions()"));

        issuesPage.newSearch();
        basic = issuesPage.getBasicQuery();
        basic.project().setValueAndSubmit("10000");
        basic.openAddCriteriaDialog().selectAndOpenSearcher(
                "Affects Version", "version", MultiSelectClauseDialog.class, "", "searcher-version", ""
        ).selectValues("Unreleased Versions");
        assertThat("Selecting Released Versions in Affects Version should automatically convert it to use unreleasedVersion() in JQL",
                issuesPage.getAdvancedQuery().getCurrentQuery(),
                Matchers.containsString("project = \"NUMBER\" AND affectedVersion in unreleasedVersions()"));
    }

    @Test
    //Test for a function that don't exist. Also checks what happens when the plugin is disabled.
    public void testBadFunctions() throws Exception
    {
        administration.restoreData("TestJqlFunctionDisabled.xml");

        try
        {
            // Disable the echo function
            administration.plugins().disablePluginModule("jira.jql.function","jira.jql.function:current-user-jql-function");
            issuesPage = goToIssuesPage();

            //Run the query project = echo('HSP') where function echo is disabled.
            createSearchAndExpectError("project = currentUser()", "Unable to find JQL function 'currentUser()'.");
            //Run the query project = dontExist(HSP).
            createSearchAndExpectError("project = dontExist(HSP)", "Unable to find JQL function 'dontExist(HSP)'.");
        }
        finally
        {
            // Always re-enable the function
            administration.plugins().enablePluginModule("jira.jql.function","jira.jql.function:current-user-jql-function");
        }
    }

    @Test
    public void testAllReleasedVersions()
    {
        administration.restoreData("TestJqlReleasedVersionsFunctions.xml");

        createSearchAndAssertIssues("fixVersion = earliestUnreleasedVersion(MKY)", "MKY-1");
        createSearchAndAssertIssues("affectedVersion = earliestUnreleasedVersion(MKY)", "MKY-2");
        createSearchAndAssertIssues("VP = earliestUnreleasedVersion(MKY)", "MKY-2");
        createSearchAndAssertIssues("VP = earliestUnreleasedVersion(MK)", "MK-2");

        // resolution of project argument happens in the following order: key, name, id; case-insensitive
        createSearchAndAssertIssues("VP = earliestUnreleasedVersion(HSP)", "HSP-3");
        createSearchAndAssertIssues("VP = earliestUnreleasedVersion(hsp)", "HSP-3");
        createSearchAndAssertIssues("VP = earliestUnreleasedVersion(homosapien)", "HSP-3");
        createSearchAndAssertIssues("VP = earliestUnreleasedVersion(HOMOSAPIEN)", "HSP-3");
        createSearchAndAssertIssues("VP = earliestUnreleasedVersion(10000)", "NUMBER-2");
        createSearchAndAssertIssues("VP = earliestUnreleasedVersion(monkey)", "MKY-2");
        createSearchAndAssertIssues("VP = earliestUnreleasedVersion(MKY)", "MKY-2");
        createSearchAndAssertIssues("VP = earliestUnreleasedVersion(MK)", "MK-2");
        createSearchAndAssertIssues("VP = earliestUnreleasedVersion(10001)", "MKY-2");

        // Test with no argument (all projects)
        createSearchAndAssertIssues("VP = earliestUnreleasedVersion()", "NUMBER-2", "MKY-2", "MK-2", "HSP-3");

        // Test with multiple projects
        createSearchAndAssertIssues("VP = earliestUnreleasedVersion(HSP, MKY)", "MKY-2", "HSP-3");
        createSearchAndAssertIssues("VP = earliestUnreleasedVersion(HSP, MKY, MK)", "MKY-2", "MK-2", "HSP-3");
        createSearchAndAssertIssues("VP = earliestUnreleasedVersion(HSP, 10001)", "MKY-2", "HSP-3");

        product.logout();
        issuesPage = logInToIssuesPage("fred", "fred").getListLayout();

        createSearchAndAssertIssues("fixVersion in releasedVersions()", "NUMBER-2", "MKY-2", "MK-2");
        createSearchAndAssertIssues("affectedVersion in releasedVersions()", "NUMBER-1", "MKY-1", "MK-1");

        // filter specifies "HSP" as function argument, but fred cannot see HSP, so it will be sanitised to "10000"
        goToIssuesPage("?filter=10000");
        assertThat("Lacking project mission should sanitised the project name to ID in JQL",
                issuesPage.getAdvancedQuery().getCurrentQuery(),
                Matchers.containsString("VP in releasedVersions(10000)"));

        BasicQueryComponent basic = issuesPage.newSearch().getBasicQuery();
        basic.project().setValueAndSubmit("10000");
        basic.openAddCriteriaDialog().selectAndOpenSearcher(
                "Fix Version", "fixfor", MultiSelectClauseDialog.class, "", "searcher-fixfor", ""
        ).selectValues("Released Versions");
        assertThat("Selecting Released Versions in Fix Version should automatically convert it to use releasedVersion() in JQL",
                issuesPage.getAdvancedQuery().getCurrentQuery(),
                Matchers.containsString("project = \"NUMBER\" AND fixVersion in releasedVersions()"));

        issuesPage.newSearch();
        basic = issuesPage.getBasicQuery();
        basic.project().setValueAndSubmit("10000");
        basic.openAddCriteriaDialog().selectAndOpenSearcher(
              "Affects Version", "version", MultiSelectClauseDialog.class, "", "searcher-version", ""
        ).selectValues("Released Versions");
        assertThat("Selecting Released Versions in Affects Version should automatically convert it to use releasedVersion() in JQL",
                issuesPage.getAdvancedQuery().getCurrentQuery(),
                Matchers.containsString("project = \"NUMBER\" AND affectedVersion in releasedVersions()"));
    }

    @Test
    public void testIssueTypesFunctions()
    {
        administration.restoreData("TestJqlStandardIssueTypesFunctions.xml");

        administration.subtasks().disable();
        createSearchAndAssertIssues("type in standardIssueTypes()", "HSP-4", "HSP-3", "HSP-2", "HSP-1");
        createSearchAndExpectError("type in subTaskIssueTypes()", "Function 'subTaskIssueTypes' is invalid as sub-tasks are currently disabled.");

        administration.subtasks().enable();
        createSearchAndAssertIssues("type in standardIssueTypes()", "HSP-4", "HSP-3", "HSP-2", "HSP-1");
        createSearchAndAssertIssues("type in subTaskIssueTypes()");
        final String subTaskKey = navigation.issue().createSubTask("HSP-4", "Sub-task", "SUBSUBSUB", "");
        createSearchAndAssertIssues("type in subTaskIssueTypes()", subTaskKey);
        createSearchAndExpectError("type in standardIssueTypes(arg)", "Function 'standardIssueTypes' expected '0' arguments but received '1'.");
        createSearchAndExpectError("type in subTaskIssueTypes(arg)", "Function 'subTaskIssueTypes' expected '0' arguments but received '1'.");

        BasicQueryComponent basic = issuesPage.newSearch().getBasicQuery();
        basic.issueType().setValueAndSubmit("All Standard Issue Types");
        assertThat("Selecting All Standard Issue Types should automatically convert it to use standardIssueTypes() in JQL",
                issuesPage.getAdvancedQuery().getCurrentQuery(),
                Matchers.containsString("issuetype in standardIssueTypes()"));

        issuesPage.newSearch();
        issuesPage.getBasicQuery().issueType().setValueAndSubmit("All Sub-Task Issue Types");
        assertThat("Selecting All Sub-Task Issue Types should automatically convert it to use subTaskIssueTypes() in JQL",
                issuesPage.getAdvancedQuery().getCurrentQuery(),
                Matchers.containsString("issuetype in subTaskIssueTypes()"));
    }

    @Test
    public void testWatchedIssuesFunction()
    {
        administration.restoreData("TestVotedAndWatchedIssuesFunction.xml");

        logInToIssuesPage("fred", "fred").getListLayout();
        createSearchAndAssertIssues("issue IN watchedIssues()", "HSP-2", "HSP-1");
        assertJQLTooComplex();
        createSearchAndExpectError("issue in watchedIssues(arg)", "Function 'watchedIssues' expected '0' arguments but received '1'.");

        product.logout();
        goToIssuesPage("");
        createSearchAndExpectError("issue in watchedIssues(arg)", "Function 'watchedIssues' cannot be called as anonymous user.");

        logInToIssuesPageAsSysadmin();
        administration.generalConfiguration().disableWatching();

        logInToIssuesPage("fred", "fred");
        createSearchAndExpectError("issue in watchedIssues()", "Function 'watchedIssues' cannot be called as watching issues is currently disabled.");
    }

    @Test
    public void testVotedIssuesFunction()
    {
        administration.restoreData("TestVotedAndWatchedIssuesFunction.xml");

        logInToIssuesPage("fred", "fred").getListLayout();
        createSearchAndAssertIssues("issue IN votedIssues()", "HSP-2", "HSP-1");
        createSearchAndExpectError("issue in votedIssues(arg)", "Function 'votedIssues' expected '0' arguments but received '1'.");

        product.logout();
        goToIssuesPage("");
        createSearchAndExpectError("issue in votedIssues(arg)", "Function 'votedIssues' cannot be called as anonymous user.");

        logInToIssuesPageAsSysadmin();
        administration.generalConfiguration().disableVoting();

        logInToIssuesPage("fred", "fred");
        createSearchAndExpectError("issue in votedIssues()", "Function 'votedIssues' cannot be called as voting on issues is currently disabled.");
    }

    @Test
    public void testIssueHistoryFunction()
    {
        administration.restoreData("TestIssueHistoryFunction.xml");
        logInToIssuesPage("fred", "fred").getListLayout();
        product.goToViewIssue("HSP-1");
        goToIssuesPage();

        // Split view is the default, so HSP-3 was initially visible.
        createSearchAndAssertIssues("issue in issueHistory()", "HSP-1", "HSP-3");

        product.goToViewIssue("HSP-2");
        goToIssuesPage();
        createSearchAndAssertIssues("issue in issueHistory()", "HSP-1", "HSP-2", "HSP-3");
        createSearchAndExpectError("issue in issueHistory(arg)", "Function 'issueHistory' expected '0' arguments but received '1'.");

        // check that history is persisted
        IssuesPage issuesPage = logInToIssuesPage("fred", "fred");
        createSearchAndAssertIssues("issue in issueHistory()", "HSP-1", "HSP-2", "HSP-3");
        assertThat(issuesPage.getFilters().selectFilter(10000).getResultsTable(), new ResultsTableComponent.ContainsIssuesMatcher("HSP-1", "HSP-2", "HSP-3"));

        // check anonymous user history
        product.logout();
        product.goToViewIssue("HSP-2");
        goToIssuesPage().getListLayout();

        // Split view is the default, so HSP-3 was initially visible.
        createSearchAndAssertIssues("issue in issueHistory()", "HSP-2", "HSP-3");

        logInToIssuesPage("fred", "fred");
        product.logout();

        product.goToViewIssue("HSP-1");
        goToIssuesPage().getListLayout();
        createSearchAndAssertIssues("issue in issueHistory()", "HSP-1", "HSP-3");
    }

    @Test
    public void testLinkedIssues()
    {
        administration.restoreData("TestLinkedIssuesFunction.xml");

        // positive test cases for correctness
        // note: case is no longer important for link descriptions
        createSearchAndAssertIssues("issue in linkedIssues('HSP-1')", "MKY-1", "HSP-4", "HSP-3", "HSP-2");
        createSearchAndAssertIssues("issue in linkedIssues(10000)", "MKY-1", "HSP-4", "HSP-3", "HSP-2");
        createSearchAndAssertIssues("issue in linkedIssues('HSP-1', 'blocks')", "MKY-1", "HSP-4", "HSP-3", "HSP-2");
        createSearchAndAssertIssues("issue in linkedIssues('HSP-1', 'blOCKS')", "MKY-1", "HSP-4", "HSP-3", "HSP-2");
        createSearchAndAssertIssues("issue in linkedIssues('HSP-1', 'is blocked by')");
        createSearchAndAssertIssues("issue in linkedIssues('HSP-1', 'duplicates')", "HSP-3", "HSP-2");
        createSearchAndAssertIssues("issue in linkedIssues('HSP-1', 'DUPLICATES')", "HSP-3", "HSP-2");
        createSearchAndAssertIssues("issue in linkedIssues('HSP-1', 'is duplicated by')", "MKY-1");
        createSearchAndAssertIssues("issue in linkedIssues('HSP-1', 'duplicates', 'is duplicated by')", "MKY-1", "HSP-3", "HSP-2");
        createSearchAndAssertIssues("issue in linkedIssues('HSP-1', 'duplICAtes', 'IS duplicated BY')", "MKY-1", "HSP-3", "HSP-2");
        createSearchAndExpectError("issue in linkedIssues('HSP-1', 'duplicates', 'fred')", "Issue link type 'fred' could not be found in function 'linkedIssues'.");
        createSearchAndExpectError("issue in linkedIssues('HSP-5')", "Issue 'HSP-5' could not be found in function 'linkedIssues'.");
        createSearchAndExpectError("issue in linkedIssues(99999)", "Issue '99999' could not be found in function 'linkedIssues'.");
        administration.issueLinking().disable();
        createSearchAndExpectError("issue in linkedIssues('10000')", "Function 'linkedIssues' cannot be called as issue linking is currently disabled.");
        administration.issueLinking().enable();

        logInToIssuesPage("fred", "fred").getListLayout();
        createSearchAndAssertIssues("issue in linkedIssues('HSP-1')", "HSP-4", "HSP-3", "HSP-2");
        createSearchAndExpectError("issue in linkedIssues('MKY-1')", "Issue 'MKY-1' could not be found in function 'linkedIssues'.");
        createSearchAndExpectError("issue in linkedIssues('10001')", "Issue '10001' could not be found in function 'linkedIssues'.");
    }

    @Test
    public void testRemoteLinksByGlobalId()
    {
        administration.restoreData("TestRemoteLinksByGlobalIdFunction.xml");

        // positive test cases for correctness
        createSearchAndAssertIssues("issue in issuesWithRemoteLinksByGlobalId('non-existing-globalid')");
        createSearchAndAssertIssues("issue in issuesWithRemoteLinksByGlobalId('jira=123')", "MKY-1", "HSP-4", "HSP-2");
        createSearchAndAssertIssues("issue in issuesWithRemoteLinksByGlobalId('confluence=456')", "HSP-3", "HSP-2");
        createSearchAndAssertIssues("issue in issuesWithRemoteLinksByGlobalId('jira=123', 'confluence=456')", "MKY-1", "HSP-4", "HSP-3", "HSP-2");
        administration.issueLinking().disable();
        createSearchAndExpectError("issue in issuesWithRemoteLinksByGlobalId('jira=123')", "Function 'issuesWithRemoteLinksByGlobalId' cannot be called as issue linking is currently disabled.");
        administration.issueLinking().enable();

        // test with permissions filtering, MKY-1 should not appear in search results
        logInToIssuesPage("fred", "fred").getListLayout();
        createSearchAndAssertIssues("issue in issuesWithRemoteLinksByGlobalId('confluence=456')", "HSP-3", "HSP-2");
        createSearchAndAssertIssues("issue in issuesWithRemoteLinksByGlobalId('jira=123')", "HSP-4", "HSP-2");
        createSearchAndAssertIssues("issue in issuesWithRemoteLinksByGlobalId('jira=123', 'confluence=456')", "HSP-4", "HSP-3", "HSP-2");

        // no parameters error
        createSearchAndExpectError("issue in issuesWithRemoteLinksByGlobalId()", "Function 'issuesWithRemoteLinksByGlobalId' expected between '1' and '100' arguments but received '0'. Usage: 'issuesWithRemoteLinksByGlobalId ( globalId+ )'.");
    }

    @Test
    public void testNowFunction() throws Exception
    {
        final Calendar calendar = Calendar.getInstance();
        _testDateFunction("now()", calendar);
        assertEquals("Function 'now' expected '0' arguments but received '1'.", issuesPage.getAdvancedQuery().searchAndWait("created > now ('hrehjre')").getJQLErrors());
    }

    @Test
    public void testStartOfDayFunction() throws Exception
    {
        // Test issue dates are set to be 2 days either side of the passed in calendar
        Calendar calendar = Calendar.getInstance();
        _testDateFunction("startOfDay()", calendar);
        calendar = Calendar.getInstance();
        _testDateFunction("startOfDay(1)", calendar);
        calendar = Calendar.getInstance();
        _testDateFunction("startOfDay(-1)", calendar);
        calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        _testDateFunction("startOfDay(\"+1M\")", calendar);
    }

    @Test
    public void testStartOfWeekFunction() throws Exception
    {
        // Test issue dates are set to be 2 days either side of the passed in calendar
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        _testDateFunction("startOfWeek()", calendar);

        calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        _testDateFunction("startOfWeek(1d)", calendar);
        calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        _testDateFunction("startOfWeek(-1d)", calendar);

        calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        // Need to get the time to  force the calendar to compute the date properly as
        // DAY_OF_WEEK confuses things.
        calendar.getTimeInMillis();
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        calendar.getTimeInMillis();
        _testDateFunction("startOfWeek(\"+1M\")", calendar);
    }

    @Test
    public void testStartOfMonthFunction() throws Exception
    {
        // Test issue dates are set to be 2 days either side of the passed in calendar
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        _testDateFunction("startOfMonth()", calendar);

        calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        _testDateFunction("startOfMonth(1d)", calendar);
        calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        _testDateFunction("startOfMonth(-1d)", calendar);

        calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        _testDateFunction("startOfMonth(\"+1M\")", calendar);
    }

    @Test
    public void testStartOfYearFunction() throws Exception
    {
        // Test issue dates are set to be 2 days either side of the passed in calendar
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMinimum(Calendar.DAY_OF_YEAR));
        _testDateFunction("startOfYear()", calendar);

        calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMinimum(Calendar.DAY_OF_YEAR));
        _testDateFunction("startOfYear(1d)", calendar);
        calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMinimum(Calendar.DAY_OF_YEAR));
        _testDateFunction("startOfYear(-1d)", calendar);

        calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMinimum(Calendar.DAY_OF_YEAR));
        calendar.add(Calendar.MONTH, 1);
        _testDateFunction("startOfYear(\"+1M\")", calendar);
    }

    @Test
    public void testEndOfDayFunction() throws Exception
    {
        // Test issue dates are set to be 2 days either side of the passed in calendar
        Calendar calendar = Calendar.getInstance();
        _testDateFunction("endOfDay()", calendar);
        calendar = Calendar.getInstance();
        _testDateFunction("endOfDay(1)", calendar);
        calendar = Calendar.getInstance();
        _testDateFunction("endOfDay(-1)", calendar);
        calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        _testDateFunction("endOfDay(\"+1M\")", calendar);
    }

    @Test
    public void testEndOfWeekFunction() throws Exception
    {
        int lastDayOfWeek;
        Calendar calendar = new GregorianCalendar();
        if (calendar.getFirstDayOfWeek() == Calendar.MONDAY)
        {
            lastDayOfWeek = Calendar.SUNDAY;
        }
        else
        {
            lastDayOfWeek = Calendar.SATURDAY;
        }    	// Test issue dates are set to be 2 days either side of the passed in calendar
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.DAY_OF_WEEK, lastDayOfWeek);
        _testDateFunction("endOfWeek()", calendar);

        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.DAY_OF_WEEK, lastDayOfWeek);
        _testDateFunction("endOfWeek(1d)", calendar);
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.DAY_OF_WEEK, lastDayOfWeek);
        _testDateFunction("endOfWeek(-1d)", calendar);

        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.MONTH, 1);
        // Need to get the time to  force the calendar to compute the date properly as
        // DAY_OF_WEEK confuses things.
        calendar.getTimeInMillis();
        calendar.set(Calendar.DAY_OF_WEEK, lastDayOfWeek);
        _testDateFunction("endOfWeek(\"+1M\")", calendar);
    }

    @Test
    public void testEndOfMonthFunction() throws Exception
    {
        // Test issue dates are set to be 2 days either side of the passed in calendar
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        _testDateFunction("endOfMonth()", calendar);

        calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        _testDateFunction("endOfMonth(1d)", calendar);
        calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        _testDateFunction("endOfMonth(-1d)", calendar);

        calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        _testDateFunction("endOfMonth(\"+1M\")", calendar);
    }

    @Test
    public void testEndOfYearFunction() throws Exception
    {
        // Test issue dates are set to be 2 days either side of the passed in calendar
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
        _testDateFunction("endOfYear()", calendar);

        calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
        _testDateFunction("endOfYear(1d)", calendar);
        calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
        _testDateFunction("endOfYear(-1d)", calendar);

        calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
        calendar.add(Calendar.MONTH, 1);
        _testDateFunction("endOfYear(\"+1M\")", calendar);
    }

    @Test
    public void testLastLogin()
    {
        administration.restoreData("TestSystemJqlFunctions.xml");
        // this if first login, there are no previous login entries. According to specification, for last logim without
        // last login we expect all issues from 1970
        logInToIssuesPage("fred", "fred").getListLayout();
        createSearchAndAssertIssues("created >= lastLogin()", "HSP-4", "HSP-3", "HSP-2", "HSP-1");

        product.logout();
        logInToIssuesPage("fred", "fred");
        issuesPage.getHeader().createIssue().fill("summary", "created after login").submit(IssuesPage.class);
        createSearchAndAssertIssues("created >= lastLogin()", "HSP-5");

        createSearchAndExpectError("created > lastLogin('hrehjre')", "Function 'lastLogin' expected '0' arguments but received '1'.");

        assertJQLTooComplex("created > lastLogin()");

        product.logout();

        goToIssuesPage().getListLayout();
        createSearchAndAssertIssues("created >= lastLogin()");
        createSearchAndAssertIssues("created <= lastLogin()");
    }

    @Test
    public void testCurrentLogin()
    {
        administration.restoreData("TestSystemJqlFunctions.xml");
        createSearchAndAssertIssues("created < currentLogin()", "HSP-4", "HSP-3", "HSP-2", "HSP-1");
        createSearchAndAssertIssues("created >= currentLogin()");

        // create an issue after our login
        issuesPage.getHeader().createIssue().fill("summary", "created after login").submit(IssuesPage.class);
        createSearchAndAssertIssues("created < currentLogin()", "HSP-4", "HSP-3", "HSP-2", "HSP-1");
        createSearchAndAssertIssues("created >= currentLogin()", "HSP-5");

        product.logout();
        issuesPage = logInToIssuesPage("admin", "admin").getListLayout();
        createSearchAndAssertIssues("created > currentLogin()");
        createSearchAndAssertIssues("created <= currentLogin()", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1");

        createSearchAndExpectError("created > currentLogin('hrehjre')", "Function 'currentLogin' expected '0' arguments but received '1'.");
        assertJQLTooComplex("created > currentLogin()");

        product.logout();
        goToIssuesPage("").getListLayout();
        createSearchAndAssertIssues("created >= currentLogin()");
        createSearchAndAssertIssues("created <= currentLogin()");
    }

    @Test
    public void testCurrentUser()
    {
        administration.restoreData("TestSystemJqlFunctions.xml");
        logInToIssuesPageAsSysadmin().getListLayout();

        createSearchAndAssertIssues("assignee = currentUser()", "HSP-4", "HSP-2");
        createSearchAndAssertIssues("reporter = currentUser()", "HSP-2", "HSP-1");

        //This assert is testing the internal behaviour of the JQL system.
        //It is practically impossible to create/save a filter with invalid result
        issuesPage.getFilters().selectFilter(10001l).getAdvancedQuery().submit();
        assertEquals("Function 'currentUser' expected '0' arguments but received '1'.", issuesPage.getJQLErrors());

        logInToIssuesPage("fred", "fred").getListLayout();
        createSearchAndAssertIssues("assignee = currentUser()", "HSP-3", "HSP-1");
        createSearchAndAssertIssues("reporter = CURRENTUSER()", "HSP-4", "HSP-3");

        //Now make sure that the function with argument returns an error when used with arguments.
        createSearchAndExpectError("assignee = currentuser('hrehjre')", "Function 'currentuser' expected '0' arguments but received '1'.");

        //Make sure that the 'currentUser' is used for custom fields.
        goToIssuesPage("?jql=").getBasicQuery()
                .openAddCriteriaDialog()
                .selectAndOpenSearcher("UserPickerCustomField", "customfield_10001", MultiSelectClauseDialog.class, "", "customfield_10001", "")
                .selectValues("Current User");
        assertEquals("UserPickerCustomField in (currentUser())", issuesPage.getAdvancedQuery().getCurrentQuery());

        goToIssuesPage("?filter=-2").getBasicQuery().assignee().open().selectValues("Current User");
        assertThat(issuesPage.getAdvancedQuery().getCurrentQuery(), Matchers.startsWith("assignee in (currentUser()) AND reporter in (currentUser())"));

        product.logout();
        goToIssuesPage().getListLayout();
        createSearchAndAssertIssues("reporter = CURRENTUSER()");
    }

    @Test
    public void testMembersOf() throws Exception
    {
        administration.restoreData("TestSystemJqlFunctionsMembersOf.xml");

        //Check some searches against the admin user.
        //note: function is case-insensitive
        createSearchAndAssertIssues("assignee in membersOf('jira-users')", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1");
        createSearchAndAssertIssues("assignee in membersOf('JIRA-users')", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1");
        createSearchAndAssertIssues("reporter in membersOf('jira-administrators')", "HSP-2", "HSP-1");
        createSearchAndAssertIssues("reporter in membersOf('jira-ADMINistrators')", "HSP-2", "HSP-1");
        createSearchAndAssertIssues("reporter in membersOf('empty-group')");

        // JIRA used to be case-sensitive for Group and User names, and no longer is, so these tests don't really anything different to above
        createSearchAndAssertIssues("reporter in membersOf('jira-developers')", "HSP-4", "HSP-3", "HSP-2", "HSP-1");
        createSearchAndAssertIssues("reporter in membersOf('JIRA-DEVELOPERS')", "HSP-4", "HSP-3", "HSP-2", "HSP-1");
        createSearchAndAssertIssues("reporter in membersOf('JIRA-DEVELOPERS-1')", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1");

        //Lets check what happens with the query "assignee in membersOf()". It should return nothing.
        //This assert is testing the internal behaviour of the JQL system.
        //It is practically impossible to create/save a filter with invalid result
        issuesPage.getFilters().selectFilter(10004).getAdvancedQuery().submit();
        assertEquals("Function 'membersOf' expected '1' arguments but received '2'.", issuesPage.getJQLErrors());

        //Now make sure that the function with incorrect number of arguments does not work.
        createSearchAndExpectError("assignee in membersOf ()", "Function 'membersOf' expected '1' arguments but received '0'.");
        createSearchAndExpectError("assignee in membersOf (8689, 38383)", "Function 'membersOf' expected '1' arguments but received '2'.");
        createSearchAndExpectError("assignee in membersof ('notAGroupDoesNotExist')", "Function 'membersof' can not generate a list of usernames for group 'notAGroupDoesNotExist'; the group does not exist.");

        //Make sure that the 'membersOf' is used for custom fields.
        goToIssuesPage("?jql=").getBasicQuery()
                .openAddCriteriaDialog()
                .selectAndOpenSearcher("UserGroupSearcherCustomField", "customfield_10000", MultiSelectClauseDialog.class, "", "customfield_10000", "")
                .selectValues("jira-users");
        assertEquals("UserGroupSearcherCustomField in (membersOf(jira-users))", issuesPage.getAdvancedQuery().getCurrentQuery());

        goToIssuesPage("?jql=").getBasicQuery().assignee().setValueAndSubmit("jira-users", "jira-administrators");
        assertThat(issuesPage.getAdvancedQuery().getCurrentQuery(), anyOf(
                equalTo("assignee in (membersOf(jira-users), membersOf(jira-administrators))"),
                equalTo("assignee in (membersOf(jira-administrators), membersOf(jira-users))")
        ));
    }

    // this test uses hand-crafted XML that has filters saved with bogus arguments to JQL functions. We want to
    // verify that these don't just throw 500 pages at users.
    @Test
    public void testIllegalJqlFunctionParameters()
    {
        administration.restoreData("TestIllegalJqlFunctionParameters.xml");

        // these are the expected error messages for the various crap filters saved in the data
        String[] errorMessages = new String[] {
                "Could not resolve the project 'random argument' provided to function 'releasedVersions'.",
                "Could not resolve the project 'random argument' provided to function 'unreleasedVersions'.",
                createFunctionArgumentError("standardIssueTypes", 0, 1),
                createFunctionArgumentError("subTaskIssueTypes", 0, 1),
                "Incorrect number of arguments specified for the function 'cascadeOption'. Usages: cascadeOption(parentOption), cascadeOption(parentOption, childOption), cascadeOption(parentOption, \"None\").",
                "The option 'random argument' specified in function 'cascadeOption' is not a valid parent option.",
                "", //"The option 'random argument' is not a child of option 'parent' in function 'cascadeOption'.",
                "Incorrect number of arguments specified for the function 'cascadeOption'. Usages: cascadeOption(parentOption), cascadeOption(parentOption, childOption), cascadeOption(parentOption, \"None\").",
                createFunctionArgumentError("currentUser", 0, 1),
                createFunctionArgumentError("issueHistory", 0, 1),
                "Incorrect number of arguments for function 'linkedIssues'. Usage: 'linkedIssues ( issuekey [, linkDescription ]* )'.",
                "Issue 'random argument' could not be found in function 'linkedIssues'.",
                "Issue link type 'random argument' could not be found in function 'linkedIssues'.",
                createFunctionArgumentError("membersOf", 1, 0),
                "Function 'membersOf' can not generate a list of usernames for group 'random argument'; the group does not exist.",
                createFunctionArgumentError("now", 0, 1),
                createFunctionArgumentError("votedIssues", 0, 1),
                createFunctionArgumentError("watchedIssues", 0, 1),
                createFunctionArgumentError("lastLogin", 0, 1),
                createFunctionArgumentError("currentLogin", 0, 1),
        };

        for (int i = 0; i < errorMessages.length; i++)
        {
            String errorMessage = errorMessages[i];
            if (errorMessage.isEmpty())
            {
                continue;
            }
            issuesPage.getFilters().selectFilter(new Long(10000 + i)).getAdvancedQuery().submit();
            assertEquals(errorMessage, issuesPage.getJQLErrors());
        }
    }

    @Test
    public void testProjectsLeadByUserFunction()
    {
        administration.restoreData("TestProjectsLeadByUserFunction.xml");

        issuesPage = logInToIssuesPage("fred", "fred").getListLayout();
        createSearchAndAssertIssues("project IN projectsLeadByUser()", "FKY-1");
        createSearchAndAssertIssues("project IN projectsLeadByUser(fred)", "FKY-1");
        createSearchAndAssertIssues("project IN projectsLeadByUser(bill)", "BKY-2", "BKY-1");
        createSearchAndAssertIssues("project IN projectsLeadByUser(admin)", "MKY-1", "HSP-2", "HSP-1");
        createSearchAndAssertIssues("project IN projectsLeadByUser(mary)");

        assertJQLTooComplex();
        issuesPage.getAdvancedQuery().searchAndWait("project IN projectsLeadByUser(user1, user2)");
        assertEquals("Function 'projectsLeadByUser' expected between '0' and '1' arguments but received '2'.", issuesPage.getJQLErrors());

        product.logout();

        issuesPage = goToIssuesPage().getListLayout();
        issuesPage.getAdvancedQuery().searchAndWait("project IN projectsLeadByUser()");
        assertEquals("Function 'projectsLeadByUser' cannot be called as anonymous user.", issuesPage.getJQLErrors());
        createSearchAndAssertIssues("project IN projectsLeadByUser(admin)", "MKY-1", "HSP-2", "HSP-1");

        issuesPage.getFilters().selectFilter(10000).getAdvancedQuery().submit();
        assertEquals("Function 'projectsLeadByUser' cannot be called as anonymous user.", issuesPage.getJQLErrors());
        issuesPage.getFilters().selectFilter(10001).getAdvancedQuery().submit();
        assertEquals("project in projectsLeadByUser(admin)", issuesPage.getAdvancedQuery().getCurrentQuery());
    }

    @Test
    public void testProjectsWhereUserHasPermissionFunction()
    {
        administration.restoreData("TestProjectsWhereUserHasPermissionFunction.xml");
        issuesPage = logInToIssuesPage("fred", "fred").getListLayout();

        createSearchAndAssertIssues("project IN projectsWhereUserHasPermission(\"Create Issues\")", "MKY-2", "MKY-1", "HSP-5", "HSP-4", "HSP-2", "HSP-1");
        createSearchAndAssertIssues("project IN projectsWhereUserHasPermission(\"Resolve Issues\")", "HSP-5", "HSP-4", "HSP-2", "HSP-1");
        // Lower case should also work
        createSearchAndAssertIssues("project IN projectsWhereUserHasPermission(\"create issues\")", "MKY-2", "MKY-1", "HSP-5", "HSP-4", "HSP-2", "HSP-1");
        createSearchAndAssertIssues("project IN projectsWhereUserHasPermission(\"resolve issues\")", "HSP-5", "HSP-4", "HSP-2", "HSP-1");

        assertJQLTooComplex();

        createSearchAndExpectError("project IN projectsWhereUserHasPermission()", createFunctionArgumentError("projectsWhereUserHasPermission", 1, 0));
        createSearchAndExpectError("project IN projectsWhereUserHasPermission(\"Create Issues\", bill)", createFunctionArgumentError("projectsWhereUserHasPermission", 1, 2));

        issuesPage.getFilters().selectFilter(10000).getAdvancedQuery().submit();
        assertEquals("project in projectsWhereUserHasPermission(\"Resolve Issues\")", issuesPage.getAdvancedQuery().getCurrentQuery());

        product.logout();
        issuesPage = goToIssuesPage().getListLayout();
        createSearchAndExpectError("project IN projectsWhereUserHasPermission(\"Create Issues\")", "Function 'projectsWhereUserHasPermission' cannot be called as anonymous user.");

//        Strange ... expecting error here, but the JQL doesn't produce any error for anon user
//        issuesPage.getFilters().selectFilter(10000).getAdvancedQuery().submit();
//        assertEquals("Function 'projectsWhereUserHasPermission' cannot be called as anonymous user.", issuesPage.getJQLErrors());
    }

    @Test
    public void testProjectsWhereUserHasRoleFunction()
    {
        administration.restoreData("TestProjectsWhereUserHasRoleFunction.xml");
        issuesPage = logInToIssuesPage("fred", "fred").getListLayout();

        createSearchAndAssertIssues("project IN projectsWhereUserHasRole(\"Developers\")", "HSP-5", "HSP-4", "HSP-2", "HSP-1");
        createSearchAndAssertIssues("project IN projectsWhereUserHasRole(\"Users\")", "MKY-2", "MKY-1");

        assertJQLTooComplex();

        createSearchAndExpectError("project IN projectsWhereUserHasRole()", createFunctionArgumentError("projectsWhereUserHasRole", 1, 0));
        createSearchAndExpectError("project IN projectsWhereUserHasRole(\"Developers\", admin)", createFunctionArgumentError("projectsWhereUserHasRole", 1, 2));
        issuesPage.getFilters().selectFilter(10000).getAdvancedQuery().submit();
        assertEquals("project in projectsWhereUserHasRole(Developers)", issuesPage.getAdvancedQuery().getCurrentQuery());

        // Anonymous test
        product.logout();
        issuesPage = goToIssuesPage().getListLayout();
        createSearchAndExpectError("project IN projectsWhereUserHasRole(\"Developers\")", "Function 'projectsWhereUserHasRole' cannot be called as anonymous user.");
        issuesPage.getFilters().selectFilter(10000).getAdvancedQuery().submit();
        assertEquals("Function 'projectsWhereUserHasRole' cannot be called as anonymous user.", issuesPage.getJQLErrors());
    }

    @Test
    public void testComponentsLeadByUserFunction()
    {
        administration.restoreData("TestComponentsLeadByUserFunction.xml");
        issuesPage = logInToIssuesPage("fred", "fred").getListLayout();

        createSearchAndAssertIssues("component IN componentsLeadByUser()", "HSP-5");
        createSearchAndAssertIssues("component IN componentsLeadByUser(fred)", "HSP-5");
        createSearchAndAssertIssues("component IN componentsLeadByUser(bill)", "MKY-1", "HSP-4", "HSP-2", "HSP-1");
        createSearchAndAssertIssues("component IN componentsLeadByUser(admin)");
        createSearchAndAssertIssues("component IN componentsLeadByUser(mary)");

        assertJQLTooComplex();

        createSearchAndExpectError("component IN componentsLeadByUser(user1, user2)", createFunctionArgumentError("componentsLeadByUser", 0, 1, 2));

        product.logout();
        issuesPage = goToIssuesPage().getListLayout();
        createSearchAndExpectError("component IN componentsLeadByUser()", "Function 'componentsLeadByUser' cannot be called as anonymous user.");
        createSearchAndAssertIssues("component IN componentsLeadByUser(fred)", "HSP-5");

        issuesPage.getFilters().selectFilter(10000).getAdvancedQuery().submit();
        assertEquals("Function 'componentsLeadByUser' cannot be called as anonymous user.", issuesPage.getJQLErrors());
        issuesPage.getFilters().selectFilter(10001).getAdvancedQuery().submit();
        assertEquals("component in componentsLeadByUser(fred)", issuesPage.getAdvancedQuery().getCurrentQuery());
    }

    //Test to ensure that functions do not display values when displaying errors.
    @Test
    public void testJqlFunctionMessages() throws Exception
    {
        administration.restoreData("TestJqlFunctionErrors.xml");
        issuesPage = logInToIssuesPageAsSysadmin().getListLayout();

        //Enumerated functions return the same thing.
        assertJqlFunctionErrorEnumeratedValues("affectedVersion");
        assertJqlFunctionErrorEnumeratedValues("category");
        assertJqlFunctionErrorEnumeratedValues("component");
        assertJqlFunctionErrorEnumeratedValues("fixVersion");
        assertJqlFunctionErrorEnumeratedValues("level");
        assertJqlFunctionErrorEnumeratedValues("priority");
        assertJqlFunctionErrorEnumeratedValues("project");
        assertJqlFunctionErrorEnumeratedValues("resolution");
        assertJqlFunctionErrorEnumeratedValues("savedFilter");
        assertJqlFunctionErrorEnumeratedValues("status");
        assertJqlFunctionErrorEnumeratedValues("type");
        assertJqlFunctionErrorEnumeratedValues("PP");
        assertJqlFunctionErrorEnumeratedValues("SVP");
        assertJqlFunctionErrorEnumeratedValues("VP");

        createSearchAndExpectError("votes = currentUser()", "A value provided by the function 'currentUser' is invalid for the field 'votes'. Votes must be a positive whole number.");

        //Time tracking.
        assertJqlFunctionErrorTimeTracking("originalEstimate");
        assertJqlFunctionErrorTimeTracking("remainingEstimate");
        assertJqlFunctionErrorTimeTracking("timeSpent");

        //Check out the workRatio searcher.
        createSearchAndExpectError("workRatio < currentUser()", "A value provided by the function 'currentUser' for the field 'workRatio' is not an integer.");

        //Issue Fields
        assertJqlFunctionErrorIssueKey("issue");
        assertJqlFunctionErrorIssueKey("parent");

        //UserFields.
        assertJqlFunctionWarningUser("assignee");
        assertJqlFunctionWarningUser("reporter");
        assertJqlFunctionWarningUser("MUP");
        assertJqlFunctionWarningUser("UP");

        //Date based fields.
        assertJqlFunctionErrorDateValues("updated");
        assertJqlFunctionErrorDateValues("created");
        assertJqlFunctionErrorDateValues("due");
        assertJqlFunctionErrorDateValues("resolved");
        assertJqlFunctionErrorDateValues("DP");
        assertJqlFunctionErrorDateValues("DT");

        //Text Fields
        assertJqlFunctionErrorTextValues("comment");
        assertJqlFunctionErrorTextValues("description");
        assertJqlFunctionErrorTextValues("environment");
        assertJqlFunctionErrorTextValues("summary");
        assertJqlFunctionErrorTextValues("FTF");
        assertJqlFunctionErrorTextValues("ROTF");
        assertJqlFunctionErrorTextValues("TF");

        //Test for the number fields.
        assertJqlFunctionErrorNumber("II");
        assertJqlFunctionErrorNumber("NF");

        //Test for the group pickers.
        assertJqlFunctionErrorGroup("GP");
        assertJqlFunctionErrorGroup("MGP");

        //Lets work with the option based custom fields.
        assertJqlFunctionErrorOption("CSF");
        assertJqlFunctionErrorOption("MC");
        assertJqlFunctionErrorOption("MS");
        assertJqlFunctionErrorOption("RB");
        assertJqlFunctionErrorOption("SL");
    }

    private String createFunctionArgumentError(final Object funcName, final int minArgs, final int maxArgs, final int actualArgs)
    {
        return String.format("Function '%s' expected between '%d' and '%d' arguments but received '%d'.", funcName, minArgs, maxArgs, actualArgs);
    }

    private String createFunctionArgumentError(final Object funcName, final int expectedArgs, final int actualArgs)
    {
        return String.format("Function '%s' expected '%d' arguments but received '%d'.", funcName, expectedArgs, actualArgs);
    }

    private void createSearchAndExpectError(final String query, final String errorMessage)
    {
        assertEquals(errorMessage, issuesPage.getAdvancedQuery().searchAndWait(query).getJQLErrors());
    }

    private void createSearchAndExpectWarning(final String query, final String warningMessage)
    {
        assertEquals(warningMessage, issuesPage.getAdvancedQuery().searchAndWait(query).getJQLWarnings());
    }

    private void createSearchAndAssertIssues(final String query)
    {
        assertEquals("No issues were found to match your search", issuesPage.getAdvancedQuery().searchAndWait(query).getEmptySearchResultMessage().byDefaultTimeout());
    }

    private void createSearchAndAssertIssues(final String query, final String... issueKeys)
    {
        ResultsTableComponent results = issuesPage.getAdvancedQuery().searchAndWait(query).getResultsTable();
        assertEquals(issueKeys.length, results.getResultCount());
        assertThat(results, new ResultsTableComponent.ContainsIssuesMatcher(issueKeys));
    }

    private void assertJqlFunctionErrorEnumeratedValues(String fieldName)
    {
        createSearchAndExpectError(String.format("%s = CuRReNTuseR()", fieldName), String.format("A value provided by the function 'CuRReNTuseR' is invalid for the field '%s'.", fieldName));
    }

    private void assertJqlFunctionErrorTimeTracking(String fieldName)
    {
        createSearchAndExpectError(String.format("%s = cuRRENTUSER()", fieldName), String.format("A value provided by the function 'cuRRENTUSER' for the field '%s' is not a positive duration.", fieldName));
    }

    private void assertJqlFunctionErrorIssueKey(String fieldName)
    {
        createSearchAndExpectError(String.format("%s = currentuser()", fieldName), String.format("A value provided by the function 'currentuser' for the field '%s' is not a valid issue key.", fieldName));
    }

    private void assertJqlFunctionWarningUser(String fieldName)
    {
        createSearchAndExpectWarning(String.format("%s = now()", fieldName), String.format("A value provided by the function 'now' is invalid for the field '%s'.", fieldName));
    }

    private void assertJqlFunctionErrorDateValues(String fieldName)
    {
        createSearchAndExpectError(String.format("%s = currentUSER()", fieldName), String.format("A date for the field '%s' provided by the function 'currentUSER' is not valid.", fieldName));
    }

    private void assertJqlFunctionErrorTextValues(final String fieldName)
    {
        createSearchAndExpectError(String.format("%s ~ ECHO('?illegal')", fieldName), String.format(
                "The value returned by function 'ECHO' for field '%s' is not valid: the '*' and '?' are not allowed as first character in wildcard query.", fieldName));
        createSearchAndExpectError(String.format("%s ~ ECHO('~')", fieldName), String.format(
                "The field '%s' is unable to parse the text given to it by the function 'ECHO'.", fieldName));
    }

    private void assertJqlFunctionErrorNumber(String fieldName)
    {
        createSearchAndExpectError(String.format("%s = echo('bad')", fieldName), String.format("A value provided by the function 'echo' for the field '%s' is not a valid number.", fieldName));
    }

    private void assertJqlFunctionErrorGroup(String fieldName)
    {
        createSearchAndExpectError(String.format("%s = now()", fieldName), String.format("A group provided by the function 'now' for the field '%s' does not exist.", fieldName));
    }

    private void assertJqlFunctionErrorOption(final String fieldName)
    {
        createSearchAndExpectError(String.format("%s = EcHo('notAnOption')", fieldName), String.format("An option provided by the function 'EcHo' for the field '%s' does not exist.", fieldName));
    }


    private void assertJQLTooComplex()
    {
        assertJQLTooComplex(null);
    }

    private void assertJQLTooComplex(String jql)
    {
        if (jql != null)
        {
            issuesPage.getAdvancedQuery().searchAndWait(jql);
        }
        Poller.waitUntilTrue("User can not switch to basic mode when JQL is too complex", issuesPage.isSwitcherDisabled());
    }

    private void _testDateFunction(String function, Calendar calendar) throws Exception
    {
        // Make the calendar real before we try manipulating it.
        calendar.getTimeInMillis();
        // Set MKY-1 dates to 2 days before the given date
        calendar.add(Calendar.DAY_OF_MONTH, -2);
        String mky1Date = new Timestamp(calendar.getTimeInMillis()).toString();
        // Set MKY-2 dates to 2 days after the given date
        calendar.add(Calendar.DAY_OF_MONTH, 4);
        String mky2Date = new Timestamp(calendar.getTimeInMillis()).toString();
        //Set up data with current relevant dates.
        Map<String, String> map = new HashMap<String, String>();
        map.put("@@MKY-1_CREATED_DATE@@", mky1Date);
        map.put("@@MKY-1_UPDATED_DATE@@", mky1Date);
        map.put("@@MKY-2_CREATED_DATE@@", mky2Date);
        map.put("@@MKY-2_UPDATED_DATE@@", mky2Date);

        administration.restoreDataWithReplacedTokens("TestJqlNowFunction.xml", map);
        issuesPage = goToIssuesPage();
        issuesPage.getListLayout();

        //Make sure we find the correct issues in the future.
        createSearchAndAssertIssues("created > " + function, "MKY-2");

        //Make sure we find the issues in the past.
        createSearchAndAssertIssues("created < " + function, "MKY-1");
    }
}
