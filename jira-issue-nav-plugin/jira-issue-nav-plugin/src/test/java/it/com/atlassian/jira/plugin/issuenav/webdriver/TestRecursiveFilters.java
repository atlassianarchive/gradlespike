package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @since v6.1
 */
public class TestRecursiveFilters extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreBlankInstance();
    }

    /**
     * JRA-24366 Test if opening a filter that has a cyclical reference causes stack overflow exception.
     * After the fix it should be possible to open the test and see an error message.
     */
    @Test
    public void testRecursiveFilters() {
        backdoor.project().addProject("Test", "TST", "admin");
        backdoor.searchRequests().createFilter("admin", "filter=Test1", "Test1", "");
        String filterId = backdoor.searchRequests().createFilter("admin", "filter=Test1", "Test2", "");

        issuesPage = logInToIssuesPageAsSysadmin();
        issuesPage.getFilters().selectFilter(Long.parseLong(filterId));
        issuesPage.getAdvancedQuery().submit();

        assertEquals("Field 'filter' with value 'Test1' matches filter 'Test1' and causes a cyclical reference,"
                + " this query can not be executed and should be edited.", issuesPage.getJQLErrors());
    }
}
