package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.MultiSelectClauseDialog;
import com.atlassian.jira.plugin.issuenav.pageobjects.PrimeCriteriaUserPickerComponent;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RestoreOnce ("TestSearching.xml")
public class TestUserGroupCustomFields extends KickassWebDriverTestCase
{
    private static final ArrayList<String> EXPECTED_USERS_GROUPS = Lists.newArrayList(
            "Current User",
            "Empty",
            "admin",
            "fred",
            "jira-administrators",
            "jira-developers",
            "jira-users");
    private static final ArrayList<String> EXPECTED_GROUPS = Lists.newArrayList(
            "jira-administrators",
            "jira-developers",
            "jira-users");
    private IssuesPage issuesPage;
    private TraceContext tracer;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        tracer = product.getPageBinder().bind(TraceContext.class);
        issuesPage = logInToIssuesPageAsSysadmin();
    }

    @Test
    public void testUserAndGroupsCustomField()
    {
        final String name = "User Group Custom";
        final String id = backdoor.customFields().createCustomField(name, "",
                "com.atlassian.jira.plugin.system.customfieldtypes:multiuserpicker",
                "com.atlassian.jira.plugin.system.customfieldtypes:userpickergroupsearcher");
        _testUserAndGroup(name, id);
    }

    @Test
    public void testUserCustomField()
    {
        final String name = "User Group Custom 2";
        final String id = backdoor.customFields().createCustomField(name, "",
                "com.atlassian.jira.plugin.system.customfieldtypes:userpicker",
                "com.atlassian.jira.plugin.system.customfieldtypes:userpickergroupsearcher");
        _testUserAndGroup(name, id);
    }


    @Test
    public void testGroupCustomField()
    {
        String name = "Group Custom";
        String id = backdoor.customFields().createCustomField(name, "",
                "com.atlassian.jira.plugin.system.customfieldtypes:multigrouppicker",
                "com.atlassian.jira.plugin.system.customfieldtypes:grouppickersearcher");
        _testGroup(name, id);
    }

    private void _testUserAndGroup(String fieldName, String fieldId)
    {
        log.log("Testing user and group searcher for field [" + fieldId + "]");
        issuesPage.newSearch();
        final MultiSelectClauseDialog multiSelect = issuesPage.getBasicQuery()
                .openAddCriteriaDialog()
                .selectAndOpenSearcher(fieldName, fieldId, MultiSelectClauseDialog.class, "", fieldId, "");
        final List<String> allOptions = multiSelect.getAllOptionLabels();
        for (String option : EXPECTED_USERS_GROUPS)
        {
            assertTrue("Expected suggested users to contain [" + option + "]" , allOptions.contains(option));
        }
        Tracer checkpoint = tracer.checkpoint();
        multiSelect.selectValues("Current User");
        issuesPage.waitForResults(checkpoint);
        assertEquals("Current User", getSelectedValue(fieldId));
        checkpoint = tracer.checkpoint();
        multiSelect.selectValues("fred");
        issuesPage.waitForResults(checkpoint);
        assertEquals("Current User, fred", getSelectedValue(fieldId));
    }

    private void _testGroup(String fieldName, String fieldId)
    {
        log.log("Testing group searcher for field [" + fieldId + "]");
        issuesPage.newSearch();
        final MultiSelectClauseDialog multiSelect = issuesPage.getBasicQuery()
                .openAddCriteriaDialog()
                .selectAndOpenSearcher(fieldName, fieldId, MultiSelectClauseDialog.class, "", fieldId, "");
        final List<String> allOptions = multiSelect.getAllOptionLabels();
        assertEquals(EXPECTED_GROUPS, allOptions);
        Tracer checkpoint = tracer.checkpoint();
        multiSelect.selectValues("jira-developers");
        issuesPage.waitForResults(checkpoint);
        assertEquals("jira-developers", getSelectedValue(fieldId));
        checkpoint = tracer.checkpoint();
        multiSelect.selectValues("jira-users");
        issuesPage.waitForResults(checkpoint);
        assertEquals("jira-developers, jira-users", getSelectedValue(fieldId));
    }

    public String getSelectedValue(String fieldId)
    {
        return issuesPage.getBasicQuery().getCriteria(fieldId, PrimeCriteriaUserPickerComponent.class, "", fieldId, fieldId).getSelectedValue();
    }


}
