package it.com.atlassian.jira.plugin.issuenav.func;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.plugin.issuenav.client.SearchRendererClient;
import com.atlassian.jira.plugin.issuenav.client.SearchRendererValue;
import com.atlassian.jira.plugin.issuenav.service.SearchRendererValueResults;
import com.atlassian.jira.plugin.issuenav.service.SearchResults;
import org.junit.Test;

/**
 * Test security and response for the search renderer resource
 *
 * @since v5.1
 */
@WebTest ({ Category.FUNC_TEST })
public class TestSearchRendererSystemTextFields extends FuncTestCase
{
    public static final String SUMMARY = "summary";
    public static final String COMMENT = "comment";
    public static final String ENVIRONMENT = "environment";
    public static final String DESCRIPTION = "description";
    public static final String THE_COMMENT = "The comment";
    public static final String THE_ENV = "The env";
    public static final String THE_DESC = "The desc";
    private SearchRendererClient client;
    public static final String THE_SUMMARY = "The summary";

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestIssueSearcherTextSystemFields.xml");
        client = new SearchRendererClient(getEnvironmentData());
    }

    @Test
    public void testValueSuccessSummaryJql()
    {

        final SearchResults results = client.getMultiForJql(SUMMARY + " ~ \"" + THE_SUMMARY + "\"");

        SearchRendererValueResults value = results.values;

        assertEquals(value.size(), 5);

        assertTrue(value.containsKey(SUMMARY));

        assertField(THE_SUMMARY, "Summary", SUMMARY, value.get(SUMMARY), true);

    }

    @Test
    public void testValueSuccessSummaryJqlMixedCase()
    {

        final SearchResults results = client.getMultiForJql("SumMary ~ \"" + THE_SUMMARY + "\"");

        SearchRendererValueResults value = results.values;

        assertEquals(value.size(), 5);

        assertTrue(value.containsKey(SUMMARY));

        assertField(THE_SUMMARY, "Summary", SUMMARY, value.get(SUMMARY), true);

    }

    @Test
    public void testValueSuccessCommentsJql()
    {

        final SearchResults results = client.getMultiForJql(COMMENT + " ~ \"" + THE_COMMENT + "\"");

        SearchRendererValueResults value = results.values;

        assertEquals(value.size(), 5);

        assertTrue(value.containsKey(COMMENT));

        assertField(THE_COMMENT, "Comment", COMMENT, value.get(COMMENT), true);

    }

    @Test
    public void testValueSuccessCommentJqlMixedCase()
    {

        final SearchResults results = client.getMultiForJql("ComMEnt ~ \"" + THE_COMMENT + "\"");

        SearchRendererValueResults value = results.values;

        assertEquals(value.size(), 5);

        assertTrue(value.containsKey(COMMENT));

        assertField(THE_COMMENT, "Comment", COMMENT, value.get(COMMENT), true);

    }

    @Test
    public void testValueSuccessNoProject()
    {
        SearchRendererValueResults value = client.getValue(SUMMARY, THE_SUMMARY, COMMENT, THE_COMMENT, ENVIRONMENT, THE_ENV, DESCRIPTION, THE_DESC);

        assertEquals(value.size(), 4);
        assertTrue(value.containsKey(SUMMARY));
        assertTrue(value.containsKey(DESCRIPTION));
        assertTrue(value.containsKey(COMMENT));
        assertTrue(value.containsKey(ENVIRONMENT));

        assertField(THE_SUMMARY, "Summary", SUMMARY, value.get(SUMMARY), true);
        assertField(THE_DESC, "Description", DESCRIPTION, value.get(DESCRIPTION), true);
        assertField(THE_COMMENT, "Comment", COMMENT, value.get(COMMENT), true);
        assertField(THE_ENV, "Environment", ENVIRONMENT, value.get(ENVIRONMENT), true);
    }

    @Test
    public void testValueSuccessValidProject()
    {

        SearchRendererValueResults value = client.getValue("pid", "10000", SUMMARY, THE_SUMMARY, COMMENT, THE_COMMENT, ENVIRONMENT, THE_ENV, DESCRIPTION, THE_DESC);

        assertEquals(value.size(), 5);
        assertTrue(value.containsKey("project"));
        assertEquals("project = HSP", value.get("project").jql);

        assertTrue(value.containsKey(SUMMARY));
        assertTrue(value.containsKey(DESCRIPTION));
        assertTrue(value.containsKey(COMMENT));
        assertTrue(value.containsKey(ENVIRONMENT));

        assertField(THE_SUMMARY, "Summary", SUMMARY, value.get(SUMMARY), true);
        assertField(THE_DESC, "Description", DESCRIPTION, value.get(DESCRIPTION), true);
        assertField(THE_COMMENT, "Comment", COMMENT, value.get(COMMENT), true);
        assertField(THE_ENV, "Environment", ENVIRONMENT, value.get(ENVIRONMENT), true);
    }

    @Test
    public void testValueSuccessInValidProject()
    {

        SearchRendererValueResults value = client.getValue("pid", "10001", SUMMARY, THE_SUMMARY, COMMENT, THE_COMMENT, ENVIRONMENT, THE_ENV, DESCRIPTION, THE_DESC);

        assertEquals(value.size(), 5);
        assertTrue(value.containsKey("project"));
        assertEquals("project = MKY", value.get("project").jql);

        assertTrue(value.containsKey(SUMMARY));
        assertTrue(value.containsKey(DESCRIPTION));
        assertTrue(value.containsKey(COMMENT));
        assertTrue(value.containsKey(ENVIRONMENT));

        assertField(THE_SUMMARY, "Summary", SUMMARY, value.get(SUMMARY), true);
        assertField(THE_DESC, "Description", DESCRIPTION, value.get(DESCRIPTION), false);
        assertField(THE_COMMENT, "Comment", COMMENT, value.get(COMMENT), true);
        assertField(THE_ENV, "Environment", ENVIRONMENT, value.get(ENVIRONMENT), false);
    }

    @Test
    public void testValueSuccessMultiProjects()
    {

        SearchRendererValueResults value = client.getValue("pid", "10001", "pid", "10000", SUMMARY, THE_SUMMARY, COMMENT, THE_COMMENT, ENVIRONMENT, THE_ENV, DESCRIPTION, THE_DESC);

        assertEquals(value.size(), 5);
        assertTrue(value.containsKey("project"));
        assertEquals("project in (MKY, HSP)", value.get("project").jql);

        assertTrue(value.containsKey(SUMMARY));
        assertTrue(value.containsKey(DESCRIPTION));
        assertTrue(value.containsKey(COMMENT));
        assertTrue(value.containsKey(ENVIRONMENT));

        assertField(THE_SUMMARY, "Summary", SUMMARY, value.get(SUMMARY), true);
        assertField(THE_DESC, "Description", DESCRIPTION, value.get(DESCRIPTION), true);
        assertField(THE_COMMENT, "Comment", COMMENT, value.get(COMMENT), true);
        assertField(THE_ENV, "Environment", ENVIRONMENT, value.get(ENVIRONMENT), true);
    }

    private void assertField(String fieldValue, String label, String field, SearchRendererValue result, boolean valid)
    {

        assertEquals(result.name, label);

        assertTrue(result.viewHtml.contains(fieldValue));

        // Asserts that some representation of edit html comes back from the server
        assertTrue(result.editHtml.contains(fieldValue));

        assertEquals(valid, result.validSearcher);

        assertEquals(field + " ~ \"" + fieldValue + "\"", result.jql);
    }


}