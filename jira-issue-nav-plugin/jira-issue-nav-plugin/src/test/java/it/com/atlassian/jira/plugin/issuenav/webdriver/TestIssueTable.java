package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.nimblefunctests.annotation.Restore;
import com.atlassian.jira.plugin.issuenav.pageobjects.AdvancedQueryComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.BasicQueryComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.ExtendedCriteriaComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.ResultsTableComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.ShareDialog;
import com.atlassian.jira.testkit.client.restclient.User;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@Restore("TestSearching.xml")
public class TestIssueTable extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();
    }

    @Test
    public void testJqlChangeUpdatesTable()
    {
        int resultCount = issuesPage.getResultsTable().getResultCount();
        assertEquals("Expected all issues to be shown on load", 14, resultCount);
        AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.searchAndWait("project = MEN");
        resultCount = issuesPage.getResultsTable().getResultCount();
        assertEquals("Expected all issues to be shown on load", 3, resultCount);
    }

    // JRADEV-18655
    @Test
    public void testJqlChangeUpdatesBasicSearch()
    {
        AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.searchAndWait("description ~ \"abc\" AND priority IN (Critical, Blocker)");
        ExtendedCriteriaComponent extendedCriteria = issuesPage.getBasicQuery().extendedCriteria();
        assertTrue(extendedCriteria.clauseExists("description"));
        assertTrue(extendedCriteria.clauseExists("priority"));
    }

    @Test
    public void testSharingSystemFilter()
    {
        issuesPage.getFilters().selectSystem("Reported by Me");
        final ShareDialog dialog = issuesPage.getShareDialog();
        dialog.open().addRecipient("admin");
        assertThat(dialog.getRecipients(), Matchers.contains("admin"));
        dialog.submit();
    }

    @Test
    public void testViewsMenu()
    {
        issuesPage.getFilters().selectSystem("My Open Issues");
        assertEquals(Lists.newArrayList("Printable", "Full Content", "XML"), issuesPage.openViewsMenu().getItemLabels());
    }

    @Test
    public void testNewSearch()
    {
        // The advanced (JQL) input should be cleared.
        AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.searchAndWait("project = MEN");
        issuesPage.newSearch();
        assertEquals(advancedQueryComponent.getCurrentQuery(), "");

        // ...even if they didn't actually run the search.
        advancedQueryComponent.getSearchInput().type("project = MEN");
        issuesPage.newSearch(false);
        waitUntilTrue(advancedQueryComponent.getSearchInput().timed().hasValue(""));

        // No filter should be selected.
        issuesPage.getFilters().waitForNoActiveFilter();
        issuesPage = issuesPage.getFilters().selectSystem("My Open Issues");
        issuesPage.newSearch();
        assertEquals(advancedQueryComponent.getCurrentQuery(), "");
        issuesPage.getFilters().waitForNoActiveFilter();

        // The basic searchers should be cleared.
        BasicQueryComponent basicQuery = issuesPage.getBasicQuery();
        issuesPage = issuesPage.getFilters().selectSystem("My Open Issues");
        issuesPage.newSearch();
        assertEquals("All", basicQuery.status().getSelectedValue());
        issuesPage.getFilters().waitForNoActiveFilter();

        // ...even if they didn't actually run the the text search.
        basicQuery.getSearchInput().type("Hello!");
        issuesPage.newSearch(false);
        waitUntilTrue(basicQuery.getSearchInput().timed().hasValue(""));
    }

    @Test
    public void testRefresh()
    {
        issuesPage.refreshTable();

        // New issues should appear after refreshing.
        int resultsCount = issuesPage.getResultsTable().getResultsCountTotal();
        backdoor.issues().createIssue("AN", "A new issue!");
        issuesPage.refreshTable();
        assertEquals(resultsCount + 1, issuesPage.getResultsTable().getResultsCountTotal());
    }

    @Test
    public void testSortingByColumnWithCustomFieldId()
    {
        AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.searchAndWait("order by cf[10000]");

        issuesPage.getResultsTable().sortColumn("customfield_10000", ResultsTableComponent.SORT_ORDER.desc);
        assertEquals("ORDER BY cf[10000] DESC", issuesPage.getAdvancedQuery().getCurrentQuery());

        issuesPage.getResultsTable().sortColumn("customfield_10000", ResultsTableComponent.SORT_ORDER.asc);
        assertEquals("ORDER BY cf[10000] ASC", issuesPage.getAdvancedQuery().getCurrentQuery());
    }

    @Test
    public void testSortingByColumnWithCustomFieldName()
    {
        AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.searchAndWait("order by \"date field\"");

        issuesPage.getResultsTable().sortColumn("customfield_10000", ResultsTableComponent.SORT_ORDER.desc);
        assertEquals("ORDER BY \"date field\" DESC", issuesPage.getAdvancedQuery().getCurrentQuery());

        issuesPage.getResultsTable().sortColumn("customfield_10000", ResultsTableComponent.SORT_ORDER.asc);
        assertEquals("ORDER BY \"date field\" ASC", issuesPage.getAdvancedQuery().getCurrentQuery());
    }

    @Test
    public void testSortingByColumnWithDescDefaultColumn()
    {
        AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.searchAndWait("order by issuetype");

        issuesPage.getResultsTable().sortColumn("issuetype", ResultsTableComponent.SORT_ORDER.asc);
        assertEquals("ORDER BY issuetype ASC", issuesPage.getAdvancedQuery().getCurrentQuery());

        issuesPage.getResultsTable().sortColumn("issuetype", ResultsTableComponent.SORT_ORDER.desc);
        assertEquals("ORDER BY issuetype DESC", issuesPage.getAdvancedQuery().getCurrentQuery());
    }

    @Test
    public void testSortingByColumnAliases()
    {
        AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.searchAndWait("ORDER BY summary ASC, issuekey ASC");

        //Want to call this function but stopped by another with the automatic column sorting: see issue JRADEV-19171.
        //issuesPage.getResultsTable().sortColumn("issuekey", ResultsTableComponent.SORT_ORDER.desc);

        issuesPage.getResultsTable().getColumnHeadingByDataId("issuekey").click();

        assertEquals("ORDER BY issuekey DESC, summary ASC", issuesPage.getAdvancedQuery().getCurrentQuery());
    }


    @Test
    public void testSortingByHtmlEscapedColumnAlias()
    {
        AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.searchAndWait("ORDER BY \"<field>\"");

        issuesPage.getResultsTable().getColumnHeadingByDataId("customfield_10500").click();
        assertEquals("ORDER BY \"<field>\" DESC", issuesPage.getAdvancedQuery().getCurrentQuery());
    }

    @Test
    public void testMultipleAssignToMeActionsAssignTheCorrectIssues()
    {
        backdoor.usersAndGroups().addUserToGroup("fred", "jira-developers");
        backdoor.issues().assignIssue("HAW-1", "fred");
        backdoor.issues().assignIssue("HAW-2", "fred");
        backdoor.issues().assignIssue("HAW-3", "fred");
        issuesPage.getBasicQuery().assignee().setValueAndSubmit("fred");

        ResultsTableComponent table = issuesPage.getResultsTable();
        for (int i = 0; i < 3; ++i)
        {
            table.assignToMeTheCurrentIssue();
            assertEquals("The assignee after assign-to-me should be admin", "admin",
                    ((User)backdoor.issues().getIssue(table.getSelectedIssueKey()).fields.get("assignee")).name);
            table.next();
        }
    }

    @Test
    public void testInvalidFilterShowErrorMessageAndRemoveIssueTable()
    {
        issuesPage = goToIssuesPage("?filter=-123456789");

        assertEquals("Opening invalid JQL should show the error message",
                "The requested filter doesn't exist or is private.", issuesPage.getJQLErrors());
        waitUntilEquals("No issues were found to match your search", issuesPage.getEmptySearchResultMessage());

        issuesPage.newSearch();
        assertEquals("SUM-1", issuesPage.getResultsTable().getSelectedIssueKey());

        issuesPage.back();
        assertEquals("Opening invalid JQL should show the error message",
                "The requested filter doesn't exist or is private.", issuesPage.getJQLErrors());
        waitUntilEquals("No issues were found to match your search", issuesPage.getEmptySearchResultMessage());
    }
}