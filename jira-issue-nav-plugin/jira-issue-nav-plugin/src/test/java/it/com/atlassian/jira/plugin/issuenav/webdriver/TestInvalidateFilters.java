package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import it.com.atlassian.jira.plugin.issuenav.webdriver.KickassWebDriverTestCase;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * @since v6.1
 */
public class TestInvalidateFilters extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;


    @Test
    public void testInvalidateFilterCascadeSelect() throws Exception
    {
        administration.restoreBlankInstance();
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();

        addCustomField("myfield", "com.atlassian.jira.plugin.system.customfieldtypes:cascadingselect");
        navigation.issue().createIssue("monkey", "Bug", "bug 1");

        issuesPage.getAdvancedQuery().searchAndWait("myfield in cascadeOption(\"None\")");
        issuesPage.getFilters().createFilter("css");

        assertIssuesInTable(issuesPage, "MKY-1");

        addCustomField("myfield", "com.atlassian.jira.plugin.system.customfieldtypes:textfield");

        goToIssuesPage("?filter=10000");
        assertEquals("myfield in cascadeOption(None)", issuesPage.getAdvancedQuery().getCurrentQuery());
        issuesPage.getAdvancedQuery().submit();
        assertEquals("The operator 'in' is not supported by the 'myfield' field.", issuesPage.getJQLErrors());
    }

    @Test
    public void testInvalidateFilterOperands() throws Exception
    {
        administration.restoreBlankInstance();
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();

        addCustomField("myfield", "com.atlassian.jira.plugin.system.customfieldtypes:url");

        issuesPage.getAdvancedQuery().searchAndWait("myfield = 'http://www.example.com'");
        issuesPage.getFilters().createFilter("field");
        final long exactTextFilterId = 10000;

        // add another customfield with the same name so that '=' is no longer a valid operator in the saved filter
        addCustomField("myfield", "com.atlassian.jira.plugin.system.customfieldtypes:textfield");
        assertInvalidFilter(exactTextFilterId, "myfield = \"http://www.example.com\"", "The operator '=' is not supported by the 'myfield' field.");

        // now do the same thing with relational operators <, <=, >, >=
        addCustomField("myfield_2", "com.atlassian.jira.plugin.system.customfieldtypes:float");

        issuesPage.getAdvancedQuery().searchAndWait("myfield_2 > 0");
        issuesPage.getFilters().createFilter("myfield2");
        final long gtFilterId = 10001;

        issuesPage.getAdvancedQuery().searchAndWait("myfield_2 >= 0");
        issuesPage.getFilters().createFilter("myfield gte");
        final long gteFilterId = 10002;

        issuesPage.getAdvancedQuery().searchAndWait("myfield_2 < 10");
        issuesPage.getFilters().createFilter("myfield lt");
        final long ltFilterId = 10003;

        issuesPage.getAdvancedQuery().searchAndWait("myfield_2 <= 10");
        issuesPage.getFilters().createFilter("myfield lte");
        final long lteFilterId = 10004;

        addCustomField("myfield_2", "com.atlassian.jira.plugin.system.customfieldtypes:userpicker");
        assertInvalidFilter(gtFilterId, "myfield_2 > 0", "The operator '>' is not supported by the 'myfield_2' field.");
        assertInvalidFilter(gteFilterId, "myfield_2 >= 0", "The operator '>=' is not supported by the 'myfield_2' field.");
        assertInvalidFilter(ltFilterId, "myfield_2 < 10", "The operator '<' is not supported by the 'myfield_2' field.");
        assertInvalidFilter(lteFilterId, "myfield_2 <= 10", "The operator '<=' is not supported by the 'myfield_2' field.");

        // now the ~ operator
        addCustomField("myfield_3", "com.atlassian.jira.plugin.system.customfieldtypes:textfield");
        issuesPage.getAdvancedQuery().searchAndWait("myfield_3 ~ freetext");
        issuesPage.getFilters().createFilter("myfield3");
        final long likeFilterId = 10005;

        addCustomField("myfield_3", "com.atlassian.jira.plugin.system.customfieldtypes:userpicker");
        assertInvalidFilter(likeFilterId, "myfield_3 ~ freetext", "The operator '~' is not supported by the 'myfield_3' field.");
    }

    private void assertInvalidFilter(long filterId, String jql, String message)
    {
        issuesPage.getFilters().selectFilter(filterId);
        assertEquals(jql, issuesPage.getAdvancedQuery().getCurrentQuery());
        issuesPage.getAdvancedQuery().submit();
        assertEquals(message, issuesPage.getJQLErrors());
    }

    private void addCustomField(final String fieldName, final String customFieldName)
    {
        administration.customFields().addCustomField(customFieldName, fieldName);
        administration.reIndex();
    }
}
