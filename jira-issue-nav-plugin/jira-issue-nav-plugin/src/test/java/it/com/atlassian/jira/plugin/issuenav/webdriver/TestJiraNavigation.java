package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests navigating from other parts of kickass to JIRA
 *
 * @since v5.2
 */
public class TestJiraNavigation extends KickassWebDriverTestCase
{
    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestNavigatorSwitching.xml");
    }

    /**
     * Clicking "Search for Issues" in the header should start a new search.
     */
    @Test
    public void testSearchForIssues()
    {
        logInToIssuesPage("fred", "fred").getListLayout();

        jqlSearch("project = XSS");

        // Clicking "Search for Issues" should start a new, empty search in
        // KickAss (the user's preference). Session search shouldn't be used.
        JiraHeader jiraHeader = product.getPageBinder().bind(JiraHeader.class);
        jiraHeader.getIssuesMenu().open().searchForIssues();
        IssuesPage issuesPage = product.getPageBinder().bind(IssuesPage.class);
        assertCurrentURL("/issues/?jql=");
        assertEquals("", issuesPage.getAdvancedQuery().getCurrentQuery());
    }

    /**
     * Clicking "Issues" in the header should start a new search or use the current session search
     */
    @Test
    public void testIssuesLink()
    {
        logInToIssuesPage("fred", "fred").getListLayout();

        // Navigating to empty page redirects to default query
        clickIssuesHeader();
        assertCurrentURL("/issues/?filter=-1");

        // Navigating to empty page with jql query in session search
        jqlSearch("project = BLUK");
        IssuesPage issuesPage = clickIssuesHeader();
        assertCurrentURL("/issues/?jql=project%20%3D%20BLUK");
        assertEquals("project = BLUK", issuesPage.getAdvancedQuery().getCurrentQuery());

        // Navigating to empty page with nefarious jql query in session search
        jqlSearch("text ~ \"<script>alert('oh noes')</script>\"");
        issuesPage = clickIssuesHeader();
        assertCurrentURL("/issues/?jql=text%20~%20%22%3Cscript%3Ealert(%27oh%20noes%27)%3C%2Fscript%3E%22",
                "/issues/?jql=text%20~%20%22%3Cscript%3Ealert('oh%20noes')%3C%2Fscript%3E%22");
        assertEquals("text ~ \"<script>alert('oh noes')</script>\"", issuesPage.getAdvancedQuery().getCurrentQuery());

        // Navigating to empty page with filter query in session search
        goToIssuesPage("?filter=10016");
        issuesPage = clickIssuesHeader();
        assertCurrentURL("/issues/?filter=10016");
        assertEquals("Bugs", issuesPage.getFilters().waitForActiveFilter().getActiveFilterName());

        // Navigating to empty page with filter query in session search
        goToIssuesPage("?filter=10016&jql=project%20%3D%20XSS");
        issuesPage = clickIssuesHeader();
        assertCurrentURL("/issues/?filter=10016&jql=project%20%3D%20XSS");
        assertEquals("Bugs", issuesPage.getFilters().waitForActiveFilter().getActiveFilterName());
        assertEquals("project = XSS", issuesPage.getAdvancedQuery().getCurrentQuery());
    }

    /**
     * Tests navigating with session search
     */
    @Test
    public void testSessionSearch()
    {
        logInToIssuesPage("fred", "fred").getListLayout();

        IssuesPage issuesPage = jqlSearch("project = BLUK").waitForResultsTable();
        assertCurrentURL("/issues/?jql=project%20%3D%20BLUK");
        assertEquals("project = BLUK", issuesPage.getAdvancedQuery().getCurrentQuery());
        assertEquals(3, issuesPage.getResultsTable().getResultsCountTotal());
        issuesPage = goToIssuesPage().waitForResultsTable();
        assertCurrentURL("/issues/?jql=project%20%3D%20BLUK");
        assertEquals("project = BLUK", issuesPage.getAdvancedQuery().getCurrentQuery());
        assertEquals(3, issuesPage.getResultsTable().getResultsCountTotal());

        issuesPage = goToIssuesPage("?filter=10016").waitForResultsTable();
        assertCurrentURL("/issues/?filter=10016");
        assertEquals("issuetype = Bug", issuesPage.getAdvancedQuery().getCurrentQuery());
        assertEquals(79, issuesPage.getResultsTable().getResultsCountTotal());
        issuesPage = goToIssuesPage().waitForResultsTable();
        assertCurrentURL("/issues/?filter=10016");
        assertEquals("issuetype = Bug", issuesPage.getAdvancedQuery().getCurrentQuery());
        assertEquals(79, issuesPage.getResultsTable().getResultsCountTotal());

        issuesPage = goToIssuesPage("?filter=10016&jql=project = XSS").waitForResultsTable();
        assertCurrentURL("/issues/?filter=10016&jql=project%20%3D%20XSS");
        assertEquals("project = XSS", issuesPage.getAdvancedQuery().getCurrentQuery());
        assertEquals(17, issuesPage.getResultsTable().getResultsCountTotal());
        issuesPage = goToIssuesPage().waitForResultsTable();
        assertCurrentURL("/issues/?filter=10016&jql=project%20%3D%20XSS");
        assertEquals("project = XSS", issuesPage.getAdvancedQuery().getCurrentQuery());
        assertEquals(17, issuesPage.getResultsTable().getResultsCountTotal());
        //TODO check for dirty filter element

        // Basic mode should set the session search (including the filter).
        issuesPage.getFilters().selectFavourite("Bugs");
        issuesPage.getBasicQuery().status().setValueAndSubmit("Open");
        issuesPage = goToIssuesPage().waitForResultsTable();
        assertCurrentURL("/issues/?filter=10016&jql=issuetype%20%3D%20Bug%20AND%20status%20%3D%20Open");
        assertEquals("Bugs", issuesPage.getFilters().getActiveFilterName());
    }

    @Test
    public void testLoginLinkReturnUrl()
    {
        product.logout();
        goToIssuesPage().getListLayout();

        IssuesPage issuesPage = jqlSearch("project = BULK").waitForResultsTable();
        assertEquals(environmentData.getBaseUrl() + "/login.jsp?os_destination=%2Fissues%2F%3Fjql%3Dproject%2520%253D%2520BULK", issuesPage.getLoginLinkUrl());

        issuesPage.getAdvancedQuery().searchAndWait("issuetype = Bug");
        assertEquals(environmentData.getBaseUrl() + "/login.jsp?os_destination=%2Fissues%2F%3Fjql%3Dissuetype%2520%253D%2520Bug", issuesPage.getLoginLinkUrl());
    }

    private IssuesPage clickIssuesHeader()
    {
        product.getTester().getDriver().findElement(By.id("find_link")).click();
        return product.getPageBinder().bind(IssuesPage.class);
    }

    private void assertCurrentURL(String firefoxUrlSuffix, String chromeUrlSuffix)
    {
        String expectedFirefoxUrl = product.getProductInstance().getBaseUrl() + firefoxUrlSuffix;
        String expectedChromeUrl = product.getProductInstance().getBaseUrl() + chromeUrlSuffix;
        assertTrue(expectedFirefoxUrl.equals(product.getTester().getDriver().getCurrentUrl()) ||
                expectedChromeUrl.equals(product.getTester().getDriver().getCurrentUrl()));

    }
}