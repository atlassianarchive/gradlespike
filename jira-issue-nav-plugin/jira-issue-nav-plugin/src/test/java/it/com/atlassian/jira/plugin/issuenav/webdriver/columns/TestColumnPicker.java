package it.com.atlassian.jira.plugin.issuenav.webdriver.columns;

import com.atlassian.jira.nimblefunctests.annotation.Restore;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.MultiSelectClauseDialog;
import com.atlassian.jira.plugin.issuenav.pageobjects.ResultsTableComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.columns.ColumnPicker;
import com.atlassian.pageobjects.elements.Option;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import it.com.atlassian.jira.plugin.issuenav.webdriver.KickassWebDriverTestCase;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertFalse;

/**
 * Testing the column picker in list view
 *
 * @since v6.1
 */
@Restore("TestColumnConfig.xml")
public class TestColumnPicker extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;
    private ColumnPicker columnPicker;
    private TraceContext traceContext;
    private Tracer tracer;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        traceContext = product.getPageBinder().bind(TraceContext.class);
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();
    }

    @Test
    public void testColumnPicker()
    {
        //Stateless
        _testNewSearchDisablesFilterColumns();
        _testSystemFiltersDisablesFilterColumns();

        //Stateful
        //Make sure these tests do not re use same piece of data
        _testFilterWithNoColumnsSelectsUserColumns();
        _testChangingUserColumns();
        _testFilterWithColumnsSelectsFilterColumns();
        _testRestoreDefaultsAfterUnselectingAll();
        _testColumnOperationsPreserveStableness();
        _testNewColumnsCanBeSorted();

        //Testing filters from other user. These tests should be at the end, they make a lot of changes to the state
        _testFilterWithoutPermissionAndWithoutColumnsSelectsUserColumns();
        _testFilterWithoutPermissionCantBeModified();
    }

    public void _testNewColumnsCanBeSorted()
    {
        ready();
        columnPicker.restoreCurrentConfigDefault();
        columnPicker.toggleColumnOptions("Project");
        issuesPage.getResultsTable().sortColumn("project", ResultsTableComponent.SORT_ORDER.desc);
    }

    @Test
    public void testColumnPickerForSystemColumns()
    {
        //Completely new user, without columns of anything
        product.backdoor().usersAndGroups().addUser("newnewuser");

        _testOnlyAdminCanGetAndModifySystemColumns();
        issuesPage = logInToIssuesPage("admin", "admin");
        _testAdminModifyingSystemColumnsGetWarningMessageAndCanExit();
    }

    public void _testNewSearchDisablesFilterColumns()
    {
        getColumnPicker();
        List<String> columnConfigs = columnPicker.getAllAvailableColumnConfigs();
        assertEquals("Only user and system columns are available on new search", Lists.newArrayList("user", "system"), columnConfigs);
    }

    public void _testSystemFiltersDisablesFilterColumns()
    {
        List<String> systemFilters = Lists.newArrayList("My Open Issues", "Reported by Me", "Recently Viewed", "All Issues");
        int count = 0;
        for (String systemFilter : systemFilters)
        {
            issuesPage.getFilters().selectSystem(systemFilter);
            getColumnPicker();

            List<String> columnConfigs = columnPicker.getAllAvailableColumnConfigs();

            assertEquals("Only user and system columns are available on new search", Lists.newArrayList("user", "system"), columnConfigs);
            assertColumnHeaders("T", "Key", "Summary", "Status", "Created", "Updated");
            count++;
        }
        assertEquals("System filters and assert counts should be 4", 4, count);
    }

    public void _testFilterWithNoColumnsSelectsUserColumns()
    {
        ready();
        issuesPage.getFilters().createFilter("NEW NO COL FILTER");
        List<String> columnConfigs = columnPicker.getAllAvailableColumnConfigs();
        assertEquals("User and filter columns are available on new filter",
                Lists.newArrayList("user", "filter", "system"), columnConfigs);
        assertEquals("User column is selected by default for filter with no columns",
                "user", columnPicker.getSelectedColumnConfig());
        assertColumnHeaders("T", "Key", "Summary", "Status", "Created", "Updated");
    }

    public void _testFilterWithColumnsSelectsFilterColumns()
    {
        ready();
        columnPicker.restoreCurrentConfigDefault();
        columnPicker.toggleColumnOptions("Issue Type", "Key", "Summary", "Assignee");

        issuesPage.getFilters().createFilter("New filter with columns");

        //Setting columns for filter
        columnPicker.selectColumnConfig("filter", false);
        //Make sure a new filter with no columns copies from the user columns
        assertColumnOptions("Reporter", "Priority", "Status", "Resolution", "Created", "Updated", "Due Date");

        //Keep columns = "Resolution"
        //Remove columns = "Reporter", "Priority", "Status", "Created", "Updated", "Due Date"
        //New columns = "Issue Type", "Key", "Summary", "Assignee" -- added at the end ALPHABETICALLY
        columnPicker.toggleColumnOptions("Issue Type", "Key", "Summary", "Assignee", "Reporter",
                "Priority", "Status", "Resolution", "Created", "Updated", "Due Date", "Issue Type", "Issue Type", "Resolution");

        assertColumnHeaders("Resolution", "Assignee", "T", "Key", "Summary");

        //Refreshing the page should show the filter with the new columns
        goToIssuesPage("?filter=" + getFilterId());
        assertColumnHeaders("Resolution", "Assignee", "T", "Key", "Summary");
        getColumnPicker();
        assertEquals("filter", columnPicker.getSelectedColumnConfig());

        //Switch back to user should use the user columns
        columnPicker.selectColumnConfig("user");
        assertColumnHeaders("Reporter", "P", "Status", "Resolution", "Created", "Updated", "Due");

        //Switch back to filter should use the filter columns
        columnPicker.selectColumnConfig("filter");
        assertColumnHeaders("Resolution", "Assignee", "T", "Key", "Summary");

    }

    public void _testChangingUserColumns()
    {
        ready();
        columnPicker.restoreCurrentConfigDefault();

        //User defaults are: Issue Type, Key, Summary, Assignee, Reporter, Priority, Status, Resolution, Created, Updated, Due
        //Toggling the same option twice shouldn't change it's positioning. New option should be added at the end ALPHABETICALLY
        columnPicker.toggleColumnOptions("Summary", "Assignee", "Created", "Due Date", "Last Viewed", "Environment", "Summary");
        assertColumnHeaders("T", "Key", "Summary", "Reporter", "P", "Status", "Resolution", "Updated", "Environment", "Last Viewed");
    }

    public void _testRestoreDefaultsAfterUnselectingAll()
    {
        ready();
        List<String> selected = Lists.transform(columnPicker.getColumnChooser().getSelectedOptions(),
                MultiSelectClauseDialog.OPTION_TO_STRING);
        //Removing all selected columns
        columnPicker.toggleColumnOptions(selected);

        waitUntilTrue("Restore defaults must be available when everything has been deselected", columnPicker.hasRestoreDefaultsLink());

        columnPicker.restoreCurrentConfigDefault();
        assertDefaultColumnHeaders();
    }
    
    public void _testFilterWithoutPermissionCantBeModified()
    {
        ready();
        issuesPage = logInToIssuesPage("admin", "admin");
        columnPicker.restoreCurrentConfigDefault();
        issuesPage.getFilters().createFilter("Not owner filter with columns");
        columnPicker.selectColumnConfig("filter", false);
        columnPicker.toggleColumnOptions("Summary", "Assignee", "Reporter", "Priority", "Last Viewed");

        String filterId = getFilterId();

        issuesPage.getFilters().getFilterDetailsMenu().editPermissions().setSharingGroupAndSubmit("jira-users");

        logInToIssuesPage("fred", "fred").getListLayout();
        goToIssuesPage("?filter=" + filterId);
        assertColumnHeaders("T", "Key", "Status", "Resolution", "Created", "Updated", "Due", "Last Viewed");
        columnPicker.open();
        assertEquals("filter", columnPicker.getSelectedColumnConfig());
        waitUntilFalse("Opening filter that user does not own should disable editing columns", columnPicker.canEditColumnChooser());
    }

    public void _testFilterWithoutPermissionAndWithoutColumnsSelectsUserColumns()
    {
        ready();
        columnPicker.restoreCurrentConfigDefault();
        issuesPage.getFilters().createFilter("Not owner filter without columns");

        String filterId = getFilterId();

        issuesPage.getFilters().getFilterDetailsMenu().editPermissions().setSharingGroupAndSubmit("jira-users");

        logInToIssuesPage("fred", "fred").getListLayout();
        goToIssuesPage("?filter=" + filterId);

        assertDefaultColumnHeaders();
        columnPicker.open();
        assertEquals("Opening a columnless filter that user does not own should select user",
                "user", columnPicker.getSelectedColumnConfig());

        columnPicker.selectColumnConfig("filter", false);
        waitUntilFalse("Opening filter that user does not own should disable editing columns", columnPicker.canEditColumnChooser());

        columnPicker.cancel();
        columnPicker.selectColumnConfig("filter", false);
        waitUntilFalse("Opening Column Picker again should still disable editing columns", columnPicker.canEditColumnChooser());
    }

    public void _testColumnOperationsPreserveStableness()
    {
        //Any columns operations should keep the stableness of the issue table
        ready();
        columnPicker.restoreCurrentConfigDefault();

        issuesPage.getBasicQuery().issueType().setValueAndSubmit("Bug");
        assertEquals(7, issuesPage.getResultsTable().getResultsCountTotal());

        issuesPage.getFilters().createFilter("POKEMON");

        backdoor.issues().createIssue("SUM", "GOTTA CATCHEM ALL?");
        backdoor.issues().createIssue("SUM", "PIKACHU I CHOOSE YOU");
        backdoor.issues().createIssue("SUM", "CHUUUUUUUUUUUU");

        columnPicker.toggleColumnOptions("Summary", "Assignee", "Reporter", "Priority", "Last Viewed");
        assertColumnHeaders("T", "Key", "Status", "Resolution", "Created", "Updated", "Due", "Last Viewed");
        assertEquals(7, issuesPage.getResultsTable().getResultsCountTotal());

        columnPicker.selectColumnConfig("filter", false);
        assertColumnHeaders("T", "Key", "Status", "Resolution", "Created", "Updated", "Due", "Last Viewed");
        assertEquals(7, issuesPage.getResultsTable().getResultsCountTotal());

        columnPicker.toggleColumnOptions("Summary", "Assignee", "Reporter");
        assertColumnHeaders("T", "Key", "Status", "Resolution", "Created", "Updated", "Due", "Last Viewed", "Assignee", "Reporter", "Summary");
        assertEquals(7, issuesPage.getResultsTable().getResultsCountTotal());

        columnPicker.restoreCurrentConfigDefault();
        assertColumnHeaders("T", "Key", "Status", "Resolution", "Created", "Updated", "Due", "Last Viewed");
        assertEquals(7, issuesPage.getResultsTable().getResultsCountTotal());
        assertEquals("user", columnPicker.getSelectedColumnConfig());

        columnPicker.restoreCurrentConfigDefault();
        assertDefaultColumnHeaders();
        assertEquals(7, issuesPage.getResultsTable().getResultsCountTotal());

        issuesPage.getFilters().selectFavourite("POKEMON");
        assertEquals(10, issuesPage.getResultsTable().getResultsCountTotal());
    }

    public void _testOnlyAdminCanGetAndModifySystemColumns()
    {
        product.backdoor().columnControl().setSystemDefaultColumns(Lists.newArrayList("summary", "issuetype", "resolution", "duedate"));

        ready();
        //Get
        assertTrue("Admin has access to system columns", columnPicker.getAllAvailableColumnConfigs().contains("system"));
        columnPicker.selectColumnConfig("system");
        //waitUntilTrue("System columns show a pre submit warning message", columnPicker.hasMessageInPicker());
        assertColumnHeaders("Summary", "T", "Resolution", "Due");
        assertEquals(Lists.newArrayList("Due Date", "Issue Type", "Resolution", "Summary"), //Alphabetically
                Lists.transform(columnPicker.getColumnChooser().getSelectedOptions(), MultiSelectClauseDialog.OPTION_TO_STRING));

        //Set
        columnPicker.toggleColumnOptions("Reporter", "Assignee", "Last Viewed");
        assertColumnHeaders("Summary", "T", "Resolution", "Due", "Assignee", "Last Viewed", "Reporter");

        logInToIssuesPage("fred", "fred").getListLayout();
        getColumnPicker();
        assertTrue("Non-admin has NO access to system columns", !columnPicker.getAllAvailableColumnConfigs().contains("system"));

        logInToIssuesPage("newnewuser", "newnewuser").getListLayout();
        getColumnPicker();
        assertTrue("Non-admin has NO access to system columns", !columnPicker.getAllAvailableColumnConfigs().contains("system"));
        assertColumnHeaders("Summary", "T", "Resolution", "Due", "Assignee", "Last Viewed", "Reporter");
    }

    public void _testAdminModifyingSystemColumnsGetWarningMessageAndCanExit()
    {
        product.backdoor().columnControl().setSystemDefaultColumns(Lists.newArrayList("summary", "issuetype", "resolution"));

        ready();
        columnPicker.selectColumnConfig("system");
        columnPicker.toggleColumnOptions("Reporter", "Assignee", "Last Viewed");
        assertColumnHeaders("Summary", "T", "Resolution", "Assignee", "Last Viewed", "Reporter");

        //Check there is a message and it is not closeable
        ColumnPicker.SystemModeMessage message = columnPicker.getSystemColumnsWarningMessage();
        waitUntilTrue("There is a SystemMode message", message.isPresent());
        assertFalse("The global message must not be closable", message.isCloseable());

        //Exis system mode
        message.exit();
        waitUntilFalse("SystemMode message should be hidden", message.isPresent());
        assertEquals("After exit SystemMode, selected column config is 'user'",
                "user", columnPicker.getSelectedColumnConfig());

        //Default columns for the user
        assertColumnHeaders("T", "Key", "Summary", "Status", "Created", "Updated");

        columnPicker.selectColumnConfig("system");
        message = columnPicker.getSystemColumnsWarningMessage();
        columnPicker.selectColumnConfig("user");
        waitUntilFalse("Switching to user clears the SystemMode dialog", message.isPresent());

        //New users get the system columns
        logInToIssuesPage("newnewuser", "newnewuser").getListLayout();
        assertColumnHeaders("Summary", "T", "Resolution", "Assignee", "Last Viewed", "Reporter");
    }



    private void assertColumnOptions(String... expected)
    {
        Set<String> expectedOptions = Sets.newHashSet(expected);
        List<Option> selectedOptions = columnPicker.getColumnChooser().getSelectedOptions();
        for (Option option : selectedOptions)
        {
            //Order doesn't really matter, as long as all the options are available
            assertTrue(expectedOptions.contains(option.text()));
        }
        assertEquals(expected.length, selectedOptions.size());
    }

    private void assertDefaultColumnHeaders()
    {
        assertColumnHeaders("T", "Key", "Summary", "Assignee", "Reporter", "P", "Status", "Resolution", "Created", "Updated", "Due");
    }

    private void assertColumnHeaders(String... expectedColumns)
    {
        List<String> actualColumns = issuesPage.getResultsTable().getColumnHeaders();
        //Remove the column picker trigger header
        //TODO to be removed when the trigger is being moved outside of issue table
        actualColumns.remove(actualColumns.size() - 1);
        assertEquals("Column headers should be the same and in the same order",
                Lists.newArrayList(expectedColumns), actualColumns);
    }

    private String getFilterId()
    {
        //Please change this if you have a better way
        //I need to get the id of a newly created filter in issue nav
        String[] params = product.getTester().getDriver().getCurrentUrl().split("\\?")[1].split("&");

        for (String param : params)
        {
            if (param.contains("filter="))
            {
                return param.replace("filter=", "");
            }
        }

        return "";
    }

    private void ready()
    {
        goToIssuesPage("?jql=");
        getColumnPicker();
    }


    private ColumnPicker getColumnPicker()
    {
        columnPicker = issuesPage.getColumnPicker();
        return columnPicker;
    }
}
