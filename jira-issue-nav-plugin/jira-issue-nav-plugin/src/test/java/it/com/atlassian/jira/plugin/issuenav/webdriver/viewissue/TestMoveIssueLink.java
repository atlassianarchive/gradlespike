package it.com.atlassian.jira.plugin.issuenav.webdriver.viewissue;

import com.atlassian.jira.nimblefunctests.annotation.Restore;
import com.atlassian.jira.pageobjects.pages.viewissue.Subtask;
import com.atlassian.jira.pageobjects.pages.viewissue.SubtaskModule;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.AuiMessage;
import com.atlassian.jira.plugin.issuenav.pageobjects.util.KickassLoginUtil;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.google.inject.Inject;
import it.com.atlassian.jira.plugin.issuenav.webdriver.KickassWebDriverTestCase;
import org.junit.Test;
import org.openqa.selenium.By;

import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

@Restore("TestInlineEdit.xml")
public class TestMoveIssueLink extends KickassWebDriverTestCase
{
    protected ViewIssuePage viewIssuePage;
    protected TraceContext traceContext;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        traceContext = product.getPageBinder().bind(TraceContext.class);
    }

    @Test
    public void testMoveIssueLinkSuccess()
    {
        viewIssuePage = KickassLoginUtil.kickAssWith(product, ViewIssuePage.class, "BULK-20");

        SubtaskModule subTaskModule = viewIssuePage.getSubTasksModule();

        List<Subtask> subTasks = subTaskModule.getSubtasks();
        assertThat(subTasks, hasSize(2));
        assertThat(subTasks.get(0).getSummary(), equalTo("sdfsd fsd fsad f"));
        assertThat(subTasks.get(1).getSummary(), equalTo("sadfsadfsadfsdafsadf"));

        moveIssueLinkExpectSuccess(10081L, "down");
        subTasks = subTaskModule.getSubtasks();
        assertThat(subTasks, hasSize(2));
        assertThat(subTasks.get(1).getSummary(), equalTo("sdfsd fsd fsad f"));
        assertThat(subTasks.get(0).getSummary(), equalTo("sadfsadfsadfsdafsadf"));

        moveIssueLinkExpectSuccess(10081L, "up");
        subTasks = subTaskModule.getSubtasks();
        assertThat(subTasks, hasSize(2));
        assertThat(subTasks.get(0).getSummary(), equalTo("sdfsd fsd fsad f"));
        assertThat(subTasks.get(1).getSummary(), equalTo("sadfsadfsadfsdafsadf"));
    }

    @Test
    public void testMoveIssueLinkNoId()
    {
        String url = product.getProductInstance().getBaseUrl() + "/secure/MoveIssueLink.jspa?currentSubTaskSequence=0&subTaskSequence=1";
        viewIssuePage = KickassLoginUtil.kickAssWith(product, ViewIssuePage.class, "BULK-20");
        AuiMessage message = moveIssueLinkExpectError(url);

        assertTrue(message.isError());
        assertThat(message.getText(), containsString("Cannot move sub-task when no parent issue exists."));
    }

    @Test
    public void testMoveIssueLinkNoCurrentSubTaskSequence()
    {
        String url = product.getProductInstance().getBaseUrl() + "/secure/MoveIssueLink.jspa?id=10079&subTaskSequence=1";
        viewIssuePage = KickassLoginUtil.kickAssWith(product, ViewIssuePage.class, "BULK-20");
        AuiMessage message = moveIssueLinkExpectError(url);

        assertTrue(message.isError());
        assertThat(message.getText(), containsString("Cannot move sub-task when current sequence is unset"));
    }

    @Test
    public void testMoveIssueLinkNosubTaskSequence()
    {
        String url = product.getProductInstance().getBaseUrl() + "/secure/MoveIssueLink.jspa?id=10079&currentSubTaskSequence=0";
        viewIssuePage = KickassLoginUtil.kickAssWith(product, ViewIssuePage.class, "BULK-20");
        AuiMessage message = moveIssueLinkExpectError(url);

        assertTrue(message.isError());
        assertThat(message.getText(), containsString("Cannot move sub-task when current sequence is unset"));
    }

    private AuiMessage moveIssueLinkExpectError(String url)
    {
        goToUrl(url);
        return product.getPageBinder().bind(AuiMessage.class, "");
    }

    private ViewIssuePage moveIssueLinkExpectSuccess(Long issueId, String direction)
    {
        Tracer tracer = traceContext.checkpoint();
        MoveableSubtask subtask = product.getPageBinder().bind(MoveableSubtask.class, issueId);
        goToUrl(subtask.getSubtaskMoveLink(direction));

        viewIssuePage.waitForAjaxRefresh(tracer);
        return viewIssuePage;
    }

    private void goToUrl(String url)
    {
        product.getTester().getDriver().navigate().to(url);
    }

    public static class MoveableSubtask
    {
        @Inject
        PageElementFinder finder;
        private Long issueId;

        public MoveableSubtask(Long issueId)
        {
            this.issueId = issueId;
        }

        public String getSubtaskMoveLink(String direction)
        {
            PageElement moveIcon = finder.find(By.cssSelector("#issuetable #issuerow" + issueId + " .subtask-reorder .icon-sort-" + direction));
            waitUntilTrue(moveIcon.timed().isPresent());
            // Unfortunately webdriver doesn't like clicking on link that is not visible
            // The move link is only visible when a mouse is hovered over it.
            // In the interest of not introducing a flaky test, this will instruct webdriver to directly navigate to the url
            // KA will be smart enough to do that without a page pop.
            return moveIcon.getAttribute("href");
        }
    }
}
