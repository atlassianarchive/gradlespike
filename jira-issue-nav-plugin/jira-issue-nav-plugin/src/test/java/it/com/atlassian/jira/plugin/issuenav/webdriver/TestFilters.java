package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.nimblefunctests.annotation.Restore;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.ResultsTableComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.filter.FilterActionsMenu;
import com.atlassian.jira.plugin.issuenav.pageobjects.filter.Filters;
import com.atlassian.jira.plugin.issuenav.pageobjects.filter.SaveFilterDialog;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.ResponsiveDesign;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.SplitLayout;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;

import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Tests that filter operations work.
 *
 * @since v5.2
 */
@Restore("TestFilters.xml")
public class TestFilters extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;
    private TraceContext tracer;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();
        tracer = product.getPageBinder().bind(TraceContext.class);

        //Make sure that the filters panel is docked as these tests assume that it is.
        final Filters filters = issuesPage.getFilters();
        if (!filters.isDocked())
        {
            filters.dock();
        }
    }

    @Test
    public void testViewNonfavouriteFilter() throws Exception
    {
        String searchTitle = issuesPage.getFilters().getSearchTitle();
        assertEquals("Search title should start as 'Search'", "Search", searchTitle);

        // fred's non-favourite filter
        Tracer checkpoint = tracer.checkpoint();
        issuesPage = issuesPage.getFilters().selectFilter(10000L).waitForResults(checkpoint);
        searchTitle = issuesPage.getFilters().getSearchTitle();
        assertEquals("For Fred's filter, search title was not updated to the filter name", "Fred's Shared Filter", searchTitle);

        // own non-favourite filter
        checkpoint = tracer.checkpoint();
        issuesPage = this.issuesPage.getFilters().selectFilter(10100L).waitForResults(checkpoint);
        searchTitle = issuesPage.getFilters().getSearchTitle();
        assertEquals("For my own non-favourite filter, search title was not updated to the filter name", "foo", searchTitle);
    }

    @Test
    public void testDockingAndUndocking()
    {
        Filters filters = issuesPage.getFilters();
        assertTrue("Expected filters panel to be docked by default", filters.isDocked());
        filters.undock();
        goToIssuesPage();
        assertTrue("Filter panel should be collapsed", filters.isCollapsed());
        assertTrue("Filter panel should not be docked", !filters.isDocked());

        //Open the panel again to return to its initial state.
        filters.dock();

        assertTrue("Filter panel should be docked.", filters.isDocked());
        assertTrue("Filter panel should not be collapsed when docked.", !filters.isCollapsed());
    }

    @Test
    public void testExpandingAndContractingFilterSidebar()
    {
        product.getTester().getDriver().manage().window().maximize();
        Filters filters = issuesPage.getFilters();

        filters.expandBy(200);
        assertThat("Sidebar's width should be 400px", 400, is(filters.getWidth()));
        filters.expandBy(100);
        assertThat("Sidebar's width should be 500px", 500, is(filters.getWidth()));

        filters.undock();
        filters.open();

        assertThat("Undocked sidebar's width should be 500px", 500, is(filters.getWidth()));

        filters.dock();

        filters.contractBy(100);
        assertThat("Sidebar's width should be 400px", 400, is(filters.getWidth()));
        filters.contractBy(200);
        assertThat("Sidebar's width should be 200px", 200, is(filters.getWidth()));
    }

    @Test
    public void testCannotSaveUnownedFilter() throws Exception
    {
        issuesPage = issuesPage.getFilters().selectFilter(10000L).waitForResultsTable();
        issuesPage.getBasicQuery().project().setValueAndSubmit("Antarctica").waitForResultsTable();
        assertFalse(issuesPage.getFilters().canSaveFilterChanges());
    }

    @Test
    public void testSavingFilters() throws Exception
    {
        // Saving changes to an existing filter.
        issuesPage = issuesPage.getFilters().selectFilter(10200L).waitForResultsTable();
        issuesPage.getBasicQuery().project().setValueAndSubmit("Bali").waitForResultsTable();
        assertTrue(issuesPage.getFilters().canSaveFilterChanges());
        issuesPage = issuesPage.getFilters().saveFilter();
        assertFalse(issuesPage.getFilters().canSaveFilterChanges());

        // Creating a new filter from a system filter.
        issuesPage = issuesPage.getFilters().selectSystem("My Open Issues").waitForResultsTable();
        issuesPage.getBasicQuery().project().setValueAndSubmit("Bali").waitForResultsTable();
        assertTrue(issuesPage.getFilters().canSaveCopy());
        issuesPage = issuesPage.getFilters().createFilter("My Open Issues Copy");
        waitUntilEquals("My Open Issues Copy", issuesPage.getFilters().getActiveFilterElement().timed().getText());
    }

    @Test
    public void testSavingFilterWithNoName()
    {
        issuesPage = issuesPage.getFilters().selectFilter(10200L).waitForResultsTable();
        SaveFilterDialog saveFilterDialog = issuesPage.getFilters().openSaveFilterDialog();
        assertTrue(saveFilterDialog.submit());
        saveFilterDialog.setName("test");
        assertFalse(saveFilterDialog.submit());
    }

    @Test
    public void testSystemFilters() throws Exception
    {
        issuesPage = goToIssuesPage("?filter=-1");
        assertEquals("Current User", issuesPage.getBasicQuery().assignee().getSelectedValue());
        issuesPage.getFilters().selectSystem("Reported by Me");
        assertEquals("All", issuesPage.getBasicQuery().assignee().getSelectedValue());
        assertEquals("Current User", issuesPage.getBasicQuery().reporter().getSelectedValue());
        issuesPage.getFilters().selectSystem("My Open Issues");
        assertEquals("Current User", issuesPage.getBasicQuery().assignee().getSelectedValue());
        issuesPage = goToIssuesPage("?filter=-2");
        assertEquals("All", issuesPage.getBasicQuery().assignee().getSelectedValue());
        assertEquals("Current User", issuesPage.getBasicQuery().reporter().getSelectedValue());
    }

    @Test
    public void testModifiedSystemFilter() throws Exception
    {
        issuesPage = goToIssuesPage("?filter=-1&jql=project%20%3D%20BALI%20AND%20resolution%20%3D%20Unresolved%20AND%20assignee%20in%20(currentUser())%20ORDER%20BY%20updatedDate%20DESC");
        assertEquals("Current User", issuesPage.getBasicQuery().assignee().getSelectedValue());
        assertEquals("Bali", issuesPage.getBasicQuery().project().getSelectedValue());
    }

    @Test
    public void testSavingModifiedSystemFilter()
    {
        issuesPage = goToIssuesPage("?filter=-1&jql=project%20%3D%20FIJI%20AND%20resolution%20%3D%20Unresolved%20AND%20assignee%20in%20(currentUser())%20ORDER%20BY%20updatedDate%20DESC");
        issuesPage.getBasicQuery().status().setValueAndClickOutside("Open");
        issuesPage.getFilters().createFilter("New Snazzy Filter");
        assertEquals("Fiji", issuesPage.getBasicQuery().project().getSelectedValue());
        assertEquals("Current User", issuesPage.getBasicQuery().assignee().getSelectedValue());
    }

    @Test
    public void testSavingModifiedFavouriteFilter()
    {
        issuesPage.getFilters().selectFavourite("Less than 10");
        issuesPage.getResultsTable().sortColumn("issuekey", ResultsTableComponent.SORT_ORDER.asc);
        issuesPage = issuesPage.waitForResultsTable();
        issuesPage.getFilters().createFilterFromOperationsMenu("Less than 10 ordered");
        assertEquals("created < 10m ORDER BY key ASC", issuesPage.getAdvancedQuery().getCurrentQuery());
    }

    // JRADEV-15194
    @Test
    public void testSwitchingBetweenAdvancedFilters()
    {
        issuesPage.getFilters().selectSystem("Recently Viewed");
        assertEquals("issuekey in issueHistory() ORDER BY lastViewed DESC", issuesPage.getAdvancedQuery().getCurrentQuery());
        issuesPage.getFilters().selectFavourite("Less than 10");
        assertEquals("created < 10m", issuesPage.getAdvancedQuery().getCurrentQuery());
    }

    @Test
    public void testRenamingUnselectedFilter()
    {
        issuesPage.getFilters().selectFavourite("Less than 10");
        FilterActionsMenu filterActionsMenu = issuesPage.getFilters().openFilterActionsForFilter(10200);
        Tracer trace = tracer.checkpoint();
        filterActionsMenu.rename("Something new", trace);
        assertTrue(issuesPage.getFilters().favouriteFilterExists("Something new"));
    }

    @Test
    public void testRenamingSelectedFilter()
    {
        issuesPage.getFilters().selectFavourite("Admin's Filter");
        FilterActionsMenu filterActionsMenu = issuesPage.getFilters().openFilterActionsForFilter(10200);
        Tracer trace = tracer.checkpoint();
        filterActionsMenu.rename("Something new", trace);
        assertTrue(issuesPage.getFilters().favouriteFilterExists("Something new"));
        assertEquals("Something new", issuesPage.getCurrentSearchTitle());
    }

    @Test
    public void testRemovingUnselectedFilterFromFavourites()
    {
        issuesPage.getFilters().selectFavourite("Less than 10");
        FilterActionsMenu filterActionsMenu = issuesPage.getFilters().openFilterActionsForFilter(10200);
        assertTrue(issuesPage.getFilters().favouriteFilterExists("Admin's Filter"));
        filterActionsMenu.removeFromFavourites();
        assertFalse(issuesPage.getFilters().favouriteFilterExists("Admin's Filter"));
    }

    @Test
    public void testRemovingSelectedFilterFromFavourites()
    {
        issuesPage.getFilters().selectFavourite("Admin's Filter");
        FilterActionsMenu filterActionsMenu = issuesPage.getFilters().openFilterActionsForFilter(10200);
        assertTrue(issuesPage.getFilters().favouriteFilterExists("Admin's Filter"));
        filterActionsMenu.removeFromFavourites();
        assertFalse(issuesPage.getFilters().favouriteFilterExists("Admin's Filter"));
    }

    @Test
    public void testCopyingUnselectedFilter()
    {
        issuesPage.getFilters().selectFavourite("Less than 10");
        FilterActionsMenu filterActionsMenu = issuesPage.getFilters().openFilterActionsForFilter(10200);
        Tracer trace = tracer.checkpoint();
        filterActionsMenu.copyFilter("Copy of Admin's Filter", trace);
        assertTrue(issuesPage.getFilters().favouriteFilterExists("Copy of Admin's Filter"));
    }

    @Test
    public void testCopyingSelectedFilter()
    {
        issuesPage.getFilters().selectFavourite("Admin's Filter");
        FilterActionsMenu filterActionsMenu = issuesPage.getFilters().openFilterActionsForFilter(10200);
        Tracer trace = tracer.checkpoint();
        filterActionsMenu.copyFilter("Copy of Admin's Filter", trace);
        assertTrue(issuesPage.getFilters().favouriteFilterExists("Copy of Admin's Filter"));
        assertEquals("Copy of Admin's Filter", issuesPage.getFilters().getActiveFilterName());
    }

    @Test
    public void testCopyingFilterPreservesColumns()
    {
        issuesPage.getFilters().selectFavourite("Admin's Filter");
        FilterActionsMenu filterActionsMenu = issuesPage.getFilters().openFilterActionsForFilter(10200);
        Tracer trace = tracer.checkpoint();
        filterActionsMenu.copyFilter("Copy of Admin's Filter", trace);
        issuesPage.getFilters().selectFavourite("Copy of Admin's Filter");
        assertEquals(Lists.newArrayList("T, Key, Summary, Status, Created, Updated, Labels, ").toString(), issuesPage.getResultsTable().getColumnHeaders().toString());
    }

    @Test
    public void testSaveAsFilterPreservesColumns()
    {
        issuesPage.getFilters().selectFavourite("Admin's Filter");
        issuesPage.getFilters().openFilterActionsForFilter(10200);
        issuesPage.getBasicQuery().assignee().setValueAndSubmit("admin");
        issuesPage.getFilters().createFilter("My Filter");
        issuesPage.getFilters().selectFavourite("My Filter");
        assertEquals(Lists.newArrayList("T, Key, Summary, Status, Created, Updated, Labels, ").toString(), issuesPage.getResultsTable().getColumnHeaders().toString());
        assertEquals("admin", issuesPage.getBasicQuery().assignee().getSelectedValue());
    }

    @Test
    public void testDeletingUnselectedFilter()
    {
        assertTrue(issuesPage.getFilters().favouriteFilterExists("Admin's Filter"));
        issuesPage.getFilters().selectFavourite("Less than 10");
        FilterActionsMenu filterActionsMenu = issuesPage.getFilters().openFilterActionsForFilter(10200);
        filterActionsMenu.deleteFilterWithNoSubsequentSearch();
        assertFalse(issuesPage.getFilters().favouriteFilterExists("Admin's Filter"));
        assertEquals("Less than 10", issuesPage.getCurrentSearchTitle());
    }

    @Test
    public void testDeletingSelectedFilter()
    {
        issuesPage.getFilters().selectFilter(10200L);
        FilterActionsMenu filterActionsMenu = issuesPage.getFilters().openFilterActionsForFilter(10200);
        assertTrue(issuesPage.getFilters().favouriteFilterExists("Admin's Filter"));
        Tracer trace = tracer.checkpoint();
        filterActionsMenu.deleteFilter(trace);
        assertFalse(issuesPage.getFilters().favouriteFilterExists("Admin's Filter"));
        assertEquals("Search", issuesPage.getCurrentSearchTitle());
    }

    @Test
    public void testUnownedFilterShowsOnlyCopyAndRemove()
    {
        issuesPage.getFilters().selectFavourite("Admin's Filter");
        FilterActionsMenu filterActionsMenu = issuesPage.getFilters().openFilterActionsForFilter(10200);
        assertTrue(issuesPage.getFilters().favouriteFilterExists("Admin's Filter"));
        Tracer trace = tracer.checkpoint();
        filterActionsMenu.deleteFilter(trace);
        assertFalse(issuesPage.getFilters().favouriteFilterExists("Admin's Filter"));
        assertEquals("Search", issuesPage.getCurrentSearchTitle());
    }

    @Test
    public void testInvalidFilters()
    {
        // Viewing a private filter.
        Tracer checkpoint = tracer.checkpoint();
        issuesPage = goToIssuesPage("?filter=" + backdoor.searchRequests()
            .createFilter("fred", "", "Private Filter", ""));

        issuesPage.waitForResults(checkpoint, true);
        assertTrue(issuesPage.hasJQLErrors());
        assertFalse(issuesPage.isQueryVisible());

        issuesPage.newSearch();
        assertFalse(issuesPage.hasJQLErrors());
        assertTrue(issuesPage.isQueryVisible());

        issuesPage.backAndWait(true, true);
        assertTrue(issuesPage.hasJQLErrors());
        assertFalse(issuesPage.isQueryVisible());

        // Invalid filter ID.
        checkpoint = tracer.checkpoint();
        issuesPage = goToIssuesPage("?filter=lol");
        issuesPage.waitForResults(checkpoint, true);
        assertTrue(issuesPage.hasJQLErrors());
        assertFalse(issuesPage.isQueryVisible());
    }

    @Test
    public void testUpdatedDropdown()
    {
        issuesPage.getFilters().selectFilter(10200L);
        assertFalse(issuesPage.getFilters().getUpdatedDropDown().isExists());
        issuesPage.getBasicQuery().project().setValueAndSubmit("Bali").waitForResultsTable();
        Tracer trace = tracer.checkpoint();
        issuesPage.getFilters().getUpdatedDropDown().openAndClick(By.className("discard-filter-changes"));
        issuesPage.waitForResults(trace);
        trace = tracer.checkpoint();
        issuesPage.getBasicQuery().project().setValueAndSubmit("Peru").waitForResultsTable();
        issuesPage.waitForResults(trace);
        issuesPage.getFilters().getUpdatedDropDown().openAndClick(By.className("save-as-new-filter"));
        trace = tracer.checkpoint();
        product.getPageBinder().bind(SaveFilterDialog.class).setName("My new filter").submit();
        issuesPage.waitForResults(trace);
        issuesPage.getBasicQuery().project().setValueAndSubmit("Fiji").waitForResultsTable();
        trace = tracer.checkpoint();
        issuesPage.getFilters().getUpdatedDropDown().openAndClick(By.className("discard-filter-changes"));
        issuesPage.waitForResults(trace);
    }

    @Test
    public void testCriteriaFocusBehaviour()
    {
        issuesPage.newSearch();
        Poller.waitUntilTrue("New issue should have the first criteria being focused", issuesPage.isFirstCriteriaFocused());

        issuesPage.getFilters().selectSystem("Reported by Me").waitForResultsTable();
        Poller.waitUntilFalse("First criteria when loading a filter should not be focused", issuesPage.isFirstCriteriaFocused());
    }

    @Test
    public void testUnmodifiedFilterHasNoJqlParamInUrl()
    {
        issuesPage = goToIssuesPage("?filter=-2");
        String filterQuery = issuesPage.getAdvancedQuery().getCurrentQuery();

        issuesPage.getAdvancedQuery().searchAndWait("");
        assertCurrentURL("/issues/?filter=-2&jql=");

        issuesPage.getAdvancedQuery().searchAndWait(filterQuery);
        assertCurrentURL("/issues/?filter=-2");
    }

    @Test
    public void testTogglingLayoutsRetainsColumns()
    {
        issuesPage.getFilters().selectFilter(10200L);
        List<String> columnHeaders = issuesPage.getResultsTable().getColumnHeaders();
        assertEquals(Lists.newArrayList("T, Key, Summary, Status, Created, Updated, Labels, ").toString(), columnHeaders.toString());
        issuesPage.getSplitLayout();
        issuesPage.getListLayout();
        columnHeaders = issuesPage.getResultsTable().getColumnHeaders();
        assertEquals(Lists.newArrayList("T, Key, Summary, Status, Created, Updated, Labels, ").toString(), columnHeaders.toString());
    }


    @Test
    public void testNavigatingToPrivateFilterWithSelectedIssue()
    {
        IssueDetailComponent issueDetailComponent = goToIssuePage("BALI-1", null, 1353545L);
        assertEquals("Expected issue to load even though filter doesn't exist", "BALI-1", issueDetailComponent.getIssueKey());
        assertEquals("Expected url to be removed of invalid filter", "/browse/BALI-1", issuesPage.getWindowLocation());

        // Swithc to split layout
        issuesPage = goToIssuesPage("?jql=");
        issuesPage.getSplitLayout();

        issueDetailComponent = goToIssuePage("BALI-1", null, 1353545L);
        assertEquals("Expected issue to load even though filter doesn't exist", "BALI-1", issueDetailComponent.getIssueKey());
        assertEquals("Expected url to be removed of invalid filter", "/browse/BALI-1", issuesPage.getWindowLocation());

    }

    @Test
    public void testInvalidJQLCannotChangeLayouts() {
        issuesPage.newSearch();
        issuesPage.getAdvancedQuery().searchAndWait("invalid jql");
        Poller.waitUntilTrue(issuesPage.isLayoutSwitcherDisabled());
    }

    @Test
    public void testInvalidJQLCannotChangeLayoutsViaKeyboardShortcut() throws InterruptedException {
        issuesPage.newSearch();
        issuesPage.getAdvancedQuery().searchAndWait("invalid jql");
        issuesPage.switchLayoutsWithoutWait();

        // Wait 5s
        Thread.sleep(5 * 1000L);

        assertTrue(!issuesPage.isSplitLayout());

        issuesPage.getAdvancedQuery().searchAndWait("");

        issuesPage.switchLayouts();
    }
}