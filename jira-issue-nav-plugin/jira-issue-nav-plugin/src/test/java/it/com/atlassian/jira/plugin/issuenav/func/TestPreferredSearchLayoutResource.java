package it.com.atlassian.jira.plugin.issuenav.func;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.plugin.issuenav.client.PreferredSearchLayoutClient;
import com.atlassian.jira.plugin.issuenav.util.PreferredSearchLayoutService;
import org.junit.Test;

/**
 * Tests for <tt>PreferredSearchLayoutResource</tt>.
 *
 * @since v6.0
 */
public class TestPreferredSearchLayoutResource extends FuncTestCase
{
    private PreferredSearchLayoutClient client;

    @Override
    protected void setUpTest()
    {
        client = new PreferredSearchLayoutClient(environmentData);
    }

    @Test
    public void testSetPreferredSearchLayout()
    {
        String layoutKey = PreferredSearchLayoutService.SPLIT_VIEW_LAYOUT;

        client.loginAs("admin");
        client.setPreferredSearchLayout(layoutKey);
        assertEquals(layoutKey, client.getPreferredSearchLayout());
    }
}