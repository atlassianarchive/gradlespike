package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.nimblefunctests.annotation.Restore;
import com.atlassian.jira.pageobjects.components.CalendarPicker;
import com.atlassian.jira.pageobjects.components.CalendarPopup;
import com.atlassian.jira.pageobjects.components.menu.IssueActions;
import com.atlassian.jira.pageobjects.dialogs.LogWorkDialog;
import com.atlassian.jira.pageobjects.dialogs.quickedit.WorkflowTransitionDialog;
import com.atlassian.jira.pageobjects.model.DefaultIssueActions;
import com.atlassian.jira.pageobjects.model.WorkflowIssueAction;
import com.atlassian.jira.pageobjects.navigator.SelectedIssue;
import com.atlassian.jira.plugin.issuenav.pageobjects.BasicQueryComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.DateSearcher;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.PrimeCriteriaDateComponent;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.Test;

import java.util.LinkedHashMap;

import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestDatePicker extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestSearching.xml");
        backdoor.customFields().createCustomField("My Custom Date", "blah blah blah", "com.atlassian.jira.plugin.system.customfieldtypes:datepicker", "com.atlassian.jira.plugin.system.customfieldtypes:daterange");
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();
    }

    @Test
    public void testSettingDueDate()
    {
        BasicQueryComponent basic = issuesPage.getBasicQuery();
        DateSearcher dueDateDialog = addDueDate();
        dueDateDialog.setMoreThanMinutes("5");
        final PrimeCriteriaDateComponent duedate = basic.duedate();
        assertEquals("More than 5 minutes ago", duedate.getSelectedValue());
        assertEquals(DateSearcher.DATE_TYPE.moreThan, duedate.open().getSelectedDateType());
    }

    @Test
    public void testSettingInvalidDueDate()
    {
        BasicQueryComponent basic = issuesPage.getBasicQuery();
        DateSearcher dueDateDialog = addDueDate();
        dueDateDialog.setDueInNextMinutes("dfsfdsa");
        dueDateDialog = product.getPageBinder().bind(DateSearcher.class);
        assertEquals("Invalid period format.", dueDateDialog.getError());
        assertEquals(DateSearcher.DATE_TYPE.dueInNext, dueDateDialog.getSelectedDateType());
    }

    @Test
    public void testSettingDate()
    {
        LinkedHashMap<String, String> dateSearchers = new LinkedHashMap<String, String>();
        dateSearchers.put("created", "Created Date");
        dateSearchers.put("resolutiondate", "Resolution Date");
        dateSearchers.put("updated", "Updated Date");
        dateSearchers.put("customfield_10100", "My Custom Date");
        for (String id : dateSearchers.keySet())
        {
            final String label = dateSearchers.get(id);
            BasicQueryComponent basic = issuesPage.getBasicQuery();
            final DateSearcher dateSearcher = basic.openAddCriteriaDialog().selectAndOpenSearcher(label, id, DateSearcher.class);
            dateSearcher.setWithinTheLast("5");
            final PrimeCriteriaDateComponent lozenge = basic.getCriteria(id, PrimeCriteriaDateComponent.class);
            assertEquals("Within the last 5 minutes", lozenge.getSelectedValue());
        }
    }

    @Test
    public void testSettingDateError()
    {
        LinkedHashMap<String, String> dateSearchers = new LinkedHashMap<String, String>();
        dateSearchers.put("created", "Created Date");
        dateSearchers.put("resolutiondate", "Resolution Date");
        dateSearchers.put("updated", "Updated Date");
        dateSearchers.put("customfield_10100", "My Custom Date");

        for (String id : dateSearchers.keySet())
        {
            final String label = dateSearchers.get(id);
            BasicQueryComponent basic = issuesPage.getBasicQuery();
            DateSearcher dateSearcher = basic.openAddCriteriaDialog().selectAndOpenSearcher(label, id, DateSearcher.class);
            dateSearcher.setWithinTheLast("-5");
            dateSearcher = product.getPageBinder().bind(DateSearcher.class);
            assertEquals("Invalid period format.", dateSearcher.getError());
            assertEquals(DateSearcher.DATE_TYPE.withinTheLast, dateSearcher.getSelectedDateType());
            final PrimeCriteriaDateComponent lozenge = basic.getCriteria(id, PrimeCriteriaDateComponent.class);
            assertEquals("Within the last -5m", lozenge.getSelectedValue());
        }
    }

    /**
     * The date picker should work when modifying system filters.
     */
    @Test
    public void testSystemFilters()
    {
        issuesPage.getFilters().selectSystem("My Open Issues");
        DateSearcher dueDateDialog = addDueDate();
        dueDateDialog.setDueInNextMinutes("1337");

        // The dialog will re-open if there's a validation error.
        assertFalse(dueDateDialog.isOpen());
    }

    @Test
    public void testCalendarPicker()
    {
        DateSearcher dueDateDialog = addDueDate();
        dueDateDialog.setDateBetween("10/May/13", "16/May/13");
        assertFalse(dueDateDialog.isOpen());
        assertEquals(2, issuesPage.getResultsTable().getResultCount());
        assertTrue(issuesPage.getResultsTable().containsIssue("BALI-1"));
        assertTrue(issuesPage.getResultsTable().containsIssue("MEN-2"));
    }

    @Test
    public void testDateTimePickerInLogWorkDialogFromIssueNavigator()
    {
        SelectedIssue issue = issuesPage.getListLayout().getResultsTable().getSelectedIssue();
        issue.getActionsMenu().open().clickItem(DefaultIssueActions.LOG_WORK);
        LogWorkDialog logWorkDialog = product.getPageBinder().bind(LogWorkDialog.class);
        testCalendarPopupSelectsDayByClick(logWorkDialog.getDateStarted());
    }

    @Test
    public void testDateTimePickerInCloseIssueDialogFromIssueNavigator() throws Exception
    {
        administration.restoreData("TestCalendarInDialog.xml");
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();

        SelectedIssue issue = issuesPage.getListLayout().getResultsTable().getSelectedIssue();
        issue.getActionsMenu().open().clickItem(IssueActions.CLOSE_ISSUE);
        WorkflowTransitionDialog logWorkDialog = product.getPageBinder().bind(WorkflowTransitionDialog.class, WorkflowIssueAction.CLOSE_ISSUE.workflowActionId());
        testCalendarPopupSelectsDayByClick(logWorkDialog.getCustomField(CalendarPicker.class, 10000));
        testCalendarPopupSelectsDayByClick(logWorkDialog.getCustomField(CalendarPicker.class, 10001));
    }

    private void testCalendarPopupSelectsDayByClick(CalendarPicker calendarPicker)
    {
        CalendarPopup calendarPopup = calendarPicker.openCalendarPopup();
        Poller.waitUntilTrue("Calender Popup did not open successfully", calendarPopup.isOpen());

        final int expectedDay = selectOtherDay(calendarPopup);
        Poller.waitUntilTrue(calendarPopup.isClosed());
        Poller.waitUntil("Calendar date was not set on the input field.", calendarPicker.getDateValue(), startsWith(Integer.toString(expectedDay)));
    }

    private int selectOtherDay(CalendarPopup popup)
    {
        final int selectedDay = popup.getSelectedDay().now();
        for (int day=1; day<=31; day++)
        {
            if (popup.hasDay(day) && day != selectedDay)
            {
                popup.selectDay(day);
                return day;
            }
        }
        throw new AssertionError("Ooops, this test is not smart enough");
    }

    /**
     * Add the due date searcher (if necessary) and open it.
     *
     * @return the due date searcher.
     */
    private DateSearcher addDueDate()
    {
        BasicQueryComponent basicQuery = issuesPage.getBasicQuery();
        return basicQuery.openAddCriteriaDialog().selectAndOpenDueDate();
    }
}