package it.com.atlassian.jira.plugin.issuenav.func;

import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.jira.nimblefunctests.framework.NimbleFuncTestCase;
import com.atlassian.jira.plugin.issuenav.client.PreferredSearchLayoutClient;
import com.atlassian.jira.plugin.issuenav.util.PreferredSearchLayoutService;
import it.com.atlassian.jira.plugin.issuenav.func.util.funcobject.IssueNavigator;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.junit.Test;

import java.util.Map;

import static com.atlassian.jira.functest.framework.assertions.IssueTableAssertions.ORDER_BY_CLAUSE;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.JQL;
import static com.atlassian.jira.functest.framework.suite.Category.TIME_ZONES;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RestoreOnce ("TestJqlSearchWithTimeZones.xml")
@WebTest ({ FUNC_TEST, JQL, TIME_ZONES })
public class TestJqlSearchWithTimeZones extends NimbleFuncTestCase
{
    public static final String BERLIN_USER = "berlin";
    public static final String ADMIN_USER = "admin";

    private PreferredSearchLayoutClient preferredSearchLayoutClient;

    @Override
    public void beforeMethod()
    {
        super.beforeMethod();

        preferredSearchLayoutClient = new PreferredSearchLayoutClient(environmentData);
    }

    @Test
    public void testSearchResultsInIssueNavigatorSydneyTimeZone() throws Exception
    {
        final String jqlString = "project = Bovine";
        navigation.login(ADMIN_USER);
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
        final IssueNavigator nav = openIssueNavigator(jqlString + ORDER_BY_CLAUSE);

        assertThat(nav.getResultRow(1), rowMatches("COW-16", "17/Apr/11", "20/Apr/11", "04/Apr/11", "20/Apr/11", "13/Apr/11", "12/Apr/11 2:00 AM"));
        assertThat(nav.getResultRow(2), rowMatches("COW-15", "19/Apr/11", "18/Apr/11", "21/Apr/11", "", "", ""));
    }

    @Test
    public void testSearchResultsInIssueNavigatorBerlinTimeZone() throws Exception
    {
        final String jqlString = "project = Bovine";
        navigation.login(BERLIN_USER);
        preferredSearchLayoutClient.loginAs(BERLIN_USER);
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
        final IssueNavigator nav = openIssueNavigator(jqlString + ORDER_BY_CLAUSE);

        assertThat(nav.getResultRow(1), rowMatches("COW-16", "16/Apr/11", "19/Apr/11", "04/Apr/11", "19/Apr/11", "13/Apr/11", "11/Apr/11 6:00 PM"));
        assertThat(nav.getResultRow(2), rowMatches("COW-15", "18/Apr/11", "17/Apr/11", "21/Apr/11", "", "", ""));
    }

    @Test
    public void testSearchResultsInIssueNavigatorGMTMinus12TimeZone() throws Exception
    {
        final String jqlString = "project = Bovine";
        navigation.login("gmtminus12");
        preferredSearchLayoutClient.loginAs("gmtminus12");
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
        final IssueNavigator nav = openIssueNavigator(jqlString + ORDER_BY_CLAUSE);

        assertThat(nav.getResultRow(1), rowMatches("COW-16", "16/Apr/11", "19/Apr/11", "04/Apr/11", "19/Apr/11", "13/Apr/11", "11/Apr/11 4:00 AM"));
        assertThat(nav.getResultRow(2), rowMatches("COW-15", "18/Apr/11", "17/Apr/11", "21/Apr/11", "", "", ""));
    }

    @Test
    public void testJqlForCreatedDateField() throws Exception
    {
        final String jqlString = "project = Bovine and createdDate < \"2011-04-17\"";
        navigation.login(ADMIN_USER);
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
        final IssueNavigator adminNav = openIssueNavigator(jqlString + ORDER_BY_CLAUSE);
        assertTrue("Navigator content should be empty!", adminNav.isResultEmpty());

        navigation.login(BERLIN_USER);
        preferredSearchLayoutClient.loginAs(BERLIN_USER);
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
        final IssueNavigator berlinNav = openIssueNavigator(jqlString + ORDER_BY_CLAUSE);

        assertThat(berlinNav.getResultRow(1), rowMatches("COW-16", "16/Apr/11", "19/Apr/11", "04/Apr/11", "19/Apr/11", "13/Apr/11", "11/Apr/11 6:00 PM"));
    }

    @Test
    public void testJqlForDueDateField() throws Exception
    {
        final String jqlString = "project = Bovine and duedate = \"2011-04-04\"";

        navigation.login(ADMIN_USER);
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
        final IssueNavigator adminNav = openIssueNavigator(jqlString + ORDER_BY_CLAUSE);
        assertThat(adminNav.getResultRow(1), rowMatches("COW-16", "17/Apr/11", "20/Apr/11", "04/Apr/11", "20/Apr/11", "13/Apr/11", "12/Apr/11 2:00 AM"));

        navigation.login(BERLIN_USER);
        preferredSearchLayoutClient.loginAs(BERLIN_USER);
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
        final IssueNavigator berlinNav = openIssueNavigator(jqlString + ORDER_BY_CLAUSE);
        assertThat(berlinNav.getResultRow(1), rowMatches("COW-16", "16/Apr/11", "19/Apr/11", "04/Apr/11", "19/Apr/11", "13/Apr/11", "11/Apr/11 6:00 PM"));
    }

    @Test
    public void testJqlForUpdatedField() throws Exception
    {
        final String jqlString = "project = Bovine and updated < \"2011-04-20\"";
        navigation.login(ADMIN_USER);
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
        final IssueNavigator adminNav = openIssueNavigator(jqlString + ORDER_BY_CLAUSE);

        assertThat(adminNav.getResultRow(1), rowMatches("COW-15", "19/Apr/11", "18/Apr/11", "21/Apr/11", "", "", ""));

        navigation.login(BERLIN_USER);
        preferredSearchLayoutClient.loginAs(BERLIN_USER);
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
        final IssueNavigator berlinNav = openIssueNavigator(jqlString + ORDER_BY_CLAUSE);

        assertThat(berlinNav.getResultRow(1), rowMatches("COW-16", "16/Apr/11", "19/Apr/11", "04/Apr/11", "19/Apr/11", "13/Apr/11", "11/Apr/11 6:00 PM"));
        assertThat(berlinNav.getResultRow(2), rowMatches("COW-15", "18/Apr/11", "17/Apr/11", "21/Apr/11", "", "", ""));
    }

    @Test
    public void testJqlForResolvedField() throws Exception
    {
        final String jqlString = "project = Bovine and resolutiondate <= \"2011-04-20\"";
        navigation.login(ADMIN_USER);
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
        final IssueNavigator adminNav = openIssueNavigator(jqlString + ORDER_BY_CLAUSE);
        assertTrue("Navigator content should be empty!", adminNav.isResultEmpty());

        navigation.login(BERLIN_USER);
        preferredSearchLayoutClient.loginAs(BERLIN_USER);
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
        final IssueNavigator berlinNav = openIssueNavigator(jqlString + ORDER_BY_CLAUSE);

        assertThat(berlinNav.getResultRow(1), rowMatches("COW-16", "16/Apr/11", "19/Apr/11", "04/Apr/11", "19/Apr/11", "13/Apr/11", "11/Apr/11 6:00 PM"));
    }

    @Test
    public void testJqlDatePickerCustomField() throws Exception
    {
        final String jqlString = "\"Review date\" = \"2011-04-13\"";

        navigation.login(ADMIN_USER);
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
        final IssueNavigator adminNav = openIssueNavigator(jqlString + ORDER_BY_CLAUSE);
        assertThat(adminNav.getResultRow(1), rowMatches("COW-16", "17/Apr/11", "20/Apr/11", "04/Apr/11", "20/Apr/11", "13/Apr/11", "12/Apr/11 2:00 AM"));

        navigation.login(BERLIN_USER);
        preferredSearchLayoutClient.loginAs(BERLIN_USER);
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
        final IssueNavigator berlinNav = openIssueNavigator(jqlString + ORDER_BY_CLAUSE);

        assertThat(berlinNav.getResultRow(1), rowMatches("COW-16", "16/Apr/11", "19/Apr/11", "04/Apr/11", "19/Apr/11", "13/Apr/11", "11/Apr/11 6:00 PM"));
    }

    @Test
    public void testJqlDateTimeCustomField() throws Exception
    {
        final String jqlString = "expires < \"2011-04-12\"";

        navigation.login(ADMIN_USER);
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
        final IssueNavigator adminNav = openIssueNavigator(jqlString + ORDER_BY_CLAUSE);
        assertTrue("Navigator content should be empty!", adminNav.isResultEmpty());

        navigation.login(BERLIN_USER);
        preferredSearchLayoutClient.loginAs(BERLIN_USER);
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
        final IssueNavigator berlinNav = openIssueNavigator(jqlString + ORDER_BY_CLAUSE);

        assertThat(berlinNav.getResultRow(1), rowMatches("COW-16", "16/Apr/11", "19/Apr/11", "04/Apr/11", "19/Apr/11", "13/Apr/11", "11/Apr/11 6:00 PM"));
    }

    final Matcher<Map<String, String>> rowMatches(String issueKey, String createdDate, String updatedDate, String dueDate, String resolvedDate, String reviewDate, String expires)
    {
        //noinspection unchecked
        return CoreMatchers.allOf(hasEntry("issuekey", issueKey),
                hasEntry("created", createdDate),
                hasEntry("updated", updatedDate),
                hasEntry("duedate", dueDate),
                hasEntry("resolutiondate", resolvedDate),
                hasEntry("customfield_10010", reviewDate),
                hasEntry("customfield_10111", expires));
    }

    private IssueNavigator openIssueNavigator(final String jql)
    {
        final IssueNavigator nav = new IssueNavigator(navigation, tester);
        return nav.openSearchWithJql(jql);
    }
}
