package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.jira.plugin.issuenav.pageobjects.AdvancedQueryComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test that the quicksearch form works from kickass (it goes through a translation filter and different on-screen
 * treatment.
 *
 * @since v5.2
 */
@RestoreOnce("TestSearching.xml")
public class TestQuickSearch extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        issuesPage = logInToIssuesPageAsSysadmin();
    }

    @Test
    public void testQueriesSmartAndDumb()
    {
        assertEquals("text ~ \"dumb\"", qsToJql("dumb"));
        assertEquals("text ~ \"super dumb\"", qsToJql("super dumb"));
        assertEquals("project = FIJI AND issuetype = Bug", qsToJql("fiji bug"));

        //special case empty quicksearch
        assertEquals("", qsToJql(""));

        //backaslash escaping
        final String singleSlash = "\\a";
        final String textTildeQuotedEscapedSlash = "text ~ \"\\\\a\""; // what a car crash
        assertEquals("Expected a single slash to be escaped by a slash prefix", textTildeQuotedEscapedSlash, qsToJql(singleSlash));

        // doublequote escaping
        final String doubleQuote = "\"a\"";
        final String textTildeQuotedEscapedDoublequote = "text ~ \"\\\"a\\\"\""; // warning! java backslash escaping
        assertEquals("Expected a doublequote to be escaped by a slash prefix", textTildeQuotedEscapedDoublequote, qsToJql(doubleQuote));
    }

    private String qsToJql(String quickSearchQuery)
    {
        issuesPage.quickSearch(quickSearchQuery);
        final AdvancedQueryComponent advancedQuery = issuesPage.getAdvancedQuery();
        assertTrue(advancedQuery.jqlQueryIsValid());
        return advancedQuery.getCurrentQuery();
    }
}