package it.com.atlassian.jira.plugin.issuenav.webdriver.issueactions;

import com.atlassian.jira.functest.framework.matchers.IterableMatchers;
import com.atlassian.jira.pageobjects.components.fields.Suggestion;
import com.atlassian.jira.pageobjects.model.DefaultIssueActions;
import com.atlassian.jira.pageobjects.navigator.SelectedIssue;
import com.atlassian.jira.pageobjects.pages.viewissue.ActionTrigger;
import com.atlassian.jira.pageobjects.pages.viewissue.linkissue.LinkIssueDialog;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.query.Poller;
import it.com.atlassian.jira.plugin.issuenav.webdriver.KickassWebDriverTestCase;
import org.junit.Test;
import org.openqa.selenium.Keys;

/**
 * @since v6.1
 */
public class TestLinkIssue extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;
    private PageBinder pageBinder;
    private TraceContext traceContext;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestIssuePickerInteractions.xml");
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();
        pageBinder = product.getPageBinder();
        traceContext = pageBinder.bind(TraceContext.class);
    }
    @Test
    public void testDownArrowWithNoQueryShowsHistoryOnly()
    {
        SelectedIssue issue = issuesPage.getResultsTable().getSelectedIssue();
        issue.getActionsMenu().open().clickItem(DefaultIssueActions.LINK_ISSUE);

        LinkIssueDialog linkIssueDialog = product.getPageBinder().bind(LinkIssueDialog.class, "SUM-1");
        linkIssueDialog.issuePicker().triggerSuggestions();

        Poller.waitUntilTrue(linkIssueDialog.issuePicker().isSuggestionsPresent());
        Poller.waitUntil(linkIssueDialog.issuePicker().getHistorySearchSuggestions().getSuggestions(),
                IterableMatchers.iterableWithSize(6, Suggestion.class));

        Poller.waitUntilFalse("Current search is present when it shouldn't be", linkIssueDialog.issuePicker().hasCurrentSearchSuggestions());

        linkIssueDialog.issuePicker().clearQuery().query("h");
        Poller.waitUntilTrue(linkIssueDialog.issuePicker().isSuggestionsPresent());
        Poller.waitUntilTrue("Current search is not present when it should be", linkIssueDialog.issuePicker().hasCurrentSearchSuggestions());

        linkIssueDialog.issuePicker().query(Keys.BACK_SPACE);
        linkIssueDialog.issuePicker().triggerSuggestions().isSuggestionsPresent();
        Poller.waitUntilFalse("Current search is present when it shouldn't be", linkIssueDialog.issuePicker().hasCurrentSearchSuggestions());
        Poller.waitUntil(linkIssueDialog.issuePicker().getHistorySearchSuggestions().getSuggestions(),
                IterableMatchers.iterableWithSize(6, Suggestion.class));
    }
}
