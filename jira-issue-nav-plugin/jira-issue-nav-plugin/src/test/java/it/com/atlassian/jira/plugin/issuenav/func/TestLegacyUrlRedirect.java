package it.com.atlassian.jira.plugin.issuenav.func;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.plugin.issuenav.client.PreferredSearchLayoutClient;
import com.atlassian.jira.plugin.issuenav.util.PreferredSearchLayoutService;
import com.atlassian.jira.webtests.table.HtmlTable;
import org.junit.Test;

import java.io.IOException;

/**
 * @since v5.2
 */
@WebTest({ Category.FUNC_TEST })
public class TestLegacyUrlRedirect extends FuncTestCase
{
    @Override
    protected void setUpTest()
    {
        administration.restoreData("TestLegacyUrlRedirect.xml");

        // The issue table is only server rendered if the user's preferred layout is list view.
        PreferredSearchLayoutClient preferredSearchLayoutClient = new PreferredSearchLayoutClient(environmentData);
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
    }

    @Test
    public void testLegacyUrlWithSearcherQueryParams() throws IOException
    {
        navigation.gotoPage("/secure/IssueNavigator.jspa?reset=true&mode=hide&pid=10020&fixfor=Version%20B&resolution=-1");
        assertEquals("/issues/?jql=project%20%3D%20XSS%20AND%20resolution%20%3D%20Unresolved%20AND%20fixVersion%20%3D%20%22Version%20B%22", navigation.getCurrentPage());
        tester.assertElementPresent("issuetable");
        tester.assertElementPresent("issuerow10181"); // XSS-12 - A bug
        HtmlTable table = page.getHtmlTable("issuetable");
        assertEquals(2, table.getRowCount()); // one result row, one header row
    }
}