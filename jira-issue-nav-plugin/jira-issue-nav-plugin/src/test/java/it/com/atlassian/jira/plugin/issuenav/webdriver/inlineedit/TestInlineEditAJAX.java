package it.com.atlassian.jira.plugin.issuenav.webdriver.inlineedit;

import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.util.KickassLoginUtil;

/**
 * Extended to test inline edit for AJAX view issue
 *
 * @since v6.0
 */
public class TestInlineEditAJAX extends TestInlineEdit
{
    @Override
    protected IssueDetailComponent goToIssue(final String issueKey, String username, String password, String query)
    {
        Tracer checkpoint = traceContext.checkpoint();
        IssueDetailPage issueDetailPage = KickassLoginUtil.kickAssWith(product, username, password, IssueDetailPage.class, issueKey);
        IssueDetailComponent issueDetailComponent = issueDetailPage.details();
        traceContext.waitFor(checkpoint, "jira.issue.fields.loaded");

        return issueDetailComponent;
    }
}
