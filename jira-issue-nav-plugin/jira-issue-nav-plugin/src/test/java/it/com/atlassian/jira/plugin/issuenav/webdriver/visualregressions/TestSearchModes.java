package it.com.atlassian.jira.plugin.issuenav.webdriver.visualregressions;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.nimblefunctests.annotation.Restore;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import org.junit.Test;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.1
 */
@WebTest({ Category.WEBDRIVER_TEST, Category.VISUAL_REGRESSION })
@Restore("TestInlineEdit.xml")
public class TestSearchModes extends VisualRegressionTest {
    private final String JQL = "cf[10010] = aaa ORDER BY issuekey DESC";

    @Test
    public void testAdvancedSearch() throws InterruptedException
    {
        logInToIssuesPageAsSysadmin();
        final IssuesPage issuesPage = jqlSearch(JQL);
        issuesPage.getAdvancedQuery().ready();
        assertUIMatches("issue-nav-advanced");
    }

    @Test
    public void testSimpleSearch() throws InterruptedException
    {
        logInToIssuesPageAsSysadmin();
        final IssuesPage issuesPage = jqlSearch(JQL);
        issuesPage.getBasicQuery().ready();
        assertUIMatches("issue-nav-simple");
    }
}
