package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.nimblefunctests.annotation.Restore;
import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.components.fields.AssigneeField;
import com.atlassian.jira.pageobjects.dialogs.quickedit.WorkflowTransitionDialog;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.AuiMessage;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueErrorComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.ResultsTableComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.ShareDialog;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.ResponsiveDesign;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.SplitLayout;
import com.atlassian.pageobjects.elements.query.Poller;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.openqa.selenium.Dimension;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Tests for the split layout (including swapping to/from it).
 *
 * @since v6.0
 */
@Restore ("TestInlineEdit.xml")
public class TestSplitLayout extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;
    private SplitLayout splitLayout;
    private TraceContext traceContext;

    @Override
    public void setUpTest()
    {
        super.setUpTest();

        traceContext = product.getPageBinder().bind(TraceContext.class);
        Tracer tracer = traceContext.checkpoint();

        issuesPage = logInToIssuesPageAsSysadmin("?jql=");
        splitLayout = issuesPage.getSplitLayout(tracer);
    }

    @Test
    public void testDeletedIssues()
    {
        // Select list view, navigate to XSS-17, and delete it.
        issuesPage = issuesPage.getListLayout();
        Tracer tracer = traceContext.checkpoint();
        IssueDetailComponent issuePage = issuesPage.getResultsTable().navigateToIssueDetailPage("XSS-17");
        waitForIssue(tracer);
        issuePage.deleteIssue("jira.search.finished");

        // Switch to split view, assert that the results list renders.
        splitLayout = issuesPage.getSplitLayout();
        assertEquals("XSS-16", splitLayout.issueList().getHighlightedIssueKey().now());

        // Switching layouts after someone else deletes an issue should work.
        issuesPage = issuesPage.getListLayout();
        backdoor.project().deleteProject("XSS");
        splitLayout = issuesPage.getSplitLayout();

        assertTrue(splitLayout.issueList().issueIsInaccessible(0));
        assertEquals(10280L, splitLayout.issueList().getHighlightedIssueID());
        assertEquals(131, splitLayout.issueList().getResultsCountTotal());

        // You should be able to j/k over inaccessible issues.
        splitLayout.issueList().highlightNextIssue();
        assertEquals(10272L, splitLayout.issueList().getHighlightedIssueID());

        splitLayout.issueList().highlightPrevIssue();
        assertEquals(10280L, splitLayout.issueList().getHighlightedIssueID());

        // You should be able to click on inaccessible issues.
        splitLayout.issueList().clickIssueAndWait(10272L);
        assertEquals(10272L, splitLayout.issueList().getHighlightedIssueID());

        // Clicking an issue that was deleted by someone else should mark it as inaccessible.
        issuesPage = goToIssuesPageSplitView("?jql=");
        splitLayout = issuesPage.getSplitLayout();
        backdoor.project().deleteProject("QA");

        tracer = traceContext.checkpoint();
        splitLayout.issueList().clickIssue("QA-35");
        waitForIssue(tracer);
        assertTrue(splitLayout.issueList().issueIsInaccessible(1));

        // Deleting an issue should select the next issue.
        issuesPage = goToIssuesPageSplitView("?jql=");
        splitLayout = issuesPage.getSplitLayout();
        splitLayout.issue().deleteIssue("jira.issue.refreshed");

        assertEquals("BULK-103", splitLayout.issue().getIssueKey());
        assertEquals("Deleting an issue selects the next issue", "BULK-103",
                splitLayout.issueList().getHighlightedIssueKey().now());

        // Pressing j/k after deleting an issue should work.
        splitLayout.nextIssue();
        assertEquals("BULK-102", splitLayout.issue().getIssueKey());
        assertEquals("Pressing j/k after deleting an issue should work", "BULK-102",
                splitLayout.issueList().getHighlightedIssueKey().now());
    }

    @Test
    public void testDropdownsHideOnScroll()
    {
        issuesPage = goToIssuesPageSplitView("?jql=key = XSS-17");
        splitLayout = issuesPage.getSplitLayout();
        IssueDetailComponent issueDetailComponent = product.getPageBinder().bind(IssueDetailComponent.class, "XSS-17");
        issueDetailComponent.clickMoreActions();
        assertTrue("Expected actions menu to be open", issueDetailComponent.isMoreActionsOpen());
        issueDetailComponent.scrollTo(500, true);
        assertFalse("Expected actions menu to be closed on scroll", issueDetailComponent.isMoreActionsOpen());
    }

    @Test
    public void testDeleteLastIssue()
    {
        // Deleting the last issue should show the "no results" message.
        issuesPage = goToIssuesPageSplitView("?jql=key = XSS-16");
        issuesPage.getSplitLayout().issue().deleteIssue("jira.search.finished");
        Poller.waitUntilEquals("No issues were found to match your search", issuesPage.getEmptySearchResultMessage());
    }

    @Test
    public void testDeleteOnlyIssueOnPage()
    {
        backdoor.applicationProperties().setString("user.issues.per.page", "131");
        backdoor.usersAndGroups().addUser("chris", "chris", "Chris", "chris@example.com");
        backdoor.usersAndGroups().addUserToGroup("chris", "jira-administrators");

        issuesPage = logInToIssuesPage("chris", "chris", "?jql=");
        splitLayout = issuesPage.getSplitLayout();

        splitLayout.nextPage();
        splitLayout.issue().deleteIssue("jira.issue.refreshed");
        assertFalse("Pagination links have been removed", splitLayout.hasPagination());
        assertEquals("Selected the last issue on the previous page", "ARA-2", splitLayout.issue().getIssueKey());
        assertEquals("Highlighted the last issue on the previous page", "ARA-2",
                splitLayout.issueList().getHighlightedIssueKey().now());
    }

    /**
     * Modifying the search should update the split layout.
     */
    @Test
    public void testModifyingSearch()
    {
        assertEquals(132, splitLayout.issueList().getResultsCountTotal());
        Tracer checkpoint = traceContext.checkpoint();
        issuesPage.getBasicQuery().project().setValueAndSubmit("QA");
        splitLayout.waitForIssueAndResults(checkpoint);
        assertEquals(18, splitLayout.issueList().getResultsCountTotal());
        assertEquals("QA-36", splitLayout.issue().getIssueKey());
        assertEquals("QA-36", splitLayout.issueList().getHighlightedIssueKey().now());

        checkpoint = traceContext.checkpoint();
        issuesPage.getFilters().selectFilter(10016);
        splitLayout.waitForIssueAndResults(checkpoint);
        assertCurrentURLTimed("/browse/XSS-17?filter=10016");
        assertEquals("XSS-17", splitLayout.issue().getIssueKey());
        assertEquals("XSS-17", splitLayout.issueList().getHighlightedIssueKey().now());
    }

    @Test
    public void testRefreshShouldHighlightFirstInRow()
    {
        issuesPage.getBasicQuery().assignee().setValueAndSubmit("jira-administrators");

        assertEquals("First issue in scope should be XSS-10", "XSS-10", splitLayout.issue().getIssueKey());

        backdoor.issues().assignIssue("XSS-10", "moody");

        Tracer tracer = traceContext.checkpoint();
        splitLayout.clickRefresh();
        splitLayout.waitForIssueAndResults(tracer);

        assertCurrentURLTimed("/browse/XSS-9?jql=assignee%20in%20(membersOf(jira-administrators))");
        assertEquals("Split view should select the first issue in the list after refresh", "XSS-9", splitLayout.issue().getIssueKey());
    }

    @Test
    public void testSwitchingLayouts()
    {
        // The dark feature is enabled, so we start on split view.
        assertTrue(issuesPage.isSplitLayout());
        assertCurrentURL("/browse/XSS-17?jql=");
        assertEquals("XSS-17", splitLayout.issue().getIssueKey());
        assertEquals("XSS-17", splitLayout.issueList().getHighlightedIssueKey().now());
        assertEquals(132, splitLayout.issueList().getResultsCountTotal());

        // Change the search.
        Tracer checkpoint = traceContext.checkpoint();
        issuesPage.getBasicQuery().project().setValueAndSubmit("QA");
        splitLayout.waitForIssueAndResults(checkpoint);
        assertTrue(issuesPage.isSplitLayout());
        assertCurrentURL("/browse/QA-36?jql=project%20%3D%20QA");
        assertEquals("QA-36", splitLayout.issue().getIssueKey());
        assertEquals("QA-36", splitLayout.issueList().getHighlightedIssueKey().now());
        assertEquals(18, splitLayout.issueList().getResultsCountTotal());

        // Click the next issue
        splitLayout.clickIssue("QA-35");
        assertCurrentURL("/browse/QA-35?jql=project%20%3D%20QA");
        assertEquals("QA-35", splitLayout.issue().getIssueKey());
        assertEquals("QA-35", splitLayout.issueList().getHighlightedIssueKey().now());

        // Switch to list layout
        issuesPage.getListLayout();
        ResultsTableComponent resultsTable = issuesPage.getResultsTable();

        assertTrue(issuesPage.isListLayout());
        assertCurrentURL("/issues/?jql=project%20%3D%20QA");
        assertEquals("QA-35", resultsTable.getSelectedIssueKey());
        assertEquals(18, resultsTable.getResultsCountTotal());

        // 'j' to the next issue
        resultsTable.selectNextIssue();
        assertEquals("QA-34", resultsTable.getSelectedIssueKey());

        // Switch back to split layout
        splitLayout = issuesPage.getSplitLayout();
        assertTrue(issuesPage.isSplitLayout());
        assertCurrentURL("/browse/QA-34?jql=project%20%3D%20QA");
        assertEquals("QA-34", splitLayout.issue().getIssueKey());
        assertEquals("QA-34", splitLayout.issueList().getHighlightedIssueKey().now());
        assertEquals(18, splitLayout.issueList().getResultsCountTotal());
    }

    /**
     * Navigating directly to /issues/ when split view is the preferred layout should correctly update the url
     */
    @Test
    public void testNavigateToIssueNavWithSplitViewShowsCorrectUrl()
    {
        String filter = "?filter=-4&jql=project%20%3D%20BULK%20AND%20issuetype%20%3D%20Improvement%20ORDER%20BY%20createdDate%20DESC";

        goToIssuesPage("?jql=");
        assertCurrentURLTimed("/browse/XSS-17?jql=");
        goToIssuesPage(filter);
        assertCurrentURLTimed("/browse/BULK-2" + filter);
    }

    @Test
    public void testReplaceStateForSessionSearch()
    {
        product.gotoHomePage();
        goToIssuesPage("?jql=issuetype%20%3D%20Improvement");
        assertCurrentURLTimed("/browse/BULK-2?jql=issuetype%20%3D%20Improvement");
        issuesPage.back();
        assertCurrentURLTimed("/secure/Dashboard.jspa");
        goToIssuesPage("");
        assertCurrentURLTimed("/browse/BULK-2?jql=issuetype%20%3D%20Improvement");
        issuesPage.back();
        assertCurrentURLTimed("/secure/Dashboard.jspa");
    }

    @Test
    public void testNavigatingToIssue()
    {
        goToIssuePage("XSS-8", "");
        assertEquals("XSS-8", splitLayout.issue().getIssueKey());
        assertEquals("XSS-8", splitLayout.issueList().getHighlightedIssueKey().now());
    }

    @Test
    public void testReplaceStateWhenNavigatingIssues()
    {
        product.gotoHomePage();
        goToIssuesPage("?jql=");
        assertCurrentURLTimed("/browse/XSS-17?jql=");
        splitLayout.nextIssue();
        assertCurrentURLTimed("/browse/XSS-16?jql=");
        splitLayout.nextIssue();
        assertCurrentURLTimed("/browse/XSS-15?jql=");
        splitLayout.prevIssue();
        assertCurrentURLTimed("/browse/XSS-16?jql=");
        issuesPage.getListLayout();
        issuesPage.back();
        assertCurrentURLTimed("/secure/Dashboard.jspa");
    }

    /**
     * Test for page navigation via prev/next icon, numbered page links and direct via url with startIndex
     */
    @Test
    public void testSplitViewPageNavigation()
    {
        //Test initial page results count is correct
        Poller.waitUntilEquals("1", splitLayout.getCurrentPageIndex());
        assertCurrentURLTimed("/browse/XSS-17?jql=");

        //Test next page results count is correct and the correct issue is loaded
        splitLayout.nextPage();
        Poller.waitUntilEquals("2", splitLayout.getCurrentPageIndex());

        IssueDetailComponent issue = splitLayout.issue();
        assertEquals("Detail panel should load the first issue on page 2 upon navigate via next icon",
                "BULK-83", issue.getIssueKey());
        assertCurrentURLTimed("/browse/BULK-83?jql=");

        //Test pressing k into last page shows the correct issue
        splitLayout.prevIssue();
        issue = splitLayout.issue();
        Poller.waitUntilEquals("1", splitLayout.getCurrentPageIndex());
        assertEquals("Detail panel should load the last issue on page 1 upon navigate via k",
                "BULK-84", issue.getIssueKey());
        assertCurrentURLTimed("/browse/BULK-84?jql=");

        //Test directly pressing page link for page 3 load the correct results count and issue
        splitLayout.goToPage(3, 50);
        Poller.waitUntilEquals("3", splitLayout.getCurrentPageIndex());
        assertEquals("Detail panel should load the first issue on page 3 upon navigate via page 3 link",
                "BULK-27", issue.getIssueKey());
        assertCurrentURLTimed("/browse/BULK-27?jql=");

        //Test navigation directly via url with startIndex param show correct url
        issuesPage = goToIssuesPage("?jql=&startIndex=50");
        Poller.waitUntilEquals("2", splitLayout.getCurrentPageIndex());
        assertEquals("Detail panel should load the first issue on page 2 upon page load with startIndex param",
                "BULK-83", issue.getIssueKey());
        assertCurrentURLTimed("/browse/BULK-83?jql=");
    }

    @Test
    public void testSplitViewBrowserBackAndForwardOnPageNavigation()
    {
        issuesPage = goToIssuesPage("?filter=10016");

        splitLayout.goToPage(2, 50);
        assertCurrentURLTimed("/browse/BULK-57?filter=10016");

        issuesPage.back();
        Poller.waitUntilEquals("1", splitLayout.getCurrentPageIndex());
        assertCurrentURLTimed("/browse/XSS-17?filter=10016");

        issuesPage.forward();
        Poller.waitUntilEquals("2", splitLayout.getCurrentPageIndex());
        assertCurrentURLTimed("/browse/BULK-57?filter=10016");
    }

    @Test
    public void testSearchWithNoResultsShowTheAppropriateMessage()
    {
        issuesPage.getBasicQuery().status().setValueAndClickOutside("FFFF");
        Poller.waitUntilEquals("No issues were found to match your search", issuesPage.getEmptySearchResultMessage());
        assertFalse("Bottom pagination should disappear", splitLayout.hasPagination());

        issuesPage = issuesPage.newSearch();
        issuesPage.getSplitLayout().nextPage();
        issuesPage.getAdvancedQuery().searchAndWait("type = Bug AND type != Bug");
        assertThat("Navigator content isn't pending", issuesPage.isPending(), is(false));
        assertThat("The no results message is visible", issuesPage.emptySearchResultsMessageIsVisible(), is(true));
    }

    @Test
    public void testViewIssuePagerNeverAppearInSplitView()
    {
        waitUntilTrue(splitLayout.issue().hasNextPreviousPagerTimed());
        waitUntilFalse(splitLayout.issue().hasReturnToSearchPagerTimed());

        Tracer tracer = traceContext.checkpoint();
        JiraHeader header = product.getPageBinder().bind(JiraHeader.class);
        header.getIssuesMenu().open().getElementsForRecentIssues().get(1).click();
        waitForIssue(tracer);

        assertCurrentURLTimed("/browse/ARA-1");
        waitUntilFalse(splitLayout.issue().hasNextPreviousPagerTimed());
    }

    @Test
    public void testNonIssueTableLinkShowFullPageViewIssue()
    {
        final Matcher<String> standaloneViewIssueLoaded = containsString("ka ajax-issue-search-and-view page-type-navigator navigator-issue-only");

        Tracer tracer = traceContext.checkpoint();
        JiraHeader header = product.getPageBinder().bind(JiraHeader.class);
        header.getIssuesMenu().open().getElementsForRecentIssues().get(3).click();
        waitForIssue(tracer);
        IssueDetailComponent issue = splitLayout.issue();

        assertCurrentURLTimed("/browse/BULK-46");
        Poller.waitUntil(
                "Clicking on recent issue should open issue in standalone VI",
                issue.getBodyClasses(),
                standaloneViewIssueLoaded);

        issuesPage.back();
        assertCurrentURLTimed("/browse/XSS-17?jql=");

        issue = splitLayout.issue();
        issue.clickIssueLink("BULK-4");
        assertCurrentURLTimed("/browse/BULK-4");
        Poller.waitUntil(
                "Clicking on linked issue should open issue in standalone VI",
                issue.getBodyClasses(),
                standaloneViewIssueLoaded);

        issue.returnToSearchUsingKeyboardShortcut();
        assertCurrentURLTimed("/browse/XSS-17?jql=");

        issuesPage.getAdvancedQuery().searchAndWait("issuekey = BULK-101");
        issue = splitLayout.issue().clickSubtask("BULK-102");
        assertCurrentURLTimed("/browse/BULK-102");
        Poller.waitUntil(
                "Clicking on linked issue should open issue in standalone VI",
                issue.getBodyClasses(),
                standaloneViewIssueLoaded);

        issue.returnToSearchUsingKeyboardShortcut();
        assertCurrentURLTimed("/browse/XSS-17?jql=");
    }

    /**
     * JRADEV-19125 Requesting an issue that isn't in the search results should show it in the details panel.
     */
    @Test
    public void testIssueNotInSearchResults()
    {
        Tracer tracer = traceContext.checkpoint();
        issuesPage = goToIssuesPageSplitView("XSS-17", "?jql=project != XSS");
        splitLayout = issuesPage.getSplitLayout(tracer);

        assertEquals("XSS-17", splitLayout.issue().getIssueKey());
        assertEquals(115, splitLayout.issueList().getResultsCountTotal());
        assertFalse(splitLayout.issueList().hasHighlightedIssue());

        // Navigating to a non-existent issue should show an error message in the details panel.
        tracer = traceContext.checkpoint();
        issuesPage = goToIssuesPageSplitView("JRA-1", "?jql=");
        splitLayout = issuesPage.getSplitLayout();
        splitLayout.waitForIssueAndResults(tracer);

        AuiMessage errorMessage = splitLayout.issueError().getErrorMessage();
        assertTrue("Error is displayed in issue details panel", errorMessage.isError());
        assertTrue(errorMessage.getText().contains("The issue you are trying to view does not exist"));

        // Navigating to a non-existent issue without a search context should show a full-screen error message.
        IssueErrorComponent issueError = goToInvalidIssuePage("JRA-1");
        assertTrue("A full-screen error message is displayed", issueError.isFullScreen());
        assertTrue(issueError.getErrorMessage().getText().contains("The issue you are trying to view does not exist"));
    }

    @Test
    public void testReturnToSearch() throws Exception
    {
        // The return to search shortcut ('u') shouldn't do anything on split view.
        issuesPage.getKeyboardShortcutTarget().type("u");
        Thread.sleep(2000);
        assertCurrentURL("/browse/XSS-17?jql=");
        assertEquals(splitLayout.issue().getIssueKey(), "XSS-17");

        // Returning to search from a standalone issue.
        IssueDetailComponent issue = goToIssuePage("XSS-8", null);
        issue.returnToSearchUsingKeyboardShortcut();
        issue = splitLayout.issue();

        assertCurrentURLTimed("/browse/XSS-17?jql=");
        waitUntilEquals("1", splitLayout.getCurrentPageIndex());
        assertEquals("XSS-17", issue.getIssueKey());
    }

    @Test
    public void testSplitViewForAnonymousUser()
    {
        product.logout();

        //Go to /issues, where the default layout will be list view, change to split view
        String urlToGo = product.getProductInstance().getBaseUrl() + "/issues";
        product.getTester().getDriver().navigate().to(urlToGo);
        issuesPage = product.getPageBinder().bind(IssuesPage.class);
        issuesPage.getSplitLayout();

        //Refresh the page
        goToIssuesPage("?jql=");

        IssueDetailComponent issue = splitLayout.issue();
        assertEquals("XSS-17", issue.getIssueKey());
    }

    @Test
    public void testLoginRedirectURLForAnonymousUser()
    {
        product.logout();
        issuesPage = goToIssuesPage();
        issuesPage.newSearch();
        splitLayout = issuesPage.getSplitLayout();
        assertEquals(environmentData.getBaseUrl() + "/login.jsp?os_destination=%2Fbrowse%2FXSS-17%3Fjql%3D", issuesPage.getLoginLinkUrl());

        splitLayout.clickNextIssue();
        assertEquals(environmentData.getBaseUrl() + "/login.jsp?os_destination=%2Fbrowse%2FXSS-16%3Fjql%3D", issuesPage.getLoginLinkUrl());

        issuesPage.getSplitLayout().nextPage();
        assertEquals(environmentData.getBaseUrl() + "/login.jsp?os_destination=%2Fbrowse%2FBULK-64%3Fjql%3D", issuesPage.getLoginLinkUrl());
    }

    @Test
    public void testNoLoginButtonForAnonymousUserInDetailView()
    {
        product.logout();
        issuesPage = goToIssuesPage();
        issuesPage.newSearch();
        splitLayout = issuesPage.getSplitLayout();

        splitLayout.clickIssue("XSS-16");
        Poller.waitUntilFalse(splitLayout.issue().hasLoginButton());

        splitLayout.clickIssue("XSS-15");
        Poller.waitUntilFalse(splitLayout.issue().hasLoginButton());
    }

    @Test
    public void testShareDialogWithKeyboardOnListView()
    {
        IssueDetailComponent issue = splitLayout.issue();

        //SV
        String permalink = issue.getShareDialogViaShortcut().getSearchPermalinkValue().now();
        assertThat("The dialog is the Share Issue dialog", permalink, containsString("/browse/XSS-17"));

        //LV
        IssuesPage listView = splitLayout.switchLayouts().waitForResultsTable();
        permalink = listView.getShareDialogViaShortcut().getSearchPermalinkValue().now();
        assertThat("The dialog is the Share Issue dialog", permalink, containsString("/issues"));
    }

    @Test
    public void testShareDialogNoPersistOnIssueChange()
    {
        IssueDetailComponent issue = splitLayout.issue();
        ShareDialog issueShareDialog = issue.getShareDialog().open();
        issueShareDialog.addNote("note");
        issueShareDialog.closeBySelectingOutside();

        splitLayout.nextIssue();
        issue = splitLayout.issue();
        issueShareDialog = issue.getShareDialog();
        assertEquals("Should have not persisted note on issue change", "", issueShareDialog.open().getNote());
    }

    @Test
    public void testDisablingOutgoingMailShowsNoForm()
    {
        //Sharing is enabled
        ShareDialog filterShareDialog = issuesPage.getShareDialog().open();
        waitUntilTrue("Search permalink is available", filterShareDialog.hasSearchPermalink());
        waitUntilFalse("Share form is available", filterShareDialog.hasDisabledOutgoingMailMessage());

        //Restoring a blank instance = sharing is not enabled by default
        administration.restoreBlankInstance();
        issuesPage = goToIssuesPage("?jql=");
        filterShareDialog = issuesPage.getShareDialog().open();
        waitUntilTrue("Search permalink is available", filterShareDialog.hasSearchPermalink());
        waitUntilTrue("A message for disabled outgoing mail should be showed instead", filterShareDialog.hasDisabledOutgoingMailMessage());
    }

    @Test
    public void testDisablingOutgoingMailShareKeyboardShortcutWorks()
    {
        ShareDialog shareDialog = issuesPage.getShareDialog();
        shareDialog.openViaKeyboardShortcut();
        assertTrue(shareDialog.isOpen());
    }

    @Test
    public void testSharingIssueAndSearch()
    {
        IssueDetailComponent issue = splitLayout.issue();

        //Sharing a standalone VI
        issue.clickIssueLink("BULK-4");
        Tracer tracer = traceContext.checkpoint();
        ShareDialog dialog = issue.getShareDialog();
        dialog.open().addRecipient("a@a.com").addNote("standalone vi").submit().waitForSubmit(tracer);
        checkShareRequestData(dialog.getShareRequestData(tracer), "issue/BULK-4", null, "a@a.com", "standalone vi", null);

        tracer = traceContext.checkpoint();
        issue.returnToSearchUsingKeyboardShortcut();
        splitLayout.waitForIssueAndResults(tracer);

        //Sharing a jql in SV
        tracer = traceContext.checkpoint();
        dialog = splitLayout.getShareDialog();
        dialog.open().addRecipient("admin").submit().waitForSubmit(tracer);
        checkShareRequestData(dialog.getShareRequestData(tracer), "search", "admin", null, null, "");

        //Sharing an issue in SV
        tracer = traceContext.checkpoint();
        dialog = issue.getShareDialog();
        dialog.open().addRecipient("moody").addNote("whaaaaaaa").submit().waitForSubmit(tracer);
        checkShareRequestData(dialog.getShareRequestData(tracer), "issue/XSS-17", "moody", null, "whaaaaaaa", null);

        issuesPage.getListLayout();
        assertCurrentURLTimed("/issues/?jql=");

        //Sharing should still work after switching layout in issue nav
        tracer = traceContext.checkpoint();
        dialog = splitLayout.getShareDialog();
        dialog.open().addRecipient("b@b.com").addNote("list view issue nav").submit().waitForSubmit(tracer);
        checkShareRequestData(dialog.getShareRequestData(tracer), "search", null, "b@b.com", "list view issue nav", "");

        //Same for VI
        tracer = traceContext.checkpoint();
        issuesPage.getResultsTable().next().openSelectedIssue();
        dialog = issue.getShareDialog();
        dialog.open().addRecipient("admin").addNote("list view view issue").submit().waitForSubmit(tracer);
        checkShareRequestData(dialog.getShareRequestData(tracer), "issue/XSS-16", "admin", null, "list view view issue", null);

        //Sharing a filter
        issue.returnToSearch();
        tracer = traceContext.checkpoint();
        issuesPage.getFilters().selectFilter(10016);
        issuesPage.getSplitLayout();
        dialog = splitLayout.getShareDialog();
        dialog.open().addRecipient("admin").submit().waitForSubmit(tracer);
        checkShareRequestData(dialog.getShareRequestData(tracer), "filter/10016", "admin", null, null, null);

        splitLayout.issue().deleteIssue("jira.issue.refreshed");
        tracer = traceContext.checkpoint();
        dialog = issue.getShareDialog();
        dialog.open().addRecipient("veenu").addNote("Sharing issue after delete").submit().waitForSubmit(tracer);
        checkShareRequestData(dialog.getShareRequestData(tracer), "issue/XSS-16", "veenu", null, "Sharing issue after delete", null);
    }

    @Test
    public void testSearchPermalink()
    {
        String baseUrl = "http://HELLO-WORLD/jira";
        product.backdoor().applicationProperties().setString(APKeys.JIRA_BASEURL, baseUrl);
        product.getTester().getDriver().navigate().refresh();

        IssueDetailComponent issue = splitLayout.issue();

        //Standalone VI
        issue.clickIssueLink("BULK-4");
        waitUntil(issue.getShareDialog().getSearchPermalinkValue(), containsString(baseUrl + "/browse/BULK-4"));

        Tracer tracer = traceContext.checkpoint();
        issue.returnToSearchUsingKeyboardShortcut();
        splitLayout.waitForIssueAndResults(tracer);

        //SV
        waitUntil(splitLayout.getShareDialog().getSearchPermalinkValue(), containsString(baseUrl + "/issues/?jql="));

        issuesPage.getListLayout();
        assertCurrentURLTimed("/issues/?jql=");

        //LV
        waitUntil(splitLayout.getShareDialog().getSearchPermalinkValue(), containsString(baseUrl + "/issues/?jql="));

        //Fullscreen VI
        issuesPage.getResultsTable().next().openSelectedIssue();
        waitUntil(issue.getShareDialog().getSearchPermalinkValue(), containsString(baseUrl + "/browse/XSS-16"));

        //LV using a filter
        issue.returnToSearch();
        issuesPage = issuesPage.getFilters().selectFilter(10016);
        waitUntil(splitLayout.getShareDialog().getSearchPermalinkValue(), containsString(baseUrl + "/issues/?filter=10016"));

        //SV using a filter after deleting an issue
        issuesPage.getSplitLayout();
        splitLayout.issue().deleteIssue("jira.issue.refreshed");
        waitUntil(splitLayout.getShareDialog().getSearchPermalinkValue(), containsString(baseUrl + "/issues/?filter=10016"));
    }

    @Test
    public void testLoadIssueUpdateIssueTableRow()
    {
        backdoor.issues().setSummary("XSS-16", "Pikachu! I CHOOOOOOOOOOSE YOUUUUUUU");

        Tracer tracer = traceContext.checkpoint();
        splitLayout.clickIssue("XSS-16");
        issuesPage.waitForStableUpdate(tracer);

        assertEquals("Loading an issue should update the issue row in issue nav",
                "Pikachu! I CHOOOOOOOOOOSE YOUUUUUUU",
                splitLayout.getSelectedIssue().getSummary());
    }

    @Test
    public void testEndOfStableSearchMessage()
    {
        backdoor.applicationProperties().setString(APKeys.JIRA_STABLE_SEARCH_MAX_RESULTS, "10");

        issuesPage = goToIssuesPage("?jql=");
        assertThat("The end of stable search message should be visible", issuesPage.endOfStableSearchMessageIsVisible(), is(true));
    }

    /**
     * JRADEV-19682 Discarding changes to a filter should reset the selected issue.
     */
    @Test
    public void testDiscardFilterChanges()
    {
        issuesPage.getFilters().selectFavourite("Bugs");
        issuesPage.getAdvancedQuery().searchAndWait("key = XSS-15");
        issuesPage.discardFilterChanges();

        assertThat("The selected issue is reset", issuesPage.getSplitLayout().issue().getIssueKey(), is("XSS-17"));
    }

    /**
     * JRADEV-19959 Browsing to an issue in a search context should work as an anonymous user.
     */
    @Test
    public void testAnonymousPageLoad()
    {
        product.logout();
        goToIssuesPage().getSplitLayout();

        issuesPage = goToIssuesPageSplitView("XSS-17", "?jql=");
        assertThat("The requested issue is visible", issuesPage.getSplitLayout().issue().getIssueKey(), is("XSS-17"));
    }

    @Test
    public void testMissingIssueShowsNoReturnToSearch()
    {
        goToInvalidIssuePage("XSS-1722", "");
        IssueErrorComponent issueError = issuesPage.getSplitLayout().issueError();
        Poller.waitUntilFalse(issueError.hasReturnToSearchLink());
    }

    @Test
    public void testNoResultsOnPageLoad()
    {
        // Server-rendered detail view
        issuesPage = goToIssuesPage("?jql=");
        issuesPage.getBasicQuery().searchAndWait("Oh, you'd better believe that's a paddlin'.");

        Tracer tracer = traceContext.checkpoint();
        issuesPage.getBasicQuery().searchAndWait("");
        waitForIssue(tracer);

        assertThat("The issue was rendered", splitLayout.issue().getIssueKey(), is("XSS-17"));
        assertThat("The results were rendered", splitLayout.issueList().getResultsCountTotal(), is(132));

        // Server-rendered detail view with no results
        issuesPage = goToIssuesPage("?jql=type = Bug AND type != Bug");
        assertThat("Navigator content isn't pending", issuesPage.isPending(), is(false));
        assertThat("The no results message is visible", issuesPage.emptySearchResultsMessageIsVisible(), is(true));

        issuesPage.newSearch();
        splitLayout = issuesPage.getSplitLayout();
        assertThat("The issue list is visible", splitLayout.issueList().hasHighlightedIssue(), is(true));
    }

    @Test
    public void testExpandingAndContractingSidebar()
    {
        product.getTester().getDriver().manage().window().setSize(new Dimension(1500, 800));
        issuesPage = goToIssuesPageSplitView("XSS-17", "?jql=");
        SplitLayout splitLayout = issuesPage.getSplitLayout();
        splitLayout.issueList().expandBy(200);
        assertThat("Layout should have changed skinny", splitLayout.getResponseDesign(), is(ResponsiveDesign.SKINNY));
        splitLayout.issueList().expandBy(300);
        assertThat("Layout should have changed to very skinny", splitLayout.getResponseDesign(), is(ResponsiveDesign.VERY_SKINNY));
        splitLayout.issueList().contactBy(300);
        assertThat("Layout should have changed skinny", splitLayout.getResponseDesign(), is(ResponsiveDesign.SKINNY));
        splitLayout.issueList().contactBy(200);
        assertThat("Layout should have changed normal", splitLayout.getResponseDesign(), is(ResponsiveDesign.NORMAL));
    }

    @Test
    public void testSelectedIssueIsPreserved()
    {
        //Select an issue
        String issueKey = "QA-19";
        goToIssuesPageSplitView(issueKey, "?jql=");

        //Refresh the page
        product.getTester().getDriver().navigate().refresh();
        assertThat("Selected issue is preserved on page refresh", issuesPage.getSplitLayout().issue().getIssueKey(), is(issueKey));
        assertThat("Selected issue is visible", issuesPage.getSplitLayout().getSelectedIssue().isInView(), is(true));

        //Switch to list layout
        IssuesPage listLayout = issuesPage.getListLayout().waitForResultsTable();
        assertThat("Selected issue is preserved when switching to List view", listLayout.getResultsTable().getSelectedIssueKey(), is(issueKey));
        assertThat("Selected issue is visible", listLayout.getResultsTable().getSelectedIssue().isInView(), is(true));

        //And switch back to split layout
        SplitLayout splitLayout = issuesPage.getSplitLayout();
        assertThat("Selected issue is preserved when switching to Split view", splitLayout.issue().getIssueKey(), is(issueKey));
        assertThat("Selected issue is visible", splitLayout.getSelectedIssue().isInView(), is(true));
    }

    /**
     * JRADEV-20364
     */
    @Test
    public void testServerRenderedSplitViewIssueClickLoadsThatIssue()
    {
        issuesPage = goToIssuesPageSplitView("BULK-83", "?jql=");
        issuesPage.getSplitLayout().clickIssue("BULK-81");
        assertEquals("BULK-81", issuesPage.getSplitLayout().getSelectedIssue().getIssueKey());
    }

    @Test
    public void testShortcutForSwitchLayouts()
    {
        //Select an issue
        String issueKey = "QA-19";
        SplitLayout splitView = goToIssuesPageSplitView(issueKey, "?jql=").getSplitLayout();

        IssuesPage listView = splitView.switchLayouts().waitForResultsTable();
        assertThat("Selected issue is visible", listView.getResultsTable().getSelectedIssue().isInView(), is(true));

        Tracer checkpoint = traceContext.checkpoint();
        SplitLayout secondSplitView = listView.switchLayouts().waitForIssueAndResults(checkpoint);
        assertThat("Selected issue is visible", secondSplitView.getSelectedIssue().isInView(), is(true));
    }

    /**
     * JRADEV-20650 Returning from a standalone issue should execute a blank search and update the interface.
     */
    @Test
    public void testReturnToSearchFromStandaloneIssue()
    {
        issuesPage.getAdvancedQuery().searchAndWait("key = BULK-11");

        Tracer tracer = traceContext.checkpoint();
        IssueDetailComponent issue = splitLayout.issue().clickIssueLink("BULK-1");
        waitForIssue(tracer);

        issuesPage = issue.returnToSearchUsingKeyboardShortcut();
        assertThat("Results were updated", issuesPage.getSplitLayout().issueList().getHighlightedIssueKey().now(), is("XSS-17"));
    }

    @Test
    public void testOpeningAnIssueDoesNotUpdateCurrentProject()
    {
        // JRA-36659: viewing issues in split view should not affect the current project

        JiraHeader jiraHeader = product.getPageBinder().bind(JiraHeader.class);
        jiraHeader.getProjectsMenu().open();
        String initialProjectText = jiraHeader.getProjectsMenu().getCurrentProject().getText();

        issuesPage = goToIssuesPageSplitView("BLUK-6", "?jql=project in (BULK, BLUK) ORDER BY key ASC");

        jiraHeader = product.getPageBinder().bind(JiraHeader.class);
        jiraHeader.getProjectsMenu().open();

        assertThat("The Current Project is " + initialProjectText,
                jiraHeader.getProjectsMenu().getCurrentProject().getText(), CoreMatchers.is(initialProjectText));

        issuesPage.getSplitLayout().nextIssue();

        jiraHeader.getProjectsMenu().open();
        assertThat("The Current Project is " + initialProjectText,
                jiraHeader.getProjectsMenu().getCurrentProject().getText(), CoreMatchers.is(initialProjectText));

    }

    /**
     * See JRADEV-23569. On Split view, when transitioning from one status to another on an issue in Project A, when on
     * an issue in Project B and attempting to fill out the Assignee field, problems used to occur because there is data
     * carryover from the original assignee field.
     */
    @Test
    public void testAssigneeFieldOnTransitionScreenDoesNotInterfereWithAssigneeFieldOnOtherProjectsWithoutThatTransition()
    {

        issuesPage = goToIssuesPageSplitView("QA-36", "?jql=key = XSS-17 or key = QA-36");
        IssueDetailComponent issue = splitLayout.issue();

        final WorkflowTransitionDialog workflowTransitionDialog = issue.openWorkflowTransitionDialog(711L, "1");
        final AssigneeField assigneeField = product.getPageBinder().bind(AssigneeField.class);
        assigneeField.setAssignee("Fred and George");
        workflowTransitionDialog.submitWorkflowTransition();

        Tracer tracer = traceContext.checkpoint();
        splitLayout.clickIssue("XSS-17");
        waitForIssue(tracer);

        issue = splitLayout.issue();
        issue.assignee().editSaveWait("Fred and George");
        assertThat(issue.assignee().getValue(), equalTo("Fred and George"));

    }

    protected void waitForIssue(final Tracer tracer)
    {
        traceContext.waitFor(tracer, "jira.issue.refreshed");
    }
}