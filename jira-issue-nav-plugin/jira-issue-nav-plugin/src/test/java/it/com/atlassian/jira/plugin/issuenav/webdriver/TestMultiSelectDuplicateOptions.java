package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.MultiSelectClauseDialog;
import com.atlassian.jira.plugin.issuenav.pageobjects.PrimeSelectCriteria;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * JRADEV-21437
 * Test customfields with Mutli Select Search Renderer remove any option that has duplicated labels
 *
 * @since v6.1
 */
@RestoreOnce("TestMultiSelectDuplicateOptions.xml")
public class TestMultiSelectDuplicateOptions extends KickassWebDriverTestCase
{
    protected IssuesPage issuesPage;
    protected TraceContext traceContext;

    @Override
    protected void setUpTest()
    {
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();
        traceContext = product.getPageBinder().bind(TraceContext.class);
    }

    @Test
    public void testMultiSelectDuplicateOptions() throws Exception
    {
        /**
         * Data is loaded with the following custom fields:
         * 1. Select list with contexts:
         *    - All: Option 1, Option 2, Option 3
         *    - Project monkey: Option 2, Option 3
         *    - Project homosapien: Option 1, Option 3, Option 4
         * 2. Radio list with the same contexts as above
         */
        //Stateless
        _testRemoveAllDuplicateLabelsFromValidOptions();
        _testTheAboveShouldAlsoWorkForRadioList();
        _testRemoveAllDuplicateLabelsFromInvalidOptions();
        _testOnlyRemoveDuplicateOptionsWhenValidOptionsAreSelectedToo();

        //Stateful
    }

    private void _testRemoveAllDuplicateLabelsFromValidOptions() throws Exception
    {
        String jql = "\"The Select List\" in (\"Option 1\", \"Option 2\", \"Option 3\")";

        goToIssuesPage("?jql=" + jql);

        PrimeSelectCriteria searcher = getMultiSelect("The Select List");
        MultiSelectClauseDialog selectList = searcher.open();

        assertEquals(3, selectList.getAllOptionsCount());
        assertEquals(3, selectList.getSelectedOptions().size());
        assertEquals(0, selectList.getInvalidOptions().size());
        assertEquals("Option 1, Option 2, Option 3", searcher.getSelectedValue());
    }

    private void _testRemoveAllDuplicateLabelsFromInvalidOptions() throws Exception
    {
        String jql = "project = HSP AND \"The Select List\" = \"Option 2\"";

        goToIssuesPage("?jql=" + jql);

        PrimeSelectCriteria searcher = getMultiSelect("The Select List");
        MultiSelectClauseDialog selectList = searcher.open();

        assertEquals(4, selectList.getAllOptionsCount());
        assertEquals(1, selectList.getSelectedOptions().size());
        assertEquals(1, selectList.getInvalidOptions().size());
        assertEquals("Option 2", searcher.getSelectedValue());
    }

    private void _testTheAboveShouldAlsoWorkForRadioList()
    {
        String jql = "\"The Radio List\" in (\"Radio 1\", \"Radio 2\", \"Radio 3\")";

        goToIssuesPage("?jql=" + jql);

        PrimeSelectCriteria searcher = getMultiSelect("The Radio List");
        MultiSelectClauseDialog selectList = searcher.open();

        assertEquals(3, selectList.getAllOptionsCount());
        assertEquals(3, selectList.getSelectedOptions().size());
        assertEquals(0, selectList.getInvalidOptions().size());
        assertEquals("Radio 1, Radio 2, Radio 3", searcher.getSelectedValue());
    }

    private void _testOnlyRemoveDuplicateOptionsWhenValidOptionsAreSelectedToo()
    {
        String jql = "\"The Select List\" = \"Option 2\"";
        goToIssuesPage("?jql=" + jql);

        issuesPage.getBasicQuery().project().setValueAndSubmit("monkey");

        PrimeSelectCriteria searcher = getMultiSelect("The Select List");
        MultiSelectClauseDialog selectList = searcher.open();

        assertEquals(3, selectList.getAllOptionsCount());
        assertEquals(1, selectList.getSelectedOptions().size());
        assertEquals(1, selectList.getInvalidOptions().size());
        assertEquals("Option 2", searcher.getSelectedValue());

        goToIssuesPage("?jql=" + issuesPage.getAdvancedQuery().getCurrentQuery());
        searcher = getMultiSelect("The Select List");
        selectList = searcher.open();

        //Server loading with the same JQL will automatically infer "Option 2" from monkey context
        //Thus the invalid selection is removed
        assertEquals(2, selectList.getAllOptionsCount());
        assertEquals(1, selectList.getSelectedOptions().size());
        assertEquals(0, selectList.getInvalidOptions().size());
        assertEquals("Option 2", searcher.getSelectedValue());
    }

    private PrimeSelectCriteria getMultiSelect(String fieldName)
    {
        String id = "customfield_";
        String name = fieldName.toLowerCase();
        if (name.contains("select"))
        {
            id += "10000";
        }
        else if (name.contains("radio"))
        {
            id += "10001";
        }

        return issuesPage.getBasicQuery().getCriteria(id, PrimeSelectCriteria.class, "", "searcher-" + id, "");
    }
}
