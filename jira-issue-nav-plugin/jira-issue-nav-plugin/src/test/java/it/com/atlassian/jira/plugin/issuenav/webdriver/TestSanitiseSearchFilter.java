package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @since v6.1
 */
public class TestSanitiseSearchFilter extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestSanitiseSearchFilter.xml");
        issuesPage = logInToIssuesPage("fred", "fred").getListLayout();
    }

    @Test
    public void testSanitiseIssueClause() throws Exception
    {
        //Fred Doesn't have the Browse Project Permission for ABC-4
        runFilterWithIdAndVerifySanitisedJQL(10030, "Issue = 10015", "A value with ID '10015' does not exist for the field 'Issue'.");
        runFilterWithIdAndVerifySanitisedJQL(10031, "Issue != 10015", "A value with ID '10015' does not exist for the field 'Issue'.");
        runFilterWithIdAndVerifySanitisedJQL(10032, "Issue in (10015, ABC-3)", "A value with ID '10015' does not exist for the field 'Issue'.");
        runFilterWithIdAndVerifySanitisedJQL(10033, "Issue not in (10015, ABC-3)", "A value with ID '10015' does not exist for the field 'Issue'.");
        runFilterWithIdAndVerifySanitisedJQL(10034, "Issue <= 10015", "A value with ID '10015' does not exist for the field 'Issue'.");
        runFilterWithIdAndVerifySanitisedJQL(10035, "Issue < 10015", "A value with ID '10015' does not exist for the field 'Issue'.");
        runFilterWithIdAndVerifySanitisedJQL(10039, "Issue > 10015", "A value with ID '10015' does not exist for the field 'Issue'.");

        // valid
        runFilterWithIdAndVerifyJQL(10036, "Issue < ABC-3");
        assertIssuesInTable("ABC-1", "ABC-2");
        runFilterWithIdAndVerifyJQL(10037, "Issue >= ABC-1");
        assertIssuesInTable("ABC-1", "ABC-2", "ABC-3");
        runFilterWithIdAndVerifyJQL(10038, "Issue > ABC-1");
        assertIssuesInTable("ABC-2", "ABC-3");
    }

    @Test
    public void testSanitiseProjectPickerClause() throws Exception
    {
        runFilterWithIdAndVerifySanitisedJQL(10050, "ProjectPicker = 10000", "A value with ID '10000' does not exist for the field 'ProjectPicker'.");
        runFilterWithIdAndVerifySanitisedJQL(10051, "ProjectPicker != 10000", "A value with ID '10000' does not exist for the field 'ProjectPicker'.");
        runFilterWithIdAndVerifySanitisedJQL(10052, "ProjectPicker in (10000, Monkey)", "A value with ID '10000' does not exist for the field 'ProjectPicker'.");
        runFilterWithIdAndVerifySanitisedJQL(10053, "ProjectPicker not in (10000, Monkey)", "A value with ID '10000' does not exist for the field 'ProjectPicker'.");
    }

    @Test
    public void testSanitiseProjectClause() throws Exception
    {
        runFilterWithIdAndVerifySanitisedJQL(10044, "Project = 10000", "A value with ID '10000' does not exist for the field 'Project'.");
        runFilterWithIdAndVerifySanitisedJQL(10045, "Project != 10000", "A value with ID '10000' does not exist for the field 'Project'.");
        runFilterWithIdAndVerifySanitisedJQL(10046, "Project in (10000, Monkey)", "ORDER BY type ASC", "A value with ID '10000' does not exist for the field 'Project'.");
        runFilterWithIdAndVerifySanitisedJQL(10047, "Project not in (10000, Monkey)", "A value with ID '10000' does not exist for the field 'Project'.");
    }

    @Test
    public void testSanitiseParentClause() throws Exception
    {
//        Fred Doesn't have the Browse Project Permission for ABC-4 and Project Homosapien
        runFilterWithIdAndVerifyJQL(10040, "Parent = MKY-1");
        assertIssuesInTable("MKY-2");
        runFilterWithIdAndVerifyJQL(10041, "Parent != MKY-1");
        assertIssuesInTable("ABC-1", "ABC-2", "ABC-3", "MKY-1");

        runFilterWithIdAndVerifySanitisedJQL(10042, "Parent in (MKY-1, 10000)", "A value with ID '10000' does not exist for the field 'Parent'.");
        runFilterWithIdAndVerifySanitisedJQL(10043, "Parent not in (MKY-1, 10000)", "A value with ID '10000' does not exist for the field 'Parent'.");
    }

    private void assertIssuesInTable(String... issues)
    {
        assertIssuesInTable(issuesPage, issues);
    }

    private void runFilterWithIdAndVerifySanitisedJQL(int filterId, String jqlString, String orderBy, String error)
    {
        issuesPage.getFilters().selectFilter(filterId);
        assertEquals(jqlString + " " + orderBy, issuesPage.getAdvancedQuery().getCurrentQuery());
        issuesPage.getAdvancedQuery().submit();
        assertEquals(error, issuesPage.getJQLErrors());
    }

    private void runFilterWithIdAndVerifySanitisedJQL(int filterId, String jqlString, String error)
    {
        runFilterWithIdAndVerifySanitisedJQL(filterId, jqlString, "ORDER BY key ASC", error);
    }

    private void runFilterWithIdAndVerifyJQL(int filter, String jql)
    {
        String orderBy = "ORDER BY key ASC";
        issuesPage.getFilters().selectFilter(filter);
        assertEquals(jql + " " + orderBy, issuesPage.getAdvancedQuery().getCurrentQuery());
    }
}
