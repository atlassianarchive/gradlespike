package it.com.atlassian.jira.plugin.issuenav.webdriver.issueactions;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.pageobjects.PageBinder;
import it.com.atlassian.jira.plugin.issuenav.webdriver.KickassWebDriverTestCase;

@WebTest ({ Category.WEBDRIVER_TEST, Category.ISSUES })
public class TestEditComment extends KickassWebDriverTestCase
{

    private IssuesPage issuesPage;
    private PageBinder pageBinder;
    private TraceContext traceContext;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestIssueActions.xml");
        issuesPage = logInToIssuesPageAsSysadmin("?jql=");
        pageBinder = product.getPageBinder();
        traceContext = pageBinder.bind(TraceContext.class);
    }

}
