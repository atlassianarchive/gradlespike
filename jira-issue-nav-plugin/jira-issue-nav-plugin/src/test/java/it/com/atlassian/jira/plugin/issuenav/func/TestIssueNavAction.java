package it.com.atlassian.jira.plugin.issuenav.func;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.plugin.issuenav.client.PreferredSearchLayoutClient;
import com.atlassian.jira.plugin.issuenav.util.PreferredSearchLayoutService;
import com.atlassian.jira.webtests.table.HtmlTable;
import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.WebResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Test;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import java.io.IOException;

/**
 * Tests the KickAss Issue Nav Action.
 *
 * This class is a good candidate for the "RestoreOnce" annotation.
 *
 * @since v5.2
 */
@WebTest({ Category.FUNC_TEST })
public class TestIssueNavAction extends FuncTestCase
{
    @Override
    protected void setUpTest()
    {
        administration.restoreData("TestNavigation.xml");

        // The issue table is only server rendered if the user's preferred layout is list view.
        PreferredSearchLayoutClient preferredSearchLayoutClient = new PreferredSearchLayoutClient(environmentData);
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
    }

    /**
     * Tests that "/issues" is redirected to "/issues/" via an HTTP 301 (moved permanently)
     */
    @Test
    public void testIssues301Redirect() throws IOException
    {
        HttpGet httpGet = new HttpGet(environmentData.getBaseUrl() + "/issues");
        HttpClient httpClient = new DefaultHttpClient();
        httpClient.getParams().setBooleanParameter(ClientPNames.HANDLE_REDIRECTS, false);
        HttpResponse response = httpClient.execute(httpGet);

        assertEquals(301, response.getStatusLine().getStatusCode());
        assertEquals("issues/", response.getHeaders("Location")[0].getValue());
    }

    /**
     * Tests that no-caching headers are returned from /issues/
     */
    @Test
    public void testIssuesCachingHeaders() throws IOException
    {
        navigation.gotoPage("/issues/");
        WebResponse response = tester.getDialog().getResponse();
        assertEquals("no-cache, no-store, must-revalidate", response.getHeaderField("Cache-Control"));
        assertEquals("no-cache", response.getHeaderField("Pragma"));
        assertEquals("Thu, 01 Jan 1970 00:00:00 GMT", response.getHeaderField("Expires"));
    }

    @Test
    public void testSeoFriendlyTableIsNotPresentWithNoQuery() throws IOException
    {
        navigation.gotoPage("/issues/");
        tester.assertElementNotPresent("issuetable");
    }

    @Test
    public void testSeoFriendlyTableIsPresentAndUsesEmptyJqlQuery() throws IOException
    {
        navigation.gotoPage("/issues/?jql=");
        tester.assertElementPresent("issuetable");
        tester.assertElementPresent("issuerow10360");
        HtmlTable table = page.getHtmlTable("issuetable");
        assertEquals(51, table.getRowCount());
    }

    @Test
    public void testSeoFriendlyTableIsPresentAndReflectsSearch() throws IOException
    {
        navigation.gotoPage("/issues/?jql=issuetype=bug");
        tester.assertElementPresent("issuetable");
        tester.assertElementNotPresent("issuerow10272"); // XSS-15 - A New Feature in this dataset
        tester.assertElementPresent("issuerow10241"); // XSS-12 - A bug
        HtmlTable table = page.getHtmlTable("issuetable");
        assertEquals(51, table.getRowCount());
    }

    @Test
    public void testCurrentSearchIsStoredInSession() throws Exception
    {
        // Test session search params returned from navigating to /issues/
        navigation.gotoPage("/issues/?jql=project = BLUK");
        navigation.gotoPage("/issues/");
        assertEquals("/issues/", navigation.getCurrentPage()); // test session search not redirected
        assertEquals("{\"filter\":null,\"jql\":\"project = BLUK\",\"startIndex\":0}", getSessionSearch());

        // Test nefarious session search jql
        navigation.gotoPage("/issues/?jql=text ~ \"</script><script>alert('hi')</script>\"");
        navigation.gotoPage("/issues/");
        assertEquals("/issues/", navigation.getCurrentPage()); // test session search not redirected
        assertEquals("{\"filter\":null,\"jql\":\"text ~ \\\"</script><script>alert('hi')</script>\\\"\",\"startIndex\":0}", getSessionSearch());

        // assert not present when filter is in querystring
        navigation.gotoPage("/issues/?jql=project = BLUK");
        navigation.gotoPage("/issues/?filter=10016");
        assertTrue(StringUtils.isBlank(getSessionSearch()));

        // assert not present when jql is in querystring
        navigation.gotoPage("/issues/?filter=10020&jql=project = BLUK");
        navigation.gotoPage("/issues/");
        assertEquals("{\"filter\":10020,\"jql\":\"project = BLUK\",\"startIndex\":0}", getSessionSearch());

        // assert not present when jql is in querystring
        navigation.gotoPage("/issues/?jql=project = BLUK");
        navigation.gotoPage("/issues/?jql=project=XSS");
        assertTrue(StringUtils.isBlank(getSessionSearch()));
    }

    @Test
    public void testSeoFriendlyTableExistsWhenFilterProvided() throws IOException
    {
        navigation.gotoPage("/issues/?filter=10016");
        tester.assertElementPresent("issuerow10320");
        HtmlTable table = page.getHtmlTable("issuetable");
        assertEquals(51, table.getRowCount());
    }

    @Test
    public void testSeoFriendlyTableStartsAtZeroWithNastyIndex() throws IOException
    {
        navigation.gotoPage("/issues/?startIndex=ImANastyIndex");
        tester.assertElementNotPresent("issuetable");
    }

    @Test
    public void testSeoFriendlyTableStartsAtZeroWithNegativeIndex() throws IOException
    {
        navigation.gotoPage("/issues/?startIndex=-12");
        tester.assertElementPresent("issuerow10320");
        HtmlTable table = page.getHtmlTable("issuetable");
        String rowText = table.getRow(1).getCellAsText(2); // First row is the column titles
        assertTrue(rowText.equals("asdasdasdasd 10:57am"));
    }

    @Test
    public void testSeoFriendlyTableExistForSystemFilter() throws IOException
    {
        navigation.gotoPage("/issues/?filter=-3");
        tester.assertElementPresent("issuetable");
    }

    @Test
    public void testRequestingWithSystemFilterIdDoesnThrowNFE()
    {
        navigation.gotoPage("/issues/?filter=-1&jql=project=XSS");
        tester.assertElementPresent("issuerow10320");
    }

    @Test
    public void testUserParms()
    {
        navigation.logout();
        navigation.gotoPage("/issues/");
        String userParms = tester.getDialog().getElement("user-parms").getAttribute("data-user-parms");
        assertTrue(userParms.contains("\"createSharedObjects\":false"));

        navigation.login("admin");
        navigation.gotoPage("/issues/");
        userParms = tester.getDialog().getElement("user-parms").getAttribute("data-user-parms");
        assertTrue(userParms.contains("\"createSharedObjects\":true"));
    }

    /**
     * If the user doesn't have a preferred search mode, we should use basic.
     */
    @Test
    public void testDefaultSearchModeIsBasic()
    {
        navigation.logout();
        navigation.login("fred", "fred");
        navigation.gotoPage("/issues/");

        String selector = "meta[name=ajs-user.search.mode]";
        Element element = (Element)locator.css(selector).getNode();
        assertEquals("basic", element.getAttribute("content"));
    }

    /**
     * Tests that going to a system filter that requires login when not logged in redirects to login page
     * @throws IOException
     */
    @Test
    public void testSystemFilterThatRequiresLoginRedirectsToLogin() throws IOException
    {
        HttpGet httpGet = new HttpGet(environmentData.getBaseUrl() + "/issues/?filter=-2");
        HttpClient httpClient = new DefaultHttpClient();
        httpClient.getParams().setBooleanParameter(ClientPNames.HANDLE_REDIRECTS, false);
        HttpResponse response = httpClient.execute(httpGet);

        assertEquals(302, response.getStatusLine().getStatusCode());
        String expectedUrl = environmentData.getBaseUrl() + "/login.jsp?permissionViolation=true&os_destination=%2Fissues%2F%3Ffilter%3D-2";
        assertEquals(expectedUrl, response.getHeaders("Location")[0].getValue());
    }

    /**
     * Tests that going to a system filter that does not require login when not logged in behaves normally
     * @throws IOException
     */
    @Test
    public void testSystemFilterThatDoesNotRequireLogin() throws IOException
    {
        HttpGet httpGet = new HttpGet(environmentData.getBaseUrl() + "/issues/?filter=-3");
        HttpClient httpClient = new DefaultHttpClient();
        httpClient.getParams().setBooleanParameter(ClientPNames.HANDLE_REDIRECTS, false);
        HttpResponse response = httpClient.execute(httpGet);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    /**
     * Tests that going to a system filter that requires login when logged in behaves normally
     * @throws IOException
     */
    @Test
    public void testSystemFilterThatRequiresLoginWhenLoggedIn() throws IOException
    {
        navigation.logout();
        navigation.login("fred", "fred");
        navigation.gotoPage("/issues/?filter=-2");

        assertEquals("/issues/?filter=-2", navigation.getCurrentPage());
    }

    @Test
    public void testRequestingTabsDirectly() throws IOException, SAXException
    {
        navigation.logout();
        navigation.login("admin", "admin");
        tester.getDialog().getWebClient().setHeaderField("X-PJAX", "true");

        final GetMethodWebRequest request = new GetMethodWebRequest(environmentData.getBaseUrl() + "/browse/XSS-14?page=com.atlassian.jira.plugin.system.issuetabpanels:worklog-tabpanel");
        WebResponse response = tester.getDialog().getWebClient().sendRequest(request);

        assertNotNull(response.getElementWithID("issue-tabs"));
        assertNull(response.getElementWithID("content"));
    }

    private String getSessionSearch()
    {
        Element navigatorContent = (Element) locator.css(".navigator-content").getNode();
        if (null == navigatorContent)
        {
            fail("navigator-content element not found");
        }
        return navigatorContent.getAttribute("data-session-search-state");
    }
}
