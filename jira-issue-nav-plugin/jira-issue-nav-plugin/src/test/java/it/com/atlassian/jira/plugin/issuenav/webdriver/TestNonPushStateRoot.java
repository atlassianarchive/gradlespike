package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.nimblefunctests.annotation.Restore;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.components.issueviewer.config.IssueNavFeatures;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import org.junit.Test;

/**
 * @since v6.0
 */
@Restore("TestFilters.xml")
public class TestNonPushStateRoot extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;
    private TraceContext traceContext;

    @Override
    public void setUpTest()
    {
        super.setUpTest();
        backdoor.darkFeatures().enableForSite(IssueNavFeatures.I_ROOT.featureKey());

        issuesPage = logInToIssuesPageAsSysadmin(false, "?jql=").getListLayout();
        traceContext = product.getPageBinder().bind(TraceContext.class);
    }

    /**
     * Smoke test for the non-pushState (/i) root.
     * <p/>
     * We do this by making Firefox pretend to be IE.
     */
    @Test
    public void testBasicFunctionality()
    {
        assertCurrentURL("/i#issues/?jql=");

        // Let's perform a search.
        issuesPage.getBasicQuery().project().setValueAndClickOutside("Antarctica").waitForResultsTable();
        assertCurrentURL("/i#issues/?jql=project%20%3D%20AN");

        // And select an issue!
        Tracer checkpoint = traceContext.checkpoint();
        IssueDetailComponent issuePage = issuesPage.getResultsTable().navigateToIssueDetailPage("AN-1");
        traceContext.waitFor(checkpoint, "jira.issue.refreshed");
        assertCurrentURL("/i#browse/AN-1?jql=project%20%3D%20AN");

        // Return to search.
        issuesPage = issuePage.returnToSearch();
        assertCurrentURL("/i#issues/?jql=project%20%3D%20AN");

        // Navigating to raw /i should load session search. We must call goToIssuesPage twice here because simply
        // changing the fragment doesn't cause a page pop, and thus we would be using stale session search data.
        goToIssuesPage().waitForResultsTable();
        goToIssuesPage(false, "").waitForResultsTable();
        assertCurrentURL("/i#issues/?jql=project%20%3D%20AN");

        // Save our search as a filter.
        issuesPage = issuesPage.getFilters().createFilter("My Fabulous Filter");
    }

    /**
     * pushState browsers should be redirected to non-<tt>/i</tt> URLs.
     */
    @Test
    public void testRedirectFromIRoot()
    {
        // We must navigate to a non-/i URL; just changing the hash won't make a request.
        backdoor.darkFeatures().disableForSite(IssueNavFeatures.I_ROOT.featureKey());
        goToIssuesPage();

        issuesPage = goToIssuesPage(false, "?jql=type = Bug").waitForResultsTable();
        assertCurrentURL("/issues/?jql=type%20%3D%20Bug");

        // Clicking browser back should work.
        Tracer tracer = traceContext.checkpoint();
        issuesPage.back().waitForResults(tracer);
        assertCurrentURL("/issues/?jql=");
    }

    /**
     * Non-pushState browsers should be redirected to <tt>/i</tt> URLs on the server.
     */
    @Test
    public void testRedirectToIRoot()
    {
        // Blank search
        issuesPage = goToIssuesPage().waitForResultsTable();
        assertCurrentURL("/i#issues/?jql=");

        // JQL search
        issuesPage = goToIssuesPage("?jql=issuetype = Bug").waitForResultsTable();
        assertCurrentURL("/i#issues/?jql=issuetype%20%3D%20Bug");

        // View issue
        IssueDetailComponent issuePage = goToIssuePage("AN-1");
        assertCurrentURL("/i#browse/AN-1");
    }
}