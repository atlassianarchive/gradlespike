package it.com.atlassian.jira.plugin.issuenav.webdriver.inlineedit;

import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.SplitLayout;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.SplitLayoutIssueList;
import com.atlassian.jira.plugin.issuenav.pageobjects.util.KickassLoginUtil;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import static junit.framework.Assert.assertEquals;

public class TestInlineEditSplitView extends TestInlineEdit
{
    private IssuesPage issuesPage;
    private SplitLayout splitLayout;

    @Override
    protected IssueDetailComponent goToIssue(String issueKey, String username, String password, String query)
    {
        //This only works under the assumption that split view layout is the default layout
        Tracer checkpoint = traceContext.checkpoint();

        String queryString = null;
        try {
            queryString = "?jql=" + URLEncoder.encode(query, "UTF-8");
        }
        catch (UnsupportedEncodingException e) { }

        issuesPage = KickassLoginUtil.kickAssWith(product, username, password, IssuesPage.class, queryString);
        if (splitLayout == null)
        {
            splitLayout = issuesPage.getSplitLayout();
        }
        IssueDetailComponent issueDetailComponent = splitLayout.issue();
        traceContext.waitFor(checkpoint, "jira.issue.fields.loaded");
        return issueDetailComponent;
    }

    @Test
    public void testSuccessInlineEditUpdateTheListPanel()
    {
        String summary = "gotta catch 'em all";
        IssueDetailComponent issuesPage = goToIssue("XSS-17");

        Tracer tracer = traceContext.checkpoint();
        issuesPage.summary().editSaveWait(summary);
        traceContext.waitFor(tracer, "jira.search.finished");

        SplitLayoutIssueList listPanel = splitLayout.issueList();
        assertEquals("XSS-17", listPanel.getHighlightedIssueKey().now());
        Poller.waitUntilEquals(summary, listPanel.getHighlightedIssueSummary());
    }
}
