package it.com.atlassian.jira.plugin.issuenav.qunit;

import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.qunit.test.runner.QUnitPageObjectsHelper;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.io.File;

public class TestQUnit extends BaseJiraWebTest
{
    private final File outputDirectory;
    private final JiraTestedProduct product = TestedProductFactory.create(JiraTestedProduct.class);

    public TestQUnit()
    {
        String location = System.getProperty("jira.qunit.testoutput.location");

        if (StringUtils.isEmpty(location)) {
            System.err.println("jira.qunit.testoutput.location is not defined, writing TestQUnit output to tmp.");
            location = System.getProperty("java.io.tmpdir");
        }

        outputDirectory = new File(location);
    }

    @Test
    public void runJustOurTest() throws Exception
    {
        // Clear web-resource cache so that i18n transformation applies
        backdoor.systemProperties().setProperty("skipI18nTransformation", "true");
        backdoor.systemProperties().setProperty("atlassian.webresource.file.cache.disable", "true");

        QUnitPageObjectsHelper helper = new QUnitPageObjectsHelper(outputDirectory, product.getPageBinder(), "/qunit");
        helper.runTests(QUnitPageObjectsHelper.testFilePathContains("com.atlassian.jira.jira-issue-nav-plugin"));
        helper.runTests(QUnitPageObjectsHelper.testFilePathContains("com.atlassian.jira.jira-issue-nav-components"));

        backdoor.systemProperties().setProperty("skipI18nTransformation", "false");
        backdoor.systemProperties().setProperty("atlassian.webresource.file.cache.disable", "false");
    }
}