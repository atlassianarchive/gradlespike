package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.nimblefunctests.annotation.Restore;
import com.atlassian.jira.pageobjects.pages.viewissue.ActionTrigger;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.AdvancedQueryComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.AuiMessage;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.ResultsTableComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.util.BaseUrlBanner;
import com.atlassian.pageobjects.elements.PageElement;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@Restore("TestSearching.xml")
public class TestStableIssueTable extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;
    private TraceContext traceContext;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();
        traceContext = product.getPageBinder().bind(TraceContext.class);
    }

    @Test
    public void testStaleIssueAfterEdit()
    {
        AdvancedQueryComponent advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.searchAndWait("project = MEN AND issuetype = Task");

        Tracer t = traceContext.checkpoint();
        issuesPage.getResultsTable().getSelectedIssue().editIssue(EasyMap.build("issuetype-field", "Bug"), ActionTrigger.MENU);
        issuesPage.waitForStableUpdate(t);

        Assert.assertEquals(issuesPage.getResultsTable().getResultCount(), 3);
        Assert.assertFalse(issuesPage.getResultsTable().getSelectedIssue().isStaleIssue());
    }

    @Test
    public void testOpeningDeletedIssue()
    {
        // Dismiss the base URL banner as this interferes with the AuiMessage check.
        product.getPageBinder().bind(BaseUrlBanner.class).dismissIfPresent();

        issuesPage.getAdvancedQuery().searchAndWait("project in (MEN, PERU)");
        String currentURL = product.getTester().getDriver().getCurrentUrl().replace(product.getProductInstance().getBaseUrl(), "");

        // Delete a project, then attempt to open one of its issues.
        backdoor.project().deleteProject("MEN");
        AuiMessage auiMessage = issuesPage.getResultsTable().navigateAndExpectError("MEN-2", "");
        ResultsTableComponent resultsTable = issuesPage.getResultsTable();

        assertThat("AUI error is shown", auiMessage.isError(), Matchers.equalTo(true));
        assertTrue(resultsTable.issueIsInaccessible(2));
        assertEquals("10001", resultsTable.getSelectedIssue().getIssueId());
        assertCurrentURL(currentURL);

        // You should be able to j/k onto inaccessible issues.
        resultsTable.selectPreviousIssue().selectNextIssue();
        assertEquals("10001", resultsTable.getSelectedIssueID());

        resultsTable.selectNextIssue().selectPreviousIssue();
        assertEquals("10001", resultsTable.getSelectedIssueID());

        // Clicking on inaccessible issues should highlight them.
        resultsTable.selectNextIssue();
        resultsTable.highlightIssueViaClick(10001L);
        assertEquals("10001", resultsTable.getSelectedIssueID());
    }

    @Test
    public void testSortingIsPreservedBetweenPages()
    {
        // set a small page size so we have multiple pages
        backdoor.applicationProperties().setString("user.issues.per.page", "5");

        AdvancedQueryComponent advancedQueryComponent;
        PageElement sortedColumn;
        Tracer t;


        advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.searchAndWait("order by issuetype DESC");

        sortedColumn = issuesPage.getResultsTable().getColumnHeadingByDataId("issuetype");
        assertThat(sortedColumn, is(sortable()));
        assertThat(sortedColumn, is(sorted("descending")));

        t = traceContext.checkpoint();
        issuesPage.getResultsTable().goToNextPageWithoutWaiting();
        issuesPage.waitForStableUpdate(t);

        sortedColumn = issuesPage.getResultsTable().getColumnHeadingByDataId("issuetype");
        assertThat(sortedColumn, is(sortable()));
        assertThat(sortedColumn, is(sorted("descending")));


        advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.searchAndWait("");

        sortedColumn = issuesPage.getResultsTable().getColumnHeadingByDataId("issuetype");
        assertThat(sortedColumn, is(sortable()));
        assertThat(sortedColumn, is(not(sorted("descending"))));

        t = traceContext.checkpoint();
        issuesPage.getResultsTable().goToNextPageWithoutWaiting();
        issuesPage.waitForStableUpdate(t);

        sortedColumn = issuesPage.getResultsTable().getColumnHeadingByDataId("issuetype");
        assertThat(sortedColumn, is(sortable()));
        assertThat(sortedColumn, is(not(sorted("descending"))));

        issuesPage = issuesPage.getFilters().selectFilter(-1).waitForResultsTable();
        sortedColumn = issuesPage.getResultsTable().getColumnHeadingByDataId("updated");
        assertThat(sortedColumn, is(sortable()));
        assertThat(sortedColumn, sorted("descending"));

        t = traceContext.checkpoint();
        issuesPage.getResultsTable().goToNextPageWithoutWaiting();
        issuesPage.waitForStableUpdate(t);

        sortedColumn = issuesPage.getResultsTable().getColumnHeadingByDataId("updated");
        assertThat(sortedColumn, is(sortable()));
        assertThat(sortedColumn, is(sorted("descending")));

        // now sort by a custom field
        advancedQueryComponent = issuesPage.getAdvancedQuery();
        advancedQueryComponent.searchAndWait("ORDER BY cf[10000] ASC");

        sortedColumn = issuesPage.getResultsTable().getColumnHeadingByDataId("customfield_10000");
        assertThat(sortedColumn, is(sortable()));
        assertThat("CF sort should be shown on first page", sortedColumn, is(sorted("ascending")));

        t = traceContext.checkpoint();
        issuesPage.getResultsTable().goToNextPageWithoutWaiting();
        issuesPage.waitForStableUpdate(t);

        sortedColumn = issuesPage.getResultsTable().getColumnHeadingByDataId("customfield_10000");
        assertThat(sortedColumn, is(sortable()));
        assertThat("CF sort should be retained when navigating across pages", sortedColumn, is(sorted("ascending")));
    }

    @Test
    public void testEndOfStableSearchMessage()
    {
        backdoor.applicationProperties().setString(APKeys.JIRA_STABLE_SEARCH_MAX_RESULTS, "10");

        issuesPage = goToIssuesPage("?jql=");
        assertThat("The end of stable search message should be visible", issuesPage.endOfStableSearchMessageIsVisible(), is(true));
    }

    static Matcher<PageElement> sortable()
    {
        return new TypeSafeMatcher<PageElement>()
        {
            @Override
            protected boolean matchesSafely(PageElement actual)
            {
                return actual.hasClass("sortable");
            }

            @Override
            public void describeTo(Description description)
            {
                description.appendText("an element with CSS class 'sortable'");
            }
        };
    }

    static Matcher<PageElement> sorted(final String order)
    {
        return new TypeSafeMatcher<PageElement>()
        {
            @Override
            protected boolean matchesSafely(PageElement actual)
            {
                return actual.hasClass("active") && actual.hasClass(order);
            }

            @Override
            public void describeTo(Description description)
            {
                description.appendText("an element with CSS classes 'active' and '" + order + "'");
            }
        };
    }
}