package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.ResultsTableComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.SplitLayout;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @since v6.1
 */
public class TestSorting extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;

    @Test
    public void testClearSorts() throws Exception
    {
        administration.restoreData("TestDefaultOrderByClause.xml");
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();

        issuesPage.getAdvancedQuery().searchAndWait("project = hsp ORDER BY assignee ASC");
        waitUntilTrue("Clear sorts should be available when JQL is sorted", issuesPage.tools().isClearSortAvailable());

        issuesPage = issuesPage.tools().selectClearSorts();
        assertEquals("project = hsp", issuesPage.getAdvancedQuery().getCurrentQuery());
        waitUntilFalse("Clear sorts should not be available after clearing it", issuesPage.tools().isClearSortAvailable());
    }

    @Test
    public void testSystemFieldDefaultSortOrderings()
    {
        administration.restoreData("TestSystemFieldDefaultSortOrderings.xml");
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();
        assertDoesNotSupportSorting("comment");
        assertNotAbleToSort("category");
        assertNotAbleToSort("parent");
        assertNotAbleToSort("savedFilter");
        assertNotAbleToSort("text");
    }

    @Test
    public void testMultipleCustomFieldsWithSameDisplayName() throws Exception
    {
        administration.restoreData("TestSortMultipleCustomFieldsWithSameDisplayName.xml");
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();

        issuesPage.getAdvancedQuery().searchAndWait("ORDER BY cf[10004] ASC");
        issuesPage.getResultsTable().sortColumn("customfield_10005", ResultsTableComponent.SORT_ORDER.asc);
        assertTrue("No JQL errors/warnings", !(issuesPage.hasJQLWarnings() || issuesPage.hasJQLErrors()));
        assertEquals("ORDER BY cf[10005] ASC, cf[10004] ASC", issuesPage.getAdvancedQuery().getCurrentQuery());
    }

    @Test
    public void testOrderByInvisibleCustomField() throws Exception
    {
        // try ordering by a custom field that is only configured in a project that the user
        // does not have browse project on.
        administration.restoreData("TestOrderByInvisibleCustomField.xml");
        issuesPage = logInToIssuesPage("fred", "fred", "?jql=").getListLayout();
        issuesPage.getAdvancedQuery().searchAndWait("ORDER BY DT");
        assertEquals("Not able to sort using field 'DT'.", issuesPage.getJQLErrors());
    }

    @Test
    public void testJqlBuiltOrderByClauses()
    {
        administration.restoreData("TestCustomFieldDefaultSortOrderings.xml");
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();

        _testFieldCannotBeUsedTwice();
        _testDisabledCustomFieldsSorting();

        assertNotAbleToSort("bleargh");
    }

    @Test
    public void testSortingStillWorksAfterSwitchingBetweenLayouts()
    {
        final List<String> EXPECTED_ISSUES = Lists.newArrayList("HSP-3", "HSP-2", "HSP-1");
        final List<String> REVERSED_EXPECTED_ISSUES = Lists.reverse(EXPECTED_ISSUES);

        administration.restoreData("TestDefaultOrderByClause.xml");
        //Split and list view sorting still works when loading a JQL with a order by clause. See JDEV-24593
        issuesPage = logInToIssuesPageAsSysadmin("?jql=ORDER BY summary ASC");
        //Assert initial ordering for split view
        SplitLayout splitLayout = issuesPage.getSplitLayout();
        assertEquals(EXPECTED_ISSUES, splitLayout.issueList().getIssueKeys());

        //Assert ordering is reversed after clicking the order by text
        splitLayout.getOrderByPanel().clickOrderByText();
        assertEquals(REVERSED_EXPECTED_ISSUES, splitLayout.issueList().getIssueKeys());

        issuesPage.getBasicQuery().project().setValueAndSubmit("homosapien");
        assertEquals("project = HSP ORDER BY summary DESC", issuesPage.getAdvancedQuery().getCurrentQuery());

        //Assert initial ordering for list view
        issuesPage = splitLayout.switchLayouts();
        assertEquals(REVERSED_EXPECTED_ISSUES, issuesPage.getResultsTable().getIssueKeys());

        //Assert ordering is reverse after click the summary column
        issuesPage.getResultsTable().sortColumn("summary", ResultsTableComponent.SORT_ORDER.asc);
        assertEquals(EXPECTED_ISSUES, issuesPage.getResultsTable().getIssueKeys());

        //The JQL is not trashed when performing sorting. See JDEV-24596
        assertEquals("project = HSP ORDER BY summary ASC", issuesPage.getAdvancedQuery().getCurrentQuery());
    }

    private void _testFieldCannotBeUsedTwice()
    {
        // same field twice
        issuesPage.getAdvancedQuery().searchAndWait("ORDER BY key, priority, key");
        assertJQLError("The sort field 'key' is referenced multiple times in the JQL sort.");

        // system field with alias
        issuesPage.getAdvancedQuery().searchAndWait("ORDER BY issuekey, priority, key");
        assertJQLError("The sort field 'key' is referenced multiple times in the JQL sort. Field 'key' is an alias for field 'issuekey'.");

        // custom field with alias
        issuesPage.getAdvancedQuery().searchAndWait("ORDER BY DP, priority, cf[10001]");
        assertJQLError("The sort field 'cf[10001]' is referenced multiple times in the JQL sort. Field 'cf[10001]' is an alias for field 'DP'.");
    }

    private void _testDisabledCustomFieldsSorting()
    {
        try
        {
            administration.plugins().disablePlugin("com.atlassian.jira.plugin.system.customfieldtypes");

            // verify that we can't sort by custom fields when they are disabled
            final String[] fields = {"CSF", "DP", "DT", "FTF", "GP", "II", "MC", "MGP", "MS", "MUP", "NF", "PP", "RB", "ROTF", "SL", "SVP", "TF", "UP", "URL", "VP",
                    "cf[10000]", "cf[10001]", "cf[10002]", "cf[10003]", "cf[10004]", "cf[10005]", "cf[10006]", "cf[10007]", "cf[10008]", "cf[10009]",
                    "cf[10010]", "cf[10011]", "cf[10012]", "cf[10013]", "cf[10014]", "cf[10015]", "cf[10016]", "cf[10017]", "cf[10018]", "cf[10019]", "cf[10020]" };
            for (String field : fields)
            {
                assertNotAbleToSort(field);
            }

            loadFilterAndAssertNotAbleToSort("CSF", 10000);
            loadFilterAndAssertNotAbleToSort("DP", 10001);
            loadFilterAndAssertNotAbleToSort("DT", 10002);
            loadFilterAndAssertNotAbleToSort("FTF", 10003);
        }
        finally
        {
            administration.plugins().enablePlugin("com.atlassian.jira.plugin.system.customfieldtypes");
        }
    }

    private void loadFilterAndAssertNotAbleToSort(final String fieldName, final int filterId)
    {
        issuesPage = goToIssuesPage("?filter=" + filterId);
        issuesPage.getAdvancedQuery().submit();
        assertJQLError(String.format("Not able to sort using field '%s'.", fieldName));
    }

    private void assertDoesNotSupportSorting(final String fieldName)
    {
        issuesPage.getAdvancedQuery().searchAndWait("ORDER BY " + fieldName + " ASC");
        assertJQLError(String.format("Field '%s' does not support sorting.", fieldName));
    }

    private void assertNotAbleToSort(final String fieldName)
    {
        issuesPage.getAdvancedQuery().searchAndWait("ORDER BY " + fieldName + " ASC");
        assertJQLError(String.format("Not able to sort using field '%s'.", fieldName));
    }

    private void assertJQLError(String message)
    {
        assertEquals(message, issuesPage.getJQLErrors());
    }
}
