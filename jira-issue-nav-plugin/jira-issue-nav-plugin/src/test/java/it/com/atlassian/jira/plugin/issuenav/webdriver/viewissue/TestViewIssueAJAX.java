package it.com.atlassian.jira.plugin.issuenav.webdriver.viewissue;

import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.components.menu.IssuesMenu;
import com.atlassian.jira.pageobjects.dialogs.CommentDialog;
import com.atlassian.jira.plugin.issuenav.pageobjects.ShareDialog;
import com.atlassian.jira.pageobjects.dialogs.quickedit.DeleteIssueDialog;
import com.atlassian.jira.pageobjects.elements.GlobalMessage;
import com.atlassian.jira.pageobjects.pages.viewissue.Subtask;
import com.atlassian.jira.pageobjects.pages.viewissue.SubtaskModule;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.AbstractIssueDetailComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueErrorComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.ResultsTableComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.SplitLayout;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.pageobjects.DelayedBinder;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static org.hamcrest.Matchers.comparesEqualTo;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * View issue tests with the AJAX/pushState dark feature enabled.
 *
 * @since v6.0
 */
public class TestViewIssueAJAX extends TestViewIssue
{
    @Override
    public void setUpTest()
    {
        super.setUpTest();
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();
    }

    @Test
    public void testClickingRecentIssuesShowNoPager()
    {
        Tracer checkpoint = traceContext.checkpoint();
        getIssuesMenu().getElementsForRecentIssues().get(1).click();
        traceContext.waitFor(checkpoint, getViewIssueTraceKey());
        IssueDetailComponent issue = product.getPageBinder().bind(IssueDetailComponent.class, "ARA-1");
        assertCurrentURLTimed("The URL after loading issue from 'recent issues' shouldn't contain search context",
                "/browse/ARA-1");
        assertEquals("ARA-1", issue.getIssueKey());
        Poller.waitUntilFalse(issue.hasNextPreviousPagerTimed());

        directNavigateToViewIssueUrl("/browse/XSS-17", "XSS-17");
        assertEquals("XSS-17", issue.getIssueKey());
        Poller.waitUntilFalse(issue.hasNextPreviousPagerTimed());
    }

    @Test
    public void switchingBetweenTabPanelsDoesNotChangeHistory() throws Exception
    {
        // first click on XSS-12
        IssueDetailComponent issue = chooseIssue("XSS-12");
        issue.clickIssueTabPanel("worklog-tabpanel");

        // then go to XSS-11
        issue = issue.nextIssue();
        issue.clickIssueTabPanel("comment-tabpanel");

        // now click back.
        product.getTester().getDriver().navigate().back();

        // we should be back on XSS-12, on the comment-tabpanel
        final DelayedBinder<IssueDetailComponent> issueBinder = product.getPageBinder().delayedBind(IssueDetailComponent.class, "XSS-12");

        TimedQuery<Boolean> canBind = new CanBind(issueBinder);
        Poller.waitUntilTrue("Failed to bind to XSS-12 after clicking Back", canBind);
        assertEquals("The Comments tab should be selected", "comment-tabpanel", issue.getIssueTabPanel());
    }

    @Test
    public void testSavingIssueUpdatesCache()
    {
        IssueDetailComponent issue = chooseIssue("XSS-12");
        String updatedSummary = "updated summary";
        issue.summary().editSaveWait(updatedSummary);
        IssuesPage issuesPage = issue.returnToSearch();
        SplitLayout splitLayout = issuesPage.getSplitLayout();
        IssueDetailComponent issueDetail = splitLayout.getIssueDetail();
        assertEquals(updatedSummary, issueDetail.getSummary());
        assertEquals(updatedSummary, splitLayout.getSelectedIssue().getSummary());
    }

    /**
     * Test that the inline error message shows up when navigating to a missing issue.
     */
    @Test
    public void testMissingIssueInlineError()
    {
        final String deletedIssue = "XSS-12";
        assertEquals(204, new IssueClient(environmentData).delete(deletedIssue, "false").statusCode);

        final ResultsTableComponent resultsTable = issuesPage.getResultsTable();
        resultsTable.clickIssue(deletedIssue);
        final DelayedBinder<IssueErrorComponent> error = product.getPageBinder().delayedBind(IssueErrorComponent.class);
        TimedQuery<Boolean> canBind = new CanBind(error);

        Poller.waitUntilTrue("Expected inline error message for missing issue", canBind);
        final String expected = "XSS-12 can not be opened. The issue may have been deleted or you might not have permission to see the issue.";
        assertEquals(expected, error.get().getErrorMessage().getText());
    }

    @Test
    public void testIssueHistory()
    {
        chooseIssue("XSS-17")
                .nextIssue().waitForNextIssueLink()
                .nextIssue().waitForNextIssueLink()
                .nextIssue().waitForNextIssueLink()
                .nextIssue().waitForNextIssueLink();
        IssuesMenu issuesMenu = product.getPageBinder().bind(JiraHeader.class).getIssuesMenu();
        assertEquals(Lists.newArrayList("XSS-13 asdsad", "XSS-14 asdasdasd", "XSS-15 asdsadasd", "XSS-16 asdasdasd", "XSS-17 asdasdasdasd 10:57am"), issuesMenu.open().getRecentIssues());
    }

    @Test
    public void testIssueHistoryWithDirectNavigation()
    {
        chooseIssue("XSS-17");

        String frag = getIssueURL("XSS-16", "project%20%3D%20XSS");
        this.directNavigateToViewIssueUrl(frag, "XSS-16");

        frag = getIssueURL("XSS-15", "project%20%3D%20XSS");
        this.directNavigateToViewIssueUrl(frag, "XSS-15");

        frag = getIssueURL("XSS-14", "project%20%3D%20XSS");
        this.directNavigateToViewIssueUrl(frag, "XSS-14");

        frag = getIssueURL("XSS-13", "project%20%3D%20XSS");
        this.directNavigateToViewIssueUrl(frag, "XSS-13");

        IssuesMenu issuesMenu = product.getPageBinder().bind(JiraHeader.class).getIssuesMenu();
        assertEquals(Lists.newArrayList("XSS-13 asdsad", "XSS-14 asdasdasd", "XSS-15 asdsadasd", "XSS-16 asdasdasd", "XSS-17 asdasdasdasd 10:57am"), issuesMenu.open().getRecentIssues());
    }

    @Test
    public void testClickingIssueInIssuesMenuRemovesSearchContext()
    {
        chooseIssue("XSS-17").nextIssue();
        final IssuesMenu issuesMenu = product.getPageBinder().bind(JiraHeader.class).getIssuesMenu();
        issuesMenu.open();
        assertThat("XSS-17 is second in recent issues", issuesMenu.getRecentIssues().get(1).contains("XSS-17"), Matchers.equalTo(true));

        issuesMenu.getElementsForRecentIssues().get(1).click();
        final DelayedBinder<IssueDetailComponent> issueBinder = product.getPageBinder().delayedBind(IssueDetailComponent.class, "XSS-17");
        final TimedQuery<Boolean> canBind = new CanBind(issueBinder);
        Poller.waitUntilTrue("Failed to bind to XSS-17 after clicking on issue in Issues menu", canBind);

        assertCurrentURL("/browse/XSS-17");
    }

    @Test
    public void pageThroughDeletedIssues()
    {
        String jqlQuery = "?jql=project%20in%20(BLUK%2C%20ARA)%20ORDER%20BY%20summary%20ASC";

        issuesPage = goToIssuesPage(jqlQuery);
        AbstractIssueDetailComponent abstractIssueDetailComponent = chooseIssue("ARA-1");
        backdoor.project().deleteProject("BLUK");

        IssueErrorComponent issueErrorComponent = abstractIssueDetailComponent.nextIssue(IssueErrorComponent.class);
        assertThat("Full page error is displayed", issueErrorComponent.getErrorMessage().isError(), Matchers.equalTo(true));
        assertThat("Pager is rendered", issueErrorComponent.hasNextPreviousPager(), Matchers.equalTo(true));

        IssueDetailComponent issueDetailComponent = issueErrorComponent.nextIssue();
        assertCurrentURL("/browse/ARA-3" + jqlQuery);
        assertThat("Summary is rendered", issueDetailComponent.getSummary(), Matchers.equalTo("link test"));
        assertThat("Pager is rendered", issueDetailComponent.hasNextPreviousPager(), Matchers.equalTo(true));

    }

    @Test
    public void testServerViewIssueContainsPagerWithValidJQL()
    {
        /*
         Directly navigate to the view issue URL with search context
         */
        String frag = getIssueURL("XSS-6", "project%20%3D%20XSS");
        IssueDetailComponent issuePage = this.directNavigateToViewIssueUrl(frag, "XSS-6");

        Poller.waitUntilTrue("Pager should exist when given a valid search context",
                issuePage.hasNextPreviousPagerTimed());
        TimedCondition nextPrevIssueElement = Conditions.and(issuePage.hasPreviousIssueTimed(), issuePage.hasNextIssueTimed());
        Poller.waitUntilTrue("Next and previous issue element should exist ",
                nextPrevIssueElement);

        Poller.waitUntilTrue("Prev issue should exist with the correct issue key",
                issuePage.getPreviousIssueLink().timed().hasAttribute("title", "Previous Issue 'XSS-7' ( Type 'k' )"));
        Poller.waitUntilTrue("Next issue should exist with the correct issue key",
                issuePage.getNextIssueLink().timed().hasAttribute("title", "Next Issue 'XSS-5' ( Type 'j' )"));
    }

    @Test
    public void testServerViewIssueDoesNotContainsPagerWhenNotInSearchContext()
    {
        /*
         Directly navigate to the view issue URL with invalid search context
         */
        String frag = getIssueURL("XSS-6", "Atlassian????");
        IssueDetailComponent issuePage = this.directNavigateToViewIssueUrl(frag, "XSS-6");

        Poller.waitUntilFalse("Pager should not exist when given an ivalid search context",
                issuePage.hasNextPreviousPagerTimed());
        TimedCondition nextPrevIssueElement = Conditions.and(issuePage.hasPreviousIssueTimed(), issuePage.hasNextIssueTimed());
        Poller.waitUntilFalse("Next and previous issue element should not exist ",
                nextPrevIssueElement);

        /*
         Directly navigate to the view issue URL with valid jql but issue is not in search context
        */
        frag = getIssueURL("XSS-6", "project%20%3D%20BULK");
        issuePage = this.directNavigateToViewIssueUrl(frag, "XSS-6");

        Poller.waitUntilFalse("Pager should not exist when not in search context",
                issuePage.hasNextPreviousPagerTimed());
        nextPrevIssueElement = Conditions.and(issuePage.hasPreviousIssueTimed(), issuePage.hasNextIssueTimed());
        Poller.waitUntilFalse("Next and previous issue element when not in search context",
                nextPrevIssueElement);
    }

    /**
     * JRADEV-18168 MoreActions drop down should close when navigating to another issue
     */
    @Test
    public void testViewIssueShouldResetMoreActionsMenuOnNextPreviousIssue()
    {
        issuesPage.getResultsTable();
        IssueDetailComponent issueDetailComponent = chooseIssue("BULK-84");

        issueDetailComponent.clickMoreActions();
        assertEquals("MoreActions dropdown should be visible", true, issueDetailComponent.isMoreActionsOpen());

        IssueDetailComponent nextIssue = issueDetailComponent.nextIssueUsingKeyboardShortcut();
        assertEquals("MoreActions dropdown should be hidden", true, !nextIssue.isMoreActionsOpen());
    }

    @Test
    public void testReturnToSearchFromViewIssueEnteredViaIssueActionCogShowsCorrectUrl()
    {
        IssueDetailComponent issue = issuesPage.getResultsTable().openSelectedIssueViaIssueActionCog();
        assertCurrentURLTimed("/browse/XSS-17?jql=");
        issue.returnToSearch();
        assertCurrentURLTimed("/issues/?jql=");

        String filter = "?filter=-4&jql=project%20%3D%20BULK%20AND%20issuetype%20%3D%20Improvement%20ORDER%20BY%20createdDate%20DESC";
        issuesPage = goToIssuesPage(filter);
        issue = issuesPage.getResultsTable().openSelectedIssueViaIssueActionCog();
        assertCurrentURLTimed("/browse/BULK-2" + filter);
        issue.returnToSearch();
        assertCurrentURLTimed("/issues/" + filter);
    }

    /**
     * JRADEV-19023 Navigating directly to an issue that's not on the first page should show the correct page.
     */
    @Test
    public void testServerRenderedTable()
    {
        issuesPage = goToIssuePage("BULK-83", "").returnToSearch();
        assertEquals("BULK-83", issuesPage.getResultsTable().getSelectedIssueKey());
    }

    /**
     * JRADEV-19139 Pressing 'u' to return to search from a stand-alone issue should show a blank search.
     */
    @Test
    public void testReturnToSearchFromStandAloneIssue()
    {
        IssueDetailComponent issuePage = goToIssuePage("BULK-83");
        issuesPage = issuePage.returnToSearchUsingKeyboardShortcut();

        assertEquals("", issuesPage.getAdvancedQuery().getCurrentQuery());
        assertEquals("XSS-17", issuesPage.getResultsTable().getSelectedIssueKey());

        // Requesting an issue that's not in the search results should work.
        issuePage = goToIssuePage("XSS-17", "project != XSS");
        issuesPage = issuePage.returnToSearchUsingKeyboardShortcut();

        assertEquals("project != XSS", issuesPage.getAdvancedQuery().getCurrentQuery());
        assertEquals(115, issuesPage.getResultsTable().getResultsCountTotal());
        assertTrue(issuesPage.getResultsTable().containsIssue("QA-36")); // We should show the first page of results.
    }

    @Test
    public void testDeleteLastIssue()
    {
        issuesPage = goToIssuesPage("?jql=key = QA-36");
        IssueDetailComponent issuePage = issuesPage.getResultsTable().navigateToIssueDetailPage("QA-36");
        issuePage.deleteIssue("jira.search.finished");

        Poller.waitUntilEquals("No issues were found to match your search", issuesPage.getEmptySearchResultMessage());
    }

    @Test
    public void testNonExistentIssue()
    {
        IssueErrorComponent issueError = goToInvalidIssuePage("JRA-1");
        assertTrue("A full-screen error message is shown", issueError.isFullScreen());
        assertTrue(issueError.getErrorMessage().getText().contains("The issue you are trying to view does not exist"));

        // With a search context.
        issueError = goToInvalidIssuePage("JRA-1", "");
        assertTrue("A full-screen error message is shown", issueError.isFullScreen());
        assertTrue(issueError.getErrorMessage().getText().contains("The issue you are trying to view does not exist"));
    }

    @Test
    public void testNavigateFromStandaloneLoadedIssueNavDataProperly()
    {
        //Return to search
        IssueDetailComponent issue = directNavigateToViewIssueUrl("/browse/XSS-17", "XSS-17");
        issuesPage = issue.returnToSearchUsingKeyboardShortcut();

        assertTrue("System filters exist after returning to issue nav via 'return to search' from standalone VI",
                issuesPage.getFilters().areSystemFiltersPresent());

        issuesPage.getAdvancedQuery().typePartialQuery("assign");
        Poller.waitUntilTrue("JQL Auto Complete is working after returning to issue nav via 'return to search' from standalone VI",
                issuesPage.getAdvancedQuery().getAutocompleteSuggestions().timed().isVisible());

        //Search for issues
        directNavigateToViewIssueUrl("/browse/XSS-17", "XSS-17");
        final IssuesMenu issuesMenu = product.getPageBinder().bind(JiraHeader.class).getIssuesMenu();
        Tracer checkpoint = traceContext.checkpoint();
        issuesMenu.open().searchForIssues();
        issuesPage.waitForResults(checkpoint);

        assertTrue("System filters exist after returning to issue nav via 'search for issues' from standalone VI",
                issuesPage.getFilters().areSystemFiltersPresent());

        issuesPage.getAdvancedQuery().typePartialQuery("assign");
        Poller.waitUntilTrue("JQL Auto Complete is working after returning to issue nav via 'search for issues' from standalone VI",
                issuesPage.getAdvancedQuery().getAutocompleteSuggestions().timed().isVisible());

        //Delete issue
        directNavigateToViewIssueUrl("/browse/XSS-17", "XSS-17");
        issue.deleteIssue("jira.search.finished");

        assertTrue("System filters exist after returning to issue nav via 'delete issue' from standalone VI",
                issuesPage.getFilters().areSystemFiltersPresent());

        issuesPage.getAdvancedQuery().typePartialQuery("assign");
        Poller.waitUntilTrue("JQL Auto Complete is working after returning to issue nav via 'delete issue' from standalone VI",
                issuesPage.getAdvancedQuery().getAutocompleteSuggestions().timed().isVisible());
    }

    @Test
    public void testAnonymousUserLoginLinkUpdatesOnNavigation()
    {
        product.logout();
        issuesPage = goToIssuesPage().getListLayout();
        issuesPage.newSearch();
        assertEquals(environmentData.getBaseUrl() + "/login.jsp?os_destination=%2Fissues%2F%3Fjql%3D", issuesPage.getLoginLinkUrl());

        chooseIssue("XSS-17");
        assertEquals(environmentData.getBaseUrl() + "/login.jsp?os_destination=%2Fbrowse%2FXSS-17%3Fjql%3D", issuesPage.getLoginLinkUrl());

        issuesPage.back();
        issuesPage.getResultsTable().goToNextPage();
        assertEquals(environmentData.getBaseUrl() + "/login.jsp?os_destination=%2Fissues%2F%3Fjql%3D%26startIndex%3D50", issuesPage.getLoginLinkUrl());
    }

    @Test
    public void testShareParametersAreCorrectFromStandaloneVI()
    {
        ShareDialog dialog = issuesPage.getShareDialog();
        IssueDetailComponent issue = directNavigateToViewIssueUrl("/browse/XSS-17?jql=", "XSS-17");

        Tracer tracer = traceContext.checkpoint();
        dialog.open().addRecipient("a@a.com").addNote("standalone vi").submit().waitForSubmit(tracer);
        checkShareRequestData(dialog.getShareRequestData(tracer), "issue/XSS-17", null, "a@a.com", "standalone vi", null);

        Poller.waitUntil(dialog.getSearchPermalinkValue(), Matchers.endsWith("/jira/browse/XSS-17"));

        issue.returnToSearch();

        dialog = issuesPage.getShareDialog();
        dialog.refineShareTarget("issuenav");
        Poller.waitUntil(dialog.getSearchPermalinkValue(), Matchers.endsWith("/jira/issues/?jql="));
    }

    @Test
    public void testShareDialogNoPersistOnIssueChange()
    {
        IssueDetailComponent issue = directNavigateToViewIssueUrl("/browse/XSS-17?jql=", "XSS-17");
        ShareDialog shareDialog = issue.getShareDialog().open();
        shareDialog.addNote("note");
        issue = issue.nextIssue();
        assertEquals("Should not have persisted note on issue change", "", issue.getShareDialog().open().getNote());

        issue = issue.previousIssue();
        assertEquals("Should not have persisted note on returning to set issue", "", issue.getShareDialog().open().getNote());
    }

    @Test
    public void testStandAloneIssueOperations()
    {
        JiraHeader header = product.getPageBinder().bind(JiraHeader.class);
        header.getIssuesMenu().open().getElementsForRecentIssues().get(1).click();

        IssueDetailComponent issue = product.getPageBinder().bind(IssueDetailComponent.class, "ARA-1");
        assertThat("The edit dialog is operating on the correct issue", issue.openEditDialog().getTitle(), containsString("ARA-1"));
    }

    @Test
    public void testLoadIssueUpdateIssueTableRow()
    {
        assertEquals("Ron Weasley", issuesPage.getResultsTable().getSelectedIssue().getAssignee());

        backdoor.issues().assignIssue("XSS-17", "george");

        Tracer tracer = traceContext.checkpoint();
        issuesPage.getResultsTable().clickIssue("XSS-17");

        IssueDetailComponent issue = product.getPageBinder().bind(IssueDetailComponent.class, "XSS-17");
        issue.ready();
        issuesPage.waitForStableUpdate(tracer);

        issue.returnToSearch();
        assertEquals("Loading an issue should update the issue row in issue nav",
                "George", issuesPage.getResultsTable().getSelectedIssue().getAssignee());
    }

    /**
     * JRADEV-19788 Deleting a standalone issue should show a success message.
     */
    @Test
    public void testDeleteStandaloneIssue()
    {
        IssueDetailComponent issuePage = goToIssuePage("XSS-17");
        issuePage.deleteIssue("jira.search.finished");

        assertCurrentURL("/issues/?jql=");
        assertThat("A success message is displayed", product.getPageBinder().bind(GlobalMessage.class).getMessage().now(),
                containsString("XSS-17 has been deleted"));
    }

    /**
     * JRADEV-20085 Deleting a sub-task shouldn't return you to issue search.
     */
    @Test
    public void testDeleteSubTask()
    {
        IssueDetailComponent issuePage = goToIssuePage("BULK-101");
        Tracer tracer = traceContext.checkpoint();

        Subtask subtask = issuePage.subtasks().getSubtasks().get(0);
        subtask.invoke("issueaction-delete-issue", DeleteIssueDialog.class).deleteIssue();
        waitForIssueRefresh(tracer);

        assertCurrentURL("/browse/BULK-101");
        assertThat("A success message is displayed", product.getPageBinder().bind(GlobalMessage.class).getMessage().now(),
                containsString("BULK-102 has been deleted"));
    }

    /**
     * JRADEV-19788 Modifying a sub-task should show a success message.
     */
    @Test
    public void testModifySubTask()
    {
        IssueDetailComponent issuePage = goToIssuePage("BULK-95");
        Tracer tracer = traceContext.checkpoint();

        Subtask subtask = issuePage.subtasks().getSubtasks().get(0);
        subtask.invoke("issueaction-watch-issue", IssueDetailComponent.class, "BULK-95");
        waitForIssueRefresh(tracer);

        assertThat("A success message is displayed", product.getPageBinder().bind(GlobalMessage.class).getMessage().now(),
                containsString("BULK-96 has been updated"));
    }

    /**
     * JRADEV-20374 Selecting a system filter from standalone view issue should work.
     */
    @Test
    public void testSystemFilterFromStandaloneViewIssue()
    {
        goToIssuePage("XSS-17");

        // Select the "My Open Issues" filter
        Tracer tracer = traceContext.checkpoint();
        JiraHeader header = product.getPageBinder().bind(JiraHeader.class);
        header.getIssuesMenu().open().getElementsForFilters().get(0).click();
        issuesPage.waitForResults(tracer);

        // The results count will be 132 if the filter isn't executed.
        assertThat("The filter was executed", issuesPage.getResultsTable().getResultsCountTotal(), is(102));
    }

    /**
     * JRADEV-20559 The "switch layouts" shortcut must not work on full screen issue
     * @throws InterruptedException
     */
    @Test
    public void testSwitchLayoutsShortcutDoesNotWork() throws InterruptedException
    {
        // Activate SplitLayout
        issuesPage.getSplitLayout();

        // Directly navigate to an issue
        String frag = getIssueURL("XSS-17");
        IssueDetailComponent issuePage = directNavigateToViewIssueUrl(frag, "XSS-17");

        // Press the Switch Layout shortcut
        issuePage.switchLayoutsWithoutWait();

        // Wait 10s
        Thread.sleep(10 * 1000L);

        // Check that we still are on the issue page
        assertTrue("The IssuePage should be still loaded", issuePage.isAt());
    }

    /**
     * JRADEV-20611 The "show open sub-tasks" functionality should work in AJAX view issue.
     */
    @Test
    public void testSubTaskFiltering()
    {
        IssueDetailComponent issuePage = goToIssuePage("BULK-41");
        issuePage.closeIssue("jira.issue.update");

        issuePage = goToIssuePage("BULK-11");
        SubtaskModule subtasks = issuePage.subtasks();
        assertThat("The correct number of sub-tasks are shown", subtasks.getSubtasks().size(), is(2));

        Tracer tracer = traceContext.checkpoint();
        subtasks.showOpen();
        traceContext.waitFor(tracer, "jira.issue.refreshed");
        assertThat("The correct number of open sub-tasks are shown", subtasks.getSubtasks().size(), is(1));
    }

    /**
     * JRADEV-21105: Updating an issue does not cause a scroll to the top
     */
    @Test
    public void testIssueUpdateDoesNotScrollToTop()
    {
        IssueDetailComponent issuePage = goToIssuePage("QA-34");
        issuePage.scrollTo(500, false);
        issuePage.closeIssue("jira.issue.update");
        assertFalse(issuePage.scrollPositionAtTop(false));
    }
    /**
     * JRADEV-21105: Updating an issue subtask should not cause a scroll to the top
     */
    @Test
    public void testSubTaskUpdateDoesNotScrollToTop()
    {
        IssueDetailComponent issuePage = goToIssuePage("BULK-4");
        issuePage.scrollTo(500, false);
        Subtask subtask = issuePage.subtasks().getSubtasks().get(1);
        CommentDialog commentDialog = subtask.invoke("issueaction-comment-issue", CommentDialog.class);
        commentDialog.setComment("test comment");
        commentDialog.submit();
        assertFalse(issuePage.scrollPositionAtTop(false));
    }

    /**
     * Many links in the header's issues menu should load via AJAX.
     */
    @Test
    public void testIssuesMenu()
    {
        // Clicking a recent issue should load it
        Tracer checkpoint = traceContext.checkpoint();
        getIssuesMenu().getElementsForRecentIssues().get(0).click();
        traceContext.waitFor(checkpoint, getViewIssueTraceKey());

        // Clicking "more..." should show the "Recently Viewed" filter.
        checkpoint = traceContext.checkpoint();
        getIssuesMenu().openMoreIssues();
        traceContext.waitFor(checkpoint, getSearchFinishedTraceKey());
        assertFalse("Page should not reload, but did", traceContext.exists(checkpoint, getPageReloadSearchFinishedTraceKey()));
        assertTrue("AJAX search should have been performed, but wasn't", traceContext.exists(checkpoint, getAJAXSearchFinishedTraceKey()));
        issuesPage = product.getPageBinder().bind(IssuesPage.class);
        assertEquals("Recently Viewed", issuesPage.getFilters().getActiveFilterName());

        // Clicking "Search for Issues" should execute a new search.
        checkpoint = traceContext.checkpoint();
        getIssuesMenu().searchForIssues();
        traceContext.waitFor(checkpoint, getSearchFinishedTraceKey());
        assertFalse("Page should not reload, but did", traceContext.exists(checkpoint, getPageReloadSearchFinishedTraceKey()));
        assertTrue("AJAX search should have been performed, but wasn't", traceContext.exists(checkpoint, getAJAXSearchFinishedTraceKey()));
        issuesPage = product.getPageBinder().bind(IssuesPage.class);
        assertEquals("", issuesPage.getAdvancedQuery().getCurrentQuery());
    }

    /**
     * @param issueKey The issue's key.
     * @return the described issue's URL.
     */
    @Override
    protected String getIssueURL(String issueKey, String context)
    {
        return getViewIssueUrl(issueKey, context);
    }

    @Override
    protected String getInvalidIssueURL(String issueKey, String context)
    {
        /*
         In the case of AJAX, url will not change when attempting to view an invalid issue
         Url will stay on issue nav with the search context
         */
        return getIssueNavUrl(context);
    }

    @Override
    protected String getHeaderFilterTraceKey()
    {
        return getAJAXSearchFinishedTraceKey();
    }

    @Override
    protected String getBrowserUrlAfterLoadingWithCommentPermaLink(String issueKey, String commentId)
    {
        return getIssueURL(issueKey);
    }

    @Override
    protected String getReturnToSearchTraceKey()
    {
        return "jira.returned.to.search";
    }

    @Override
    protected String getViewIssueTraceKey()
    {
        return "jira.issue.refreshed";
    }

    @Override
    protected String getErrorMessage()
    {
        return "can not be opened. The issue may have been deleted or you might not have permission to see the issue.";
    }
}