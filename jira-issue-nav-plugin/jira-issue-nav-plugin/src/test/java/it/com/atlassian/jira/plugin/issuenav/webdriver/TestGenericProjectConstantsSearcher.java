package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.junit.Assert.assertThat;

/**
 * @since v6.1
 */
@RestoreOnce("TestGenericProjectConstantsSearcher.xml")
public class TestGenericProjectConstantsSearcher extends KickassWebDriverTestCase
{
    public static final String VERSION_ONE_VALUE = "New Version 1";
    public static final String VERSION_FOUR_VALUE = "New Version 4";
    public static final String COMPONENT_ONE_VALUE = "New Component 1";
    public static final String COMPONENT_TWO_VALUE = "New Component 2";
    public static final String NO_VERSION_VALUE = "No Version";
    public static final String NO_COMPONENT_VALUE = "No Component";
    public static final String NO_VERSION = "No Version";
    public static final String UNRELEASED_VERSIONS = "Unreleased Versions";
    public static final String NO_COMPONENT = "No Component";

    private IssuesPage issuesPage;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();
    }

    @Test
    public void testLinkMultipleFixForVersions()
    {
        issuesPage.getBasicQuery().openAddCriteriaDialog().selectAndOpenFixVersion()
                .selectValues(VERSION_ONE_VALUE, VERSION_FOUR_VALUE);

        String value = issuesPage.getBasicQuery().extendedCriteria().getClause("fixfor").getViewHtml();

        assertThat(value, Matchers.allOf(
                Matchers.containsString(FunctTestConstants.VERSION_NAME_ONE),
                Matchers.containsString(FunctTestConstants.VERSION_NAME_FOUR)));
    }

    @Test
    public void testLinkMultipleComponents()
    {
        issuesPage.getBasicQuery().openAddCriteriaDialog().selectAndOpenComponent()
                .selectValues(COMPONENT_ONE_VALUE, COMPONENT_TWO_VALUE);
        String value = issuesPage.getBasicQuery().component().getSelectedValue();

        assertThat(value, Matchers.allOf(
                Matchers.containsString(FunctTestConstants.COMPONENT_NAME_ONE),
                Matchers.containsString(FunctTestConstants.COMPONENT_NAME_TWO)));
    }

    @Test
    public void testNoLinkMultipleAffectsVersions()
    {
        issuesPage.getBasicQuery().openAddCriteriaDialog().selectAndOpenAffectsVersion()
                .selectValues(VERSION_ONE_VALUE, VERSION_FOUR_VALUE);

        String value = issuesPage.getBasicQuery().extendedCriteria().getClause("version").getViewHtml();

        assertThat(value, Matchers.allOf(
                Matchers.containsString(FunctTestConstants.VERSION_NAME_ONE),
                Matchers.containsString(FunctTestConstants.VERSION_NAME_FOUR)));
    }

    @Test
    public void testNoLinkUnreleasedNoComponentsAndVersions()
    {
        issuesPage.getBasicQuery().openAddCriteriaDialog().selectAndOpenFixVersion()
                .selectValues(NO_VERSION_VALUE);
        issuesPage.getBasicQuery().openAddCriteriaDialog().selectAndOpenComponent()
                .selectValues(NO_COMPONENT_VALUE);
        issuesPage.getBasicQuery().openAddCriteriaDialog().selectAndOpenAffectsVersion()
                .selectValues(NO_VERSION_VALUE);

        assertThat(issuesPage.getBasicQuery().extendedCriteria().getClause("fixfor").getViewHtml(), Matchers.containsString(NO_VERSION));
        assertThat(issuesPage.getBasicQuery().component().getSelectedValue(), Matchers.containsString(NO_COMPONENT_VALUE));
        assertThat(issuesPage.getBasicQuery().extendedCriteria().getClause("version").getViewHtml(), Matchers.containsString(NO_VERSION_VALUE));
    }
}
