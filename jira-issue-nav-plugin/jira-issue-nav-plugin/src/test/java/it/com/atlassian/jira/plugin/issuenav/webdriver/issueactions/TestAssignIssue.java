package it.com.atlassian.jira.plugin.issuenav.webdriver.issueactions;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.elements.GlobalMessage;
import com.atlassian.jira.pageobjects.navigator.SelectedIssue;
import com.atlassian.jira.pageobjects.pages.viewissue.ActionTrigger;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.pageobjects.elements.query.Poller;
import it.com.atlassian.jira.plugin.issuenav.webdriver.KickassWebDriverTestCase;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

@WebTest ({ Category.WEBDRIVER_TEST, Category.ISSUES })
public class TestAssignIssue extends KickassWebDriverTestCase
{
    String key = "SUM-1";

    private IssuesPage issuesPage;
    private TraceContext traceContext;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestIssueActions.xml");
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();
        traceContext = product.getPageBinder().bind(TraceContext.class);
    }

    @Test
    public void testFromIssueActionsDialog()
    {
        changeAssignee(ActionTrigger.ACTIONS_DIALOG);
    }

    @Test
    public void testFromKeyboardShortcut()
    {
        changeAssignee(ActionTrigger.KEYBOARD_SHORTCUT);
    }

    @Test
    public void testFromCog()
    {
        changeAssignee(ActionTrigger.MENU);
    }

    private void changeAssignee(ActionTrigger actionTrigger)
    {
        final SelectedIssue selectedIssue = issuesPage.getResultsTable().getSelectedIssue();
        Tracer tracer = traceContext.checkpoint();
        selectedIssue.assignIssue("admin", actionTrigger);
        issuesPage.waitForStableUpdate(tracer);
        Poller.waitUntil(product.getPageBinder().bind(GlobalMessage.class).getMessage(), Matchers.containsString(key + " has been assigned."));
        assertEquals("admin", selectedIssue.getAssignee());
    }
}