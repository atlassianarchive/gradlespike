package it.com.atlassian.jira.plugin.issuenav.func;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.issue.fields.rest.json.beans.FieldHtmlBean;
import com.atlassian.jira.issue.fields.rest.json.beans.FieldTab;
import com.atlassian.jira.components.issueeditor.action.EditFields;
import com.atlassian.jira.components.issueviewer.action.IssueBean;
import com.atlassian.jira.components.issueviewer.config.IssueNavFeatures;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@WebTest ({ Category.FUNC_TEST, Category.ISSUES })
public class TestIssueAction extends FuncTestCase
{
    public static final String URI = "/secure/AjaxIssueAction!default.jspa?issueId=%d&decorator=none";
    public static final String URI_WITH_KEY = "/secure/AjaxIssueAction!default.jspa?issueKey=%s&decorator=none";
    public static final String POST_URI = "/secure/AjaxIssueAction.jspa?issueId=%d&decorator=none";
    public static final String TALK_TO_THE_HAND = "You do not have the permission to see the specified issue";

    private Gson gson;

    @Override
    protected void setUpTest()
    {
        administration.restoreData("TestEditForm.xml");
        gson = new Gson();
    }

    @Test
    public void testEditPermissions() throws IOException
    {
        final HttpClient anonymousClient = createClientForUser(null);
        //this issue exists, but anonymous shouldn't be able to edit!
        HttpResponse response = anonymousClient.execute(new HttpGet(getEnvironmentData().getBaseUrl() + getEditUri(10000)));
        EditFields editFields = gson.fromJson(IOUtils.toString(response.getEntity().getContent()), EditFields.class);
        assertTrue(editFields.getErrorCollection().getErrorMessages().iterator().next().contains("You do not have the permission to see the specified issue"));

        final HttpClient client = createClientForUser("admin");
        //this issue doesn't exist
        HttpGet get = new HttpGet(getEnvironmentData().getBaseUrl() + getEditUri(100));
        response = client.execute(get);

        editFields = gson.fromJson(IOUtils.toString(response.getEntity().getContent()), EditFields.class);
        assertEquals("Issue Does Not Exist", editFields.getErrorCollection().getErrorMessages().iterator().next());

        //finally we have permission. Should be able to see the fields!
        response = client.execute(new HttpGet(getEnvironmentData().getBaseUrl() + getEditUri(10000)));
        editFields = gson.fromJson(IOUtils.toString(response.getEntity().getContent()), EditFields.class);

        assertEquals(13, editFields.getFields().size());
        //lets test one field to make sure it's all good
        FieldHtmlBean expectedSummaryField = new FieldHtmlBean("summary", "Summary", true,
                "<div class=\"field-group\" >\n                                                    <input class=\"text long-field\" id=\"summary\" maxlength=\"255\" name=\"summary\" type=\"text\" value=\"Buggy bug bug!\" />\n                                    </div>", new FieldTab("Field Tab", 0));
        assertEquals(expectedSummaryField, editFields.getFields().get(0));
    }

    @Test
    public void testEditValidation() throws IOException
    {
        navigation.issue().viewIssue("HSP-1");
        text.assertTextPresent("Buggy bug bug!");

        final HttpClient anonymousClient = createClientForUser(null);
        //this issue exists, but anonymous shouldn't be able to edit!

        final HttpPost post = getPost(10000, "my new bug summary!");
        HttpResponse postResponse = anonymousClient.execute(post);

        final String json = IOUtils.toString(postResponse.getEntity().getContent());
        EditFields editFields = gson.fromJson(json, EditFields.class);
        final Collection<String> errorMessages = editFields.getErrorCollection().getErrorMessages();
        final Iterator<String> iterator = errorMessages.iterator();
        final String first = iterator.next();
        assertTrue(first.contains(TALK_TO_THE_HAND));

        //ensure the issue has not been modified!
        navigation.issue().viewIssue("HSP-1");
        text.assertTextPresent("Buggy bug bug!");

        final HttpClient client = createClientForUser("admin");
        postResponse = client.execute(getPost(10000, ""));

        editFields = gson.fromJson(IOUtils.toString(postResponse.getEntity().getContent()), EditFields.class);
        assertEquals("You must specify a summary of the issue.", editFields.getErrorCollection().getErrors().get("summary"));
        
        //should also contain the summary field with the value filled in by the user.
        FieldHtmlBean summaryField = editFields.getFields().get(0);
        assertEquals("summary", summaryField.getId());
        assertTrue(summaryField.getEditHtml().contains("You must specify a summary of the issue."));
        assertTrue(summaryField.getEditHtml().contains("value=\"\""));
        assertFalse(summaryField.getEditHtml().contains("value=\"Buggy bug bug!\""));

        //ensure the issue has not been modified!
        navigation.issue().viewIssue("HSP-1");
        text.assertTextPresent("Buggy bug bug!");

        postResponse = client.execute(getPost(10000, "Yo Dawg: This is my new summary!"));
        assertEquals(200, postResponse.getStatusLine().getStatusCode());

        //ensure the issue has not been modified!
        navigation.issue().viewIssue("HSP-1");
        text.assertTextNotPresent("Buggy bug bug!");
        text.assertTextPresent("Yo Dawg: This is my new summary!");
    }

    @Test
    public void testEditXsrfCheck() throws IOException
    {
        navigation.issue().viewIssue("HSP-1");
        text.assertTextPresent("Buggy bug bug!");

        final HttpClient anonymousClient = createClientForUser("admin");
        HttpPost post = getPost(10000, "my new bug summary!");
        post.removeHeaders("X-Atlassian-Token");
        HttpResponse postResponse = anonymousClient.execute(post);

        assertTrue(IOUtils.toString(postResponse.getEntity().getContent()).contains("XSRF Security Token Missing"));

        //ensure the issue has not been modified!
        navigation.issue().viewIssue("HSP-1");
        text.assertTextPresent("Buggy bug bug!");
    }

    /**
     * Smoke test viewing a private filter.
     */
    @Test
    public void testViewingPrivateFilter()
    {
        navigation.logout();
        navigation.login("fred", "fred");
        navigation.gotoPage("/issues/?filter=" + backdoor.searchRequests()
                .createFilter("admin", "", "Private Filter", ""));
    }

    @Test
    public void testLoadMovedIssueGivesNewIssue() throws IOException
    {
        navigation.gotoPage("/browse/HSP-1");

        tester.clickLink("move-issue");
        tester.assertTextPresent("Move Issue: HSP-1 - Buggy bug bug!");
        tester.selectOption("pid", "monkey");
        tester.submit("Next >>");
        tester.submit("Next >>");
        tester.submit("Move");

        final HttpClient client = createClientForUser("admin");
        HttpGet get = new HttpGet(getEnvironmentData().getBaseUrl() + getEditUriWithKey("HSP-1"));
        HttpResponse response = client.execute(get);

        String json = IOUtils.toString(response.getEntity().getContent());
        IssueContainer issue = gson.fromJson(json, IssueContainer.class);

        Assert.assertThat(issue.getIssue(), Matchers.notNullValue());
        Assert.assertThat(issue.getIssue().getKey(), Matchers.not("HSP-1"));
        Assert.assertThat(issue.getIssue().getKey(), Matchers.equalTo("MKY-1"));
        Assert.assertThat(issue.getIssue().getId(), Matchers.equalTo(10000L));
        String url = tester.getDialog().getResponse().getURL().toString();
        Assert.assertThat(url, Matchers.containsString("MKY-1"));
        Assert.assertThat(url, Matchers.not(Matchers.containsString("HSP-1")));
    }

    private HttpPost getPost(final int issueId, final String summary) throws UnsupportedEncodingException
    {
        final HttpPost method = new HttpPost(getEnvironmentData().getBaseUrl() + getEditPostUri(10000));
        List<NameValuePair> formparams = new ArrayList<NameValuePair>();
        formparams.add(new BasicNameValuePair("summary", summary));
        formparams.add(new BasicNameValuePair("reporter", "admin"));
        formparams.add(new BasicNameValuePair("issuetype", "1"));
        formparams.add(new BasicNameValuePair("assignee", "admin"));
        method.setEntity(new UrlEncodedFormEntity(formparams, "UTF-8"));
        method.setHeader("X-Atlassian-Token", "no-check");

        return method;
    }

    private String getEditUri(final long issueId)
    {
        return String.format(URI, issueId);
    }

    private String getEditPostUri(final long issueId)
    {
        return String.format(POST_URI, issueId);
    }

    private String getEditUriWithKey(final String issueKey)
    {
        return String.format(URI_WITH_KEY, issueKey);
    }

    private HttpClient createClientForUser(@Nullable final String user) throws IOException
    {
        HttpClient client = new DefaultHttpClient();
        client.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BROWSER_COMPATIBILITY);

        if (user != null)
        {
            HttpPost method = new HttpPost(getEnvironmentData().getBaseUrl() + "/login.jsp");
            List<NameValuePair> formparams = new ArrayList<NameValuePair>();
            formparams.add(new BasicNameValuePair("os_username", user));
            formparams.add(new BasicNameValuePair("os_password", user));
            formparams.add(new BasicNameValuePair("os_cookie", String.valueOf(true)));
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, "UTF-8");
            method.setEntity(entity);


            final HttpResponse response = client.execute(method);
            assertEquals("OK", response.getHeaders("X-Seraph-LoginReason")[0].getValue());
            IOUtils.closeQuietly(response.getEntity().getContent());
        }

        return client;
    }

    /**
     * Dummy container class for issuebean to parse json response into usable object.
     */
    private static class IssueContainer {
        IssueBean issue;
        public IssueBean getIssue() {return issue;}
    }
}
