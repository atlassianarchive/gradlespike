package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.nimblefunctests.annotation.Restore;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Tests common to both the pushState and non-pushState variants of view issue.
 *
 * @since v6.0
 */
@Restore("TestInlineEdit.xml")
public class TestViewIssueCaching extends KickassWebDriverTestCase
{
    protected IssuesPage issuesPage;
    protected TraceContext traceContext;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();
        traceContext = product.getPageBinder().bind(TraceContext.class);
    }

    @Test
    public void testPreviouslyViewedIssueLoadsFromCache()
    {
        assertTrue(checkIssueLoadedByServerAndReturn("XSS-12"));
        assertTrue(checkIssueLoadedByCacheAndReturn("XSS-12"));
    }

    @Test
    public void testCacheOverflowLoadsFromServer()
    {
        backdoor.applicationProperties().setString(APKeys.JIRA_SEARCH_CACHE_MAX_SIZE, "3");

        assertTrue(checkIssueLoadedByServerAndReturn("XSS-1"));
        assertTrue(checkIssueLoadedByServerAndReturn("XSS-2"));
        assertTrue(checkIssueLoadedByServerAndReturn("XSS-3"));
        assertTrue(checkIssueLoadedByServerAndReturn("XSS-4"));

        assertTrue(checkIssueLoadedByCacheAndReturn("XSS-2"));
        assertTrue(checkIssueLoadedByCacheAndReturn("XSS-3"));
        assertTrue(checkIssueLoadedByCacheAndReturn("XSS-4"));
        assertTrue(checkIssueLoadedByServerAndReturn("XSS-1"));
    }

    private boolean checkIssueLoadedByServerAndReturn(String issueID)
    {
        Tracer checkpoint = traceContext.checkpoint();
        IssueDetailComponent issueDetailComponent = issuesPage.getResultsTable().navigateToIssueDetailPage(issueID);
        TimedCondition traceMade = traceContext.condition(checkpoint, "jira.issue.loadFromServer");
        boolean result = traceMade.byDefaultTimeout();
        issueDetailComponent.returnToSearch();
        return result;
    }

    private boolean checkIssueLoadedByCacheAndReturn(String issueID)
    {
        Tracer checkpoint = traceContext.checkpoint();
        IssueDetailComponent issueDetailComponent = issuesPage.getResultsTable().navigateToIssueDetailPage(issueID);
        TimedCondition traceMade = traceContext.condition(checkpoint, "jira.issue.loadFromCache");
        boolean result = traceMade.byDefaultTimeout();
        issueDetailComponent.returnToSearch();
        return result;
    }
}