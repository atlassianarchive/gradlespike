package it.com.atlassian.jira.plugin.issuenav.webdriver.inlineedit;

import com.atlassian.jira.pageobjects.components.userpicker.MentionsUserPicker;
import com.atlassian.jira.pageobjects.dialogs.ShifterDialog;
import com.atlassian.jira.pageobjects.dialogs.quickedit.EditIssueDialog;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.Field;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.InlineAssigneeField;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.InlineDescriptionField;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.InlinePriorityField;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.InlineSummaryField;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.InlineTextCustomField;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.common.collect.Lists;
import it.com.atlassian.jira.plugin.issuenav.webdriver.KickassWebDriverTestCase;
import org.junit.Test;
import org.openqa.selenium.Keys;

import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Abstract base class for inline edit in either standalone or split view modes.
 *
 * @since v5.0
 */
public abstract class TestInlineEdit extends KickassWebDriverTestCase
{
    protected TraceContext traceContext;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestInlineEdit.xml");
        traceContext = product.getPageBinder().bind(TraceContext.class);
    }

    @Test
    public void testEditLabels()
    {
        IssueDetailComponent issuesPage = goToIssue("XSS-17");
        issuesPage.labels().editSaveWait("Value1", "Value2");
        List<String> labels = issuesPage.getLabels();
        assertEquals(4, labels.size());
        assertTrue(labels.contains("Value1"));
        assertTrue(labels.contains("Value2"));
        assertTrue(labels.contains("hello"));
        assertTrue(labels.contains("world"));
    }

    @Test
    public void testDirtyEdits()
    {
        IssueDetailComponent issuesPage = goToIssue("XSS-17");
        assertEquals(Lists.newArrayList("hello", "world"), issuesPage.getLabels());
        issuesPage.labels().editSaveWait("Value1", "Value2");
        navigation.gotoDashboard();
        issuesPage = goToIssue("XSS-17");
        assertEquals(Lists.newArrayList("Value1", "Value2", "hello", "world"), issuesPage.getLabels());
    }

    @Test
    public void singleSelectsOpenAutomatically()
    {
        IssueDetailComponent issuesPage = goToIssue("XSS-17");
        InlinePriorityField priority = issuesPage.priority();
        priority.switchToEdit();
        waitUntilTrue("Expected suggestions to be auto opened", priority.singleSelect().isSuggestionsVisible());
        InlineAssigneeField assignee = issuesPage.assignee();
        assignee.switchToEdit();
        waitUntilFalse("Expected suggestions NOT be auto opened for assignee", assignee.singleSelect().isSuggestionsVisible());
    }


    @Test
    public void testInlineEditWithValidationErrors()
    {
        IssueDetailComponent issuesPage = goToIssue("XSS-17");
        final String summaryFragment = "This is a long Summary. ";
        final StringBuilder longSummary = new StringBuilder();
        for (int i = 0; i < 20; i++)
        {
            longSummary.append(summaryFragment);
        }
        final InlineSummaryField summary = issuesPage.summary();
        summary.switchToEdit();
        summary.setMaxLength(300);
        summary.fill(longSummary.toString());

        final Field priority = issuesPage.priority().switchToEdit().fill("Minor");
        Tracer tracer = traceContext.checkpoint();
        priority.save();

        summary.waitToSave(tracer);  // setting priority will result in the blurring and saving of summary
        assertTrue("Errors should remain for [summary]", issuesPage.getFieldsInError().contains("summary"));

        tracer = traceContext.checkpoint();
        issuesPage.labels().editSaveWait("Value1", "Value2");

        priority.waitToSave(tracer);

        assertTrue("Errors should remain for [summary]", issuesPage.getFieldsInError().contains("summary"));

        issuesPage.summary().editSaveWait("Valid Summary");

        assertEquals("Valid Summary", summary.getValue());
        assertFalse("Errors should NOT remain for [summary]", issuesPage.getFieldsInError().contains("summary"));
    }

    @Test
    public void testNewFieldsAreEditable()
    {
        IssueDetailComponent issuesPage = goToIssue("XSS-17");
        issuesPage.waitForEditButton().clickEditButton();

        // Setting the issue's due date through the quick edit dialog will
        // result in a full page refresh, making the due date appear.
        EditIssueDialog editIssueDialog = product.getPageBinder().bind(EditIssueDialog.class);
        editIssueDialog.fill("duedate", "31/Mar/2012");
        editIssueDialog.submitExpectingViewIssue("XSS-17");

        // The due date field should be inline-editable.
        issuesPage.dueDate().editSaveWait("01/Jan/12");
        assertEquals("01/Jan/12", issuesPage.dueDate().getValue());
    }

    @Test
    public void testPermissionDenied()
    {
        IssueDetailComponent issuesPage = goToIssue("XSS-17", "fred", "fred", "");
        String originalSummary = issuesPage.summary().getValue();
        backdoor.usersAndGroups().removeUserFromGroup("fred", "jira-developers");
        issuesPage.summary().editSaveWait("blah blah blah blah blah");
        issuesPage.waitForGlobalErrorMessage();
        waitUntilFalse(issuesPage.hasEditableFields());
        waitUntilFalse(issuesPage.hasDescriptionModule());
        assertEquals(issuesPage.summary().getValue(), originalSummary);
    }

    @Test
    public void testXSRFFailure()
    {
        IssueDetailComponent issuesPage = goToIssue("XSS-17");
        issuesPage.setXSRFTokenToJunk();
        Field priority = issuesPage.priority().switchToEdit().fill("Blocker").save();
        Tracer tracer = traceContext.checkpoint();
        issuesPage.waitForXSRFRetryButton().clickXSRFRetryButton();
        priority.waitToSave(tracer);
        assertEquals("Blocker", issuesPage.priority().getValue());

        // The page's XSRF token should now be valid.
        issuesPage.priority().editSaveWait("Minor");
        assertEquals("Minor", issuesPage.priority().getValue());
    }

    @Test
    public void testSaveOnBlur()
    {
        IssueDetailComponent issuesPage = goToIssue("XSS-17");
        String originalPriority = issuesPage.priority().getValue();
        String priority = "Blocker".equals(originalPriority) ? "Minor" : "Blocker";
        Field field = issuesPage.priority().switchToEdit().fill(priority);
        Tracer tracer = traceContext.checkpoint();
        // Danger Will Robinson! This next statement will be a no-op unless your browser window has the focus
        issuesPage.clickInWhitespace();
        field.waitToSave(tracer);

        assertEquals(priority, issuesPage.priority().getValue());
    }

    /**
     * JRADEV-11544 Set field to invalid, start editing another field, ensure that second field has correct editHtml
     */
    @Test
    public void testSaveAfterValidationError()
    {
        IssueDetailComponent issuesPage = goToIssue("XSS-17");
        issuesPage.summary().editSaveWait("");
        assertTrue(issuesPage.getFieldsInError().contains("summary"));

        InlinePriorityField priorityField = issuesPage.priority();
        String originalPriority = priorityField.getValue();
        priorityField.switchToEdit();
        assertEquals(originalPriority, priorityField.singleSelect().getValue());
    }

    @Test
    public void testTwixieCanTwix()
    {
        IssueDetailComponent issuesPage = goToIssue("ARA-1", "issueKey = ARA-1");

        final boolean originalState = issuesPage.isDetailsTwixieOpen();
        final String origName = originalState ? "open" : "close";
        issuesPage.clickDetailsTwixie();
        assertEquals("Twixie won't twix from " + origName, !originalState, issuesPage.isDetailsTwixieOpen());
        issuesPage.clickDetailsTwixie();
        assertEquals("Twixie don't retwix back to " + origName, originalState, issuesPage.isDetailsTwixieOpen());
    }

    @Test
    public void testFocusShifterToCustomField()
    {
        // QA-19 has a custom field not in a tab
        final IssueDetailComponent issuesPage = goToIssue("QA-19", "issueKey = QA-19");
        final int champId = 10110;
        final InlineTextCustomField champField = issuesPage.getCustomTextField(champId);
        final ShifterDialog shifter = issuesPage.openFocusShifter();
        shifter.queryAndSelect("champ");

        assertTrue("Champ de text field should be visible after the focus shifter chose it", champField.isVisible());
    }

    @Test
    public void testFocusShifterOpensTwixieAndTabToRevealField()
    {
        // ARA-1 has two (non-user, non-date) custom fields with values on different tabs
        final IssueDetailComponent issuesPage = goToIssue("ARA-1", "issueKey = ARA-1");
        final int champId = 10110;
        final InlineTextCustomField champField = issuesPage.getCustomTextField(champId);

        // Close details twixie
        if (issuesPage.isDetailsTwixieOpen())
        {
            issuesPage.clickDetailsTwixie();
        }
        assertFalse(issuesPage.isDetailsTwixieOpen());
        assertFalse(champField.isVisible());

        // Use focus shifter
        final ShifterDialog shifter = issuesPage.openFocusShifter();
        shifter.queryAndSelect("champ");

        assertTrue("Details twixie should have been opened by the focus shifter", issuesPage.isDetailsTwixieOpen());
        assertTrue("Champ de text field should be visible after the focus shifter chose it", champField.isVisible());

        // we can edit the field now
        final String newValue = "new champ de texte value";
        champField.fillSaveWait(newValue);

        assertEquals("Oi! I just edited the Champ de texte field!!", newValue, champField.getValue());
        assertTrue("The details twixie should still be open", issuesPage.isDetailsTwixieOpen());
    }

    @Test
    public void testFocusShifterPrefillsLastSelection()
    {
        // ARA-1 has two (non-user, non-date) custom fields with values on different tabs
        final IssueDetailComponent issuesPage = goToIssue("ARA-1", "issueKey = ARA-1");

        ShifterDialog shifter = issuesPage.openFocusShifter();
        shifter.queryAndSelect("Champ");

        shifter = issuesPage.openFocusShifter();
        final String selected = shifter.getAutoComplete().getActiveSuggestion().getText().byDefaultTimeout();
        assertEquals("Expected pre-fill to contain my last selected value", "Champ de texte", selected);
    }

    @Test
    public void testAddComment()
    {
        IssueDetailComponent issueDetails = goToIssue("XSS-17");
        issueDetails.addCommentUsingFooterLink("SAVE ON BLUR FTW");
        assertEquals("SAVE ON BLUR FTW", issueDetails.waitForCommentWithId("10460"));
        //check focused class
        Poller.waitUntilTrue("new comment should be focused", issueDetails.commentIsFocused("10460"));
    }

    @Test
    public void testArchivedAffectsVersion()
    {
        Tracer tracer = traceContext.checkpoint();
        IssueDetailComponent issueDetails = goToIssue("XSS-17");
        assertEquals("Archived Version Shown", "Version A", issueDetails.affectsVersion().getValue());
        //saving nothing should still show archived version A. (Regression test for JRADEV-12638)
        issueDetails.affectsVersion().switchToEdit().save().waitToSave(tracer);
        assertEquals("Archived Version still shown", "Version A", issueDetails.affectsVersion().getValue());
        issueDetails.affectsVersion().editSaveWait("Version B");

        //In split view, there is a lack of horizontal screen estate,
        //so the values are being ellipsis-fy. In an attempt to be generic, the assert will check for
        //existence of "Version A" and "Version B" instead of full string.
        String affectsVersionText = issueDetails.affectsVersion().getValue();
        assertTrue("Version A (archived) and B as stored and displayed. Actual value: " + affectsVersionText,
                (affectsVersionText.contains("Version A") && affectsVersionText.contains("Version B")));
    }

    @Test
    public void testMentionsDropdownIsClosedOnESC()
    {
        IssueDetailComponent issueDetails = goToIssue("XSS-17");

        final InlineDescriptionField description = issueDetails.description();
        description.switchToEdit();
        description.type("this is a test @a");

        MentionsUserPicker mentions = description.mentions();

        //mentions.isOpen() is flakey, it is better to check there actually user suggestions there
        waitUntilTrue(mentions.hasSuggestion("moody"));

        description.type(Keys.ESCAPE);

        assertThat("Mentions drop down is closed", mentions.isOpen().now(), is(false));
        assertThat("Description field still has the focus", description.isVisible(), is(true));
    }

    /**
     * Navigates to the issue with the given key, first logging in as admin.
     */
    protected IssueDetailComponent goToIssue(final String issueKey)
    {
        return goToIssue(issueKey, "");
    }

    /**
     * Navigates to the issue with the given key, first logging in as admin.
     */
    protected IssueDetailComponent goToIssue(final String issueKey, String query)
    {
        return goToIssue(issueKey, "admin", "admin", query);
    }

    /**
     * Navigates to the issue with the given key, first logging in with the given username and password and, if it's
     * supported (ok if it's split view), starting in the context of a search using the given jql query.
     */
    protected abstract IssueDetailComponent goToIssue(String issueKey, String username, String password, String query);
}
