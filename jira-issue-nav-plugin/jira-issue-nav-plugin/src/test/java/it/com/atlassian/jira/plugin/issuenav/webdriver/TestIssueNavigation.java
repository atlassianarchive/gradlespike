package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.ResultsTableComponent;
import org.junit.Test;

import static org.junit.Assert.*;

@RestoreOnce("TestIssueNavigation.xml")
public class TestIssueNavigation extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;
    private TraceContext traceContext;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        issuesPage = logInToIssuesPageAsSysadmin().getListLayout();
        traceContext = product.getPageBinder().bind(TraceContext.class);
    }

    @Test
    public void testJKNavigation()
    {
        issuesPage.newSearch(true);
        ResultsTableComponent resultsTable = issuesPage.getResultsTable();

        resultsTable.next();
        assertEquals("PERU-1", resultsTable.getSelectedIssue().getSelectedIssueKey());

        resultsTable.prev();
        assertEquals("SUM-1", resultsTable.getSelectedIssue().getSelectedIssueKey());

        resultsTable.prev();
        assertEquals("SUM-1", resultsTable.getSelectedIssue().getSelectedIssueKey());

        resultsTable.next().next().next().next().next().next().next().next().next();
        assertEquals("FIJI-1", resultsTable.getSelectedIssue().getSelectedIssueKey());

        // Across page boundaries
        Tracer tracer = traceContext.checkpoint();
        resultsTable.next();
        issuesPage.waitForResults(tracer);
        assertEquals("BALI-2", resultsTable.getSelectedIssue().getSelectedIssueKey());
        assertCurrentURL("/issues/?jql=&startIndex=10");

        tracer = traceContext.checkpoint();
        resultsTable.prev();
        issuesPage.waitForResults(tracer);
        assertEquals("FIJI-1", resultsTable.getSelectedIssue().getSelectedIssueKey());
        assertCurrentURL("/issues/?jql=");

        // JRADEV-19012 Pressing the browser's back button should update the issue table accordingly.
        issuesPage.backAndWait(true, false);
        assertEquals("BALI-2", resultsTable.getSelectedIssue().getSelectedIssueKey());
        assertCurrentURL("/issues/?jql=&startIndex=10");
    }
}