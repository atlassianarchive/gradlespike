package it.com.atlassian.jira.plugin.issuenav.webdriver;

import com.atlassian.jira.nimblefunctests.annotation.Restore;
import com.atlassian.jira.plugin.issuenav.pageobjects.ShareDialog;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.filter.EditFilterPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.filter.FindFiltersPage;
import junit.framework.Assert;
import org.hamcrest.Matcher;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

@Restore("TestSharingFilters.xml")
public class TestSharingFilters extends KickassWebDriverTestCase
{
    @Test
    public void testFilterPermissions()
    {
        // set the filters permissions to jira-users, login as hitgirl and make the filter favourite
        IssuesPage issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout()
                .getBasicQuery().project().setValueAndSubmit("Bali").getFilters().createFilter("Bali Admin's Filter");
        assertEquals("Bali Admin's Filter", issuesPage.getFilters().getSearchTitle());

        EditFilterPage editFilter = issuesPage.getFilters().getFilterDetailsMenu().editPermissions();
        issuesPage = editFilter.setSharingGroupAndSubmit("jira-users");
        assertThat(issuesPage.getFilters().getFilterDetailsMenu().getPermissions(), contains("Group: jira-users"));

        issuesPage = logInToIssuesPage("hitgirl", "hitgirl");
        FindFiltersPage findFiltersPage = issuesPage.getFilters().getFindFiltersPage();
        waitUntil("Finds Admin's filter", findFiltersPage.searchAndGetFiltersName("bali").timed().getText(),
                (Matcher<String>) equalTo("Bali Admin's Filter")
        );

        findFiltersPage.searchAndMakeFavourite("bali");

        assertThat(jqlSearch("").getFilters().getFavouriteFilters(), contains("Bali Admin's Filter"));

        // login as admin, remove the permissions to filter from jira-users, login as hitgirl, verify the filter
        // don't appear in favourites any more
        logInToIssuesPageAsSysadmin()
            .getFilters()
            .getFilterDetailsMenu()
            .editPermissions()
            .removeTheOnlyShareAndSubmit();
        assertFalse(logInToIssuesPage("hitgirl", "hitgirl").getFilters().areFavouriteFiltersPresent());
    }

    @Test
    public void testSharingFilter()
    {
        logInToIssuesPageAsSysadmin().getListLayout().getFilters().selectSystem("Reported by Me");
        ShareDialog dialog = product.getPageBinder().bind(ShareDialog.class);
        dialog.open().addRecipient("hitgirl");
        assertThat(dialog.getRecipients(), contains("hitgirl"));
        dialog.submit();

        // hitgirl is in jira-developers group, she can browse users and share the filter
        logInToIssuesPage("hitgirl", "hitgirl").getFilters().selectSystem("Reported by Me");
        dialog = product.getPageBinder().bind(ShareDialog.class).open().addRecipient("admin").addRecipient("fred");
        assertThat(dialog.getRecipients(), containsInAnyOrder("admin", "fred"));
        dialog.submit();
    }

    @Test
    public void testSharingFilterNoBrowsePermissions()
    {
        // fred is in jira-users group, he can't browse users and therefore he can't share the filter
        logInToIssuesPage("fred", "fred").getListLayout().getFilters().selectSystem("Reported by Me");
        ShareDialog dialog = product.getPageBinder().bind(ShareDialog.class);
        assertFalse(dialog.isTriggerPresent());
    }

    @Test
    public void testShareDialogPersistsDataOnHide()
    {
        IssuesPage issuesPage = logInToIssuesPageAsSysadmin().getListLayout().getFilters().selectSystem("Reported by Me");
        ShareDialog shareDialog = issuesPage.getShareDialog().open();
        shareDialog.addNote("note");
        shareDialog.closeBySelectingOutside();
        Assert.assertEquals("Should have persisted note on hide", "note", shareDialog.open().getNote());
    }

    @Test
    public void testShareDialogNoPersistOnCancel()
    {
        IssuesPage issuesPage = logInToIssuesPageAsSysadmin().getListLayout().getFilters().selectSystem("Reported by Me");
        ShareDialog shareDialog = issuesPage.getShareDialog().open();
        shareDialog.addNote("note");
        shareDialog.closeByCancelButton();
        Assert.assertEquals("Should have not persisted note on hide", "", shareDialog.open().getNote());
    }

    @Test
    public void testShareDialogNoPersistOnSubmit()
    {
        IssuesPage issuesPage = logInToIssuesPageAsSysadmin().getListLayout().getFilters().selectSystem("Reported by Me");
        ShareDialog shareDialog = issuesPage.getShareDialog().open();
        shareDialog.addRecipient("hitgirl");
        shareDialog.submit();
        assertFalse("Should have not persisted recipients", shareDialog.open().getRecipients().contains("hitgirl"));
    }
}
