package it.com.atlassian.jira.plugin.issuenav.func;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.jira.nimblefunctests.framework.NimbleFuncTestCase;
import com.atlassian.jira.plugin.issuenav.client.PreferredSearchLayoutClient;
import com.atlassian.jira.plugin.issuenav.util.PreferredSearchLayoutService;
import com.atlassian.jira.webtests.table.HtmlTable;
import org.junit.Test;

import static org.junit.Assert.*;

@WebTest({ Category.FUNC_TEST })
@RestoreOnce("TestIssueTableColumnVisibility.xml")
public class TestIssueTableColumnVisibility extends NimbleFuncTestCase
{
    @Override
    public void beforeMethod()
    {
        super.beforeMethod();

        // The issue table is only server rendered if the user's preferred layout is list view.
        PreferredSearchLayoutClient preferredSearchLayoutClient = new PreferredSearchLayoutClient(environmentData);
        preferredSearchLayoutClient.setPreferredSearchLayout(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
    }

    @Test
    public void testAllProjectsShowsHiddenFieldAtEnd()
    {
        navigation.gotoPage("/issues/?jql=");
        tester.assertElementPresent("issuetable");
        HtmlTable table = page.getHtmlTable("issuetable");
        final boolean dateInUserColumnLayoutPosition = table.doesCellHaveText(0, 10, "DatePickerCF");
        final boolean dateInFilterColumnLayoutPosition = table.doesCellHaveText(0, 3, "DatePickerCF");
        if (dateInFilterColumnLayoutPosition)
        {
            fail("Seem to be using the column position for a filter, expecting the user's column layout");
        } else {
            String message = "Expected the date to be in column position 10 as per user column layout configuration";
            assertTrue(message, dateInUserColumnLayoutPosition);
        }
    }

    @Test
    public void testProjectAAShowsHiddenFieldAtEnd()
    {
        navigation.gotoPage("/issues/?jql=project=AA");
        tester.assertElementPresent("issuetable");
        HtmlTable table = page.getHtmlTable("issuetable");
        assertTrue(table.doesCellHaveText(0, 10, "DatePickerCF"));
    }

    @Test
    public void testProjectBBDoesntShowHiddenField()
    {
        navigation.gotoPage("/issues/?jql=project=BB");
        tester.assertElementPresent("issuetable");
        assertTrue(locator.css(".headerrow-customfield_10001").getNodes().length == 0);
    }

    @Test
    public void testFilterAAShowsHiddenFieldInCustomLocation()
    {
        navigation.gotoPage("/issues/?filter=10001");
        tester.assertElementPresent("issuetable");
        HtmlTable table = page.getHtmlTable("issuetable");
        assertTrue(table.doesCellHaveText(0, 3, "DatePickerCF"));
    }

    @Test
    public void testFilterBBDoesntShowHiddenField()
    {
        navigation.gotoPage("/issues/?filter=10000");
        tester.assertElementPresent("issuetable");
        assertTrue(locator.css(".headerrow-customfield_10001").getNodes().length == 0);
    }
}
