package it.com.atlassian.jira.plugin.issuenav.webdriver.issueactions;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.elements.GlobalMessage;
import com.atlassian.jira.pageobjects.navigator.SelectedIssue;
import com.atlassian.jira.pageobjects.pages.viewissue.ActionTrigger;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.query.Poller;
import it.com.atlassian.jira.plugin.issuenav.webdriver.KickassWebDriverTestCase;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.Arrays;

@WebTest ({ Category.WEBDRIVER_TEST, Category.ISSUES })
public class TestLabelIssue extends KickassWebDriverTestCase
{
    private IssuesPage issuesPage;
    private PageBinder pageBinder;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestIssueActions.xml");
        issuesPage = logInToIssuesPageAsSysadmin("?jql=").getListLayout();
        pageBinder = product.getPageBinder();
    }

    @Test
    public void testFromIssueActionsDialog()
    {
        perform(ActionTrigger.ACTIONS_DIALOG);
    }

    @Test
    public void testFromKeyboardShortcut()
    {
        perform(ActionTrigger.KEYBOARD_SHORTCUT);
    }

    @Test
    public void testFromCog()
    {
        perform(ActionTrigger.MENU);
    }

    private void perform(ActionTrigger actionTrigger)
    {
        final SelectedIssue selectedIssue = issuesPage.getResultsTable().getSelectedIssue();
        selectedIssue.addLabels(Arrays.asList("one", "two"), actionTrigger);
        Poller.waitUntil(pageBinder.bind(GlobalMessage.class).getMessage(), Matchers.containsString("The labels on SUM-1 have been updated."));
    }
}