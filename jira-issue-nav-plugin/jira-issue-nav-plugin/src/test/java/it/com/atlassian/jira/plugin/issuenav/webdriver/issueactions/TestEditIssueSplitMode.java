package it.com.atlassian.jira.plugin.issuenav.webdriver.issueactions;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.elements.GlobalMessage;
import com.atlassian.jira.pageobjects.pages.viewissue.ActionTrigger;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.SplitLayout;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.SplitViewSelectedIssue;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.query.Poller;
import it.com.atlassian.jira.plugin.issuenav.webdriver.KickassWebDriverTestCase;
import org.hamcrest.Matchers;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.HashMap;
import java.util.Map;

@WebTest ({ Category.WEBDRIVER_TEST, Category.ISSUES })
public class TestEditIssueSplitMode extends KickassWebDriverTestCase
{
    String key = "SUM-1";

    private IssuesPage issuesPage;
    private TraceContext traceContext;
    private PageBinder pageBinder;
    private SplitLayout splitLayout;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestIssueActions.xml");

        pageBinder = product.getPageBinder();
        traceContext = pageBinder.bind(TraceContext.class);
        Tracer tracer = traceContext.checkpoint();

        issuesPage = logInToIssuesPageAsSysadmin("?jql=");
        splitLayout = issuesPage.getSplitLayout(tracer);
    }

    @Test
    public void testFromIssueActionsDialog()
    {
        perform(ActionTrigger.ACTIONS_DIALOG);
    }

    @Test
    public void testFromKeyboardShortcut()
    {
        perform(ActionTrigger.KEYBOARD_SHORTCUT);
    }

    @Test
    public void testFromMenu()
    {
        perform(ActionTrigger.MENU);
    }

    private void perform(ActionTrigger actionTrigger)
    {
        final SplitViewSelectedIssue selectedIssue = splitLayout.getSelectedIssue();
        Map<String, String> vals = new HashMap<String, String>();
        vals.put("summary", "Changed Summary");
        selectedIssue.editIssue(vals, actionTrigger);
        Poller.waitUntil(pageBinder.bind(GlobalMessage.class).getMessage(), Matchers.containsString(key + " has been updated."));
        assertEquals(selectedIssue.getSummary(), "Changed Summary");
        IssueDetailComponent issue = pageBinder.bind(IssueDetailComponent.class, "SUM-1");
        assertEquals(issue.getSummary(), "Changed Summary");

    }
}
