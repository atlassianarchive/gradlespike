package it.com.atlassian.jira.plugin.issuenav.func;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.plugin.issuenav.client.SearchRendererClient;
import com.atlassian.jira.plugin.issuenav.client.SearchRendererValue;
import com.atlassian.jira.plugin.issuenav.service.SearchRendererValueResults;
import com.atlassian.jira.plugin.issuenav.util.EditHtmlUtils;
import com.atlassian.jira.testkit.client.restclient.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

/**
 * Test security and response for the search renderer resource
 *
 * @since v5.1
 */
@WebTest ({ Category.FUNC_TEST })
public class TestSearchRendererCustomFields extends FuncTestCase
{
    private static final String SEARCHER_NAME = "<iframe src=\\\"http://www.atlassian.com\\\"></iframe>";

    private SearchRendererClient client;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        client = new SearchRendererClient(getEnvironmentData());
        administration.restoreData("TestClauseLozenges.xml");
    }

    /**
     * Tests value response form custom fields
     */
    @Test
    public void testValueCustomFields()
    {
        SearchRendererValueResults values = client.getValue("customfield_10110", "i'm a barbie girl, in a barbie world.");

        assertEquals(1, values.size());

        SearchRendererValue customField = values.get("customfield_10110");
        assertEquals("\"Champ de texte\" ~ \"i'm a barbie girl, in a barbie world.\"", customField.jql);
        assertNotNull(customField);
        assertTrue(customField.validSearcher);
        EditHtmlUtils.assertTextFieldValues(customField.editHtml, "customfield_10110", "i'm a barbie girl, in a barbie world.");
    }

    /**
     * Tests value response form custom fields with a context
     */
    @Test
    public void testValueCustomFieldsWithContext()
    {
        SearchRendererValueResults values = client.getValue("pid", "10010", "customfield_10071", "i'm a barbie girl, in a barbie world.");

        assertEquals(2, values.size());

        SearchRendererValue customField = values.get("customfield_10071");
        assertNotNull(customField);
        assertTrue(customField.validSearcher);
        assertEquals("\"" + SEARCHER_NAME + "\" ~ \"i'm a barbie girl, in a barbie world.\"", customField.jql);
        assertNotNull(customField.editHtml);
        EditHtmlUtils.assertTextFieldValues(customField.editHtml, "customfield_10071", "i'm a barbie girl, in a barbie world.");
    }

    @Test
    public void testValueCustomFieldsFromJqlWhenInvalid()
    {
        SearchRendererValueResults values = client.getValue("__jql_customfield_10071", "\"" + SEARCHER_NAME + "\" ~ \"i'm a barbie girl, in a barbie world.\"");

        assertEquals(1, values.size());

        SearchRendererValue customField = values.get("customfield_10071");
        assertNotNull(customField);
        assertFalse(customField.validSearcher);
        assertEquals("\"" + SEARCHER_NAME + "\" ~ \"i'm a barbie girl, in a barbie world.\"", customField.jql);
        EditHtmlUtils.assertTextFieldValues(customField.editHtml, "customfield_10071", "i'm a barbie girl, in a barbie world.");
    }

    @Test
    public void testValueCustomFieldsFromJqlWhenValid()
    {
        SearchRendererValueResults values = client.getValue("pid", "10010", "__jql_customfield_10071", "\"" + SEARCHER_NAME + "\" ~ \"i'm a barbie girl, in a barbie world.\"");

        assertEquals(2, values.size());

        SearchRendererValue customField = values.get("customfield_10071");
        assertNotNull(customField);
        assertTrue(customField.validSearcher);
        assertEquals("\"" + SEARCHER_NAME + "\" ~ \"i'm a barbie girl, in a barbie world.\"", customField.jql);
        EditHtmlUtils.assertTextFieldValues(customField.editHtml, "customfield_10071", "i'm a barbie girl, in a barbie world.");
    }

    @Test
    public void testValueCustomFieldsFromInvalidJql()
    {
        Response response = client.getValueResponse("__jql_customfield_10071", "but this is not jql!");

        assertEquals(400, response.statusCode);
        assertEquals(1, response.entity.errorMessages.size());
    }

    @Test
    public void testIssueTypeContext()
    {
        // valid - issue type only
        SearchRendererValueResults values = client.getValue("type", "1");

        assertEquals(1, values.size());
        SearchRendererValue issueTypeField = values.get("issuetype");

        Document viewNode = Jsoup.parse(issueTypeField.viewHtml);
        assertEquals("Type:", viewNode.select(".fieldLabel").text());
        assertEquals("Bug", viewNode.select(".fieldValue").text());
        assertEquals(0, viewNode.select(".invalid_sel").size());

        Document editNode = Jsoup.parse(issueTypeField.editHtml);
        assertEquals("Bug", editNode.select("option[selected]").text());
        assertEquals(0, editNode.select(".invalid_sel").size());

        // issue type invalid for project
        values = client.getValue("pid", "10040", "type", "1");

        assertEquals(2, values.size());
        issueTypeField = values.get("issuetype");

        viewNode = Jsoup.parse(issueTypeField.viewHtml);
        assertEquals("Type:", viewNode.select(".fieldLabel").text());
        assertEquals("Bug", viewNode.select(".fieldValue").text());
        assertEquals(1, viewNode.select(".invalid_sel").size());
        assertEquals("Type 'Bug' is not valid for your current Project(s) selection", viewNode.select(".invalid_sel").attr("title"));

        editNode = Jsoup.parse(issueTypeField.editHtml);
        assertEquals("Bug", editNode.select("option[selected]").text());
        assertEquals(1, editNode.select(".invalid_sel").size());
        assertEquals("Type 'Bug' is not valid for your current Project(s) selection", editNode.select(".invalid_sel").attr("title"));
    }

    @Test
    public void testStatusContext()
    {
        // valid - status only
        SearchRendererValueResults values = client.getValue("status", "10000");

        assertEquals(1, values.size());
        SearchRendererValue statusField = values.get("status");

        Document viewNode = Jsoup.parse(statusField.viewHtml);
        assertEquals("Status:", viewNode.select(".fieldLabel").text());
        assertEquals("1111", viewNode.select(".fieldValue").text());
        assertEquals(0, viewNode.select(".invalid_sel").size());

        Document editNode = Jsoup.parse(statusField.editHtml);
        assertEquals("1111", editNode.select("option[selected]").text());
        assertEquals(0, editNode.select(".invalid_sel").size());

        // status invalid for project
        values = client.getValue("pid", "10010", "status", "10000");

        assertEquals(2, values.size());
        statusField = values.get("status");

        viewNode = Jsoup.parse(statusField.viewHtml);
        assertEquals("Status:", viewNode.select(".fieldLabel").text());
        assertEquals("1111", viewNode.select(".fieldValue").text());
        assertEquals(1, viewNode.select(".invalid_sel").size());
        assertEquals("Status '1111' is not valid for your current Project(s) and/or Issue Type(s) selection", viewNode.select(".invalid_sel").attr("title"));

        editNode = Jsoup.parse(statusField.editHtml);
        assertEquals("1111", editNode.select("option[selected]").text());
        assertEquals(1, editNode.select(".invalid_sel").size());
        assertEquals("Status '1111' is not valid for your current Project(s) and/or Issue Type(s) selection", editNode.select(".invalid_sel").attr("title"));
    }
}
