package com.atlassian.jira.plugin.issuenav;

import com.atlassian.jira.components.query.util.DefaultUserSearchModeService;
import com.atlassian.jira.components.query.util.UserSearchModeService;
import com.atlassian.core.AtlassianCoreException;
import com.atlassian.core.user.preferences.Preferences;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.util.velocity.VelocityRequestSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

/**
 * Tests for {@link com.atlassian.jira.components.query.util.DefaultUserSearchModeService}.
 *
 * @since v5.2
 */
@RunWith(MockitoJUnitRunner.class)
public class TestUserSearchModeService
{
    @Mock
    JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    Preferences preferences;
    @Mock
    VelocityRequestSession session;
    @Mock
    UserPreferencesManager userPreferencesManager;
    private UserSearchModeService userSearchModeService;
    @Mock
    VelocityRequestContextFactory requestContextFactory;

    @Before
    public void setUp()
    {
        User user = mock(User.class);
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(user);

        when(userPreferencesManager.getPreferences(any(User.class)))
                .thenReturn(preferences);

        VelocityRequestContext requestContext = mock(VelocityRequestContext.class);
        when(requestContext.getSession()).thenReturn(session);
        when(requestContextFactory.getJiraVelocityRequestContext())
                .thenReturn(requestContext);

        userSearchModeService = new DefaultUserSearchModeService(
                jiraAuthenticationContext, userPreferencesManager,
                requestContextFactory);
    }

    @Test
    public void testGetSearchModeDefault()
    {
        assertEquals(UserSearchModeService.BASIC,
                userSearchModeService.getSearchMode());
    }

    @Test
    public void testGetSearchModePreferences()
    {
        when(preferences.getString(anyString())).thenReturn(
                UserSearchModeService.ADVANCED);

        assertEquals(UserSearchModeService.ADVANCED,
                userSearchModeService.getSearchMode());
    }

    @Test
    public void testGetSearchModeSession()
    {
        when(session.getAttribute(anyString())).thenReturn(
                UserSearchModeService.ADVANCED);
        when(preferences.getString(anyString())).thenReturn(
                UserSearchModeService.BASIC);

        assertEquals(UserSearchModeService.ADVANCED,
                userSearchModeService.getSearchMode());
    }

    @Test
    public void testSetSearchMode() throws AtlassianCoreException
    {
        String searchMode = UserSearchModeService.BASIC;
        userSearchModeService.setSearchMode(searchMode);

        verify(preferences).setString("user.search.mode", searchMode);
        verify(session).setAttribute("user.search.mode", searchMode);
    }

    @Test
    public void testSetSearchModeAnonymous() throws AtlassianCoreException
    {
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(null);

        String searchMode = UserSearchModeService.ADVANCED;
        userSearchModeService.setSearchMode(searchMode);

        verify(preferences, never()).setString(anyString(), anyString());
        verify(session).setAttribute("user.search.mode", searchMode);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetSearchModeInvalid()
    {
        userSearchModeService.setSearchMode("That's a paddlin'.");
    }
}