package com.atlassian.jira.plugin.issuenav;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.components.issueviewer.service.SystemFilter;
import com.atlassian.jira.plugin.issuenav.util.DefaultSearchUtil;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v5.2
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDefaultSearchUtil
{
    // Override the query methods to avoid calling out to ComponentAccessor.
    private static class TestableDefaultSearchUtil extends DefaultSearchUtil
    {
        private TestableDefaultSearchUtil(
                final JiraAuthenticationContext jiraAuthenticationContext,
                final SearchProvider searchProvider,
                final UserProjectHistoryManager userProjectHistoryManager)
        {
            super(jiraAuthenticationContext, searchProvider,
                    userProjectHistoryManager);
        }

        @Override
        protected Query getOpenIssuesQuery()
        {
            return new QueryImpl();
        }

        @Override
        protected Query getProjectQuery(Project project)
        {
            return new QueryImpl();
        }
    }

    private DefaultSearchUtil defaultSearchUtil;
    @Mock private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock private SearchProvider searchProvider;
    @Mock private User user;
    @Mock private UserProjectHistoryManager userProjectHistoryManager;

    @Before
    public void setUp()
    {
        when(user.getName()).thenReturn("fred");
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(user);

        defaultSearchUtil = new TestableDefaultSearchUtil(
                jiraAuthenticationContext, searchProvider,
                userProjectHistoryManager);
    }

    @Test
    public void testGetRedirectURLOpenIssues() throws SearchException
    {
        when(searchProvider.searchCount(any(Query.class), any(User.class)))
                .thenReturn(1L);

        Search search = defaultSearchUtil.getSearch();
        assertEquals(SystemFilter.MY_OPEN_ISSUES.getId(), search.getFilter());
        assertNull(search.getJql());
        assertEquals(0, search.getStartIndex());
    }

    @Test
    public void testGetRedirectURLCurrentProject() throws SearchException
    {
        when(searchProvider.searchCount(any(Query.class), any(User.class)))
                .thenReturn(0L, 1L);

        Project project = mock(Project.class);
        when(project.getKey()).thenReturn("ABC");
        when(userProjectHistoryManager.getCurrentProject(anyInt(),
                any(User.class))).thenReturn(project);

        Search search = defaultSearchUtil.getSearch();
        assertNull(search.getFilter());
        assertEquals("project = ABC", search.getJql());
        assertEquals(0, search.getStartIndex());
    }

    @Test
    public void testGetRedirectURLEmptyCurrentProject() throws SearchException
    {
        when(searchProvider.searchCount(any(Query.class), any(User.class)))
                .thenReturn(0L);

        Project project = mock(Project.class);
        when(project.getKey()).thenReturn("ABC");
        when(userProjectHistoryManager.getCurrentProject(anyInt(),
                any(User.class))).thenReturn(project);

        Search search = defaultSearchUtil.getSearch();
        assertEquals(SystemFilter.ALL_ISSUES.getId(), search.getFilter());
        assertEquals(null, search.getJql());
        assertEquals(0, search.getStartIndex());
    }

    @Test
    public void testGetRedirectURLNoCurrentProject() throws SearchException
    {
        when(searchProvider.searchCount(any(Query.class), any(User.class)))
            .thenReturn(0L);

        when(userProjectHistoryManager.getCurrentProject(anyInt(),
                any(User.class))).thenReturn(null);

        Search search = defaultSearchUtil.getSearch();
        assertEquals(SystemFilter.ALL_ISSUES.getId(), search.getFilter());
        assertEquals(null, search.getJql());
        assertEquals(0, search.getStartIndex());
    }
}