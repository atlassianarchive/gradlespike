package com.atlassian.jira.plugin.issuenav;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.jql.util.JqlStringSupport;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Collection;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestIssueNavAction
{
    @Mock
    JqlStringSupport jqlStringSupport;
    @Mock
    PagerFilter<?> pagerFilter;

    @Mock
    UserManager userManager;

    @Before
    public void setUp()
    {
        when(userManager.getUserByName("")).thenReturn(null);

        ComponentAccessor.initialiseWorker(new MockComponentWorker()
                .addMock(UserManager.class, userManager)
        );
    }

    @Test
    public void testSessionSearchEmpty()
    {
        SearchRequest searchRequest = new SearchRequest();
        Search search = new Search(searchRequest, jqlStringSupport, null);

        assertEquals("", search.toQueryString());

        when(pagerFilter.getStart()).thenReturn(0);
        search = new Search(searchRequest, jqlStringSupport, pagerFilter);
        assertEquals("", search.toQueryString());

        when(pagerFilter.getStart()).thenReturn(50);
        search = new Search(searchRequest, jqlStringSupport, pagerFilter);
        assertEquals("?startIndex=50", search.toQueryString());
    }

    @Test
    public void testSessionSearchEmptyJql()
    {
        Query query = new QueryImpl();
        SearchRequest searchRequest = new SearchRequest(query);
        when(jqlStringSupport.generateJqlString(any(Query.class))).thenReturn("");
        Search search = new Search(searchRequest, jqlStringSupport, null);

        assertEquals("?jql", search.toQueryString());

        when(pagerFilter.getStart()).thenReturn(50);
        search = new Search(searchRequest, jqlStringSupport, pagerFilter);
        assertEquals("?jql&startIndex=50", search.toQueryString());
    }

    @Test
    public void testSessionSearchJql()
    {
        Query query = new QueryImpl();
        SearchRequest searchRequest = new SearchRequest(query);
        when(jqlStringSupport.generateJqlString(any(Query.class))).thenReturn("blah");
        Search search = new Search(searchRequest, jqlStringSupport, null);

        assertEquals("?jql=blah", search.toQueryString());

        when(pagerFilter.getStart()).thenReturn(50);
        search = new Search(searchRequest, jqlStringSupport, pagerFilter);
        assertEquals("?jql=blah&startIndex=50", search.toQueryString());
    }

    @Test
    public void testSessionSearchFilter()
    {
        Query query = new QueryImpl();
        SearchRequest searchRequest = new SearchRequest(query, "", "", "", 666L, 0);
        when(jqlStringSupport.generateJqlString(any(Query.class))).thenReturn("blah");
        Search search = new Search(searchRequest, jqlStringSupport, null);

        assertEquals("?filter=666", search.toQueryString());

        when(pagerFilter.getStart()).thenReturn(50);
        search = new Search(searchRequest, jqlStringSupport, pagerFilter);
        assertEquals("?filter=666&startIndex=50", search.toQueryString());
    }

    @Test
    public void testSessionSearchModifiedFilter()
    {
        Query query = new QueryImpl();
        SearchRequest searchRequest = new SearchRequest(query, "", "", "", 666L, 0);
        searchRequest.setModified(true);
        when(jqlStringSupport.generateJqlString(any(Query.class))).thenReturn("blah");
        Search search = new Search(searchRequest, jqlStringSupport, null);

        assertEquals("?filter=666&jql=blah", search.toQueryString());

        when(pagerFilter.getStart()).thenReturn(50);
        search = new Search(searchRequest, jqlStringSupport, pagerFilter);
        assertEquals("?filter=666&jql=blah&startIndex=50", search.toQueryString());
    }

    @Test
    public void testSessionSearchChanged()
    {
        String originalQuery = "SOME QUERY";
        String modifiedQuery = "A DIFFERENT QUERY";
        SearchRequest searchRequest = new SearchRequest(new QueryImpl(null, originalQuery));
        SearchRequest differentSearchRequest = new SearchRequest(new QueryImpl());

        IssueNavAction issueNavAction = newIssueNavAction(null);
        assertTrue(issueNavAction.sessionSearchChanged(null, ""));
        assertTrue(issueNavAction.sessionSearchChanged(searchRequest, null));

        issueNavAction = newIssueNavAction(searchRequest);
        assertFalse(issueNavAction.sessionSearchChanged(searchRequest, null));
        assertFalse(issueNavAction.sessionSearchChanged(null, originalQuery));
        assertTrue(issueNavAction.sessionSearchChanged(differentSearchRequest, null));
        assertTrue(issueNavAction.sessionSearchChanged(null, modifiedQuery));
    }

    private IssueNavAction newIssueNavAction(final SearchRequest searchRequest)
    {
        when(jqlStringSupport.generateJqlString(any(Query.class))).then(new Answer<String>()
        {
            @Override
            public String answer(InvocationOnMock invocationOnMock) throws Throwable
            {
                Query query = (Query) invocationOnMock.getArguments()[0];
                return null == query ? null : query.getQueryString();
            }
        });
        return new IssueNavAction(null, null, null, null, null, jqlStringSupport, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null)
        {
            @Override
            public SearchRequest getSearchRequest()
            {
                return searchRequest;
            }

            @Override
            public Collection<String> getErrorMessages()
            {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public Map<String, String> getErrors()
            {
                throw new UnsupportedOperationException("Not implemented");
            }
        };
    }
}

