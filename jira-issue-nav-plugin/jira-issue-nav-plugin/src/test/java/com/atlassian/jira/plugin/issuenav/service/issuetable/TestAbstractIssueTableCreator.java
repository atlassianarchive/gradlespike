package com.atlassian.jira.plugin.issuenav.service.issuetable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.components.orderby.OrderByUtil;
import com.atlassian.jira.components.util.SortJqlGenerator;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayout;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutManager;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutStorageException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.jira.issue.fields.layout.column.ColumnLayout.ColumnConfig;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link IssueTableCreator}.
 *
 * @since v60.
 */
@RunWith (MockitoJUnitRunner.class)
public class TestAbstractIssueTableCreator
{
    private static class TestableIssueTableCreator extends AbstractIssueTableCreator
    {
        private List<Issue> issues;

        public TestableIssueTableCreator(
                final ApplicationProperties applicationProperties,
                final ColumnLayoutManager columnLayoutManager,
                final IssueTableServiceConfiguration configuration,
                final boolean fromIssueIds,
                final IssueFactory issueFactory,
                final List<Long> issueIds,
                final SortJqlGenerator sortJqlGenerator,
                final Query query,
                final boolean returnIssueIds,
                final SearchHandlerManager searchHandlerManager,
                final SearchProvider searchProvider,
                final SearchProviderFactory searchProviderFactory,
                final SearchRequest searchRequest,
                final SearchService searchService,
                final User user,
                final FieldManager fieldManager,
                final OrderByUtil orderByUtil)
        {
            super(applicationProperties, columnLayoutManager, configuration, fromIssueIds, issueFactory, issueIds,
                    sortJqlGenerator, query, returnIssueIds, searchHandlerManager, searchProvider,
                    searchProviderFactory, searchRequest, searchService, user, fieldManager, orderByUtil);
        }

        public List<Issue> getIssues()
        {
            return issues;
        }

        @Override
        protected Object getTable()
        {
            issues = searchResults.getIssues();
            return null;
        }

        @Override
        JqlQueryBuilder getJqlQueryBuilder()
        {
            return mock(JqlQueryBuilder.class, RETURNS_DEEP_STUBS);
        }
    }

    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private ColumnLayoutManager columnLayoutManager;
    private IssueTableServiceConfiguration configuration;
    private TestableIssueTableCreator issueTableCreator;
    private Query query;
    @Mock
    private SearchProvider searchProvider;
    @Mock
    private SearchRequest searchRequest;
    @Mock
    private SearchService searchService;
    @Mock
    private User user;
    @Mock
    private FieldManager fieldManager;
    @Mock
    private DisplayedColumnsHelper searchColumnsFinder;
    @Mock
    private SortJqlGenerator sortJqlGenerator;
    @Mock
    SearchHandlerManager searchHandlerManager;
    @Mock
    JqlQueryParser parser;
    @Mock
    UserManager userManager;
    @Mock
    OrderByUtil orderByUtil;


    @Before
    public void setUp() throws ColumnLayoutStorageException
    {
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_STABLE_SEARCH_MAX_RESULTS)).thenReturn("1000");

        configuration = new IssueTableServiceConfiguration();
        issueTableCreator = null;
        query = new QueryImpl();
    }

    /**
     * When we create an issue table from issue IDs, the issues should be returned in the same order as the IDs.
     * <p/>
     * This isn't a "public" interface; we're just making assertions about the data exposed to subclasses.
     */
    @Test
    public void testIdOrderMaintained() throws Exception
    {
        ColumnLayout columnLayout = mock(ColumnLayout.class);
        when(columnLayout.getColumnConfig()).thenReturn(ColumnConfig.FILTER);
        when(searchColumnsFinder.getDisplayedColumns(any(User.class), any(SearchRequest.class), any(IssueTableServiceConfiguration.class))).thenReturn(columnLayout);

        configuration.setColumnConfig(ColumnConfig.FILTER);
        final ArrayList<Issue> issueList = Lists.<Issue>newArrayList(new MockIssue(1L), new MockIssue(2L));
        final SearchResults results = new SearchResults(issueList, new PagerFilter<Issue>(0, 50));
        when(searchProvider.search(any(Query.class), eq(user), any(PagerFilter.class))).thenReturn(results);
        when(columnLayoutManager.getColumnLayout(any(User.class))).thenReturn(columnLayout);
        issueTableCreator = new TestableIssueTableCreator(applicationProperties, columnLayoutManager, configuration,
                true, null, Lists.newArrayList(2L, 1L), sortJqlGenerator, null, true, null, searchProvider, null, null,
                searchService, user, fieldManager, orderByUtil);

        issueTableCreator.create();

        // searchProvider returned 1, 2, but we requested 2, 1; subclasses should see the issues in the latter order.
        assertEquals(Lists.<Issue>newArrayList(new MockIssue(2L), new MockIssue(1L)), issueTableCreator.getIssues());
    }

    @Test
    public void testValidate()
    {
        final String errorMessage = "Something went horribly wrong.";
        MessageSet validationResult = new MessageSetImpl();
        validationResult.addErrorMessage(errorMessage);
        when(searchService.validateQuery(any(User.class), any(Query.class), any(Long.class))).thenReturn(validationResult);

        issueTableCreator = new TestableIssueTableCreator(applicationProperties, columnLayoutManager, configuration,
                false, null, null, sortJqlGenerator, query, true, null, searchProvider, null, searchRequest, searchService, user,
                fieldManager, orderByUtil);

        validationResult = issueTableCreator.validate();
        assertEquals(1, validationResult.getErrorMessages().size());
        assertEquals(errorMessage, validationResult.getErrorMessages().iterator().next());
        verify(searchService).validateQuery(user, query, 0L);
    }

    /**
     * We shouldn't validate {@link IssueTableCreator}s created with a list of issue IDs; we assume that they're valid.
     */
    @Test
    public void testValidateFromIssueIds()
    {
        issueTableCreator = new TestableIssueTableCreator(applicationProperties, columnLayoutManager, configuration,
                true, null, Lists.newArrayList(1L), sortJqlGenerator, null, true, null, searchProvider, null, null, searchService,
                user, fieldManager, orderByUtil);

        assertFalse(issueTableCreator.validate().hasAnyMessages());
        verify(searchService, never()).validateQuery(user, query);
    }
}
