package com.atlassian.jira.plugin.issuenav;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.plugin.issuenav.rest.IssueTableResource;
import com.atlassian.jira.components.issueviewer.service.SessionSearchService;
import com.atlassian.jira.plugin.issuenav.service.StableSearchService;
import com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTableService;
import com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTableServiceConfiguration;
import com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTableServiceOutcome;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.preferences.ExtendedPreferences;
import com.atlassian.jira.user.preferences.PreferenceKeys;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.order.OrderBy;
import com.atlassian.query.order.OrderByImpl;
import com.atlassian.query.order.SearchSort;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;
import java.util.Collections;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestIssueTableResource
{
    @Mock private JiraAuthenticationContext authenticationContext;
    private IssueTableResource issueTableResource;
    @Mock private IssueTableService issueTableService;
    @Mock private ExtendedPreferences preferences;
    @Mock private SearchRequestService searchRequestService;
    @Mock private SearchService searchService;
    @Mock private SessionSearchService sessionSearchService;
    @Mock private UserPreferencesManager userPreferencesManager;
    @Mock private FeatureManager featureManager;
    @Mock private StableSearchService stableSearchService;

    @Before
    public void setup()
    {
        issueTableResource = new IssueTableResource(authenticationContext, issueTableService,
                userPreferencesManager, searchRequestService, searchService, stableSearchService,
                sessionSearchService, userPreferencesManager);

        // Return an "empty" SearchRequest when one is requested.
        when(searchRequestService.getFilter(any(JiraServiceContext.class),
                anyLong())).thenReturn(new SearchRequest());
        when(userPreferencesManager.getExtendedPreferences(any(ApplicationUser.class))).thenReturn(preferences);
    }

    @Test
    public void testStartIndex()
    {
        when(authenticationContext.getUser()).thenReturn(null);
        when(preferences.getLong(PreferenceKeys.USER_ISSUES_PER_PAGE)).thenReturn(50L);
        when(issueTableService.getIssueTableFromFilterWithJql(any(String.class), any(String.class), any(IssueTableServiceConfiguration.class), any(Boolean.class)))
                .thenReturn(ServiceOutcomeImpl.ok(new IssueTableServiceOutcome(null, Collections.<String>emptyList())));

        // Test empty / zero start index
        issueTableResource.getIssueTableHtml(null, "", null, 0, null, "user");

        verify(sessionSearchService).setSessionSearch("", null, 0, 50);

        // Test actual start index
        issueTableResource.getIssueTableHtml(null, "", null, 2, null, "user");

        verify(sessionSearchService).setSessionSearch("", null, 2, 50);
    }

    @Test
    public void testJql()
    {
        when(authenticationContext.getUser()).thenReturn(null);
        when(preferences.getLong(PreferenceKeys.USER_ISSUES_PER_PAGE)).thenReturn(50L);
        when(issueTableService.getIssueTableFromFilterWithJql(any(String.class), any(String.class), any(IssueTableServiceConfiguration.class), any(Boolean.class)))
                .thenReturn(ServiceOutcomeImpl.ok(new IssueTableServiceOutcome(null, Collections.<String>emptyList())));

        // Test empty / zero start index
        issueTableResource.getIssueTableHtml(null, "project=management", null, 0, null, "user");

        verify(sessionSearchService).setSessionSearch("project=management", null, 0, 50);
    }

    @Test
    public void testFilter()
    {
        when(authenticationContext.getUser()).thenReturn(null);
        when(preferences.getLong(PreferenceKeys.USER_ISSUES_PER_PAGE)).thenReturn(50L);
        when(issueTableService.getIssueTableFromFilterWithJql(any(String.class), any(String.class), any(IssueTableServiceConfiguration.class), any(Boolean.class)))
                .thenReturn(ServiceOutcomeImpl.ok(new IssueTableServiceOutcome(null, Collections.<String>emptyList())));

        // Test no jql
        issueTableResource.getIssueTableHtml(1000L, null, null, 0, null, "user");

        verify(sessionSearchService).setSessionSearch(null, 1000L, 0, 50);

        // Test with jql
        issueTableResource.getIssueTableHtml(1000L, "project=management", null, 2, null, "user");

        verify(sessionSearchService).setSessionSearch("project=management", 1000L, 2, 50);

        // Test negative id (system filter)
        issueTableResource.getIssueTableHtml(-1L, null, null, 2, null, "user");

        verify(sessionSearchService).setSessionSearch(null, -1L, 2, 50);
    }

    /**
     * Setting just JQL should work.
     */
    @Test
    public void testSetSessionSearchJQL()
    {
        String JQL = "issuetype = Bug";
        Response response = issueTableResource.setSessionSearch(null, JQL);

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        verify(sessionSearchService).setSessionSearch(JQL, null, 0, 0);
    }

    /**
     * Setting just a filter ID should work.
     */
    @Test
    public void testSetSessionSearchFilterId()
    {
        Long filterId = 12345L;
        Response response = issueTableResource.setSessionSearch(filterId, null);

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        verify(sessionSearchService).setSessionSearch(null, filterId, 0, 0);
    }

    /**
     * 400 Bad Request should be returned if the given filter ID is invalid.
     */
    @Test
    public void testSetSessionSearchFilterIdInvalid()
    {
        // Return null when the filter is requested: invalid.
        when(searchRequestService.getFilter(any(JiraServiceContext.class), anyLong())).thenReturn(null);

        Response response = issueTableResource.setSessionSearch(12345L, null);
        assertEquals(BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    /**
     * Setting both a filter ID and JQL should work.
     */
    @Test
    public void testSetSessionSearchDirty()
    {
        OrderBy orderBy = new OrderByImpl(new SearchSort("created"));
        Query query = new QueryImpl(null, orderBy, null);
        when(searchService.parseQuery(any(User.class), anyString())).thenReturn(
                new SearchService.ParseResult(query, new MessageSetImpl()));

        Long filterId = 12345L;
        String JQL = "issuetype = Bug ORDER BY created";
        Response response = issueTableResource.setSessionSearch(filterId, JQL);

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        verify(sessionSearchService).setSessionSearch(JQL, filterId, 0, 0);
    }

    /**
     * When both a filter ID and JQL are provided, the JQL should be ignored if
     * it's the same as what's stored in the filter (isn't actually dirty).
     */
    @Test
    public void testSetSessionSearchNotDirty()
    {
        Query query = new QueryImpl(null, new OrderByImpl(), null);
        when(searchService.parseQuery(any(User.class), anyString())).thenReturn(
                new SearchService.ParseResult(query, new MessageSetImpl()));

        Long filterId = 12345L;
        Response response = issueTableResource.setSessionSearch(filterId, "issuetype = Bug");

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        verify(sessionSearchService).setSessionSearch(null, filterId, 0, 0);
    }

    /**
     * 400 Bad Request should be returned if both a filter ID and JQL are
     * provided and the JQL is invalid.
     */
    @Test
    public void testSetSessionSearchDirtyInvalidJQL()
    {
        MessageSet messageSet = new MessageSetImpl();
        messageSet.addErrorMessage("This is an error!");
        when(searchService.parseQuery(any(User.class), anyString())).thenReturn(
                new SearchService.ParseResult(null, messageSet));

        Response response = issueTableResource.setSessionSearch(12345L, "issuetype = Bug");

        assertEquals(BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    /**
     * The user's configured page size and start should be recorded.
     */
    @Test
    public void testSetSessionSearchPaging()
    {
        when(preferences.getLong(anyString())).thenReturn(50L);

        PagerFilter pagerFilter = mock(PagerFilter.class);
        when(pagerFilter.getStart()).thenReturn(50);
        when(sessionSearchService.getPagerFilter()).thenReturn(pagerFilter);

        Response response = issueTableResource.setSessionSearch(null, "");

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        verify(sessionSearchService).setSessionSearch("", null, 50, 50);
    }
}
