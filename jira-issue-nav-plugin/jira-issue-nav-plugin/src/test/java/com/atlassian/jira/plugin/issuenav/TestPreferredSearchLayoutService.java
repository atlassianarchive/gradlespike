package com.atlassian.jira.plugin.issuenav;

import com.atlassian.core.AtlassianCoreException;
import com.atlassian.core.user.preferences.Preferences;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.components.issueviewer.service.ActionUtilsService;
import com.atlassian.jira.plugin.issuenav.util.PreferredSearchLayoutService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.util.velocity.VelocityRequestSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link PreferredSearchLayoutService}.
 *
 * @since v6.0
 */
@RunWith (MockitoJUnitRunner.class)
public class TestPreferredSearchLayoutService
{
    @Mock private ActionUtilsService actionUtilsService;
    @Mock private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock private Preferences preferences;
    private PreferredSearchLayoutService preferredSearchLayoutService;
    @Mock private VelocityRequestContext velocityRequestContext;
    @Mock private VelocityRequestSession session;
    @Mock private UserPreferencesManager userPreferencesManager;
    @Mock private VelocityRequestContextFactory velocityRequestContextFactory;

    @Before
    public void setUp()
    {
        User user = mock(User.class);
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(user);
        when(jiraAuthenticationContext.isLoggedInUser()).thenReturn(true);
        when(userPreferencesManager.getPreferences(any(User.class))).thenReturn(preferences);
        when(velocityRequestContext.getSession()).thenReturn(session);
        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);

        preferredSearchLayoutService = new PreferredSearchLayoutService(
                actionUtilsService, jiraAuthenticationContext, userPreferencesManager, velocityRequestContextFactory);
    }

    /**
     * When the user doesn't have a preferred search layout set, the default should be returned.
     */
    @Test
    public void testGetPreferredSearchLayoutDefault()
    {
        assertThat("The default layout was returned", preferredSearchLayoutService.getPreferredSearchLayout(),
                is(PreferredSearchLayoutService.SPLIT_VIEW_LAYOUT));
    }

    /**
     * We should first attempt to retrieve the user's preferred layout from their preferences.
     */
    @Test
    public void testGetPreferredSearchLayoutPreference()
    {
        when(session.getAttribute(anyString())).thenReturn(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
        when(preferences.getString(anyString())).thenReturn(PreferredSearchLayoutService.SPLIT_VIEW_LAYOUT);
        assertThat("The user's preferred layout was read from their preferences",
                preferredSearchLayoutService.getPreferredSearchLayout(),
                is(PreferredSearchLayoutService.SPLIT_VIEW_LAYOUT));
    }

    /**
     * If the user's preferred layout isn't in their preferences, it should be retrieved from their session.
     */
    @Test
    public void testGetPreferredSearchLayoutSession()
    {
        when(session.getAttribute(anyString())).thenReturn(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
        when(preferences.getString(anyString())).thenReturn(PreferredSearchLayoutService.SPLIT_VIEW_LAYOUT);
        assertThat("The user's preferred layout was read from their session",
                preferredSearchLayoutService.getPreferredSearchLayout(),
                is(PreferredSearchLayoutService.SPLIT_VIEW_LAYOUT));
    }

    /**
     * Anonymous users' preferences should be stored in their session.
     */
    @Test
    public void testSetPreferredSearchLayoutAnonymous() throws AtlassianCoreException
    {
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(null);
        when(userPreferencesManager.getPreferences(any(User.class))).thenReturn(null);

        preferredSearchLayoutService.setPreferredSearchLayout(PreferredSearchLayoutService.SPLIT_VIEW_LAYOUT);
        verify(session).setAttribute(PreferredSearchLayoutService.PREFERRED_SEARCH_LAYOUT_KEY,
                PreferredSearchLayoutService.SPLIT_VIEW_LAYOUT);
    }

    /**
     * A user's preferred layout should be stored in both their preferences and session.
     */
    @Test
    public void testSetPreferredSearchLayout() throws AtlassianCoreException
    {
        preferredSearchLayoutService.setPreferredSearchLayout(PreferredSearchLayoutService.SPLIT_VIEW_LAYOUT);

        String key = PreferredSearchLayoutService.PREFERRED_SEARCH_LAYOUT_KEY;
        String value = PreferredSearchLayoutService.SPLIT_VIEW_LAYOUT;
        verify(preferences).setString(key, value);
        verify(session).setAttribute(key, value);
    }
}