package com.atlassian.jira.plugin.issuenav;

import junit.framework.Assert;
import org.junit.Test;

/**
 * Unit test for {@link LegacyTextQueryUrlRedirectFilter}
 *
 * @since v5.2
 */
public class TestLegacyTextQueryUrlRedirectFilter
{
    @Test
    public void testTransform() {
        LegacyTextQueryUrlRedirectFilter filter = new LegacyTextQueryUrlRedirectFilter();
        String newUrl = filter.transform("foobar.com/jira/IssueNavigator.jspa?this=irrelevantparam&query=this%20is%20a%20test&summary=true&body=true&another=something", "this%20is%20a%20test");
        Assert.assertEquals("foobar.com/jira/IssueNavigator.jspa?this=irrelevantparam&summary=this%20is%20a%20test&comment=this%20is%20a%20test&another=something", newUrl);

        newUrl = filter.transform("foobar.com/jira/IssueNavigator.jspa?query=thequery&description=true", "thequery");
        Assert.assertEquals("foobar.com/jira/IssueNavigator.jspa?description=thequery", newUrl);

        newUrl = filter.transform("/sr/jira.issueviews:searchrequest-xml/temp/SearchRequest.xml?decorator=none&reset=true&tempMax=1000&query=testignore&summary=true", "testignore");
        Assert.assertEquals("/sr/jira.issueviews:searchrequest-xml/temp/SearchRequest.xml?decorator=none&reset=true&tempMax=1000&summary=testignore", newUrl);
    }
}
