package com.atlassian.jira.plugin.issuenav;

import com.atlassian.crowd.model.user.User;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.components.query.util.UserSearchModeService;
import com.atlassian.jira.issue.search.MockJqlSearchRequest;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestFactory;
import com.atlassian.jira.issue.transport.ActionParams;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.security.JiraAuthenticationContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletResponse;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

/**
 * Test that redirects from the old to the new issue navigator work.
 * Based on <a href="https://jira.atlassian.com/browse/JRA-36281">JRA-36281</a>.
 */
@RunWith (MockitoJUnitRunner.class)
public class TestKickassRedirectFilter
{
    @Mock private JiraAuthenticationContext authenticationContext;
    @Mock private EventPublisher eventPublisher;
    @Mock private UserSearchModeService userSearchModeService;
    @Mock private SearchRequestFactory searchRequestFactory;
    @Mock private SearchService searchService;
    private KickassRedirectFilter filter;
    private MockHttpServletRequest request;
    @Mock private HttpServletResponse response;

    @Before
    public void setUp()
    {
        final MockComponentWorker componentAccessorWorker = new MockComponentWorker();
        componentAccessorWorker.getMockApplicationProperties().setEncoding("UTF-8");
        ComponentAccessor.initialiseWorker(componentAccessorWorker);

        filter = new KickassRedirectFilter(authenticationContext, eventPublisher, searchRequestFactory, searchService, userSearchModeService);

        request = new MockHttpServletRequest();
        request.setContextPath("");
        request.setServletPath("");
        request.setRequestURL("/secure/IssueNavigator.jspa");
    }

    @Test
    public void redirectWithNoParameters() throws Exception
    {
        SearchRequest sr = new MockJqlSearchRequest(null, null);
        when(searchRequestFactory.createFromParameters(any(SearchRequest.class), any(User.class), any(ActionParams.class))).thenReturn(sr);
        filter.doFilter(request, response, null);

        verify(response).sendRedirect("/issues/?redirectedFromClassic=");
    }

    @Test
    public void redirectWithNewQuery() throws Exception
    {
        request.setParameter("createNew", "true");

        filter.doFilter(request, response, null);

        verify(response).sendRedirect("/issues/?jql=");
    }

    @Test
    public void redirectWithFilter() throws Exception
    {
        request.setParameter("requestId", "10001");

        filter.doFilter(request, response, null);

        verify(response).sendRedirect("/issues/?filter=10001");
    }

    @Test
    public void redirectWithSimpleTextQuery() throws Exception
    {
        request.setParameter("searchString", "summary text");
        filter.doFilter(request, response, null);

        verify(response).sendRedirect("/issues/?jql=text%20%7E%20%22summary%20text%22");
    }

    @Test
    public void redirectWithSimpleJQLQuery() throws Exception
    {
        request.setParameter("jqlQuery", "project in (MKY)");
        filter.doFilter(request, response, null);

        verify(response).sendRedirect("/issues/?jql=project+in+%28MKY%29");
    }

    // JRA-36281
    @Test
    public void redirectWithJQLQueryWithTimestamp() throws Exception
    {
        request.setParameter("jqlQuery", "createdDate <= '2014-01-01 9:30'");
        filter.doFilter(request, response, null);

        verify(response).sendRedirect("/issues/?jql=createdDate+%3C%3D+%272014-01-01+9%3A30%27");
    }

    @Test
    public void newQueryParamWinsOverExistingQuery() throws Exception
    {
        request.setParameter("createNew", "true");
        request.setParameter("jqlQuery", "MKY-1");

        filter.doFilter(request, response, null);

        verify(response).sendRedirect("/issues/?jql=");
    }

    @Test
    public void startIndexPutLast() throws Exception
    {
        request.setParameter("pager/start", "123");
        request.setParameter("jqlQuery", "MKY-1");

        filter.doFilter(request, response, null);

        verify(response).sendRedirect("/issues/?jql=MKY-1&startIndex=123");
    }
}
