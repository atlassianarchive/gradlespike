package com.atlassian.jira.plugin.issuenav;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.search.managers.IssueSearcherManager;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.issue.search.searchers.IssueSearcher;
import com.atlassian.jira.issue.search.searchers.information.SearcherInformation;
import com.atlassian.jira.issue.search.searchers.renderer.SearchRenderer;
import com.atlassian.jira.components.query.SearchContextHelper;
import com.atlassian.jira.components.query.SearcherService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestSearcherService {
    @Mock
    private SearchContextHelper searchContextHelper;

    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

//    @Mock
//    private UserIssueSearcherHistoryManager userIssueSearcherHistoryManager;

    @Mock
    private SearchHandlerManager searchHandlerManager;

    @Mock
    private IssueSearcherManager issueSearcherManager;

    private SearcherService searcherService;
    private User user;

    @Before
//    public void setUp() {
//        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(new MockI18nHelper());
//        when(searchContextHelper.getSearchContextWithFieldValuesFromJqlString(any(String.class))).thenReturn(Mockito.mock(SearchContextHelper.SearchContextWithFieldValues.class));
//        this.user = jiraAuthenticationContext.getLoggedInUser();
//        this.searcherService = new DefaultSearcherService(searchContextHelper, issueSearcherManager, null, jiraAuthenticationContext, searchHandlerManager, null, null, userIssueSearcherHistoryManager);
//    }

    @Test
    public void aTest() {
        assertTrue(true);
    }

//    @Test
//    public void testGetEditHtml() {
//        String SEARCHER_ID = "";
//
//        IssueSearcher issueSearcher = createMockIssueSearcher(SEARCHER_ID);
//        when(issueSearcherManager.getSearcher(SEARCHER_ID)).thenReturn(issueSearcher);
//
//        verify(userIssueSearcherHistoryManager, never()).addIssueSearcherToHistory(user, issueSearcher);
//        ServiceOutcome<String> editHtmlOutcome = searcherService.getEditHtml(SEARCHER_ID, null, null);
//        assertThat(editHtmlOutcome.isValid(), equalTo(true));
//        verify(userIssueSearcherHistoryManager).addIssueSearcherToHistory(user, issueSearcher);
//    }
//
//    @Test
//    public void testGetSearchers() {
//        String RECENT_SEARCHER = "recent_searcher";
//        String NOT_RECENT_SEARCHER = "not_recent_searcher";
//
//        IssueSearcher<?> recentSearcher = createMockIssueSearcher(RECENT_SEARCHER);
//        IssueSearcher<?> notRecentSearcher = createMockIssueSearcher(NOT_RECENT_SEARCHER);
//        Collection<IssueSearcher<?>> searcherSet = ImmutableSet.of(recentSearcher, notRecentSearcher);
//
//        UserHistoryItem recentSearcherHistoryItem = new UserHistoryItem(UserHistoryItem.ISSUESEARCHER, RECENT_SEARCHER);
//        when(userIssueSearcherHistoryManager.getUserIssueSearcherHistory(user)).thenReturn(ImmutableList.of(recentSearcherHistoryItem));
//        when(searchHandlerManager.getAllSearchers()).thenReturn(searcherSet);
//
//        com.atlassian.jira.plugin.issuenav.service.Searchers searchers = searcherService.getSearchers(null);
//        List<com.atlassian.jira.plugin.issuenav.service.FilteredSearcherGroup> groups = searchers.getGroups();
//        FilteredSearcherGroup filteredSearcherGroup = Iterables.getOnlyElement(groups);
//        List<com.atlassian.jira.plugin.issuenav.service.Searcher> searchersList = filteredSearcherGroup.getSearchers();
//        assertThat(searchersList.size(), equalTo(searcherSet.size()));
//
//        for (Searcher searcher : searchersList) {
//            assertThat(searcher.getLastViewed(), equalTo(searcher.getId().equals(RECENT_SEARCHER) ? recentSearcherHistoryItem.getLastViewed() : null));
//        }
//    }

    private static IssueSearcher<?> createMockIssueSearcher(String searcherId) {
        IssueSearcher issueSearcher = Mockito.mock(IssueSearcher.class);
        SearchRenderer searchRenderer = Mockito.mock(SearchRenderer.class);
        SearcherInformation searcherInformation = Mockito.mock(SearcherInformation.class);
        when(issueSearcher.getSearchRenderer()).thenReturn(searchRenderer);
        when(issueSearcher.getSearchInformation()).thenReturn(searcherInformation);
        when(searcherInformation.getNameKey()).thenReturn(searcherId);
        when(searcherInformation.getId()).thenReturn(searcherId);
        return issueSearcher;
    }
}

