package com.atlassian.jira.plugin.issuenav.hallelujah;

import com.atlassian.buildeng.hallelujah.HallelujahServer;
import com.atlassian.buildeng.hallelujah.api.model.TestCaseName;
import com.atlassian.buildeng.hallelujah.core.JUnitUtils;
import com.atlassian.buildeng.hallelujah.jms.JMSHallelujahServer;
import com.atlassian.buildeng.hallelujah.listener.TestRetryingServerListener;
import com.atlassian.jira.webtests.util.ClassLocator;
import junit.framework.Test;
import junit.framework.TestSuite;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.TestClass;

import java.io.File;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class KickAssHallelujahServer
{
    public static void main(String[] args) throws Exception
    {
        int maxRetries = getMaxRetriesSetting();
        System.out.println("KickAss hallelujah server starting... Retries=" + maxRetries);

        HallelujahServer server = new JMSHallelujahServer.Builder()
                .setSuite(getTestSuite())
                .setTestResultFileName("Hallelujah.xml")
                .setSuiteName("KickAssTests")
                .setServerIdleTimeout(TimeUnit.MINUTES.toMillis(10))
                .build()
                .registerListeners(
                        new TestRetryingServerListener(maxRetries, new File("KA-flakyTest.txt"))
                );

        boolean success = server.call();
        if (!success)
        {
            System.err.println("KickAss Hallelujah Server has committed SUICIDE.");
            System.exit(-1);
        }

        System.out.println("KickAss Hallelujah Server finished.");
    }

    private static int getMaxRetriesSetting()
    {
        try
        {
            return Integer.parseInt(System.getProperty("hallelujah.retries"));
        }
        catch (NumberFormatException e)
        {
            // ignore
        }

        return 1;
    }

    /**
     * @return the test suite to run on Hallelujah.
     */
    private static TestSuite getTestSuite()
    {
        List<String> testNames = new ArrayList<String>();
        testNames.addAll(getTestNamesInPackage("com.atlassian.jira.plugin.issuenav"));
        testNames.addAll(getTestNamesInPackage("it.com.atlassian.jira.plugin.issuenav"));

        // Don't run visual regression tests. We won't have the baseline images available, so the tests will just fail.
        testNames.removeAll(getTestNamesInPackage("it.com.atlassian.jira.plugin.issuenav.webdriver.visualregressions"));

        List<Test> tests = getTestsFromClassNames(testNames);
        TestSuite testSuite = new TestSuite();
        for (Test test : tests)
        {
            testSuite.addTest(test);
        }

        return testSuite;
    }

    /**
     * Search a package for all test classes.
     *
     * @param packageName The name of the package
     * @return the names of all classes within a package.
     */
    private static List<String> getTestNamesInPackage(String packageName)
    {
        ClassLocator<Object> classLocator = ClassLocator.forAnyClass();
        classLocator.setPackage(packageName);

        List<String> names = new ArrayList<String>();
        for (Class<?> klass : classLocator.findClasses())
        {
            names.add(klass.getName());
        }
        return names;
    }

    /**
     * Extract all tests from classes with the given names.
     *
     * @param classNames The names of the classes from which tests should be extracted.
     * @return a list of {@link Test} objects in the package.
     */
    private static List<Test> getTestsFromClassNames(List<String> classNames)
    {
        List<Test> tests = new ArrayList<Test>();
        Class<?> klazz;

        for (String className : classNames)
        {
            try
            {
                klazz = Class.forName(className);
            }
            catch (ClassNotFoundException e)
            {
                throw new RuntimeException("Unexpected class not found exception for: " + className, e);
            }

            boolean isAbstract = Modifier.isAbstract(klazz.getModifiers());
            boolean isHallelujahClient = klazz.equals(KickAssHallelujahClient.class);

            if (isAbstract || isHallelujahClient)
            {
                continue;
            }

            TestClass testClass = new TestClass(klazz);
            List<FrameworkMethod> testMethods = testClass.getAnnotatedMethods(org.junit.Test.class);

            for (FrameworkMethod method : testMethods)
            {
                TestCaseName testCaseName = new TestCaseName(klazz.getName(), method.getName());
                tests.add(JUnitUtils.testFromTestCaseName(testCaseName));
            }
        }

        return tests;
    }
}
