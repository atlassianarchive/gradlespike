package com.atlassian.jira.plugin.issuenav.service.issuetable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.components.orderby.OrderByUtil;
import com.atlassian.jira.components.util.SortJqlGenerator;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutManager;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Tests for {@link DefaultIssueTableCreatorFactory}.
 *
 * @since v6.0
 */
@RunWith (MockitoJUnitRunner.class)
public class TestDefaultIssueTableCreatorFactory
{
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private ColumnLayoutManager columnLayoutManager;
    private IssueTableServiceConfiguration configuration;
    @Mock
    private IssueFactory issueFactory;
    private IssueTableCreatorFactory issueTableCreatorFactory;
    @Mock
    private SortJqlGenerator sortJqlGenerator;
    @Mock
    private SearchHandlerManager searchHandlerManager;
    @Mock
    private SearchProvider searchProvider;
    @Mock
    private SearchProviderFactory searchProviderFactory;
    @Mock
    private SearchService searchService;
    @Mock
    private User user;
    @Mock
    private FieldManager fieldManager;
    @Mock
    private DisplayedColumnsHelper displayedColumnsHelper;
    @Mock
    private OrderByUtil orderByUtil;

    @Before
    public void setUp()
    {
        configuration = new IssueTableServiceConfiguration();
        issueTableCreatorFactory = new DefaultIssueTableCreatorFactory(applicationProperties, columnLayoutManager,
                issueFactory, sortJqlGenerator, searchHandlerManager, searchProvider, searchProviderFactory,
                searchService, fieldManager, orderByUtil);
    }

    /**
     * Passing an invalid layout key should result in an {@link IllegalArgumentException} being thrown.
     */
    @Test (expected = IllegalArgumentException.class)
    public void testInvalidLayoutKey()
    {
        configuration.setLayoutKey("lololol");
        issueTableCreatorFactory.getStableIssueTableCreator(configuration, null, Lists.newArrayList(1L), new SearchRequest(), user);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testNullLayoutKey()
    {
        issueTableCreatorFactory.getStableIssueTableCreator(configuration, null, Lists.newArrayList(1L), new SearchRequest(), user);
    }
}
