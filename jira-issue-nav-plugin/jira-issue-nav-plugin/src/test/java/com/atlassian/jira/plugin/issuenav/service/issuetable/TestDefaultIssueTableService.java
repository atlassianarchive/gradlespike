package com.atlassian.jira.plugin.issuenav.service.issuetable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.components.orderby.OrderByUtil;
import com.atlassian.jira.components.util.FilterLookerUpper;
import com.atlassian.jira.components.util.SortJqlGenerator;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.fields.FieldException;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayout;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutManager;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.issue.search.util.SearchSortUtil;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.issue.fields.MockNavigableField;
import com.atlassian.jira.mock.security.MockAuthenticationContext;
import com.atlassian.jira.plugin.issuenav.util.PreferredSearchLayoutService;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.jira.web.component.TableLayoutUtils;
import com.atlassian.query.Query;
import com.atlassian.query.order.OrderBy;
import com.atlassian.query.order.OrderByImpl;
import com.atlassian.query.order.SearchSort;
import com.atlassian.query.order.SortOrder;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestDefaultIssueTableService
{
    @Mock
    private SearchService searchService;
    @Mock
    private SearchSortUtil searchSortUtil;
    @Mock
    private ColumnLayoutManager columnLayoutManager;
    @Mock
    private TableLayoutUtils tableLayoutUtils;
    @Mock
    private SortJqlGenerator sortJqlGenerator;
    @Mock
    private SearchProvider searchProvider;
    @Mock
    private FieldManager fieldManager;
    @Mock
    private DisplayedColumnsHelper displayedColumnsHelper;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private SearchHandlerManager searchHandlerManager;
    @Mock
    private Query query;
    @Mock
    private PagerFilter pager;
    @Mock
    private SearchResults searchResults;
    @Mock
    private SearchResults adjustedSearchResults;
    @Mock
    private ColumnLayout columnLayout;
    @Mock
    private SearchRequest searchRequest;
    @Mock
    SearchRequestService searchRequestService;
    @Mock
    private SearchProviderFactory searchProviderFactory;
    @Mock
    private PreferredSearchLayoutService preferredSearchLayoutService;
    @Mock
    private OrderByUtil orderByUtil;
    @Mock
    private IssueFactory issueFactory;
    private DefaultIssueTableService service;
    private final User user = new MockUser("angryanderson");
    @Mock
    private FilterLookerUpper filterLookerUpper;
    private MockAuthenticationContext authenticationContext;

    @Before
    public void setUp()
    {
        ComponentAccessor.initialiseWorker(new MockComponentWorker());
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_STABLE_SEARCH_MAX_RESULTS)).thenReturn("1000");

        authenticationContext = new MockAuthenticationContext(user, null);
        final IssueTableCreatorFactory issueTableCreatorFactory = new DefaultIssueTableCreatorFactory(applicationProperties, columnLayoutManager,
                issueFactory, sortJqlGenerator, searchHandlerManager, searchProvider, searchProviderFactory, searchService, fieldManager, orderByUtil);
        service = new DefaultIssueTableService(authenticationContext, fieldManager, issueTableCreatorFactory,
                preferredSearchLayoutService, filterLookerUpper, searchService, searchSortUtil, searchHandlerManager);
    }

    @Test
    public void testFetchIssueTableWithInvalidJQL()
    {
        final String invalidJQL = "invalid JQL";

        MessageSet messageSet = new MessageSetImpl();
        messageSet.addErrorMessage("Error");
        SearchService.ParseResult parseResult = new SearchService.ParseResult(null, messageSet);

        when(searchService.parseQuery(user, invalidJQL)).thenReturn(parseResult);
        ServiceOutcome<IssueTableServiceOutcome> outcome = service.getIssueTableFromJQL(invalidJQL, new IssueTableServiceConfiguration(), false);
        assertFalse(outcome.getErrorCollection().getErrorMessages().isEmpty());
    }

    @Test
    public void testValidateColumnNames() throws FieldException
    {
        final Set<NavigableField> navigableFields = new HashSet<NavigableField>();
        navigableFields.add(new MockNavigableField("col1"));
        navigableFields.add(new MockNavigableField("col2"));
        navigableFields.add(new MockNavigableField("col3"));
        navigableFields.add(new MockNavigableField("col4"));

        when(fieldManager.getAvailableNavigableFields(user)).thenReturn(navigableFields);

        ErrorCollection errors = new SimpleErrorCollection();
        service.validateColumnNames(Arrays.asList("col1", "col2", "col3", "col4"), errors);
        assertFalse(errors.hasAnyErrors());
    }

    @Test
    public void testValidateColumnNamesNotFound() throws FieldException
    {
        final Set<NavigableField> navigableFields = new HashSet<NavigableField>();
        navigableFields.add(new MockNavigableField("col1"));
        navigableFields.add(new MockNavigableField("col2"));

        when(fieldManager.getAvailableNavigableFields(user)).thenReturn(navigableFields);

        ErrorCollection errors = new SimpleErrorCollection();
        service.validateColumnNames(Arrays.asList("col1", "col2", "col3", "col4"), errors);
        assertEquals(1, errors.getErrors().size());
        assertEquals("The following columns are invalid: col3, col4.", errors.getErrors().get("columnNames"));
    }

    @Test
    public void testValidateColumnNamesEmpty() throws FieldException
    {
        ErrorCollection errors = new SimpleErrorCollection();
        service.validateColumnNames(null, errors);

        assertEquals(0, errors.getErrors().size());
    }

    @Test
    public void testDefaultLayout() throws Exception
    {
        IssueTableServiceConfiguration configHadNoLayout = new IssueTableServiceConfiguration();
        final IssueTableCreatorFactory mockIssueTableCreatorFactory = mock(IssueTableCreatorFactory.class);
        IssueTable issueTable = mock(IssueTable.class);
        IssueTableCreator itc = mock(IssueTableCreator.class);
        when(itc.create()).thenReturn(issueTable);
        when(itc.validate()).thenReturn(new MessageSetImpl());
        when(mockIssueTableCreatorFactory.getNormalIssueTableCreator(eq(configHadNoLayout), any(Query.class), any(Boolean.class), any(SearchRequest.class), any(User.class))).thenReturn(itc);
        final String preferredLayout = "preferred-layout-is-preferred";
        when(preferredSearchLayoutService.getPreferredSearchLayout()).thenReturn(preferredLayout);
        service = new DefaultIssueTableService(authenticationContext, fieldManager, mockIssueTableCreatorFactory,
                preferredSearchLayoutService, filterLookerUpper, searchService, searchSortUtil, searchHandlerManager);
        service.getIssueTable(configHadNoLayout, query, false, null);
        assertEquals(preferredLayout, configHadNoLayout.getLayoutKey());

        configHadNoLayout = new IssueTableServiceConfiguration();

        configHadNoLayout.setLayoutKey(null);
        when(mockIssueTableCreatorFactory.getStableIssueTableCreator(any(IssueTableServiceConfiguration.class), any(Query.class), any(List.class), any(SearchRequest.class), any(User.class))).thenReturn(itc);
        service.getIssueTableFromIssueIds(null, null, Lists.newArrayList(1L, 2L), configHadNoLayout);
        assertEquals(preferredLayout, configHadNoLayout.getLayoutKey());
    }

    @Test
    public void testBuildOrderByWithEmptyOrderBy()
    {
        final NavigableField field = getMockNavigableFieldWithDefaultSortOrder("col1", NavigableField.ORDER_ASCENDING);
        final OrderBy result = service.buildOrderBy(new OrderByImpl(new ArrayList<SearchSort>()), field);
        final List<SearchSort> searchSorts = result.getSearchSorts();
        assertThat(searchSorts.size(), equalTo(1));
        assertThat(searchSorts.get(0).getField(), equalTo("col1"));
    }

    @Test
    public void testBuildOrderByToggleFirstSortField()
    {
        final NavigableField field = getMockNavigableFieldWithDefaultSortOrder("col1", NavigableField.ORDER_ASCENDING);
        final OrderBy result = service.buildOrderBy(makeOrderByWithFields("col1", "col2", "col3"), field);
        final List<SearchSort> searchSorts = result.getSearchSorts();
        assertThat(searchSorts.size(), equalTo(3));
        assertThat(searchSorts.get(0).getField(), equalTo("col1"));
        assertThat(searchSorts.get(0).getSortOrder(), equalTo(SortOrder.DESC));

        final OrderBy result2 = service.buildOrderBy(result, field);
        final List<SearchSort> searchSorts2 = result2.getSearchSorts();
        assertThat(searchSorts2.size(), equalTo(3));
        assertThat(searchSorts2.get(0).getField(), equalTo("col1"));
        assertThat(searchSorts2.get(0).getSortOrder(), equalTo(SortOrder.ASC));
    }

    @Test
    public void testBuildOrderByAddExistingSortField()
    {
        final NavigableField field = getMockNavigableFieldWithDefaultSortOrder("col2", NavigableField.ORDER_ASCENDING);
        final OrderBy result = service.buildOrderBy(makeOrderByWithFields("col1", "col2", "col3"), field);
        final List<SearchSort> searchSorts = result.getSearchSorts();
        assertThat(searchSorts.size(), equalTo(3));
        assertThat(searchSorts.get(0).getField(), equalTo("col2"));
        assertThat(searchSorts.get(0).getSortOrder(), equalTo(SortOrder.ASC));
    }

    private static OrderBy makeOrderByWithFields(String... ids)
    {
        final List<SearchSort> searchSorts = new ArrayList<SearchSort>();
        for (String id : ids)
        {
            SearchSort searchSort = new SearchSort(id);
            searchSorts.add(searchSort);
        }
        return new OrderByImpl(searchSorts);
    }

    private static NavigableField getMockNavigableFieldWithDefaultSortOrder(final String id, final String defaultSortOrder)
    {
        return new MockNavigableField(id)
        {
            @Override
            public String getDefaultSortOrder()
            {
                return defaultSortOrder;
            }
        };
    }
}
