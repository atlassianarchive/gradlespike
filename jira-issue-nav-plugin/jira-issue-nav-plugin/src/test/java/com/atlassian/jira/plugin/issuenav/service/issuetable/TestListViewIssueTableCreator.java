package com.atlassian.jira.plugin.issuenav.service.issuetable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.components.orderby.OrderByUtil;
import com.atlassian.jira.components.util.SortJqlGenerator;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutManager;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static junit.framework.Assert.assertNull;

/**
 * Tests for {@link ListViewIssueTableCreator}.
 *
 * @since v6.0
 */
@RunWith (MockitoJUnitRunner.class)
public class TestListViewIssueTableCreator
{
    private static class TestableListViewIssueTableCreator extends ListViewIssueTableCreator
    {
        public TestableListViewIssueTableCreator(
                final ApplicationProperties applicationProperties,
                final ColumnLayoutManager columnLayoutManager,
                final IssueTableServiceConfiguration configuration,
                final boolean fromIssueIds,
                final IssueFactory issueFactory,
                final List<Long> issueIds,
                final SortJqlGenerator sortJqlGenerator,
                final Query query,
                final boolean returnIssueIds,
                final SearchHandlerManager searchHandlerManager,
                final SearchProvider searchProvider,
                final SearchProviderFactory searchProviderFactory,
                final SearchRequest searchRequest,
                final SearchService searchService,
                final User user,
                final FieldManager fieldManager,
                final OrderByUtil orderByUtil)
        {
            super(applicationProperties, columnLayoutManager, configuration, fromIssueIds, issueFactory, issueIds,
                    sortJqlGenerator, query, returnIssueIds, searchHandlerManager, searchProvider,
                    searchProviderFactory, searchRequest, searchService, user, fieldManager, orderByUtil);
        }

        public void setSearchResults(final SearchResults searchResults)
        {
            this.searchResults = searchResults;
        }
    }

    /**
     * {@link com.atlassian.jira.plugin.issuenav.service.issuetable.ListViewIssueTableCreator#getTable()} should not
     * attempt to render HTML if the search returned no issues; instead it should just return {@code null}.
     */
    @Test
    public void testGetTableNoIssues()
    {
        final TestableListViewIssueTableCreator listViewIssueTableCreator = new TestableListViewIssueTableCreator(
                null, null, null, false, null, null, null, null, true, null, null, null, null, null, null, null, null
        );

        final PagerFilter<Issue> pagerFilter = new PagerFilter<Issue>(0, 50);
        final SearchResults searchResults = new SearchResults(Collections.EMPTY_LIST, pagerFilter);
        listViewIssueTableCreator.setSearchResults(searchResults);
        assertNull(listViewIssueTableCreator.getTable());
    }
}
