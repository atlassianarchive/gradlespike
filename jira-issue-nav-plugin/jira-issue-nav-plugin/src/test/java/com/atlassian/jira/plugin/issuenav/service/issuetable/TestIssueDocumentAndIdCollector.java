package com.atlassian.jira.plugin.issuenav.service.issuetable;

import com.atlassian.jira.issue.index.DocumentConstants;
import com.google.common.collect.Lists;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.NumericField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static junit.framework.Assert.assertEquals;

/**
 * Tests for {@link IssueDocumentAndIdCollector}.
 *
 * @since v6.0
 */
public class TestIssueDocumentAndIdCollector
{
    RAMDirectory ramidx = new RAMDirectory();

    List<Document> sampleDocuments;
    private IndexSearcher mockIssueSearcher;

    @Before
    public void loadSampleDocuments() throws Exception {

        sampleDocuments = Lists.newArrayList(
            mockIssueDocument(1L, "JRA-1"),
            mockIssueDocument(2L, "JRA-2"),
            mockIssueDocument(3L, "JRA-3"),
            mockIssueDocument(4L, "JRA-4"),
            mockIssueDocument(5L, "JRA-5"),
            mockIssueDocument(6L, "JRA-6"),
            mockIssueDocument(7L, "JRA-7"),
            mockIssueDocument(8L, "JRA-8"),
            mockIssueDocument(9L, "JRA-9")
        );
        mockIssueSearcher = new IndexSearcher(ramidx);
    }

    private IssueDocumentAndIdCollector loadDocuments(IssueDocumentAndIdCollector documentCollector) throws IOException
    {
        for (Document document : sampleDocuments)
        {
            documentCollector.collect(docIdForKey(document.getFieldable(DocumentConstants.ISSUE_KEY).stringValue()));
        }
        collector.setTotalHits(sampleDocuments.size());
        return documentCollector;
    }


    private IssueDocumentAndIdCollector collector;

    /**
     * No more than the maximum number of IDs / keys should be collected.
     */
    @Test
    public void testCollectMaximumZero() throws Exception {
        collector = new IssueDocumentAndIdCollector(mockIssueSearcher, 0, 0, null, 0);
        collector.collect(docIdForKey("JRA-1"));
        collector.setTotalHits(1);
        IssueDocumentAndIdCollector.Result result = collector.computeResult();

        assertEquals(0, result.getIssueIDs().size());
        assertEquals(1, result.getTotal());
    }

    /**
     * Test that when the maximum number of ids is set the correct number of issues are collected
     */
    @Test
    public void testCollectMaximum() throws Exception {
        final int MAX_ISSUES = 5;
        collector = new IssueDocumentAndIdCollector(mockIssueSearcher, MAX_ISSUES, 3, null, 0);
        loadDocuments(collector);
        IssueDocumentAndIdCollector.Result result = collector.computeResult();

        assertEquals(Lists.newArrayList(1L, 2L, 3L, 4L, 5L), result.getIssueIDs());
        assertEquals(Lists.newArrayList("JRA-1", "JRA-2", "JRA-3", "JRA-4", "JRA-5"), result.getIssueKeys());
        assertEquals(9, result.getTotal());
    }

    /**
     * Test that the correct number of issues are returned when the page size is set
     */
    @Test
    public void testCollectPageSizeReturnsCorrectNumberOfIssues() throws Exception
    {
        final int expectedStart = 3;
        final int expectedEnd = 6;
        final List<Document> expectedDocuments = sampleDocuments.subList(expectedStart, expectedEnd);

        collector = new IssueDocumentAndIdCollector(mockIssueSearcher, 9, 3, null, 3);
        loadDocuments(collector);
        IssueDocumentAndIdCollector.Result result = collector.computeResult();

        assertDocumentsEqual(expectedDocuments, result.getDocuments());
        assertEquals(expectedStart, result.getStart());
    }

    /**
     * If the user selects an issue and it is found the page that that issue is on is shown.
     */
    @Test
    public void testCollectSelectedIssue() throws Exception
    {
        final int expectedStart = 3;
        final int expectedEnd = 6;
        final List<Document> expectedDocuments = sampleDocuments.subList(expectedStart, expectedEnd);

        collector = new IssueDocumentAndIdCollector(mockIssueSearcher, 9, 3, "JRA-5", 0);
        loadDocuments(collector);
        IssueDocumentAndIdCollector.Result result = collector.computeResult();

        assertDocumentsEqual(expectedDocuments, result.getDocuments());
        assertEquals(expectedStart, result.getStart());
    }

    /**
     * If a user puts in a url=/issues/?jql=&issueKey=<issue_key_not_found>, where the issue key is not found
     * the first page should be displayed.
     */
    @Test
    public void testCollectSelectedIssueNotFound() throws Exception
    {
        final int expectedStart = 0;
        final int expectedEnd = 3;
        final List<Document> expectedDocuments = sampleDocuments.subList(expectedStart, expectedEnd);

        collector = new IssueDocumentAndIdCollector(mockIssueSearcher, 9, 3, "JRA-asd5", 0);
        loadDocuments(collector);
        IssueDocumentAndIdCollector.Result result = collector.computeResult();

        assertDocumentsEqual(expectedDocuments, result.getDocuments());
        assertEquals(expectedStart, result.getStart());
    }

    /**
     * If the user puts in a url=/issues/?jql=&startIndex=4&issueKey=<issue_key_found>, the page with the issue key
     * should be displayed.
     */
    @Test
    public void testCollectStartIndexWithSelectedIssueFound() throws Exception
    {
        final int expectedStart = 3;
        final int expectedEnd = 6;
        final List<Document> expectedDocuments = sampleDocuments.subList(expectedStart, expectedEnd);

        collector = new IssueDocumentAndIdCollector(mockIssueSearcher, 9, 3, "JRA-5", 6);
        loadDocuments(collector);
        IssueDocumentAndIdCollector.Result result = collector.computeResult();

        assertDocumentsEqual(expectedDocuments, result.getDocuments());
        assertEquals(expectedStart, result.getStart());
    }

    /**
     * If the user puts in a url=/issues/jql=&startIndex=4&issueKey=<issue_key_not_found>, the page with the start Index
     * should be displayed.
     */
    @Test
    public void testCollectStartIndexWithSelectedIssueNotFound() throws Exception
    {
        final int expectedStart = 6;
        final int expectedEnd = 9;
        final List<Document> expectedDocuments = sampleDocuments.subList(expectedStart, expectedEnd);

        collector = new IssueDocumentAndIdCollector(mockIssueSearcher, 9, 3, "JRA-asd5", 6);
        loadDocuments(collector);
        IssueDocumentAndIdCollector.Result result = collector.computeResult();

        assertDocumentsEqual(expectedDocuments, result.getDocuments());
        assertEquals(expectedStart, result.getStart());
    }

    /**
     * Test that the start index should return the right page results for that page
     */
    @Test
    public void testCollectStartIndexIsCorrectPage() throws Exception
    {
        final int expectedStart = 6;
        final int expectedEnd = 9;
        final List<Document> expectedDocuments = sampleDocuments.subList(expectedStart, expectedEnd);

        collector = new IssueDocumentAndIdCollector(mockIssueSearcher, 9, 3, null, 6);
        loadDocuments(collector);
        IssueDocumentAndIdCollector.Result result = collector.computeResult();

        assertDocumentsEqual(expectedDocuments, result.getDocuments());
        assertEquals(expectedStart, result.getStart());
    }


    /**
     * If the user puts in a startIndex greater than the stable search limit or greater than the total number of issues
     * the last page should be displayed.
     */
    @Test
    public void testCollectStartIndexGreaterThanResults() throws Exception
    {
        final int expectedStart = 6;
        final int expectedEnd = 9;
        final List<Document> expectedDocuments = sampleDocuments.subList(expectedStart, expectedEnd);

        collector = new IssueDocumentAndIdCollector(mockIssueSearcher, 9, 3, null, 20);
        loadDocuments(collector);
        IssueDocumentAndIdCollector.Result result = collector.computeResult();

        assertDocumentsEqual(expectedDocuments, result.getDocuments());
        assertEquals(expectedStart, result.getStart());
    }

    /**
     * If a user specifies a start index that is inbetween the page size start limit, the page results should be rounded
     * down to the nearest page start index so pagination is maintained.
     */
    @Test
    public void testCollectStartIndexBetweenPaginations() throws Exception
    {
        final int expectedStart = 3;
        final int expectedEnd = 6;
        final List<Document> expectedDocuments = sampleDocuments.subList(expectedStart, expectedEnd);

        collector = new IssueDocumentAndIdCollector(mockIssueSearcher, 9, 3, null, 5);
        loadDocuments(collector);
        IssueDocumentAndIdCollector.Result result = collector.computeResult();

        assertDocumentsEqual(expectedDocuments, result.getDocuments());
        assertEquals(expectedStart, result.getStart());
    }

    private Document mockIssueDocument(Long ID, String key) throws Exception
    {
        final NumericField IDField = new NumericField(DocumentConstants.ISSUE_ID, Field.Store.YES, true);
        IDField.setLongValue(ID);

        final Document document = new Document();
        document.add(IDField);
        document.add(new Field(DocumentConstants.ISSUE_KEY, key, Field.Store.YES, Field.Index.NOT_ANALYZED));

        addDocumentToSearcher(document);

        return document;
    }

    private void addDocumentToSearcher(Document document) throws IOException {
        IndexWriter writer = new IndexWriter(ramidx, new IndexWriterConfig(Version.LUCENE_32, new StandardAnalyzer(Version.LUCENE_32)));
        writer.addDocument(document);
        writer.close();
    }
    private int docIdForKey(String key) throws IOException {
        final TermDocs docs = mockIssueSearcher.getIndexReader().termDocs(new Term(DocumentConstants.ISSUE_KEY, key));
        if (docs.next()) {
            return docs.doc();
        }
        throw new RuntimeException("Could not find issue key in our index: " + key);
    }

    private static void assertDocumentsEqual(List<Document> expectedDocuments, List<Document> documents) {
        assertEquals(expectedDocuments.toString(), documents.toString());
    }


}