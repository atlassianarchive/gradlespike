package com.atlassian.jira.plugin.issuenav.hallelujah;

import com.atlassian.buildeng.hallelujah.jms.JMSHallelujahClient;
import com.atlassian.buildeng.hallelujah.listener.TestsRunListener;
import org.junit.Test;

public class KickAssHallelujahClient
{
    @Test
    public void run() throws Exception
    {
        System.out.println("KickAss hallelujah client starting...");

        new JMSHallelujahClient.Builder()
                .build()
                .registerListeners(new TestsRunListener())
                .run();

        System.out.println("KickAss hallelujah client finished.");
    }
}