package com.atlassian.jira.plugin.issuenav.service.issuetable;

import com.atlassian.jira.components.util.DefaultSortJqlGenerator;
import com.atlassian.jira.components.util.SortJqlGenerator;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.jql.util.JqlStringSupportImpl;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.order.OrderByImpl;
import com.atlassian.query.order.SearchSort;
import com.atlassian.query.order.SortOrder;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestSortJqlGenerator
{
    @Mock SearchHandlerManager searchHandlerManager;
    @Mock JqlQueryParser parser;
    @Mock NavigableField updatedSystemField;
    @Mock CustomField customField;

    SortJqlGenerator sortJqlGenerator;

    @Before
    public void setUp() throws Exception
    {
        updatedSystemField = mock(NavigableField.class);
        when(updatedSystemField.getId()).thenReturn("updated");
        when(searchHandlerManager.getJqlClauseNames("updated")).thenReturn(ImmutableList.of(new ClauseNames("updated", "updatedDate")));

        // class under test
        sortJqlGenerator = new DefaultSortJqlGenerator(new JqlStringSupportImpl(parser), searchHandlerManager);
    }

    /**
     * Ensure that JRADEV-19521 does not regress.
     */
    @Test
    public void columnSortJqlShouldPreserveAliasesUsedInTheOriginalJql()
    {
        // the query is "ORDER BY updatedDate DESC"
        QueryImpl query = new QueryImpl(null, new OrderByImpl(new SearchSort("updatedDate", SortOrder.DESC)), "ORDER by updatedDate DESC");

        Map<String, String> sortJql = sortJqlGenerator.generateColumnSortJql(query, ImmutableList.of(updatedSystemField));
        assertThat(sortJql.get("updated"), equalTo("ORDER BY \"updatedDate\" ASC"));
    }
}
