package com.atlassian.jira.plugin.issuenav;

import com.atlassian.jira.components.issueviewer.util.HashTransformer;
import com.atlassian.jira.util.UrlBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class TestHashTransformer
{
    // Overrides addParameter to avoid the call to ComponentAccessor.
    private static class TestableHashTransformer extends HashTransformer
    {
        @Override
        protected void addParameter(UrlBuilder urlBuilder, String key, Object[] values)
        {
            for (Object value : values)
            {
                String valueString = value == null ? null : value.toString();
                urlBuilder.addParameterUnsafe(key, valueString);
            }
        }
    }

    private static final String USERAGENT_IE8 = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)";
    private static final String USERAGENT_IE9 = "Mozilla/4.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)";
    private static final String USERAGENT_FF = "Mozilla/5.0 (Windows NT 6.1; rv:12.0) Gecko/20120403211507 Firefox/12.0";

    @Test
    public void testSupportsPushState()
    {
        assertFalse(new HashTransformer().supportsPushState(USERAGENT_IE8));
        assertFalse(new HashTransformer().supportsPushState(USERAGENT_IE9));
        assertTrue(new HashTransformer().supportsPushState(USERAGENT_FF));
    }

    @Test
    public void testTransformURL()
    {
        HashTransformer hashTransformer = new TestableHashTransformer();
        HashMap<String, Object[]> parameters = new HashMap<String, Object[]>();

        assertEquals("Empty URL should be transformed to empty string",
            "", hashTransformer.transformURL(null, null)
        );

        assertEquals("Null parameters should not be append to the transformed URL (/issues)",
            "/i#issues/", hashTransformer.transformURL("/issues/", null));
        assertEquals("Null parameters should not be append to the transformed URL (/browse/KEY)",
            "/i#browse/KEY-123", hashTransformer.transformURL("/browse/KEY-123", null));

        assertEquals("Empty parameters should not be append to the transformed URL (/issues)",
            "/i#issues/", hashTransformer.transformURL("/issues/", parameters));
        assertEquals("Empty parameters should not be append to the transformed URL (/browse/KEY)",
            "/i#browse/KEY-123", hashTransformer.transformURL("/browse/KEY-123", parameters));

        parameters.put("jql", new Object[] { "" });
        assertEquals("Empty SearchContext should not be append to the transformed URL (/issues)",
            "/i#issues/?jql=", hashTransformer.transformURL("/issues/", parameters));
        assertEquals("Empty SearchContext should not be append to the transformed URL (/browser/KEY)",
            "/i#browse/KEY-123?jql=", hashTransformer.transformURL("/browse/KEY-123", parameters));

        parameters.clear();
        parameters.put("jql", new Object[] { "text~?" });
        assertEquals("SearchContext should be part of the hashed parameters (/issues)",
            "/i#issues/?jql=text~?", hashTransformer.transformURL("/issues/", parameters));
        assertEquals("SearchContext should be part of the hashed parameters (/browser/KEY)",
            "/i#browse/KEY-123?jql=text~?", hashTransformer.transformURL("/browse/KEY-123", parameters));

        parameters.clear();
        parameters.put("foo", new Object[] { "bar" });
        assertEquals("Any parameter what is not in the WHITE_LIST should be part of the hashed parameters (/issues)",
                "/i#issues/?foo=bar", hashTransformer.transformURL("/issues/", parameters));
        assertEquals("Any parameter what is not in the WHITE_LIST should be part of the hashed parameters (/browse/KEY)",
                "/i#browse/KEY-123?foo=bar", hashTransformer.transformURL("/browse/KEY-123", parameters));

        parameters.clear();
        parameters.put("jwupdated", new Object[] { "123" });
        assertEquals("WHITE-LISTed parameters should be part of the queryString (/issues)",
            "/i?jwupdated=123#issues/", hashTransformer.transformURL("/issues/", parameters));
        assertEquals("WHITE-LISTed parameters should be part of the queryString (/browser/KEY)",
            "/i?jwupdated=123#browse/KEY-123", hashTransformer.transformURL("/browse/KEY-123", parameters));

        parameters.clear();
        parameters.put("jql", new Object[] { "text~?" });
        parameters.put("jwupdated", new Object[] { "123" });
        assertEquals("SearchContext and WHITE-LISTed parameters should be in the right place (/issues)",
            "/i?jwupdated=123#issues/?jql=text~?", hashTransformer.transformURL("/issues/", parameters));
        assertEquals("SearchContext and WHITE-LISTed parameters should be in the right place (/browse/KEY-123)",
            "/i?jwupdated=123#browse/KEY-123?jql=text~?", hashTransformer.transformURL("/browse/KEY-123", parameters));

        parameters.clear();
        parameters.put("jql", new Object[] { "text~?" });
        parameters.put("jwupdated", new Object[] { "123" });
        parameters.put("foo", new Object[] { "bar" });
        parameters.put("quickSearchQuery", new Object[] { "foobar" });
        assertEquals("Lots of mixed parameters should be in the right place (/issues)",
                "/i?jwupdated=123&quickSearchQuery=foobar#issues/?jql=text~?&foo=bar",
                hashTransformer.transformURL("/issues/", parameters));
        assertEquals("Lots of mixed parameters should be in the right place (/browse/KEY)",
                "/i?jwupdated=123&quickSearchQuery=foobar#browse/KEY?jql=text~?&foo=bar",
                hashTransformer.transformURL("/browse/KEY", parameters));

    }
}