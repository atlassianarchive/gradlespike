
// Backbone.define() - alternative to AJS.namespace that also adds a 'name' property to
// the ctors returned from Backbone's extend().
// Also does some hacky eval to show the name of the object when using console.log() in Chrome.
// Example: Backbone.define('JIRA.Issues.SuperModel', Backbone.Model.extend({ ... }));
Backbone.define = function(name, ctor) {
    eval("Backbone['Class: " + name + "'] = function() { Backbone['Class: " + name + "'].__ctor.apply(this, arguments); }");
    var cls = Backbone['Class: ' + name];
    cls.__ctor = ctor;
    ctor.prototype.name = name;
    cls.prototype = ctor.prototype;
    _.extend(cls, ctor);

    _.each(ctor.prototype, function(fn, fnName) {
        if (typeof fn === 'function') {
            fn.displayName = name + '.' + fnName;
        }
    });

    var context = window;
    var parts = name.split('.');
    _.each(parts, function(part, i) {
        if (i === parts.length - 1) {
            context[part] = cls;
        } else {
            context = context[part] == null ? (context[part] = {}) : context[part];
        }
    });

    return cls;
};