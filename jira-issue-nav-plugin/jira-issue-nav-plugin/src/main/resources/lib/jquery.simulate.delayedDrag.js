/*!
 * jQuery Simulate v0.0.1 - simulate browser mouse and keyboard events
 * https://github.com/jquery/jquery-simulate
 *
 * Copyright 2012 jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * Date: Tue Dec 31 09:57:39 2013 -0500
 */

;(function( $, undefined ) {

    function findCenter( elem ) {
        var offset,
            document = $( elem.ownerDocument );
        elem = $( elem );
        offset = elem.offset();

        return {
            x: offset.left + elem.outerWidth() / 2 - document.scrollLeft(),
            y: offset.top + elem.outerHeight() / 2 - document.scrollTop()
        };
    }

    $.extend( $.simulate.prototype, {
        simulateDelayedDrag: function() {
            var target = this.target,
                options = this.options,
                center = findCenter( target ),
                x = Math.floor( center.x ),
                y = Math.floor( center.y ),
                dx = options.dx || 0,
                dy = options.dy || 0,
                moves = options.moves || 3,
                coord = { clientX: x, clientY: y };

            this.simulateEvent( target, "mousedown", coord );

            var instance = this;
            setTimeout(function() {
                var i = 0;
                for ( ; i < moves ; i++ ) {
                    x += dx / moves;
                    y += dy / moves;

                    coord = {
                        clientX: Math.round( x ),
                        clientY: Math.round( y )
                    };

                    instance.simulateEvent( document, "mousemove", coord );
                }

                instance.simulateEvent( target, "mouseup", coord );
                instance.simulateEvent( target, "click", coord );
            }, options.delay || 100 );
        }
    });

})( jQuery );