/**
 * The list of issues in split view.
 * <p/>
 * Plays roughly the same role as <tt>JIRA.Issues.IssueTableView</tt>.
 */
Backbone.define("JIRA.Issues.SplitScreenListView", JIRA.Issues.BaseView.extend({
    inaccessibleIssueRowTemplate: JIRA.Templates.SplitView.inaccessibleIssueRow,
    template: JIRA.Templates.SplitView.issueList,

    events : {
        "simpleClick .issue-list li": "_onClickIssue"
    },

    /**
     * @param {object} options
     * @param {JIRA.Issues.SearchModule} options.search The view's interface to the rest of the application.
     */
    initialize: function (options) {
        _.bindAll(this,
            "_onSearchDone",
            "_onSearchFail");

        this.search = options.search;
        this.searchContainer = options.searchContainer;
        this.searchPromise = this._createSearchPromise({
            done: this._onSearchDone,
            fail: this._onSearchFail
        });

        this.searchResults = options.search.getResults();
        this.serverRendered = !!options.serverRendered && this._issuesRendered(options);

        this.addListener(this.searchResults, "highlightedIssueChange", this._onHighlightedIssueChange, this);
        this.addListener(this.searchResults, "issueUpdated", this._onIssueUpdated, this);
        this.addListener(this.searchResults, "startIndexChange", this.render, this);
    },

    _issuesRendered: function(options) {
        return !!options.searchContainer.find(".issue-list").length;
    },

    scrollToFocused: function () {
        var $issuepanel = this.searchContainer.find(".list-panel");
        var $focused = this.$el.find(".focused");
        if ($issuepanel.size() && $focused.size()) {
            $issuepanel.scrollTop($focused.position().top);
        }
    },

    /**
     * Prepare to be removed, deactivating all subviews.
     */
    deactivate: function() {
        this.searchPromise.reset();
        JIRA.Issues.BaseView.prototype.deactivate.apply(this, arguments);
    },

    /**
     * Load an issue into the detail panel after its row was clicked.
     *
     * @param {jQuery.Event} e The click event.
     * @private
     */
    _onClickIssue: function (e) {
        var issueID = AJS.$(e.target).closest("[data-id]").data("id"),
            isSelected = this.searchResults.getSelectedIssue().getId() == issueID;

        e.preventDefault();

        if (!isSelected) {
            this.searchResults.selectIssueById(issueID, {replace: true});

            JIRA.Issues.Application.execute("analytics:trigger", "kickass.openIssueFromTable", {
                issueId: issueID,
                // these are 1-based indices
                absolutePosition: this.searchResults.getPositionOfIssueInSearchResults(issueID) + 1,
                relativePosition: this.searchResults.getPositionOfIssueInPage(issueID) + 1
            });
        }
    },

    /**
     * Create the recurring search promise.
     * <p/>
     * The added deferreds trigger a render on done.
     *
     * @param {object} [options]
     * @param {function} [options.done] A done callback.
     * @param {function} [options.fail] A fail callback.
     * @return {jQuery.RecurringPromise} the recurring search promise.
     * @private
     */
    _createSearchPromise: function (options) {
        var promise = jQuery.RecurringPromise();

        options = options || {};
        options.done && promise.done(_.bind(options.done, this));
        options.fail && promise.fail(_.bind(options.fail, this));

        return promise;
    },

    getIssueById: function (id) {
        return this.$el.find("[data-id=" + id + "]");
    },

    /**
     * Highlight an issue in the list.
     *
     * @param {JIRA.Issues.SimpleIssue} [issue] The issue to highlight; if falsey, the highlight is removed.
     * @private
     */
    _highlightIssue: function (issue) {
        this.$el.find(".focused").removeClass("focused");
        // A little trick to scroll to the correct item without having to implement our
        issue && this.getIssueById(issue.getId()).addClass("focused")
                .scrollIntoView();
    },

    /**
     * Mark an issue in the list as being inaccessible.
     *
     * @param {number} id The issue's ID.
     */
    markIssueInaccessible: function (id) {
        this.getIssueById(id).replaceWith(this.inaccessibleIssueRowTemplate({
            isHighlighted: this.searchResults.getHighlightedIssue().getId() === id,
            issueID: id
        }));
    },

    /**
     * Highlight the currently highlighted issue.
     *
     * @private
     */
    _onHighlightedIssueChange: function () {
        this._highlightIssue(this.searchResults.getHighlightedIssue());
    },

    _onIssueUpdated: function (id, entity) {
        if (entity.table[0] === null) {
            this.markIssueInaccessible(id);
        } else {
            this.getIssueById(id).html(JIRA.Templates.SplitView.issueRow(entity.table[0]));
        }
    },

    /**
     * Render the issue list.
     * <p/>
     * Called when a search request is fulfilled.
     *
     * @param {object} result The search payload, extended with additional information.
     * @param {boolean} [result.serverRendered] Whether the existing HTML was server rendered.
     * @private
     */
    _onSearchDone: function (result) {
        //Depending on the default layout, anonymous user may have their layout preference set to the other layout
        //if that's true, data given by the server on page load will not be compatible. Hence do a check here.
        if (result instanceof Array) {
            if (this.searchResults.hasIssues()) {
                this.$el.html(this.template({
                    highlightedID: this.searchResults.getHighlightedIssue().getId(),
                    issueIDs: this.searchResults.getPageIssueIds(),
                    issues: result
                }));
            }

            _.defer(_.bind(this.scrollToFocused, this));
        }
    },

    /**
     * Display an error after a search fails.
     * <p/>
     * Called when an operation in <tt>searchPromise</tt> fails.
     *
     * @private
     */
    _onSearchFail: function () {
        var navigatorContent = this.searchContainer.find(".navigator-content");
        navigatorContent.html(JIRA.Templates.Issues.ComponentUtil.errorMessage({
            msg: AJS.I18n.getText("issue.nav.common.server.error")
        }));
    },

    render: function () {
        var serverRendered = this.serverRendered;
        this.serverRendered = false;

        if (this.searchResults.hasIssues()) {
            if (!this.searchResults.hasHighlightedIssue() && !this.searchResults.hasSelectedIssue()) {
                // Highlighting the issue sets the start index, causing another render. Return to avoid double rendering.
                this.searchResults.highlightFirstInPage();
                return;
            }

            // Pass the serverRendered value through so _onSearchDone knows whether to preserve HTML.
            return this.searchPromise.add(this.searchResults.getResultsForPage({
                jql: this.search.getEffectiveJql()
            }).then(function (result) {
                return _.extend(result, {serverRendered: serverRendered});
            }));
        } else {
            return jQuery.Deferred().resolve();
        }


    }
}));