AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

module("JIRA.Issues.SplitScreenDetailView", {
    setup: function() {
        this.searchModule = new JIRA.Issues.MockSearchModule();
        JIRA.Issues.Application.start();
        this.sandbox = sinon.sandbox.create();

        this.searchResults = JIRA.Issues.TestUtils.mockSearchResults();
        this.searchModule.setResults(this.searchResults);

        this.splitScreenDetailView = new JIRA.Issues.SplitScreenDetailView({
            search: this.searchModule
        });

        var $layoutSplit = jQuery("<div class='page-type-split'></div>").appendTo("body");
        var $scrollContainer = jQuery("<div class='issue-container'></div>")
                .css({maxHeight: 200, overflow: "auto"});
        var $commentModule = jQuery("<div id='addcomment'></div>")
                .css("marginTop", 500);
        var $comment = jQuery("<textarea id='comment'></textarea>");
        $layoutSplit.append($scrollContainer);
        $scrollContainer.append($commentModule);
        $commentModule.append($comment);
        this.splitScreenDetailView.setElement($layoutSplit);
    },

    teardown: function () {
        this.sandbox.restore();
        this.splitScreenDetailView.$el.remove();
    }
});

test("Focusing comment scrolls to bottom", function () {
    this.splitScreenDetailView.$el.find("#comment").focus();
    ok(this.splitScreenDetailView.$el.find(".issue-container").scrollTop() > 300, "Expected comment textfield to be scrolled to");
});

test("Don't refresh issue if update occurs from inlineEdit", function () {
    this.sandbox.stub(JIRA.Issues.Application, "request");

    this.splitScreenDetailView.onIssueUpdated(10000, {}, {action: "inlineEdit"});

    ok(!JIRA.Issues.Application.request.called, "Application.request was not called");
});

test("Focusing/bluring", function () {
    this.splitScreenDetailView.activate();

    this.splitScreenDetailView.focus();
    ok(this.splitScreenDetailView.hasFocus(), "View has focus");

    this.splitScreenDetailView.blur();
    ok(!this.splitScreenDetailView.hasFocus(), "View doesn't have focus");
});

/*
// TODO Moved to ErrorController, redo this test
test("Triggering 'issueDoesNotExist' renders error view", function () {
    this.splitScreenDetailView.activate();

    this.issueModule.renderIssueDoesNotExist = sinon.spy();

    this.searchResults.triggerIssueDoesNotExist();

    ok(this.issueModule.renderIssueDoesNotExist.called);
    ok(this.splitScreenDetailView.$el === this.issueModule.renderIssueDoesNotExist.args[0][0]);
});
*/

