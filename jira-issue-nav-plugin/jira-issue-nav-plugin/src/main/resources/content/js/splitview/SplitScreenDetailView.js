/**
 * The split view "issue panel" (where the issue is rendered).
 */
Backbone.define("JIRA.Issues.SplitScreenDetailView", JIRA.Issues.BaseView.extend({

    events: {
        "focus #addcomment textarea" : "scrollToBottom"
    },

    issueDetailsNoSelectionTemplate: JIRA.Templates.SplitView.issueDetailsNoSelection,

    /**
     * @param {object} options
     * @param {JIRA.Issues.Cache.IssueCacheManager} options.issueCacheManager The application's <tt>JIRA.Issues.Cache.IssueCacheManager</tt>.
     * @param {JIRA.Components.IssueViewer} options.issueModule The application's <tt>JIRA.Components.IssueViewer</tt>.
     * @param {JIRA.Issues.SearchModule} options.search The application's <tt>JIRA.Issues.SearchModule</tt>.
     */
    initialize: function (options) {
        _.bindAll(this, "adjustHeight", "_renderIssue");

        this.issueCacheManager = options.issueCacheManager;
        this.search = options.search;
        this.searchResults = options.search.getResults();

        JIRA.Issues.Application.on("issueEditor:fieldSubmitted", this.focus, this);
        JIRA.Issues.Application.on("issueEditor:replacedFocusedPanel", this.focus, this);
        this.adjustHeight = _.debounce(this.adjustHeight, options.easeOff);
    },

    adjustHeight: function () {
        _.defer(_.bind(function () {
            var issueContainer = this.getIssueContainer(),
                issueContainerTop,
                windowHeight;
            if (issueContainer.length) {
                issueContainerTop = issueContainer.length && issueContainer.offset().top;
                windowHeight = jQuery(window).height();
                issueContainer.css("height", windowHeight - issueContainerTop);
            }
        }, this));
    },

    getIssueContainer: function () {
        return this.$el.find(".issue-container");
    },

    scrollToTop: function () {
        var $issueContainer = this.getIssueContainer();
        if ($issueContainer.size()) {
            $issueContainer.scrollTop(0);
        }
    },

    scrollToBottom: function () {
        var $issueContainer = this.getIssueContainer();
        if ($issueContainer.size()) {
            $issueContainer.scrollTop($issueContainer.prop("scrollHeight"));
        }
    },

    /**
     * Activates this SplitScreenDetailView.
     *
     * @return {JIRA.Issues.SplitScreenDetailView} this
     */
    activate: function() {
        //If this view is already activated, do nothing
        if (this.isActive) return this;

        var instance = this;
        this.$el.focusin(function() {
            instance.focused = true;
        });
        this.$el.focusout(function() {
            instance.focused = false;
        });

        this.addListener(this.searchResults, "selectedIssueChange", this.render, this);
        this.addListener(this.searchResults, "issueUpdated", this.onIssueUpdated, this);
        this.addListener(this.searchResults, "issueDoesNotExist", this._onIssueDoesNotExist, this);

        JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, this.fixMentionsDropdownInMentionableFields);

        JIRA.Issues.onVerticalResize(this.adjustHeight);
        JIRA.Issues.Application.on("issueEditor:loadComplete", this.adjustHeight, this);
        this.isActive = true;
        return this;
    },

    deactivate: function () {
        JIRA.Issues.Application.execute("issueEditor:abortPending");
        JIRA.Issues.offVerticalResize(this.adjustHeight);
        JIRA.Issues.Application.off("issueEditor:loadComplete", this.adjustHeight);
        JIRA.unbind(JIRA.Events.NEW_CONTENT_ADDED, this.fixMentionsDropdownInMentionableFields);
        JIRA.Issues.BaseView.prototype.deactivate.apply(this, arguments);
        this.isActive = false;
    },

    /**
     * Adds markup to hide all mentions drop down on mentionable fields when the user scrolls
     *
     * @param event
     * @param $context
     * @param reason
     */
    fixMentionsDropdownInMentionableFields: function (event, $context, reason) {
        if (reason === JIRA.CONTENT_ADDED_REASON.panelRefreshed ||
            reason === JIRA.CONTENT_ADDED_REASON.inlineEditStarted
            ) {
            // Search for mentionable elements, add markup to force Mentions drop down
            // to follow the scroll of .split-view .issue-container
            $context.find(".mentionable:not([data-follow-scroll])").attr("follow-scroll", ".split-view .issue-container");

            // Search for mentionable elements, add markup to force Mentions drop down
            // to push the scroll of .issue-body-content if there is not enough room to
            // display the drop down
            $context.find(".mentionable:not([data-push-scroll])").attr("push-scroll", ".issue-body-content");

        }
    },

    onIssueUpdated: function (id, entity, reason) {
        if (reason.action !== JIRA.Issues.Actions.INLINE_EDIT && reason.action !== JIRA.Issues.Actions.ROW_UPDATE) {
            return JIRA.Issues.Application.request("issueEditor:refreshIssue", reason);
        } else {
            return jQuery.Deferred().resolve();
        }
    },

    render: function (model, options) {
        options = options || {};
        if (this.searchResults.hasIssues() && !this.search.isStandAloneIssue() && options.reason !== "issueLoaded") {
            if (this.searchResults.hasSelectedIssue()) {
                JIRA.Issues.Application.execute("issueEditor:abortPending");
                JIRA.Issues.Utils.debounce(this, "_renderIssue", this.searchResults.getSelectedIssue());
            } else {
                this._renderNoIssue();
            }
        }
        this.adjustHeight();
        return this;
    },

    _renderIssue: function (selectedIssue) {
        JIRA.Issues.Application.request("issueEditor:loadIssue", {
            id: selectedIssue.get("id"),
            key: selectedIssue.get("key"),
            detailView: true // JRA-36659: keep track of whether we are in detail view
        }).always(_.bind(function() {
            JIRA.Issues.Application.execute("pager:update", _.extend({},this.searchResults.getPager(),{isSplitView: true}));
            //JRADEV-19089 When the first issue is selected, prefetch the second one
            if (this.searchResults.isFirstIssueSelected() && this.searchResults.getDisplayableTotal()>1) {
                this.issueCacheManager.scheduleIssueToBePrefetched(this.searchResults.getNextIssueForSelectedIssue());
            }
            this.issueCacheManager.prefetchIssues();
            this.$el.addClass("active");
            this.scrollToTop();
        }, this));
    },

    focus: function() {
        if (!this.hasFocus()) {
            this.getIssueContainer().focus();
        }
    },

    blur: function() {
        this.getIssueContainer().blur();
    },

    _onIssueDoesNotExist: function() {
        this._renderNoIssue();
        JIRA.Issues.Application.execute("issueEditor:setContainer",this.$el);
        this.render();
    },

    _renderNoIssue:function () {
        this.$el.html(this.issueDetailsNoSelectionTemplate());
    },

    hasFocus: function() {
        return this.focused === true;
    }
}));
