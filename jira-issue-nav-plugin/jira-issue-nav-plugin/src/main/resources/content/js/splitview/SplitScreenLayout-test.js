AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

module("JIRA.Issues.SplitScreenLayout", {
    setup: function() {
        this.searchPageModule = JIRA.Issues.TestUtils.mockSearchPageModule();
        this.searchPageModule.registerViewContainers({
            issueContainer: this.searchPageModule.issueContainer,
            searchContainer: AJS.$("<div><div class=\"navigator-content\"></div></div>")
        });
        this.sandbox = sinon.sandbox.create();

        this.splitScreenLayout = new JIRA.Issues.SplitScreenLayout({
            fullScreenIssue: this.searchPageModule.fullScreenIssue,
            search: this.searchPageModule.search,
            searchContainer: this.searchPageModule.searchContainer,
            issueCacheManager: this.searchPageModule.issueCacheManager
        });
        this.searchPageModule.setCurrentLayout(this.splitScreenLayout);
    },

    teardown: function() {
        this.sandbox.restore();
    }
});

test("Dims navigator content while a search is executing", function () {
    ok(!this.splitScreenLayout.navigatorContent.hasClass("pending"), "Navigator content is visible.");

    this.searchPageModule.search.triggerBeforeSearch();
    ok(this.splitScreenLayout.navigatorContent.hasClass("pending"), "Navigator content was dimmed.");
});

test("returnToSearch() doesn't deselect the selected issue", function () {
    this.searchPageModule.returnToSearch();
    ok(!this.searchPageModule.searchResults.selectIssueByKey.called, "Didn't deselect the selected issue.");
});

test("Navigate to right/left focuses/blurs the details view", function () {
    var focusSpy = this.sandbox.spy();
    var blurSpy = this.sandbox.spy();

    this.splitScreenLayout.detailsView.focus = focusSpy;
    this.splitScreenLayout.detailsView.blur = blurSpy;

    this.splitScreenLayout.handleRight();
    equal(focusSpy.callCount, 1, "Issue details view focused");

    this.splitScreenLayout.handleLeft();
    equal(blurSpy.callCount, 1, "Issue details view unfocused");

    this.splitScreenLayout.detailsView.hasFocus = function() { return true };
    ok(this.splitScreenLayout.isIssueViewActive(), "Issue view is active when details panel has focus");

    this.splitScreenLayout.detailsView.hasFocus = function() { return false };
    ok(!this.splitScreenLayout.isIssueViewActive(), "Issue view is inactive when details panel doesn't have focus");
});

test("Deleting the issue dims the view and locally hide the deleted issue", function() {
    var getIssueStub = this.sandbox.stub(this.splitScreenLayout.listView, "getIssueById");
    var $issueRow = jQuery("<div></div>");
    getIssueStub.returns($issueRow);

    this.searchPageModule.searchResults.set({
        total: 0,
        displayableTotal: 0
    });

    var issue = {
        id: 9001,
        key: "POKE-9000"
    }

    equal($issueRow.attr("style"), undefined, "Issue row is not hidden before deleting");
    this.searchPageModule.searchResults.triggerIssueDeleted(issue);

    ok(getIssueStub.calledWithExactly(issue.id));
    equal($issueRow.attr("style"), "display: none;", "Issue row is hidden after deleting");
    ok(this.splitScreenLayout.navigatorContent.hasClass("pending"), "Split view is dimmed after deleting an issue");

    getIssueStub.restore();
});