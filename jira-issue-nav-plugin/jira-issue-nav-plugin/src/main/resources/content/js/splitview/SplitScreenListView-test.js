AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav-common");

module("JIRA.Issues.SplitScreenListView", {
    setup: function() {
        this.searchModule = new JIRA.Issues.MockSearchModule();
        this.searchResults = this.searchModule.getResults();
        this.splitScreenListView = new JIRA.Issues.SplitScreenListView({search: this.searchModule});
    }
});

test("Changing the highlighted issue doesn't result in a search", function () {
    this.searchResults.triggerHighlightedIssueChange();
    ok(!this.searchResults.getResultsForIds.called, "A search wasn't performed.");
});