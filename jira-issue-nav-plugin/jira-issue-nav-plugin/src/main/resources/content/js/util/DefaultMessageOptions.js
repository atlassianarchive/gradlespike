AJS.namespace('JIRA.Issues.getDefaultMessageOptions', window, function() {
    return {
        closeable: true,
        timeout: 5
    };
});