
/**
* Removes semitransparent DIV
* @see AJS.dim
*/
AJS.undim = function() {
    if (AJS.dim.$dim) {
        AJS.dim.$dim.remove();
        AJS.dim.$dim = null;
        if (AJS.dim.shim) {
            AJS.dim.shim.remove();
        }

        // IE needs the overflow on the HTML element so scrollbars are hidden
        if (AJS.$.browser.msie && parseInt(AJS.$.browser.version,10) < 8) {
            AJS.$("html").removeAttr("style");
        } else {
            AJS.$("body").removeAttr("style");
        }
    }
};