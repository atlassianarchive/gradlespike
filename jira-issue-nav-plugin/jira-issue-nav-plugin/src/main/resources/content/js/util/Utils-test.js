AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");


module("JIRA.Issues.Utils.debounce", {
    setup:function () {
        this.myView = {
            callCount: 0,
            render: function () {
                this.callCount++;
            }
        };
    }
});

test("first call executes straight away", function () {
    JIRA.Issues.Utils.debounce(this.myView, "render");
    equal(this.myView.callCount, 1, "Expected render to be called straight away");
});

test("debounce defers execution until input stops", function () {
    var clock = sinon.useFakeTimers();
    JIRA.Issues.Utils.debounce(this.myView, "render");

    for (var i = 0; i < 100; i++) {
        window.setTimeout(_.bind(function () {
            this.myView.render();
        }, this), 10);
    }
    clock.tick(1000);

    equal(this.myView.callCount, 2, "Expected render to be called straight away");
    clock.restore();
});

test("debounce stops debouncing after 500ms of last execution", function () {
    var clock = sinon.useFakeTimers();
    JIRA.Issues.Utils.debounce(this.myView, "render");

    for (var i =0; i < 100; i++) {
        window.setTimeout(_.bind(function () {
            this.myView.render();
        }, this), 10)
    }
    clock.tick(1500);

    this.myView.render();

    equal(this.myView.callCount, 3, "Expected render to be called straight away");
    clock.restore();
});


test("debounce does not apply twice if called consecutively", function () {
    var clock = sinon.useFakeTimers();

    JIRA.Issues.Utils.debounce(this.myView, "render");
    JIRA.Issues.Utils.debounce(this.myView, "render");
    JIRA.Issues.Utils.debounce(this.myView, "render");
    JIRA.Issues.Utils.debounce(this.myView, "render");
    JIRA.Issues.Utils.debounce(this.myView, "render");
    JIRA.Issues.Utils.debounce(this.myView, "render");
    JIRA.Issues.Utils.debounce(this.myView, "render");
    JIRA.Issues.Utils.debounce(this.myView, "render");
    JIRA.Issues.Utils.debounce(this.myView, "render");
    JIRA.Issues.Utils.debounce(this.myView, "render");

    for (var i = 0; i < 100; i++) {
        window.setTimeout(_.bind(function () {
            this.myView.render();
        }, this), 10)
    }
    clock.tick(1000);

    equal(this.myView.callCount, 2, "Expected delay not to accumulate");
    clock.restore();
});

test("patch restores non-existent attributes", function () {
    var obj = {},
        restore;

    restore = JIRA.Issues.Utils.patch(obj, "foo", "bar");
    restore();

    equal("foo" in obj, false, "key 'foo' shouldn't exist");
});

test("patch replaces attribute", function () {
    var obj = {};

    JIRA.Issues.Utils.patch(obj, "foo", "bar");
    equal(obj.foo, "bar");
});

test("patch restores original attributes", function () {
    var obj = {foo: "baz"},
        restore;

    restore = JIRA.Issues.Utils.patch(obj, "foo", "bar");
    equal(obj.foo, "bar");

    restore();
    equal(obj.foo, "baz");
});

