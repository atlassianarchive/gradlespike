
JIRA.Issues.LoginUtils = {

    /**
     * Returns true if the current user is logged in
     */
    isLoggedIn: function() {
        return !!this.loggedInUserName();
    },

    /**
     * Returns the name of the currently logged in user
     */
    loggedInUserName: function() {
        return AJS.Meta.get('remote-user');
    },

    /**
     * Returns the path of the current page, relative to the contextPath
     * eg if we are at http://localhost:2990/jira/issues/?jql=blah#pos, returns /issues?jql=blah#pos
     */
    currentContextRelativePath: function() {
        var loc = this._getWindowLocation();
        var start = loc.protocol + "//" + loc.host + (contextPath || '');
        var href = loc.href;
        if (href.indexOf(start) === 0) {
            return href.substr(start.length);
        }
        return href;
    },

    /**
     * Returns url to redirect to the current page (eg /login.jsp?os_destination=[encodedCurrentUrl]
     * @returns login url
     */
    redirectUrlToCurrent: function() {
        var url = this.currentContextRelativePath();
        return contextPath + '/login.jsp?os_destination=' + encodeURIComponent(url)
    },

    /**
     * Returns url to redirect to (eg /login.jsp?os_destination=[encodedUrl]
     * @param url url to redirect to after user has logged in
     * @returns login url
     */
    redirectUrl: function(url) {
        return AJS.contextPath() + '/login.jsp?os_destination=' + encodeURIComponent(url)
    },

    /**
     * Returns window.location; provided to make mocking out easier
     */
    _getWindowLocation: function() {
        return window.location;
    }
};
