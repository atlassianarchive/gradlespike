// Define JIRA.Issues manually so we don't have to depend on AJS.
JIRA = (typeof JIRA == "undefined") ? {} : JIRA;
JIRA.Issues = JIRA.Issues || {};

JIRA.Issues.HashRewrite = {
    /**
     * Translate an /i-root URL to a normal URL.
     *
     * @param {string} path The URL's path (not including query string).
     * @param {string} query The URL's query string.
     * @param {string} fragment The URL's fragment.
     * @return {string} The translated URL.
     */
    rewrite: function(path, query, fragment) {
        // We can assume that the last two characters of path are /i as this file is only served from there.
        var contextPath = path.slice(0, -1);
        var path = fragment.replace("#", "");

        // Extract the params from the queryString.
        var queryParams = query.replace('?', path.indexOf("?") < 0 ? "?" : "&");

        return contextPath + path + queryParams;
    }
}