AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:common");

module('JIRA.Issues.Feature');

test("Feature.enabled() delegates to AJS.DarkFeatures.isEnabled()", function() {
    var feature = new JIRA.Issues.Feature('foo.bar');

    ok(!feature.enabled(), "foo.bar=off => DISABLED");
    JIRA.Issues.TestUtils.darkFeature('foo.bar', true, function() {
        ok(feature.enabled(), "foo.bar=on => ENABLED");
    }, this);
});