JIRA.Issues.Actions = JIRA.Issues.Actions || {};
JIRA.Issues.Actions.DELETE= "delete";
JIRA.Issues.Actions.UPDATE= "update";
JIRA.Issues.Actions.INLINE_EDIT= "inlineEdit";
JIRA.Issues.Actions.ROW_UPDATE= "rowUpdate";

JIRA.Issues.XsrfTokenHeader = {
    "X-Atlassian-Token": "nocheck"
};

JIRA.Issues.Utils = {


    isIssueDialog: function (dialog) {
        return  !!dialog.issueId || _.include(JIRA.Dialogs, dialog);
    },

    isDeleteDialog: function (dialog) {
        return dialog.options.id === "delete-issue-dialog";
    },


    isEditCommentDialog: function (dialog) {
        return dialog.options.id === "edit-comment";
    },

    getEffectiveIssueKeyForDialog: function (dialog) {
        // Use the key from the trigger link that was used to create the
        // dialog. If that's not there, then grab it from the issue table.
        var keyFromTrigger = dialog && dialog.$activeTrigger && dialog.$activeTrigger.data("issuekey");
        if (keyFromTrigger) {
            return keyFromTrigger;
        } else {
            return JIRA.Issues.Api.getSelectedIssue().getKey();
        }
    },
    getEffectiveIssueIdForDialog: function (dialog) {
        if (dialog.issueId) {
            return dialog.issueId;
        } else {
            return JIRA.Issues.Api.getSelectedIssue().getId();
        }
    },

    getAction: function (dialog) {
        if (this.isDeleteDialog(dialog)) {
            return JIRA.Issues.Actions.DELETE;
        } else if (this.isEditCommentDialog(dialog)) {
            return JIRA.Issues.Actions.EDIT_COMMENT;
        } else {
            return JIRA.Issues.Actions.UPDATE;
        }
    },

    getMessage: function (dialog) {
        if (!this.isEditCommentDialog(dialog)) {
            return dialog.options.issueMsg || "thanks_issue_updated";
        }
    },
    getMeta: function (dialog) {
        if (this.isEditCommentDialog(dialog)) {
            return {
                commentId: dialog.$activeTrigger.attr("id").replace("edit_comment_", "")
            };
        } else {
            return {};
        }
    },
    getUpdateCommandForDialog: function (dialog) {
        var utils = this;
        if (this.isIssueDialog(dialog)) {
            return {
                key: utils.getEffectiveIssueKeyForDialog(dialog),
                id: utils.getEffectiveIssueIdForDialog(dialog),
                action: utils.getAction(dialog),
                message: utils.getMessage(dialog),
                meta: utils.getMeta(dialog)
            }
        }
    },
    /**
     * A debounce implementation the differs from the undercore one. This implementation:
     * 1. Executes the supplied method straight away
     * 2. Using underscore debounce, postpones its execution until 300ms since the last time it was invoked.
     * 3. After the debounced invocation, waits 500ms before restoring to the original state (step 1).
     *
     * @param {Object} ctx
     * @param {String} method
     * @param {...} arguments for first invocation.
     */
    debounce: function (ctx, method) {


        var args = Array.prototype.slice.call(arguments, 2);

        if (!ctx) {
            console.error("JIRA.Issues.Utils.debounce: ctx must be defined");
        }

        clearTimeout(ctx[method + "DebounceTimeout"]);

        // Invoke method, first time this will happen straight away. Subsequent times it will be calling the debounced
        // method.
        ctx[method].apply(ctx, args);

        if (!ctx[method + "DebounceTimeout"]) {
            ctx[method + "Original"] = ctx[method];
            ctx[method] = _.debounce(function () {
                return ctx[method + "Original"].apply(ctx, arguments);
            }, 300);
        }

        // After 500 ms or recieving input, get rid of debounced version.
        ctx[method + "DebounceTimeout"] = setTimeout(function () {
            ctx[method] = ctx[method + "Original"];
            ctx[method + "DebounceTimeout"] = null;
        }, 500);
    },
    waitForDeferreds: function (obj, iterator, ctx) {
        ctx = ctx || window;
        var promises = [];
        var deferred = jQuery.Deferred();

        _.each(obj, function () {
            var result = iterator.apply(ctx, arguments);
            if (result && result.promise) {
                promises.push(result);
            }
        });
        if (promises.length) {
            jQuery.when.apply(jQuery, promises).then(function () {
                deferred.resolveWith(ctx)
            });
        } else {
            deferred.resolveWith(ctx);
        }
        return deferred.promise();
    },
    resolvedPromise: function () {
        return jQuery.Deferred().resolve().promise();
    },

    /**
     * Patches an attribute on an object, and provides an easy way to 'undo'.
     *
     * @param {object} object
     * @param {string} name The attribute to patch
     * @param {object} replacement
     * @return {Function} A 'restore' function that reverts the patch.
     */
    patch: function (object, name, replacement) {
        var hadOriginal = object.hasOwnProperty(name),
            original = object[name];

        object[name] = replacement;

        return function restore() {
            if (hadOriginal) {
                object[name] = original;
            } else {
                delete object[name];
            }
        }
    }
};
