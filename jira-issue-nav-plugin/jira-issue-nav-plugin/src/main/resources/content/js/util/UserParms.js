/**
 * Parameters relating to the permissions of the current user
 */
JIRA.Issues.UserParms = {

    createSharedObjects: function() {
        return this.get().createSharedObjects;
    },

    get: function() {
        if (!this.data) {
            this.data = AJS.$("#user-parms").data("user-parms") || {};
        }
        return this.data;
    }
};
