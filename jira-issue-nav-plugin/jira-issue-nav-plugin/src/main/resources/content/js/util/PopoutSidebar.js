(function($) {

    $.widget( "ui.popoutSidebar",  {
        version: "0.1",

        options: {
            /**
             * {Function} returns html that is to be rendered when in expanded mode
             */
            renderExpanded: $.noop,
            /**
             * {Function} returns html that is to be rendered when in collapsed mode
             */
            renderCollapsed: $.noop,
            /**
             * {Function} is used to save the docked/undocked state
             */
            persist: $.noop,
            /**
             * {number} width when collapsed
             */
            collapsedWidth: 30,
            /**
             * {number} width when expanded
             */
            expandedWidth: 200,
            /**
             * {Function} returns maximum width sidebar can be extended to
             */
            minWidth: function () { return 200; },
            /**
             * {Function} returns maximum width sidebar can be extended to
             */
            maxWidth: function () { return 500; },
            /**
             * {Function} is used to perform needed operations when the sidebar is being resized
             */
            resize: $.noop,
            /**
             * {string} text to be rendered for dock link
             */
            dockText: "Dock",
            /**
             * {string} text to be rendered for undock link
             */
            undockText: "Undock",
            /**
             * {string} the dock link message when hovered over
             */
            dockTitle: "Dock the filter panel so you can always see the list of filters",
            /**
             * {string} the undock link message when hovered over
             */
            undockTitle: "Undock the filter panel so it is hidden when not being focused"
        },

        CLASS_NAMES: {
            popout: "ui-popout",
            placeholder: "ui-popout-placeholder",
            detached: "ui-popout-detached", // This guy makes the sidebar fixed positioned
            expanding: "ui-popout-expanding",
            expanded: "ui-popout-expanded",
            collapsed: "ui-popout-collapsed",
            collapsing: "ui-popout-collapsing"
        },

        _create: function () {

            _.bindAll(this,
                "_handleDropdown",
                "_registerMousePos",
                "collapse",
                "expand",
                "updateOffsets");

            // avoid binding control twice
            if (!this.element.data("popoutsidebar")) {
                this.element.data("popoutsidebar", true);
                // you can trigger this event by jQuery.event.trigger("updateOffsets.popout")
                this.element.bind("updateOffsets.popout", this.updateOffsets);
                this.element.delegate(".ui-dock", "click", _.bind(function (e) {
                    this.dock(true);
                    e.preventDefault();
                }, this));
                this.element.delegate(".ui-undock", "click", _.bind(function (e) {
                    this.undock(true);
                    e.preventDefault();
                }, this));
                this.isDocked = this.options.isDocked;
                this.expandedWidth = this.options.expandedWidth;
                this.useTransitions = !jQuery.browser.msie || parseInt(jQuery.browser.version, 10) > 8;

                if (this.options.isDocked) {
                    // If not we just render the undock link
                    this._renderUnDock();
                } else {
                    // If we are docked we add all the hover handlers etc
                    this._enableUndockedMode();
                }

                // Ensure the height and positioning of the sidebar is correct
                this.updateOffsets();
            }
        },

        _init: function() {
            if (this.options.isDocked) {
                this._setupHandle();
            }
        },

        /**
         * Persists the state of the sidebar to the user configured colapsed/expanded state and width
         * so it works across page refreshes.
         * @private
         */
        _persistState: function () {
            this.expandedWidth = this.element.outerWidth();
            this.options.persist(this.isDocked, this.expandedWidth);
        },

        /**
         * Sets the min & max width boundaries for dragging side bar
         * @private
         */
        _setContainment: function () {
            var windowHeight = jQuery(window).height();
            this._elementLeft = this.element.offset().left;
            this._minLeft = this._elementLeft + this.options.minWidth();
            this._maxLeft = Math.max(this._minLeft, this._elementLeft + this.options.maxWidth());
            this.handle.draggable({containment: [this._minLeft, windowHeight,  this._maxLeft, windowHeight]});
            this.handle.css("position", "absolute"); // jQuery UI sets position relative on detached elements
        },

        /**
         * Sets the width of sidebar taking into account the min/max width boundaries
         * @param {integer} target
         * @param {boolean} force
         * @private
         */
        _setWidth: function (target) {
            target = Math.min(Math.max(this.options.minWidth(), target), this.options.maxWidth());
            this.element.width(target);
        },

        /**
         * Adds drag handle to the sidebar and sets events up
         * @private
         */
        _setupHandle: function () {
            this._getContents(); // ensure proper wrapper element is in place before attaching the drag handle
            this.handle = jQuery("<div />").addClass("ui-sidebar").appendTo(this.element);
            this._setWidth(this.expandedWidth);
            this._setHandlePosition();
            this.handle.mousedown(_.bind(this._setContainment, this));
            this.handle.draggable({
                axis: "x",
                drag: _.bind(this._handleDrag, this),
                stop: _.bind(this._persistState, this)
            });
        },

        /**
         * Aligns the drag handler to the sidebar
         * @private
         */
        _setHandlePosition: function () {
            this._setContainment();
            if (this._minLeft === this._maxLeft) {
                this.handle.hide();
            } else {
                var elementOffset = this.element.offset();
                var left = elementOffset.left + this.element.outerWidth();
                this.handle.css({
                    left: left
                }).show();
            }
        },

        /**
         * Sets the width of sidebar while dragging the handle
         * @param {Object} e
         * @param {Object} ui
         * @private
         */
        _handleDrag: function (e, ui) {
            var target = ui.position.left;
            this._setWidth(target);
            this.options.resize();
        },

        /**
         * Returns the list of classes when docked
         * @return {String}
         * @private
         */
        _getUndockedClasses: function () {
            return [this.CLASS_NAMES.detached].join(" ");
        },

        /**
         * Returns the possible states that the undocked filter panel can be in.
         * @returns {string}
         * @private
         */
        _getUndockedStates: function() {
            return [this.CLASS_NAMES.collapsed, this.CLASS_NAMES.collapsing, this.CLASS_NAMES.expanding, this.CLASS_NAMES.expanded].join(" ");
        },

        /**
         * Remove all current undocked states and change it to the defined state.
         * @param stateClass
         * @private
         */
        _setUndockedState: function(stateClass) {
            this.element.removeClass(this._getUndockedStates());
            this.element.addClass(stateClass);
        },


        /**
         * Hover intent doesn't offer us a way to disable it nicely, so we are doing it in hacky way
         * @private
         */
        unbindHoverIntent: function () {
            this.element.off("mouseleave mouseenter mousemove");
            this.element[0].hoverIntent_s = 0;
        },


        /**
         * Applies hover intent
         * http://cherne.net/brian/resources/jquery.hoverIntent.html
         * @private
         */
        bindHoverIntent: function () {
            this.element.hoverIntent({
                interval: 40,
                over: this.expand,
                out: this.collapse,
                sensitivity: 8
            });
        },

        /**
         * Unbinds all the events we have for undocked state
         * @private
         */
        _unbindUnDockedEvents: function () {
            this.unbindHoverIntent();
            JIRA.unbind(AJS.InlineLayer.EVENTS.show, this._handleDropdown);
            $(window).unbind("scroll", this.updateOffsets);
        },

        /**
         * Binds all events needed for undocked state
         * @param {Boolean} isExpanded - Whether when we bind the events the panel is in expanded state
         * @private
         */
        _bindUnDockedEvents: function (isExpanded) {
            this.bindHoverIntent();
            $(window).scroll(this.updateOffsets);
            this.element[0].hoverIntent_s = isExpanded ? 1 : 0;
        },


        /**
         * Is the sidebar undocked
         * @return {Boolean}
         */
        isUndocked: function () {
            return this.element.hasClass(this.CLASS_NAMES.detached);
        },

        _registerMousePos: function (e) {
            this._clientX = e.clientX;
            this._clientY = e.clientY;
        },

        /**
         * If we open a dropdown from within the sidebar, then move our mouse outside the region to select
         * an item in the dropdown, the sidebar collapses. This is a workaround to avoid that behaviour.
         * @private
         */
        _handleDropdown: function () {

            $(document).mousemove(this._registerMousePos);

            if (this.isUndocked() && AJS.InlineLayer.current.offsetTarget().closest(this.element).size()) {
                // DODGY unbind of hover intent. As unbind is unsupported by library.
                this.unbindHoverIntent();
                JIRA.one(AJS.InlineLayer.EVENTS.hide, _.bind(function () {
                    _.defer(_.bind(function () {
                        if (!AJS.InlineLayer.current) {
                            if (JIRA.Dialog.current || this._clientX > this.element.offset().left + this.element.outerWidth()) {
                                // If selecting something in dropdown invokes a dialog we collapse without animation
                                this.collapse(JIRA.Dialog.current ? false : true);
                                // Restore mouseenter, mouseleave events
                                this.bindHoverIntent();
                            } else {
                                // We have clicked inside the sidebar, so we don't want to collapse, just restore events.
                                this.bindHoverIntent();
                                // Manually set hover intent state to reflect "mouseentered state"
                                this.element[0].hoverIntent_s = 1;
                            }
                            $(document).unbind("mousemove", this._registerMousePos);
                        }
                    }, this))

                }, this));
            }
        },

        /**
         * Updates the position and height of the sidebar. You can trigger this method by using the event
         * jQuery.event.trigger("updateOffsets.sidebar").
         * @param {Number} width - If provided will set the width of the sidebar also
         */
        updateOffsets: function (width) {
            if (this.element.is(":visible")) {
                var isScrollable = $("body").height() > $(window).height();
                if (isScrollable) {
                    this.element.addClass("ui-sidebar-scrollable");
                } else {
                    this.element.removeClass("ui-sidebar-scrollable");
                }
                if (this.isUndocked()) {
                    var placeholderTop = this._getPlaceholder().offset().top;
                    var scrollTop = jQuery(window).scrollTop();
                    var offset = Math.max(placeholderTop - scrollTop, 0);
                    var height = isScrollable ?
                            $(window).height() - offset :
                            Math.min(this._getPlaceholder().outerHeight(), $(window).height() - this._getPlaceholder().offset().top);
                    var props = {top: offset, height: height};
                    if (width) {
                        props.width = width;
                    }
                    this.element.css(props);
                }
            }
        },

        /**
         * Renders undock link at bottom of sidebar
         * @private
         */
        _renderUnDock: function () {
            if (this.isDocked && !this.element.find(".ui-undock").size()) {
                this.element.find(".ui-dock").remove();
                this._renderDockingLink(this.options.undockText, this.options.undockTitle, "ui-undock");
            }
        },

        /**
         * Renders doc link at bottom of sidebar
         * @private
         */
        _renderDock: function () {
            if (this.isUndocked() && !this.element.find(".ui-dock").size()) {
                this.element.find(".ui-undock").remove();
                this._renderDockingLink(this.options.dockText, this.options.dockTitle, "ui-dock");
            }
        },

        /**
         * Renders the actual link with specific text and titles.
         * @param text of link
         * @param title to display when hovering
         * @param dockClass css class of docking link.
         * @private
         */
        _renderDockingLink: function(text, title, dockClass) {
            var link = $("<a class='aui-button aui-button-subtle' href='#'/>")
                    .addClass(dockClass)
                    .append($("<span class='icon'/>").text(text))
                    .attr("title", title);

            this.dockTipsy = new JIRA.Issues.Tipsy({ el: link });

            this._getContents().find(this.options.toggleTarget).append(link);
        },

        /**
         * Renders the correct undock or dock link at the bottom of sidebar
         */
        renderDockState: function () {
            if (this.isDocked) {
                this._renderUnDock();
            } else {
                this._renderDock();
            }
        },

        /**
         * Enables undocked mode by appending element to the body and applying all the correct events.
         *
         * @param {Boolean} isExpanded - is the sidebar currently expanded
         * @private
         */
        _enableUndockedMode: function (isExpanded) {
            this.element.addClass(this.CLASS_NAMES.popout);
            if (!this.isUndocked()) {
                this._getPlaceholder().insertBefore(this.element);
                this.element.addClass(this.CLASS_NAMES.detached).addClass(this.CLASS_NAMES.collapsed).appendTo("body");
            }
            this.updateOffsets();
            this._bindUnDockedEvents(isExpanded);

            JIRA.bind(AJS.InlineLayer.EVENTS.show, this._handleDropdown);
        },

        /**
         * Returns placeholder, which holds the realestate, as our element is fixed positioned when in undocked mode.
         * @return {jQuery}
         * @private
         */
        _getPlaceholder: function () {
            if (!this.placeholder) {
                this.placeholder = $("<div class='navigator-sidebar collapsed' />");
            }
            return this.placeholder;
        },

        toggle: function () {
            //if not in a collapsed or expanded state
            if (!this.isExpanding() && !this.isCollapsing()) {
                if (this.isDocked) {
                    this.undock(false);
                } else {
                    this.dock(false);
                }
            }
        },

        /**
         * Undocks sidebar
         */
        undock: function (hasTransition) {
            if (!this.isUndocked()) {
                this.isDocked = false;
                if (this.dockTipsy) {
                    this.dockTipsy.remove();
                    this.dockTipsy = null;
                }
                this._persistState();
                this._getPlaceholder().insertBefore(this.element);
                this.element.addClass(this._getUndockedClasses() + " " + this.CLASS_NAMES.expanded).appendTo("body");
                if (hasTransition !== false && this.useTransitions) {
                    this._getPlaceholder().width(this.expandedWidth).animate({width: this.options.collapsedWidth}, 200);
                } else {
                    this._getPlaceholder().width(this.options.collapsedWidth);
                }
                this.updateOffsets(this.expandedWidth);
                this.collapse(hasTransition, _.bind(function () {
                    this._enableUndockedMode(false);
                }, this));

            }
        },

        /**
         * Docks sidebar
         */
        dock: function (hasTransition) {
            if (this.isUndocked()) {
                this.isDocked = true;
                if (this.dockTipsy) {
                    this.dockTipsy.remove();
                    this.dockTipsy = null;
                }
                this._unbindUnDockedEvents();
                this._persistState();
                if (hasTransition !== false && this.useTransitions) {
                    this._getPlaceholder().animate({width: this.expandedWidth}, 200, _.bind(this._dockingComplete, this));
                } else {
                    this._dockingComplete();
                }
            }
        },

        /**
         * Tasks to be completed when the docking process is complete.
         * @private
         */
        _dockingComplete: function() {
            this.element.removeClass(this._getUndockedClasses());
            this.element.removeClass(this._getUndockedStates());
            this.element.css({height: "", width: "", top: ""}).insertBefore(this._getPlaceholder());
            this._getPlaceholder().remove();
            this.options.renderExpanded(this.element);
            this._setupHandle();
            this._renderUnDock();
            JIRA.Issues.triggerHorizontalResize();
        },

        /**
         * Wraps the contents of the sidebar in a div. We use this to fade in the contents
         * @return {*}
         * @private
         */
        _getContents: function () {
            if (!this.element.find(".ui-sidebar-content").size()) {
                this.element.wrapInner("<div class='ui-sidebar-content' />");
            }
            return this.element.find(".ui-sidebar-content");
        },

        /**
         * Handles the transition to expanded
         * @private
         */
        expand: function (animate) {
            if (this.isCollapsed()) {
                this.updateOffsets();
                if (animate !== false && this.useTransitions) {
                    this._setUndockedState(this.CLASS_NAMES.expanding);
                    this.element.animate({width: this.expandedWidth}, 150, _.bind(function() {
                        this._setUndockedState(this.CLASS_NAMES.expanded);
                        this.options.renderExpanded(this.element);
                        this._renderDock();
                        this._getContents().css("opacity", 0).animate({opacity: 1}, 200, _.bind(this._expandingComplete, this));
                    }, this));
                } else {
                    this.element.css("width", this.expandedWidth);
                    this.options.renderExpanded(this.element);
                    this._renderDock();
                    this._expandingComplete();
                }
            } else if (this.isCollapsing()) {
                this.activityAfterTransition = "expand";
            } else if (this.isExpanding()) {
                this.activityAfterTransition = null;
            }
        },

        /**
         * Executed after the filter panel has finished expanding.
         * @private
         */
        _expandingComplete: function() {
            JIRA.Issues.triggerHorizontalResize();
            this._bindUnDockedEvents(true);
            this._setUndockedState(this.CLASS_NAMES.expanded);
            if (this.activityAfterTransition === "collapse") {
                this.activityAfterTransition = null;
                this.collapse(true);
            }
        },

        /**
         * Handles the transition to collapsed
         * @private
         */
        collapse: function (animate, callback) {
            if (this.isExpanded()) {
                if (animate !== false && this.useTransitions) {
                    this._setUndockedState(this.CLASS_NAMES.collapsing);
                    this._getContents().animate({opacity: 0}, 150, _.bind(function() {
                        this.options.renderCollapsed(this.element);
                        this.element.width(this.expandedWidth);
                        this.element.animate({width: this.options.collapsedWidth}, 150, _.bind(function () {
                            this._collapsingComplete(callback);
                        }, this));
                    }, this));
                } else {
                    this.options.renderCollapsed(this.element);
                    this.element.width("");
                    this._collapsingComplete(callback);
                }
            } else if (this.isExpanding()) {
                this.activityAfterTransition = "collapse";
            } else if (this.isCollapsing()) {
              this.activityAfterTransition = null;
            }
        },

        /**
         * This is executed after collapsing has completed.
         * @private
         */
        _collapsingComplete: function(callback) {
            this._setUndockedState(this.CLASS_NAMES.collapsed);
            JIRA.Issues.triggerHorizontalResize();
            this._bindUnDockedEvents(false);
            if (this.activityAfterTransition === "expand") {
                this.activityAfterTransition = null;
                this.expand(true);
            }
            if (typeof callback === "function") {
                callback();
            }
        },

        isCollapsed: function() {
            return this.element.hasClass(this.CLASS_NAMES.collapsed);
        },

        isCollapsing: function() {
            return this.element.hasClass(this.CLASS_NAMES.collapsing);
        },

        isExpanded: function() {
            return this.element.hasClass(this.CLASS_NAMES.expanded);
        },

        isExpanding: function() {
            return this.element.hasClass(this.CLASS_NAMES.expanding);
        }
    });

}(jQuery));