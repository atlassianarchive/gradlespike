AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:hash-rewrite");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:common");


module("HashRewrite", {});

test("Should redirect to non-hash url", function() {

    function equalURL(result, expected, msg) {

        var resultObj = {
            path: result.substring(result.indexOf("?"),-1),
            query: JIRA.Issues.QueryStringParser.parse(result.substring(result.indexOf("?")))
        };

        var expectedObj = {
            path: expected.substring(expected.indexOf("?"),-1),
            query: JIRA.Issues.QueryStringParser.parse(expected.substring(expected.indexOf("?")))
        };

        equal(resultObj.path, expectedObj.path, (msg?msg:"") + " (url)");
        deepEqual(resultObj.path, expectedObj.path, (msg?msg:"") + " (query string)");
    }

    equalURL(JIRA.Issues.HashRewrite.rewrite("/i", "?jwupdated=123", "#issues/"), "/issues/?jwupdated=123",
        "Issues URL without context path and white-listed parameters"
    );
    equalURL(JIRA.Issues.HashRewrite.rewrite("/i", "", "#issues/?jql=KEY-123"), "/issues/?jql=KEY-123",
        "Issues URL without context path and regular parameters"
    );
    equalURL(JIRA.Issues.HashRewrite.rewrite("/i", "?jwupdated=123", "#issues/?jql=KEY-123"), "/issues/?jql=KEY-123&jwupdated=123",
        "Issues URL without context path, regular and white-listed parameters"
    );

    equalURL(JIRA.Issues.HashRewrite.rewrite("/i", "?jwupdated=123", "#browse/KEY-123"), "/browse/KEY-123?jwupdated=123",
        "Browse URL without context path and white-listed parameters"
    );
    equalURL(JIRA.Issues.HashRewrite.rewrite("/i", "", "#browse/KEY-123?jql=KEY-123"), "/browse/KEY-123?jql=KEY-123",
        "Browse URL without context path and regular parameters"
    );
    equalURL(JIRA.Issues.HashRewrite.rewrite("/i", "?jwupdated=123", "#browse/KEY-123?jql=KEY-123"), "/browse/KEY-123?jql=KEY-123&jwupdated=123",
        "Browse URL without context path, regular and white-listed parameters"
    );


    equalURL(JIRA.Issues.HashRewrite.rewrite("/jira/i", "?jwupdated=123", "#issues/"), "/jira/issues/?jwupdated=123",
        "Issues URL whit context path and white-listed parameters"
    );
    equalURL(JIRA.Issues.HashRewrite.rewrite("/jira/i", "", "#issues/?jql=KEY-123"), "/jira/issues/?jql=KEY-123",
        "Issues URL whit context path and regular parameters"
    );
    equalURL(JIRA.Issues.HashRewrite.rewrite("/jira/i", "?jwupdated=123", "#issues/?jql=KEY-123"), "/jira/issues/?jql=KEY-123&jwupdated=123",
        "Issues URL whit context path, regular and white-listed parameters"
    );

    equalURL(JIRA.Issues.HashRewrite.rewrite("/jira/i", "?jwupdated=123", "#browse/KEY-123"), "/jira/browse/KEY-123?jwupdated=123",
        "Browse URL whit context path and white-listed parameters"
    );
    equalURL(JIRA.Issues.HashRewrite.rewrite("/jira/i", "", "#browse/KEY-123?jql=KEY-123"), "/jira/browse/KEY-123?jql=KEY-123",
        "Browse URL whit context path and regular parameters"
    );
    equalURL(JIRA.Issues.HashRewrite.rewrite("/jira/i", "?jwupdated=123", "#browse/KEY-123?jql=KEY-123"), "/jira/browse/KEY-123?jql=KEY-123&jwupdated=123",
        "Browse URL whit context path, regular and white-listed parameters"
    );

});
