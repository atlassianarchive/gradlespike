AJS.namespace("JIRA.Issues.SessionTimedOutDialog");
JIRA.Issues.SessionTimedOutDialog = {

    dialogHasShown: false,

    whiteList: [
        "/secure/Search!Jql.jspa",
        "/secure/SearchRendererEdit!Default.jspa",
        "/rest/issueNav/1/issueTable",
        "/rest/issueNav/latest",
        "/rest/api/2/filter"
    ],

    onRequestResponse: function(e, resp, opts) {

        if (!this.dialogHasShown && this.urlIsInWhiteList(opts.url)) {
            var currentUserName = JIRA.Issues.LoginUtils.loggedInUserName(),
                responseUserName = resp.getResponseHeader("X-AUSERNAME");

            if (this.userHasChanged(currentUserName, responseUserName)) {
                this.dialogHasShown = true;
                var dialog = new JIRA.FormDialog({
                    content: AJS.$(JIRA.Templates.Issues.Util.anonymousErrorDialog({
                        modifierKey: AJS.Navigator.modifierKey(),
                        redirect: window.location.href.substring(window.location.href.indexOf(contextPath) + contextPath.length)
                    }))
                });
                dialog.show();
            }
        }
    },

    userHasChanged: function(currentUserName, responseUserName) {
        // User is anonymous and page has loaded in that state
        // Or possibly the user logged in and out elsewhere ~ however this is a bit edgecasey.
        if (currentUserName === "" && responseUserName === "anonymous") {
            return false;
        }

        // User loaded the page as anonymous and logged in elsewhere
        if (currentUserName === "" && responseUserName && responseUserName !== "anonymous" && responseUserName !== "") {
            return false;
        }

        // User was previously logged in and now their username is now anonymous (session timed out).
        if (currentUserName !== "" && responseUserName === "anonymous") {
            return true;
        }

        return false;
    },

    urlIsInWhiteList: function(url) {
        for (var i = 0; i < this.whiteList.length; i++) {
            var whiteListUrl = this.whiteList[i];
            if (url.indexOf(whiteListUrl) > -1) {
                return true;
            }
        }

        return false;
    }
};

(function($) {
    $(document).ajaxComplete(function(e, resp, opts) {
        JIRA.Issues.SessionTimedOutDialog.onRequestResponse(e, resp, opts);
    });
})(AJS.$);