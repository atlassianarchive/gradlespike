/**
 * A vertical resize event is triggered when the page changes in such a way that elements may need to resize themselves
 * vertically (e.g. an element is added/removed, or an element's height changes).
 */
(function () {
    var horizontalEventName = "horizontalResize",
        verticalEventName = "verticalResize";

    JIRA.Issues.offHorizontalResize = function (block) {
        AJS.$(document).off(horizontalEventName, block);
    };

    JIRA.Issues.onHorizontalResize = function (block) {
        AJS.$(document).on(horizontalEventName, block);
    };

    JIRA.Issues.triggerHorizontalResize = _.throttle(function () {
        AJS.$(document).trigger(horizontalEventName);
    }, 100);

    JIRA.Issues.offVerticalResize = function (block) {
        AJS.$(document).off(verticalEventName, block);
    };

    JIRA.Issues.onVerticalResize = function (block) {
        AJS.$(document).on(verticalEventName, block);
    };

    JIRA.Issues.triggerVerticalResize = _.throttle(function () {
        AJS.$(document).trigger(verticalEventName);
    }, 100);

    jQuery(window).resize(JIRA.Issues.triggerVerticalResize);
}());