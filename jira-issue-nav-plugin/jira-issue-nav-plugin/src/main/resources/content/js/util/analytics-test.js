AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:viewissue");

module("analytics", {
    setup: function() {
        this.sandbox = sinon.sandbox.create();
        this.orignalApplication = JIRA.Issues.Application;
        JIRA.Issues.Application = new JIRA.Marionette.Application();
        this.app = JIRA.Issues.Application;

        JIRA.Issues.AnalyticsLoader();
    },

    teardown: function () {
        this.sandbox.restore();
        JIRA.Issues.Application = this.orignalApplication;
    }
});

test("Should trigger kickass.viewIssue analytics event on loadComplete only when issue is not fetched from cache", function() {
    this.sandbox.stub(JIRA.Issues.Application, "execute");

    var mockModel = {
        getId : function(){return 123;}
    };

    this.app.trigger("issueEditor:loadComplete", mockModel, {fromCache: false});
    ok(this.app.execute.calledTwice, "Application.execute was called twice");
    ok(this.app.execute.calledWith("analytics:trigger", "kickass.viewIssue"), "kickass.viewIssue event was fired");
    ok(this.app.execute.calledWith("analytics:trigger", "kickass.issueLoadDuration"), "kickass.issueLoadDuration event was fired");
    this.app.execute.reset();

    this.app.trigger("issueEditor:loadComplete", mockModel, {fromCache: true});
    ok(this.app.execute.calledOnce, "Application.execute was called once");
    ok(this.app.execute.calledWith("analytics:trigger", "kickass.issueLoadDuration"), "kickass.issueLoadDuration event was fired");
});

test("Should trigger kickass.viewIssue analytics event on render only when issue is loaded from dom", function() {
    this.sandbox.stub(JIRA.Issues.Application, "execute");

    this.app.trigger("issueEditor:render", null, {loadedFromDom: false});
    ok(!this.app.execute.called, "none of the events should be fired")
    this.app.execute.reset();

    this.app.trigger("issueEditor:render", null, {loadedFromDom: true});
    ok(this.app.execute.calledOnce, "Application.execute was called once");
    ok(this.app.execute.calledWith("analytics:trigger", "kickass.viewIssue"), "kickass.viewIssue event was fired");
});

test("Should trigger kickass.inlineEdit analytics event when a field has been saved", function() {
    this.sandbox.stub(JIRA.Issues.Application, "execute");

    var props = {
        issueId: 123,
        savedFieldIds: ["summary"],
        duration: 456
    };
    this.app.trigger("issueEditor:saveSuccess", props);

    ok(this.app.execute.calledOnce, "Application.execute was called once");
    ok(this.app.execute.calledWith("analytics:trigger", "kickass.inlineEdit"), "kickass.inlineEdit event was fired");
    ok(this.app.execute.calledWith("analytics:trigger", "kickass.inlineEdit", props), "kickass.inlineEdit event includes the properties");
});

