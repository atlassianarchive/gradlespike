AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

module("JIRA.Issues.URLSerializer", {
    setup: function() {
        this.getState = function (state) {
            return _.extend({
                filter: null,
                jql: null,
                selectedIssueKey: null,
                startIndex: 0
            }, state);
        }

        this.testGetURLFromState = function (state, expectedURL, message) {
            var actualURL = this.URLSerializer.getURLFromState(state),
                actualComponents,
                expectedComponents;

            window.makeComponents = function(url) {
                var indexOf = url.indexOf("?");
                return {
                    path: indexOf != -1 ? url.substring(0, indexOf) : url,
                    query: indexOf != -1 ? JIRA.Issues.QueryStringParser.parse(url.substring(indexOf)) : {}
                }
            };

            actualComponents = makeComponents(actualURL);
            expectedComponents = makeComponents(expectedURL);

            deepEqual(actualComponents, expectedComponents, message);
        };

        this.URLSerializer = JIRA.Issues.URLSerializer;
    }
});

test("getURLFromState", function () {
    // Issue Search
    this.testGetURLFromState(this.getState(), "issues/");
    this.testGetURLFromState(this.getState({jql: ""}), "issues/?jql=");
    this.testGetURLFromState(this.getState({filter: 10000}), "issues/?filter=10000");
    this.testGetURLFromState(this.getState({jql: "", startIndex: 42}), "issues/?jql=&startIndex=42");
    this.testGetURLFromState(this.getState({filter: 10000, jql: ""}), "issues/?filter=10000&jql=");
    this.testGetURLFromState(this.getState({filter: 10000, jql: "", startIndex: 42}), "issues/?filter=10000&jql=&startIndex=42");

    // View Issue
    this.testGetURLFromState(this.getState({selectedIssueKey: "JRA-123"}), "browse/JRA-123");
    this.testGetURLFromState(this.getState({jql: "", selectedIssueKey: "JRA-123"}), "browse/JRA-123?jql=");
    this.testGetURLFromState(this.getState({jql: "", selectedIssueKey: "JRA-123", startIndex: 42}), "browse/JRA-123?jql=");
    this.testGetURLFromState(this.getState({filter: 10000, selectedIssueKey: "JRA-123"}), "browse/JRA-123?filter=10000");
    this.testGetURLFromState(this.getState({filter: 10000, jql: "", selectedIssueKey: "JRA-123"}), "browse/JRA-123?filter=10000&jql=");
    this.testGetURLFromState(this.getState({filter: 10000, jql: "", selectedIssueKey: "JRA-123", startIndex: 42}), "browse/JRA-123?filter=10000&jql=");

    // Ignored Parameters
    this.testGetURLFromState(this.getState({highlightedIssue: "JRA-123"}), "issues/");
});

test("getStateFromURL", function () {
    // Issue Search
    deepEqual(this.URLSerializer.getStateFromURL("issues/"), this.getState());
    deepEqual(this.URLSerializer.getStateFromURL("issues/?filter=10000"), this.getState({filter: "10000"}));
    deepEqual(this.URLSerializer.getStateFromURL("issues/?jql="), this.getState({jql: ""}));
    deepEqual(this.URLSerializer.getStateFromURL("issues/?jql=&startIndex=42"), this.getState({jql: "", startIndex: 42}));
    deepEqual(this.URLSerializer.getStateFromURL("issues/?filter=10000&jql="), this.getState({filter: "10000", jql: ""}));
    deepEqual(this.URLSerializer.getStateFromURL("issues/?filter=10000&jql=&startIndex=42"), this.getState({filter: "10000", jql: "", startIndex: 42}));

    // View Issue
    deepEqual(this.URLSerializer.getStateFromURL("browse/JRA-123"), this.getState({selectedIssueKey: "JRA-123"}));
    deepEqual(this.URLSerializer.getStateFromURL("browse/JRA-123?filter=10000"), this.getState({filter: "10000", selectedIssueKey: "JRA-123"}));
    deepEqual(this.URLSerializer.getStateFromURL("browse/JRA-123?jql="), this.getState({jql: "", selectedIssueKey: "JRA-123"}));
    deepEqual(this.URLSerializer.getStateFromURL("browse/JRA-123?jql=&startIndex=42"), this.getState({jql: "", selectedIssueKey: "JRA-123", startIndex: 42}));
    deepEqual(this.URLSerializer.getStateFromURL("browse/JRA-123?filter=10000&jql="), this.getState({filter: "10000", jql: "", selectedIssueKey: "JRA-123"}));
    deepEqual(this.URLSerializer.getStateFromURL("browse/JRA-123?filter=10000&jql=&startIndex=42"), this.getState({filter: "10000", jql: "", selectedIssueKey: "JRA-123", startIndex: 42}));

    // Ignored Parameters
    deepEqual(this.URLSerializer.getStateFromURL("browse/JRA-123?foo=bar"), this.getState({selectedIssueKey: "JRA-123"}));
});