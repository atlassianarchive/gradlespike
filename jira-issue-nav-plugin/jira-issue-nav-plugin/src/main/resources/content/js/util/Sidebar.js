/*!
 * jQuery UI Sidebar
 * http://jqueryui.com
 *
 * Depends:
 *   jquery.ui.widget.js
 */
;(function($) {


    $.widget( "ui.sidebar",  {
        version: "0.1",

        /* defaults */
        options: {
            minWidth: function (ui) { return  50; },
            maxWidth: function (ui) { return jQuery(window).width(); },
            resize: $.noop,
            easeOff: (jQuery.browser.msie && jQuery.browser.version <= 8) ? 500 : 0
        },

        /**
         * @constructor
         */
        _create: function() {
            _.bindAll(this,
                "_handleDrag",
                "_persist",
                "_setContainment",
                "_setHandlePosition",
                "updatePosition");

            if (this.options.easeOff) {
                this.updatePosition = _.debounce(this.updatePosition, this.options.easeOff);
            }
            if (!this.options.id) {
                console.error("ui.sidebar: You must specify an id")
            }
            // Create the handle before restoring anything, as the restore operation could fire some events that
            // might end using the handle before it is created. JRADEV-20661
            this._addHandle();
            this._restore();
            this.handle.mousedown(this._setContainment);
            this.handle.draggable({axis: "x", drag: this._handleDrag, stop: this._persist});
            $(window).resize(_.debounce(this.updatePosition, 30));
        },

        /**
         * Restores the sidebar to the user configured width
         * @private
         */
        _restore: function () {
            if (window.localStorage) {
                var width = localStorage.getItem("ui.sidebar." + this.options.id);
                if (width) {
                    this._setWidth(width);
                }
            }
        },

        /**
         * Persists the sidebar to the user configured width so it works across page refreshes.
         * @private
         */
        _persist: function () {
            if (window.localStorage) {
                localStorage.setItem("ui.sidebar." + this.options.id, this.element.outerWidth());
            }
        },

        /**
         * Sets the min & max width boundaries for dragging side bar
         * @private
         */
        _setContainment: function () {
            var windowHeight = jQuery(window).height();
            this._elementLeft = this.element.offset().left;
            this._minLeft = this._elementLeft + this.options.minWidth(this);
            this._maxLeft = Math.max(this._minLeft, this._elementLeft + this.options.maxWidth(this));
            this.handle.draggable({containment: [this._minLeft, windowHeight,  this._maxLeft, windowHeight]});
        },

        /**\
         * Sets the width of sidebar
         * @param {Object} e
         * @param {Object} ui
         * @private
         */
        _handleDrag: function (e, ui) {
            var target = ui.position.left - this._elementLeft;
            this._setWidth(target, true);
        },

        _setWidth: function (target, force) {
            if (!force) {
                var maxWidth = this.options.maxWidth(this);
                var minWidth = this.options.minWidth(this);
                if (target > maxWidth) {
                    target = maxWidth
                } else if (target < minWidth) {
                    target = minWidth;
                }
            }
            // JRADEV-20949 Assume box-sizing is border-box, adjusting target width for padding/border.
            // The reason this.element is not box-sizing: border-box is because Safari does not respect this
            // with display: table-cell
            target -= this.element.outerWidth() - this.element.width();
            this.element.width(target);
            this.options.resize(target);
        },


        /**
         * Appends a drag handle next to the sidebar
         * @private
         */
        _addHandle: function () {
            this.handle = jQuery("<div />").addClass("ui-sidebar").appendTo(this.element);
            _.defer(this._setHandlePosition);
        },

        /**
         * Aligns the drag handler to the sidebar
         * @private
         */
        _setHandlePosition: function () {
            this._setContainment();
            if (this._minLeft === this._maxLeft) {
                this.handle.hide();
            } else {
                var elOffset = this.element.offset();
                var left = elOffset.left + this.element.outerWidth();
                this.handle.css({
                    top: elOffset.top,
                    left: left,
                    height: this.element.outerHeight()
                }).show();
            }
        },

        /**
         * Updates position of handle. You trigger this externally by jQuery(".sidebar").sidebar("updatePosition")
         */
        updatePosition: function () {
            this._setHandlePosition();
            this._setWidth(this.handle.offset().left - this._elementLeft);
            this._persist();
        }
    });

})( jQuery );
