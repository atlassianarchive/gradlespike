AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:common");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:sinon");

module('JIRA.Issues.LoginUtils', {
    setup: function() {
        sinon.stub(JIRA.Issues.LoginUtils, "_getWindowLocation");
    },
    teardown: function() {
        JIRA.Issues.LoginUtils._getWindowLocation.restore();
    }
});

test("redirectUrl", function() {
    var r = JIRA.Issues.LoginUtils.redirectUrl;

    equal(AJS.contextPath()+"/login.jsp?os_destination=%2Fissues%2F", r("/issues/"));
    equal(AJS.contextPath()+"/login.jsp?os_destination=%2Fissues%2F%3F", r("/issues/?"));
    equal(AJS.contextPath()+"/login.jsp?os_destination=%2Fissues%2F%3Fa%3Db", r("/issues/?a=b"));
});

test("currentContextRelativePath", function() {
    var r = function() { return JIRA.Issues.LoginUtils.currentContextRelativePath(); };

    JIRA.Issues.LoginUtils._getWindowLocation.returns({
        href: "http://jira" + contextPath + "/blah",
        protocol: "http:",
        host: "jira"
    });

    equal(r(), "/blah");

    JIRA.Issues.LoginUtils._getWindowLocation.returns({
        href: "http://jira:2990" + contextPath + "/blah?que=ry&str=ing#hashcake",
        protocol: "http:",
        host: "jira:2990"
    });

    equal(r(), "/blah?que=ry&str=ing#hashcake");
});

test("redirectUrlToCurrent", function() {
    var r = function() { return JIRA.Issues.LoginUtils.redirectUrlToCurrent(); };

    JIRA.Issues.LoginUtils._getWindowLocation.returns({
        href: "http://jira" + contextPath + "/blah",
        protocol: "http:",
        host: "jira"
    });

    equal(r(), "/login.jsp?os_destination=%2Fblah");

    JIRA.Issues.LoginUtils._getWindowLocation.returns({
        href: "http://jira:2990" + contextPath + "/blah/?que=ry&str=ing#hashcake",
        protocol: "http:",
        host: "jira:2990"
    });

    equal(r(), "/login.jsp?os_destination=%2Fblah%2F%3Fque%3Dry%26str%3Ding%23hashcake");
});
