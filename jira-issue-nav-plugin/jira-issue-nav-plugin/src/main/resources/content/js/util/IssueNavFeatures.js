/**
 * The Feature class is used for querying a JIRA dark feature.
 */
AJS.namespace("JIRA.Issues.Feature");
JIRA.Issues.Feature = Class.extend({
    /**
     * Creates a new Feature object for the given feature key.
     *
     * @param featureKey {String} the name of the feature
     */
    init: function(featureKey) {
        this.featureKey = featureKey;
    },

    /**
     * Returns true if this Feature is enabled.
     *
     * @return {Boolean}
     */
    enabled: function() {
        return AJS.DarkFeatures.isEnabled(this.featureKey);
    }
});

/**
 * Holds the dark feature switches that affect how the KA app works.
 */
AJS.namespace("JIRA.Issues.DarkFeatures");
JIRA.Issues.DarkFeatures = {
    /**
     * If enabled, kills issue prefetching.
     */
    NO_PREFETCH: new JIRA.Issues.Feature("ka.NO_PREFETCH"),

    /**
     * If enabled, project's avatar will display a popup on click
     */
    PROJECT_SHORTCUTS: new JIRA.Issues.Feature("rotp.project.shortcuts")
};
