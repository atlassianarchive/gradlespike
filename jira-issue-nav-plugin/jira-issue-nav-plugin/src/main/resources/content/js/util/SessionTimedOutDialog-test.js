AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:common");

module('JIRA.Issues.SessionTimedOutDialog');

test("Dialog is shown when page user name is admin and response is anonymous", function() {
    var initialUser = "admin",
        newUser = "anonymous";
    equal(JIRA.Issues.SessionTimedOutDialog.userHasChanged(initialUser, newUser), true, "Dialog should show if different user is detected.");
});

test("Dialog is not shown when page user name is blank and response is anonymous", function() {
    var initialUser = "",
        newUser = "anonymous";
    equal(JIRA.Issues.SessionTimedOutDialog.userHasChanged(initialUser, newUser), false);
});

test("Dialog is not shown when page user name is blank and response is not anonymous and not blank", function() {
    var initialUser = "",
        newUser = "someoneelse";
    equal(JIRA.Issues.SessionTimedOutDialog.userHasChanged(initialUser, newUser), false);
});

test("Nonexistant URL is not in whitelist", function() {
    var url = "http://localhost:2990/jira/rest/api/1.0/menus/find_link?_=1347838927783";
    equal(JIRA.Issues.SessionTimedOutDialog.urlIsInWhiteList(url), false);
});

test("Existant URL is in whitelist", function() {
    var url = "http://localhost:2990/jira/secure/Search!Jql.jspa?decorator=none&jql=project+in+(AN%2C+BALI)&filter=10200&_=1347837403791";
    equal(JIRA.Issues.SessionTimedOutDialog.urlIsInWhiteList(url), true);
});