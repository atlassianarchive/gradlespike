AJS.namespace("JIRA.Issues.AnalyticsLoader");

JIRA.Issues.AnalyticsLoader = function() {
    /**
     * App Events.
     *
     * These events are triggered from within the application.
     * This is a form of register of events which the application can broadcast.
     *
     * This event list does not need to contain all of the names of the Client Events listed below passed to
     * "analytics:registerTracker"
     *
     * IMPORTANT: Only have one level of dots under kickass or they wont show up in graphite!
     */
    JIRA.Issues.Application.execute("analytics:registerEvent",[
        /* Any client side search triggers this (including details of the search */
        "kickass.search",

        /* Triggered when the user switches back to keyword search */
        "kickass.switchtobasic",

        /* Triggered when the user moves to JQL search */
        "kickass.switchtoadvanced",

        /* Triggered when the user hits return to search */
        "kickass.returntosearch",

        /* Records if an inline edit action took place. Also includes which field is being edited */
        "kickass.editField",

        /* If more than one field was edited on a view issue page (Note: the count resets only when a new issue is loaded, *not*
         * when the user clicked 'Save') */
        "kickass.editMultipleFields",

        /* Records the duration the user spends in edit mode until they hit save */
        "kickass.editClientDuration",

        /* Records the duration the user spends in edit mode until they hit cancel */
        "kickass.editClientCancelledDuration",

        /* Time it takes from hitting save to the view issue page having rerenderd on the client */
        "kickass.editSaveonserverDuration",

        /* Time it takes to load the view issue page */
        "kickass.issueLoadDuration",

        /* Triggered when the client opens an issue from the results table */
        "kickass.openIssueFromTable",

        /* Time it takes save an issue, from user click to full panel reblat */
        "kickass.issueTotalSaveDuration",

        /* When the user opens the focus shifter. */
        "kickass.focusshifteropened",

        /* When the query is too complex we need to switch to advanced mode */
        "kickass.queryTooComplexSwitchToAdvanced",

        /* The number of clauses when searching with basic mode */
        "kickass.basicModeCriteriaCountWhenSearching",

        /* Whether the user has auto update enabled */
        "kickass.criteriaAutoUpdateEnabled",

        /* When clicking on the refresh table button */
        "kickass.issueTableRefresh",

        /* When loading the issue from server conditionally */
        "kickass.conditionalLoading",

        /* When issue is rendered on the screen */
        "kickass.viewIssue",

        /* When issue is inline-edited */
        "kickass.inlineEdit"
    ]);

    /**
     * Track whether the user has auto updated enabled or not.
     */
    JIRA.Issues.Application.execute("analytics:trigger", "kickass.criteriaAutoUpdateEnabled", {
        enabled: AJS.Meta.getBoolean("hasCriteriaAutoUpdate")
    });

    /**
     * An array of Analytics Event tracking definitions.
     *
     * These definitions are used for simple browser event tracking such as clicks, etc.
     *
     * There are two reasons for having these here instead of in the Backbone code itself.
     * 1) All "pushstate" links in KA do not have a trackable code path. They simply adjust the URL which
     *    causes more events to be propagated. At this point it is not possible to tell whether the user
     *    clicked on, say, a Filter link or whether they entered the Issue Navigator with that filter in the URL.
     *
     * 2) It's nice to have them all in one place.
     */
    JIRA.Issues.Application.execute("analytics:registerTracker",[
        /**
         * A user clicks on a System Filter.
         */
        {
            name: "kickass.clickedOnSystemFilter",
            selector: ".system-filters a",
            handler: systemFilterClicked
        },

        /**
         * A user clicks on a Favourite Filter.
         */
        {
            name: "kickass.clickedOnFavouriteFilter",
            selector: ".favourite-filters a.filter-link"
        },

        /**
         * A user clicks on the Filter Actions Menu.
         */
        {
            name: "kickass.clickedOnFavouriteFilterActionsMenu",
            selector: ".favourite-filters a.filter-actions"
        },

        /**
         * A user renames a filter from the actions menu.
         */
        {
            name: "kickass.renamedFilterFromActionsMenu",
            selector: "#rename-filter-form",
            type: "submit"
        },

        /**
         * A user deletes a filter from the actions menu
         */
        {
            name: "kickass.deletedFilterFromActionsMenu",
            selector: "#delete-filter-form",
            type: "submit"
        },

        /**
         * A user copies a filter from the actions menu
         */
        {
            name: "kickass.copiedFilterFromActionsMenu",
            selector: "#copy-filter-dialog form",
            type: "submit"
        },

        /**
         * A user renames a filter from the actions menu
         */
        {
            name: "kickass.removeFilterFromFavouritesViaActionsMenu",
            selector: ".unfavourite-filter"
        },

        /**
         * A user saves a copy of a filter from the Search Header
         */
        {
            name: "kickass.saveACopyOfFilterFromSearchHeader",
            selector: "#save-filter-dialog form",
            type: "submit"
        },

        /**
         * A user saves their changes to a modified filter they own.
         */
        {
            name: "kickass.filterChangesSaved",
            selector: ".header-section-primary .save-changes"
        },

        /**
         * A user clicks on a System Filter
         * We have to use mousedown for the InlineDialog analytics event as
         * a) InlineDialog will stopPropagation on click events after it is initialised.
         * b) InlineDialog will send duplicate events for showLayer.
         */
        {
            name: "kickass.clickedOnShowFiltersDetailsDropDown",
            selector: ".show-filter-details",
            type: "mousedown"
        },

        /**
         * A user clicks on the change permissions link.
         */
        {
            name: "kickass.clickedOnChangePermissions",
            selector: ".filter-permissions .change-permissions"
        },

        /**
         * A user clicks on the Un-favourite Star.
         *
         * By the time the delegated event propagates to the document the element no longer has the active class
         */
        {
            name: "kickass.clickedOnUnfavouriteStar",
            selector: ".filter-operations .fav-link.off"
        },

        /**
         * A user clicks on the Save A Copy link whilst viewing a System Filter.
         */
        {
            name: "kickass.clickedOnSaveFilterAs",
            selector: ".save-as-new-filter",
            handler: clickedOnSaveFilterAs
        },

        /**
         * Tracks user's clicks on pagination links
         */
        {
            name: "kickass.clickedOnPagniationLink",
            selector: ".pagination a",
            handler: clickedOnPagniationLink
        },

        /**
         * Tracks a user doing an in-browser search for text
         */
        {
            name: "kickass.inbrowsersearch",
            type: "keydown",
            handler: inBrowserSearch
        },

        /**
         * Tracks when the user attempts to reload the page.
         */
        {
            name: "kickass.reloadPage",
            type: "keydown",
            handler: onReloadPage
        },

        /**
         * The number of criteria newly selected when the More Criteria dropdown is displayed.
         */
        {
            name: "kickass.moreCriteriaNewlySelectedCount",
            type: AJS.InlineLayer.EVENTS.beforeHide,
            handler: moreCriteriaDropdownClosed
        },

        /**
         * User clicks new search/filter button
         */
        {
            name: "kickass.createdNewSearch",
            selector: ".new-search"
        },

        /**
         * A user sorts by column.
         */
        {
            name: "kickass.sortByColumn",
            selector: ".sortable",
            handler: columnHeaderClicked
        },

        /**
         * A user clicks on the find filters link
         */
        {
            name: "kickass.findFiltersClicked",
            selector: ".find-filters"
        }
    ]);

    /**
     * Send the name of the System Filter clicked.
     *
     * @param e The Click event.
     */
    function systemFilterClicked(e) {
        var target = AJS.$(e.target);
        JIRA.Issues.Application.execute("analytics:trigger", this.name, {
            name: getFilterName(target.attr("href"))
        });
    }

    /**
     * Records a click on the "Save Filter As" link.
     *
     * @param e The Click Event
     */
    function clickedOnSaveFilterAs() {
        var systemFilterSelected = AJS.$(".system-filters a.active").length === 1,
            favouriteFilterSelected = AJS.$(".favourite-filters a.active").length === 1;

        JIRA.Issues.Application.execute("analytics:trigger", this.name, {
            systemFilterSelected: systemFilterSelected,
            favouriteFilterSelected: favouriteFilterSelected
        });
    }

    /**
     * Returns the name of the current filter from a URI.
     *
     * @param uri A URI of your choosing.
     * @return The filter ID or Name.
     */
    function getFilterName(uri) {
        var parsedUri = parseUri(uri);
        return parsedUri.queryKey.filter;
    }

    /**
     * Tracks the page the selected to view.
     *
     * @param e The click event.
     */
    function clickedOnPagniationLink(e) {
        var target = AJS.$(e.target),
            page = target.text();

        JIRA.Issues.Application.execute("analytics:trigger", this.name, {
            page: page
        });
    }

    /**
     * Tracks a user pressing ctrl/command-F to search for text.
     *
     * @param e The keydown event.
     */
    function inBrowserSearch(e) {
        if(e.keyCode === 70 && (e.metaKey || e.ctrlKey)) {
            JIRA.Issues.Application.execute("analytics:trigger", this.name);
        }
    }

    /**
     * Tracks a user pressing ctrl/command-R to reload the page.
     *
     * @param e the keydown event.
     */
    function onReloadPage(e) {
        if(e.keyCode === 82 && (e.metaKey || e.ctrlKey)) {
            JIRA.Issues.Application.execute("analytics:trigger", this.name);
        }
    }

    function moreCriteriaDropdownClosed(e, layer) {
        if (layer && layer.find("#criteria-multi-select").length) {
            var dropDown = AJS.$("#criteria-multi-select");
            var numberOfSelectedCriteria = dropDown.find("input[checked]").length - dropDown.find(".selected-group input[checked]").length;
            JIRA.Issues.Application.execute("analytics:trigger", this.name, {
                count: numberOfSelectedCriteria
            });
        }
    }

    /**
     * When a user clicks on a column header we want to know what they were sorting by and which way.
     *
     * @param e The click event.
     */
    function columnHeaderClicked(e) {
        var header = jQuery(e.target);

        if (!header.is("th")) {
            header = header.parent("th");
        }

        if (header.length) {
            JIRA.Issues.Application.execute("analytics:trigger", this.name, {
                direction: header.hasClass("ascending") ? "descending" : "ascending", // This event occurs before HTML is replaced to display new state
                column: header.data("id")
            });
        }
    }


    JIRA.Issues.Application.on("issueEditor:loadComplete", function(model, props){
        if(!props.fromCache){
            JIRA.Issues.Application.execute("analytics:trigger", "kickass.viewIssue", {
                issueId: model.getId(),
                loadReason: props.loadReason,
                duration: props.duration,
                loadedFromDom: false

            });
        }

        JIRA.Issues.Application.execute("analytics:trigger", "kickass.issueLoadDuration", {
            issueId: props.issueId,
            duration: props.duration,
            loadReason: props.loadReason,
            fromCache: props.fromCache
        });
    });

    JIRA.Issues.Application.on("issueEditor:render", function(regions, props){
        if(props.loadedFromDom){
            JIRA.Issues.Application.execute("analytics:trigger", "kickass.viewIssue", {
                issueId: props.issueId,
                loadedFromDom: true
            });
        }
    });

    JIRA.Issues.Application.on("issueEditor:saveSuccess", function(props){
        JIRA.Issues.Application.execute("analytics:trigger", "kickass.inlineEdit", {
            issueId: props.issueId,
            savedFieldIds: props.savedFieldIds,
            duration: props.duration
        });
    });

};
