/**
 * A mock <tt>JIRA.Issues.FilterModule</tt> object for use in tests.
 */
Backbone.define("JIRA.Issues.MockFilterModule", JIRA.Issues.BaseModel.extend({
    initialize: function () {
        this.filtersComponent = new JIRA.Components.Filters();
        this.getFilterById = sinon.stub();
        this.getFilterById.returns(jQuery.Deferred().promise());
    },

    initSystemFilters: function () {
        return jQuery.Deferred().resolve().promise();
    },

    canEditColumns: function() {
        return true;
    }
}));