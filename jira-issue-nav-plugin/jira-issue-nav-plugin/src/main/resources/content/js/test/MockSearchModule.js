/**
 * A mock <tt>JIRA.Issues.SearchModule</tt> object for use in tests.
 */
Backbone.define("JIRA.Issues.MockSearchModule", JIRA.Issues.BaseModel.extend({
    namedEvents: [
        "beforeSearch",
        "searchError"
    ],

    properties: [
        "results"
    ],

    initialize: function () {
        this._mockOnOffMethods("beforeSearch");
        this._mockOnOffMethods("searchError");

        this.refresh = sinon.stub();
        this.doSort = sinon.stub();
        this.getJql = sinon.stub();
        this.getFilterId = sinon.stub();
        this.isStandAloneIssue = sinon.stub();
        this.getState = sinon.stub().returns({});
        this.getEffectiveJql = sinon.stub();
        this.setResults(new JIRA.Issues.MockSearchResults());
    },

    /**
     * Mocks <tt>on*</tt> and <tt>off*</tt> methods by aliasing <tt>bind</tt> and <tt>unbind</tt>.
     *
     * @param {string} event The name of the event to mock.
     * @private
     */
    _mockOnOffMethods: function (event) {
        var uppercaseEvent = event.charAt(0).toUpperCase() + event.slice(1);
        this["on" + uppercaseEvent] = this["bind" + uppercaseEvent];
        this["off" + uppercaseEvent] = this["unbind" + uppercaseEvent];
    }
}));