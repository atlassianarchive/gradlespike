/**
 * A mock <tt>JIRA.Issues.SearchResults</tt> object for use in tests.
 */
Backbone.define("JIRA.Issues.MockSearchResults", JIRA.Issues.BaseModel.extend({
    namedEvents: [
        "highlightedIssueChange",
        "issueDeleted",
        "issueUpdated",
        "issueSelected",
        "issueUnSelected",
        "newPayload",
        "selectedIssueChange",
        "startIndexChange",
        "columnConfigChange",
        "issueDoesNotExist",
        "columnsChange",
        "stableUpdate"
    ],

    properties: [
        "displayableTotal",
        "highlightedIssue",
        "selectedIssue",
        "numberOfPages",
        "resultsId",
        "startIndex",
        "state",
        "total",
        "pager",
        "columnConfig",
        "columns"
    ],
    
    initialize: function () {
        this._mockOnOffMethods("columnConfigChange");
        this._mockOnOffMethods("columnsChange");
        this._mockOnOffMethods("highlightedIssueChange");
        this._mockOnOffMethods("issueDeleted");
        this._mockOnOffMethods("issueUpdated");
        this._mockOnOffMethods("newPayload");
        this._mockOnOffMethods("selectedIssueChange");
        this._mockOnOffMethods("startIndexChange");

        this.getPageIssueIds = sinon.stub();
        this.getResultsForIds = sinon.stub();
        this.getResultsForPage  = sinon.stub().returns(jQuery.Deferred().promise());
        this.getPositionOfLastIssueInPage = sinon.stub();
        this.goToPage = sinon.stub();
        this.hasHighlightedIssue = sinon.stub();
        this.hasIssues = sinon.stub();
        this.hasSelectedIssue = sinon.stub();
        this.highlightNextIssue = sinon.stub();
        this.highlightPrevIssue = sinon.stub();
        this.resetFromSearch = sinon.stub();
        this.selectIssueByKey = sinon.stub();
        this.selectNextIssue = sinon.stub();
        this.selectPrevIssue = sinon.stub();
        this.unselectIssue = sinon.stub();
        this.getSortBy = sinon.stub();
        this.getNextIssueForId = sinon.stub();
        this.getNextIssueForSelectedIssue = sinon.stub();
        this.isFirstIssueSelected = sinon.stub();
    },

    highlightIssueById: jQuery.noop,

    /**
     * Mocks <tt>on*</tt> and <tt>off*</tt> methods by aliasing <tt>bind</tt> and <tt>unbind</tt>.
     *
     * @param {string} event The name of the event to mock.
     * @private
     */
    _mockOnOffMethods: function (event) {
        var uppercaseEvent = event.charAt(0).toUpperCase() + event.slice(1);
        this["on" + uppercaseEvent] = this["bind" + uppercaseEvent];
        this["off" + uppercaseEvent] = this["unbind" + uppercaseEvent];
    }
}));