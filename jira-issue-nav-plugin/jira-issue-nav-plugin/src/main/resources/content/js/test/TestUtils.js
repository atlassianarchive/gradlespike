AJS.namespace("JIRA.Issues.TestUtils");

JIRA.Issues.TestUtils = {
    /**
     * Create a mock of a given class.
     *
     * @param {function} constructor The class's constructor.
     * @return {object} a mock of the given class.
     * @private
     */
    _mockOfClass: function (constructor) {
        var mock = {};
        _.each(_.functions(constructor.prototype), function (functionName) {
            mock[functionName] = sinon.stub();
        });

        return mock;
    },

    mockIssueCacheManager: function() {
        return this._mockOfClass(JIRA.Issues.Cache.IssueCacheManager);
    },
    
    mockColumnConfig: function() {
        var clss = this._mockOfClass(JIRA.Issues.ColumnPicker);
        clss.setElement = function () {
            return clss;
        };
        return clss;
    },


    mockQueryModule: function () {
        var queryModule = this._mockOfClass(JIRA.Issues.QueryModule);
        queryModule.resetToQuery = function () {
            return jQuery.Deferred().resolve();
        };
        return queryModule;
    },

    /**
     * @return {JIRA.Issues.SearchPageModule} A search page module with all dependencies mocked out.
     */
    mockSearchPageModule: function() {
        var searchPageModule = new JIRA.Issues.SearchPageModule();


        searchPageModule._showIntroDialogs = jQuery.noop;

        searchPageModule.registerViewContainers({
            issueContainer: AJS.$("<div></div>"),
            searchContainer: AJS.$("<div></div>")
        });

        searchPageModule.registerFilterModule(new JIRA.Issues.MockFilterModule());
        searchPageModule.registerSearch(new JIRA.Issues.MockSearchModule());
        searchPageModule.registerFullScreenIssue(this.mockFullScreenIssue());
        searchPageModule.registerIssueNavRouter(this._mockOfClass(JIRA.Issues.IssueNavRouter));
        searchPageModule.registerQueryModule(this.mockQueryModule());
        searchPageModule.registerSearchHeaderModule(this._mockOfClass(JIRA.Issues.SearchHeaderModule));
        searchPageModule.registerIssueCacheManager(this.mockIssueCacheManager());

        // Again, returning a dud deferred saves mocking.
        searchPageModule.registerIssueSearchManager(this.mockIssueSearchManager());
        searchPageModule.issueSearchManager.search.returns(jQuery.Deferred().promise());

        //Don't show intro dialogs when running qunit tests
        searchPageModule._shownIntroDialog = true;

        // Override with mocks
        var ListView = function () {};
        ListView.prototype = this._mockOfClass(JIRA.Issues.FullScreenLayout);
        var SplitScreen = function () {};
        SplitScreen.prototype = this._mockOfClass(JIRA.Issues.FullScreenLayout);
        searchPageModule.registerLayout("list-view", {
            label: "List View",
            view: ListView
        });
        searchPageModule.registerLayout("split-view", {
            label: "Split View",
            view: SplitScreen
        });


        return searchPageModule;
    },

    mockIssueCacheManager: function() {
        return {
            prefetchIssues: function() {}
        };
    },

    mockSearchResults: function (attr) {
        var issueSearchManager = this._mockOfClass(JIRA.Issues.IssueSearchManager);
        var columnConfig = JIRA.Issues.ColumnPicker.create({
            search: JIRA.Issues.TestUtils.mockSearchPageModule()
        });
        var sr = new JIRA.Issues.SearchResults(attr, {
            issueSearchManager: issueSearchManager,
            columnConfig: columnConfig
        });
        issueSearchManager.getRowsForIds = sinon.stub().returns(jQuery.Deferred().resolve({table: "<table></table>"}).promise());
        issueSearchManager.issueKeys = {
            getAllCached: function () {
                var res = {};
                var count = 1;
                _.each(sr.getIssueIds(), function (id) {
                    res[id] = "HSP-" + count;
                    count++;
                });
                return res;
            }
        };
        return sr;
    },

    mockQueryStateModel: function () {
        return this._mockOfClass(JIRA.Issues.QueryStateModel);
    },

    mockIssueSearchManager: function () {
        return this._mockOfClass(JIRA.Issues.IssueSearchManager)
    },

    mockFullScreenIssue: function() {
        return this._mockOfClass(JIRA.Issues.FullScreenIssue);
    },

    /**
     * Creates and returns a mock FieldModel.
     *
     * @param {string} id The ID to use.
     * @param {boolean} isEditable whether the field is editable.
     * @param {boolean} matchesFieldSelector Whether the field is present.
     */
    mockIssueFieldModel: function(id, isEditable, matchesFieldSelector) {
        return _.extend(new Backbone.Model(), {
            edit: AJS.$.noop,
            getLabel: _.lambda("Label for " + id),
            getSaving: _.lambda(false),
            id: id,
            isEditable: _.lambda(isEditable),
            matchesFieldSelector: _.lambda(matchesFieldSelector)
        });
    },

    createSearcherCollection: function (vals, ext) {
        var searcherCollection = new JIRA.Issues.SearcherCollection(vals || [], {searchPageModule: new JIRA.Issues.SearchPageModule()});
        sinon.stub(searcherCollection, "_querySearchersAndValues", function() {
            return jQuery.Deferred().resolve().promise();
        });
        sinon.stub(searcherCollection, "_querySearchersByValue", function () {
            return jQuery.Deferred().resolve().promise();
        });
        return searcherCollection;
    },

    /**
     * Creates an issue nav router with the navigate method (that actually sets the url) mocked out.
     *
     * Note that in order to use this navigate method you will want to use sinon timers as the issue nav router
     * currently debounces multiple navigate calls using setTimeout
     *
     * Also initialises backbone history and adds a "fakeLoad" method to the issueNavRouter that triggers the actual
     * routing. Eg fakeLoad("/some/url/fragment") will actually trigger the configured route for that url fragment.
     *
     * @param options options to pass to IssueNavRouter constructor
     */
    createIssueNavRouter: function(options) {
        var fakeContextRoot = "fakeContextRoot";

        var issueNavRouter = new JIRA.Issues.IssueNavRouter(options);

        // Don't call Backbone.history.start, as it puts in listeners and we can't call it twice
        // Set root directly, as it's required for "loadUrl" calls below
        // We're using undocumented backbone API here and it could break in future versions,
        // but it's worth it to test our routes
        Backbone.history.options = {
            root: fakeContextRoot
        };
        issueNavRouter.fakeLoad = function(urlFragment) {
            Backbone.history.loadUrl(fakeContextRoot + urlFragment);
        };

        // Mock out navigation as it sets the URL
        issueNavRouter.navigate = sinon.spy();

        return issueNavRouter;
    },
    //quick function to test if two strings contain the same html.
    //could probably be improved by using a dom fragment, but this method *should* takes implementation details away from the browser
    //currently only tests single elements.
    //This is mainly because i did not trust .html();
    areHtmlStringsEqual: function(initial, compare) {
        var $initial = AJS.$(initial),
            $compare = AJS.$(compare),
            compareAttr = $compare[0].attributes,
            initialAttr = $initial[0].attributes;

        if($compare[0].tagName != $initial[0].tagName) {
            return false;
        }

        if(AJS.$.trim($compare.text()) != AJS.$.trim($initial.text())) {
            return false;
        }
        var attributeKVP = function(attr,key) {
            return {name : attr.nodeName , value: attr.nodeValue }
        };

        return _.isEqual(_.map(compareAttr,attributeKVP), _.map(initialAttr,attributeKVP));
    },

    stubGetLoggedInUser: function(user) {
        AJS.$("<meta />").attr('name', 'ajs-remote-user').attr('content', user).appendTo(AJS.$("#qunit-fixture"));
    },

    // Runs the block function with the dark feature temporarily switched on/off depending on toggle.
    // Use this for testing dark features since AJS.DarkFeature is global and thus not restored between tests.
    // Context is optional and sets the block's context (instead of requiring a _.bind()).
    darkFeature: function (darkFeatureKey, toggle, block, context) {
        var darkFeatures = {};
        darkFeatures[darkFeatureKey] = toggle;

        this.darkFeatures(darkFeatures, block, context);
    },

    /**
     * Execute a function within a dark feature configuration.
     *
     * @param {object} darkFeatures The dark features to enable/disable.
     * @param {function} block The function to execute within that dark feature configuration.
     * @param {object} context The context to invoke the function in.
     */
    darkFeatures: function (darkFeatures, block, context) {
        function setDarkFeatures (darkFeatures) {
            _.each(darkFeatures, function (value, key) {
                AJS.DarkFeatures[value ? "enable" : "disable"](key);
            });
        }

        var originalValues = {};
        _.each(darkFeatures, function (value, key) {
            originalValues[key] = AJS.DarkFeatures.isEnabled(key);
        });

        setDarkFeatures(darkFeatures);
        block.call(context);
        setDarkFeatures(originalValues);
    }
};

// QUnit 1.10.0 treats global JS errors as fail
JIRA.Issues.displayFailSearchMessage = jQuery.noop;
