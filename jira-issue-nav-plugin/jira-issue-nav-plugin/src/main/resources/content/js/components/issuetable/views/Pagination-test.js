AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuetable-component-test");

(function (jQuery, AJS, Pagination) {
    "use strict";

    module("JIRA.Components.IssueNavigator.Views.Pagination");

    test("It takes into account the current filter and jql when generating the pagination links", function() {
        var runTest = _.bind(function(filterId, jql, expected) {
            var paginationView = new Pagination({
                startIndex: 50,
                pageSize: 10,
                displayableTotal: 100,
                currentSearch: {
                    jql: jql,
                    filterId: filterId
                }
            });
            paginationView.render();

            var previousLink = paginationView.$("a").eq(0).attr('href');
            equal(previousLink, expected);
        }, this);

        runTest(null, "lala", AJS.contextPath()+"/issues/?jql=lala&startIndex=40");
        runTest("booboo", null, AJS.contextPath()+"/issues/?filter=booboo&startIndex=40");
        runTest("booboo", "lala", AJS.contextPath()+"/issues/?filter=booboo&jql=lala&startIndex=40");
    });

    test("It renders the pagination links", function() {
        var paginationView = new Pagination({
            startIndex: 0,
            pageSize: 10,
            total: 95,
            currentSearch: {
                jql: ""
            }
        });
        paginationView.render();

        equal(paginationView.$el.find('.pagination strong').text(), 1, "Current page is rendered");
        equal(paginationView.$el.find('.pagination a.icon').length, 1, "Only 1 of the prev/next page icons is rendered");
        equal(paginationView.$el.find('.pagination a.icon').attr('href'), AJS.contextPath() + '/issues/?jql=&startIndex=10', "Icon for next page has correct url");
        equal(paginationView.$el.find('.pagination a:not(.icon)').length, 4, "It render 4 numbers");
        paginationView.$el.find('.pagination a:not(.icon)').each(function(i) {
            equal(jQuery(this).text(), i + 2, "Page number is correctly rendered");
            equal(jQuery(this).attr('href'), AJS.contextPath() + '/issues/?jql=&startIndex=' + (i + 1) * 10, "Link href is correct");
        });
    });

    test("It renders the correct values when there is only one page", function() {
        var paginationView = new Pagination({
            startIndex: 0,
            pageSize: 10,
            total: 10,
            currentSearch: {
                jql: ""
            }
        });
        paginationView.render();

        equal(paginationView.$el.find('.pagination').length, 1, "has pagination element inside");
        equal(paginationView.$el.find('.pagination').text(), '', "Nothing rendered");
    });

}(AJS.$, AJS, JIRA.Components.IssueNavigator.Views.Pagination));
