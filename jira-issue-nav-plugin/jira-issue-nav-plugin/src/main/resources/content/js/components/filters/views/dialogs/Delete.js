AJS.namespace("JIRA.Components.Filters.Views.Dialogs.Delete");

/**
 * @class JIRA.Components.Filters.Views.Dialogs.Delete
 *
 * @extends JIRA.Marionette.DialogView
 */
JIRA.Components.Filters.Views.Dialogs.Delete = JIRA.Marionette.DialogView.extend({
    template: JIRA.Components.Filters.Templates.Dialogs.Delete,

    id: "delete-filter-dialog",

    serializeData: function () {
        var data = {
            modifierKey: AJS.Navigator.modifierKey()
        };

        if (this.model) {
            data.filter = this.model.toJSON();
        } else {
            data.filter = {subscriptions: {items: []}};
        }

        return data;
    }
});