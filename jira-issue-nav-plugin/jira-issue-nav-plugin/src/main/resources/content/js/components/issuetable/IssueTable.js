AJS.namespace("JIRA.Components.IssueTable");
JIRA.Components.IssueTable = JIRA.Marionette.Controller.extend({

    initialize: function (options) {
        this.issueTableView = new JIRA.Issues.IssueTableView({
            columnConfig: options.columnConfig
        });
        this._createSearchService(options);
        this._createTableController(options);

        JIRA.Issues.Application.on("issueEditor:loadError", this._handleIssueLoadError, this);
    },

    _createSearchService: function(options) {
        this.searchService = options.searchService;
        this.listenTo(this.searchService, {
            "before:search": function() {
                this.issueTableController.showPending();
            },
            "search": function(table, searchResults) {
                this.latestResults = {
                    table: table,
                    sortOptions: searchResults.getSortBy(),
                    totalDisplayableIssues: searchResults.getDisplayableTotal(),
                    startIndex: searchResults.getStartIndex(),
                    pageSize: searchResults.getPageSize(),
                    pageNumber: searchResults.getPageNumber(),
                    numberOfPages: searchResults.getNumberOfPages(),
                    totalIssues: searchResults.getTotal(),
                    currentSearch: JIRA.Issues.Application.request("issueNav:currentSearchRequest"),
                    jiraHasIssues: searchResults.getJiraHasIssues(),
                    hasIssues: searchResults.hasIssues()
                };
                this.show();
                this.highlightIssue(searchResults.getHighlightedIssue().id, false);
            },
            "error:search": function() {
                this.issueTableController.showErrorMessage();
            },
            "issueUpdated": function(issueId, entity) {
                this.issueTableController.updateIssue(issueId, entity.table);
            },
            "issueHighlighted": function(issueId) {
                this.issueTableController.highlightIssue(issueId);
            },
            "selectedIssueChanged": function(selectedIssue, highlightedIssue) {
                if (!selectedIssue.hasIssue()) {
                    this.issueTableController.highlightIssue(highlightedIssue.id);
                }
            }

        });
    },

    _createTableController: function(options) {
        var columnConfig = options.columnConfig;
        var el = options.el;

        this.issueTableController = new JIRA.Components.IssueTable.Controllers.IssueTable({
            el: el
        });
        this.listenTo(this.issueTableController, {
            "goToPage": function(startIndex) {
                this.searchService.goToPage(startIndex);
            },
            "columnsChanged": function(cols) {
                columnConfig.saveColumns(cols);
            },
            "highlightIssue": function (issueId) {
                this.trigger("highlightIssue", issueId);
            },
            "sort": function(fieldId) {
                this.searchService.sort(fieldId);
            },
            "refresh": function() {
                this.searchService.runCurrentSearch();
            },
            "renderTable": function($el) {
                JIRA.trigger(JIRA.Events.NEW_CONTENT_ADDED, [$el, JIRA.CONTENT_ADDED_REASON.issueTableRefreshed]);
                JIRA.trace("jira.search.stable.update");
            },
            "renderEmpty": function($el) {
                JIRA.trigger(JIRA.Events.NEW_CONTENT_ADDED, [$el, JIRA.CONTENT_ADDED_REASON.issueTableRefreshed]);
                JIRA.trace("jira.search.stable.update");
            },
            "issueRowUpdated": function($newRow) {
                JIRA.trigger(JIRA.Events.NEW_CONTENT_ADDED, [$newRow, JIRA.CONTENT_ADDED_REASON.issueTableRowRefreshed]);
                // even though we've only replaced one row, a search has occurred at this point. all our
                // WebDriver tests expect this trace after modifying issues from the issue nav
                JIRA.trace('jira.search.finished');
            }
        });
    },

    show: function() {
        if (!this.latestResults) {
            this.searchService.updateExitingResults();
        } else {
            this.issueTableController.show(this.latestResults);
            this.trigger("render");
            this.issueTableView._onSearchDone(this.issueTableController.view.$el);
        }
    },

    close: function() {
        this.issueTableController.close();
        this.stopListening(this.searchService);
        JIRA.Issues.Application.off("issueEditor:loadError", this._handleIssueLoadError, this);
        delete this.issueTableController;
        delete this.searchService;
    },

    handleIssueInaccessible: function (issueId) {
        this.issueTableController.markIssueAsInaccessible(issueId);
    },

    /**
     * Highlight an issue in the table.
     *
     * @param {number} issueId The ID of the issue to highlight.
     * @param {boolean} [focus=true] Whether the highlighted issue should have the focus
     */
    highlightIssue: function (issueId, focus) {
        this.issueTableController.highlightIssue(issueId, focus);
    },

    _handleIssueLoadError: function(entity) {
        // If the issue has been deleted, update its row in the table.
        if (entity.response.status === 404) {
            this.issueTableController.markIssueAsInaccessible(entity.issueId);
        }
    }
});