AJS.namespace("JIRA.Components.Filters.Views.List.FilterWithActions");

/**
 * @class JIRA.Components.Filters.Views.List.FilterWithActions
 *
 * Renders each individual item in the filter's list, including a dropdown for the filter Actions
 *
 * @extends JIRA.Components.Filters.Views.List.Filter
 */
JIRA.Components.Filters.Views.List.FilterWithActions = JIRA.Components.Filters.Views.List.Filter.extend({
    ui: _.extend({
        actionsTrigger: ".filter-actions"
    }, JIRA.Components.Filters.Views.List.Filter.prototype.ui),

    triggers: _.extend({
        "click .filter-actions": "openActions"
    }, JIRA.Components.Filters.Views.List.Filter.prototype.triggers),

    templateHelpers: {
        useActions: true
    },

    /**
     * Returns the trigger used to display the Actions dropdown
     *
     * @returns {jQuery}
     */
    getActionsTrigger: function() {
        return this.ui.actionsTrigger;
    }
});
