AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:filters-component-test");

module("JIRA.Components.Filters.Views.List.Filter", {
    createView: function(options) {
        return new JIRA.Components.Filters.Views.List.Filter({
            model: options.model
        });
    }
});

test("When rendering a filter, it unwraps the template", function() {
    var model = new JIRA.Components.Filters.Models.Filter({ name: "My filter" });
    var view = this.createView({ model: model });

    view.render();

    ok(view.$el.is("li"));
});

test("When rendering a filter, it should display the name", function() {
    var model = new JIRA.Components.Filters.Models.Filter({ name: "My filter" });
    var view = this.createView({ model: model });

    view.render();

    equal(view.ui.filterLink.text(), "My filter");
});

test("When rendering a filter, it should render the URL of the filter", function() {
    var model = new JIRA.Components.Filters.Models.Filter({ id: 1234 });
    var view = this.createView({ model: model });

    view.render();

    equal(view.ui.filterLink.attr("href"), AJS.contextPath() + "/issues/?filter=1234");
});

test("When rendering a filter, it should include the filter's ID as a data attribute", function() {
    var model = new JIRA.Components.Filters.Models.Filter({ id: 1234 });
    var view = this.createView({ model: model });

    view.render();

    equal(view.ui.filterLink.attr("data-id"), "1234");
});

test("When the model is changed, it should render the filter again", function() {
    var model = new JIRA.Components.Filters.Models.Filter({ name: "My filter" });
    var view = this.createView({ model: model });

    view.render();
    model.set("name", "My renamed filter");

    equal(view.ui.filterLink.text(), "My renamed filter");
});

test("When clicking on the link, it should trigger the selectFilter event", function() {
    var model = new JIRA.Components.Filters.Models.Filter({ name: "My filter" });
    var view = this.createView({ model: model });
    var selectFilterSpy = this.spy();

    view.on("selectFilter", selectFilterSpy);
    view.render();

    view.ui.filterLink.click();

    sinon.assert.calledOnce(selectFilterSpy);
    ok(selectFilterSpy.getCall(0).args[0].view === view, "Includes the view in the event arguments");
    ok(selectFilterSpy.getCall(0).args[0].model === model, "Includes the model in the event arguments");
});

test("When clicking on a filter that requires login, it should not trigger the selectFilter event", function() {
    var model = new JIRA.Components.Filters.Models.Filter({ name: "My filter", requiresLogin: true });
    var view = this.createView({ model: model });
    var selectFilterSpy = this.spy();

    view.on("selectFilter", selectFilterSpy);
    view.render();

    view.ui.filterLink.click();

    sinon.assert.notCalled(selectFilterSpy);
});

test("When highlighting a filter, it should be displayed as active", function() {
    var model = new JIRA.Components.Filters.Models.Filter({ name: "My filter" });
    var view = this.createView({ model: model });

    view.render();
    view.highlight();

    ok(view.ui.filterLink.hasClass("active"));
});

test("When highlighting a filter, it should be scrolled into the view", function() {
    // This test is testing the actual implementation (i.e. call scrollIntoView) instead of testing
    // the behaviour (i.e. check that the element is visible), because setting up the environment for
    // checking the behaviour would be quite convoluted (crete a list, add lots of filters, setup the
    // CSS for scrolling, scroll the item out and then exercise the class). As the behaviour is already
    // tested in WebDriver land, we are ok.
    var model = new JIRA.Components.Filters.Models.Filter({ name: "My filter" });
    var view = this.createView({ model: model });

    view.render();
    this.stub(view.ui.filterLink, "scrollIntoView");
    view.highlight();

    sinon.assert.calledOnce(view.ui.filterLink.scrollIntoView);
});

test("When unhighlighting a highlighted filter, it should be displayed as inactive", function() {
    var model = new JIRA.Components.Filters.Models.Filter({ name: "My filter" });
    var view = this.createView({ model: model });

    view.render();
    view.highlight();
    view.unhighlight();

    ok(!view.ui.filterLink.hasClass("active"));
});
