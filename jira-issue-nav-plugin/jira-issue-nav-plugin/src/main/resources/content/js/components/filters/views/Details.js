AJS.namespace("JIRA.Components.Filters.Views.Details");

/**
 * @class JIRA.Components.Filters.Controllers.Details
 *
 * @extends JIRA.Marionette.InlineDialogView
 */
JIRA.Components.Filters.Views.Details = JIRA.Marionette.InlineDialogView.extend({
    template: JIRA.Components.Filters.Templates.Details.Main,
    id: "filter-details-overlay",

    dialogOptions: {
        addActiveClass: true,
        hideDelay: 36e5,
        offsetX: -36,
        width: 400
    },

    initialize: function(options) {
        this.trigger = options.triggerEl;
    },

    serializeData: function() {
        var currentFilter = this.model;
        var shares = _.sortBy(currentFilter.getSharePermissions(), function(share) {
            var criteria = [share.type];
            share.group && criteria.push(share.group.name);
            share.project && criteria.push(share.project.name);
            share.role && criteria.push(share.role.name);
            return criteria;
        });
        var subscriptions = _.clone(currentFilter.getSubscriptions());
        var canEdit = JIRA.Issues.LoginUtils.loggedInUserName() === currentFilter.getOwnerUserName();

        subscriptions.additionalLength = currentFilter.getNumberOfAdditionalSubscriptions();

        var data = {
            filter: currentFilter.toJSON(),
            sharePermissions: shares,
            subscriptions: subscriptions,
            permissions: {
                // The "Edit Permissions" link should be displayed whenever the user can actually change sharing permissions.
                // This is possible even when they do not have "create shared objects" permission. @see JRADEV-14810
                userHasEditSharingPermission: canEdit && (!!JIRA.Issues.UserParms.createSharedObjects() || shares.length > 0),
                userHasEditPermission: canEdit
            },
            userIsLoggedIn: JIRA.Issues.LoginUtils.isLoggedIn(),
            returnUrl: window.location
        };

        if (currentFilter.getOwner()) {
            _.extend(data, {
                owner: currentFilter.getOwner() || "",
                ownerUserName: currentFilter.getOwnerUserName() || "",
                avatarUrl: currentFilter.getAvatarUrl(),
                ownerUrl: AJS.contextPath() + '/secure/ViewProfile.jspa?name=' + encodeURIComponent(currentFilter.getOwnerUserName())
            });
        }

        return data;
    },

    onRender: function() {
        // Hack to work around JIRA's LayerManager.js closing this dialog.
        this.dialog.bind("click", function(event) {
            event.stopPropagation();
        });
    }
});
