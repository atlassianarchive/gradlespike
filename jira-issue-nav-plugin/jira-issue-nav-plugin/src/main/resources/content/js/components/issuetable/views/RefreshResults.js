AJS.namespace("JIRA.Components.IssueNavigator.Views.RefreshResults");

/**
 * @class JIRA.Components.IssueNavigator.Views.RefreshResults
 *
 * This view renders a 'refresh' button.
 *
 * @extends JIRA.Marionette.ItemView
 */
JIRA.Components.IssueNavigator.Views.RefreshResults = JIRA.Marionette.ItemView.extend({
    tagName: 'span',
    template: JIRA.Templates.IssueNav.refreshResults,
    triggers: {
        /**
         * @event refresh
         * When the user clicks the refresh button
         */
        "click a" : "refresh"
    }
});