AJS.namespace("JIRA.Components.SearchService");

/**
 * @class JIRA.Components.SearchService
 *
 * This service encapsulates all the search related stuff
 *
 * @extends JIRA.Marionette.Controller
 */
JIRA.Components.SearchService = JIRA.Marionette.Controller.extend({
    /**
     * @event issueUpdated
     * When an issue has been updated
     */

    /**
     * @event search
     * When an new search has been done
     */

    /**
     * @event before:search
     * Before doing a new search
     */

    /**
     * @event error:search
     * When a new search has been done but it thrown an error
     */

    /**
     * @event issueHighlighted
     * When an issue has been marked as highlighted in the internal model
     */

    /**
     * @event selectedIssueChanged
     * When an issue has been marked as selected in the internal model
     */

    /**
     * @param {Object} options
     * @param {JIRA.Issues.SearchModule} options.searchModule
     * @param {JIRA.Issues.SearchResults} options.searchResults
     * @param {JIRA.Issues.ColumnConfigModel} options.columnConfig
     */
    initialize: function(options) {
        this.searchModule = options.searchModule;
        this.searchResults = options.searchResults;
        this.columnConfig = options.columnConfig;

        _.bindAll(this, "_onIssueUpdated", "_onHighlightedIssueChange", "_onSelectedIssueChange");

        this.listenTo(this.searchResults, "change:resultsId change:startIndex stableUpdate issueDeleted", function() {
            this._doSearch();
        });

        // These are *not* regular Backbone events, we can't use listenTo.
        this.searchResults.onIssueUpdated(this._onIssueUpdated);
        this.searchResults.onHighlightedIssueChange(this._onHighlightedIssueChange);
        this.searchResults.onSelectedIssueChange(this._onSelectedIssueChange);
    },

    _onSelectedIssueChange: function(issue) {
        this.trigger("selectedIssueChanged", issue, this.searchResults.getHighlightedIssue());
    },

    _onIssueUpdated: function(issueId, entity, reason) {
        this.trigger("issueUpdated", issueId, entity, reason);
    },

    _onHighlightedIssueChange: function(issue) {
        this.trigger("issueHighlighted", issue.getId());
    },

    close: function() {
        this.stopListening();
        this.searchResults.offIssueUpdated(this._onIssueUpdated);
        this.searchResults.offHighlightedIssueChange(this._onHighlightedIssueChange);
    },

    /**
     * Asks the SearchResults object to do a new search with the parameters already contained
     * in the SearchModule
     * @private
     */
    _doSearch: function() {
        this.searchInProgress = true;
        this.trigger("before:search");

        var filterId = this.searchModule.getFilterId();
        var isSystemFilter = filterId < 0;
        filterId = (isSystemFilter || _.isNull(filterId)) ? undefined : filterId;

        this.searchResults.getResultsForPage({
            jql: this.searchModule.getEffectiveJql(),
            filterId: filterId
        })
            .always(_.bind(function() {
                this.searchInProgress = false;
            }, this))
            .done(_.bind(function(table) {
                if (!this.searchResults.hasHighlightedIssue()) {
                    this.searchResults.highlightFirstInPage();
                }
                this.trigger("search", table, this.searchResults);
            }, this))
            .fail(_.bind(function() {
                this.trigger("error:search");
            }, this));
    },

    /**
     * Loads a page of the current search results. This code expects the issue position,
     * not the page number. Example: using a pageSize of 25, passing startIndex=50 will load the
     * issues #50 to #74.
     *
     * This method will do nothing if a search is already in progress
     *
     * @param {number} startIndex Position of the issue in the page
     */
    goToPage: function(startIndex) {
        if (this.searchInProgress) return;
        this.searchResults.goToPage(startIndex);
    },

    /**
     * Sorts the current search results by a field.
     *
     * This method will do nothing if a search is already in progress
     *
     * @param {string} fieldId ID of the field to sort by
     */
    sort: function(fieldId) {
        if (this.searchInProgress) return;

        var allSorts = this.searchModule.getResults().getColumnSortJql();
        var sortJql = allSorts[fieldId];
        if (sortJql) {
            this.searchModule.doSort(sortJql);
        }
    },

    /**
     * Re-runs the current search.
     *
     * This method will do nothing if a search is already in progress
     */
    runCurrentSearch: function() {
        if (this.searchInProgress) return;

        if (JIRA.Issues.Application.request("issueEditor:canDismissComment")) {
            JIRA.Issues.Application.execute("analytics:trigger", "kickass.issueTableRefresh");
            this.searchModule.refresh();
        }
    },

    /**
     * This method updates the existing set of results (aka Stable Search). It does not re-run the search again, so the
     * list of Issues will remain the same.
     *
     * This method will do nothing if a search is already in progress.
     */
    updateExitingResults: function() {
        if (this.searchInProgress) return;

        if (JIRA.Issues.Application.request("issueEditor:canDismissComment")) {
            JIRA.Issues.Application.execute("analytics:trigger", "kickass.issueTableRefresh");
            this._doSearch();
        }
    },

    highlightIssue: function(issueId) {
        this.searchResults.highlightIssueById(issueId);
    },

    getPager: function() {
        return this.searchResults.getPager();
    },

    selectNextIssue: function() {
        this.searchResults.highlightNextIssue();
        if (this.searchResults.hasSelectedIssue()) {
            this.searchResults.selectNextIssue();
        }
    },

    selectPreviousIssue: function () {
        this.searchResults.highlightPrevIssue();
        if (this.searchResults.hasSelectedIssue()) {
            this.searchResults.selectPrevIssue();
        }
    },

    unselectIssue: function() {
        this.searchResults.unselectIssue();
    },

    hasSelectedIssue: function() {
        return this.searchResults.hasSelectedIssue();
    },

    getHighlightedIssue: function() {
        return this.searchResults.getHighlightedIssue().id;
    }

});
