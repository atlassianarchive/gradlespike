AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:filters-component-test");

module('JIRA.Components.Filters.Models.Filter');

test("It uses the owner's display name as owner", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        ownerDisplayName: "Mr Charlie Test"
    });
    equal(model.getOwner(), "Mr Charlie Test");
});

test("It uses the owner's user name as owner", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        ownerUserName: "charlie_test"
    });
    equal(model.getOwner(), "charlie_test");
});

test("It uses the owner's display name as owner even if owner's user name is present", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        ownerDisplayName: "Mr Charlie Test",
        ownerUserName: "charlie_test"
    });
    equal(model.getOwner(), "Mr Charlie Test");
});

test("System filters are not favourites", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        isSystem: true
    });
    ok(!model.isFavouriteFilter());
});

test("Favourite filters are favourites", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        favourite: true
    });
    ok(model.isFavouriteFilter());
});

test("The URL contains the filter ID and the expand for subscriptions", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        id: 1234
    });
    equal(model.url(), AJS.contextPath() + "/rest/api/2/filter/1234?expand=subscriptions[-5:]");
});

test("System filters does not belong to the current user", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        isSystem: true
    });
    ok(!model.isMyFilter());
});

test("A filter belongs to the current user if the username matches", function() {
    this.stub(JIRA.Issues.LoginUtils, "loggedInUserName").returns("fred");
    var model = new JIRA.Components.Filters.Models.Filter({
        ownerUserName: "fred"
    });
    ok(model.isMyFilter());
});

test("A filter does not belong to the current user if the username does not match", function() {
    this.stub(JIRA.Issues.LoginUtils, "loggedInUserName").returns("fred");
    var model = new JIRA.Components.Filters.Models.Filter({
        ownerUserName: "charlie"
    });
    ok(!model.isMyFilter());
});

test("It extract the filter basic attributes from a server response", function() {
    var model = new JIRA.Components.Filters.Models.Filter();

    var serverResponse = {
        id: 1,
        name: "name",
        description: "description",
        jql: "jql",
        isSystem: false,
        favourite: true,
        requiresLogin: true,
        sharePermissions: ["some", "value"],
        owner: {
            name: "anna",
            displayName: "Mrs. Anna",
            avatarUrls: {
                '48x48': "http://my/avatar.png"
            }
        }
    };
    var expectedAttributes = {
        avatarUrl: "http://my/avatar.png",
        description: "description",
        favourite: true,
        id: 1,
        isSystem: false,
        jql: "jql",
        name: "name",
        ownerDisplayName: "Mrs. Anna",
        ownerUserName: "anna",
        requiresLogin: true,
        subscriptions: undefined,
        sharePermissions: ["some", "value"]
    };

    var parsedAttributes = model.parse(serverResponse);
    deepEqual(parsedAttributes, expectedAttributes);
});

test("It extract the subscrition in reverse order from the server response", function() {
    var model = new JIRA.Components.Filters.Models.Filter();
    var serverResponse = {
        subscriptions: {
            items: ["a","b","c"]
        }
    };

    var parsedAttributes = model.parse(serverResponse);
    deepEqual(parsedAttributes.subscriptions.items, ["c","b","a"]);
});

test("It computes the number of additional subscriptions", function() {
    var model = new JIRA.Components.Filters.Models.Filter();
    model.set({
        subscriptions: {
            size: 10,
            items: ["a","b","c"]
        }
    }, {parse: true});

    equal(model.getNumberOfAdditionalSubscriptions(), 7);
});

test("It computes the number of additional subscriptions when the model has no subscriptions", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        subscriptions: {
            items: {}
        }
    });
    equal(model.getNumberOfAdditionalSubscriptions(), 0);
});

test("When converting the model to JSON, public filters require no login", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        requiresLogin: false
    });
    ok(!model.toJSON().requiresLoginForUser);
});

test("When converting the model to JSON, private filters require no login for authenticated user", function() {
    this.stub(JIRA.Issues.LoginUtils, "isLoggedIn").returns(true);

    var model = new JIRA.Components.Filters.Models.Filter({
        requiresLogin: true
    });
    ok(!model.toJSON().requiresLoginForUser);
});

test("When converting the model to JSON, private filters require login for anonymous user", function() {
    this.stub(JIRA.Issues.LoginUtils, "isLoggedIn").returns(false);

    var model = new JIRA.Components.Filters.Models.Filter({
        requiresLogin: true
    });
    ok(model.toJSON().requiresLoginForUser);
});

test("Save favourite marks the model as favourite before getting the server response", function() {
    this.sandbox.useFakeServer();
    var model = new JIRA.Components.Filters.Models.Filter({
        favourite: false
    });

    model.saveFavourite(true);
    ok(model.get("favourite"), true);
});

test("Saving a model as favourite sends a PUT request to the REST API", function() {
    var server = this.sandbox.useFakeServer();
    var model = new JIRA.Components.Filters.Models.Filter({id: 1234});

    model.saveFavourite(true);

    equal(server.requests[0].url, AJS.contextPath() + "/rest/api/1.0/filters/1234/favourite");
    equal(server.requests[0].method, "PUT");
});

test("Saving a model as non-favourite sends a DELETE request to the REST API", function() {
    var server = this.sandbox.useFakeServer();
    var model = new JIRA.Components.Filters.Models.Filter({id: 1234});

    model.saveFavourite(false);

    equal(server.requests[0].url, AJS.contextPath() + "/rest/api/1.0/filters/1234/favourite");
    equal(server.requests[0].method, "DELETE");
});

test("Saving a save a model as favourite cancels the previous request", function() {
    var server = this.sandbox.useFakeServer();
    var model = new JIRA.Components.Filters.Models.Filter({id: 1234});

    model.saveFavourite(false);
    model.saveFavourite(false);

    ok(server.requests[0].aborted);
});

test("When a favourite request is not successful, the filter favourite state", function() {
    var server = this.sandbox.useFakeServer();
    var model = new JIRA.Components.Filters.Models.Filter({favourite: true});

    model.saveFavourite(false);
    server.requests[0].respond(500);

    ok(model.get('favourite'));
});

test("When a favourite request is successful, the filter is marked as favourite", function() {
    var server = this.sandbox.useFakeServer();
    var model = new JIRA.Components.Filters.Models.Filter({favourite: true});

    model.saveFavourite(false);
    server.requests[0].respond(200, {}, '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><text-message>Success in setting favourite for : admin:1 (SearchRequest - 10200) - favourite state: false</text-message>');

    ok(!model.get('favourite'));
});

test("When a filter is saved with a new JQL, it sends a request to the REST API", function() {
    var server = this.sandbox.useFakeServer();
    var model = new JIRA.Components.Filters.Models.Filter({
        id: 1234,
        name: "My filter",
        favourite: false
    });

    model.saveFilter("project=X");

    var expectedPayload = {
        id: 1234,
        name: "My filter",
        favourite: false,
        jql: "project=X"
    };
    equal(server.requests[0].url, model.url());
    equal(server.requests[0].method, "PUT");
    deepEqual(JSON.parse(server.requests[0].requestBody), expectedPayload);
});

test("When a filter is saved with a new JQL, the attributes are updated with the server response", function() {
    var server = this.sandbox.useFakeServer();
    var model = new JIRA.Components.Filters.Models.Filter({id: 1234});

    model.saveFilter("project=X");
    //This is not a complete response. A complete response will include all the attributes for the model.
    server.requests[0].respond(200, {"Content-Type": "application/json"}, JSON.stringify({id: 1234, jql: 'project=X', name:'a'}));

    equal(model.id, 1234, "The new id is saved in the model");
    equal(model.get('jql'), "project=X", "The JQL is saved in the model");
    equal(model.get('name'), "a", "The name is saved in the model");
});

test("When a filter is saved successfully, it throws a JIRA.trace", function() {
    this.stub(JIRA, "trace");
    var server = this.sandbox.useFakeServer();
    var model = new JIRA.Components.Filters.Models.Filter({id: 1234});

    model.saveFilter("project=X");
    server.requests[0].respond(200);

    sinon.assert.calledOnce(JIRA.trace);
    sinon.assert.calledWith(JIRA.trace, "jira.filter.saved");

});