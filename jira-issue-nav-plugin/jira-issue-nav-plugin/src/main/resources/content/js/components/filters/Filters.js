AJS.namespace("JIRA.Components.Filters");

/**
 * @class JIRA.Components.Filters
 *
 * This class is the main entry point for the Filters component.
 *
 * @extends JIRA.Marionette.Controller
 */
JIRA.Components.Filters = JIRA.Marionette.Controller.extend({

    initialize: function(options) {
        this._initializeDialogController();
        this.searchPageModule = options.searchPageModule;

        this._initializeCollections(options);
        this._initializeSystemFiltersController();
        this._initializeFavouriteFiltersController();
        this._initializeFilterHeaderController();
    },

    _initializeCollections: function(options) {
        this.systemFiltersCollection = new JIRA.Components.Filters.Collections.SystemFilters(options.systemFilters);

        this.favouriteFiltersCollection = new JIRA.Components.Filters.Collections.FavouriteFilters([]);
        this.listenTo(this.favouriteFiltersCollection, {
            "change:favourite": function (filterModel, isFavourite) {
                if (!isFavourite) {
                    this.favouriteFiltersCollection.remove(filterModel);
                }
            },
            "remove": function(filterModel) {
                this.trigger('filterRemoved', { filterId: filterModel.getId() });
            }
        });
    },

    _initializeDialogController: function() {
        this.dialogController = new JIRA.Components.Filters.Controllers.Dialog();

        this.listenTo(this.dialogController, "delete:success", function(filterModel) {
            this.favouriteFiltersCollection.remove(filterModel);

            JIRA.Messages.showSuccessMsg(
                AJS.I18n.getText("issuenav.favourite.filters.delete.success.msg", AJS.escapeHtml(filterModel.getName())),
                JIRA.Issues.getDefaultMessageOptions()
            );

            JIRA.trace("jira.filter.saved");
        });

        this.listenTo(this.dialogController, "delete:error", function() {
            JIRA.trace("jira.filter.saved");
        });

        this.listenTo(this.dialogController, "rename:success", function(filterModel) {
            JIRA.Messages.showSuccessMsg(
                AJS.I18n.getText("issuenav.favourite.filters.rename.success.msg", AJS.escapeHtml(filterModel.getName())),
                JIRA.Issues.getDefaultMessageOptions()
            );

            JIRA.trace("jira.filter.saved");
        });

        this.listenTo(this.dialogController, "rename:error", function() {
            JIRA.trace("jira.filter.saved");
        });

        this.listenTo(this.dialogController, "copy:success", function(newFilterModel) {
            this.trigger("newFilter", newFilterModel);
            this._addFavouriteFilter(newFilterModel);
            JIRA.trace("jira.filter.saved");
        }, this);

        this.listenTo(this.dialogController, "copy:error", function() {
            JIRA.trace("jira.filter.saved");
        });

        this.listenTo(this.dialogController, "saveas:success", function(newFilterModel) {
            this.trigger("newFilter", newFilterModel);
            this._addFavouriteFilter(newFilterModel);
            JIRA.trace("jira.filter.saved");
        }, this);

        this.listenTo(this.dialogController, "saveas:error", function() {
            JIRA.trace("jira.filter.saved");
        });

    },

    _initializeSystemFiltersController: function() {
        this.systemFiltersController = new JIRA.Components.Filters.Controllers.List({
            collection: this.systemFiltersCollection,
            className: "system-filters",
            errorMessage: AJS.I18n.getText('issue.nav.filters.system.error'),
            loadingMessage: AJS.I18n.getText('issue.nav.filters.system.loading'),
            emptyMessage: AJS.I18n.getText('issue.nav.filters.system.empty'),
            loginMessage: AJS.I18n.getText('issue.nav.filters.system.anonymous', '<a class="login-link" href="' + JIRA.Issues.LoginUtils.redirectUrlToCurrent() + '">', '</a>')
        });

        this.systemFiltersController.on("selectFilter", function (filterModel) {
            this.headerController.closeDetails();
            this.trigger("filterSelected", filterModel.id);
        }, this);
    },

    _initializeFavouriteFiltersController: function() {
        this.favouriteFiltersController = new JIRA.Components.Filters.Controllers.ListWithActions({
            collection: this.favouriteFiltersCollection,
            className: "favourite-filters",
            title: AJS.I18n.getText('issue.nav.filters.fav.heading'),
            errorMessage: AJS.I18n.getText('issue.nav.filters.fav.error'),
            loadingMessage: AJS.I18n.getText('issue.nav.filters.fav.loading'),
            emptyMessage: AJS.I18n.getText('issue.nav.filters.fav.empty'),
            loginMessage: AJS.I18n.getText('issue.nav.filters.fav.anonymous', '<a class="login-link" href="' + JIRA.Issues.LoginUtils.redirectUrlToCurrent() + '">', '</a>')
        });
        this.favouriteFiltersController.on("selectFilter", function(filterModel) {
            this.headerController.closeDetails();
            this.trigger("filterSelected", filterModel.id);
        }, this);
        this.favouriteFiltersController.on("deleteFilter", function(filterModel) {
            this.showDeleteDialog(filterModel.id);
        }, this);
        this.favouriteFiltersController.on("renameFilter", function(filterModel) {
            this.showRenameDialog(filterModel.id);
        }, this);
        this.favouriteFiltersController.on("copyFilter", function(filterModel) {
            this.showCopyDialog(filterModel.id);
        }, this);
        this.favouriteFiltersController.on("unfavouriteFilter", function(filterModel) {
            filterModel.saveFavourite(false);
        }, this);
    },

    _initializeFilterHeaderController: function() {
        this.headerController = new JIRA.Components.Filters.Controllers.Header();

        this.listenTo(this.headerController, "saveAs", function(filterModel) {
            this.showSaveAsDialog(filterModel);
        });

        this.listenTo(this.headerController, "save", function(filterModel) {
            var filterName = AJS.escapeHtml(filterModel.getName());

            filterModel.saveFilter(this.searchPageModule.getEffectiveJql())
                .done(_.bind(function() {
                    JIRA.Messages.showSuccessMsg(
                        AJS.I18n.getText('issuenav.filters.save.success.msg', filterName),
                        JIRA.Issues.getDefaultMessageOptions()
                    );

                    this.trigger("savedFilter", filterModel);
                },this))
                .fail(function() {
                    JIRA.Messages.showErrorMsg(
                        AJS.I18n.getText('issuenav.filters.save.error.msg', filterName),
                        JIRA.Issues.getDefaultMessageOptions()
                    );
                });
        });

        this.listenTo(this.headerController, "discard", function() {
            this.trigger("filterDiscarded");
        });

        this.listenTo(this.headerController, "favourite", function(filterModel) {
            filterModel = this._addFavouriteFilter(filterModel);
            this.highlightFilter(filterModel);
            this.trigger("fitlerFavourited", filterModel);
        });
    },

    _addFavouriteFilter: function(filterModel) {
        var isFavourite = !!filterModel.getFavourite();
        var isInFavouriteCollection = !!this.favouriteFiltersCollection.get(filterModel.getId());

        if ( isFavourite && !isInFavouriteCollection) {
            this.favouriteFiltersCollection.add(filterModel);
        }

        return this.favouriteFiltersCollection.get(filterModel.getId());
    },

    getFilterById: function (filterId) {
        var filter = this.systemFiltersCollection.get(filterId) || this.favouriteFiltersCollection.get(filterId);
        var deferred = jQuery.Deferred();

        if (filter) {
            deferred.resolve(filter);
        } else {
            var model = new JIRA.Components.Filters.Models.Filter({ id: filterId });
            model.fetch({
                success: _.bind(function() {
                    if (model.getFavourite()) {
                        this.favouriteFiltersCollection.add(model, {merge: true});

                        // We need to return the model in the collection, because 'add' will only merge the attributes,
                        // not the 'cid'. The 'cid' is used to identify the views associated with this model.
                        deferred.resolve(this.favouriteFiltersCollection.get(model.id));
                    } else if (model.getIsSystem()) {
                        this.systemFiltersCollection.add(model, {merge: true});
                        deferred.resolve(this.systemFiltersCollection.get(model.id));
                    } else {
                        deferred.resolve(model);
                    }
                },this),
                error: function() {
                    model.setIsValid(false);
                    deferred.reject.apply(this, arguments);
                }
            });
        }

        return deferred.promise();
    },

    showDeleteDialog: function(filterId) {
        this.getFilterById(filterId).done(_.bind(function(filterModel) {
            this.dialogController.showDeleteDialog(filterModel);
        }, this));
    },

    showRenameDialog: function(filterId) {
        this.getFilterById(filterId).done(_.bind(function(filterModel) {
            this.dialogController.showRenameDialog(filterModel);
        }, this));
    },

    showCopyDialog: function(filterId) {
        this.getFilterById(filterId).done(_.bind(function(filterModel) {
            this.dialogController.showCopyDialog(filterModel);
        }, this));
    },

    showSaveAsDialog: function(filterModel) {
        this.searchPageModule.getJqlDeferred()
            .done(_.bind(function(jql) {
                this.dialogController.showSaveAsDialog(filterModel, jql);
            },this));
    },

    showSystemFilters: function(el) {
        this.systemFiltersController.show(el);
    },

    showFavouriteFilters: function(el){
        this.favouriteFiltersController.show(el);
    },

    showFilterHeader: function(options) {
        this.headerController.show({
            el: options.el,
            model: options.model,
            isDirty: options.isDirty
        });
    },

    updateFilterHeader: function(options) {
        this.headerController.update({
            model: options.model,
            isDirty: options.isDirty
        });
    },

    highlightFilter: function(filterModel) {
        this.systemFiltersController.highlightFilter(filterModel);
        this.favouriteFiltersController.highlightFilter(filterModel);
    },

    markFilterHeaderAsInvalid: function() {
        this.headerController.markAsInvalid();
    },

    fetchSystemFilters: function() {
        return this.systemFiltersCollection.fetch();
    },

    fetchFavouriteFilters: function() {
        return this.favouriteFiltersCollection.fetch({reset: true});
    }
});