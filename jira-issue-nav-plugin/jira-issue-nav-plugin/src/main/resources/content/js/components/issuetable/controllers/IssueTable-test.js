AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuetable-component-test");

(function ($, IssueTable) {
    "use strict";

    module("JIRA.Components.IssueTable.Controllers.IssueTable", {
        setup: function () {
            this.controller = new IssueTable({el: $("#qunit-fixture")});
        },

        resultsWithIssues: {
            hasIssues: true,
            currentSearch: {
                jql: "project = BULK",
                filterId: null
            },
            sortOptions: {},
            totalIssues: 42,
            totalDisplayableIssues: 42,
            startIndex: 0,
            pageSize: 10,
            pageNumber: 0,
            totalPages: 5,
            table: [
                "<table>",
                    "<tbody>",
                        "<tr class='focused issuerow' rel='10000'><td>JRA-1</td></tr>",
                        "<tr class='issuerow' rel='10010'><td>JRA-2</td></tr>",
                    "</tbody>",
                "</table>"
            ].join("")

        },

        resultsWithoutIssues: {
            hasIssues: false
        },

        assertRethrowEventFromView: function(eventName, viewNamespace, viewName) {
            var viewSpy = this.spy(viewNamespace, viewName);
            var eventSpy = this.spy();
            this.controller.on(eventName, eventSpy);

            this.controller.show(this.resultsWithIssues);
            viewSpy.thisValues[0].trigger(eventName);

            sinon.assert.calledOnce(eventSpy);
        }
    });

    test("It renders the table if search contain some results", function() {
        var renderSpy = this.stub(JIRA.Components.IssueTable.Views.IssueTableLayout.prototype, "render");
        var viewSpy = this.spy(JIRA.Components.IssueTable.Views, "IssueTableLayout");

        this.controller.show(this.resultsWithIssues);

        sinon.assert.calledOnce(viewSpy);
        ok(viewSpy.calledWithNew());
        sinon.assert.calledOnce(renderSpy);
    });

    test("It renders the empty results message if the search contains no results", function() {
        var renderSpy = this.stub(JIRA.Components.IssueTable.Views.EmptyResults.prototype, "render");
        var viewSpy = this.spy(JIRA.Components.IssueTable.Views, "EmptyResults");

        this.controller.show(this.resultsWithoutIssues);

        sinon.assert.calledOnce(viewSpy);
        ok(viewSpy.calledWithNew());
        sinon.assert.calledOnce(renderSpy);
    });

    test("It marks the rendered view as pending", function() {
        var emptyPendingSpy = this.spy(JIRA.Components.IssueTable.Views.EmptyResults.prototype, "showPending");
        var tablePendingSpy = this.spy(JIRA.Components.IssueTable.Views.IssueTableLayout.prototype, "showPending");

        this.controller.show(this.resultsWithoutIssues);
        this.controller.showPending();
        sinon.assert.calledOnce(emptyPendingSpy);

        this.controller.show(this.resultsWithIssues);
        this.controller.showPending();
        sinon.assert.calledOnce(tablePendingSpy);
    });

    test("It renders the error message", function() {
        var renderSpy = this.stub(JIRA.Components.IssueTable.Views.EmptyResults.prototype, "render");
        var viewSpy = this.spy(JIRA.Components.IssueTable.Views, "EmptyResults");

        this.controller.showErrorMessage();

        sinon.assert.calledOnce(viewSpy);
        ok(viewSpy.calledWithNew());
        sinon.assert.calledOnce(renderSpy);
    });

    test("When rendering the table, it closes the previously rendered view", function(){
        var closeSpy = this.spy(JIRA.Components.IssueTable.Views.IssueTableLayout.prototype, "close");
        this.controller.show(this.resultsWithIssues);

        this.controller.show(this.resultsWithIssues);

        sinon.assert.calledOnce(closeSpy);
    });

    test("When rendering the table, it renders the main table", function(){
        var renderSpy = this.stub(JIRA.Components.IssueTable.Views.IssueTable.prototype, "render");
        var viewSpy = this.spy(JIRA.Components.IssueTable.Views, "IssueTable");

        this.controller.show(this.resultsWithIssues);

        sinon.assert.calledOnce(viewSpy);
        sinon.assert.calledWithExactly(viewSpy, {
            resultsTable: this.resultsWithIssues.table,
            sortOptions: this.resultsWithIssues.sortOptions
        });
        ok(viewSpy.calledWithNew());
        sinon.assert.calledOnce(renderSpy);
    });

    test("When rendering the table, it renders the results count", function(){
        var renderSpy = this.stub(JIRA.Components.IssueNavigator.Views.ResultsCount.prototype, "render");
        var viewSpy = this.spy(JIRA.Components.IssueNavigator.Views, "ResultsCount");

        this.controller.show(this.resultsWithIssues);

        sinon.assert.calledOnce(viewSpy);
        sinon.assert.calledWithExactly(viewSpy, {
            total: this.resultsWithIssues.totalIssues,
            startIndex: this.resultsWithIssues.startIndex,
            pageSize: this.resultsWithIssues.pageSize
        });
        ok(viewSpy.calledWithNew());
        sinon.assert.calledOnce(renderSpy);
    });

    test("When rendering the table, it renders the pagination", function(){
        var renderSpy = this.stub(JIRA.Components.IssueNavigator.Views.Pagination.prototype, "render");
        var viewSpy = this.spy(JIRA.Components.IssueNavigator.Views, "Pagination");

        this.controller.show(this.resultsWithIssues);

        sinon.assert.calledOnce(viewSpy);
        sinon.assert.calledWithExactly(viewSpy, {
            total: this.resultsWithIssues.totalIssues,
            startIndex: this.resultsWithIssues.startIndex,
            pageSize: this.resultsWithIssues.pageSize,
            currentSearch: this.resultsWithIssues.currentSearch
        });
        ok(viewSpy.calledWithNew());
        sinon.assert.calledOnce(renderSpy);
    });

    test("When rendering the table, it renders the refresh results button", function(){
        var renderSpy = this.stub(JIRA.Components.IssueNavigator.Views.RefreshResults.prototype, "render");
        var viewSpy = this.spy(JIRA.Components.IssueNavigator.Views, "RefreshResults");

        this.controller.show(this.resultsWithIssues);

        sinon.assert.calledOnce(viewSpy);
        ok(viewSpy.calledWithNew());
        sinon.assert.calledOnce(renderSpy);
    });

    test("When rendering the table, it renders the end of stable message", function(){
        var renderSpy = this.stub(JIRA.Components.IssueNavigator.Views.EndOfStableMessage.prototype, "render");
        var viewSpy = this.spy(JIRA.Components.IssueNavigator.Views, "EndOfStableMessage");

        this.controller.show(this.resultsWithIssues);

        sinon.assert.calledOnce(viewSpy);
        sinon.assert.calledWithExactly(viewSpy, {
            total: this.resultsWithIssues.totalIssues,
            displayableTotal: this.resultsWithIssues.totalDisplayableIssues,
            pageNumber: this.resultsWithIssues.pageNumber,
            numberOfPages: this.resultsWithIssues.numberOfPages
        });
        ok(viewSpy.calledWithNew());
        sinon.assert.calledOnce(renderSpy);
    });

    test("When rendering the table, it stores the view's markup in $el", function() {
        this.controller.show(this.resultsWithIssues);
        equal(this.controller.$el.find("table").length, 1);
    });

    test("When rendering the table, it triggers the 'renderTable' event with $el as payload", function() {
        var onRenderTable = this.spy();

        this.controller.on("renderTable", onRenderTable);
        this.controller.show(this.resultsWithIssues);

        sinon.assert.calledOnce(onRenderTable);
        sinon.assert.calledWithExactly(onRenderTable, this.controller.$el);
    });

    test("When rendering the empty messages, it closes the previously rendered view", function(){
        var closeSpy = this.spy(JIRA.Components.IssueTable.Views.IssueTableLayout.prototype, "close");
        this.controller.show(this.resultsWithIssues);

        this.controller.show(this.resultsWithoutIssues);

        sinon.assert.calledOnce(closeSpy);
    });

    test("When rendering the empty message, it stores the view's markup in $el", function() {
        this.controller.show(this.resultsWithoutIssues);
        equal(this.controller.$el.find(".empty-results").length, 1);
    });

    test("It highlights an issue in the table", function () {
        var highlightIssueSpy = this.spy(JIRA.Components.IssueTable.Views.IssueTable.prototype, "highlightIssue");

        this.controller.show(this.resultsWithIssues);
        this.controller.highlightIssue(10000);

        sinon.assert.calledOnce(highlightIssueSpy);
        sinon.assert.calledWithExactly(highlightIssueSpy, 10000, undefined);
    });

    test("It marks an issue as inaccessible in the table", function () {
        var markIssueAsInaccessibleSpy = this.spy(JIRA.Components.IssueTable.Views.IssueTable.prototype, "markIssueAsInaccessible");

        this.controller.show(this.resultsWithIssues);
        this.controller.markIssueAsInaccessible(10000);

        sinon.assert.calledOnce(markIssueAsInaccessibleSpy);
        sinon.assert.calledWithExactly(markIssueAsInaccessibleSpy, 10000);
    });

    test("It updates an issue in the table", function () {
        var updateIssueSpy = this.spy(JIRA.Components.IssueTable.Views.IssueTable.prototype, "updateIssue");

        this.controller.show(this.resultsWithIssues);
        this.controller.updateIssue(10000, "<table></table>");

        sinon.assert.calledOnce(updateIssueSpy);
        sinon.assert.calledWithExactly(updateIssueSpy, 10000, "<table></table>");
    });

    test("It rethrows the 'columnsChanged' event from the table view", function() {
        this.assertRethrowEventFromView("columnsChanged", JIRA.Components.IssueTable.Views, "IssueTable");
    });

    test("It rethrows the 'highlightIssue' event from the table view", function() {
        this.assertRethrowEventFromView("highlightIssue", JIRA.Components.IssueTable.Views, "IssueTable");
    });

    test("It rethrows the 'issueRowUpdated' event from the table view", function() {
        this.assertRethrowEventFromView("issueRowUpdated", JIRA.Components.IssueTable.Views, "IssueTable");
    });

    test("It rethrows the 'sort' event from the table view", function() {
        this.assertRethrowEventFromView("sort", JIRA.Components.IssueTable.Views, "IssueTable");
    });

    test("It rethrows the 'goToPage' event from the pagination view", function() {
        this.assertRethrowEventFromView("goToPage", JIRA.Components.IssueNavigator.Views, "Pagination");
    });

    test("It rethrows the 'refresh' event from the refresh results view", function() {
        this.assertRethrowEventFromView("refresh", JIRA.Components.IssueNavigator.Views, "RefreshResults");
    });

}(AJS.$, JIRA.Components.IssueTable.Controllers.IssueTable));
