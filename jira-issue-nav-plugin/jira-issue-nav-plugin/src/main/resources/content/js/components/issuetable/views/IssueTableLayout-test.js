AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuetable-component-test");

(function (IssueTableLayout) {
    "use strict";

    module("JIRA.Components.IssueNavigator.Views.IssueTableLayout");

    test("It disables the pending state when it is rendered", function() {
        var layoutView = new IssueTableLayout();
        layoutView.$el.addClass("pending");
        layoutView.render();

        ok(!layoutView.$el.hasClass("pending"));
    });

    test("It marks the table as pending", function() {
        var layoutView = new IssueTableLayout();
        layoutView.render();

        layoutView.showPending();

        ok(layoutView.$el.hasClass("pending"));
    });

    test("It unmarks the table as pending", function() {
        var layoutView = new IssueTableLayout();
        layoutView.render();
        layoutView.showPending();

        layoutView.hidePending();

        ok(!layoutView.$el.hasClass("pending"));
    });

    test("It renders its internal regions", function() {
        function assertRegion(regionName) {
            var layoutView = new IssueTableLayout();
            layoutView.render();

            layoutView[regionName].ensureEl();
            ok(layoutView[regionName].$el.length !== 0, "Region '"+regionName+"' is present");
        }

        assertRegion("table");
        assertRegion("pagination");
        assertRegion("resultsCount");
        assertRegion("endOfStableMessage");
    });

}(JIRA.Components.IssueTable.Views.IssueTableLayout));
