AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:filters-component-test");

module('JIRA.Components.Filters.Controllers.List ', {
    mockModuleView: function() {
        var instance = {
            render: this.spy(),
            content: {
                show: this.spy()
            }
        };
        var constructor = this.stub(JIRA.Components.Filters.Views.List, "Module").returns(instance);
        return {
            constructor: constructor,
            instance: instance
        };
    },

    mockView: function(namespace, view) {
        var instance = {
            render: this.spy()
        };
        var constructor = this.stub(namespace, view).returns(instance);
        return {
            constructor: constructor,
            instance: instance
        };
    },

    mockListView: function() {
        var instance = {
            render: this.spy()
        };
        _.extend(instance, Backbone.Events);
        var constructor = this.stub(JIRA.Components.Filters.Views.List, "List").returns(instance);
        return {
            constructor: constructor,
            instance: instance
        };
    }
});

test("It renders the module view", function() {
    var controller = new JIRA.Components.Filters.Controllers.List({
        collection: new JIRA.Components.Filters.Collections.Filters([])
    });
    var view = this.mockModuleView();

    controller.show();

    sinon.assert.calledOnce(view.instance.render);
});

test("It renders the module view with a title", function() {
    var controller = new JIRA.Components.Filters.Controllers.List({
        collection: new JIRA.Components.Filters.Collections.Filters([]),
        title: "Module title"
    });
    var view = this.mockModuleView();

    controller.show();

    equal(view.constructor.firstCall.args[0].title, "Module title");
});

test("When the collection as been fetched with an error, it renders a message with the error text", function() {
    var controller = new JIRA.Components.Filters.Controllers.List({
        collection: new JIRA.Components.Filters.Collections.Filters([], {fetchState: "error"}),
        errorMessage: "There is an error",
        className: "my-saved-filters"
    });
    var view = this.mockView(JIRA.Components.Filters.Views.List, "Message");

    controller.show();

    sinon.assert.calledOnce(view.instance.render);
    equal(view.constructor.firstCall.args[0].text, "There is an error");
    equal(view.constructor.firstCall.args[0].className, "my-saved-filters");
});

test("When the collection has no fetch state, it renders a message with the loading text", function() {
    var controller = new JIRA.Components.Filters.Controllers.List({
        collection: new JIRA.Components.Filters.Collections.Filters([]),
        loadingMessage: "Loading...",
        className: "my-saved-filters"
    });
    var view = this.mockView(JIRA.Components.Filters.Views.List, "Message");

    controller.show();

    sinon.assert.calledOnce(view.instance.render);
    equal(view.constructor.firstCall.args[0].text, "Loading...");
    equal(view.constructor.firstCall.args[0].className, "my-saved-filters");
});

test("When the collection has been fetched and it is empty, it renders a message with the empty text", function() {
    this.stub(JIRA.Issues.LoginUtils, "isLoggedIn").returns(true);
    var controller = new JIRA.Components.Filters.Controllers.List({
        collection: new JIRA.Components.Filters.Collections.Filters([], {fetchState: "fetched"}),
        emptyMessage: "Nothing here",
        className: "my-saved-filters"
    });
    var view = this.mockView(JIRA.Components.Filters.Views.List, "Message");

    controller.show();

    sinon.assert.calledOnce(view.instance.render);
    equal(view.constructor.firstCall.args[0].text, "Nothing here");
    equal(view.constructor.firstCall.args[0].className, "my-saved-filters");
});

test("When rendering the empty message for an anonymous user, it renders a message with the login text", function() {
    this.stub(JIRA.Issues.LoginUtils, "isLoggedIn").returns(false);
    var controller = new JIRA.Components.Filters.Controllers.List({
        collection: new JIRA.Components.Filters.Collections.Filters([], {fetchState: "fetched"}),
        loginMessage: "Go to login",
        className: "my-saved-filters"
    });
    var view = this.mockView(JIRA.Components.Filters.Views.List, "Message");

    controller.show();

    sinon.assert.calledOnce(view.instance.render);
    equal(view.constructor.firstCall.args[0].text, "Go to login");
    equal(view.constructor.firstCall.args[0].className, "my-saved-filters");
});

test("When rendering the empty message, it renders the list if an item is added to the collection", function() {
    this.stub(JIRA.Issues.LoginUtils, "isLoggedIn").returns(false);

    var collection = new JIRA.Components.Filters.Collections.Filters([], {fetchState: "fetched"});
    var controller = new JIRA.Components.Filters.Controllers.List({
        collection: collection,
        className: "my-saved-filters"
    });
    var view = this.mockListView();

    controller.show();
    collection.add({id: "1234", name: "My filter"});

    sinon.assert.calledOnce(view.instance.render);
});

test("When the collection as been fetched with content, it renders the list view", function() {
    var collection = new JIRA.Components.Filters.Collections.Filters([
        {id: "1234", name: "My filter"}
    ], {fetchState: "fetched"});
    var controller = new JIRA.Components.Filters.Controllers.List({
        collection: collection,
        className: "my-saved-filters"
    });
    var view = this.mockListView();

    controller.show();

    sinon.assert.calledOnce(view.instance.render);
    equal(view.constructor.firstCall.args[0].collection, collection);
    equal(view.constructor.firstCall.args[0].className, "my-saved-filters");
});

test("When the collection as been fetched with content, it renders the empty view when all items from the collection are removed", function() {
    this.stub(JIRA.Issues.LoginUtils, "isLoggedIn").returns(true);

    var collection = new JIRA.Components.Filters.Collections.Filters([
        {id: "1234", name: "My filter"}
    ], {fetchState: "fetched"});
    var controller = new JIRA.Components.Filters.Controllers.List({
        collection: collection,
        emptyMessage: "Nothing here",
        className: "my-saved-filters"
    });
    var view = this.mockView(JIRA.Components.Filters.Views.List, "Message");

    controller.show();
    collection.remove(collection.get("1234"));

    sinon.assert.calledOnce(view.instance.render);
    equal(view.constructor.firstCall.args[0].text, "Nothing here");
    equal(view.constructor.firstCall.args[0].className, "my-saved-filters");
});

test("When a list is initially rendered, it triggers the 'render' event", function() {
    var collection = new JIRA.Components.Filters.Collections.Filters([
        {id: "1234", name: "My filter"}
    ], {fetchState: "fetched"});
    var controller = new JIRA.Components.Filters.Controllers.List({
        collection: collection
    });
    var onRender = this.spy();

    controller.on("render", onRender);
    controller.show();

    sinon.assert.calledOnce(onRender);
});

test("When a new item is rendered, it triggers the 'render' event", function() {
    var collection = new JIRA.Components.Filters.Collections.Filters([
        {id: "1234", name: "My filter"}
    ], {fetchState: "fetched"});
    var controller = new JIRA.Components.Filters.Controllers.List({
        collection: collection
    });
    var onRender = this.spy();

    controller.show();
    controller.on("render", onRender);
    collection.add({id: "4567", name: "My second filter"});

    sinon.assert.calledOnce(onRender);
});

test("When a filter is selected, it triggers the 'selectFilter' event", function() {
    var collection = new JIRA.Components.Filters.Collections.Filters([
        {id: "1234", name: "My filter"}
    ], {fetchState: "fetched"});
    var controller = new JIRA.Components.Filters.Controllers.List({
        collection: collection
    });
    var onSelectFilter = this.spy();
    controller.on("selectFilter", onSelectFilter);
    var view = this.mockListView();

    controller.show();
    view.instance.trigger("itemview:selectFilter", view, {model: collection.get("1234")});

    sinon.assert.calledOnce(onSelectFilter);
    sinon.assert.calledWith(onSelectFilter, collection.get("1234"));
});