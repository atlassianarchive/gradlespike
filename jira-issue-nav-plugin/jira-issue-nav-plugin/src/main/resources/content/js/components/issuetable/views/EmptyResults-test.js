AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuetable-component-test");

(function (EmptyResults) {
    "use strict";

    module("JIRA.Components.IssueTable.Views.EmptyResults", {
    });

    test("When rendering, if the user is not logged in it renders a login hint message", function() {
        this.stub(JIRA.Issues.LoginUtils, "isLoggedIn").returns(false);

        var view = new JIRA.Components.IssueTable.Views.EmptyResults()
        view.render();

        ok(view.$(".jira-adbox").hasClass("not-logged-in-message"), "It has the proper class");
        equal(view.$(".jira-adbox h3").text(), "issuenav.results.none.found");
        equal(view.$(".jira-adbox p").text(), "issuenav.results.none.hint.login");
    });

    test("When rendering, if the user is logged in and there are no issues in JIRA, it renders a create issue hint message", function() {
        this.stub(JIRA.Issues.LoginUtils, "isLoggedIn").returns(true);
        this.stub(JIRA.Issues.UserParms, "get").returns({
            createIssue: true
        });
        var view = new JIRA.Components.IssueTable.Views.EmptyResults({
            jiraHasIssues: false
        });
        view.render();

        ok(view.$(".jira-adbox").hasClass("empty-results-message"), "It has the proper class");
        equal(view.$(".jira-adbox h3").text(), "issuenav.results.none.created");
        equal(view.$(".jira-adbox p").text(), "issuenav.results.none.hint.firsttocreate");
    });

    test("When rendering, if the user is logged in and there are issues in JIRA, it renders a regular not-found message", function() {
        this.stub(JIRA.Issues.LoginUtils, "isLoggedIn").returns(true);
        this.stub(JIRA.Issues.UserParms, "get").returns({
            createIssue: true
        });
        var view = new JIRA.Components.IssueTable.Views.EmptyResults({
            jiraHasIssues: true
        });
        view.render();

        ok(view.$(".jira-adbox").hasClass("no-results-message"), "It has the proper class");
        equal(view.$(".jira-adbox h3").text(), "issuenav.results.none.found");
        equal(view.$(".jira-adbox p").text(), "issuenav.results.none.hint.modifyorcreate");
    });

    test("When rendering, if the user is logged in, there are issues in JIRA but the user has no creation permissions, it renders a regular not-found message", function() {
        this.stub(JIRA.Issues.LoginUtils, "isLoggedIn").returns(true);
        this.stub(JIRA.Issues.UserParms, "get").returns({
            createIssue: false
        });
        var view = new JIRA.Components.IssueTable.Views.EmptyResults({
            jiraHasIssues: true
        });
        view.render();

        ok(view.$(".jira-adbox").hasClass("no-results-message"), "It has the proper class");
        equal(view.$(".jira-adbox h3").text(), "issuenav.results.none.found");
        equal(view.$(".jira-adbox p").text(), "issuenav.results.none.hint.modify");
    });

    test("When rendering, if the user is not logged in it transforms the hint link into a login link", function() {
        this.stub(JIRA.Issues.LoginUtils, "isLoggedIn").returns(false);
        this.stub(JIRA.Issues.LoginUtils, "redirectUrlToCurrent").returns("/goToLogin");

        var view = new JIRA.Components.IssueTable.Views.EmptyResults({
            jiraHasIssues: true
        });

        // This is hideous. The link is actually added by the translated string, so we need to mock it :(
        this.stub(view,"serializeData").returns({
            hint:"login <a>link</a>"
        });
        view.render();

        equal(view.$('.no-results-hint a').attr("href"), "/goToLogin");
        ok(view.$('.no-results-hint a').hasClass("login-link"));
    });

    test("When rendering, if the user is not logged in it transforms the hint link into a createIssue link", function() {
        this.stub(JIRA.Issues.LoginUtils, "isLoggedIn").returns(true);
        this.stub(JIRA.Issues.LoginUtils, "redirectUrlToCurrent").returns("/goToLogin");

        var view = new JIRA.Components.IssueTable.Views.EmptyResults({
            jiraHasIssues: true
        });

        // This is hideous. The link is actually added by the translated string, so we need to mock it :(
        this.stub(view,"serializeData").returns({
            hint:"login <a>link</a>"
        });
        view.render();

        equal(view.$('.no-results-hint a').attr("href"), AJS.contextPath() + "/secure/CreateIssue!default.jspa");
        ok(view.$('.no-results-hint a').hasClass("create-issue"));
    });

    test("It can mark the view as pending", function() {
        var view = new JIRA.Components.IssueTable.Views.EmptyResults();
        view.showPending();

        ok(view.$el.hasClass("pending"));
    });

    test("It can unmark the view as pending", function() {
        var view = new JIRA.Components.IssueTable.Views.EmptyResults();
        view.showPending();
        view.hidePending();

        ok(!view.$el.hasClass("pending"));
    });

}(JIRA.Components.IssueTable.Views.EmptyResults));