AJS.namespace("JIRA.Components.Filters.Controllers.List");

/**
 * @class JIRA.Components.Filters.Controllers.List
 *
 * This controllers displays a list of filters
 *
 * @extends JIRA.Marionette.Controller
 */
JIRA.Components.Filters.Controllers.List = JIRA.Marionette.Controller.extend({
    /**
     * Main view rendered by this controller
     * @type {JIRA.Components.Filters.Views.List.Module}
     */
    view: null,

    /**
     * @param {Object} options
     * @param {JIRA.Components.Filters.Collections.Filters} options.collection Collection of filters
     */
    initialize: function(options) {
        this.collection = options.collection;
        this.title = options.title;
        this.className = options.className;
        this.errorMessage = options.errorMessage;
        this.loadingMessage = options.loadingMessage;
        this.emptyMessage = options.emptyMessage;
        this.loginMessage = options.loginMessage;
    },

    /**
     * Displays the filter's list in the provided element
     *
     * @param {jQuery} el Container for the filter's list
     */
    show: function(el) {
        if (this.view) {
            this.close();
        }

        this.view = new JIRA.Components.Filters.Views.List.Module({
            el: el,
            title: this.title
        });
        this.view.render();

        this._showInternalView(this.collection.fetchState);
        this.listenTo(this.collection, "change:fetchState", this._showInternalView);
    },

    close: function() {
        if (this.view) {
            this.stopListening(this.collection, "change:fetchState", this._showRegion);
            this.stopListening(this.view);
            this.view.close();
            this.view = null;
        }
    },

    /**
     * Highlight a filter, unhighlights all the other filters
     *
     * @param {JIRA.Components.Filters.Models.Filter} filterModel Model to highlight
     */
    highlightFilter: function(filterModel) {
        if (this._listView) {
            this._listView.unhighlightAllFilters();
            if (filterModel) {
                this._listView.highlightFilter(filterModel);
            }
        }
    },

    /**
     * Displays the internal view based on the state of the filter's collection.
     *
     * @param {string} fetchState Fetch state of the collection, valid values are "error", "fetched" or ""
     * @private
     */
    _showInternalView: function(fetchState) {
        switch (fetchState) {
            case "error":
                this._showError();
                break;
            case "fetched":
                this._showList();
                break;
            default:
                this._showLoading();
                break;
        }
    },

    _showLoading: function() {
        this.view.content.show(new JIRA.Components.Filters.Views.List.Message({
            className: this.className,
            text: this.loadingMessage
        }));
    },

    _showError: function() {
        this.view.content.show(new JIRA.Components.Filters.Views.List.Message({
            className: this.className,
            text: this.errorMessage
        }));
    },

    _showList: function() {
        var collection = this.collection;
        if (collection.length) {
            this._showListWithItems();
        } else {
            this._showEmptyList();
        }
    },

    _getListViewConstructor: function() {
        return JIRA.Components.Filters.Views.List.List
    },

    _showListWithItems: function() {
        var ViewConstructor = this._getListViewConstructor();
        this._listView = new ViewConstructor({
            collection: this.collection,
            className: this.className
        });
        this.view.content.show(this._listView);
        this.trigger("render");

        // When we remove the last item from the collection, render the empty list
        this.listenTo(this.collection, "remove", function f() {
            if (!this.collection.length) {
                this.stopListening(this.collection, "remove", f);
                this._showEmptyList();
            }
        });

        this.listenTo(this._listView, "itemview:selectFilter", function(itemView, args) {
            this.trigger("selectFilter", args.model);
        });
        this.listenTo(this._listView, "itemview:render", function() {
            this.trigger("render");
        });

        this.triggerMethod("list:render", this._listView);
    },

    _showEmptyList: function() {
        if (JIRA.Issues.LoginUtils.isLoggedIn()) {
            this.view.content.show(new JIRA.Components.Filters.Views.List.Message({
                className: this.className,
                text: this.emptyMessage
            }));
        } else {
            this.view.content.show(new JIRA.Components.Filters.Views.List.Message({
                className: this.className,
                text: this.loginMessage
            }));
        }

        // When we add a new item to the collection, render the list with items
        this.listenToOnce(this.collection, "add", function() {
            this._showListWithItems();
        });
    }
});