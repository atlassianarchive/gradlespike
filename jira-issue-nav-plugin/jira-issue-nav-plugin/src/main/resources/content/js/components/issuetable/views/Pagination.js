AJS.namespace("JIRA.Components.IssueNavigator.Views.Pagination");

/**
 * @class JIRA.Components.IssueNavigator.Views.Pagination
 *
 * This view renders a set of pagination links
 *
 * @extends JIRA.Marionette.ItemView
 *
 * @param {object} options Options
 * @param {string} options.currentSearch Search that produced these search results
 * @param {string} [options.currentSearch.jql] JQL that produced these search results
 * @param {string} [options.currentSearch.filterId] ID of the filter used for this search
 * @param {number} options.pageSize Size of each page
 * @param {number} options.startIndex Index of first issue displayed in the table
 * @param {number} options.total Number of issues in this search
 */
JIRA.Components.IssueNavigator.Views.Pagination = JIRA.Marionette.ItemView.extend({
    template: JIRA.Templates.IssueNavTable.pagination,

    events: {
        /**
         * @event goToPage
         * When the user clicks in a pagination link
         */
        "simpleClick a[data-start-index]": function(e) {
            e.preventDefault();
            var val = e.target.getAttribute("data-start-index");
            this.trigger("goToPage", parseInt(val, 10));
        }
    },

    serializeData: function() {
        return {
            startIndex: this.options.startIndex,
            pageSize: this.options.pageSize,
            searchQuery: this._getPagingUri(),
            displayableTotal: this.options.total
        };
    },

    /**
     * Construct a URL for an issue table pagination link.
     *
     * @return {string} the URL.
     */
    _getPagingUri: function() {
        return JIRA.Issues.URLSerializer.getURLFromState({
            selectedIssueKey: null,
            jql: this.options.currentSearch.jql,
            filter: this.options.currentSearch.filterId
        });
    }
});
