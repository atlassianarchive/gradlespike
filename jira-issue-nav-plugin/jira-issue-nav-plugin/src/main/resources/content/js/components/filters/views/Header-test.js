AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:filters-component-test");

module('JIRA.Components.Filters.Views.Header', {
    renderHeader: function() {
        var model = new JIRA.Components.Filters.Models.Filter({id: 1, name: "My filter", ownerUserName: "some user"});
        var isDirty;
        var sinon = this;

        return {
            withOthersFilter: function() {
                sinon.stub(JIRA.Issues.LoginUtils, "loggedInUserName").returns("myUser");
                model.set('ownerUserName', 'other user');
                return this;
            },
            withMyOwnFilter: function() {
                sinon.stub(JIRA.Issues.LoginUtils, "loggedInUserName").returns("myUser");
                model.set('ownerUserName', 'myUser');
                return this;
            },
            withFavouriteFilter: function() {
                sinon.stub(JIRA.Issues.LoginUtils, "loggedInUserName").returns("myUser");
                model.set('ownerUserName', 'other user');
                model.set('favourite', true);
                return this;
            },
            withNoFavouriteFilter: function() {
                sinon.stub(JIRA.Issues.LoginUtils, "loggedInUserName").returns("myUser");
                model.set('ownerUserName', 'other user');
                model.set('favourite', false);
                return this;
            },
            withSystemFilter: function() {
                model.set('isSystem', true);
                return this;
            },
            withInvalidFilter: function() {
                model.set('isValid', false);
                return this;
            },
            withDirtyFilter: function() {
                isDirty = true;
                return this;
            },
            withFilterName: function(name) {
                model.set("name",name);
                return this;
            },
            withLoggedInUser: function() {
                sinon.stub(JIRA.Issues.LoginUtils, "loggedInUserName").returns("logged in user");
                return this;
            },
            withAnonymousUser: function() {
                sinon.stub(JIRA.Issues.LoginUtils, "loggedInUserName").returns(undefined);
                return this;
            },
            withoutFilter: function() {
                model = null;
                return this;
            },
            render: function() {
                var view = new JIRA.Components.Filters.Views.Header({
                    el: jQuery("#qunit-fixture")
                });
                view.render({
                    model: model,
                    isDirty: isDirty
                });

                return view;
            }
        };
    }
});

test("It triggers the 'toogleFavourite' event when clicking on the star icon", function() {
    var eventHandler = this.spy();
    var view = this.renderHeader()
        .withOthersFilter()
        .render();
    view.on("toogleFavourite", eventHandler);

    view.$el.find(".fav-link").click();

    sinon.assert.calledOnce(eventHandler);
});

test("It triggers the 'saveAs' event when clicking on the 'Save As' button", function() {
    var eventHandler = this.spy();
    var view = this.renderHeader()
        .withOthersFilter()
        .render();
    view.on("saveAs", eventHandler);

    view.$el.find(".save-as-new-filter").click();

    sinon.assert.calledOnce(eventHandler);
});

test("It triggers the 'save' event when clicking on the 'Save' button", function() {
    var eventHandler = this.spy();
    var view = this.renderHeader()
        .withMyOwnFilter()
        .withDirtyFilter()
        .render();
    view.on("save", eventHandler);

    view.$el.find(".save-changes").click();

    sinon.assert.calledOnce(eventHandler);
});

test("It triggers the 'discard' event when clicking on the 'Discard changes' link", function() {
    var eventHandler = this.spy();
    var view = this.renderHeader()
        .withMyOwnFilter()
        .withDirtyFilter()
        .render();
    view.on("discard", eventHandler);

    view.$el.find(".discard-filter-changes").click();

    sinon.assert.calledOnce(eventHandler);
});

test("It triggers the 'details' event when clicking on the 'Details' link", function() {
    var eventHandler = this.spy();
    var view = this.renderHeader()
        .withOthersFilter()
        .render();
    view.on("details", eventHandler);

    view.$el.find(".show-filter-details").click();

    sinon.assert.calledOnce(eventHandler);
    sinon.assert.calledWith(eventHandler, view.ui.detailsTrigger);
});

test("It renders the 'Save' operation as main button when rendering dirty filter owned by the current user", function() {
    var view = this.renderHeader()
        .withMyOwnFilter()
        .withDirtyFilter()
        .render();

    equal(view.$el.find(".aui-button.save-changes").length, 1);
});

test("It does not render the 'Save' operation when the filter is not dirty", function() {
    var view = this.renderHeader()
        .withMyOwnFilter()
        .render();

    equal(view.$el.find(".save-changes").length, 0);
});

test("It does not render the 'Save' operation when the current user is not the owner of the filter", function() {
    var view = this.renderHeader()
        .withOthersFilter()
        .render();

    equal(view.$el.find(".save-changes").length, 0);
});

test("It renders the 'Save As' as main operation when the user is logged in", function() {
    var view = this.renderHeader()
        .withLoggedInUser()
        .render();

    equal(view.$el.find(".aui-button.save-as-new-filter").length, 1);
});

test("It renders the 'Save As' as secondary operation when the filter is dirty", function() {
    var view = this.renderHeader()
        .withMyOwnFilter()
        .withDirtyFilter()
        .render();

    equal(view.$el.find(".aui-button.save-as-new-filter").length, 0);
});

test("It does not render the 'Save As' operation when the user is anonymous", function() {
    var view = this.renderHeader()
        .withAnonymousUser()
        .render();

    equal(view.$el.find(".save-as-new-filter").length, 0);
});

test("It renders the 'Discard changes' operation when the filter is dirty", function() {
    var view = this.renderHeader()
        .withDirtyFilter()
        .render();

    equal(view.$el.find(".discard-filter-changes").length, 1);
});

test("It renders the 'Discard changes' operation when the filter is dirty and the user is anonymous", function() {
    var view = this.renderHeader()
        .withAnonymousUser()
        .withDirtyFilter()
        .render();

    equal(view.$el.find(".aui-button.discard-filter-changes").length, 1);
});

test("It does not render the 'Discard changes' operation when the filter is not dirty", function() {
    var view = this.renderHeader()
        .render();

    equal(view.$el.find(".discard-filter-changes").length, 0);
});

test("It renders the 'Details' if the filter is owned by the current user", function() {
    var view = this.renderHeader()
        .withMyOwnFilter()
        .render();

    equal(view.$el.find(".show-filter-details").length, 1);
});

test("It renders the 'Details' if the filter is owned by the another user", function() {
    var view = this.renderHeader()
        .withOthersFilter()
        .render();

    equal(view.$el.find(".show-filter-details").length, 1);
});

test("It renders the 'Details' if the filter is owned by the another user", function() {
    var view = this.renderHeader()
        .withOthersFilter()
        .render();

    equal(view.$el.find(".show-filter-details").length, 1);
});

test("It does not render the 'Details' if the filter is from the system", function() {
    var view = this.renderHeader()
        .withSystemFilter()
        .render();

    equal(view.$el.find(".show-filter-details").length, 0);
});

test("It displays the favourite star if the filter is owned by the current user", function() {
    var view = this.renderHeader()
        .withMyOwnFilter()
        .render();

    equal(view.$el.find(".fav-link").length, 1);
});

test("It displays the favourite star if the filter is owned by the another user", function() {
    var view = this.renderHeader()
        .withMyOwnFilter()
        .render();

    equal(view.$el.find(".fav-link").length, 1);
});

test("It does not display the favourite star if the filter is from the system", function() {
    var view = this.renderHeader()
        .withSystemFilter()
        .render();

    equal(view.$el.find(".fav-link").length, 0);
});

test("It does not display the favourite star for anonymous users", function() {
    var view = this.renderHeader()
        .withAnonymousUser()
        .render();

    equal(view.$el.find(".fav-link").length, 0);
});

test("The favourite star is on if the filter is favourite", function() {
    var view = this.renderHeader()
        .withFavouriteFilter()
        .render();

    equal(view.$el.find(".fav-link.on").length, 1);
});

test("The favourite star is off if the filter is favourite", function() {
    var view = this.renderHeader()
        .withNoFavouriteFilter()
        .render();

    equal(view.$el.find(".fav-link.off").length, 1);
});

test("It renders the 'Search' header with just the 'SaveAs' operation if there is no filter", function() {
    var view = this.renderHeader()
        .withoutFilter()
        .render();

    ok(view.$el.find(".search-title").text() === "search.search");
    equal(view.$el.find(".save-as-new-filter").length, 1, "Save As");
    equal(view.$el.find(".show-filter-details").length, 0, "Details");
    equal(view.$el.find(".discard-filter-changes").length, 0, "Discard changes");
    equal(view.$el.find(".save-changes").length, 0, "Save");
    equal(view.$el.find(".fav-link").length, 0, "Favourite");
});

test("It renders the 'Search' header with no operations if the filter is not valid", function() {
    var view = this.renderHeader()
        .withInvalidFilter()
        .render();

    ok(view.$el.find(".search-title").text() === "search.search");
    equal(view.$el.find(".save-as-new-filter").length, 0, "Save As");
    equal(view.$el.find(".show-filter-details").length, 0, "Details");
    equal(view.$el.find(".discard-filter-changes").length, 0, "Discard changes");
    equal(view.$el.find(".save-changes").length, 0, "Save");
    equal(view.$el.find(".fav-link").length, 0, "Favourite");
});

test("It renders the filter's name in the header", function() {
    var view = this.renderHeader()
        .withFilterName("This is my filter")
        .render();

    ok(view.$el.find(".search-title").text() === "This is my filter");
});

test("It renders the 'Edited' label when the view with a dirty filter is updated", function() {
    var view = this.renderHeader()
        .withMyOwnFilter()
        .withDirtyFilter()
        .render();

    view.render({
        model: view.model,
        isDirty: true
    });

    equal(view.$el.find(".filter-edited-item").text(),"search.edited");
});

test("It disables the 'Save' button when the filter is marked as invalid", function() {
    var view = this.renderHeader()
        .withMyOwnFilter()
        .withDirtyFilter()
        .render();

    view.markAsInvalid();

    equal(view.$el.find(".save-changes.disabled").length, 1);
});

test("It disables the 'Save As' button when the filter is marked as invalid", function() {
    var view = this.renderHeader()
        .withLoggedInUser()
        .render();

    view.markAsInvalid();

    equal(view.$el.find(".aui-button.save-as-new-filter").length, 1);
});