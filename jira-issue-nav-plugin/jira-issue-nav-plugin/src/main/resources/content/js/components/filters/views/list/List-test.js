AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:filters-component-test");

module("JIRA.Components.Filters.Views.List.List", {
    createView: function(options) {
        return new JIRA.Components.Filters.Views.List.List({
            collection: new JIRA.Components.Filters.Collections.Filters(options.models),
            className: options.className
        });
    }
});

test("When rendering, it renders a container for the filters", function() {
    var models = [
        new JIRA.Components.Filters.Models.Filter({ name: "My filter" })
    ];
    var view = this.createView({ models: models });

    view.render();

    ok(view.$el.is("ul"), "Is a UL");
    equal(view.$el.children("li").length, 1, "Contains the filter");
});

test("When rendering, it adds the specified class name to the container", function() {
    var models = [
        new JIRA.Components.Filters.Models.Filter({ name: "My filter" })
    ];
    var view = this.createView({ models: models, className: "my-filters" });

    view.render();

    ok(view.$el.hasClass("my-filters"));
});

test("It can highlighted a particular item as active", function() {
    var models = [
        new JIRA.Components.Filters.Models.Filter({ id: '1', name: "My filter 1" }),
        new JIRA.Components.Filters.Models.Filter({ id: '2', name: "My filter 2" })
    ];
    var view = this.createView({ models: models, className: "my-filters" });

    view.render();
    view.highlightFilter(models[0]);

    ok(view.$el.find("li a[data-id=1]").hasClass("active"), "First model is highlighted");
    ok(!view.$el.find("li a[data-id=2]").hasClass("active"), "Second model is not highlighted");
});

test("It can mark unhighlight all the existing items", function() {
    var models = [
        new JIRA.Components.Filters.Models.Filter({ id: '1', name: "My filter 1" }),
        new JIRA.Components.Filters.Models.Filter({ id: '2', name: "My filter 2" })
    ];
    var view = this.createView({ models: models, className: "my-filters" });

    view.render();
    view.highlightFilter(models[0]);
    view.unhighlightAllFilters();

    ok(!view.$el.find("li a[data-id=1]").hasClass("active"), "First model is not highlighted");
    ok(!view.$el.find("li a[data-id=2]").hasClass("active"), "Second model is not highlighted");
});
