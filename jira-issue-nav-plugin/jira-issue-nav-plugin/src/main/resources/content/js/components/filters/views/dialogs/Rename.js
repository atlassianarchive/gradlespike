AJS.namespace("JIRA.Components.Filters.Views.Dialogs.Rename");

/**
 * @class JIRA.Components.Filters.Views.Dialogs.Rename
 *
 * @extends JIRA.Marionette.DialogView
 */
JIRA.Components.Filters.Views.Dialogs.Rename = JIRA.Marionette.DialogView.extend({
    template: JIRA.Components.Filters.Templates.Dialogs.Rename,

    id: "rename-filter-dialog",

    serializeData: function() {
        return {
            modifierKey: AJS.Navigator.modifierKey(),
            filter: this.model.toJSON()
        };
    },

    /**
     * Generate the data to be sent in the form request
     *
     * @param {jQuery} form Form container
     * @returns {Object} Data to be sent in the request
     */
    formToRequestData: function(form) {
        return {
            name: form.find('[name=name]').val(),
            id: this.model.getId(),
            favourite: true
        };
    },

    onSubmitError: function() {
        this.dialog.$form.find("#filterName").focus();
    }
});