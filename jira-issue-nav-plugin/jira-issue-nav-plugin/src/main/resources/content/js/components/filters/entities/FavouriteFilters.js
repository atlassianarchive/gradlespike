AJS.namespace("JIRA.Components.Filters.Collections.FavouriteFilters");

/**
 * @class JIRA.Components.Filters.Collections.FavouriteFilters
 *
 * @extends JIRA.Components.Filters.Collections.Filters
 */
JIRA.Components.Filters.Collections.FavouriteFilters = JIRA.Components.Filters.Collections.Filters.extend({
    url: AJS.contextPath() + "/rest/api/2/filter/favourite?expand=subscriptions[-5:]",

    comparator: function(filter1, filter2) {
        return filter1.getName().toLowerCase().localeCompare(filter2.getName().toLowerCase());
    }
});