AJS.namespace("JIRA.Components.Filters.Views.List.Filter");

/**
 * @class JIRA.Components.Filters.Views.List.Filter
 *
 * Renders each individual item in the filter's list.
 *
 * @extends JIRA.Marionette.ItemView
 */
JIRA.Components.Filters.Views.List.Filter = JIRA.Marionette.ItemView.extend({
    template: JIRA.Components.Filters.Templates.List.Filter,

    ui: {
        filterLink: ".filter-link"
    },

    triggers: {
        "simpleClick .filter-link:not(.requires-login)": "selectFilter"
    },

    modelEvents: {
        "change": "render"
    },

    onRender: function() {
        this.unwrapTemplate();
    },

    highlight: function() {
        this.ui.filterLink.addClass("active");
        this.ui.filterLink.scrollIntoView();
    },

    unhighlight: function() {
        this.ui.filterLink.removeClass("active");
    }
});
