AJS.namespace("JIRA.Components.Filters.Views.List.Module");

/**
 * @class JIRA.Components.Filters.Views.List.Module
 *
 * Displays the main module for the list of filters. This view only renders the placeholder
 * for the other views (List, Empty or Error),  and an optional title
 *
 * @extends JIRA.Marionette.Layout
 */
JIRA.Components.Filters.Views.List.Module = JIRA.Marionette.Layout.extend({
    template: JIRA.Components.Filters.Templates.List.Module,

    /**
     * Title to display in the message
     * @type {string}
     */
    title: "",

    regions: {
        content: ".filter-content"
    },

    /**
     * @param {Object} options Options
     * @param {string} [options.title] Title of the module. If not provided, the module will be rendered without title markup.
     */
    initialize: function(options) {
        options = _.defaults({}, options, {
            title: ""
        });
        this.title = options.title;
    },

    templateHelpers: function() {
        return {
            title: this.title
        };
    }
});
