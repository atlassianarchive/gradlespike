AJS.namespace("JIRA.Components.IssueTable.Views.EmptyResults");

/**
 * @class JIRA.Components.IssueTable.Views.EmptyResults
 *
 * Renders a message that tells the user there are no issues, with an optional link to create an issue
 *
 * @extends JIRA.Marionette.ItemView
 *
 * @param {object} options Options
 * @param {boolean} options.jiraHasIssues Whether there are issues created in this JIRA instance
 */
JIRA.Components.IssueTable.Views.EmptyResults = JIRA.Marionette.ItemView.extend({
    template: JIRA.Templates.IssueNavTable.noResults,

    serializeData: function() {
        var message;
        var hint;
        var cssClass;
        var linkType;
        var createIssuePerm = JIRA.Issues.UserParms.get().createIssue;

        if (!JIRA.Issues.LoginUtils.isLoggedIn()) {
            message = AJS.I18n.getText('issuenav.results.none.found');
            hint = AJS.I18n.getText('issuenav.results.none.hint.login');
            cssClass = "not-logged-in-message";
            linkType = 'login';
        } else if (this.options.jiraHasIssues === false) {
            message = AJS.I18n.getText('issuenav.results.none.created');
            hint = createIssuePerm ? AJS.I18n.getText('issuenav.results.none.hint.firsttocreate') : null;
            cssClass = "empty-results-message";
            this.linkType = 'create';
        } else {
            message = AJS.I18n.getText('issuenav.results.none.found');
            hint = createIssuePerm ?
                AJS.I18n.getText('issuenav.results.none.hint.modifyorcreate') :
                AJS.I18n.getText('issuenav.results.none.hint.modify');
            cssClass = "no-results-message";
            this.linkType = 'create';
        }

        return {
            message: message,
            hint: hint,
            cssClass: cssClass
        };
    },

    onRender: function() {
        this.$el.addClass("empty-results");

        var $links = this.$('.no-results-hint a');
        if (!JIRA.Issues.LoginUtils.isLoggedIn()) {
            $links.attr('href', JIRA.Issues.LoginUtils.redirectUrlToCurrent()).addClass('login-link');
        } else {
            $links.addClass('create-issue').attr('href', AJS.contextPath() + "/secure/CreateIssue!default.jspa");
        }

        this.hidePending();
    },

    showPending: function() {
        this.$el.addClass('pending');
    },

    hidePending: function() {
        this.$el.removeClass("pending");
    }
});