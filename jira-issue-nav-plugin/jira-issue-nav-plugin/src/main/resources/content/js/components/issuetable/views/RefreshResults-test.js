AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuetable-component-test");

(function (RefreshResults) {
    "use strict";

    module("JIRA.Components.IssueNavigator.Views.RefreshResults");

    test("fires event when any anchor inside it is clicked", function() {
        var refreshView = new RefreshResults();
        var onRefresh = this.spy();
        refreshView.on("refresh", onRefresh);
        refreshView.render();
        var anchor = refreshView.$("a");

        anchor.click();
        sinon.assert.calledOnce(onRefresh);
    });

}(JIRA.Components.IssueNavigator.Views.RefreshResults));
