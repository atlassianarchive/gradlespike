AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:sinon");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

module("JIRA.Issues.IssueTableHeaderOperationsView", {
    setup: function () {
        //Stop inline layer from appending to window.top so qunit iframe runner works
        AJS.params.ignoreFrame = true;

        this.$el = AJS.$([
            '<ul class="operations">',
                '<li class="aui-dd-parent"><a href="#" class="header-views">views</a></li>',
                '<li class="aui-dd-parent"><a href="#" class="header-tools">tools</a></li>',
            '</ul>'
        ].join("")).appendTo("#qunit-fixture");

        this._searchPageModule = JIRA.Issues.TestUtils.mockSearchPageModule();
        this._searchPageModule.columnConfig.setCurrentColumnConfig("user");

        this.view = new JIRA.Issues.IssueTableHeaderOperationsView({
            el: this.$el,
            searchPageModule: this._searchPageModule,
            search: this._searchPageModule.search
        });

        this.preferredLayoutSpy = sinon.stub(JIRA.Issues.LayoutPreferenceManager, "getPreferredLayoutKey").returns("split-view");

        this.server = sinon.fakeServer.create();
    },

    teardown: function () {
        this.server.restore();
        this.preferredLayoutSpy.restore();
    }
});

test("Header tools dropdown content has no option with empty ajax", function () {
    this.server.respondWith("POST", /.*tools.*/,
        [200, { "Content-Type": "application/json" }, "[]"]);

    this.$el.find(".header-tools").click();
    this.server.respond();

    ok(AJS.$(".header-tools-menu").is(":visible"));
    equal(AJS.$(".header-tools-menu").length, 1);
    equal(AJS.$(".header-tools-menu").text(), "issue.nav.operations.tools.nooptions");
});

test("Header tools dropdown content has no column related options in SplitView layout", function () {
    this.server.respondWith("POST", /.*tools.*/,
        [200, { "Content-Type": "application/json" }, "[]"]);

    this.preferredLayoutSpy.returns("split-view");
    this.$el.find(".header-tools").click();
    this.server.respond();
    ok(this.server.requests[0].requestBody.match(/skipColumns=true/), "Request to the server includes skipColumns = true" );

});

test("Header tools dropdown content has column related options in ListView layout", function () {
    this.server.respondWith("POST", /.*tools.*/,
        [200, { "Content-Type": "application/json" }, "[]"]);

    this.preferredLayoutSpy.returns("list-view");
    this.$el.find(".header-tools").click();
    this.server.respond();
    ok(this.server.requests[0].requestBody.match(/skipColumns=false/), "Request to the server includes skipColumns = false" );
});

test("Header tools dropdown is not shown if trigger is no longer visible", function () {
    this.server.respondWith("POST", /.*tools.*/,
        [200, { "Content-Type": "application/json" }, "[]"]);

    var headerToolsTrigger = this.$el.find(".header-tools");
    headerToolsTrigger.click();

    headerToolsTrigger.hide();

    this.server.respond();

    ok(!AJS.$(".header-tools-menu").is(":visible"));
});

test("_updateFilterPageResources", function () {
    var filter = new JIRA.Components.Filters.Models.Filter({id: 1, jql: "issuetype = Bug"}),
        JQL = "issuetype = Story";

    // For unmodified filters, only filter-id should be set.
    this._searchPageModule.setFilter(filter);
    this.view._updateFilterPageResources();
    equal(AJS.Meta.get("filter-id"), filter.getId(), "filter-id is set");
    equal(AJS.Meta.get("filter-jql"), undefined, "filter-jql isn't set");

    // For modified filters, only filter-jql should be set (both the gadgets and
    // share plugins ignore filter-jql if filter-id is set, so we only set one).
    this._searchPageModule.set({filter: filter, jql: JQL});
    this.view._updateFilterPageResources();
    equal(AJS.Meta.get("filter-id"), undefined, "filter-id isn't set");
    equal(AJS.Meta.get("filter-jql"), JQL, "filter-jql is set");

    // For plain ol' JQL, only filter-jql should be set.
    this._searchPageModule.set({jql: JQL});
    this.view._updateFilterPageResources();
    equal(AJS.Meta.get("filter-id"), undefined, "filter-id isn't set");
    equal(AJS.Meta.get("filter-jql"), JQL, "filter-jql is set");
});