AJS.namespace("JIRA.Components.Filters.Views.Header");

/**
 * @class JIRA.Components.Filters.Controllers.Header
 *
 * @extends JIRA.Marionette.ItemView
 */
JIRA.Components.Filters.Views.Header = JIRA.Marionette.ItemView.extend({
    operations: {
        save: {
            styleClass: "save-changes",
            label: AJS.I18n.getText("common.words.save")
        },
        saveAs: {
            styleClass: "save-as-new-filter",
            label: AJS.I18n.getText("search.save.new.filter"),
            href: AJS.contextPath() + "/secure/SaveAsFilter!default.jspa"
        },
        discardChanges: {
            styleClass: "discard-filter-changes",
            label: AJS.I18n.getText("search.discard.filter.changes")
        },
        details: {
            styleClass: "show-filter-details",
            title: AJS.I18n.getText("issuenav.filters.details.button.title"),
            label: AJS.I18n.getText("issuenav.filters.details.label")
        },
        favouriteOn: {
            title: AJS.I18n.getText('issuenav.filters.fav.button.on'),
            label: AJS.I18n.getText('issuenav.filters.fav.button.on'),
            styleClass: "fav-link icon on"
        },
        favouriteOff: {
            title: AJS.I18n.getText('issuenav.filters.fav.button.off'),
            label: AJS.I18n.getText('issuenav.filters.fav.button.off'),
            styleClass: "fav-link icon off"
        }
    },

    template: JIRA.Components.Filters.Templates.Header.Main,

    ui: {
        filterFavouriteButton: ".fav-link",
        filterEditedLabel: ".filter-edited-item",
        saveButtons: '.save-as-new-filter, .save-changes',
        detailsTrigger: '.show-filter-details',
        dropdownContent: '#js-edited-content'
    },

    triggers: {
        'click .fav-link': "toogleFavourite",
        "click .save-as-new-filter": {
            event: "saveAs",
            preventDefault: true,
            stopPropagation: false
        },
        "click .save-changes": {
            event: "save",
            preventDefault: true,
            stopPropagation: false
        },
        "click .discard-filter-changes": {
            event: "discard",
            preventDefault: true,
            stopPropagation: false
        }
    },

    events: {
        "click .show-filter-details": function() {
            this.trigger("details", this.ui.detailsTrigger);
        }
    },

    _generatePrimaryOperations: function() {
        var operations = [];

        if (this.isDirty && this.model.isMyFilter()) {
            operations.push(this.operations.save);
        }

        if (!!JIRA.Issues.LoginUtils.loggedInUserName()) {
            operations.push(this.operations.saveAs);
        }

        if (this.isDirty) {
            operations.push(this.operations.discardChanges);
        }

        return operations;
    },

    _generateSecondaryOperations: function() {
        var operations = [];

        if (!this.model.getIsSystem()) {
            operations.push(this.operations.details);
        }

        if (!!JIRA.Issues.LoginUtils.loggedInUserName() && !this.model.getIsSystem()) {
            var operation = this.model.isFavouriteFilter() ? this.operations.favouriteOn : this.operations.favouriteOff;
            operations.href = AJS.contextPath() + "/secure/EditFilter!default.jspa?filterId=" + this.model.id;
            operations.push(operation);
        }

        return operations;
    },

    serializeData: function() {
        if (!this.model) {
            return {
                title: AJS.I18n.getText("search.search"),
                primaryButton: this.operations.saveAs
            };
        }

        if (!this.model.getIsValid()) {
            return {
                title: AJS.I18n.getText("search.search")
            };
        }

        var primaryOperations = this._generatePrimaryOperations();
        var secondaryOperations = this._generateSecondaryOperations();

        var renderData = {
            title: this.model.getName(),
            primaryButton: primaryOperations.shift(),
            primaryOps: primaryOperations,
            secondaryOps: secondaryOperations
        };

        if (this.model.getOwner()) {
            _.extend(renderData, {
                owner: this.model.getOwner() || "",
                ownerUserName: this.model.getOwnerUserName() || "",
                avatarUrl: this.model.getAvatarUrl(),
                ownerUrl: AJS.contextPath() + '/secure/ViewProfile.jspa?name=' + encodeURIComponent(this.model.getOwnerUserName())
            });
        }

        return renderData;
    },

    render: function(options) {
        this.model = options.model;
        this.isDirty = options.isDirty;
        return JIRA.Marionette.ItemView.prototype.render.call(this);
    },

    onRender: function() {
        this.ui.dropdownContent.on("aui-dropdown2-show", _.bind(function(e) {
            //HACK to use Backbone's event delegation on the dropdown content.
            //Backbone's 'delegateEvents()' works only on 'this.$el', so we need to set it before calling
            //the delegate method, and restore it afterwards to the real value.
            var viewEl = this.$el;
            this.$el = jQuery(e.target);
            this.delegateEvents();
            this.$el = viewEl;
        },this));

        new JIRA.Issues.Tipsy({el: this.ui.filterFavouriteButton});
    },

    markAsInvalid: function() {
        this.ui.saveButtons.addClass("disabled");
    }
});
