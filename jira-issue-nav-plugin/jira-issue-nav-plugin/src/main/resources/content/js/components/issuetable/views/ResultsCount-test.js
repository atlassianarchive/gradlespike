AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuetable-component-test");

(function (jQuery, AJS, ResultsCount) {
    "use strict";

    module("JIRA.Components.IssueNavigator.Views.ResultsCount");

    test("render when total greater than page size", function() {
        this.spy(AJS,"format");
        var resultsCountView = new ResultsCount({
            startIndex: 0,
            pageSize: 10,
            total: 95
        });

        resultsCountView.render();

        equal(jQuery(AJS.format.firstCall.args[1]).text(), 1, "Results start number is correct");
        equal(jQuery(AJS.format.firstCall.args[2]).text(), 10, "Results end number is correct");
        equal(jQuery(AJS.format.firstCall.args[3]).text(), 95, "Results total number is correct");
    });

    test("render when total less than page size", function() {
        this.spy(AJS,"format");
        var resultsCountView = new ResultsCount({
            startIndex: 0,
            pageSize: 10,
            total: 5
        });

        resultsCountView.render();

        equal(jQuery(AJS.format.firstCall.args[1]).text(), 1, "Results start number is correct");
        equal(jQuery(AJS.format.firstCall.args[2]).text(), 5, "Results end number is correct");
        equal(jQuery(AJS.format.firstCall.args[3]).text(), 5, "Results total number is correct");
    });

}(AJS.$, AJS, JIRA.Components.IssueNavigator.Views.ResultsCount));