AJS.namespace("JIRA.Components.Filters.Controllers.Header");

/**
 * @class JIRA.Components.Filters.Controllers.Header
 *
 * @extends JIRA.Marionette.Controller
 */
JIRA.Components.Filters.Controllers.Header = JIRA.Marionette.Controller.extend({
    /**
     * @constructor
     */
    initialize: function() {
        this.view = null;
    },

    close: function() {
        if (this.view) {
            this.view.close();
            this.stopListening(this.view);
            this.view = null;
            this.trigger("close");
        }
    },

    closeDetails: function() {
        if (this.detailsView) {
            this.detailsView.close();
            this.stopListening(this.detailsView);
            this.detailsView = null;
        }
    },

    showDetails: function(triggerEl) {
        if (this.detailsView) {
            this.closeDetails();
        }

        this.detailsView = new JIRA.Components.Filters.Views.Details({
            model: this.model,
            triggerEl: triggerEl
        });
        this.listenTo(this.detailsView, "close", this.closeDetails);

        this.detailsView.render();
    },

    show: function(options) {
        if (this.view) {
            this.close();
        }

        this.view = new JIRA.Components.Filters.Views.Header({
            el: options.el
        });
        this.isInvalid = false;

        this.listenTo(this.view, {
            "saveAs": function(args) {
                if (!JIRA.Issues.LoginUtils.isLoggedIn() || this.isInvalid) return;
                this.trigger("saveAs", args.model);
            },
            "save": function(args) {
                if (!JIRA.Issues.LoginUtils.isLoggedIn() || this.isInvalid) return;
                this.trigger("save", args.model);
            },
            "discard": function() {
                this.trigger("discard");
            },
            "details": function(triggerEl) {
                this.showDetails(triggerEl);
            },
            "toogleFavourite": function(args) {
                var model = args.model;

                if (!model) return;

                var currentlyFavourited = !!model.getFavourite();
                model.saveFavourite(!currentlyFavourited);
                // Need to add to the favourite filters collection if a filter has just been starred
                if (!currentlyFavourited) {
                    this.trigger("favourite", model);
                }
            },
            "close": this.close
        });

        this.update(options);
    },

    update: function(options) {
        if (!this.view) {
            return;
        }

        this.isInvalid = false;

        this.view.render({
            model: options.model,
            isDirty: options.isDirty
        });

        this.model = options.model;
    },

    markAsInvalid: function() {
        if (this.view) {
            this.view.markAsInvalid();
        }
        this.isInvalid = true;
    }
});