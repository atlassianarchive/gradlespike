AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:filters-component-test");

module("JIRA.Components.Filters.Views.Actions", {
    createView: function(model) {
        var triggerView = new JIRA.Components.Filters.Views.List.FilterWithActions({
            model: model
        });
        triggerView.render();
        return new JIRA.Components.Filters.Views.Actions({
            model: model,
            triggerView: triggerView
        });
    },

    assertClickTriggersEvent: function(clickSelector, event) {
        var model = new JIRA.Components.Filters.Models.Filter({
            name: "My filter",
            id: "1234"
        });
        var view = this.createView(model);
        var eventSpy = this.spy();

        view.on(event, eventSpy);
        view.render();
        view.$el.find(clickSelector).click();

        sinon.assert.calledOnce(eventSpy);
        equal(eventSpy.firstCall.args[0].model, model, "The model is in the event args");
        ok(eventSpy.firstCall.args[0].view === view, "The view is in the event args");
    }
});

test("When clicking on rename-filter link, it triggers a renameFilter event", function() {
    this.assertClickTriggersEvent(".rename-filter", "renameFilter");
});

test("When clicking on copy-filter link, it triggers a copyFilter event", function() {
    this.assertClickTriggersEvent(".copy-filter", "copyFilter");
});

test("When clicking on unfavourite-filter link, it triggers a unfavouriteFilter event", function() {
    this.assertClickTriggersEvent(".unfavourite-filter", "unfavouriteFilter");
});

test("When clicking on delete-filter link, it triggers a deleteFilter event", function() {
    this.assertClickTriggersEvent(".delete-filter", "deleteFilter");
});

test("When rendering the view, it exposes the triggerView as a property", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        name: "My filter",
        id: "1234"
    });
    var triggerView = new JIRA.Components.Filters.Views.List.FilterWithActions({
        model: model
    });
    var view = new JIRA.Components.Filters.Views.Actions({
        model: model,
        triggerView: triggerView
    });

    ok(view.triggerView === triggerView);
});

test("When rendering the view, it exposes the dropDown as a property", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        name: "My filter",
        id: "1234"
    });
    var view = this.createView(model);
    view.render();

    ok(view.dropDown instanceof AJS.Dropdown);
});

test("When rendering the view, it renders only the read-only options if the filter is not editable", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        name: "My filter",
        id: "1234",
        ownerUserName: "owner_uer"
    });
    this.stub(JIRA.Issues.LoginUtils, "loggedInUserName").returns("another_user");
    var view = this.createView(model);
    view.render();

    equal(view.$el.find("li").length, 2, "Two items in the dropdown");
    equal(view.$el.find("li").eq(0).text(), "common.words.copy", "First link is Copy");
    equal(view.$el.find("li").eq(1).text(), "issuenav.filters.actions.remove_from_favourites", "Second link is Remove");
});

test("When rendering the view, it renders all the options if the filter is editable", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        name: "My filter",
        id: "1234",
        ownerUserName: "owner_uer"
    });
    this.stub(JIRA.Issues.LoginUtils, "loggedInUserName").returns("owner_uer");
    var view = this.createView(model);
    view.render();

    equal(view.$el.find("li").length, 4, "Four items in the dropdown");
    equal(view.$el.find("li").eq(0).text(), "issuenav.filters.actions.rename", "First link is Rename");
    equal(view.$el.find("li").eq(1).text(), "common.words.copy", "Second link is Copy");
    equal(view.$el.find("li").eq(2).text(), "issuenav.filters.actions.remove_from_favourites", "Third link is Remove");
    equal(view.$el.find("li").eq(3).text(), "common.words.delete", "Fourth link is Delete");
});

test("When rendering the view, the filter is marked as selected", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        name: "My filter",
        id: "1234"
    });
    var view = this.createView(model);

    view.render();
    ok(view.triggerView.$el.hasClass("hover"));
    ok(view.triggerView.$el.find("span").hasClass("active"));
});

test("When the dropdown is closed, the view is closed", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        name: "My filter",
        id: "1234"
    });
    var view = this.createView(model);
    var closeSpy = this.spy();
    view.on("close", closeSpy);

    view.render();
    view.dropDown.hide();

    sinon.assert.calledOnce(closeSpy);
});

test("When closing the view, the dropdown is hidden", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        name: "My filter",
        id: "1234"
    });
    var view = this.createView(model);
    var closeSpy = this.spy();

    view.render();
    view.dropDown.bind("hideLayer", closeSpy);
    view.close();

    sinon.assert.calledOnce(closeSpy);
});

test("When closing the view, the dropdown placeholder is removed", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        name: "My filter",
        id: "1234"
    });
    var view = this.createView(model);

    view.render();
    var placeholder = view.dropDown.layerController.placeholder();
    view.close();

    equal(placeholder.parent().length, 0);
});

test("When closing the view, the filter is marked as unselected", function() {
    var model = new JIRA.Components.Filters.Models.Filter({
        name: "My filter",
        id: "1234"
    });
    var view = this.createView(model);

    view.render();
    view.close();
    ok(!view.triggerView.$el.hasClass("hover"));
    ok(!view.triggerView.$el.find("span").hasClass("active"));
});
