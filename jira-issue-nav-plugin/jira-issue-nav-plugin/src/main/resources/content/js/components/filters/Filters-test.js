AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:filters-component-test");

module('JIRA.Components.Filters', {
    setup: function() {
        this.filtersComponent = new JIRA.Components.Filters({
            searchPageModule: new JIRA.Issues.SearchPageModule()
        });
        this.server = sinon.fakeServer.create();

        this.favouriteFiltersCollection = this.filtersComponent.favouriteFiltersCollection;
        this.systemFiltersCollection = this.filtersComponent.systemFiltersCollection;

        // Avoid displaying the message in the test
        sinon.stub(JIRA.Messages, "showSuccessMsg");
    },

    teardown: function() {
        JIRA.Messages.showSuccessMsg.restore();
    },

    mockFilterRequest: function(filterData) {
        this.server.respondWith("GET", new RegExp("/rest/api/2/filter/(\\d+)"), [200, { "Content-Type": "application/json" }, JSON.stringify(filterData)]);
    },

    requestFilter: function(filterId) {
        var onDone = this.requestFilterWithoutServerResponse(filterId);
        this.server.respond();
        return onDone;
    },

    requestFilterWithoutServerResponse: function(filterId) {
        var onDone = this.spy();
        var response = this.filtersComponent.getFilterById(filterId);
        response.done(onDone);
        return onDone;
    }
});

test("It loads a filter model form the network", function() {
    this.mockFilterRequest({id: 101, name: "the fake filter"});

    var onDone = this.requestFilter(101);

    sinon.assert.calledOnce(onDone);
    equal(onDone.getCall(0).args[0] instanceof JIRA.Components.Filters.Models.Filter, true, "should be a filter model");
    equal(onDone.getCall(0).args[0].get('name'), "the fake filter", "returned data for our fake filter");
});

test("If the filter is favourite, it adds it to the favourites collection", function() {
    this.mockFilterRequest({id: 101, favourite: true});

    this.requestFilter(101);

    equal(this.favouriteFiltersCollection.length, 1, "There is one filter in the collection");
    ok(this.favouriteFiltersCollection.get(101) instanceof JIRA.Components.Filters.Models.Filter, "The fake filter is in the collection");
});

test("If the filter is system, it adds it to the system collection", function() {
    this.mockFilterRequest({id: 101, isSystem: true});

    this.requestFilter(101);

    equal(this.systemFiltersCollection.length, 1, "There is one filter in the collection");
    ok(this.systemFiltersCollection.get(101) instanceof JIRA.Components.Filters.Models.Filter, "The fake filter is in the collection");
});

test("If the filter is already in the favourites collection, it returns the filter from the collection", function() {
    this.favouriteFiltersCollection.add({id: 101, name: "my filter", favourite: true});

    var onDone = this.requestFilter(101);

    equal(onDone.getCall(0).args[0], this.favouriteFiltersCollection.get(101));
});

test("If the filter is already in the system collection, it returns the filter from the collection", function() {
    this.systemFiltersCollection.add({id: 101, name: "my filter", isSystem: true});

    var onDone = this.requestFilter(101);

    equal(onDone.getCall(0).args[0], this.systemFiltersCollection.get(101));
});

test("If the filter appears on the favourite collection while it was being requested, it merges the new data with the data already present in the collection", function() {
    var fakeFilterInTheCollection = {id: 101, favourite: true};
    this.mockFilterRequest({id: 101, name: "my filter", favourite: true});

    var onDone = this.requestFilterWithoutServerResponse(101);
    this.favouriteFiltersCollection.add(fakeFilterInTheCollection);
    this.server.respond();

    sinon.assert.calledOnce(onDone);
    equal(onDone.getCall(0).args[0] instanceof JIRA.Components.Filters.Models.Filter, true, "should be a filter model");
    equal(onDone.getCall(0).args[0].get('name'),"my filter", "returned data for our fake filter");
});

test("If the filter appears on the system collection while it was being requested, it merges the new data with the data already present in the collection", function() {
    var fakeFilterInTheCollection = {id: 101, isSystem: true};
    this.mockFilterRequest({id: 101, name: "my filter", isSystem: true});

    var onDone = this.requestFilterWithoutServerResponse(101);
    this.systemFiltersCollection.add(fakeFilterInTheCollection);
    this.server.respond();

    sinon.assert.calledOnce(onDone);
    equal(onDone.getCall(0).args[0] instanceof JIRA.Components.Filters.Models.Filter, true, "should be a filter model");
    equal(onDone.getCall(0).args[0].get('name'),"my filter", "returned data for our fake filter");
});

test("It raises a 'filterRemoved' event when a filter is removed from the favourites collection", function() {
    var fakeFilterInTheCollection = {id: 101, favourite: true};
    this.favouriteFiltersCollection.add(fakeFilterInTheCollection);
    var onRemove = this.spy();

    this.filtersComponent.on("filterRemoved", onRemove);
    this.favouriteFiltersCollection.remove(101);

    sinon.assert.calledOnce(onRemove);
    sinon.assert.calledWith(onRemove, {filterId: 101});
});

test("It deletes a filter form the collection when the DeleteDialog is successfully submitted", function() {
    var fakeFilterInTheCollection = {id: 101, name: "My fake filter"};
    this.favouriteFiltersCollection.add(fakeFilterInTheCollection);

    this.filtersComponent.dialogController.trigger("delete:success", this.favouriteFiltersCollection.get(101));

    equal(this.favouriteFiltersCollection.length, 0);
});

test("It triggers a 'filterSelected' event when a system filter is selected", function() {
    var onSelect = this.spy();
    var fakeFilter = new JIRA.Components.Filters.Models.Filter({id: 100});
    this.filtersComponent.on("filterSelected", onSelect);

    this.filtersComponent.systemFiltersController.trigger("selectFilter", fakeFilter);

    sinon.assert.calledOnce(onSelect);
    sinon.assert.calledWith(onSelect, 100);
});

test("Marking a filter as favourite from the header, saves it in the favourites collection", function() {
    var filterModel = new JIRA.Components.Filters.Models.Filter({id: "1", name: "a filter", favourite: true});

    this.filtersComponent.headerController.trigger('favourite', filterModel);
    ok(this.filtersComponent.favouriteFiltersCollection.contains(filterModel), "The filter is added to the favourites collection");
});


test("Marking a filter as favourite from the header, it is marked as active", function() {
    var filterModel = new JIRA.Components.Filters.Models.Filter({id: "1", name: "a filter", favourite: true});
    this.spy(this.filtersComponent, "highlightFilter");

    this.filtersComponent.headerController.trigger('favourite', filterModel);

    sinon.assert.calledOnce(this.filtersComponent.highlightFilter);
    sinon.assert.calledWith(this.filtersComponent.highlightFilter, filterModel);
});

test("When saving a filter, it throws the 'savedFilter' event with the model as argument", function() {
    var filterModel = new JIRA.Components.Filters.Models.Filter({id: "1", name: "a filter", favourite: true});
    this.stub(filterModel, "saveFilter").returns(jQuery.Deferred().resolve().promise());
    var onNewFilter = this.spy();
    this.filtersComponent.on('savedFilter', onNewFilter);

    this.filtersComponent.headerController.trigger('save', filterModel);

    sinon.assert.calledOnce(onNewFilter);
    sinon.assert.calledWith(onNewFilter, filterModel);
});

test("When saving a filter, it shows a successful message", function() {
    var filterModel = new JIRA.Components.Filters.Models.Filter({id: "1", name: "a filter", favourite: true});
    this.stub(filterModel, "saveFilter").returns(jQuery.Deferred().resolve().promise());

    this.filtersComponent.headerController.trigger('save', filterModel);

    sinon.assert.calledOnce(JIRA.Messages.showSuccessMsg);
    sinon.assert.calledWith(JIRA.Messages.showSuccessMsg, 'issuenav.filters.save.success.msg');
});

