AJS.namespace("JIRA.Components.IssueTable.Views.IssueTableLayout");

JIRA.Components.IssueTable.Views.IssueTableLayout = JIRA.Marionette.Layout.extend({
    template: JIRA.Templates.IssueNavTable.structure,

    regions: {
        pagination: ".pagination",
        table: ".issue-table-container",
        resultsCount: ".results-count",

        //This region is actually rendered inside 'ResultsCount' view
        refreshResults: {
            selector: ".refresh-table",
            regionType: JIRA.Marionette.ReplaceRegion
        },
        endOfStableMessage: {
            selector: ".end-of-stable-message",
            regionType: JIRA.Marionette.ReplaceRegion
        }
    },

    onRender: function() {
        this.hidePending();
    },

    showPending: function() {
        this.$el.addClass('pending');
    },

    hidePending: function() {
        this.$el.removeClass("pending");
    }
});
