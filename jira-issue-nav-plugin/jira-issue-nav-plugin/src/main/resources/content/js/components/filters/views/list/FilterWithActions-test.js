AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:filters-component-test");

module("JIRA.Components.Filters.Views.List.Filter", {
    createView: function(options) {
        return new JIRA.Components.Filters.Views.List.FilterWithActions({
            model: options.model
        });
    }
});

test("When rendering a filter, it should display the action's trigger", function() {
    var model = new JIRA.Components.Filters.Models.Filter({ name: "My filter" });
    var view = this.createView({ model: model });

    view.render();

    ok(jQuery.contains(view.el, view.ui.actionsTrigger[0]));
});

test("When clicking on the actions triggers, it should fire the 'openActions' event", function() {
    var model = new JIRA.Components.Filters.Models.Filter({ name: "My filter" });
    var view = this.createView({ model: model });
    var openActionsSpy = this.spy();

    view.on("openActions", openActionsSpy);
    view.render();

    view.ui.actionsTrigger.click();

    sinon.assert.calledOnce(openActionsSpy);
    ok(openActionsSpy.getCall(0).args[0].view === view, "Includes the view in the event arguments");
    ok(openActionsSpy.getCall(0).args[0].model === model, "Includes the model in the event arguments");
});

test("It should provide access to the actionsTrigger", function() {
    var model = new JIRA.Components.Filters.Models.Filter({ name: "My filter" });
    var view = this.createView({ model: model });

    view.render();

    ok(view.getActionsTrigger() === view.ui.actionsTrigger);
});