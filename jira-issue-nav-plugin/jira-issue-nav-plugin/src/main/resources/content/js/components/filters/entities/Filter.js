AJS.namespace("JIRA.Components.Filters.Models.Filter");

/**
 * @class JIRA.Components.Filters.Models.Filter
 *
 * Represents a saved (normally favourite) filter, also known as a "Saved Search".
 *
 * @extends JIRA.Issues.Brace.Model
 */
JIRA.Components.Filters.Models.Filter = JIRA.Issues.Brace.Model.extend({

    namedAttributes: [
        "id",
        "name",
        "description",
        "jql",
        "favourite",
        "isSystem",
        "sharePermissions",
        "subscriptions",
        "ownerUserName",
        "ownerDisplayName",
        "avatarUrl",
        "requiresLogin",
        "isValid"
    ],

    defaults: {
        isSystem: false,
        isValid: true
    },

    urlRoot: AJS.contextPath() + "/rest/api/2/filter/",

    getOwner: function () {
        return this.getOwnerDisplayName() || this.getOwnerUserName();
    },

    isFavouriteFilter: function () {
        return !this.getIsSystem() && !!this.getFavourite();
    },

    url: function() {
        return this.urlRoot + encodeURIComponent(this.id) + "?expand=subscriptions[-5:]";
    },

    isMyFilter: function () {
        return !this.getIsSystem() && this.getOwnerUserName() === JIRA.Issues.LoginUtils.loggedInUserName();
    },

    parse: function(response) {
        // Reverse the subscriptions so they're sorted newest to oldest.
        if (response.subscriptions && response.subscriptions.items) {
            response.subscriptions.items.reverse();
        }

        return {
            id: response.id,
            name: response.name,
            description: response.description,
            jql: response.jql,
            isSystem: response.isSystem,
            favourite: response.favourite,
            sharePermissions: response.sharePermissions || [],
            subscriptions: response.subscriptions,
            ownerUserName: response.owner && response.owner.name,
            ownerDisplayName: response.owner && response.owner.displayName,
            avatarUrl: response.owner && response.owner.avatarUrls['48x48'],
            requiresLogin: response.requiresLogin
        };
    },

    /**
     * Not all the subscriptions are retrieved from the server. This function
     * returns the number of un-retrieved subscriptions for the filter.
     */
    getNumberOfAdditionalSubscriptions: function() {
        var subs = this.getSubscriptions();
        return subs.size > subs.items.length ? subs.size - subs.items.length : 0;
    },

    saveFavourite: function(isFavourite) {
        var instance = this;
        var prevState = this.getFavourite();

        // Optimistically set favourite status
        this.setFavourite(isFavourite);

        // Abort previous pending request
        if (this.pendingRequest) {
            this.pendingRequest.abort();
        }

        this.pendingRequest = JIRA.SmartAjax.makeRequest({
            // This is v1 of the REST API. TODO: add the ability to set favourite to REST API v2
            url: AJS.contextPath() + '/rest/api/1.0/filters/' + encodeURIComponent(this.getId()) + '/favourite',
            type: isFavourite ? 'PUT' : 'DELETE'
        }).done(function(data) {
            var faveState = AJS.$(data).text().match(/favourite state: (\w+)/)[1] === 'true';
            delete instance.pendingRequest;
            instance.setFavourite(faveState);
        }).fail(function() {
            instance.setFavourite(prevState);
        });

        return this.pendingRequest;
    },

    saveFilter: function(newJql) {

        // Abort previous pending request
        if (this.pendingRequest) {
            this.pendingRequest.abort();
        }

        var json = this.toJSON();
        var data = {
            id: json.id,
            name: json.name,
            jql: newJql,
            favourite: json.favourite
        };

        var instance = this;
        this.pendingRequest = JIRA.SmartAjax.makeRequest({
            url: this.url(),
            type: 'PUT',
            contentType: "application/json",
            data: JSON.stringify(data)
        }).done(function(filterData) {
            instance.set(instance.parse(filterData));
        }).then(function() {
            delete instance.pendingRequest;
            JIRA.trace("jira.filter.saved");
        });

        return this.pendingRequest;
    },

    toJSON: function() {
        var json = JIRA.Issues.BaseModel.prototype.toJSON.apply(this);
        _.extend(json, {
            requiresLoginForUser: this._requiresLoginForUser(this.getRequiresLogin()),
            navUrl: this._constructNavUrl(this.getId(), this.getRequiresLogin())
        });
        return json;
    },

    _requiresLoginForUser: function(requiresLogin) {
        return !JIRA.Issues.LoginUtils.isLoggedIn() && requiresLogin;
    },

    _constructNavUrl: function(filterId, requiresLogin) {
        var navUrl = '/issues/?filter=' + filterId;
        // Set url to redirect to login if user is not logged in
        if (this._requiresLoginForUser(requiresLogin)) {
            return JIRA.Issues.LoginUtils.redirectUrl(navUrl);
        }
        else {
            return AJS.contextPath() + navUrl;
        }
    }
});
