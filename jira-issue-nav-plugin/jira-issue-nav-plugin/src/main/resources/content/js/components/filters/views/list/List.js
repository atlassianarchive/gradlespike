AJS.namespace("JIRA.Components.Filters.Views.List.List");

/**
 * @class JIRA.Components.Filters.Views.List.List
 *
 * Displays a list of filters
 *
 * @extends JIRA.Marionette.CompositeView
 */
JIRA.Components.Filters.Views.List.List = JIRA.Marionette.CompositeView.extend({
    template: JIRA.Components.Filters.Templates.List.List,
    itemView: JIRA.Components.Filters.Views.List.Filter,
    itemViewContainer: ".filter-list",

    onRender: function() {
        this.unwrapTemplate();
    },

    templateHelpers: function() {
        return {
            className: this.className
        };
    },

    unhighlightAllFilters: function() {
        this.children.apply("unhighlight");
    },

    /**
     * Highlight a filter. If the model does not exist in the collection represented by this list,
     * this method does nothing.
     *
     * @param {JIRA.Components.Filters.Models.Filter} filterModel Model to highlight
     */
    highlightFilter: function(filterModel) {
        var itemView = this.children.findByModel(filterModel);
        if (itemView) {
            itemView.highlight();
        }
    }
});
