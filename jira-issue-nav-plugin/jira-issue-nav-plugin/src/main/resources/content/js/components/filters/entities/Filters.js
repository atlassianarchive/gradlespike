AJS.namespace("JIRA.Components.Filters.Collections.Filters");

/**
 * @class JIRA.Components.Filters.Collections.Filters
 *
 * A collection of FilterModel objects.
 *
 * @extends Backbone.Collection
 */
JIRA.Components.Filters.Collections.Filters = JIRA.Issues.Brace.Collection.extend({

    model: JIRA.Components.Filters.Models.Filter,

    fetch: function () {
        var promise = JIRA.Issues.Brace.Collection.prototype.fetch.apply(this, arguments);

        promise.done(_.bind(function () {
            this.fetchState = "fetched";
            this.trigger("change:fetchState", this.fetchState);
        }, this));

        promise.fail(_.bind(function () {
            this.fetchState = "error";
            this.trigger("change:fetchState", this.fetchState);
        }, this));

        return promise;
    },

    initialize: function(models, options) {
        options = options || {};
        this.fetchState = options.fetchState;
    }
});
