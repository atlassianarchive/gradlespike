AJS.namespace("JIRA.Components.Filters.Controllers.ListWithActions");

/**
 * @class JIRA.Components.Filters.Controllers.ListWithActions
 *
 * @extends JIRA.Components.Filters.Controllers.List
 */
JIRA.Components.Filters.Controllers.ListWithActions = JIRA.Components.Filters.Controllers.List.extend({
    /**
     * Actions view (i.e. dropdown) associated to a filter
     * @type {JIRA.Components.Filters.Views.Actions}
     */
    actionsView: null,

    _getListViewConstructor: function() {
        return JIRA.Components.Filters.Views.List.ListWithActions;
    },

    close: function() {
        this._closeActions();
        JIRA.Components.Filters.Controllers.List.prototype.close.call(this);
    },

    highlightFilter: function(filterModel) {
        this._closeActions();
        JIRA.Components.Filters.Controllers.List.prototype.highlightFilter.call(this, filterModel);
    },

    /**
     * Displays the Actions view related to a filter.
     *
     * @param {JIRA.Components.Filters.Views.List.Filter} triggerView View that triggered the actions dropdown.
     * @param {JIRA.Components.Filters.Models.Filter} filterModel Filter's model.
     * @private
     */
    _showActions: function(triggerView, filterModel) {
        var isActionsForFilterAlreadyOpen = (this.actionsView && this.actionsView.model === filterModel);
        this._closeActions();

        // If actions for this filter is already open, do not open it again
        if (!isActionsForFilterAlreadyOpen) {
            this.actionsView = new JIRA.Components.Filters.Views.Actions({
                triggerView: triggerView,
                model: filterModel
            });
            this.actionsView.render();

            this.listenTo(this.actionsView, "deleteFilter", function(args) {
                this._closeActions();
                this.trigger("deleteFilter", args.model);
            });

            this.listenTo(this.actionsView, "renameFilter", function(args) {
                this._closeActions();
                this.trigger("renameFilter", args.model);
            });

            this.listenTo(this.actionsView, "copyFilter", function(args) {
                this._closeActions();
                this.trigger("copyFilter", args.model);
            });

            this.listenTo(this.actionsView, "unfavouriteFilter", function(args) {
                this._closeActions();
                this.trigger("unfavouriteFilter", args.model);
            });
        }
    },

    _closeActions: function() {
        if (this.actionsView) {
            this.actionsView.close();
            this.stopListening(this.actionsView);
            this.actionsView = null;
        }
    },

    onListRender: function() {
        this.listenTo(this._listView, "itemview:selectFilter", function() {
            this._closeActions();
        });

        this.listenTo(this._listView, "itemview:openActions", function(itemView, args) {
            this._showActions(args.view, args.model);
        });
    }
});