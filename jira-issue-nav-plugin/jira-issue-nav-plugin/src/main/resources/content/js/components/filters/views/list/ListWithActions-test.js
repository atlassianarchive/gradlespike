AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:filters-component-test");

module("JIRA.Components.Filters.Views.List.List", {
    createView: function(options) {
        return new JIRA.Components.Filters.Views.List.ListWithActions({
            collection: new JIRA.Components.Filters.Collections.Filters(options.models),
            className: options.className
        });
    }
});

test("When rendering using actions, it renders the items with an actions trigger", function() {
    var models = [
        new JIRA.Components.Filters.Models.Filter({ name: "My filter" })
    ];
    var view = this.createView({ models: models });

    view.render();

    equal(view.$el.find("a.filter-actions").length, 1, "Contains the actions trigger");
});