(function ($) {
    /**
     * The issue table rendered in <tt>JIRA.Issues.FullScreenLayout</tt>.
     */
    Backbone.define("JIRA.Issues.IssueTableView", JIRA.Issues.BaseView.extend({
        syncSystemModeMessage: function() {
            var currentMessage = $("#system-mode-warning-msg");
            var isSystemMode = this.columnConfig.isSystemMode();
            if (isSystemMode && !currentMessage.length) {
                var $msg = JIRA.Messages.showWarningMsg(
                    //TODO Move to a template
                    AJS.I18n.getText("issues.components.column.config.system.warning.post"), {
                        id: "system-mode-warning-msg",
                        closeable: false,
                        timeout: 0 //Don't close the message automatically
                    }
                );

                $msg.find(".exit").click(_.bind(function(e) {
                    e.preventDefault();
                    this.columnConfig.setCurrentColumnConfig("user");
                }, this));
                
            } else {
                currentMessage.remove();
            } 
        },
        
        /**
         * @param {object} options
         * @param {JIRA.Issues.SearchModule} options.search The application's <tt>JIRA.Issues.SearchModule</tt> instance.
         * @param {JIRA.Issues.SearchResults} options.searchResults The application's <tt>JIRA.Issues.SearchResults</tt> instance.
         */
        initialize: function (options) {
            this.columnConfig = options.columnConfig;
            this.columnConfig.on("change:columnConfig", this.syncSystemModeMessage, this);
            JIRA.Issues.onVerticalResize(_.bind(this.adjustColumnConfigHeight, this));
        },

        /**
         * Prepare to be removed, unbinding all event handlers, etc.
         */
        deactivate: function() {
            JIRA.Issues.offVerticalResize(this.adjustColumnConfigHeight);
        },

        /**
         * Render the issue table after a search completes.
         * <p/>
         * Called when an operation in <tt>searchPromise</tt> completes.
         *
         * @param {object} table The search payload.
         * @private
         */
        _onSearchDone: function (el) {
            this.columnConfig.setElement(el.find(".column-picker-trigger-container")).render();
        },

        adjustColumnConfigHeight: function () {
            this.columnConfig.adjustHeight();
        }
    }));
})(jQuery);
