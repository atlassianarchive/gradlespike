AJS.namespace("JIRA.Components.Filters.Views.List.ListWithActions");

/**
 * @class JIRA.Components.Filters.Views.List.List
 *
 * Displays a list of filters
 *
 * @extends JIRA.Components.Filters.Views.List.List
 */
JIRA.Components.Filters.Views.List.ListWithActions = JIRA.Components.Filters.Views.List.List.extend({
    itemView: JIRA.Components.Filters.Views.List.FilterWithActions,

    /**
     * Whether the items of the list should include the actions dropdown
     * @type {boolean}
     */
    useActions: false,

    /**
     * @param {Object} options Options
     * @param {boolean} [options.useActions=false] Whether the action's trigger should be displayed
     */
    initialize: function(options) {
        options = _.defaults({}, options, {
            useActions: false
        });

        this.useActions = options.useActions;
    }
});
