AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuetable-component-test");

(function ($, IssueTable) {
    "use strict";

    module("JIRA.Components.IssueTable", {
        setup: function() {
            sinon.stub(JIRA.Issues.Application, "request").withArgs("issueEditor:canDismissComment").returns(true);
            sinon.stub(JIRA.Issues.Application, "execute");

            var searchModule = new JIRA.Issues.MockSearchModule();
            var columnConfig = JIRA.Issues.TestUtils.mockColumnConfig();
            var searchService = new JIRA.Components.SearchService({
                searchModule: searchModule,
                searchResults: searchModule.getResults(),
                columnConfig: columnConfig
            });

            this.issueTable = new IssueTable({
                columnConfig: columnConfig,
                searchService: searchService,
                el: $("#qunit-fixture")
            });
        },

        teardown: function() {
            JIRA.Issues.Application.request.restore();
            JIRA.Issues.Application.execute.restore();
        }
    });

    test("It should trigger a JIRA.Events.NEW_CONTENT_ADDED when the table is rendered", function () {
        var triggerSpy = this.sandbox.spy(JIRA, "trigger");

        var table = jQuery("<div></div>")
        this.issueTable.issueTableController.trigger("renderTable", table);

        equal(triggerSpy.callCount, 1, "One event is triggered");
        equal(triggerSpy.firstCall.args[0], JIRA.Events.NEW_CONTENT_ADDED, "First event is JIRA.Events.NEW_CONTENT_ADDED");
        equal(triggerSpy.firstCall.args[1][1], JIRA.CONTENT_ADDED_REASON.issueTableRefreshed, "First event reason is JIRA.CONTENT_ADDED_REASON.issueTableRefreshed");

        triggerSpy.restore();
    });

    test("It should trigger a JIRA.Events.NEW_CONTENT_ADDED when the empty message is rendered", function () {
        var triggerSpy = this.sandbox.spy(JIRA, "trigger");

        var table = jQuery("<div></div>")
        this.issueTable.issueTableController.trigger("renderEmpty", table);

        equal(triggerSpy.callCount, 1, "One event is triggered");
        equal(triggerSpy.firstCall.args[0], JIRA.Events.NEW_CONTENT_ADDED, "First event is JIRA.Events.NEW_CONTENT_ADDED");
        equal(triggerSpy.firstCall.args[1][1], JIRA.CONTENT_ADDED_REASON.issueTableRefreshed, "First event reason is JIRA.CONTENT_ADDED_REASON.issueTableRefreshed");

        triggerSpy.restore();
    });

    test("It should trigger a JIRA.Events.NEW_CONTENT_ADDED when an issue is updated", function () {
        var triggerSpy = this.sandbox.spy(JIRA, "trigger");

        var table = jQuery("<div></div>")
        this.issueTable.issueTableController.trigger("issueRowUpdated", table);

        equal(triggerSpy.callCount, 1, "One event is triggered");
        equal(triggerSpy.firstCall.args[0], JIRA.Events.NEW_CONTENT_ADDED, "Event is JIRA.Events.NEW_CONTENT_ADDED");
        equal(typeof triggerSpy.firstCall.args[1][1], "string", "Ensure event has a reason argument");
        equal(triggerSpy.firstCall.args[1][1], JIRA.CONTENT_ADDED_REASON.issueTableRowRefreshed, "Event reason is JIRA.CONTENT_ADDED_REASON.issueTableRowRefreshed");

        triggerSpy.restore();
    });


// TODO Move to SearchResults
//test("First issue is selected on load", function() {
//    this.issueTableView.setElement(this.$bodyEl);
//    this.searchResults.resetFromSearch(mockIssueTableJSON().issueTable);
//    this.issueTableView._onSearchDone(mockIssueTableJSON().issueTable.table);
//    equal(this.$bodyEl.find("#issuetable tr.focused").length, 1);
//    equal(this.$bodyEl.find("#issuetable tr.focused").data("issuekey"), "XSS-21");
//});

// TODO Move to SearchResults
//test("selectNext", function() {
//    this.issueTableView.setElement(this.$bodyEl);
//    this.searchResults.resetFromSearch(mockIssueTableJSON().issueTable);
//    this.issueTableView._onSearchDone(mockIssueTableJSON().issueTable.table);
//    this.searchResults.highlightNextIssue();
//    equal(this.$bodyEl.find("#issuetable tr.focused").length, 1);
//    equal(this.$bodyEl.find("#issuetable tr.focused").data("issuekey"), "XSS-19");
//});

// TODO Move to SearchResults
//test("selectPrev", function() {
//    this.issueTableView.setElement(this.$bodyEl);
//    this.searchResults.resetFromSearch(mockIssueTableJSON().issueTable);
//    this.issueTableView._onSearchDone(mockIssueTableJSON().issueTable.table);
//    this.searchResults.highlightNextIssue();
//    equal(this.$bodyEl.find("#issuetable tr.focused").data("issuekey"), "XSS-19");
//    this.searchResults.highlightPrevIssue();
//    equal(this.$bodyEl.find("#issuetable tr.focused").length, 1);
//    equal(this.$bodyEl.find("#issuetable tr.focused").data("issuekey"), "XSS-21");
//});


})(jQuery, JIRA.Components.IssueTable);