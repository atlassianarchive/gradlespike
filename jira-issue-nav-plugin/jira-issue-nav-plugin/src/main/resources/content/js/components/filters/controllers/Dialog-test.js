AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:filters-component-test");

module('JIRA.Components.Filters.Controllers.Dialog ', {
    setup: function() {
        this.filterModel = new JIRA.Components.Filters.Models.Filter({
            id: "1234"
        });
    },

    buildDialogController: function() {
        return new JIRA.Components.Filters.Controllers.Dialog({});
    },

    /**
     * Asserts that a new dialog is shown.
     *
     * @param {string} constructorName Name of the dialog constructor. Valid values: "Delete", "Rename",
     *                                 "Copy" and "SaveAs".
     * @param {string} showMethod Method used to display the dialog. Valid values: "showDeleteDialog",
     *                            "showRenameDialog", "showCopyDialog" and "showSaveAsDialog".
     */
    assertDialogIsRendered: function(constructorName, showMethod) {
        var dialogController = this.buildDialogController();
        var renderStub = this.stub(JIRA.Components.Filters.Views.Dialogs[constructorName].prototype, "render");

        dialogController[showMethod]();

        sinon.assert.calledOnce(renderStub);
    },

    /**
     * Asserts that a dialog throws an error event when the form is submitted with errors.
     *
     * @param {string} constructorName Name of the dialog constructor. Valid values: "Delete", "Rename",
     *                                 "Copy" and "SaveAs".
     * @param {string} showMethod Method used to display the dialog. Valid values: "showDeleteDialog",
     *                            "showRenameDialog", "showCopyDialog" and "showSaveAsDialog".
     * @param {string} eventToTest Method that the dialog would throw in case of an error.
     */
    assertErrorEvent: function(constructorName, showMethod, eventToTest) {
        var dialogController = this.buildDialogController();
        var eventSpy = this.spy();
        this.stub(JIRA.Components.Filters.Views.Dialogs[constructorName].prototype, "render");

        dialogController[showMethod]();
        dialogController.on(eventToTest, eventSpy);
        dialogController.activeView.trigger('submit:error');

        sinon.assert.calledOnce(eventSpy);
    },

    /**
     * Asserts that a dialog throws a success event when the form is submitted without errors.
     *
     * @param {string} constructorName Name of the dialog constructor. Valid values: "Delete", "Rename",
     *                                 "Copy" and "SaveAs".
     * @param {string} showMethod Method used to display the dialog. Valid values: "showDeleteDialog",
     *                            "showRenameDialog", "showCopyDialog" and "showSaveAsDialog".
     * @param {string} eventToTest Method that the dialog would throw when the form is submitted.
     * @param {Object} serverResponse Fake response from the server when the form is submitted.
     */
    assertSuccessEvent: function(constructorName, showMethod, eventToTest, serverResponse) {
        var dialogController = this.buildDialogController();
        var eventSpy = this.spy();
        this.stub(JIRA.Components.Filters.Views.Dialogs[constructorName].prototype, "render");

        dialogController[showMethod](this.filterModel);
        dialogController.on(eventToTest, eventSpy);
        dialogController.activeView.trigger('submit:success', serverResponse);

        sinon.assert.calledOnce(eventSpy);
        return eventSpy.firstCall.args[0];
    }
});

test("It shows the delete dialog", function() {
    this.assertDialogIsRendered("Delete", "showDeleteDialog");
});

test("It shows the rename dialog", function() {
    this.assertDialogIsRendered("Rename", "showRenameDialog");
});

test("It shows the copy dialog", function() {
    this.assertDialogIsRendered("Copy", "showCopyDialog");
});

test("It shows the save-as dialog", function() {
    this.assertDialogIsRendered("SaveAs", "showSaveAsDialog");
});

test("When showing the delete dialog, it triggers 'delete:success' then the dialog is sent successfully", function() {
    this.assertSuccessEvent("Delete", "showDeleteDialog", "delete:success", {name: "filter name"});
});

test("When showing the delete dialog, it triggers 'delete:error' then the dialog is sent unsuccessfully", function() {
    this.assertErrorEvent("Delete", "showDeleteDialog", "delete:error");
});

test("When submitting the delete dialog, it returns the model being deleted", function() {
    var model = this.assertSuccessEvent("Delete", "showDeleteDialog", "delete:success", {name: "filter name"});
    equal(model, this.filterModel);
});

test("When showing the rename dialog, it triggers 'rename:success' then the dialog is sent successfully", function() {
    this.assertSuccessEvent("Rename", "showRenameDialog", "rename:success", {name: "filter name"});
});

test("When showing the rename dialog, it triggers 'rename:error' then the dialog is sent unsuccessfully", function() {
    this.assertErrorEvent("Rename", "showRenameDialog", "rename:error");
});

test("When submitting the rename dialog, it saves the new name in the model", function() {
    var model = this.assertSuccessEvent("Rename", "showRenameDialog", "rename:success", {name: "filter name"});
    equal(model.get("name"), "filter name", "The filter has the new name");
    equal(model, this.filterModel, "The affected model is returned in the event's arguments");
});

test("When showing the copy dialog, it triggers 'copy:success' then the dialog is sent successfully", function() {
    this.assertSuccessEvent("Copy", "showCopyDialog", "copy:success", {id: "2345", subscriptions: {items: []}});
});

test("When showing the copy dialog, it triggers 'copy:error' then the dialog is sent unsuccessfully", function() {
    this.assertErrorEvent("Copy", "showCopyDialog", "copy:error");
});

test("When submitting the save as dialog, it returns the new model in the event arguments", function() {
    var model = this.assertSuccessEvent("Copy", "showCopyDialog", "copy:success", {id: "2345", subscriptions: {items: []}});
    ok(model instanceof JIRA.Components.Filters.Models.Filter, "It includes a FilterModel");
    equal(model.get("id"), "2345", "The new model contains the attributes from the response");
});

test("When showing the save-as dialog, it triggers 'saveas:success' then the dialog is sent successfully", function() {
    this.assertSuccessEvent("SaveAs", "showSaveAsDialog", "saveas:success", {id: "2345", subscriptions: {items: []}});
});

test("When showing the save-as dialog, it triggers 'saveas:error' then the dialog is sent unsuccessfully", function() {
    this.assertErrorEvent("SaveAs", "showSaveAsDialog", "saveas:error");
});

test("When submitting the save-as dialog, it returns the new model in the event arguments", function() {
    var model = this.assertSuccessEvent("SaveAs", "showSaveAsDialog", "saveas:success", {id: "2345", subscriptions: {items: []}});
    ok(model instanceof JIRA.Components.Filters.Models.Filter, "It includes a FilterModel");
    equal(model.get("id"), "2345", "The new model contains the attributes from the response");
});

test("When showing a dialog, it closes the previous one", function() {
    var dialogController = this.buildDialogController();
    this.stub(JIRA.Components.Filters.Views.Dialogs.Rename.prototype, "render");
    this.stub(JIRA.Components.Filters.Views.Dialogs.Delete.prototype, "render");

    dialogController.showRenameDialog();
    var closeStub = this.stub(dialogController.activeView, "close");
    dialogController.showDeleteDialog();

    sinon.assert.calledOnce(closeStub);
});

test("When showing a dialog, it is exposed in the activeView property", function() {
    var dialogController = this.buildDialogController();
    this.stub(JIRA.Components.Filters.Views.Dialogs.Rename.prototype, "render");
    this.spy(JIRA.Components.Filters.Views.Dialogs, "Rename");

    dialogController.showRenameDialog();

    ok(dialogController.activeView === JIRA.Components.Filters.Views.Dialogs.Rename.thisValues[0]);
});
