AJS.namespace("JIRA.Components.Filters.Views.Actions");

/**
 * @class JIRA.Components.Filters.Views.Actions
 *
 * Displays the actions dropdown for a filter
 *
 * @extends JIRA.Marionette.ItemView
 */
JIRA.Components.Filters.Views.Actions = JIRA.Marionette.ItemView.extend({
    template: JIRA.Components.Filters.Templates.Actions.DropDown,

    triggers: {
        "click .rename-filter": "renameFilter",
        "click .copy-filter": "copyFilter",
        "click .unfavourite-filter": "unfavouriteFilter",
        "click .delete-filter": "deleteFilter"
    },

    /**
     * The view that was used as a trigger for the dropdown. Populated when this view is created.
     * @type {JIRA.Marionette.View}
     */
    triggerView: null,

    /**
     * Internal dropdown created with this view is rendered.
     * @type {AJS.Dropdown}
     */
    dropDown: null,

    /**
     * @param {Object} options Options
     * @param {JIRA.Components.Filters.Views.List.FilterWithActions} options.triggerView View that was used to trigger the dropDown
     */
    initialize: function(options) {
        this.triggerView = options.triggerView;
    },

    serializeData: function() {
        return {
            filter: this.model,
            canEdit: JIRA.Issues.LoginUtils.loggedInUserName() === this.model.getOwnerUserName()
        };
    },

    onBeforeRender: function() {
        this.dropDown = new AJS.Dropdown({
            alignment: AJS.LEFT,
            styleClass: "filter-actions-dropdown",
            content: _.bind(function() {
                return this.$el;
            }, this),
            positioningController: new JIRA.Issues.WindowPositioning(),
            trigger: this.triggerView.getActionsTrigger()
        });

        // When the dropdown is hidden, close this view
        this.dropDown.bind("hideLayer", _.bind(this.close, this));
    },

    onRender: function() {
        this.dropDown.show();
        this._markFilterAsSelected();
    },

    onClose: function() {
        this.dropDown.hide();
        this.dropDown.layerController.placeholder().remove();
        this.dropDown = null;
        this._markFilterAsUnselected();
    },

    _markFilterAsSelected: function() {
        var trigger = this.triggerView.getActionsTrigger();
        trigger.find("span").addClass("active");
        trigger.closest("li").addClass("hover");
    },

    _markFilterAsUnselected: function() {
        var trigger = this.triggerView.getActionsTrigger();
        trigger.find("span").removeClass("active");
        trigger.closest("li").removeClass("hover");
    }
});
