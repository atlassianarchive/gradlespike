AJS.namespace("JIRA.Components.Filters.Views.List.Message");

/**
 * @class JIRA.Components.Filters.Views.List.Message
 *
 * Displays a message
 *
 * @extends JIRA.Marionette.ItemView
 */
JIRA.Components.Filters.Views.List.Message = JIRA.Marionette.ItemView.extend({
    template: JIRA.Components.Filters.Templates.List.Message,

    /**
     * Text to display in the message
     * @type {string}
     */
    text: "",

    /**
     * @param {Object} options Options
     * @param {string} options.text Text to display in the message.
     */
    initialize: function(options) {
        this.text = options.text;
    },

    onRender: function() {
        this.unwrapTemplate();
    },

    templateHelpers: function() {
        return {
            text: this.text,
            className: this.className
        };
    }
});