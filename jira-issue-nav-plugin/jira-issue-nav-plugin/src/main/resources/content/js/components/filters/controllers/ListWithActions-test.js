AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:filters-component-test");

module('JIRA.Components.Filters.Controllers.ListWithActions ', {
    mockListView: function() {
        var instance = {
            render: this.spy(),
            unhighlightAllFilters: this.spy(),
            highlightFilter: this.spy()
        };
        _.extend(instance, Backbone.Events);
        this.stub(JIRA.Components.Filters.Views.List, "ListWithActions").returns(instance);
        return instance;
    },

    mockActionsView: function(model) {
        var instance = {
            render: this.spy(),
            close: this.spy(),
            model: model
        };
        _.extend(instance, Backbone.Events);
        var constructor = this.stub(JIRA.Components.Filters.Views, "Actions").returns(instance);
        return {
            constructor: constructor,
            instance: instance
        };
    },

    createAndOpenController: function() {
        var collection = new JIRA.Components.Filters.Collections.Filters(_.toArray(arguments), {fetchState: "fetched"});
        var controller = new JIRA.Components.Filters.Controllers.ListWithActions({
            collection: collection
        });

        controller.show();

        return controller;
    },

    createModel: function(id, name) {
        return new JIRA.Components.Filters.Models.Filter({id: id || "1234", name: name || "My filter"});
    },

    assertEvent: function(eventName) {
        var model = this.createModel();
        var listView = this.mockListView();
        var actionsView = this.mockActionsView(model);
        var controller = this.createAndOpenController(model);
        var eventSpy = this.spy();

        controller.on(eventName, eventSpy);
        listView.trigger("itemview:openActions", null, {view: null, model: model});
        actionsView.instance.trigger(eventName, {model: model});

        sinon.assert.calledOnce(actionsView.instance.close);
        sinon.assert.calledOnce(eventSpy);
        sinon.assert.calledWith(eventSpy, model);
    }
});

test("When clicking on a actions trigger, it opens the actions view", function() {
    var model = this.createModel();
    var listView = this.mockListView();
    var actionsView = this.mockActionsView();
    this.createAndOpenController(model);

    listView.trigger("itemview:openActions", null, {view: null, model: model});

    ok(actionsView.constructor.calledWithNew(), "A new actions view is created");
    equal(actionsView.constructor.firstCall.args[0].model, model, "The model is passed to the actions view");
    sinon.assert.calledOnce(actionsView.instance.render, "Actions view is opened");

});

test("When clicking on the trigger twice, it closes the actions view", function() {
    var model = this.createModel();
    var listView = this.mockListView();
    var actionsView = this.mockActionsView(model);
    this.createAndOpenController(model);

    listView.trigger("itemview:openActions", null, {view: null, model: model});
    sinon.assert.calledOnce(actionsView.constructor, "One view is created");
    sinon.assert.calledOnce(actionsView.instance.render, "The view is rendered");
    actionsView.constructor.reset();

    listView.trigger("itemview:openActions", null, {view: null, model: model});
    sinon.assert.notCalled(actionsView.constructor, "The view is not created again");
    sinon.assert.calledOnce(actionsView.instance.close, "The view is closed");
});

test("When clicking on the trigger for another filter, it opens the new actions view", function() {
    var model1 = this.createModel("1", "My first filter");
    var model2 = this.createModel("2", "My second filter");
    var listView = this.mockListView();
    var actionsView = this.mockActionsView(model1);
    this.createAndOpenController(model1, model2);

    listView.trigger("itemview:openActions", null, {view: null, model: model1});
    listView.trigger("itemview:openActions", null, {view: null, model: model2});

    sinon.assert.calledTwice(actionsView.constructor, "Two views are created");
    sinon.assert.calledOnce(actionsView.instance.close, "The view is closed");
    sinon.assert.calledTwice(actionsView.instance.render, "The view is rendered");
});

test("When closing the view, it closes the actions view too", function() {
    var model = this.createModel();
    var listView = this.mockListView();
    var actionsView = this.mockActionsView(model);
    var controller = this.createAndOpenController(model);

    listView.trigger("itemview:openActions", null, {view: null, model: model});
    controller.close();

    sinon.assert.calledOnce(actionsView.instance.close);
});

test("When selecting a filter, it closes the actions view", function() {
    var model = this.createModel();
    var listView = this.mockListView();
    var actionsView = this.mockActionsView(model);
    this.createAndOpenController(model);

    listView.trigger("itemview:openActions", null, {view: null, model: model});
    listView.trigger("itemview:selectFilter", null, {view: null, model: model});

    sinon.assert.calledOnce(actionsView.instance.close);
});

test("When highlighting a filter, it closes the actions view", function() {
    var model = this.createModel();
    var listView = this.mockListView();
    var actionsView = this.mockActionsView(model);
    var controller = this.createAndOpenController(model);

    listView.trigger("itemview:openActions", null, {view: null, model: model});
    controller.highlightFilter(model);

    sinon.assert.calledOnce(actionsView.instance.close);
});

test("It rethrows the deleteFilter event from the actions view, and closes it", function() {
    this.assertEvent("deleteFilter");
});

test("It rethrows the renameFilter event from the actions view, and closes it", function() {
    this.assertEvent("renameFilter");
});

test("It rethrows the copyFilter event from the actions view, and closes it", function() {
    this.assertEvent("copyFilter");
});

test("It rethrows the unfavouriteFilter event from the actions view, and closes it", function() {
    this.assertEvent("unfavouriteFilter");
});
