AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuetable-component-test");

(function ($, IssueTable) {
    "use strict";

    module("JIRA.Components.IssueTable.Views.IssueTable", {
        HTML: [
            "<table id='issuetable'>",
                "<thead>",
                    "<tr class='rowHeader'><th class='sortable' onclick='alert(1)' data-id='key'>Key</th></tr>",
                    "<tr class='rowHeader'><th class='sortable' onclick='alert(2)' data-id='summary'>Summary</th></tr>",
                "</thead>",
                "<tbody>",
                    "<tr id='issuerow10000' class='focused issuerow' rel='10000'><td>JRA-1</td>     <td>My issue</td></tr>",
                    "<tr id='issuerow10010' class='issuerow' rel='10010'>       <td>JRA-2</td><td>My other issue</td></tr>",
                "</tbody>",
            "</table>"
        ].join(""),

        renderView: function(extraOptions) {
            var view = new IssueTable(_.extend({
                el: $("<div/>").appendTo("#qunit-fixture"),
                resultsTable: this.HTML
            }, extraOptions));
            view.render();
            return view;
        }
    });

    test("Clicking a row throws the 'highlightIssue' event", function () {
        var onHighlightIssue = this.spy();

        var view = this.renderView();
        view.on("highlightIssue", onHighlightIssue);
        view.$("tr:last-child").click();

        sinon.assert.calledWithExactly(onHighlightIssue, 10010);
    });

    test("highlightIssue highlights the specified issue", function () {
        var view = this.renderView();

        view.highlightIssue(10010);

        ok(view.$("tr:last-child").hasClass("focused"), "The correct row is highlighted");
        equal(view.$("tr.focused").length, 1, "Only one row is highlighted");
    });

    test("Clicking on a sortable column header triggers the 'sort' event with the column's data-id", function() {
        var spy = this.spy();
        var view = this.renderView();
        view.on("sort", spy);

        view.$(".sortable").eq(0).click();

        sinon.assert.calledOnce(spy);
        sinon.assert.calledWith(spy, 'key');
    });

    test("Rendering the table triggers the before:render event", function() {
        var spy = this.spy();
        var view = this.renderView();
        view.on("before:render", spy);

        view.render();

        sinon.assert.calledOnce(spy);
    });

    test("Rendering the table triggers the render event", function() {
        var spy = this.spy();
        var view = this.renderView();
        view.on("render", spy);

        view.render();

        sinon.assert.calledOnce(spy);
    });

    test("Rendering the table removes the onclick property from the headers", function() {
        var view = this.renderView();

        equal(view.$("th")[0].onclick, null);
    });

    test("When rendering the table, it marks the sorted column as active", function() {
        var view = this.renderView({
            sortOptions: {
                fieldId: 'key'
            }
        });

        ok(view.$("th[data-id=key]").hasClass("active"));
    });

    test("When rendering the table, it marks the sorted column as descending", function() {
        var view = this.renderView({
            sortOptions: {
                fieldId: 'key',
                order: 'DESC'
            }
        });

        ok(view.$("th[data-id=key]").hasClass("descending"));
    });

    test("When rendering the table, it marks the sorted column as ascending", function() {
        var view = this.renderView({
            sortOptions: {
                fieldId: 'key',
                order: 'ASC'
            }
        });

        ok(view.$("th[data-id=key]").hasClass("ascending"));
    });

    test("When rendering the table, it adds the colHeaderLink class to all non-sorted columns", function() {
        var view = this.renderView({
            sortOptions: {
                fieldId: 'key',
                order: 'ASC'
            }
        });

        ok(!view.$("th[data-id=key]").hasClass("colHeaderLink"));
        ok(view.$("th[data-id=summary]").hasClass("colHeaderLink"));
    });

    test("When rendering the table, it adds the draggable functionality to the table", function() {
        this.spy(jQuery.fn, "dragtable");
        this.renderView();

        // Warning: these asserts seems dodgy, but this is how jQuery actually works: passing
        // an object to the fn.<pluginName>() method causes the initialization of that plugin.
        // As the dragtable plugin does not provide an API to check if it has been activated,
        // this is the only way we have to test it.
        sinon.assert.calledOnce(jQuery.fn.dragtable);
        ok(typeof jQuery.fn.dragtable.firstCall.args[0] === "object");
    });

    test("When rendering the table, it removes the text nodes from the results table", function() {
        var view = this.renderView();

        ok(view.$("table, tbody, thead, tr").contents().filter(function () {
            return this.nodeType === Node.TEXT_NODE;
        }).length===0, "Text nodes are gone");
    });

    test("When closing the table, it removes the dragtable functionality", function() {
        var view = this.renderView();
        this.spy(jQuery.fn, "dragtable");

        view.close();

        sinon.assert.calledOnce(jQuery.fn.dragtable);
        sinon.assert.calledWith(jQuery.fn.dragtable, "destroy");
    });


    test("When reordering the columns, it throws a 'columnsChanged' event", function() {
        var spy = this.spy();
        var clock = this.sandbox.useFakeTimers();
        var view = this.renderView();
        view.on("columnsChanged", spy);

        view.$("th[data-id=key]").simulate("delayed-drag", {
            delay: 300
        });
        clock.tick(350);

        sinon.assert.calledOnce(spy);
    });

    test("When clicking in the columns, the user needs to click for more than 250 ms to start the drag behaviour", function() {
        var clock = this.sandbox.useFakeTimers();
        var view = this.renderView();

        view.$("th[data-id=key]").simulate("mousedown");
        clock.tick(200);
        equal(view.$(".dragtable-sortable").length,  0, "The dragable helper is not rendered");
        clock.tick(100);
        equal(view.$(".dragtable-sortable").length,  1, "The dragable helper is rendered");
    });

    test("It marks an issue as inaccessible", function() {
        var view = this.renderView();

        view.markIssueAsInaccessible(10010);

        var inaccessibleRow = view.$("tr[rel=10010]");
        ok(inaccessibleRow.hasClass("inaccessible-issue"), "The row is inaccessible");
        equal(inaccessibleRow.find("td").length, 1, "Table cells collapsed into one cell");
    });

    test("It updates an issue with the new data", function() {
        var view = this.renderView();

        view.updateIssue(10010, [
            "<table id='issuetable'>",
                "<tbody>",
                    "<tr id='issuerow10000' class='focused issuerow' rel='10000'><td>JRA-1</td><td>MY ISSUE</td></tr>",
                    "<tr id='issuerow10010' class='issuerow' rel='10010'><td>JRA-2</td><td>MY OTHER ISSUE</td></tr>",
                "</tbody>",
            "</table>"
        ].join(""));

        var updatedRow = view.$("tr[rel=10010]");
        equal(updatedRow.find("td").eq(1).text(), "MY OTHER ISSUE");
    });

    test("When an issue is updated, it throws the 'issueRowUpdated' event", function() {
        var view = this.renderView();
        var spy = this.spy();

        view.on("issueRowUpdated", spy)
        view.updateIssue(10010, [
            "<table id='issuetable'>",
                "<tbody>",
                    "<tr id='issuerow10000' class='focused issuerow' rel='10000'><td>JRA-1</td><td>MY ISSUE</td></tr>",
                    "<tr id='issuerow10010' class='issuerow' rel='10010'><td>JRA-2</td><td>MY OTHER ISSUE</td></tr>",
                "</tbody>",
            "</table>"
        ].join(""));

        sinon.assert.calledOnce(spy);
    });

    test("When an issue is updated, it highlights the issue", function() {
        var view = this.renderView();
        this.spy(view, "highlightIssue");

        view.updateIssue(10010, [
            "<table id='issuetable'>",
                "<tbody>",
                    "<tr id='issuerow10000' class='focused issuerow' rel='10000'><td>JRA-1</td><td>MY ISSUE</td></tr>",
                    "<tr id='issuerow10010' class='issuerow' rel='10010'><td>JRA-2</td><td>MY OTHER ISSUE</td></tr>",
                "</tbody>",
            "</table>"
        ].join(""));

        sinon.assert.calledOnce(view.highlightIssue);
        sinon.assert.calledWithExactly(view.highlightIssue, 10010);
    });

}(AJS.$, JIRA.Components.IssueTable.Views.IssueTable));