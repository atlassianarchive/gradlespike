AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuetable-component-test");

module("JIRA.Components.IssueNavigator.Views.EndOfStableMessage", {
    setup: function () {
        this.searchResults = JIRA.Issues.TestUtils.mockSearchResults();

    }
});

test("render() displays message on last page", function() {
    var messageView = new JIRA.Components.IssueNavigator.Views.EndOfStableMessage({
        startIndex: 1,
        pageSize: 10,
        total: 50,
        displayableTotal: 20
    });
    messageView.render();

    ok(messageView.$el.html() !== "", "Message is rendered");
});

test("render() does not display message on non-last page", function() {
    var messageView = new JIRA.Components.IssueNavigator.Views.EndOfStableMessage({
        startIndex: 0,
        pageSize: 5,
        total: 20,
        displayableTotal: 20
    });
    messageView.render();

    equal(messageView.$el.html(), "", "Message is not rendered");
});

test("render() does not display message on last page when there are no more results", function() {
    var messageView = new JIRA.Components.IssueNavigator.Views.EndOfStableMessage({
        startIndex: 0,
        pageSize: 10,
        total: 10,
        displayableTotal: 10
    });
    messageView.render();

    equal(messageView.$el.html(), "", "Message is not rendered");
});