AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:filters-component-test");

module('JIRA.Components.Filters.Views.Dialogs.Copy', {
    setup: function() {
        this.server = sinon.fakeServer.create();
    },

    teardown: function() {
        this.server.restore();
    },

    renderDialog: function(dialog, scope){
        dialog.render();
        JIRA.Issues.Components.TestUtils.moveDialogToQunitFixture(dialog);

        // Respond the defaultShareScope request
        _.last(this.server.requests).respond(200, { "Content-Type": "application/json" }, JSON.stringify({scope: scope || "PRIVATE" }));
    },

    submitDialog: function(dialog, options){
        var dialogForm = dialog.dialog.$form;
        options = options || {};
        if (options.name) {
            dialogForm.find("#filterName").val(options.name);
        }
        if (options.changeToPrivate) {
            dialogForm.find("#setSharingPrivate").prop('checked', true);
        }
        dialogForm.submit();

        _.last(this.server.requests).respond(options.responseCode || 200);
    }
});

test("It should check if the default sharing for the user is GLOBAL", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.Copy({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter"
        })
    });
    this.renderDialog(dialog);

    var lastRequest = _.last(this.server.requests);
    ok(lastRequest.url.match("/rest/api/2/filter/defaultShareScope"), "Request to the right REST endpoint");
    equal(lastRequest.method, "GET", "Request uses GET method");
});

test("It should display a switch-to-private checkbox If the default sharing for the user is GLOBAL", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.Copy({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter"
        })
    });
    this.renderDialog(dialog, "GLOBAL");

    ok(dialog.dialog.$form.find("#setSharingPrivate:checkbox"), "The form contains a checkbox for sharingPrivate");
    equal(dialog.dialog.$form.find("label[for=setSharingPrivate]").text(), "issuenav.filters.sharing.default.changetoprivate",
        "The form contains a text about changing the default share scope to private");
});

test("It should not display a switch-to-private checkbox If the default sharing for the user is PRIVATE", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.Copy({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter"
        })
    });
    this.renderDialog(dialog, "PRIVATE");

    equal(dialog.dialog.$form.find("#setSharingPrivate:checkbox").length, 0, "The form does not contain a checkbox for sharingPrivate");
});

test("It should pass the name when submitting the form", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.Copy({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter"
        })
    });
    this.renderDialog(dialog);
    this.submitDialog(dialog, {
        name: "This filter is a copy"
    });

    equal(JSON.parse(_.last(this.server.requests).requestBody).name, "This filter is a copy");
});

test("It should pass the original filter id when submitting the form", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.Copy({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter",
            id: "1234"
        })
    });
    this.renderDialog(dialog);
    this.submitDialog(dialog);

    equal(JSON.parse(_.last(this.server.requests).requestBody).id, "1234");
});

test("It should copy the filter as favourite when submitting the form", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.Copy({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter",
            id: "1234"
        })
    });
    this.renderDialog(dialog);
    this.submitDialog(dialog);

    equal(JSON.parse(_.last(this.server.requests).requestBody).favourite, true);
});

test("It should display the 'Copy' header when rendering the form", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.Copy({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter",
            id: "1234"
        })
    });
    this.renderDialog(dialog);

    equal(dialog.dialog.$popup.find("h2").text(), "issue.nav.filters.copyialog.title : test filter");
});

test("It should prefill the filters name with 'Copy of ...'", function() {
    this.stub(AJS, "format");
    var dialog = new JIRA.Components.Filters.Views.Dialogs.Copy({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter",
            id: "1234"
        })
    });
    this.renderDialog(dialog);

    sinon.assert.calledWith(AJS.format, "common.words.copyof", "test filter");
});

test("It should display a message when copying a filter from another user", function() {
    this.stub(AJS.Meta, "get").withArgs("remote-user").returns("myUser");
    this.stub(AJS, "format");

    var dialog = new JIRA.Components.Filters.Views.Dialogs.Copy({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter",
            id: "1234",
            ownerUserName: 'ownerUser',
            ownerDisplayName: "Mr. filter's owner"
        })
    });
    this.renderDialog(dialog);

    sinon.assert.calledWith(AJS.format, "issue.nav.filters.savedialog.desc.otherowner", "Mr. filter's owner");
});

test("It should change user's default sharing to private when the switch-to-private checkbox is marked", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.Copy({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter",
            id: "1234"
        })
    });
    this.renderDialog(dialog, "GLOBAL");
    this.submitDialog(dialog, {
        changeToPrivate: true
    });

    var lastRequest = _.last(this.server.requests);
    deepEqual(JSON.parse(lastRequest.requestBody), {scope: "PRIVATE"}, "Request payload is valid");
    ok(lastRequest.url.match("/rest/api/2/filter/defaultShareScope"), "Request to the right REST endpoint");
    equal(lastRequest.method, "PUT", "Request uses PUT method");
});

test("It should focus the name field when there is an error submitting the form", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.Copy({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter",
            id: "1234"
        })
    });
    this.renderDialog(dialog);

    this.submitDialog(dialog, {
        responseCode: 500
    });

    ok(document.activeElement === dialog.dialog.$popup.find("#filterName")[0]);
});
