AJS.namespace("JIRA.Components.IssueTable.Controllers.IssueTable");

/**
 * @class JIRA.Components.IssueTable.Controllers.IssueTable
 *
 * Controller for the IssueTable
 *
 * @extends JIRA.Marionette.Controller
 */
JIRA.Components.IssueTable.Controllers.IssueTable = JIRA.Marionette.Controller.extend({
    /**
     * @event columnsChanged
     * When the order of the columns has changed
     */

    /**
     * @event goToPage
     * When the user wants to load another page by clicking on the pager
     */

    /**
     * @event highlightIssue
     * When an issue has been highlighted.
     */

    /**
     * @event issueRowUpdated
     * When a issue in the table has been updated with new information
     */

    /**
     * @event refresh
     * When the user wants to refresh the search by clicking on the 'refresh' icon
     */

    /**
     * @event renderTable
     * When the table with results and the associated internal views have been rendered
     */

    /**
     * @event sort
     * When the results has been sorted by clicking on a column's header
     */

    /**
     * @param {Object} options Initialization options
     * @param {Element} options.el Element where the table should be rendered
     */
    initialize: function(options) {
        this.$el = jQuery(options.el);
    },

    /**
     * @returns {JIRA.Components.IssueTable.Views.IssueTableLayout}
     */
    _createMainView: function() {
        return new JIRA.Components.IssueTable.Views.IssueTableLayout();
    },

    /**
     * @param {jQuery} resultsTable Server-side rendered table with the results
     * @param {Object} sortOptions State of the current sort options
     * @param {string} sortOptions.fieldId ID of the field used for sorting the results
     * @param {string} sortOptions.order Direction used for the sorting ("DESC", "ASC")
     * @returns {JIRA.Components.IssueTable.Views.IssueTable}
     */
    _createTableView: function(resultsTable, sortOptions) {
        var table = new JIRA.Components.IssueTable.Views.IssueTable({
            resultsTable: resultsTable,
            sortOptions: sortOptions
        });
        this.listenAndRethrow(table, "columnsChanged");
        this.listenAndRethrow(table, "highlightIssue");
        this.listenAndRethrow(table, "issueRowUpdated");
        this.listenAndRethrow(table, "sort");
        return table;
    },

    /**
     * @param {object} data Data to use in this view
     * @param {number} data.total Number of issues in this search
     * @param {number} data.startIndex Index of first issue displayed in the table
     * @param {number} data.pageSize Size of each page
     * @returns {JIRA.Components.IssueNavigator.Views.ResultsCount}
     * @private
     */
    _createResultsCount: function(data) {
        return new JIRA.Components.IssueNavigator.Views.ResultsCount(data);
    },

    /**
     * @param {object} data Data to use in this view
     * @param {number} data.total Number of issues in this search
     * @param {number} data.startIndex Index of first issue displayed in the table
     * @param {number} data.pageSize Size of each page
     * @param {string} data.currentSearch JQL that produced this search results
     * @returns {JIRA.Components.IssueNavigator.Views.Pagination}
     * @private
     */
    _createPagination: function(data) {
        var pagination = new JIRA.Components.IssueNavigator.Views.Pagination(data);
        this.listenAndRethrow(pagination, "goToPage");
        return pagination;
    },

    /**
     * @returns {JIRA.Components.IssueNavigator.Views.RefreshResults}
     */
    _createRefreshResults: function() {
        var refreshResults = new JIRA.Components.IssueNavigator.Views.RefreshResults();
        this.listenAndRethrow(refreshResults, "refresh");
        return refreshResults;
    },

    /**
     * @param {object} data Data to use in this view
     * @param {number} data.total Number of issues in this search
     * @param {number} data.displayableTotal Number of issues than can be displayed in a stable search
     * @param {number} data.pageNumber Number of the current page
     * @param {number} data.numberOfPages Total number of pages in the search results
     * @returns {JIRA.Components.IssueNavigator.Views.EndOfStableMessage}
     */
    _createEndOfStableMessage: function(data) {
        return new JIRA.Components.IssueNavigator.Views.EndOfStableMessage(data);
    },

    /**
     * @param {object} data Data to use in this view
     * @param {boolean} data.jiraHasIssues Whether there are issues created in this JIRA instance
     * @returns {JIRA.Components.IssueTable.Views.EmptyResults}
     */
    _createEmptyResultsView: function(data) {
        return new JIRA.Components.IssueTable.Views.EmptyResults(data);
    },

    _layoutIsRendered: function() {
        return this.view && this.view instanceof JIRA.Components.IssueTable.Views.IssueTableLayout;
    },

    close: function() {
        if (this.view) {
            this.view.close();
            delete this.view;
        }
    },

    /**
     * @param {object} options Options
     * @param {jQuery} options.table Server-side rendered table with the results
     * @param {Object} options.sortOptions State of the current sort options
     * @param {string} options.sortOptions.fieldId ID of the field used for sorting the results
     * @param {string} options.sortOptions.order Direction used for the sorting ("DESC", "ASC")
     * @param {number} options.totalIssues Number of issues in this search
     * @param {number} options.displayableTotal Number of issues than can be displayed in a stable search
     * @param {number} options.pageNumber Number of the current page
     * @param {number} options.numberOfPages Total number of pages in the search results
     * @param {number} options.startIndex Index of first issue displayed in the table
     * @param {number} options.pageSize Size of each page
     * @param {string} options.currentSearch JQL that produced this search results
     * @param {boolean} options.jiraHasIssues Whether there are issues created in this JIRA instance
     * @param {boolean} options.hasIssues Whether there search result has issues
     */
    show: function(options) {
        if (options.hasIssues) {
            this._showTable(options);
        } else {
            this._showEmptyResults(options);
        }
    },

    /**
     * @param {object} options Options
     * @param {boolean} options.jiraHasIssues Whether there are issues created in this JIRA instance
     */
    showErrorMessage: function(options) {
        this._showEmptyResults(options);
    },

    /**
     * Puts the view in the pending state (i.e. dims the table). This method also temporarily disables
     * the column reordering feature.
     */
    showPending: function() {
        if (this.view) {
            this.view.showPending();
            if (this._layoutIsRendered()) {
                this.view.table.currentView._removeDraggable();
            }
        }
    },

    /**
     * @param {object} options Options
     * @param {jQuery} options.table Server-side rendered table with the results
     * @param {Object} options.sortOptions State of the current sort options
     * @param {string} options.sortOptions.fieldId ID of the field used for sorting the results
     * @param {string} options.sortOptions.order Direction used for the sorting ("DESC", "ASC")
     * @param {number} options.totalIssues Number of issues in this search
     * @param {number} options.totalDisplayableIssues Number of issues than can be displayed in a stable search
     * @param {number} options.pageNumber Number of the current page
     * @param {number} options.numberOfPages Total number of pages in the search results
     * @param {number} options.startIndex Index of first issue displayed in the table
     * @param {number} options.pageSize Size of each page
     * @param {string} options.currentSearch JQL that produced this search results
     */
    _showTable: function(options) {
        options = options || {};

        this.close();
        this.view = this._createMainView();
        var table = this._createTableView(options.table, options.sortOptions);
        var resultsCount = this._createResultsCount({
            total: options.totalIssues,
            startIndex: options.startIndex,
            pageSize: options.pageSize
        });
        var pagination = this._createPagination({
            startIndex: options.startIndex,
            pageSize: options.pageSize,
            total: options.totalIssues,
            currentSearch: options.currentSearch
        });
        var refreshResults = this._createRefreshResults();
        var endOfStableMessage = this._createEndOfStableMessage({
            total: options.totalIssues,
            displayableTotal: options.totalDisplayableIssues,
            pageNumber: options.pageNumber,
            numberOfPages: options.numberOfPages
        });

        this.listenTo(this.view, {
            "render": function() {
                this.view.table.show(table);
                this.view.resultsCount.show(resultsCount);
                this.view.pagination.show(pagination);
                this.view.refreshResults.show(refreshResults);
                this.view.endOfStableMessage.show(endOfStableMessage);
            }
        });

        this.view.render();
        this.$el.empty().append(this.view.$el);
        this.trigger("renderTable", this.$el);
    },

    /**
     * @param {object} options Options
     * @param {boolean} options.jiraHasIssues Whether there are issues created in this JIRA instance
     */
    _showEmptyResults: function(options) {
        this.close();
        this.view = this._createEmptyResultsView(options);
        this.view.render();
        this.$el.empty().append(this.view.$el);
        this.trigger("renderEmpty", this.$el);
    },

    /**
     * Highlight an issue in the table.
     *
     * @param {number} issueId The ID of the issue to highlight.
     * @param {boolean} [focus=true] Whether the highlighted issue should have the focus
     */
    highlightIssue: function (issueId, focus) {
        if (this._layoutIsRendered()) {
            this.view.table.currentView.highlightIssue(issueId, focus);
        }
    },

    /**
     * Marks an issue as inaccessible
     *
     * @param {number} issueId ID of the issue to mark
     */
    markIssueAsInaccessible: function(issueId) {
        if (this._layoutIsRendered()) {
            this.view.table.currentView.markIssueAsInaccessible(issueId);
        }
    },

    /**
     * Updates an issue in the table with a new row
     *
     * @param {number} issueId ID of the issue to update
     * @param {jQuery} newTable Server rendered table with the new data
     */
    updateIssue: function(issueId, newTable) {
        if (this._layoutIsRendered()) {
            this.view.table.currentView.updateIssue(issueId, newTable);
        }
    }
});
