AJS.namespace("JIRA.Components.Filters.Views.Dialogs.Copy");

/**
 * @class JIRA.Components.Filters.Views.Dialogs.Copy
 *
 * @extends JIRA.Marionette.DialogView
 */
JIRA.Components.Filters.Views.Dialogs.Copy = JIRA.Marionette.DialogView.extend({
    template: JIRA.Components.Filters.Templates.Dialogs.Save,

    id: "copy-filter-dialog",

    /**
     * Generate the options needed to display the form, in the format expected by JIRA.FormDialog.
     *
     * @returns {Object}
     */
    dialogOptions: function() {
        var instance = this;
        return {
            content: function(callback) {
                // Make REST call to see if user shares filters by default
                JIRA.SmartAjax.makeRequest({
                    url: AJS.contextPath() + '/rest/api/2/filter/defaultShareScope',
                    contentType: 'application/json'
                }).done(function(data) {
                    callback(
                        instance.template(_.extend({
                            isDefaultShareScopeGlobal: data && data.scope === 'GLOBAL'
                        }, instance.serializeData()))
                    );
                });
            }
        };
    },

    /**
     * Generate the data to be sent in the form request.
     *
     * @param {jQuery} form Form container.
     * @returns {Object} Data to be sent in the request.
     */
    formToRequestData: function(form) {
        return {
            name: form.find('[name=name]').val(),
            id: this.model.getId(),
            favourite: true
        };
    },

    /**
     * Generate the data needed by the template
     *
     * @returns {Object}
     */
    serializeData: function() {
        var model = this.model;
        return {
            headerText: AJS.I18n.getText('issue.nav.filters.copyialog.title') + " : " + model.getName(),
            prefilledFilterName: AJS.I18n.getText('common.words.copyof', model.getName()),
            modifierKey: AJS.Navigator.modifierKey(),
            contextPath: AJS.contextPath(),
            filter: model.toJSON(),
            copyFromAnotherUser: model.getOwnerUserName() !== AJS.Meta.get('remote-user'),
            filterOwnerName: model.getOwnerDisplayName(),
            hiddenFields: []
        };
    },

    /**
     * Method to run when the form has been submitted successfully.
     *
     * Changes the user sharing preference to 'PRIVATE' if needed.
     */
    onSubmitSuccess: function() {
        if (this.$el.find('#setSharingPrivate').prop('checked')) {
            JIRA.SmartAjax.makeRequest({
                url: AJS.contextPath() + '/rest/api/2/filter/defaultShareScope',
                type: 'PUT',
                contentType: 'application/json',
                processData: false,
                data: JSON.stringify({ scope: 'PRIVATE' })
            });
        }
    },

    /**
     * Method to run when the form has been submitted with errors
     */
    onSubmitError: function() {
        this.dialog.$form.find("#filterName").focus();
    }
});