AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:filters-component-test");

module('JIRA.Components.Filters.Views.Dialogs.SaveAs', {
    setup: function() {
        this.server = sinon.fakeServer.create();
    },

    teardown: function() {
        this.server.restore();
    },

    renderDialog: function(dialog, scope){
        dialog.render();
        JIRA.Issues.Components.TestUtils.moveDialogToQunitFixture(dialog);

        // Respond the defaultShareScope request
        _.last(this.server.requests).respond(200, { "Content-Type": "application/json" }, JSON.stringify({scope: scope || "PRIVATE" }));
    },

    submitDialog: function(dialog, options){
        var dialogForm = dialog.dialog.$form;
        options = options || {};
        if (options.name) {
            dialogForm.find("#filterName").val(options.name);
        }
        if (options.changeToPrivate) {
            dialogForm.find("#setSharingPrivate").prop('checked', true);
        }
        dialogForm.submit();

        _.last(this.server.requests).respond(options.responseCode || 200);
    }
});

test("It should check if the default sharing for the user is GLOBAL", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.SaveAs({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter"
        })
    });
    this.renderDialog(dialog);

    var lastRequest = _.last(this.server.requests);
    ok(lastRequest.url.match("/rest/api/2/filter/defaultShareScope"), "Request to the right REST endpoint");
    equal(lastRequest.method, "GET", "Request uses GET method");
});

test("It should display a switch-to-private checkbox If the default sharing for the user is GLOBAL", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.SaveAs({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter"
        })
    });
    this.renderDialog(dialog, "GLOBAL");

    ok(dialog.dialog.$form.find("#setSharingPrivate:checkbox"), "The form contains a checkbox for sharingPrivate");
    equal(dialog.dialog.$form.find("label[for=setSharingPrivate]").text(), "issuenav.filters.sharing.default.changetoprivate",
        "The form contains a text about changing the default share scope to private");
});

test("It should not display a switch-to-private checkbox If the default sharing for the user is PRIVATE", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.SaveAs({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter"
        })
    });
    this.renderDialog(dialog, "PRIVATE");

    equal(dialog.dialog.$form.find("#setSharingPrivate:checkbox").length, 0, "The form does not contain a checkbox for sharingPrivate");
});

test("It should pass the name when submitting the form", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.SaveAs({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter"
        })
    });
    this.renderDialog(dialog);
    this.submitDialog(dialog, {
        name: "This filter is a new filter"
    });

    equal(JSON.parse(_.last(this.server.requests).requestBody).name, "This filter is a new filter");
});

test("It should pass the original filter id when submitting the form", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.SaveAs({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter",
            id: "1234"
        })
    });
    this.renderDialog(dialog);
    this.submitDialog(dialog);

    equal(JSON.parse(_.last(this.server.requests).requestBody).id, "1234");
});

test("It should create the filter as favourite when submitting the form", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.SaveAs({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter",
            id: "1234"
        })
    });
    this.renderDialog(dialog);
    this.submitDialog(dialog);

    equal(JSON.parse(_.last(this.server.requests).requestBody).favourite, true);
});

test("It should display the 'Save As' header when rendering the form", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.SaveAs({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter",
            id: "1234"
        })
    });
    this.renderDialog(dialog);

    equal(dialog.dialog.$popup.find("h2").text(), "issue.nav.filters.savedialog.title");
});

test("It should display a message when saving a filter from another user", function() {
    this.stub(AJS.Meta, "get").withArgs("remote-user").returns("myUser");
    this.stub(AJS, "format");

    var dialog = new JIRA.Components.Filters.Views.Dialogs.SaveAs({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter",
            id: "1234",
            ownerUserName: 'ownerUser',
            ownerDisplayName: "Mr. filter's owner"
        })
    });
    this.renderDialog(dialog);

    sinon.assert.calledWith(AJS.format, "issue.nav.filters.savedialog.desc.otherowner", "Mr. filter's owner");
});

test("It should change user's default sharing to private when the switch-to-private checkbox is marked", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.SaveAs({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter",
            id: "1234"
        })
    });
    this.renderDialog(dialog, "GLOBAL");
    this.submitDialog(dialog, {
        changeToPrivate: true
    });

    var lastRequest = _.last(this.server.requests);
    deepEqual(JSON.parse(lastRequest.requestBody), {scope: "PRIVATE"}, "Request payload is valid");
    ok(lastRequest.url.match("/rest/api/2/filter/defaultShareScope"), "Request to the right REST endpoint");
    equal(lastRequest.method, "PUT", "Request uses PUT method");
});

test("It should focus the name field when there is an error submitting the form", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.SaveAs({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter",
            id: "1234"
        })
    });
    this.renderDialog(dialog);

    this.submitDialog(dialog, {
        responseCode: 500
    });

    ok(document.activeElement === dialog.dialog.$popup.find("#filterName")[0]);
});

test("If includes the filter's data in the request data", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.SaveAs({
        model: new JIRA.Components.Filters.Models.Filter({
            id: "1234",
            name: "my test filter"
        }),
        jql: "proj=TEST"
    });
    this.renderDialog(dialog);

    var requestData = dialog.formToRequestData(dialog.dialog.$form);
    deepEqual(requestData, {
        id: "1234",
        favourite: true,
        name: "",
        jql: "proj=TEST"
    });
});

test("If does not include the filter's id in the request data if there is no model", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.SaveAs({
        jql: "proj=TEST"
    });
    this.renderDialog(dialog);

    var requestData = dialog.formToRequestData(dialog.dialog.$form);
    deepEqual(requestData, {
        favourite: true,
        name: "",
        jql: "proj=TEST"
    });
});

test("If does not include the filter's id in the request data if it is a system filter", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.SaveAs({
        model: new JIRA.Components.Filters.Models.Filter({
            id: "1234",
            isSystem: true
        }),
        jql: "proj=TEST"
    });
    this.renderDialog(dialog);

    var requestData = dialog.formToRequestData(dialog.dialog.$form);
    deepEqual(requestData, {
        favourite: true,
        name: "",
        jql: "proj=TEST"
    });
});