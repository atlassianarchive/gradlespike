AJS.namespace("JIRA.Components.IssueNavigator.Views.ResultsCount");

/**
 * @class JIRA.Components.IssueNavigator.Views.ResultsCount
 *
 * This view renders the count of results in a search (e.g "1-20 of 42")
 *
 * @extends JIRA.Marionette.ItemView
 *
 * @param {object} options Options
 * @param {number} options.total Number of issues in this search
 * @param {number} options.startIndex Index of first issue displayed in the table
 * @param {number} options.pageSize Size of each page
 */
JIRA.Components.IssueNavigator.Views.ResultsCount = JIRA.Marionette.ItemView.extend({
    template: JIRA.Templates.IssueNav.resultsCount,

    serializeData: function() {
        var total = this.options.total;
        var start = this.options.startIndex + 1;
        var end = this.options.startIndex + this.options.pageSize;
        return {
            start: start,
            end: Math.min(end, total),
            total: total
        };
    }
});
