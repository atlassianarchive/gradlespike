AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:filters-component-test");

module("JIRA.Components.Filters.Views.List.Message", {
    createView: function(options) {
        return new JIRA.Components.Filters.Views.List.Message({
            text: options.text,
            className: options.className
        });
    }
});

test("When rendering, it renders the text", function() {
    var view = this.createView({ text: "This is my message" });

    view.render();

    equal(view.$el.text(), "This is my message");
});

test("When rendering, it uses the provided class name", function() {
    var view = this.createView({ className: "my-message" });

    view.render();

    ok(view.$el.hasClass("my-message"));
});

test("When rendering, it unwraps the template", function() {
    var view = this.createView({ className: "my-message" });

    view.render();

    ok(view.$el.is("p"));
});
