AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:filters-component-test");

module('JIRA.Components.Filters.Views.Dialogs.Delete', {
    setup: function() {
        this.server = sinon.fakeServer.create();
    },

    teardown: function() {
        this.server.restore();
    },

    renderDialog: function(dialog){
        dialog.render();
        JIRA.Issues.Components.TestUtils.moveDialogToQunitFixture(dialog);
    },

    submitDialog: function(dialog){
        dialog.dialog.$form.submit();
        _.last(this.server.requests).respond(200);
    }
});

test("It deletes the filter using a REST call", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.Delete({
        model: new JIRA.Components.Filters.Models.Filter({
            subscriptions: {
                items: []
            },
            id: "1234"
        })
    });
    this.renderDialog(dialog);
    this.submitDialog(dialog);

    ok(_.last(this.server.requests).url.match("/rest/api/2/filter/1234"), "Request to the REST endpoint");
    equal(_.last(this.server.requests).method, "DELETE", "Request uses the DELETE method");
});
