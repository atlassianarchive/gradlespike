AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:filters-component-test");

module("JIRA.Components.Filters.Views.List.Module", {
    createView: function(options) {
        options = options || {};
        return new JIRA.Components.Filters.Views.List.Module({
            title: options.title
        });
    }
});

test("When rendering, it renders the provided title", function() {
    var view = this.createView({ title: "My module" });

    view.render();

    equal(view.$el.find("h4").text(), "My module");
});

test("When rendering without a title, it does not render the title markup", function() {
    var view = this.createView();

    view.render();

    equal(view.$el.find("h4").length, 0);
});

test("When rendering, it provides the 'content' region", function() {
    var view = this.createView();

    view.render();

    // We don't have a nice way to test for a region, let's just check it implements a show() method.
    // Other tests will fail (all the WD tests related to filters) if this is broken.
    equal(typeof view.content.show, "function");
});
