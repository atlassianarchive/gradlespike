AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:filters-component-test");

module('JIRA.Components.Filters.Views.Dialogs.Rename', {
    setup: function() {
        this.server = sinon.fakeServer.create();
    },

    teardown: function() {
        this.server.restore();
    },

    renderDialog: function(dialog){
        dialog.render();
        JIRA.Issues.Components.TestUtils.moveDialogToQunitFixture(dialog);
    },

    submitDialog: function(dialog, options){
        var dialogForm = dialog.dialog.$form;
        options = options || {};
        if (options.name) {
            dialogForm.find("#filterName").val(options.name);
        }
        if (options.changeToPrivate) {
            dialogForm.find("#setSharingPrivate").prop('checked', true);
        }
        dialogForm.submit();

        _.last(this.server.requests).respond(options.responseCode || 200);
    }
});

test("It should pass the name when submitting the form", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.Rename({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter"
        })
    });
    this.renderDialog(dialog);
    this.submitDialog(dialog, {
        name: "New name of the filter"
    });

    equal(JSON.parse(_.last(this.server.requests).requestBody).name, "New name of the filter");
});

test("It should pass the original filter id when submitting the form", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.Rename({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter",
            id: "1234"
        })
    });
    this.renderDialog(dialog);
    this.submitDialog(dialog);

    equal(JSON.parse(_.last(this.server.requests).requestBody).id, "1234");
});

test("It should display the 'Rename' header when rendering the form", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.Rename({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter",
            id: "1234"
        })
    });
    this.renderDialog(dialog);

    equal(dialog.dialog.$popup.find("h2").text(), "issue.nav.filters.renamedialog.title: test filter");
});

test("It should prefill the filter's name with the old name", function() {
    this.stub(AJS, "format");
    var dialog = new JIRA.Components.Filters.Views.Dialogs.Rename({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter",
            id: "1234"
        })
    });
    this.renderDialog(dialog);

    equal(dialog.dialog.$popup.find("#filterName").val(), "test filter");
});

test("It should focus the name field when there is an error submitting the form", function() {
    var dialog = new JIRA.Components.Filters.Views.Dialogs.Rename({
        model: new JIRA.Components.Filters.Models.Filter({
            name: "test filter",
            id: "1234"
        })
    });
    this.renderDialog(dialog);

    this.submitDialog(dialog, {
        responseCode: 500
    });

    ok(document.activeElement === dialog.dialog.$popup.find("#filterName")[0]);
});
