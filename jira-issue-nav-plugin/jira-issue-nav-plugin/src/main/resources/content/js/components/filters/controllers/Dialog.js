AJS.namespace("JIRA.Components.Filters.Controllers.Dialog");

/**
 * @class JIRA.Components.Filters.Controllers.Dialog
 *
 * @extends JIRA.Marionette.Controller
 */
JIRA.Components.Filters.Controllers.Dialog = JIRA.Marionette.Controller.extend({
    /**
     * @constructor
     */
    initialize: function() {
        this.activeView = null;
    },

    _closeActiveDialog: function() {
        if (this.activeView) {
            this.activeView.close();
            this.stopListening(this.activeView);
            this.activeView = null;
        }
    },

    /**
     * Shows a dialog .
     *
     * @param {JIRA.Marionette.DialogView} Constructor Class of the dialog.
     * @param {JIRA.Components.Filters.Models.Filter} filterModel Model of the filter related to the dialog (e.g. in the deleteDialog, this is the filter being deleted).
     * @param {Object} [options] Options
     * @param {Object} [options.viewOptions] Options passed to the view's Constructor
     * @param {Function} [options.onSuccess] Handler for the 'submit:success' event
     * @param {Function} [options.onError] Handler for the 'submit:error' event
     *
     * @private
     */
    _showDialog: function(Constructor, filterModel, options) {
        _.defaults(options, {
            viewOptions: {},
            onSuccess: jQuery.noop,
            onError: jQuery.noop
        });

        this._closeActiveDialog();

        this.activeView = new Constructor(_.extend({
            model: filterModel
        }, options.viewOptions));

        this.listenTo(this.activeView, 'submit:success', function(serverResponse) {
            options.onSuccess.call(this, serverResponse, filterModel);
        });
        this.listenTo(this.activeView, "submit:error", function() {
            options.onError.call(this, filterModel);
        });

        this.activeView.render();
    },

    /**
     * @param {JIRA.Components.Filters.Models.Filter} filterModel Model of the filter being deleted.
     */
    showDeleteDialog: function(filterModel) {
        this._showDialog(JIRA.Components.Filters.Views.Dialogs.Delete, filterModel, {
            onSuccess: function(serverResponse, filterModel) {
                this.trigger('delete:success', filterModel);
            },
            onError: function(filterModel) {
                this.trigger('delete:error', filterModel);
            }
        });
    },

    /**
     * @param {JIRA.Components.Filters.Models.Filter} filterModel Model of the filter being renamed.
     */
    showRenameDialog: function(filterModel) {
        this._showDialog(JIRA.Components.Filters.Views.Dialogs.Rename, filterModel, {
            onSuccess: function(serverResponse, filterModel) {
                filterModel.set("name", serverResponse.name);
                this.trigger('rename:success', filterModel);
            },
            onError: function(filterModel) {
                this.trigger('rename:error', filterModel);
            }
        });
    },

    /**
     * @param {JIRA.Components.Filters.Models.Filter} filterModel Model of the filter being copied.
     */
    showCopyDialog: function(filterModel) {
        this._showDialog(JIRA.Components.Filters.Views.Dialogs.Copy, filterModel, {
            onSuccess: function(serverResponse) {
                var newModel = new JIRA.Components.Filters.Models.Filter(serverResponse, {parse: true});
                this.trigger('copy:success', newModel);
            },
            onError: function(filterModel) {
                this.trigger('copy:error', filterModel);
            }
        });
    },

    /**
     * @param {JIRA.Components.Filters.Models.Filter} filterModel Model of  filter being saved.
     * @param {string} jql JQL of the filter being saved.
     */
    showSaveAsDialog: function(filterModel, jql) {
        this._showDialog(JIRA.Components.Filters.Views.Dialogs.SaveAs, filterModel, {
            viewOptions: {jql: jql},
            onSuccess: function(serverResponse) {
                var newModel = new JIRA.Components.Filters.Models.Filter(serverResponse, {parse: true});
                this.trigger('saveas:success', newModel);
            },
            onError: function(filterModel) {
                this.trigger('saveas:error', filterModel);
            }
        });
    }
});