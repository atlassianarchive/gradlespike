AJS.namespace("JIRA.Components.Filters.Collections.SystemFilters");

/**
 * @class JIRA.Components.Filters.Collections.SystemFilters
 *
 * A collection of FilterModel objects.
 *
 * @extends JIRA.Components.Filters.Collections.Filters
 */
JIRA.Components.Filters.Collections.SystemFilters = JIRA.Components.Filters.Collections.Filters.extend({

    _setFetchState: function(state) {
        var isNewSate = state !== this.fetchState;

        this.fetchState = state;
        if (isNewSate) {
            this.trigger("change:fetchState", this.fetchState);
        }
    },

    url: AJS.contextPath() + "/rest/issueNav/latest/systemFilters",

    fetch: function() {
        if (this.length > 0) {
            this._setFetchState("fetched");
            return jQuery.Deferred().resolve();
        }

        return JIRA.Components.Filters.Collections.Filters.prototype.fetch.apply(this, _.toArray(arguments));
    }
});
