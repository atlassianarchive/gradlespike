AJS.namespace("JIRA.Components.IssueTable.Views.IssueTable");

/**
 * @class JIRA.Components.IssueTable.Views.IssueTable
 *
 * A table of issues.
 *
 * @extends JIRA.Marionette.ItemView
 */
JIRA.Components.IssueTable.Views.IssueTable = JIRA.Marionette.ItemView.extend({

    /**
     * @event columnsChanged
     * When the order of the columns has changed
     */

    /**
     * @event highlightIssue
     * When the user wants to highlight an issue
     */

    /**
     * @event sort
     * When the user clicks on the column's header
     */

    events : {
        "blur .hidden-link": function(e) {
            jQuery(e.target).attr("tabIndex", -1);
        },
        "click tr.issuerow": function (e) {
            var row = jQuery(e.target).closest(".issuerow");
            var issueId = Number(row.attr("rel"));
            this.trigger("highlightIssue", issueId);
        },
        "click .sortable" : function(e) {
            e.preventDefault();
            this.trigger("sort", jQuery(e.currentTarget).data("id"));
        }
    },

    inaccessibleIssueRowTemplate: JIRA.Templates.IssueNavTable.inaccessibleIssueRow,

    /**
     *
     * @param {object} options Configuration object
     * @param {jQuery} options.resultsTable Server-side rendered table with the results
     * @param {Object} [options.sortOptions] State of the current sort options
     * @param {string} options.sortOptions.fieldId ID of the field used for sorting the results
     * @param {string} options.sortOptions.order Direction used for the sorting ("DESC", "ASC")
     */
    initialize: function(options) {
        this.resultsTable = options.resultsTable;
        this.sortOptions = options.sortOptions || {};
    },

    /**
     * Render the table of issues.
     *
     * @returns {JIRA.Components.IssueTable.Views.IssueTable} <tt>this</tt>
     */
    render: function () {
        this.triggerMethod("before:render", this);
        var table = jQuery(this.resultsTable)[0];
        this.$el.empty().append(table);
        this.triggerMethod("render", this);
        return this;
    },

    onRender: function() {
        this._decorateTableHeaderWithSortOptions();
        this._addDraggable();
        this._removeEmptyTextNodes();
    },

    onClose: function() {
        this._removeDraggable();
    },

    /**
     * Adds header decorations for sorted column.
     */
    _decorateTableHeaderWithSortOptions: function () {
        var $sortableColumns = this.$el.find('.rowHeader .sortable');
        $sortableColumns.each(function () {
            this.onclick = null;
        });

        var sortOptions = this.sortOptions;
        if (sortOptions.fieldId) {

            // Locate the element which should be decorated
            var $sortEl = $sortableColumns.filter(function() {
                return this.getAttribute("data-id") === sortOptions.fieldId;
            });

            // If the sorting element is not already decorated
            if ($sortEl.size() && !$sortEl.hasClass("descending") && !$sortEl.hasClass("ascending")) {

                // Clean any header with sort-related classes
                $sortableColumns
                    .removeClass("descending ascending active")
                    .addClass("colHeaderLink");

                // Decorate the element as sorted
                var direction = sortOptions.order === "DESC" ? "descending" : "ascending";
                $sortEl.removeClass('colHeaderLink').addClass('active ' + direction);
            }
        }
    },

    _addDraggable: function() {
        this.$("#issuetable").dragtable({
            maxMovingRows: 1,
            containment: 'body',
            axis: false,
            revert: false,
            clickDelay: 250,
            tolerance: "intersect",
            dragaccept: ":not(.headerrow-actions)",
            persistState: _.bind(this._saveColumns, this)
        });
    },

    _removeDraggable: function() {
        this.$("#issuetable").dragtable("destroy");
    },

    /**
     * Fix the issue table element so it displays correctly in IE9.
     *
     * Cells can become misaligned in IE9 if the table's markup contains whitespace.
     * Only affects IE9. IE10 and IE11 are okay.
     */
    _removeEmptyTextNodes: function () {
        this.$el.find("table, tbody, thead, tr").contents().filter(function () {
            return this.nodeType === 3; // Node.TEXT_NODE
        }).remove();
    },

    _saveColumns: function() {
        var cols = [];
        this.$("#issuetable").find('th').each(function() {
            var id = jQuery(this).data('id');
            if(_.isNotBlank(id)) {
                cols.push(id);
            }
        });
        this.trigger("columnsChanged", cols);
    },

    /**
     * Highlight an issue and scroll it into view.
     *
     * Triggers a "highlightIssue" event, passing the issue's ID.
     *
     * @param {number} issueId The ID of the issue to highlight.
     * @param {boolean} [focus=true] Whether the highlighted issue should have the focus
     */
    highlightIssue: function (issueId, focus) {
        var newRow = this.$(".issuerow[rel='" + issueId + "']"),
            oldRow = this.$(".issuerow.focused");

        oldRow.removeClass("focused");
        newRow.addClass("focused");

        if (newRow.is(":visible")) {
            // Focus the row so you can tab through its links.
            if (focus !== false) {
                newRow.find(".hidden-link").removeAttr("tabIndex").focus();
            }
            newRow.scrollIntoView({marginBottom: 50});
        }
    },

    /**
     * Gets the row element that represents a particular issue
     *
     * @param {number} issueId ID of the issue to look for
     * @param {jQuery} [container=this.$el] Table that contain the issue's row
     * @returns {jQuery} The row element
     */
    _getIssueRow: function(issueId, container) {
        container = container || this.$el;
        return container.find("#issuerow" + issueId);
    },

    /**
     * Replaces an inaccessible issue in the table with a message.
     *
     * @param {number} issueId ID of the issue to replace
     */
    markIssueAsInaccessible: function(issueId) {
        this._getIssueRow(issueId).replaceWith(this.inaccessibleIssueRowTemplate({
            columnCount: this.$el.find("thead > tr > th").length,
            issueID: issueId
        }));
    },

    /**
     * Updates an issue in the table with a new row and highlights it.
     *
     * @param {number} issueId ID of the issue to update
     * @param {jQuery} newTable Server rendered table with the new data
     */
    updateIssue: function (issueId, newTable) {
        var $newRow = this._getIssueRow(issueId, jQuery(newTable));
        this._getIssueRow(issueId).replaceWith($newRow);

        this.highlightIssue(issueId);
        this.trigger("issueRowUpdated", $newRow);
    }
});
