AJS.namespace("JIRA.Issues.FocusShifter");

JIRA.Issues.FocusShifter.init = function() {
    return JIRA.Shifter.register(function() {
        var suggestions;
        if (JIRA.Issues.Api.issueIsVisible && !JIRA.Issues.Api.issueIsVisible()) {
            suggestions = [];
        } else {
            var fields = JIRA.Issues.Api.getFieldsOnSelectedIssue();
            suggestions = fields ? JIRA.Issues.FocusShifter.getSuggestions(fields) : [];
        }

        return {
            id: 'edit-fields',
            name: AJS.I18n.getText('shifter.group.editfields'),
            context: JIRA.Issue.getIssueKey(),
            weight: 100,
            getSuggestions: function() {
                return jQuery.Deferred().resolve(suggestions);
            },
            onSelection: function(fieldId) {
                JIRA.Issues.Api.editFieldOnSelectedIssue(fieldId);
            }
        };
    });
};

JIRA.Issues.FocusShifter.show = function() {
    // TODO show the shifter with the correct group already selected
    JIRA.Shifter.show();
};

JIRA.Issues.FocusShifter.getSuggestions = function(fields) {
    return _
        .chain(fields.models)
        .filter(JIRA.Components.IssueEditor.Models.Field.IS_EDITABLE)
        .filter(function(field) {
            return field.id !== 'comment'
        })
        .map(function(field) {
            return {
                label: field.getLabel(),
                value: field.id
            };
        })
        .value();
};