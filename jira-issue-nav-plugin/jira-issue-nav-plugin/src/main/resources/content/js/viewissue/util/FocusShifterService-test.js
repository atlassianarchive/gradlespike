AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");

module("JIRA.Issues.FocusShifter", {
    setup: function() {
        this.sandbox = sinon.sandbox.create();
        JIRA.Issues.Application.start();
        this.issueIsVisible = this.sandbox.stub(JIRA.Issues.Api, 'issueIsVisible').returns(true);
        this.getSelectedIssueKey = this.sandbox.stub(JIRA.Issues.Api, 'getSelectedIssueKey');
        this.focusShifterFactory = JIRA.Issues.FocusShifter.init();
    },

    teardown: function() {
        JIRA.Issues.Application.stop();
        this.sandbox.restore();
        JIRA.Shifter.unregister(this.focusShifterFactory);
    }
});

test("getSuggestions()", function() {
    // Fields that aren't editable or present should be filtered out.
    var suggestions = JIRA.Issues.FocusShifter.getSuggestions(new Backbone.Collection([
        JIRA.Issues.TestUtils.mockIssueFieldModel("a", true, true),
        JIRA.Issues.TestUtils.mockIssueFieldModel("b", true, false),
        JIRA.Issues.TestUtils.mockIssueFieldModel("c", false, true),
        JIRA.Issues.TestUtils.mockIssueFieldModel("d", false, false),
        JIRA.Issues.TestUtils.mockIssueFieldModel("e", true, true)
    ]));

    equal(suggestions.length, 3);
    equal(suggestions[0].value, "a");
    equal(suggestions[0].label, "Label for a");
    equal(suggestions[1].value, "b");
    equal(suggestions[1].label, "Label for b");
    equal(suggestions[2].value, "e");
    equal(suggestions[2].label, "Label for e");
});

test("factory", function () {
    var fields = new Backbone.Collection(),
        getSuggestions = this.sandbox.stub(JIRA.Issues.FocusShifter, "getSuggestions");

    this.sandbox.stub(JIRA.Issues.Api, "getFieldsOnSelectedIssue").returns(fields);
    this.focusShifterFactory();
    ok(getSuggestions.calledWith(fields));
});

test("onSelection()", function() {
    var editFieldOnSelectedIssue = this.sandbox.stub(JIRA.Issues.Api, "editFieldOnSelectedIssue");

    this.focusShifterFactory().onSelection('a');
    ok(editFieldOnSelectedIssue.calledOnce, "field edit attempted");
    ok(editFieldOnSelectedIssue.calledWith('a'))
});
