AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:viewissue");

module("JIRA.Issues.FullScreenIssue", {
    setup: function() {
        this.fullScreenIssue = new JIRA.Issues.FullScreenIssue({
            issueContainer: jQuery('<div></div>'),
            searchContainer: jQuery('<div></div>')
        });
        JIRA.Issues.Application.start();
    },

    teardown: function() {
        JIRA.Issues.Application.stop();
    }
});

test("_makeIssueVisible() should trigger a JIRA.Events.NEW_CONTENT_ADDED", function () {
    var triggerSpy = sinon.spy(JIRA, "trigger");

    this.fullScreenIssue._makeIssueVisible();

    equal(triggerSpy.callCount, 1, "One event is triggered");
    equal(triggerSpy.firstCall.args[0], JIRA.Events.NEW_CONTENT_ADDED, "Event is JIRA.Events.NEW_CONTENT_ADDED");
    equal(typeof triggerSpy.firstCall.args[1][1], "string", "Ensure event has a reason argument");
    equal(triggerSpy.firstCall.args[1][1], JIRA.CONTENT_ADDED_REASON.pageLoad, "Event reason is JIRA.CONTENT_ADDED_REASON.pageLoad");

    triggerSpy.restore();
});
