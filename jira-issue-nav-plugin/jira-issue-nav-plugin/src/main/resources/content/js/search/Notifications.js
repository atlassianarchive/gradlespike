JIRA.Issues.showNotification = function () {

    var NOTIFICATIONS = {
        "thanks_issue_updated": AJS.I18n.getText("navigator.results.thanks.updated"),
        "thanks_issue_transitioned": AJS.I18n.getText("navigator.results.thanks.transitioned"),
        "thanks_issue_assigned": AJS.I18n.getText("navigator.results.thanks.assigned"),
        "thanks_issue_commented": AJS.I18n.getText("navigator.results.thanks.commented"),
        "thanks_issue_worklogged": AJS.I18n.getText("navigator.results.thanks.worklogged"),
        "thanks_issue_voted": AJS.I18n.getText("navigator.results.thanks.voted"),
        "thanks_issue_watched": AJS.I18n.getText("navigator.results.thanks.watched"),
        "thanks_issue_moved": AJS.I18n.getText("navigator.results.thanks.moved"),
        "thanks_issue_linked": AJS.I18n.getText("navigator.results.thanks.linked"),
        "thanks_issue_cloned": AJS.I18n.getText("navigator.results.thanks.cloned"),
        "thanks_issue_labelled": AJS.I18n.getText("navigator.results.thanks.labelled"),
        "thanks_issue_deleted": AJS.I18n.getText("navigator.results.thanks.deleted"),
        "thanks_issue_attached": AJS.I18n.getText("navigator.results.thanks.attached")
    };

    return function (key, issueKey) {

        var msgText;

        if (!key) {
            key = "thanks_issue_updated";
        }
       msgText = NOTIFICATIONS[key];

        if (msgText && issueKey) {
            msgText = AJS.format(msgText, issueKey);
        }

        JIRA.Messages.showSuccessMsg(msgText || key, {
            closeable: true
        });
    }

}();