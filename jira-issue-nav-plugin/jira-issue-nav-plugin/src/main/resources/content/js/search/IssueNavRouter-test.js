AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:sinon");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

// Tests integration of IssueNavRouter, SearchPageModule, FilterModule and IssueTableModule:
// 1. Changing URL changes JQL and current filter
// 2. Searching with JQL or filter changes URL
;(function() {

    var mockFilters = [
        { id: "0", name: "Project ABC", jql: "project = abc" },
        { id: "1", name: "Advanced Search Only", jql: "a = 1 OR b = 2" }
    ];

    module('JIRA.Issues.IssueNavRouter', {
        setup: function() {
            this.server = sinon.fakeServer.create();
            sinon.stub(JIRA.Issues.Application, "request").withArgs("issueEditor:canDismissComment").returns(true);

            this.searchPageModule = JIRA.Issues.TestUtils.mockSearchPageModule();
            this.filterModule = new JIRA.Issues.FilterModule({
                searchPageModule: this.searchPageModule,
                systemFilters: {
                    id: -1,
                    isSystem: true,
                    jql: "",
                    name: "pokemon",
                    requiresLogin: false,
                    sharePermissions: []
                }
            });
            this.favouriteFiltersCollection = this.filterModule.filtersComponent.favouriteFiltersCollection = new JIRA.Components.Filters.Collections.FavouriteFilters(mockFilters);

            this.searchPageModule.filterModule = this.filterModule;
            this.searchPageModule.issueSearchManager.search.returns(jQuery.Deferred().resolve({}).promise());
            this.searchPageModule.searchResults = new JIRA.Issues.SearchResults({}, {
                issueSearchManager: this.searchPageModule.issueSearchManager
            });

            this.issueNavRouter = JIRA.Issues.TestUtils.createIssueNavRouter({
                searchPageModule: this.searchPageModule
            });

            this.searchPageModule.registerIssueNavRouter(this.issueNavRouter);
            this.searchPageModule.changeLayout("list-view");


            this.navigateSpy = this.issueNavRouter.navigate;

            // There is a check in issue nav router that only happens when it is in the search context.
            this.searchPageModule.getCurrentLayout().searchIsVisible = sinon.stub().returns(true);
        },

        teardown: function() {
            JIRA.Issues.Application.request.restore();
            this.server.restore();
        },

        verifyUrl: function(url, i) {
            if (!i) {
                i = 0;
            }
            equal(this.navigateSpy.callCount, i + 1, "Navigate was called " + (i + 1) + " times");
            equal(this.navigateSpy.args[i][0], url, "Navigated to correct url");
        },

        testUrlChangeAppliesExpectedState: function(url, expected) {
            this.navigateSpy.reset();
            this.issueNavRouter.fakeLoad(url);

            equal(this.searchPageModule.getJql(), expected.jql, "Jql is correctly set");

            if (expected.startIndex == null) {
                equal(this.searchPageModule.searchResults.getStartIndex(), 0, "Default start index");
            } else {
                equal(this.searchPageModule.searchResults.getStartIndex(), expected.startIndex, "startIndex is correctly set");
            }

            var currentFilter = this.searchPageModule.getFilter();
            if (expected.filter == null) {
                equal(currentFilter, null, "No current filter");
            } else {
                ok(currentFilter, "Current filter is set");
                equal(currentFilter.getId(), expected.filter.id, "Current filter has correct id");
                equal(currentFilter.getName(), expected.filter.name, "Current filter has correct name");
                equal(currentFilter.getJql(), expected.filter.jql, "Current filter has correct jql");
                equal(this.searchPageModule.isDirty(), expected.dirty, "Current filter is" + (expected.dirty ? '' : ' not') + " dirty");
            }
        }
    });

    var testCases = {
        "?jql="                                 : { jql: "" },
        "?jql=project%20%3D%20foo"              : { jql: "project = foo" },
        "?filter=0"                             : { jql: null, filter: mockFilters[0], dirty: false },
        "?filter=1"                             : { jql: null, filter: mockFilters[1], dirty: false },
        "?filter=0&jql=project%20%3D%20foo"     : { jql: "project = foo", filter: mockFilters[0], dirty: true },
        "?filter=1&jql="                        : { jql: "", filter: mockFilters[1], dirty: true },
        '?jql=status%20=%20"In%20Progress"%20AND%20project%20=%20PRO%20AND%20text%20~%20"some"%20AND%20text%20~%20"text"%20ORDER%20BY%20custom'
                                                : { jql: 'status = "In Progress" AND project = PRO AND text ~ "some" AND text ~ "text" ORDER BY custom' },
        "?jql=&startIndex=50"                   : { jql: "", startIndex: 50 },
        "?jql=&startIndex=0"                    : { jql: "", startIndex: 0 },
        "?filter=0&startIndex=100"              : { jql: null, filter: mockFilters[0], dirty: false, startIndex: 100 },
        '?filter=0&jql=status%20=%20"Good"&startIndex=10'
                                                        : { jql: 'status = "Good"', filter: mockFilters[0], dirty: true, startIndex: 10 }
    };

    // Test cases individually
    _.each(testCases, function (expected, URL) {
        test("Set url to " + URL, function () {
            this.testUrlChangeAppliesExpectedState(URL, expected);
        });
    });

    // Test cases one after the other
    test("Test consecutive url changes", function() {
        var url;
        this.issueNavRouter.fakeLoad("?jql");

        url = "?filter=0";
        this.testUrlChangeAppliesExpectedState(url, testCases[url]);

        url = "?jql=project%20%3D%20foo";
        this.testUrlChangeAppliesExpectedState(url, testCases[url]);

        url = "?filter=1";
        this.testUrlChangeAppliesExpectedState(url, testCases[url]);
    });

    // TODO: query with basic + project param, mock out server response using sinon
    test("Changing to invalid jql in advanced mode updates url", function() {
        this.searchPageModule.issueTableSortRequested("blah", 0);
        this.searchPageModule.issueTableSearchSuccess({startIndex: 0});
        this.verifyUrl("issues/?jql=blah");
    });

    test("Changing jql in advanced mode updates url", function() {
        this.searchPageModule.issueTableSortRequested("text~&", 0);
        this.searchPageModule.issueTableSearchSuccess({startIndex: 0});
        this.verifyUrl("issues/?jql=text~%26");
    });

    test("Setting basic search criteria with a current filter updates url", function() {
        this.searchPageModule.resetToFilter(this.favouriteFiltersCollection.get(0).getId());
        this.searchPageModule.issueTableSearchSuccess({startIndex: 0});
        this.verifyUrl('issues/?filter=0');
        this.searchPageModule.issueTableSortRequested("hello", 0);
        this.searchPageModule.issueTableSearchSuccess({startIndex: 0});
        this.verifyUrl('issues/?filter=0&jql=hello', 1);
    });

    test("Setting issue nav router updates router", function() {
        this.searchPageModule.resetToFilter(this.favouriteFiltersCollection.get(0).getId());
        ok(AJS.Meta.get("issuenav-permlink").indexOf('/issues/?filter=0') > 0);
    });

    test("Setting filter to a copy of the current filter updates url", function() {
        this.searchPageModule.resetToFilter(this.favouriteFiltersCollection.get(0).getId());
        // Copy that filter, giving it a new name and ID but same JQL
        var copy = _.extend(_.clone(mockFilters[0]), { id: 10, name: 'Copy' });
        this.favouriteFiltersCollection.add(copy);
        this.searchPageModule.resetToFilter(this.favouriteFiltersCollection.get(10).getId());
        this.verifyUrl('issues/?filter=10', 1);
    });

    test("getBackboneRoot", function () {
        var contextPath = AJS.contextPath(),
            isEnabledStub = sinon.stub(AJS.DarkFeatures, "isEnabled"),
            router = JIRA.Issues.IssueNavRouter;

        isEnabledStub.returns(false);
        equal(contextPath + "/", router.getBackboneRoot({pushState: function () {}}), "Returns / when pushState is supported");
        equal(contextPath + "/i", router.getBackboneRoot({pushState: undefined}), "Returns /i when pushState isn't supported");

        isEnabledStub.returns(true);
        equal(contextPath + "/i", router.getBackboneRoot({pushState: function () {}}), "Returns /i when the ka.I_ROOT dark feature is enabled");
        equal(contextPath + "/i", router.getBackboneRoot({pushState: undefined}), "Returns /i when the ka.I_ROOT dark feature is enabled");

        isEnabledStub.restore();
    });

    test("navigate to login if needed", function () {
        equal(this.issueNavRouter._navigateToLoginIfNeeded({ jql: "" }, { pushState: undefined }), false, "No login needed if no issue key");

        equal(this.issueNavRouter._navigateToLoginIfNeeded({ jql: "", selectedIssueKey: "TEST-1" }, { pushState: function () {} }), false, "No login needed if pushed state supported");

        AJS.Meta.set("remote-user", "user");
        equal(this.issueNavRouter._navigateToLoginIfNeeded({ jql: "", selectedIssueKey: "TEST-1" }, { pushState: undefined }), false, "No login needed if already logged in");

        AJS.Meta.set("remote-user", null);
        this.issueNavRouter._navigateUsingState = sinon.spy();
        this.issueNavRouter._redirectToLogin = sinon.spy();
        var state = { jql: "", selectedIssueKey: "TEST-1" };

        this.server.respondWith("GET", /.*anonymousAccess.*/, [200, {}, '{ "accessible": "true" }']);
        equal(this.issueNavRouter._navigateToLoginIfNeeded(state, { pushState: undefined }), true, "Login needed if not logged in");
        this.server.respond();
        ok(this.issueNavRouter._navigateUsingState.calledWith(state), "Navigate called with the right state");

        this.server = sinon.fakeServer.create();
        this.server.respondWith("GET", /.*anonymousAccess.*/, [401, {}, ""]);
        equal(this.issueNavRouter._navigateToLoginIfNeeded(state, { pushState: undefined }), true, "Login needed if not logged in");
        this.server.respond();
        ok(this.issueNavRouter._redirectToLogin.calledWith(state), "Redirect to login called with the right state");
    });

    test("Dirty comment warning is respected when navigating", function() {
        JIRA.Issues.Application.request.restore();
        var canDismissCommentStub = sinon.stub(JIRA.Issues.Application, "request").withArgs("issueEditor:canDismissComment");
        var mockState = {
            preventDefault: function() { }
        };

        canDismissCommentStub.returns(false);
        this.issueNavRouter._navigateUsingState(mockState);
        equal(this.issueNavRouter.navigate.callCount, 0, "Navigation isn't performed when user doesn't want to dismiss the comment");

        canDismissCommentStub.returns(true);
        this.issueNavRouter._navigateUsingState(mockState);
        equal(this.issueNavRouter.navigate.callCount, 1, "Navigation is performed when user wants to dismiss the comment");
    });

})();
