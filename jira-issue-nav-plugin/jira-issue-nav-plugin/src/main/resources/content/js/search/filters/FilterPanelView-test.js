AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:sinon");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

module('JIRA.Issues.FilterPanelView', {
    setup: function() {
        this.sandbox = sinon.sandbox.create();
        this.sandbox.useFakeServer();
        this.server = this.sandbox.server;

        this.el = AJS.$("<div></div>");
        this.searchPageModule = JIRA.Issues.TestUtils.mockSearchPageModule();

        localStorage.removeItem("issues.sidebar.docked");

        this.filterData = [
            {
                id: 10000,
                name: "Sample Search 1",
                jql: "project = HSP"
            },
            {
                id: 10001,
                name: "Sample Search 2",
                jql: "project = TST"
            },
            {
                id: 10002,
                name: "Sample Search 3",
                jql: "project = HSP and type = bug"
            }
        ];

        this.getShortcutKeysStub = this.sandbox.stub(AJS.KeyboardShortcut, "getKeyboardShortcutKeys").returns("[");
        this.openTemplate = this.sandbox.spy(JIRA.Issues.FilterPanelView.prototype, "template");
        this.closedTemplate = this.sandbox.spy(JIRA.Issues.FilterPanelView.prototype, "collapsedTemplate");
        this.favFiltersUrl = JIRA.Components.Filters.Collections.FavouriteFilters.prototype.url;
        this.sandbox.stub(JIRA.Issues.Application, "request").withArgs("issueEditor:canDismissComment").returns(true);
    },
    createFilterModule: function(systemFilters, skipFavouritesResponse) {
        this.filterModule = new JIRA.Issues.FilterModule({
            systemFilters: systemFilters || [],
            searchPageModule: this.searchPageModule
        });
        if (systemFilters && systemFilters.length) {
            this.filterModule.initSystemFilters();
        }
        // Fetch favourite filters
        if (!skipFavouritesResponse) {
            _.last(this.server.requests).respond(200);
        }
        this.searchPageModule.filterModule = this.filterModule;
        this.filterPanelModel = this.filterModule.filterPanelModel;
        this.filterPanelModel.isDocked = this.sandbox.stub().returns(true);
        this.filterPanelView = this.filterModule.createView({$filterPanelEl:this.el});
    },
    teardown: function() {
        this.server.respond(); // to clear out any uncalled callbacks that may invoke renders
        this.server.restore();
        this.sandbox.restore();
        this.el.remove();
    }
});

test("Render fallback content when no filters exist", function() {
    this.createFilterModule();
    this.filterPanelView.render();

    equal(this.filterPanelView.$(".saved-filter").length, 0, "Filter list <ul> is not rendered");
    ok(this.filterPanelView.$(".filter-info").length > 0, "Search", "Fallback content is rendered");
});

test("Favourite filters shows correct message for logged in user", function() {
    JIRA.Issues.TestUtils.stubGetLoggedInUser("my name is james bond. james bond.");

    this.createFilterModule();
    this.server.respondWith("GET", new RegExp(this.favFiltersUrl), [200, { "Content-Type": "application/json" }, '[]']);
    this.server.respond();
    this.filterPanelView.render();
    ok(this.filterPanelView.$el.text().indexOf("issue.nav.filters.fav.empty") != -1, "Correct message shown.");
});

test("Favourite filters shows correct message for anon user", function() {
    JIRA.Issues.TestUtils.stubGetLoggedInUser("");
    this.stub(AJS, "format", function () {
        return _.toArray(arguments).concat(",");
    });

    this.createFilterModule();
    this.server.respondWith("GET", new RegExp(this.favFiltersUrl), [200, { "Content-Type": "application/json" }, '[]']);
    this.server.respond();

    this.filterPanelView.render();

    ok(this.filterPanelView.$el.find(".favourite-filters").text().indexOf("issue.nav.filters.fav.anonymous") != -1, "Correct message shown.");
    var link = this.filterPanelView.$el.find(".favourite-filters a");
    ok(link.attr("href").indexOf("/login.jsp?") == 0, "Contains a login link");
});

test("System filters render login links when no logged in user", function() {
    JIRA.Issues.TestUtils.stubGetLoggedInUser("");

    this.createFilterModule([
        {id: 'sys-filter-a', requiresLogin: true},
        {id: 'sys-filter-b', requiresLogin: false}
    ]);
    this.filterPanelView.render();

    var $el1 = this.filterPanelView.$el.find("[data-id='sys-filter-a']");
    equal($el1.attr("href"), AJS.contextPath() + '/login.jsp?os_destination=%2Fissues%2F%3Ffilter%3Dsys-filter-a');

    var $el2 = this.filterPanelView.$el.find("[data-id='sys-filter-b']");
    equal($el2.attr("href"), AJS.contextPath() + '/issues/?filter=sys-filter-b');
});

test("System filters render login links when there is a logged in user", function() {
    JIRA.Issues.TestUtils.stubGetLoggedInUser("scott");

    this.createFilterModule([
        {id: 'sys-filter-a', requiresLogin: true},
        {id: 'sys-filter-b', requiresLogin: false}
    ]);
    this.filterPanelView.render();

    var $el1 = this.filterPanelView.$el.find("[data-id='sys-filter-a']");
    equal($el1.attr("href"), AJS.contextPath() + '/issues/?filter=sys-filter-a');

    var $el2 = this.filterPanelView.$el.find("[data-id='sys-filter-b']");
    equal($el2.attr("href"), AJS.contextPath() + '/issues/?filter=sys-filter-b');
});

test("Hover state is maintained after opening the actions dialog", function() {
    this.createFilterModule();
    this.filterModule.filtersComponent.favouriteFiltersCollection.add(new JIRA.Components.Filters.Models.Filter({
        id: "1",
        jql: "",
        name: "Happy Little Filter"
    }));

    this.filterPanelView.render();

    this.filterPanelView.$el.appendTo("#qunit-fixture");

    this.filterPanelView.$el.find("a.filter-actions").click();
    ok(this.filterPanelView.$el.find(".favourite-filters li").hasClass("hover"), "List item has hover class.");
    ok(this.filterPanelView.$el.find(".favourite-filters li a span").hasClass("active"), "Dropdown button has active class.");

    this.filterPanelView.$el.find("a.filter-actions").click();
    ok(!this.filterPanelView.$el.find(".favourite-filters li").hasClass("hover"), "List item doesn't have hover class.");
    ok(!this.filterPanelView.$el.find(".favourite-filters li a span").hasClass("active"), "Dropdown button doesn't have active class.");
});

test("makes render call when initialized", function() {
    var renderSpy = this.sandbox.spy(JIRA.Issues.FilterPanelView.prototype, "render");
    this.createFilterModule();

    equal(renderSpy.callCount, 1, "should be rendered when initialize method called");
});

test("renders open template when docked", function() {
    this.createFilterModule();
    equal(this.openTemplate.callCount, 1);
    equal(this.closedTemplate.callCount, 0);
});

test("rendering open template should trigger a JIRA.Events.NEW_CONTENT_ADDED event", function () {
    var triggerSpy = this.sandbox.spy(JIRA, "trigger");

    this.createFilterModule();

    equal(triggerSpy.callCount, 1, "First event triggered for implicit render (loading values)");
    var call = triggerSpy.firstCall;
    equal(call.args[0], JIRA.Events.NEW_CONTENT_ADDED, "Event is JIRA.Events.NEW_CONTENT_ADDED");
    equal(typeof call.args[1][1], "string", "Ensure event has a reason argument");
    equal(call.args[1][1], JIRA.CONTENT_ADDED_REASON.filterPanelOpened, "Event reason is JIRA.CONTENT_ADDED_REASON.filterPanelOpened");
});

test("Clicking on NewSearch triggers the filterSelected event", function() {
    this.createFilterModule();
    this.searchPageModule.resetToBlank = jQuery.noop;

    var onFilterSelected = sinon.spy();
    this.filterPanelView.on("filterSelected", onFilterSelected);

    this.filterPanelView._renderOpen();
    this.filterPanelView.$el.find("a.new-search").click();

    equal(onFilterSelected.callCount, 1, "The event 'filterSelected' is triggered");
    ok(onFilterSelected.calledWith(null), "No filter is specified in the event payload");
});

test("Changing the selected filter clears the previously active filter", function() {
    var previousFilter = new JIRA.Components.Filters.Models.Filter(
        { id: "1", jql: "", name: "Happy Little Filter" }
    );
    var newFilter = new JIRA.Components.Filters.Models.Filter(
        { id: "2", jql: "", name: "My new filter" }
    );
    this.createFilterModule([previousFilter, newFilter]);
    this.filterPanelView.render();
    this.filterPanelModel.set("activeFilter", previousFilter);

    this.filterPanelModel.set("activeFilter", newFilter);
    ok(!this.filterPanelView.$el.find("a.filter-link[data-id=1]").hasClass("active"));
});

test("Clearing the selected filter is reflected in the UI", function() {
    var previousFilter = new JIRA.Components.Filters.Models.Filter(
        { id: "1", jql: "", name: "Happy Little Filter" }
    );
    this.createFilterModule([previousFilter]);
    this.filterPanelView.render();
    this.filterPanelModel.set("activeFilter", previousFilter);

    this.filterPanelModel.set("activeFilter", null);
    ok(!this.filterPanelView.$el.find("a.filter-link[data-id=1]").hasClass("active"));
});

test("By default, no filter is selected in the UI", function() {
    var previousFilter = new JIRA.Components.Filters.Models.Filter(
        { id: "1", jql: "", name: "Happy Little Filter" }
    );
    this.createFilterModule([previousFilter]);
    this.filterPanelView.render();
    ok(!this.filterPanelView.$el.find("a.filter-link[data-id=1]").hasClass("active"));
});

test("favourite filters should render special message if failed to retrieve from server", function() {
    this.server.respondWith("GET", new RegExp(this.favFiltersUrl), [400, { "Content-Type": "application/json" }, '{"errorMessage": "Failed for some reason"}']);

    this.createFilterModule([], true);
    equal(this.el.find(".favourite-filters").text(), "issue.nav.filters.fav.loading", "Should tell the user that we're loading from the server");

    this.server.respond();
    equal(this.el.find(".favourite-filters").text(), "issue.nav.filters.fav.error", "Should tell user there was a problem");
});

test("favourite filters should persist special message if sidebar collapsed and expanded", function() {
    this.createFilterModule([], true);
    this.server.respondWith("GET", new RegExp(this.favFiltersUrl), [400, { "Content-Type": "application/json" }, '{"errorMessage": "Failed for some reason"}']);
    this.server.respond();
    equal(this.el.find(".favourite-filters").text(), "issue.nav.filters.fav.error", "Should tell user there was a problem");

    this.filterPanelModel.isDocked = sinon.stub().returns(false);
    this.filterPanelView.render();

    this.filterPanelModel.isDocked = sinon.stub().returns(true);
    this.filterPanelView.render();
    equal(this.el.find(".favourite-filters").text(), "issue.nav.filters.fav.error", "Should still tell user there was a problem");
});
