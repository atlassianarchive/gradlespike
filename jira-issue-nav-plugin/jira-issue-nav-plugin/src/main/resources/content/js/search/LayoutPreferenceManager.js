/**
 * Manages getting/setting the user's preferred layout preference.
 */
JIRA.Issues.LayoutPreferenceManager = {
    PREFERRED_LAYOUT_KEY: "jira.issues.preferred.layout.key",

    /**
     * @return {string} The key of the current user's preferred layout.
     */
    getPreferredLayoutKey: function () {
        return AJS.Meta.get(this.PREFERRED_LAYOUT_KEY);
    },

    /**
     * @param {string} layoutKey The key of the current user's preferred layout.
     * @param {object} [options]
     * @param {object} [options.ajax=true] Whether to post the user's preferred layout to the server.
     * @param {object} [options.render]
     */
    setPreferredLayoutKey: function (layoutKey, options) {
        options = _.defaults({}, options, {
            ajax: true
        });

        AJS.Meta.set(this.PREFERRED_LAYOUT_KEY, layoutKey);
        options.ajax && AJS.$.ajax({
            data: {
                layoutKey: layoutKey
            },
            type: "POST",
            headers: JIRA.Issues.XsrfTokenHeader,
            url: AJS.contextPath() + "/rest/issueNav/latest/preferredSearchLayout"

        });
    }
};