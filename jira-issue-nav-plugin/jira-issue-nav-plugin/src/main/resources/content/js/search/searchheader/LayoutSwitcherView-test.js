AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

module("JIRA.Issues.LayoutSwitcherView", {
    setup: function () {
        this.sandbox = sinon.sandbox.create();
        this.renderSpy = this.sandbox.spy(JIRA.Issues.LayoutSwitcherView.prototype, "render");
        this.searchPageModule = JIRA.Issues.TestUtils.mockSearchPageModule();
        this.layoutSwitcherView = new JIRA.Issues.LayoutSwitcherView({
            el: jQuery("<div></div>"),
            searchPageModule: this.searchPageModule
        });
        this.canDismissCommentStub = this.sandbox.stub(JIRA.Issues.Application, "request");
        this.canDismissCommentStub.withArgs("issueEditor:canDismissComment").returns(true);
    },

    teardown: function () {
        this.sandbox.restore();
    }
});

test("changeLayout should be called when triggering a switch", function () {
    var changeLayoutSpy = this.sandbox.spy(this.layoutSwitcherView.searchPageModule, "changeLayout"),
        element,
        options = {ajax: false};

    this.searchPageModule.getActiveLayout = this.sandbox.stub().returns({id: "list-view", label: "List View"});
    this.searchPageModule.getInactiveLayouts = this.sandbox.stub().returns([{id: "split-view", label: "Split View"}]);

    element = this.layoutSwitcherView.render().$("[data-layout-key=split-view]");
    this.layoutSwitcherView._onLayoutSwitchClick({
        target: element,
        preventDefault: jQuery.noop
    }, options);

    ok(changeLayoutSpy.calledWithExactly("split-view", options),
        "changeLayout is called when the layout switcher is clicked")
});

test("Layout switch should respect dirty comment warning before actually switching the layout", function () {
    var changeLayoutStub = this.sandbox.stub(this.layoutSwitcherView.searchPageModule, "changeLayout"),
        options = {ajax: false},
        event = {
            preventDefault: jQuery.noop
        };

    this.canDismissCommentStub.withArgs("issueEditor:canDismissComment").returns(false);
    this.layoutSwitcherView._onLayoutSwitchClick(event, options);
    equal(changeLayoutStub.callCount, 0,
            "changeLayout is NOT called when the layout switcher is clicked and dirty comment is NOT dismissed");

    this.canDismissCommentStub.withArgs("issueEditor:canDismissComment").returns(true);
    this.layoutSwitcherView._onLayoutSwitchClick(event, options);
    equal(changeLayoutStub.callCount, 1,
            "changeLayout is called when the layout switcher is clicked and dirty comment is dismissed");
});


test("Changing the current layout should cause a render", function () {
    this.searchPageModule.changeLayout("split-view", {ajax: false});
    equal(this.layoutSwitcherView.__proto__.render.callCount, 1, "render was called");
});

test("render() should trigger a JIRA.Events.NEW_CONTENT_ADDED", function () {
    var triggerSpy = this.sandbox.spy(JIRA, "trigger");

    this.searchPageModule.getActiveLayout = this.sandbox.stub().returns({id: "list-view", label: "List View"});
    this.searchPageModule.getInactiveLayouts = this.sandbox.stub().returns([{id: "split-view", label: "Split View"}]);
    this.layoutSwitcherView.render();

    equal(triggerSpy.callCount, 1, "One event is triggered");
    equal(triggerSpy.firstCall.args[0], JIRA.Events.NEW_CONTENT_ADDED, "Event is JIRA.Events.NEW_CONTENT_ADDED");
    equal(typeof triggerSpy.firstCall.args[1][1], "string", "Ensure event has a reason argument");
    equal(triggerSpy.firstCall.args[1][1], JIRA.CONTENT_ADDED_REASON.layoutSwitcherReady, "Event reason is JIRA.CONTENT_ADDED_REASON.layoutSwitcherReady");
});

