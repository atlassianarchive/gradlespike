/**
 * The JIRA issue API.
 * TODO TF-711 Dismantle the old JIRA.Issue object.
 *
 * @type {object}
 */
JIRA.Issues.IssueAPI = (function () {
    var searchPageModule;

    var patch = {
        /**
         * @return {null|string} the currently selected issue's ID or
         *     <tt>null</tt> if no issue is selected.
         */
        getIssueId: function () {
            return JIRA.Issues.Api.getSelectedIssueId();
        },

        /**
         * @return {null|string} the currently selected issue's key or
         *     <tt>null</tt> if no issue is selected.
         */
        getIssueKey: function () {
            return JIRA.Issues.Api.getSelectedIssueKey();
        }
    };

    return {
        /**
         * Patch the <tt>JIRA.Issue</tt> API with the above methods.
         *
         * @param {object} options
         */
        override: function (options) {
            // We override the target in tests.
            options = _.defaults({}, options, {
                target: JIRA.Issue
            });

            searchPageModule = options.searchPageModule;
            _.extend(options.target, patch);
        }
    };
})();