AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

module("JIRA.Issues.SearchModule", {
    setup: function() {
        this.searchModule = new JIRA.Issues.SearchModule({
            searchPageModule: JIRA.Issues.TestUtils.mockSearchPageModule()
        });
    }
});

test("isStandAloneIssue", function () {
    var tests = [
        [false, {}],
        [false, {
            jql: "project = JRA",
            selectedIssueKey: "JRA-123"
        }],
        [false, {
            filter: 10000,
            selectedIssueKey: "JRA-123"
        }],
        [false, {
            filter: 10000,
            jql: "project = JRA",
            selectedIssueKey: "JRA-123"
        }],
        [true, {
            selectedIssueKey: "JRA-123"
        }]
    ];

    _.each(tests, function (test) {
        var expected = test[0],
            state = test[1];

        equal(this.searchModule.isStandAloneIssue(state), expected,
                "Returns " + expected + " for " + JSON.stringify(state));
    }, this);
});