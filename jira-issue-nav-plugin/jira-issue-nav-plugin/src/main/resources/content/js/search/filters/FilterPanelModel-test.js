AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

module("JIRA.Issues.FilterPanelModel", {
    setup: function () {
        this.sandbox = sinon.sandbox.create();
        this.sandbox.stub(JIRA.Issues.Browser, 'getWindowWidth');

        this.storage = {
            getItem: this.sandbox.stub(),
            removeItem: this.sandbox.stub(),
            setItem: this.sandbox.stub()
        };

        this.filterPanelModel = new JIRA.Issues.FilterPanelModel({}, {
            storage: this.storage
        });
    },

    teardown: function() {
        this.sandbox.restore();
    }
});

test("Default state is undocked if window width is less than 1200", function () {
    JIRA.Issues.Browser.getWindowWidth.returns(1199);
    equal(this.filterPanelModel.isDocked(), false, "Expected filter panel to be undocked");
});

test("Default state is undocked if window width is 1200", function () {
    JIRA.Issues.Browser.getWindowWidth.returns(1200);
    equal(this.filterPanelModel.isDocked(), false, "Expected filter panel to be undocked");
});

test("Default state is docked if window width is greater than 1200", function () {
    JIRA.Issues.Browser.getWindowWidth.returns(1201);
    equal(this.filterPanelModel.isDocked(), true, "Expected filter panel to be undocked");
});

test("User preference overrides default docked state", function () {
    this.storage.getItem.returns("false");
    JIRA.Issues.Browser.getWindowWidth.returns(1201);
    equal(this.filterPanelModel.isDocked(), false, "Expected filter panel to be undocked");
});

test("User preference overrides default unddocked state", function () {
    this.storage.getItem.returns("true");
    JIRA.Issues.Browser.getWindowWidth.returns(1199);
    equal(this.filterPanelModel.isDocked(), true, "Expected filter panel to be docked");
});