AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

module("JIRA.Issues.SearchPageModule", {
    setup: function() {
        //Stop inline layer from appending to window.top so qunit iframe runner works
        this.sandbox = sinon.sandbox.create({
            useFakeServer: true
        });
        this.server = this.sandbox.server;
        JIRA.Issues.Application.start();

        AJS.params.ignoreFrame = true;
        this.goToIndexSpy = sinon.spy();
        this.server = sinon.fakeServer.create();

        this.searchPageModule = JIRA.Issues.TestUtils.mockSearchPageModule();
        this.realConfirmation = this.searchPageModule.confirmNavigation;

        this.confirmNavigationStub = this.searchPageModule.confirmNavigation
                = sinon.stub().returns(true);

        this.shortcutsEnabled = AJS.keyboardShortcutsDisabled;
        AJS.keyboardShortcutsDisabled = false;
    },
    teardown: function () {
        this.sandbox.restore();
        JIRA.Issues.Application.stop();

        //Shutdown all events listeners. This should be done by this.searchPageModule
        JIRA.Issues.Application.off();
        jQuery(".aui-loading").remove();
        AJS.keyboardShortcutsDisabled = this.shortcutsEnabled;
    }
});

test("confirmNavigation", function () {
    var confirmStub = sinon.stub().returns(true),
            dirtyCommentWarningStub = sinon.stub(JIRA.Issue, "getDirtyCommentWarning"),
            dirtyWarningStub = sinon.stub(JIRA.DirtyForm, "getDirtyWarning");


    this.searchPageModule.confirmNavigation = this.realConfirmation;
    // Nothing's dirty, so we shouldn't ask for confirmation.
    this.searchPageModule.confirmNavigation({confirm: confirmStub});
    ok(!confirmStub.called, "No confirmation requested.");

    // The comment form is dirty, so we should ask for confirmation.
    dirtyCommentWarningStub.returns("Dirty");
    this.searchPageModule.confirmNavigation({confirm: confirmStub});
    ok(confirmStub.called, "Confirmation requested.");

    // A different form is dirty, so we should ask for confirmation.
    dirtyCommentWarningStub.returns(undefined);
    dirtyWarningStub.returns("Filthy");
    this.searchPageModule.confirmNavigation({confirm: confirmStub});
    equal(2, confirmStub.callCount, "Confirmation requested");

    // We're ignoring dirtiness, so we shouldn't ask for confirmation.
    dirtyCommentWarningStub("Disgusting");
    this.searchPageModule.confirmNavigation({confirm: confirmStub, ignoreDirtiness: true});
    equal(2, confirmStub.callCount, "No confirmation requested");
});

test("JRADEV-9594: We can refresh the current search in JQL mode", function () {
    this.searchPageModule.queryModuleSearchRequested("project = HSP");
    this.server.respond(); // will not allow more than 1 request at a time
    this.searchPageModule.queryModuleSearchRequested("project = HSP");
    equal(this.searchPageModule.issueSearchManager.search.callCount, 2, "Two searches were performed");
});

test("Search triggered from QueryModule resets startIndex", function() {
    this.searchPageModule.issueSearchManager.search.returns(jQuery.Deferred().resolve({}).promise());
    this.searchPageModule.issueTableSortRequested('');
    ok(this.searchPageModule.searchResults.resetFromSearch.called, 'resetFromSearch is called');

    var arguments = this.searchPageModule.searchResults.resetFromSearch.getCall(0).args[0];
    equal(arguments.startIndex, undefined, "resetFromSearch is called with startIndex == undefined");
    equal(arguments.jql, "", "resetFromSearch is called with jql = ''");
});

test("Updating SearchPageModule from router should check if state has changed before resetting", function() {
    var getStateStub = sinon.stub(this.searchPageModule, "getState");
    var applyStateStub = sinon.stub(this.searchPageModule, "applyState");

    getStateStub.returns({
        jql: "GOTTA CATCHEM ALL",
        filter: null,
        startIndex: 9001,
        selectedIssueKey: null
    });
    var newState = _.extend({}, this.searchPageModule.getState());
    newState.selectedIssueKey = "POKE-2";

    this.searchPageModule.updateFromRouter(newState);
    ok(applyStateStub.calledWithExactly(newState, false), "Apply state is called with isReset false");

    getStateStub.returns( _.extend({}, newState));
    newState.selectedIssueKey = null;
    this.searchPageModule.updateFromRouter(newState);
    ok(applyStateStub.calledWithExactly(newState, false), "Apply state is called with isReset false");

    getStateStub.returns( _.extend({}, newState));
    newState.jql = "WUT";
    this.searchPageModule.updateFromRouter(newState);
    ok(applyStateStub.calledWithExactly(newState, true), "Apply state is called with isReset true");

    getStateStub.restore();
    applyStateStub.restore();
});

test("changeLayout", function () {
    var layoutA = _.extend({close: sinon.spy(), render: sinon.spy()}, Backbone.Events),
        layoutAConstructor = sinon.stub().returns(layoutA),
        layoutB = _.extend({close: sinon.spy(), render: sinon.spy()}, Backbone.Events),
        layoutBConstructor = sinon.stub().returns(layoutB);

    // Set up the prototype chain so instanceof works.
    layoutA.__proto__ = layoutAConstructor.prototype = {};
    layoutB.__proto__ = layoutBConstructor.prototype = {};
    this.searchPageModule.registerLayout("a", {
        label: "A",
        view: layoutAConstructor
    });
    this.searchPageModule.registerLayout("b", {
        label: "B",
        view: layoutBConstructor
    });

    this.searchPageModule.changeLayout("a");
    equal(layoutAConstructor.callCount, 1, "New layout created");
    equal(layoutA.render.callCount, 1, "render() called on the layout");
    ok(layoutA === this.searchPageModule.getCurrentLayout(), "Current layout set on SearchPageModule");

    // Requesting the same layout should be a no-op.
    this.searchPageModule.changeLayout("a");
    equal(layoutA.close.callCount, 0, "Layout not deactivated");
    equal(layoutAConstructor.callCount, 1, "No layout created");
    equal(layoutA.render.callCount, 1, "render() not called on the layout");
    ok(layoutA === this.searchPageModule.getCurrentLayout(), "Layout still set on SearchPageModule");

    this.searchPageModule.changeLayout("b", {render: false});
    equal(layoutA.close.callCount, 1, "Old layout deactivated");
    equal(layoutBConstructor.callCount, 1, "New layout created");
    equal(layoutB.render.callCount, 0, "render() not called on the layout");
    ok(layoutB === this.searchPageModule.getCurrentLayout(), "Current layout set on SearchPageModule");
});



test("Issue unselected when switching layouts", function () {

    var layoutA = _.extend({close: sinon.spy(), render: sinon.spy()}, Backbone.Events),
        layoutAConstructor = sinon.stub().returns(layoutA),
        layoutB = _.extend({close: sinon.spy(), render: sinon.spy()}, Backbone.Events),
        layoutBConstructor = sinon.stub().returns(layoutB);

    // Set up the prototype chain so instanceof works.
    layoutA.__proto__ = layoutAConstructor.prototype = {};
    layoutB.__proto__ = layoutBConstructor.prototype = {};
    this.searchPageModule.registerLayout("a", {
        label: "A",
        view: layoutAConstructor
    });
    this.searchPageModule.registerLayout("b", {
        label: "B",
        view: layoutBConstructor
    });

    this.searchPageModule.changeLayout("a");
    ok(!this.searchPageModule.search.getResults().unselectIssue.called, "If we do not have a previous a layout yet we should no be unselecting");

    // Requesting the same layout should be a no-op.
    this.searchPageModule.changeLayout("a");
    ok(!this.searchPageModule.search.getResults().unselectIssue.called, "If we already have that layout activated we should not be unselecting");

    this.searchPageModule.changeLayout("b", {render: false});

    // The layout is initialised in setup, so we just need to assert that unselectIssue was called.
    ok(this.searchPageModule.search.getResults().unselectIssue.called, "Issue unselected.");
});

test("get/registerLayout", function () {
    var key = "3d-view",
        layout = {};

    equal(this.searchPageModule.getLayout(key), null, "getLayout() returns null for unknown layouts");
    this.searchPageModule.registerLayout(key, layout);
    equal(this.searchPageModule.getLayout(key), layout, "getLayout() returns known layouts");
});

test("getEffectiveJql is doing the right thing", function() {
    var filter = new JIRA.Components.Filters.Models.Filter({id: 1, jql: "issuetype = Bug"});
    this.searchPageModule.setFilter(filter);
    this.searchPageModule.setJql("");

    equal(this.searchPageModule.getEffectiveJql(), "");
    this.searchPageModule.setJql(null);
    equal(this.searchPageModule.getEffectiveJql(), "issuetype = Bug");
});

test("selected issue is set when we load an issue", function () {
    var issue = new JIRA.Issues.SimpleIssue({key: "HSP-1", id: 10000});
    var spy = sinon.spy();
    this.searchPageModule.searchResults.selectIssueById = spy;
    this.searchPageModule.searchResults.updateIssueById = sinon.spy();

    JIRA.Issues.Application.trigger("issueEditor:loadComplete", issue, {});

    ok(spy.calledWithExactly(issue.getId(), {reason: "issueLoaded"}), "Expected selected issue to be set");
});

test("selected issue is NOT set if we supply a reason", function () {
    var issue = new JIRA.Issues.SimpleIssue({key: "HSP-1", id: 10000});
    var spy = sinon.spy();
    this.searchPageModule.searchResults.selectIssueById = spy;
    this.searchPageModule.searchResults.updateIssueById = sinon.spy();

    JIRA.Issues.Application.trigger("issueEditor:loadComplete", issue, {reason: {action: "update"}});

    equal(spy.callCount, 0, "Expected selected issue Not to be set as we supplied a reason");
});

test("can't navigate to previous/next when overlay is visible", function () {
    var instance = this;

    var testNextPrevWithOverlay = function(layoutKey) {
        var nextSpy = sinon.spy();
        var prevSpy = sinon.spy();

        instance.searchPageModule.changeLayout(layoutKey);

        instance.searchPageModule.getCurrentLayout().nextIssue = nextSpy;
        instance.searchPageModule.getCurrentLayout().prevIssue = prevSpy;

        var overlay = jQuery("<div class='aui-blanket'></div>");
        jQuery(document.body).append(overlay);

        instance.searchPageModule.nextIssue();
        instance.searchPageModule.prevIssue();

        equal(nextSpy.callCount, 0, "nextIssue not called when overlay is visible");
        equal(prevSpy.callCount, 0, "prevIssue not called when overlay is visible");

        overlay.remove();

        instance.searchPageModule.nextIssue();
        instance.searchPageModule.prevIssue();

        equal(nextSpy.callCount, 1, "nextIssue called when overlay is not visible");
        equal(prevSpy.callCount, 1, "prevIssue called when overlay is not visible");
    };

    testNextPrevWithOverlay("list-view");
    testNextPrevWithOverlay("split-view");
});
test("Can not navigate to state if comment can't be dismissed", function () {
    this.sandbox.stub(JIRA.Issues.Application, "request")
        .withArgs("issueEditor:canDismissComment").returns(false);

    var searchersDialogHideSpy = sinon.spy();
    AJS.InlineLayer.current = {hide: searchersDialogHideSpy};
    this.searchPageModule.queryModule.queryChanged = sinon.spy();

    this.searchPageModule.applyState = sinon.spy();
    this.searchPageModule.issueNavRouter.pushState = sinon.spy();
    this.searchPageModule.reset({jql: "blah"});
    equal(this.searchPageModule.applyState.callCount, 0, "Should not be applying state if user doesn't want to dismiss comment");
    equal(this.searchPageModule.issueNavRouter.pushState.callCount, 0, "Should not be updating url if user doesn't want to dismiss comment");
    equal(searchersDialogHideSpy.callCount, 1, "Should hide any inline layers as they may represent state that has not been applied");
    equal(this.searchPageModule.queryModule.queryChanged.callCount, 1, "Should notify the query module to reset back to original state");
});

test("Can navigate to state if comment can be dismissed", function () {
    this.sandbox.stub(JIRA.Issues.Application, "request")
        .withArgs("issueEditor:canDismissComment").returns(true);

    var searchersDialogHideSpy = sinon.spy();
    AJS.InlineLayer.current = {hide: searchersDialogHideSpy};
    this.searchPageModule.applyState = sinon.spy();
    this.searchPageModule.applyState = sinon.spy();
    this.searchPageModule.issueNavRouter.pushState = sinon.spy();
    this.searchPageModule.reset({jql: "blah"});
    equal(this.searchPageModule.applyState.callCount, 1, "Should not be applying state if user doesn't want to dismiss comment");
    equal(this.searchPageModule.issueNavRouter.pushState.callCount, 1, "Should not be updating url if user doesn't want to dismiss comment");
    equal(searchersDialogHideSpy.callCount, 0, "Should NOT hide any inline layers");
});


test("canDismissComment called for both next and prev issue", function () {
    this.sandbox.stub(JIRA.Issues.Application, "request")
        .withArgs("issueEditor:canDismissComment").returns(false);

    this.searchPageModule.nextIssue();
    this.searchPageModule.prevIssue();
    equal(JIRA.Issues.Application.request.withArgs("issueEditor:canDismissComment").callCount, 2, "Expected can Dismiss Comment to be called for both next and prev issue methods");
});

test("Applying a new state should abort any issue load if there is one", function() {
    this.sandbox.stub(JIRA.Issues.Application, "execute");

    this.searchPageModule.applyState({jql: ""}, true);

    ok(JIRA.Issues.Application.execute.calledOnce, "Application.execute was called once");
    ok(JIRA.Issues.Application.execute.calledWithExactly("issueEditor:abortPending"), "Command 'issueEditor:abortPending' was executed");
});

test("Down/up arrow key navigates to next/previous issue when allowed", function () {
    var nextIssueSpy = sinon.stub().returns(true);
    var prevIssueSpy = sinon.stub().returns(true);

    this.searchPageModule.nextIssue = nextIssueSpy;
    this.searchPageModule.prevIssue = prevIssueSpy;

    AJS.InlineLayer.current = {};
    ok(!this.searchPageModule.handleDown(), "Can't navigate down when inline layer is present");
    ok(!this.searchPageModule.handleUp(), "Can't navigate up when inline layer is present");

    AJS.InlineLayer.current = null;
    AJS.Dropdown.current = {};
    ok(!this.searchPageModule.handleDown(), "Can't navigate down when dropdown is present");
    ok(!this.searchPageModule.handleUp(), "Can't navigate up when dropdown is present");

    AJS.Dropdown.current = null;
    JIRA.Dialog.current = {};
    ok(!this.searchPageModule.handleDown(), "Can't navigate down when dialog is present");
    ok(!this.searchPageModule.handleUp(), "Can't navigate up when dialog is present");

    JIRA.Dialog.current = null;
    jQuery("<div id='menu-item' class='aui-dropdown2'></div>").appendTo("body").show();
    ok(!this.searchPageModule.handleDown(), "Can't navigate down when menu item is open");
    ok(!this.searchPageModule.handleUp(), "Can't navigate up when menu item is open");

    jQuery("#menu-item").remove();
    this.searchPageModule.changeLayout("list-view");

    this.searchPageModule.getCurrentLayout().isIssueViewActive = function() { return true };
    ok(!this.searchPageModule.handleDown(), "Can't navigate down when issue view is active");
    ok(!this.searchPageModule.handleUp(), "Can't navigate up when issue view is active");

    this.searchPageModule.getCurrentLayout().isIssueViewActive = function() { return false };
    ok(this.searchPageModule.handleDown(), "Handle down returns true if navigated");
    equal(nextIssueSpy.callCount, 1, "Next issue called");

    this.searchPageModule.searchResults.isFirstIssueHighlighted = function() { return true };
    ok(!this.searchPageModule.handleUp(), "Can't navigate up when first issue is highlighted");

    this.searchPageModule.searchResults.isFirstIssueHighlighted = function() { return false };
    ok(this.searchPageModule.handleUp(), "Handle up returns true if navigated");
    equal(prevIssueSpy.callCount, 1, "Previous issue called");
});

test("Changing layouts scrolls to top of page", function () {
    var layoutA = _.extend({close: sinon.spy(), render: sinon.spy()}, Backbone.Events),
        layoutAConstructor = sinon.stub().returns(layoutA),
        layoutB = _.extend({close: sinon.spy(), render: sinon.spy()}, Backbone.Events),
        layoutBConstructor = sinon.stub().returns(layoutB);

    // Set up the prototype chain so instanceof works.
    layoutA.__proto__ = layoutAConstructor.prototype = {};
    layoutB.__proto__ = layoutBConstructor.prototype = {};
    this.searchPageModule.registerLayout("a", {
        label: "A",
        view: layoutAConstructor
    });
    this.searchPageModule.registerLayout("b", {
        label: "B",
        view: layoutBConstructor
    });
    var $largeDiv = jQuery("<div />").height(3000).appendTo("body");
    jQuery("body").scrollTop(1000);
    this.searchPageModule.changeLayout("b");
    equal(jQuery("body").scrollTop(), 0, "Expected to scroll back to top of page when changing layouts.");
    $largeDiv.remove(); // cleanup
});

test("getState() returns issue module issue key when in standalone mode", function () {
    this.sandbox.stub(JIRA.Issues.Application, "request")
        .withArgs("issueEditor:getIssueKey").returns("TEST-1");

    this.searchPageModule.searchResults.getState = sinon.stub().returns({
        selectedIssueKey: "TEST-2"
    });

    this.searchPageModule.standalone = true;
    var state = this.searchPageModule.getState();
    equal(state.selectedIssueKey, "TEST-1");

    this.searchPageModule.standalone = false;
    state = this.searchPageModule.getState();
    equal(state.selectedIssueKey, "TEST-2");
});

test("Next issue code should not be run when there is not currentlayout", function() {
    var layoutA = _.extend({close: sinon.spy(), render: sinon.spy(), nextIssue: sinon.spy()}, Backbone.Events),
        layoutAConstructor = sinon.stub().returns(layoutA);

    this.searchPageModule.registerLayout("a", {
        label: "A",
        view: layoutAConstructor
    });

    this.searchPageModule.standalone = true;
    ok(!this.searchPageModule.nextIssue());

    this.searchPageModule.changeLayout("a");

    ok(this.searchPageModule.nextIssue());
    equal(layoutA.nextIssue.callCount, 1);
});

test("Prev issue code should not be run when there is not currentlayout", function() {
    var layoutA = _.extend({close: sinon.spy(), render: sinon.spy(), prevIssue: sinon.spy()}, Backbone.Events),
        layoutAConstructor = sinon.stub().returns(layoutA);

    this.searchPageModule.registerLayout("a", {
        label: "A",
        view: layoutAConstructor
    });

    this.searchPageModule.standalone = true;
    ok(!this.searchPageModule.prevIssue());

    this.searchPageModule.changeLayout("a");

    ok(this.searchPageModule.prevIssue());
    equal(layoutA.prevIssue.callCount, 1);
});

test("searchPageModule isDirty", function() {
    ok(!this.searchPageModule.isDirty(), "no filter exists, can't be dirty");

    this.searchPageModule.setJql("effectiveJql");
    this.searchPageModule.setFilter(new JIRA.Components.Filters.Models.Filter({jql: "effectiveJql"}));
    ok(!this.searchPageModule.isDirty(), "filter matches effective jql");

    this.searchPageModule.setJql("project = TST");
    ok(this.searchPageModule.isDirty(), "jql does not match filter jql");

    this.searchPageModule.setJql("effectiveJql");
    ok(!this.searchPageModule.isDirty(), "back to clean");

});

test("should set the filter id in the state when the filter is set", function () {
    var filter = new JIRA.Components.Filters.Models.Filter({id: 1});
    this.searchPageModule.setFilter(filter);

    equal(this.searchPageModule.getState().filter, 1, "the filter id");
});

test("should reset to blank when a filter is removed", function () {
    var filter = new JIRA.Components.Filters.Models.Filter({id: 1});
    this.searchPageModule.setFilter(filter);
    this.searchPageModule.filterModule.trigger('filterRemoved', {filterId: 1});

    equal(this.searchPageModule.getState().filter, null);
});

test("should not reset to blank when the filter removed is not the current filter", function () {
    var filter = new JIRA.Components.Filters.Models.Filter({id: 1});
    this.searchPageModule.setFilter(filter);
    this.searchPageModule.filterModule.trigger('filterRemoved', {filterId: 2});

    equal(this.searchPageModule.getState().filter, 1);
});

test("should not throw an exception when a filter is removed and there is no current filter", function () {
    this.searchPageModule.filterModule.trigger('filterRemoved', {filterId: 2});

    ok(true, 'no exception thrown');
});

test("should stop listening to filterRemoved events on the old filter module when a new filter module is set", function () {
    var filter = new JIRA.Components.Filters.Models.Filter({id: 1});
    this.searchPageModule.setFilter(filter);

    var oldFilterModule = this.searchPageModule.filterModule;
    this.searchPageModule.registerFilterModule(new JIRA.Issues.MockFilterModule());

    oldFilterModule.trigger('filterRemoved', {filterId: 1});

    equal(this.searchPageModule.getState().filter, 1);
});

test("should reset to filter when a filter is selected", function () {
    this.searchPageModule.filterModule.trigger('filterSelected', {filterId: 1});

    equal(this.searchPageModule.issueNavRouter.pushState.callCount, 1, 'issueNavRouter.pushState.callCount');
    var actualFilter = this.searchPageModule.issueNavRouter.pushState.firstCall.args[0].filter;
    equal(actualFilter, 1, 'issueNavRouter.pushState args.filter');
});

test("should stop listening to filterSelected events on the old filter module when a new filter module is set", function () {
    var oldFilterModule = this.searchPageModule.filterModule;
    this.searchPageModule.registerFilterModule(new JIRA.Issues.MockFilterModule());
    oldFilterModule.trigger('filterSelected', {filterId: 1});

    equal(this.searchPageModule.issueNavRouter.pushState.callCount, 0, 'issueNavRouter.pushState.callCount');
});

test("Applying a new filter to the state should update the window's title", function () {
    var titleUpdateSpy = this.stub(this.searchPageModule, "updateWindowTitle");
    this.searchPageModule.filterModule.getFilterById.returns(jQuery.Deferred().resolve().promise());
    this.stub(this.searchPageModule, "initSystemFilters").returns(jQuery.Deferred().resolve().promise());

    this.searchPageModule.applyState({jql: "", filter: 1}, true);

    ok(titleUpdateSpy.called, "Window title updated.");
});