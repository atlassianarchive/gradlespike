AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav-common");

module("JIRA.Issues.IssueAPI", {
    setup: function () {
        this.API = {};
        JIRA.Issues.IssueAPI.override({
            target: this.API
        });
    }
});

test("getIssueId", function () {
    var getSelectedIssueIdSpy = sinon.stub(JIRA.Issues.Api, "getSelectedIssueId").returns("42");

    equal("42", this.API.getIssueId(), "Returned the correct issue ID");
    ok(getSelectedIssueIdSpy.called, "Called JIRA.Issues.Api.getSelectedIssueId.");

    getSelectedIssueIdSpy.restore();
});

test("getIssueKey", function () {
    var getSelectedIssueKeySpy = sinon.stub(JIRA.Issues.Api, "getSelectedIssueKey").returns("JRA-123");

    equal("JRA-123", this.API.getIssueKey(), "Returned the correct issue key");
    ok(getSelectedIssueKeySpy.called, "Called JIRA.Issues.Api.getSelectedIssueKey.");

    getSelectedIssueKeySpy.restore();
});