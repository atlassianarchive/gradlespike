AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:sinon");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

module('JIRA.Issues.IssueNavCreator', {
    setup: function() {
        AJS.$(document).undelegate();
        AJS.keys = {shortcuts: [{moduleKey: 'toggle-filter-panel', keys: '['}]};
        this.sandbox = sinon.sandbox.create({
            properties: ["spy", "stub", "server", "requests"],
            useFakeTimers: false,
            useFakeServer: true
        });
        this.server = this.sandbox.server;
        this.sandbox.stub(JIRA.Issues.FilterPanelView.prototype, "render");
        this.preferredLayoutKeyStub = sinon.stub(JIRA.Issues.LayoutPreferenceManager, "getPreferredLayoutKey"); // not using sandbox since we're restoring in a moment
        this.preferredLayoutKeyStub.returns("split-view");
        JIRA.Issues.overrideIssueDialogs = this.sandbox.stub();

        JIRA.Issues.Application.start();
        this.issuenav = JIRA.Issues.IssueNavCreator.create(AJS.$(document), {});

        JIRA.Issues.Api.viewSelectedIssue = this.viewSelectedIssueSpy = this.sandbox.spy();
        this.issuenav.searchPageModule.handleLeft = this.handleLeftSpy = this.sandbox.spy();
        this.issuenav.searchPageModule.handleRight = this.handleRightSpy = this.sandbox.spy();
        this.issuenav.searchPageModule.handleDown = this.handleDownSpy = this.sandbox.spy();
        this.issuenav.searchPageModule.handleUp = this.handleUpSpy = this.sandbox.spy();
        this.preferredLayoutKeyStub.restore();
    },

    pressKey: function(target, keyCode, expectedCallCounts) {
        target.trigger(AJS.$.Event('keydown', { which: keyCode }));

        equal(this.viewSelectedIssueSpy.callCount, expectedCallCounts[0]);
        equal(this.handleLeftSpy.callCount, expectedCallCounts[1]);
        equal(this.handleRightSpy.callCount, expectedCallCounts[2]);
        equal(this.handleDownSpy.callCount, expectedCallCounts[3]);
        equal(this.handleUpSpy.callCount, expectedCallCounts[4]);

        this.viewSelectedIssueSpy.reset();
        this.handleLeftSpy.reset();
        this.handleRightSpy.reset();
        this.handleDownSpy.reset();
        this.handleUpSpy.reset();
    },

    checkAllSupportedKeysAreNotTriggered: function(target) {
        this.pressKey(target, AJS.$.ui.keyCode.ENTER, [0, 0, 0, 0, 0]);
        this.pressKey(target, AJS.$.ui.keyCode.LEFT, [0, 0, 0, 0, 0]);
        this.pressKey(target, AJS.$.ui.keyCode.RIGHT, [0, 0, 0, 0, 0]);
        this.pressKey(target, AJS.$.ui.keyCode.UP, [0, 0, 0, 0, 0]);
        this.pressKey(target, AJS.$.ui.keyCode.DOWN, [0, 0, 0, 0, 0]);
    },

    teardown: function() {
        AJS.$(document).undelegate();
        JIRA.Issues.Application.stop();
        JIRA.Issues.Application.off();
        this.sandbox.restore();
    }
});

test("keyboard navigation", function() {
    var doc = AJS.$(document);

    // Overlay is visible.
    var blanket = AJS.$("<div class='aui-blanket'></div>");
    AJS.$("#qunit-fixture").append(blanket);
    this.checkAllSupportedKeysAreNotTriggered(doc);
    blanket.remove();

    // Pressed inside input.
    var input = AJS.$("<input type='text'>");
    AJS.$("#qunit-fixture").append(input);
    this.checkAllSupportedKeysAreNotTriggered(input);

    // Enter pressed on a link.
    var link = AJS.$("<a></a>");
    link.click(function(e) { e.preventDefault(); });
    AJS.$("#qunit-fixture").append(link);
    this.pressKey(link, AJS.$.ui.keyCode.ENTER, [0, 0, 0, 0, 0]);

    // Unknown key.
    this.pressKey(doc, 1, [0, 0, 0, 0, 0]);

    this.pressKey(doc, AJS.$.ui.keyCode.ENTER, [1, 0, 0, 0, 0]);

    this.pressKey(doc, AJS.$.ui.keyCode.LEFT, [0, 1, 0, 0, 0]);

    this.pressKey(doc, AJS.$.ui.keyCode.RIGHT, [0, 0, 1, 0, 0]);

    this.pressKey(doc, AJS.$.ui.keyCode.DOWN, [0, 0, 0, 1, 0]);

    this.pressKey(doc, AJS.$.ui.keyCode.UP, [0, 0, 0, 0, 1]);
});

test("when IssueEditor fires saveSuccess, issueRefreshed is fired on JIRA event bus", function () {
    var issueId = "123",
        jiraEventSpy = sinon.spy();
    JIRA.bind(JIRA.Events.ISSUE_REFRESHED, jiraEventSpy);

    JIRA.Issues.Application.trigger("issueEditor:saveSuccess", {issueId: issueId});

    equal(jiraEventSpy.callCount, 1, "Event handler should have been called only once.");
    equal(jiraEventSpy.firstCall.args[1], issueId, "issueId should be correct value");
});

test("when IssueEditor fires saveError, issueRefreshed is fired on JIRA event bus if saveError is not deferred", function () {
    var issueId = "123",
        jiraEventSpy = sinon.spy();
    JIRA.bind(JIRA.Events.ISSUE_REFRESHED, jiraEventSpy);

    JIRA.Issues.Application.trigger("issueEditor:saveError", {issueId: issueId, deferred: false});

    equal(jiraEventSpy.callCount, 1, "Event handler should have been called only once.");
    equal(jiraEventSpy.firstCall.args[1], issueId, "issueId should be correct value.");
});

test("when IssueEditor fires saveError, issueRefreshed is not fired on JIRA event bus if saveError is deferred", function () {
    var issueId = "123",
        jiraEventSpy = sinon.spy();
    JIRA.bind(JIRA.Events.ISSUE_REFRESHED, jiraEventSpy);

    JIRA.Issues.Application.trigger("issueEditor:saveError", {issueId: issueId, deferred: true});
    ok(!jiraEventSpy.called, "Event handler should not have been called.");
});