/**
 * Overrides for the <tt>JIRA.IssueNavigator</tt> API.
 * TODO TF-711 Dismantle the old JIRA.IssueNavigator object.
 *
 * @type {object}
 */
JIRA.Issues.IssueNavigatorAPI = (function () {
    var searchPageModule;

    var IssueNavigatorMixin = {
        /**
         * @return {Boolean} whether issue search is visible.
         */
        isNavigator: function () {
            return searchPageModule.isIssueVisible();
        }
    };

    var IssueNavigatorShortcutsMixin = {
        focusSearch: JIRA.Issues.Api.focusSearch,
        isNavigator: IssueNavigatorMixin.isNavigator,
        selectNextIssue: JIRA.Issues.Api.nextIssue,
        selectPreviousIssue: JIRA.Issues.Api.prevIssue,
        viewSelectedIssue: JIRA.Issues.Api.viewSelectedIssue
    };

    return {
        /**
         * Override the <tt>JIRA.IssueNavigator</tt> API with the above mixins.
         *
         * @param {object} options
         * @param {SearchPageModule} options.searchPageModule
         */
        override: function (options) {
            searchPageModule = options.searchPageModule;

            JIRA.IssueNavigator = _.extend(JIRA.IssueNavigator, IssueNavigatorMixin);
            JIRA.IssueNavigator.Shortcuts = _.extend(JIRA.IssueNavigator.Shortcuts,
                    IssueNavigatorShortcutsMixin);
        }
    };
})();