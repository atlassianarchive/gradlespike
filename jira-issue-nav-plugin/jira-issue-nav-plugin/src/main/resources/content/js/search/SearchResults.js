(function ($) {

    Backbone.define('JIRA.Issues.SearchResults', JIRA.Issues.BaseModel.extend({

        defaults: {
            issueIds: [],
            selectedIssue: new JIRA.Issues.SimpleIssue(),
            highlightedIssue: new JIRA.Issues.SimpleIssue(),
            total: 0
        },

        properties: [
            "columnSortJql",
            "highlightedIssue",
            "issueIds", // only present in stable search
            "issueIdsForPage",
            "jiraHasIssues",
            "pageSize",
            "resultsId",
            "selectedIssue",
            "startIndex",
            "sortBy",
            "columns",
            "table",
            "total",
            "initialPayload",
            "columnConfig"
        ],

        namedEvents: [
            "prevIssueSelected",
            "nextIssueSelected",

            "issueUpdated",
            "issueDeleted",

            "issueDoesNotExist",

            "stableUpdate"
        ],

        initialize: function (attr, options) {
            JIRA.Issues.ModelUtils.makeTransactional(this, "resetFromSearch", "selectNextIssue", "selectPrevIssue");
            this.issueUpdateCallbacks = [];
            this._issueSearchManager = options.issueSearchManager;
            this._initialSelectedIssue = options.initialSelectedIssue;
            this._columnConfig = options.columnConfig;
        },

        /**
         * Returns the startIndex for a given page.s
         *
         * @param {Number} page the page number (0 based)
         * @return {number} the startIndex
         */
        getStartIndexForPage: function (page) {
            return page * this.getPageSize();
        },

        hasIssue: function (id) {
            return _.indexOf(this.getIssueIds(), id) !== -1;
        },

        getPager: function () {
            if (this.hasIssue(this.getSelectedIssue().getId())) {
                var issueIds = this.getIssueIds();
                var selectedId = this.getSelectedIssue().getId();
                var position = _.indexOf(issueIds, selectedId);
                var resultCount = this.getTotal();
                var stableSearchLimit = issueIds.length;
                var pager = {
                    position: JIRA.NumberFormatter.format(position + 1),
                    resultCount: JIRA.NumberFormatter.format(resultCount)
                };
                if (position > 0) {
                    var prevIssueId = this._getPrevIssueId(selectedId);
                    if (prevIssueId) {
                        pager.previousIssue = {
                            id: prevIssueId,
                            key: this._getIssueKeyForId(prevIssueId)
                        };
                    }
                }
                if (position < resultCount - 1 && position < stableSearchLimit - 1) {
                    var nextIssueId = this._getNextIssueId(selectedId);
                    if (nextIssueId) {
                        pager.nextIssue = {
                            id: nextIssueId,
                            key: this._getIssueKeyForId(nextIssueId)
                        };
                    }
                }
                return pager;
            }
        },

        /**
         * Returns the number of pages in this SearchResults.
         *
         * @return {Number} the number of pages
         */
        getNumberOfPages: function () {
            return this._getPageNumberForStartIndex(this.getDisplayableTotal() - 1);
        },

        /**
         * Remove an issue from the search results.
         * <p/>
         * The highlighted issue is updated accordingly.
         *
         * @param {number} id The ID of the issue to remove.
         */
        removeIssue: function (id) {
            id = parseInt(id, 10);

            var isFirstIssue = this.getIssueIds()[0] === id,
                isHighlighted = this.getHighlightedIssue().getId() === id,
                isLastIssue = _.last(this.getIssueIds()) === id;

            if (isHighlighted) {
                if (!isLastIssue) {
                    this.highlightNextIssue({replace: true});
                } else if (!isFirstIssue) {
                    this.highlightPrevIssue({replace: true});
                }
            }

            this.setIssueIds(_.without(this.getIssueIds(), id));
            this.setTotal(this.getTotal() - 1);
            this.triggerIssueDeleted({
                id: id,
                key: this._getIssueKeyForId(id)
            });
        },

        isFirstIssueHighlighted: function() {
            if (!this.hasHighlightedIssue()) {
                return false;
            }

            return this.getHighlightedIssue().getId() === this.getIssueIds()[0];
        },

        updateIssueById: function (issueUpdateObject, options) {
            return this.updateIssue(issueUpdateObject, options);
        },

        /**
         * Update the search results in response to an issue update.
         *
         * @param {object} issueUpdate An issue update object (see <tt>JIRA.Issues.Utils.getUpdateCommandForDialog</tt>).
         * @param {object} [options]
         * @param {boolean} [options.showMessage=true] Whether a success message should be shown.
         * @param {JIRA.Components.Filters.Models.Filter} [options.filter=null] Filter, in which context issues should be shown.
         * @return {jQuery.Deferred} A deferred that is resolved when the update completes.
         */
        updateIssue: function (issueUpdate, options) {
            var isDelete = issueUpdate.action === JIRA.Issues.Actions.DELETE,
                issueID = issueUpdate.id,
                promises = [];

            options = _.defaults({}, options, {
                showMessage: true,
                filter: null
            });

            if (isDelete) {
                this.removeIssue(issueID);
                options.showMessage && this._notifyOfIssueUpdate(issueUpdate);
                return jQuery.Deferred().resolve().promise();
            } else {
                return this.getResultForId(issueID, options.filter).done(_.bind(function (entity) {
                    _.each(this.issueUpdateCallbacks, function (callback) {
                        var result = callback.handler.call(callback.ctx || window, issueID, entity, issueUpdate);
                        if (result && result.promise) {
                            promises.push(result);
                        }
                    });

                    $.when(promises).done(_.bind(function () {
                        options.showMessage && this._notifyOfIssueUpdate(issueUpdate);
                    }, this));
                }, this));
            }
        },

        _notifyOfIssueUpdate: function (issueUpdate) {
            issueUpdate.message && JIRA.Issues.showNotification(issueUpdate.message, issueUpdate.key);
            JIRA.trace("jira.search.stable.update");
        },

        getResultForId: function (id, filter) {
            var options = { columnConfig: this._columnConfig.columnPickerModel.getColumnConfig() };

            //if used filter is system filter we don't want to get results based off him
            if (filter && !filter.getIsSystem()) {
                options = _.extend({ filterId: filter.getId() }, options);
            }
            return this._issueSearchManager.getRowsForIds([id], options);
        },

        isHighlightedIssueAccessible: function () {
            if (!this.hasHighlightedIssue()) {
                return null;
            }
            return !this._issueSearchManager.issueKeys.hasError(this.getHighlightedIssue().getId());
        },

        getDisplayableTotal: function () {
            return this.getIssueIds().length;
        },

        selectIssueByKey: function (key, options) {
            if (!key) {
                this.unselectIssue();
                return;
            }

            var id = this._getIssueIdForKey(key);

            if (!id || id==-1) {
                this._unhighlightIssue();

                this.getSelectedIssue().set({
                    id: -1,
                    key: key
                });

                this.triggerIssueDoesNotExist();
            } else {
                this._selectExistingIssueById(id, options);
            }
        },

        _unhighlightIssue: function() {
            this.getHighlightedIssue().set({
                id: null,
                key: null
            });
        },

        selectFirstInPage: function (options) {
            this.selectIssueById(this.getIssueIds()[this.getStartIndex()], options);
        },

        getPrevPageStartIndex: function () {
            var target = (this.getStartIndex() || 0) - this.getPageSize();
            return (target < 0) ? null : target;
        },

        getNextPageStartIndex: function () {
            var target = this.getPositionOfLastIssueInPage();
            return (target >= this.getDisplayableTotal()) ? null : target;
        },

        /**
         * Returns the position in the search results of the last issue on the current page (e.g. if the page size is
         * 5 and we are on the first page this returns 5).
         *
         * @return {Number}
         */
        getPositionOfLastIssueInPage: function () {
            var startIndex = this.getStartIndex();
            var pageSize = this._pageSize();

            return Math.min(startIndex + pageSize, this.getDisplayableTotal());
        },

        highlightFirstInPage: function () {
            this.highlightIssueById(this.getIssueIds()[this.getStartIndex()]);
        },

        selectIssueById: function (id, options) {
            if (!id) {
                this.unselectIssue();
            } else {
                this._selectExistingIssueById(id, options);
            }
        },

        _selectExistingIssueById: function(id, options) {
            if (id !== this.getSelectedIssue().getId() && JIRA.Issues.Application.request("issueEditor:canDismissComment")) {
                id = id ? parseInt(id, 10) : null;
                this.getSelectedIssue().set({
                    id: id,
                    key: id ? this._getIssueKeyForId(id) : null
                }, options);
                this.highlightIssueById(id);
            }
        },

        /**
         * Sets the issue with the given id as the highlighted issue and updates the startIndex accordingly (such that
         * the startIndex is equal to the offset of the first issue of the page that contains the highlighted issue).
         *
         * @param {number} id The ID of the issue to highlight.
         * @param {object} [options]
         * @param {boolean} [options.replace=false] Whether highlighting the issue should be a "replace" operation.
         */
        highlightIssueById: function (id, options) {
            options = _.defaults({}, options, {
                replace: false
            });

            if (id && id !== this.getHighlightedIssue().getId()) {
                id = id ? parseInt(id, 10) : null;
                this.getHighlightedIssue().set({
                    id: id,
                    key: id ? this._getIssueKeyForId(id) : null
                }, options);
                if (id) {
                    this.setStartIndex(this._getStartIndexForIssueId(id));
                }
            }
        },

        getState: function () {
            return {
                selectedIssueKey: this.getSelectedIssue().getKey(),
                startIndex: this.getStartIndex()
            };
        },

        /**
         * Resets this SearchResults using new search data. This effectively wipes any existing state and replaces it
         * with the passed-in state.
         *
         * Calling this method generates a new <code>resultsId</code>, which triggers a "newPayload" event.
         *
         * @param state
         */
        resetFromSearch: function (state) {
            state.resultsId = _.uniqueId();
            this.getSelectedIssue().set({id: null, key: null});
            this.set({sortBy: null});
            this.set("startIndex", state.startIndex, {silent: true});
            this.set(_.pick(state, this.properties));
            if (typeof state.selectedIssueKey === 'string') {
                this.selectIssueByKey(state.selectedIssueKey);
            } else {
                if (this.hasIssues()) {
                    this.highlightFirstInPage();
                } else {
                    this.getHighlightedIssue().set({id: null, key: null});
                }
            }
        },

        hasIssues: function () {
            return !!this.getIssueIds().length;
        },

        isIssueOnPage: function (id) {
            return _.indexOf(this.getPageIssueIds(), id) !== -1;
        },

        isFirstIssueSelected: function() {
            if (this.hasIssue(this.getSelectedIssue().getId())) {
                var issueIds = this.getIssueIds();
                var selectedId = this.getSelectedIssue().getId();
                var position = _.indexOf(issueIds, selectedId);
                return position === 0;
            }

            return false;
        },

        /**
         * @param options
         * @param options.filterId
         * @param options.columnConfig
         */
        getResultsForPage: function (options) {
            _.extend(options, {columnConfig: this._columnConfig.columnPickerModel.getColumnConfig()});

            if (this.getTable()) {
                var result = this.getTable();
                this.setTable(null, {silent: true}); //do not trigger a new search
                _.defer(function () {
                    JIRA.trace("jira.search.finished");
                });
                return $.Deferred().resolve(result).promise();
            }
            return this.getResultsForIds(this.getPageIssueIds(), options);
        },

        /**
         * @param ids
         * @param options
         * @param options.filterId
         * @param options.columnConfig
         */
        getResultsForIds: function (ids, options) {
            var instance = this;
            var deferred = $.Deferred();
            this._issueSearchManager.getRowsForIds(ids, options)
            .done(function (result) {
                //HACK - The REST endpoint for splitview always returns columnConfig="user", which is very
                //wrong. We can update the columnConfig only for listview, as that REST endpoint is the one
                //returning the good values.
                var isSplitViewResponse = _.isArray(result.table);
                if (!isSplitViewResponse) {
                    instance.setColumnConfig(result.columnConfig);
                    instance.setColumns(result.columns);
                    instance.setColumnSortJql(result.columnSortJql);
                }
                deferred.resolve(result.table);
            }).fail(deferred.reject)
                .always(function () {
                    _.defer(function () {
                        JIRA.trace("jira.search.finished");
                    });
                });
            return deferred;
        },

        applyState: function (state) {
            this.set(_.pick(state, this.properties));
        },

        unselectIssue: function (options) {
            var selectedIssue = this.getSelectedIssue();
            if (selectedIssue.getId() && JIRA.Issues.Application.request("issueEditor:canDismissComment")) {
                selectedIssue.set({
                    id: null,
                    key: null
                }, options);
            }
        },

        hasSelectedIssue: function () {
            return !!this.getSelectedIssue().getId();
        },

        hasHighlightedIssue: function () {
            return !!this.getHighlightedIssue().getId();
        },

        getPageIssueIds: function () {
            var startIndex = this.getStartIndex();
            var pageSize = this._pageSize();

            var issueIds = this.getIssueIds();
            return issueIds ? issueIds.slice(startIndex, Math.min(startIndex + pageSize, issueIds.length)) : [];
        },

        getPageIssues: function () {
            // return ids and keys
            return _.map(this.getPageIssueIds(), function (id) {
                return { id: id, key: this._getIssueKeyForId(id) };
            }, this);
        },

        getPageNumber: function () {
            return Math.floor(this.getStartIndex() / this._pageSize());
        },

        /**
         * Returns the position of the given issue id on the page (0-based).
         *
         * @param issueId
         * @return {number}
         */
        getPositionOfIssueInPage: function (issueId) {
            return this.getPositionOfIssueInSearchResults(issueId) % this._pageSize();
        },

        /**
         * Returns the position of the given issue in the stable search results (0-based).
         *
         * @param issueId
         * @return {*}
         */
        getPositionOfIssueInSearchResults: function (issueId) {
            return _.indexOf(this.getIssueIds(), issueId);
        },

        /**
         * (Async) Selects the next issue in the search results if possible. Clients can register a callback using
         * onSelectedIssueChange() to subscribe to selected issue change events, or alternatively use the promise
         * that this method returns.
         *
         * @return {jQuery.Promise} a promise with the id of the selected issue if successful
         */
        selectNextIssue: function (options) {
            if (!this.hasSelectedIssue()) {
                return $.Deferred().reject().promise();
            }

            var nextId = this._getNextIssueId(this.getSelectedIssue().getId());
            this._triggerNextIssueSelectedEvent(nextId);

            return this._selectIssue(nextId, options);
        },

        /**
         * (Async) Selects the previous issue in the search results if possible. Clients can register a callback using
         * onSelectedIssueChange() to subscribe to selected issue change events, or alternatively use the promise
         * that this method returns.
         *
         * @return {jQuery.Promise} a promise with the id of the selected issue if successful
         */
        selectPrevIssue: function (options) {
            if (!this.hasSelectedIssue()) {
                return $.Deferred().reject().promise();
            }

            var prevId = this._getPrevIssueId(this.getSelectedIssue().getId());
            this._triggerPrevIssueSelectedEvent(prevId);

            return this._selectIssue(prevId, options);
        },

        _selectIssue: function(issueId, options) {
            this.selectIssueById(issueId, options);
            return $.Deferred().resolve(issueId).promise();
        },

        /**
         * (Async) Highlights the previous issue in the search results if possible. Clients can register a callback using
         * onHighlightedIssueChange() to subscribe to selected issue highlighted events, or alternatively use the
         * promise that this method returns.
         *
         * @param {object} [options]
         * @param {boolean} [options.replace=false] Whether highlighting the issue should be a "replace" operation.
         * @return {jQuery.Promise} a promise with the id of the highlighted issue if successful
         */
        highlightNextIssue: function (options) {
            if (!this.hasHighlightedIssue()) {
                return $.Deferred().reject().promise();
            }

            // this is always synchronous in stable search but it is async when we are going across page boundaries
            // in dynamic search (we need to get the set of issues in the next page at this point)
            var nextId = this._getNextIssueId(this.getHighlightedIssue().getId());
            this.highlightIssueById(nextId, options);

            return $.Deferred().resolve(nextId).promise();
        },

        /**
         * (Async) Highlights the next issue in the search results if possible. Clients can register a callback using
         * onHighlightedIssueChange() to subscribe to selected issue highlighted events, or alternatively use the
         * promise that this method returns.
         *
         * @param {object} [options]
         * @param {boolean} [options.replace=false] Whether highlighting the issue should be a "replace" operation.
         * @return {jQuery.Promise} a promise with the id of the highlighted issue if successful
         */
        highlightPrevIssue: function (options) {
            if (!this.hasHighlightedIssue()) {
                return $.Deferred().reject().promise();
            }

            // this is always synchronous in stable search but it is async when we are going across page boundaries
            // in dynamic search (we need to get the set of issues in the next page at this point)
            var prevId = this._getPrevIssueId(this.getHighlightedIssue().getId());
            this.highlightIssueById(prevId, options);

            return $.Deferred().resolve(prevId).promise();
        },

        /**
         * Show the page starting at <tt>startIndex</tt>.
         * <p/>
         * This method is asynchronous.
         *
         * @param {number} startIndex The index of the first issue on the page.
         * @param {object} [options]
         * @param {boolean} [options.replace=false] Whether showing the page should be a "replace" operation.
         * @return {jQuery.Deferred} A deferred that will be resolved with the ID of the newly highlighted issue.
         */
        goToPage: function (startIndex, options) {
            options = _.defaults({}, options, {
                replace: false
            });

            if (startIndex === null || startIndex === this.getStartIndex()) {
                return $.Deferred().resolve().promise();
            }

            // Highlighting an issue updates the startIndex to ensure it is on the current page.
            var ID = this.getIssueIds()[startIndex];
            this.highlightIssueById(ID, options);

            return $.Deferred().resolve(ID).promise();
        },

        onHighlightedIssueChange: function (callback, context) {
            this.getHighlightedIssue().on("change", callback, context);
        },

        offHighlightedIssueChange: function (callback, context) {
            this.getHighlightedIssue().off("change", callback, context);
        },

        onSelectedIssueChange: function (callback, context) {
            this.getSelectedIssue().on("change", callback, context);
        },

        offSelectedIssueChange: function (callback, context) {
            this.getSelectedIssue().off("change", callback, context);
        },

        onColumnConfigChange: function (callback, context) {
            this.on("change:columnConfig", callback, context);
        },

        offColumnConfigChange: function (callback, context) {
            this.off("change:columnConfig", callback, context);
        },

        onColumnsChange: function (callback, context) {
            this.on("change:columns", callback, context);
        },

        offColumnsChange: function (callback, context) {
            this.off("change:columns", callback, context);
        },

        onStartIndexChange: function (callback, context) {
            this.on("change:startIndex", callback, context);
        },

        onNewIssueIds: function (callback, context) {
            this.on("change:issueIds", callback, context);
        },

        offNewIssueIds: function (callback, context) {
            this.off("change:issueIds", callback, context);
        },

        offStartIndexChange: function (callback, context) {
            this.off("change:startIndex", callback, context);
        },

        onNewPayload: function (func, context) {
            this.on("change:resultsId", func, context);
        },

        offNewPayload: function (func, context) {
            this.off("change:resultsId", func, context);
        },

        onIssueUpdated: function (func, ctx) {
            this.issueUpdateCallbacks.push({
                handler: func,
                ctx: ctx
            });
        },

        offIssueUpdated: function (func) {
            var filteredCallbacks = [];
            this.issueUpdateCallbacks = _.each(this.issueUpdateCallbacks, function (callback) {
                if (callback.handler !== func) {
                    filteredCallbacks.push(callback);
                }
            });
            this.issueUpdateCallbacks = filteredCallbacks;
        },

        _getNextIssueId: function (id) {
            var issueIds = this.getIssueIds();
            return issueIds[Math.min(_.indexOf(issueIds, id) + 1, this.getDisplayableTotal() - 1)];
        },

        getNextIssueForId: function (id) {
            var nextId = this._getNextIssueId(id);
            return { id: nextId, key: this._getIssueKeyForId(nextId) }
        },

        getNextIssueForSelectedIssue: function () {
            return this.getNextIssueForId(this.getSelectedIssue().getId());
        },

        _getPrevIssueId: function (id) {
            var issueIds = this.getIssueIds();
            return issueIds[Math.max(0, _.indexOf(this.getIssueIds(), id) - 1)];
        },

        /**
         * Calculate the start index that should be used to show a particular issue.
         * <p/>
         * Returns 0 if the issue isn't present in the search results.
         *
         * @param {number} id The issue's ID.
         * @return {number} The start index.
         * @private
         */
        _getStartIndexForIssueId: function (id) {
            var issueIndex = _.indexOf(this.getIssueIds(), id),
                pageSize = this._pageSize();

            return Math.max(0, Math.floor(issueIndex / pageSize) * pageSize);
        },

        _getIssueIdForKey: function (key) {
            // this only happens if the selected issue is not in the search results (i.e. when the user
            // navigates to the selected issue directly but has a search context).
            if (this._initialSelectedIssue && key === this._initialSelectedIssue.key) {
                return this._initialSelectedIssue.id;
            }

            return this._getIssueKeysToIds()[key];
        },

        _getIssueKeyForId: function (id) {
            // this only happens if the selected issue is not in the search results (i.e. when the user
            // navigates to the selected issue directly but has a search context).
            if (this._initialSelectedIssue && id === this._initialSelectedIssue.id) {
                return this._initialSelectedIssue.key;
            }

            return this._getIssueIdsToKeys()[id];
        },

        _getIssueIdsToKeys: function () {
            return this._issueSearchManager.issueKeys.getAllCached();
        },

        _getIssueKeysToIds : function () {
            var obj = {};
            var idsToKeys = this._issueSearchManager.issueKeys.getAllCached();
            _.each(idsToKeys, function (value, name) {
                obj[value] = name;
            });

            return obj;
        },

        /**
         * Returns a 0-based page number.
         *
         * @param startIndex the start index
         * @return {Number} a 0-based page number.
         * @private
         */
        _getPageNumberForStartIndex: function (startIndex) {
            return Math.floor(startIndex / this._pageSize());
        },

        /**
         * Returns the page size used for the search.
         *
         * @return {Number} the page size
         * @private
         */
        _pageSize: function () {
            return this.getPageSize();
        },

        _triggerPrevIssueSelectedEvent: function (prevId) {
            if (prevId !== this.getSelectedIssue().getId()) {
                var prevPrevId = this._getPrevIssueId(prevId);

                this.trigger("prevIssueSelected", {
                    prevIssue:     { id: prevId, key: this._getIssueKeyForId(prevId) },
                    prevPrevIssue: { id: prevPrevId, key: this._getIssueKeyForId(prevPrevId) }
                });
            }
        },

        _triggerNextIssueSelectedEvent: function (nextId) {
            if (nextId !== this.getSelectedIssue().getId()) {
                var nextNextId = this._getNextIssueId(nextId);

                this.trigger("nextIssueSelected", {
                    nextIssue:     { id: nextId, key: this._getIssueKeyForId(nextId) },
                    nextNextIssue: { id: nextNextId, key: this._getIssueKeyForId(nextNextId) }
                });
            }
        }
    }));
})(AJS.$);
