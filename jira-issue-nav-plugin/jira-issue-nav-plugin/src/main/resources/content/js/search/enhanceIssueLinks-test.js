AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

module("JIRA.Issues.enhanceLinks", {
    setup: function () {
        //Stop inline layer from appending to window.top so qunit iframe runner works
        AJS.params.ignoreFrame = true;
        this.sandbox = sinon.sandbox.create();
        this.$testElement = AJS.$("<div></div>");
        AJS.$("body").append(this.$testElement);

        this.oldBackboneHistory = Backbone.history;
        Backbone.history = {
            options: {
                root: ""
            }
        };

        this.router = {
            navigate: sinon.spy()
        };

        this.searchPageModule = JIRA.Issues.TestUtils.mockSearchPageModule();
        this.searchPageModule.searchResults = JIRA.Issues.TestUtils.mockSearchResults();
        this.searchPageModule.update = sinon.spy();
        this.searchPageModule.reset = sinon.spy();

    },
    teardown: function(){
        Backbone.history = this.oldBackboneHistory;
        this.sandbox.restore();
        this.$testElement.remove();
        this.router.navigate.reset();
        this.searchPageModule.update.reset();
        this.searchPageModule.reset.reset();
    }
});

test("withPushState", function(){
    var $link = AJS.$("<a href='/issues/?query=123' class='push-state'>link</a>");
    this.$testElement.append($link);

    JIRA.Issues.enhanceLinks.withPushState({
        router: this.router
    });
    $link.click();

    ok(this.router.navigate.calledOnce,
        "router.navigate is called"
    );

    equal(this.router.navigate.firstCall.args[0], "/issues/?query=123",
        "Clicked queryString is passed to router.naviage"
    );
});

test("Links to issue", function(){
    JIRA.Issues.enhanceLinks.toIssue({
        searchPageModule: this.searchPageModule
    });

    var $link = AJS.$("<a href='/issues/?query=123'>link</a>");
    this.$testElement.append($link);

    var $testElement = this.$testElement;

    this.sandbox.stub(JIRA.Issues.Application, "execute");

    function runTest() {
        $link.click();
        ok(JIRA.Issues.Application.execute.calledOnce, "Application.execute was called once");
        ok(JIRA.Issues.Application.execute.calledWith("issueEditor:updateIssueWithQuery", {query: "123"}), "Command 'issueEditor:updateIssueWithQuery' was executed");
        JIRA.Issues.Application.execute.reset();
    }

    $testElement.attr("id","attachment-sorting-options");
    runTest();

    $testElement.attr("id","attachment-sorting-order-options");
    runTest();

    $link.attr("id","subtasks-show-all");
    runTest();

    $link.attr("id","subtasks-show-open");
    runTest();
});

test("Breadcrumb links inside of issue table call reset", function () {
    JIRA.Issues.enhanceLinks.toIssue({
        searchPageModule: this.searchPageModule
    });

    var $normalCell = jQuery("<td><a href='#' class='issue-link my-link parentIssue' data-issue-key='HSP-1'>test</a></td>");
    var $issueTableFrag = jQuery("<div class='list-view'><table id='issuetable'><tr></tr></table></div>");

    $issueTableFrag.find("tr:first").append($normalCell);


    $issueTableFrag.appendTo("#qunit-fixture");
    $normalCell.find(".my-link").click();

    ok(this.searchPageModule.reset.calledWith({"selectedIssueKey" : "HSP-1"}));
});

test("Links inside of issue table call update", function () {
    JIRA.Issues.enhanceLinks.toIssue({
        searchPageModule: this.searchPageModule
    });

    var $normalCell = jQuery("<td><a href='#' class='issue-link my-link' data-issue-key='HSP-1'>test</a></td>");
    var $issueTableFrag = jQuery("<div class='list-view'><table id='issuetable'><tr></tr></table></div>");

    $issueTableFrag.find("tr:first").append($normalCell);


    $issueTableFrag.appendTo("#qunit-fixture");
    $normalCell.find(".my-link").click();

    ok(this.searchPageModule.update.calledWith({"selectedIssueKey" : "HSP-1"}));
});

test("Links inside dropdown of issue table call update", function () {
    JIRA.Issues.enhanceLinks.toIssue({
        searchPageModule: this.searchPageModule
    });

    var $dropdown = jQuery("<td><a href='#' class='my-dropdown'>dropdown</a><ul class='aui-list'><li><a href='#' class='issue-link my-dropdown-link' data-issue-key='HSP-2'>dropdown test</a></li></ul></td>");
    var $issueTableFrag = jQuery("<div class='list-view'><table id='issuetable'><tr></tr></table></div>");

    $issueTableFrag.find("tr:first").append($dropdown);


    var $issueLink = $dropdown.find(".my-dropdown-link");

    new AJS.Dropdown({
        trigger: $dropdown.find(".my-dropdown"),
        content: $dropdown.find(".aui-list")
    });

    $issueTableFrag.appendTo("#qunit-fixture");
    $dropdown.find(".my-dropdown").click();
    $issueLink.click();

    ok(this.searchPageModule.update.calledWith({"selectedIssueKey" : "HSP-2"}));
});

test("Links inside dropdown outside of issue table call reset", function () {
    JIRA.Issues.enhanceLinks.toIssue({
        searchPageModule: this.searchPageModule
    });

    var $dropdown = jQuery("<td><a href='#' class='my-dropdown'>dropdown</a><ul class='aui-list'><li><a href='#' class='issue-link my-dropdown-link' data-issue-key='HSP-2'>dropdown test</a></li></ul></td>");
    var $issueTableFrag = jQuery("<table id='issuetable'><tr></tr></table>");

    $issueTableFrag.find("tr:first").append($dropdown);


    var $issueLink = $dropdown.find(".my-dropdown-link");

    new AJS.Dropdown({
        trigger: $dropdown.find(".my-dropdown"),
        content: $dropdown.find(".aui-list")
    });

    $issueTableFrag.appendTo("#qunit-fixture");
    $dropdown.find(".my-dropdown").click();
    $issueLink.click();

    ok(this.searchPageModule.reset.calledWith({"selectedIssueKey" : "HSP-2"}));
});