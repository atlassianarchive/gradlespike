AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:sinon");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

module("JIRA.Issues.LayoutPreferenceManager", {
    setup: function () {
        this.metaGetStub = sinon.stub(AJS.Meta, "get");
        this.metaSetStub = sinon.spy(AJS.Meta, "set");
    },

    teardown: function () {
        this.metaGetStub.restore();
        this.metaSetStub.restore();
    }
});

test("getPreferredLayoutKey", function () {
    this.metaGetStub.returns("list-view");
    equal(JIRA.Issues.LayoutPreferenceManager.getPreferredLayoutKey(), "list-view",
        "getPreferredLayout() reads from AJS.Meta");
});

test("setPreferredLayoutKey", function () {
    var fakeRequest = sinon.useFakeXMLHttpRequest(),
        requestMade = false;

    fakeRequest.onCreate = function () {
        requestMade = true;
    };

    JIRA.Issues.LayoutPreferenceManager.setPreferredLayoutKey("split-view");
    ok(requestMade, "setPreferredLayoutKey() makes an AJAX request to persist the user's preferred layout");
    ok(this.metaSetStub.calledWithExactly(JIRA.Issues.LayoutPreferenceManager.PREFERRED_LAYOUT_KEY, "split-view"),
        "serPreferredLayoutKey() writes to AJS.Meta");
});