AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav-query");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");

module("JIRA.Issues.SearchShifter", {
    setup: function() {
        this.isBasicMode = sinon.stub().returns(true);
        this.isFullScreenIssue = sinon.stub().returns(false);
        this.searcherCollection = JIRA.Issues.TestUtils.createSearcherCollection([{
            id: "comment",
            isShown: true,
            groupId: "group",
            groupName: "Group",
            name: "Comment"
        }, {
            id: "reporter",
            isShown: true,
            groupId: "group",
            groupName: "Group",
            name: "Reporter"
        }, {
            id: "resolution-date",
            isShown: false,
            groupId: "group",
            groupName: "Group",
            name: "Resolution Date"
        }]);

        this.searcherDialogInstance = JIRA.Issues.SearcherDialog.instance;
        this.searcherDialogGetCurrentSearcherStub = sinon.stub();
        this.searcherDialogToggleStub = sinon.stub();

        JIRA.Issues.SearcherDialog.instance = {
            getCurrentSearcher: this.searcherDialogGetCurrentSearcherStub,
            toggle: this.searcherDialogToggleStub
        };

        this.searchShifter = JIRA.Issues.SearchShifter({
            isBasicMode: this.isBasicMode,
            isFullScreenIssue: this.isFullScreenIssue,
            searcherCollection: this.searcherCollection
        });
    },

    teardown: function () {
        // Restore the original SearcherDialog instance.
        JIRA.Issues.SearcherDialog.instance = this.searcherDialogInstance;
    }
});

test("getSuggestions()", function () {
    var success = false;

    // Only valid searchers should be suggested.
    this.searchShifter().getSuggestions("").done(function (suggestions) {
        success = _.isEqual(suggestions, [{
            label: "Comment",
            value: "comment"
        }, {
            label: "Reporter",
            value: "reporter"
        }]);
    });

    ok(success, "Returns the correct suggestions.");
});

test("getSuggestions() when not in basic mode", function () {
    this.isBasicMode.returns(false);
    equal(this.searchShifter(), null, "Returns no suggestions.");
});

test("getSuggestions() when a full screen issue is visible", function () {
    this.isFullScreenIssue.returns(true);
    equal(this.searchShifter(), null, "Returns no suggestions.");
});

test("onSelection()", function () {
    var searcherModel = this.searcherCollection.get("comment"),
        searchShifter = this.searchShifter(),
        selectSpy = searcherModel.select = sinon.spy();

    searchShifter.onSelection("comment");
    equal(selectSpy.callCount, 1, "Selects the searcher");
    equal(this.searcherDialogToggleStub.callCount, 1, "Opens the searcher dialog");

    // select shouldn't be called if the searcher is already selected.
    searcherModel.setIsSelected(true);
    searchShifter.onSelection("comment");
    equal(selectSpy.callCount, 1, "Doesn't select searchers that are already selected");
    equal(this.searcherDialogToggleStub.callCount, 2, "Opens the searcher dialog");

    // toggle shouldn't be called if the dialog is already visible.
    this.searcherDialogGetCurrentSearcherStub.returns(searcherModel);
    searchShifter.onSelection("comment");
    equal(selectSpy.callCount, 1, "Doesn't select searchers that are already selected");
    equal(this.searcherDialogToggleStub.callCount, 2, "Keeps the searcher dialog open");
});
