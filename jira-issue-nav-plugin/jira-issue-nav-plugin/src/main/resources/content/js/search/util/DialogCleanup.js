/**
 * Closes any existing dialog when the application is navigation to a new route
 *
 * @param router {JIRA.Issues.IssueNavRouter} Router used by the application
 */
JIRA.Issues.dialogCleaner = function(router) {
    router.on("navigate", function() {
        if (JIRA.Dialog.current) {
            JIRA.Dialog.current.hide();
        }
        if (AJS.HelpTip && AJS.HelpTip.Manager) {
            AJS.HelpTip.Manager.hideSequences();
            AJS.HelpTip.Manager.clearSequences();
        }
    })
};