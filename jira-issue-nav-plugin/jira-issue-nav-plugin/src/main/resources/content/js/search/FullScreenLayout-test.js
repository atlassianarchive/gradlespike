(function(){
    "use strict";

    AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
    AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

    module("JIRA.Issues.FullScreenLayout", {
        setup: function() {
            sinon.stub(JIRA.Issues.Application, "request").withArgs("issueEditor:canDismissComment").returns(true);

            this.fullScreenIssue = JIRA.Issues.TestUtils.mockFullScreenIssue();
            this.fullScreenLayout = new JIRA.Issues.FullScreenLayout({
                search: new JIRA.Issues.MockSearchModule(),
                columnConfig: JIRA.Issues.TestUtils.mockColumnConfig(),
                searchContainer: jQuery("<div class='navigator-content'></div>").appendTo("#qunit-fixture"),
                fullScreenIssue: this.fullScreenIssue
            });
        },

        teardown: function () {
            JIRA.Issues.Application.request.restore();
        }
    });

    test("When the IssueTable triggers 'highlightIssue', it highlights an issue in the SearchService", function(){
        var spy = this.spy(JIRA.Components.SearchService.prototype, "highlightIssue");

        this.fullScreenLayout.issueTable.trigger("highlightIssue", 100);

        sinon.assert.calledOnce(spy);
        sinon.assert.calledWithExactly(spy, 100);
    });

    test("When the IssueTable is rendered, it triggers the 'render' event", function() {
        var onRender = this.spy();
        this.fullScreenLayout.on("render", onRender);

        this.fullScreenLayout.issueTable.trigger("render");

        sinon.assert.calledOnce(onRender);
    });

    test("When the IssueTable is rendered, it binds the SearchService events in FullScreenIssue", function() {
        this.fullScreenLayout.issueTable.trigger("render");

        sinon.assert.calledOnce(this.fullScreenIssue.bindSearchService);
    });

    test("When the layout is rendered, it renders the IssueTable", function() {
        var spy = this.spy(JIRA.Components.IssueTable.prototype, "show");

        this.fullScreenLayout.render();

        sinon.assert.calledOnce(spy);
    });

    test("When closing the Layout, the FullScreenIssue is deactivated", function() {
        this.fullScreenLayout.close();

        sinon.assert.calledOnce(this.fullScreenIssue.deactivate);
    });

    test("When closing the Layout, the IssueTable is closed", function() {
        var spy = this.spy(JIRA.Components.IssueTable.prototype, "close");

        this.fullScreenLayout.close();

        sinon.assert.calledOnce(spy);
    });

    test("When closing the Layout, the SearchService is closed", function() {
        var spy = this.spy(JIRA.Components.SearchService.prototype, "close");

        this.fullScreenLayout.close();

        sinon.assert.calledOnce(spy);
    });

    test("When calling to nextIssue, the next issue is selected in the SearchService", function() {
        var spy = this.spy(JIRA.Components.SearchService.prototype, "selectNextIssue");

        this.fullScreenLayout.nextIssue();

        sinon.assert.calledOnce(spy);
    });

    test("When calling to prevIssue, the previous issue is selected in the SearchService", function() {
        var spy = this.spy(JIRA.Components.SearchService.prototype, "selectPreviousIssue");

        this.fullScreenLayout.prevIssue();

        sinon.assert.calledOnce(spy);
    });

    test("When returning to the search, the issue is unselected in the SearchService", function() {
        var spy = this.spy(JIRA.Components.SearchService.prototype, "unselectIssue");

        this.fullScreenLayout.returnToSearch();

        sinon.assert.calledOnce(spy);
    });
}());
