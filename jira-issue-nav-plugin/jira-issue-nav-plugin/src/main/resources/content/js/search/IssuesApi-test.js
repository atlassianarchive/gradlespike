AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

module("JIRA.Issues.Api", {
    setup: function() {
        var $body = AJS.$("body");
        JIRA.Issues.Application.start();

        this.searchPageModule = JIRA.Issues.TestUtils.mockSearchPageModule();
        this.searchPageModule.registerViewContainers({
            issueContainer: AJS.$("<div class=\"hidden\"></div>").appendTo($body),
            searchContainer: AJS.$("<div></div>").appendTo($body)
        });
        this.sandbox = sinon.sandbox.create();
        this.searchPageModule.changeLayout("list-view", {ajax: false});
        JIRA.Issues.Api.initialize({
            searchPageModule: this.searchPageModule
        });
    },

    teardown: function () {
        this.searchPageModule.issueContainer.remove();
        this.searchPageModule.searchContainer.remove();
        this.sandbox.restore();
    }
});

test("openFocusShifter", function () {
    var preferredLayoutStub = sinon.stub(JIRA.Issues.LayoutPreferenceManager, "getPreferredLayoutKey");
    preferredLayoutStub.returns("list-view");

    this.layoutPreferenceManager = sinon;
    var openFocusShifterStub = sinon.stub(this.searchPageModule, "openFocusShifter");

    JIRA.Issues.Api.openFocusShifter();
    ok(!openFocusShifterStub.called, "Does nothing when no issue is visible.");

    this.searchPageModule.fullScreenIssue.isVisible.returns(true);
    JIRA.Issues.Api.openFocusShifter();
    ok(openFocusShifterStub.called, "Opens the focus shifter when an issue is visible.");

    //Focus shifter should always work in split-view
    preferredLayoutStub.returns("split-view");
    this.searchPageModule.fullScreenIssue.isVisible.returns(false);
    JIRA.Issues.Api.openFocusShifter();
    ok(openFocusShifterStub.called, "Opens the focus shifter in split-view.");

    preferredLayoutStub.restore();
});

test("nextIssue()", function () {
    JIRA.Issues.Api.nextIssue();
    equal(this.searchPageModule.getCurrentLayout().nextIssue.callCount, 1, "Next issue requested");
});

test("prevIssue()", function () {
    // Issue search is visible, so we shouldn't try to load the previous issue.
    JIRA.Issues.Api.prevIssue();
    equal(this.searchPageModule.getCurrentLayout().prevIssue.callCount, 1, "Previous issue requested");
});

test("getSelectedIssueId", function () {
    sinon.stub(this.searchPageModule, "getEffectiveIssueId").returns(42);

    equal(42, JIRA.Issues.Api.getSelectedIssueId(), "Returns the correct issue ID");
    ok(this.searchPageModule.getEffectiveIssueId.called, "SearchPageModule.getEffectiveIssueId called.");
});

test("getSelectedIssueKey", function () {
    sinon.stub(this.searchPageModule, "getEffectiveIssueKey").returns("JRA-123");

    equal("JRA-123", JIRA.Issues.Api.getSelectedIssueKey(), "Returns the correct issue key");
    ok(this.searchPageModule.getEffectiveIssueKey.called, "SearchPageModule.getEffectiveIssueKey called.");
});

test("switchLayouts", function () {
    var changeLayoutSpy = sinon.spy(this.searchPageModule, "changeLayout"),
        options = {ajax: false};

    JIRA.Issues.Api.switchLayouts(options);
    equal(changeLayoutSpy.callCount, 1, "changeLayout was called");
    deepEqual(changeLayoutSpy.lastCall.args, ["split-view", options], "Changed to split view");

    JIRA.Issues.Api.switchLayouts(options);
    equal(changeLayoutSpy.callCount, 2, "changeLayout was called");
    deepEqual(changeLayoutSpy.lastCall.args, ["list-view", options], "Changed to list view");
});

test("isFullScreenIssueVisible", function () {
    var isFullScreenIssueVisible = sinon.stub(this.searchPageModule, "isFullScreenIssueVisible");

    isFullScreenIssueVisible.returns(true);
    var isFullScreen = JIRA.Issues.Api.isFullScreenIssueVisible();
    equal(isFullScreenIssueVisible.callCount, 1, "changeLayout was called");
    equal(isFullScreen, true, "Works when isFullScreenIssueVisible returns true");
    isFullScreenIssueVisible.reset();

    isFullScreenIssueVisible.returns(false);
    isFullScreen = JIRA.Issues.Api.isFullScreenIssueVisible();
    equal(isFullScreenIssueVisible.callCount, 1, "changeLayout was called");
    equal(isFullScreen, false, "Works when isFullScreenIssueVisible returns true");
    isFullScreenIssueVisible.restore();
});

test("getFieldsOnSelectedIssue() with no selected issue", function () {
    strictEqual(JIRA.Issues.Api.getFieldsOnSelectedIssue(), undefined, "not an on issue -> no fields");
});

test("getFieldsOnSelectedIssue() with selected issue", function () {
    this.sandbox.stub(JIRA.Issues.Application, "request")
        .withArgs("issueEditor:fields").returns(new JIRA.Components.IssueEditor.Collections.Fields([
            {id: "customfield_10001"},
            {id: "customfield_10002"}
        ]));

    var fields = [];
    JIRA.Issues.Api.getFieldsOnSelectedIssue().each(function (field) {
        fields.push(field.id);
    });
    deepEqual(fields, ["customfield_10001", "customfield_10002"]);
});


test("editFieldOnSelectedIssue() with non-existent field", function () {
    this.sandbox.stub(JIRA.Issues.Application, "request")
        .withArgs("issueEditor:fields").returns([]);

    JIRA.Issues.Api.editFieldOnSelectedIssue("foobar");

    this.sandbox.stub(JIRA.Issues.Application, "execute");
    ok(!JIRA.Issues.Application.execute.called);
});

test("editFieldOnSelectedIssue() with non-editable field", function () {
    var field,
        fields;

    field = JIRA.Issues.TestUtils.mockIssueFieldModel("id", false, true);
    fields = new Backbone.Collection([field]);

    this.sandbox.stub(JIRA.Issues.Application, "request")
        .withArgs("issueEditor:fields").returns(fields);
    this.sandbox.stub(JIRA.Issues.Application, "execute");

    JIRA.Issues.Api.editFieldOnSelectedIssue("id");
    ok(!JIRA.Issues.Application.execute.called);
});

test("editFieldOnSelectedIssue() with editable field", function () {
    var field,
            fields;

    field = JIRA.Issues.TestUtils.mockIssueFieldModel("id", true, true);
    fields = new Backbone.Collection([field]);

    this.sandbox.stub(JIRA.Issues.Application, "request")
        .withArgs("issueEditor:fields").returns(fields);
    this.sandbox.stub(JIRA.Issues.Application, "execute");

    JIRA.Issues.Api.editFieldOnSelectedIssue("id");

    ok(JIRA.Issues.Application.execute.calledOnce, "Application.execute was called once");
    ok(JIRA.Issues.Application.execute.calledWithExactly("issueEditor:editField", field), "Command 'issueEditor:editField' was executed");
});