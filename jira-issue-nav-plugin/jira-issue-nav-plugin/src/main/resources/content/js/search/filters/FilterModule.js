/**
 * Interface to the filter system.
 */
Backbone.define('JIRA.Issues.FilterModule', JIRA.Issues.BaseEvented.extend({

    initialize: function(options) {
        this._searchPageModule = options.searchPageModule;

        this.filterPanelModel = options.filterPanelModel || new JIRA.Issues.FilterPanelModel({}, {
            searchPageModule: options.searchPageModule
        });

        this.filtersComponent = new JIRA.Components.Filters({
            systemFilters: options.systemFilters,
            searchPageModule: this._searchPageModule
        });
        this.filtersComponent.fetchFavouriteFilters();

        this.filtersComponent.on("newFilter", function(newFilterModel){
            this._searchPageModule.filterModuleSaved(newFilterModel);

            if (JIRA.Issues.Application.request("issueEditor:canDismissComment")) {
                this._searchPageModule.setSessionSearch(newFilterModel);
            }
        }, this);

        this.filtersComponent.on("filterRemoved", function(args){
            this.trigger("filterRemoved", args);
        }, this);

        this.filtersComponent.on("filterSelected", this._onFilterSelected, this);

        this.filtersComponent.on("filterDiscarded", function(){
            this._searchPageModule.discardFilterChanges();
        }, this);

        this.filtersComponent.on("savedFilter", function(filterModel) {
            this._searchPageModule.setSessionSearch(filterModel);
            this.filtersComponent.highlightFilter(filterModel);
        }, this);

        this._searchPageModule.on('change changeFilterProps', function() {
            this.filtersComponent.updateFilterHeader({
                model: this._searchPageModule.getFilter(),
                isDirty: this._searchPageModule.isDirty()
            });
        }, this);

        this.filterPanelModel.bind("change:activeFilter", this._markFilterAsActive, this);
        this.filterPanelModel.set("activeFilter", this._searchPageModule.getFilter());
        this._searchPageModule.bind("change:filter", function(searchPageModule, newFilter) {
            this.filterPanelModel.set("activeFilter", newFilter);
        }, this);
    },

    canEditColumns: function() {
        var filter = this._searchPageModule.getFilter();
        return filter && filter.isMyFilter();
    },

    /**
     * Retrieve system filter information via AJAX.
     * <p/>
     * System filter information is not present on the stand alone view issue page.
     *
     * @return {jQuery.Deferred} a deferred that is resolved after system filter information has been retrieved.
     */
    initSystemFilters: function() {
        return this.filtersComponent.fetchSystemFilters();
    },

    /**
     * Finds the filter with the given id. Attempts to fetch from the server if it does not exist. Returns a promise.
     */
    getFilterById: function(filterId) {
        return this.filtersComponent.getFilterById(filterId);
    },

    /**
     * Creates a FilterPanelView and renders it into the provided elements
     * @param els - a map of elements
     */
    createView: function(els) {
        this.filterPanelView = new JIRA.Issues.FilterPanelView({
            el: els.$filterPanelEl,
            model: this.filterPanelModel,
            searchPageModule: this._searchPageModule,
            easeOff: (!!jQuery.browser.msie && jQuery.browser.version <= 8) ? 500 : 0
        });

        this.filterPanelView.on("renderOpen", function() {
            this.filtersComponent.showSystemFilters(this.filterPanelView.$el.find(".filter-panel-system-container"));
            this.filtersComponent.showFavouriteFilters(this.filterPanelView.$el.find(".filter-panel-favourites-container"));
            this._markFilterAsActive();
            this.filterPanelView.adjustFavouriteFiltersHeight();
        }, this);

        this.filterPanelView.on("filterSelected", this._onFilterSelected, this);

        this.filterPanelView.render();

        return this.filterPanelView;
    },

    _markFilterAsActive: function() {
        var activeFilter = this.filterPanelModel.get("activeFilter");
        this.filtersComponent.highlightFilter(activeFilter);
    },

    toggleFilterPanel: function () {
        this.filterPanelView.toggleDockState();
    },

    createHelptipForFilterPanelDocking: function(weight) {
        var tip;
        var filterPanelModel = this.filterPanelModel;
        var filterPanelView = this.filterPanelView;

        if (filterPanelModel.shouldShowDockIntro() && filterPanelView.$el.is(":visible")) {
            tip = new AJS.HelpTip({
                id: "filters-dock-intro",
                title: AJS.I18n.getText('issue.nav.filters.dock.intro.title'),
                bodyHtml: AJS.I18n.getText('issue.nav.filters.dock.intro.desc'),
                anchor: ".ui-dock",
                isSequence: true,
                weight: weight,
                callbacks: {
                    beforeShow: function() {
                        filterPanelView.$el.popoutSidebar("expand", false);
                    },
                    init: function() {
                        if (filterPanelView.$el.popoutSidebar("isUndocked")) {
                            filterPanelView.$el.popoutSidebar("unbindHoverIntent");
                        }
                    },
                    hide: function() {
                        if (filterPanelView.$el.popoutSidebar("isUndocked")) {
                            filterPanelView.$el.popoutSidebar("bindHoverIntent");
                            filterPanelView.$el.popoutSidebar("collapse");
                        }
                    }
                }
            });
        }
        return tip;
    },

    _onFilterSelected: function(filterId) {
        if (filterId === null) {
            this._searchPageModule.resetToBlank({isNewSearch: true});
        } else {
            this._searchPageModule.resetToFilter(filterId);
        }
    }
}));
