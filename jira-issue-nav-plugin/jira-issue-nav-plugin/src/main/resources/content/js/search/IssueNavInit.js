;(function() {

    if (history && history.pushState === undefined) {

        var hashed = JIRA.Issues.hashTheUrl(window.location);
        if (hashed.changed) {
            window.location = hashed.uri;
        }
    }

    jQuery("html").addClass("hide-until-domready");
    jQuery(function() {
        jQuery("html").removeClass("hide-until-domready");
    });

    // Prevent escape key from cancelling XHR requests in FF
    jQuery(document).on('keydown', function(e) {
        if (e.which === 27) e.preventDefault();
    });

})();

AJS.$(function() {

    var $navigatorContent = AJS.$(".navigator-content");

    /**
     * Read all the initial data in the DOM
     *
     * If the ColumnConfigState has been sent from the server we want to take the HTML from the table
     * and pop it onto its table property.
     *
     * This prevents us from having to populate the HTML twice in the dom. Once in the HTML and another time in the
     * JSON. It also prevents us needing to ensure there are no XSS vulnerabilities in the JSON HTML string.
     */
    var initialIssueTableState = $navigatorContent.data("issue-table-model-state");
    if (initialIssueTableState && !initialIssueTableState.table) {
        var wrapper = AJS.$("<div></div>").append($navigatorContent.children().clone());
        initialIssueTableState.issueTable.table = wrapper.html();
    }

    var initialIssueIds = AJS.$('#stableSearchIds').data('ids');
    var selectedIssue = $navigatorContent.data("selected-issue");

    // jQuery.parseJSON gracefully returns null given an empty string.
    // Would be even nicer if the json was placed in a data- attribute, which jQuery will automatically parse with .data().
    var initialSearcherCollectionState = jQuery.parseJSON(jQuery("#criteriaJson").text());
    var initialSessionSearchState = $navigatorContent.data("session-search-state");
    var systemFilters = jQuery.parseJSON(jQuery("#systemFiltersJson").text());

    JIRA.Issues.Application.start({
        showReturnToSearchOnError: function() {
            return JIRA.Issues.LayoutPreferenceManager.getPreferredLayoutKey() !== "split-view";
        },
        useLog: AJS.Meta.get("dev-mode") === true
    });
    JIRA.Issues.AnalyticsLoader();

    /**
     * Create all our Backbone stuff
     */
    var creator = JIRA.Issues.GlobalIssueNavCreator.create(AJS.$(document), {
        initialIssueTableState: initialIssueTableState,
        initialSearcherCollectionState: initialSearcherCollectionState,
        initialSessionSearchState: initialSessionSearchState,
        initialSelectedIssue: selectedIssue,
        initialIssueIds: initialIssueIds,
        systemFilters: systemFilters
    });

    // We can make Chrome "pretend" to be IE for testing purposes by enabling
    // the ka.I_ROOT dark feature; it will use the /i root and fragment polling.
    Backbone.history.start({
        pushState: !AJS.DarkFeatures.isEnabled("ka.I_ROOT"),
        root: JIRA.Issues.IssueNavRouter.getBackboneRoot()
    });

    // Workaround for Chrome bug firing a null popstate event on page load.
    // Backbone should fix this!
    // @see http://code.google.com/p/chromium/issues/detail?id=63040
    // @see also JRADEV-14804
    if (jQuery.browser.webkit) {
        JIRA.Issues.ignorePopState = true;
        window.addEventListener('load', function() {
            setTimeout(function() {
                JIRA.Issues.ignorePopState = false;
            }, 0);
        });
    }

    AJS.KeyboardShortcut.addIgnoreCondition(function (e, key, ctx) {
        return ctx === "issueaction" && JIRA.Issues.Api.isCurrentlyLoadingIssue();
    });

    /*
    * Some shenanigans to get get table to resize with window gracefully. Sets the width of the issue navigator results
    * wrapper. Keeps the right hand page elements within the browser view when the results table is wider than the browser view.
    */
    var bodyMinWidth = parseInt(jQuery('body').css('minWidth'), 10);
    jQuery(document).bind('resultsWidthChanged', function() {
        var $contained = jQuery('.contained-content');
        var $containedParent = $contained.parent();

        if($contained.length > 0) {
            var containedLeft = $contained.offset().left;
            var windowWidth = jQuery(window).width();
            var target = Math.max(windowWidth, bodyMinWidth) - containedLeft;
            var targetPct = target / $containedParent.width() * 100;
            if (targetPct < 100) {
                $contained.css('width', targetPct + '%');
            } else {
                $contained.css('width', '');
            }
            jQuery(document).trigger('issueNavWidthChanged');
        }
    });

    // Trigger the event on page load to make sure the controls are visible.
    AJS.$(document).trigger("resultsWidthChanged");

    JIRA.Issues.onVerticalResize(function () {
        jQuery.event.trigger("updateOffsets.popout");
    });

    // When switching layouts we need to update the height of sidebar
    JIRA.bind(JIRA.Events.LAYOUT_RENDERED, function () {
        _.defer(function () {
            jQuery.event.trigger("updateOffsets.popout");
        });
    });
});

/**
 * Determines the appropriate message to show upon search ajax failure
 *
 * @param xhr XHR object from jQuery.ajax
 */
JIRA.Issues.displayFailSearchMessage = function(xhr) {
    if (xhr && xhr.statusText !== "abort") {
        return JIRA.Messages.showErrorMsg(JIRA.SmartAjax.buildSimpleErrorContent(xhr), {
            closeable: true
        });
    }
};
