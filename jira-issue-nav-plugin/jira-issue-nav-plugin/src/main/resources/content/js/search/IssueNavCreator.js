(function($, _) {
    // export some events
    JIRA.Events.LAYOUT_RENDERED = "layoutRendered";

    AJS.namespace("JIRA.Issues.IssueNavCreator");

    JIRA.Issues.IssueNavCreator.create = function($el, options) {
        var searchPageModule = this.searchPageModule = new JIRA.Issues.SearchPageModule({}, {
            initialIssueTableState: options.initialIssueTableState
        });
        searchPageModule.registerViewContainers({
            issueContainer: $(".issue-container"),
            searchContainer: $(".navigator-container")
        });

        // Initialise Modules

        var searchHeaderModule = this.searchHeaderModule = new JIRA.Issues.SearchHeaderModule({
            searchPageModule: searchPageModule
        });

        var filterModule = new JIRA.Issues.FilterModule({
            searchPageModule: searchPageModule,
            systemFilters: searchPageModule.addOwnerToSystemFilters(options.systemFilters)
        });

        var queryModule = JIRA.Components.Query.create({
            el: $el.find("form.navigator-search"),
            searchers: options.initialSearcherCollectionState,
            preferredSearchMode: AJS.Meta.get("user.search.mode"),
            layoutSwitcher: true,
            autocompleteEnabled: AJS.params.autocompleteEnabled,
            basicAutoUpdate: AJS.Meta.getBoolean("hasCriteriaAutoUpdate")
        });


        JIRA.bind(JIRA.Events.ISSUE_TABLE_REORDER, function(e) {
            if (!JIRA.Issues.Application.request("issueEditor:canDismissComment")) {
                e.preventDefault();
            }
        });

        this.layoutSwitcherView = new JIRA.Issues.LayoutSwitcherView({
            searchPageModule: searchPageModule
        });
        this.layoutSwitcherView.setElement($el.find("#layout-switcher-toggle")).render();

        var issueModule = JIRA.Issues.Application.request("issueEditor");
        JIRA.Issues.Application.on("issueEditor:render", function(regions) {
            JIRA.Issues.Application.execute("pager:render", regions.pager);
        });
        JIRA.Issues.Application.commands.setHandler("returnToSearch", function() {
            JIRA.Issues.Application.execute("issueEditor:close");
        });

        // Initialize event bubbling
        JIRA.Issues.Application.on("issueEditor:saveSuccess", function (props) {
            JIRA.trigger(JIRA.Events.ISSUE_REFRESHED, [props.issueId]);
        });
        JIRA.Issues.Application.on("issueEditor:saveError", function (props) {
            if (!props.deferred) {
                JIRA.trigger(JIRA.Events.ISSUE_REFRESHED, [props.issueId]);
            }
        });

        JIRA.Issues.FocusShifter.init();

        var viewIssueData = issueModule.viewIssueData;

        var issueSearchManager = new JIRA.Issues.IssueSearchManager({
            initialIssueTableState: options.initialIssueTableState,
            initialIssueIds: options.initialIssueIds
        });

        var searchModule = new JIRA.Issues.SearchModule({
            searchPageModule: searchPageModule,
            queryModule: queryModule,
            issueSearchManager: issueSearchManager,
            initialSelectedIssue: options.initialSelectedIssue
        });

        var issueCacheManager = new JIRA.Issues.Cache.IssueCacheManager({
            searchResults: searchModule.getResults(),
            viewIssueData: viewIssueData
        });

        // TODO TF-693 - FullScreenIssue will detach these elements, so get a reference now before they're not discoverable.
        var issueNavToolsElement = $el.find(".saved-search-selector");
        // TODO TF-693 - Try to prevent FullScreenIssue from hacking and mutilating the DOM so much...
        var fullScreenIssue = new JIRA.Issues.FullScreenIssue({
            issueContainer: searchPageModule.issueContainer,
            searchContainer: searchPageModule.searchContainer,
            issueCacheManager: issueCacheManager
        });

        // Register Modules
        searchPageModule.registerSearch(searchModule);
        searchPageModule.registerSearchHeaderModule(searchHeaderModule);
        searchPageModule.registerFilterModule(filterModule);
        searchPageModule.registerQueryModule(queryModule);

        searchPageModule.registerFullScreenIssue(fullScreenIssue);
        searchPageModule.registerIssueSearchManager( issueSearchManager);
        searchPageModule.registerIssueCacheManager(issueCacheManager);
        searchPageModule.registerLayoutSwitcher(this.layoutSwitcherView);

        searchHeaderModule.registerSearch(searchModule);
        searchHeaderModule.createToolsView(issueNavToolsElement);

        // Router

        var issueNavRouter = this.issueNavRouter = new JIRA.Issues.IssueNavRouter({
            searchPageModule: searchPageModule,
            initialSessionSearchState: options.initialSessionSearchState
        });

        searchPageModule.registerIssueNavRouter(issueNavRouter);

        // Overrides

        JIRA.Issues.enhanceLinks.toIssueNav({
            searchPageModule: searchPageModule
        });

        JIRA.Issues.enhanceLinks.withPushState({
            router: issueNavRouter
        });

        JIRA.Issues.enhanceLinks.toIssue({
            searchPageModule: searchPageModule
        });

        JIRA.Issues.enhanceLinks.transformToAjax();

        JIRA.Issues.dialogCleaner(issueNavRouter);

        JIRA.Issues.Api.initialize({
            searchPageModule: searchPageModule
        });

        JIRA.Issues.IssueAPI.override({
            searchPageModule: searchPageModule
        });

        JIRA.Issues.IssueNavigatorAPI.override({
            searchPageModule: searchPageModule
        });

        /**
         * Used to defer the showing of issue dialogs until all promises are resolved.
         * We use this to ensure the dialog we are opening has the correct data.
         * If we are inline editing the summary then open the edit dialog, we want to be sure that the summary has been
         * updated on the server first, otherwise we will be showing stale data in the edit dialog.
         */
        JIRA.Dialogs.BeforeShowIssueDialogHandler.add(JIRA.Issues.Api.waitForSavesToComplete);

        JIRA.Issues.overrideIssueDialogs({
            getIssueId: _.bind(searchPageModule.getEffectiveIssueId, searchPageModule),
            isNavigator: true,
            updateIssue: function(dialog) {
                var issueUpdate = JIRA.Issues.Utils.getUpdateCommandForDialog(dialog);
                return searchPageModule.updateIssue(issueUpdate);
            }
        });

        // Keyboard shortcuts ?

        $(document).keydown(function (e) {
            var dialogIsVisible = $("div.aui-blanket").length > 0,
                wasSupportedKey = (e.which === $.ui.keyCode.ENTER || e.which === $.ui.keyCode.LEFT ||
                    e.which === $.ui.keyCode.UP || e.which === $.ui.keyCode.RIGHT || e.which === $.ui.keyCode.DOWN);

            if (!dialogIsVisible && wasSupportedKey) {
                var target = $(e.target),
                    targetIsValid = target.is(":not(:input)");

                if (target == undefined || targetIsValid) {
                    if (e.which === $.ui.keyCode.ENTER) {
                        if (target == undefined || target.is(":not(a)")) {
                            JIRA.Issues.Api.viewSelectedIssue();
                        }
                    } else if (e.which === $.ui.keyCode.LEFT) {
                        searchPageModule.handleLeft();
                    } else if (e.which === $.ui.keyCode.RIGHT) {
                        searchPageModule.handleRight();
                    } else if (e.which === $.ui.keyCode.UP) {
                        if (searchPageModule.handleUp()) {
                            e.preventDefault();
                        }
                    } else if (e.which === $.ui.keyCode.DOWN) {
                        if (searchPageModule.handleDown()) {
                            e.preventDefault();
                        }
                    }
                }
            }
        });

        // Not such a crash hot idea; should remove it.
        this.searchResults = searchModule.getResults();

        // Create the on change bindings for updating the login link.
        this.searchPageModule.on("change", changeLoginUrl);
        this.searchResults.on("change", changeLoginUrl);
        this.searchResults.getSelectedIssue().on("change", changeLoginUrl);

        return this;
    };

    //Change the login url to the current state.
    function changeLoginUrl() {
        var url = JIRA.Issues.LoginUtils.redirectUrlToCurrent();
        $('.login-link').attr('href', url);
    }


    AJS.namespace("JIRA.Issues.GlobalIssueNavCreator");

    JIRA.Issues.GlobalIssueNavCreator.create = function($el, options) {
        // TODO TF-693 - FullScreenIssue will detach these elements, so get a reference now before they're not discoverable.
        var $filterPanelEl = $el.find(".navigator-sidebar");
        var $filterHeaderEl = $el.find("#search-header-view");

        // Construct base issue navigator
        var issuenav = JIRA.Issues.IssueNavCreator.create($el, options);

        // Create filter modules' views

        var filterModule = issuenav.searchPageModule.filterModule; // HACK: I'm reaching inside the issuenav because the filter module is depended on by base behaviour of the whole thing.

        filterModule.createView({
            $filterPanelEl: $filterPanelEl
        });
        filterModule.filtersComponent.showFilterHeader({
            el: $filterHeaderEl,
            model: issuenav.searchPageModule.getFilter(),
            isDirty: issuenav.searchPageModule.isDirty()
        });

        // Find the quickSearch message (if any) and convert it to a JIRA.Message
        var quickSearchMessaage = $("#quicksearch-message");
        if (quickSearchMessaage.length) {
            var $container = JIRA.Messages.showWarningMsg(quickSearchMessaage.find("p").html(), {
                closeable: true
            });
            $("a", $container).click( function() {
                $container.remove();
            });
        }

        return issuenav;
    };

})(AJS.$, _);
