AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

//test url hashtag and push state handling for non-pushstate browsers.
module('JIRA.Issues.IssueNavInitTest');

test("test redirect from pushState URL function", function() {
    var hashed = JIRA.Issues.hashTheUrl("http://localhost:2990/jira/issues/");
    equal(hashed.changed, false, "base url is not redirected");

    var hashed = JIRA.Issues.hashTheUrl("http://localhost:2990/jira/issues/#?jql=''");
    equal(hashed.changed, false, "pushstate url is not redirected");

    var hashed = JIRA.Issues.hashTheUrl("http://localhost:2990/jira/issues/?jql=''");
    equal(hashed.changed, true, "url is not redirected");
    equal(hashed.uri, "http://localhost:2990/jira/issues/#?jql=''", "hash added");
});
