

Backbone.define("JIRA.Issues.FullScreenIssue", JIRA.Issues.BaseView.extend({

    namedEvents: ["issueHidden"],

    initialize: function (options) {
        _.extend(this, options);
        _.bindAll(this,
            "onLoadComplete",
            "showError");

        if (this.issueContainer.hasClass("hidden")) {
            this.issueContainer.detach();
            this.issueContainer.removeClass("hidden");
        }

        if (this.searchContainer.hasClass("hidden")) {
            this.searchContainer.detach();
            this.searchContainer.removeClass("hidden");
        }
    },

    bindSearchService: function(searchService) {
        this.listenTo(searchService, {
            "issueUpdated": function(issueId, entity, reason) {
                if (reason.action !== "inlineEdit" && reason.action !== "rowUpdate") {
                    return this.updateIssue(reason);
                }
            },
            "selectedIssueChanged": function(selectedIssue) {
                if (selectedIssue.hasIssue()) {
                    JIRA.Issues.Application.execute("issueEditor:abortPending");
                    //TODO Why do we need to debounce this?
                    //JIRA.Issues.Utils.debounce(this, "_loadIssue", issue);
                    this.show({
                        id: selectedIssue.getId(),
                        key: selectedIssue.getKey(),
                        pager: searchService.getPager()
                    });
                    this.updatePager(searchService.getPager());
                } else {
                    if (this.isVisible()) {
                        this.hide();
                        JIRA.trace("jira.returned.to.search");
                        JIRA.trace("jira.psycho.returned.to.search");
                        JIRA.Issues.Application.execute("analytics:trigger", 'kickass.returntosearch');
                    }
                    //TODO Why the full $navigatorContent is marked as pending?
                    //this.$navigatorContent.removeClass("pending");
                }
            }
        });
    },

    deactivate: function () {
        this.stopListening();
        if (this.active) {
            if (this.stalker) {
                this.stalker.unstalk();
            }
            JIRA.Issues.Application.off("issueEditor:loadComplete", this.onLoadComplete);
            JIRA.Issues.Application.off("issueEditor:loadError", this.showError);
            JIRA.Issues.Application.execute("issueEditor:abortPending");
            this.active = false;
        }
    },

    activate: function () {
        if (!this.active) {
            JIRA.Issues.Application.on("issueEditor:loadComplete", this.onLoadComplete);
            JIRA.Issues.Application.on("issueEditor:loadError", this.showError);
            this.active = true;
        }
    },

    onLoadComplete: function (model, props) {
        this.stalker = AJS.$(".js-stalker", this.issueContainer).stalker();
        this._makeIssueVisible();
        if (props.isNewIssue) {
            this._scrollToTop();
        }
    },

    _makeIssueVisible: function () {
        JIRA.Issues.Application.execute("issueEditor:beforeShow");
        this._setBodyClasses({
            error: false,
            issue: true,
            search: true
        });

        if (!this.isVisible()) {
            this.issueContainer.insertBefore(this.searchContainer);
            this.searchContainer.detach();

            JIRA.trace("jira.psycho.issue.refreshed", {id: JIRA.Issues.Application.request("issueEditor:getIssueId")});
            JIRA.trigger(JIRA.Events.NEW_CONTENT_ADDED, [this.issueContainer, JIRA.CONTENT_ADDED_REASON.pageLoad]);
        }
    },

    /**
     * Refresh the visible issue in response to an issue update.
     *
     * @param {object} issueUpdate An issue update object (see <tt>JIRA.Issues.Utils.getUpdateCommandForDialog</tt>).
     * @return {jQuery.Deferred} A deferred that is resolved after the issue has been refreshed.
     */
    updateIssue: function (issueUpdate) {
        var deferred,
            isVisibleIssue;

        isVisibleIssue = issueUpdate.key == JIRA.Issues.Application.request("issueEditor:getIssueKey");

        if (this.isVisible()) {
            deferred =  JIRA.Issues.Application.request("issueEditor:refreshIssue", issueUpdate);
            deferred.done(function () {
                if (!isVisibleIssue && issueUpdate.message) {
                    JIRA.Issues.showNotification(issueUpdate.message, issueUpdate.key);
                }
            });
        } else {
            deferred = jQuery.Deferred().resolve().promise()
        }

        return deferred;
    },

    /**
     * Show error message for loading issue
     */
    showError: function() {
        this._setBodyClasses({
            error: true,
            issue: false,
            search: false
        });
    },

    /**
     * @return {boolean} whether an issue is visible.
     */
    isVisible: function () {
        return this.issueContainer.closest("body").length > 0;
    },

    /**
     * Scroll to the top of the window.
     *
     * @private
     */
    _scrollToTop: function () {
        AJS.$(window).scrollTop(0);
    },

    _setBodyClasses: function (options) {
        AJS.$("body")
                .toggleClass("page-type-message", options.error)
                .toggleClass("navigator-issue-only", options.issue)
                .toggleClass("page-type-navigator", options.search);
    },

    hide: function () {
        this._setBodyClasses({
            error: false,
            issue: false,
            search: true
        });

        if (this.isVisible()) {
            this.searchContainer.insertBefore(this.issueContainer);
            this.issueContainer.detach();
            this.trigger("issueHidden");
        }
    },

    updatePager: function (pager) {
        JIRA.Issues.Application.execute("pager:update", pager);
    },

    /**
     * Load and show an issue.
     *
     * @param {object} issue The issue to show.
     * @param {number} issue.id The issue's ID.
     * @param {string} issue.key The issue's key.
     * @return {jQuery.Deferred} A deferred that is resolved when the issue is visible.
     */
    show: function (issue) {
        this.activate();
        JIRA.Issues.Application.execute("issueEditor:setContainer",this.issueContainer);
        return JIRA.Issues.Application.request("issueEditor:loadIssue",issue).always(_.bind(function() {
            AJS.$(".js-stalker", this.issueContainer).stalker();

            this.updatePager(issue.pager);
            // now is a good time to pre-fetch anything that needs to be pre-fetched
            this.issueCacheManager.prefetchIssues();
        }, this));
    }
}));