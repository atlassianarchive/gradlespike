/**
 * Interface to the search header
 */
Backbone.define('JIRA.Issues.SearchHeaderModule', JIRA.Issues.BaseEvented.extend({

    initialize: function(options) {
        this._searchPageModule = options.searchPageModule;
    },

    registerSearch: function (search) {
        this._search = search;
    },

    createToolsView: function($toolsEl) {
        new JIRA.Issues.IssueTableHeaderOperationsView({
            el: $toolsEl,
            search: this._search,
            searchPageModule: this._searchPageModule
        });
    }
}));