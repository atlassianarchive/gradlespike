Backbone.define("JIRA.Issues.EmptyResultsView", JIRA.Issues.BaseView.extend({

    initialize: function (options) {
        this.searchResults = options.searchResults;
    },

    render: function () {
        var message;
        var hint;
        var cssClass;
        var linkType;
        var createIssuePerm = JIRA.Issues.UserParms.get().createIssue;

        if (!JIRA.Issues.LoginUtils.isLoggedIn()) {
            message = AJS.I18n.getText('issuenav.results.none.found');
            hint = AJS.I18n.getText('issuenav.results.none.hint.login');
            cssClass = "not-logged-in-message";
            linkType = 'login';
        } else if (this.searchResults.getJiraHasIssues() === false) {
            message = AJS.I18n.getText('issuenav.results.none.created');
            hint = createIssuePerm ? AJS.I18n.getText('issuenav.results.none.hint.firsttocreate') : null;
            cssClass = "empty-results-message";
            linkType = 'create';
        } else {
            message = AJS.I18n.getText('issuenav.results.none.found');
            hint = createIssuePerm ?
                    AJS.I18n.getText('issuenav.results.none.hint.modifyorcreate') :
                    AJS.I18n.getText('issuenav.results.none.hint.modify');
            cssClass = "no-results-message";
            linkType = 'create';
        }

        this.$el.addClass("empty-results");
        this.$el.html(JIRA.Templates.IssueNavTable.noResults({
            message: message,
            hint: hint,
            cssClass: cssClass
        }));

        // Make links within the hint work
        var $links = this.$('.no-results-hint a');
        switch (linkType) {
            case 'create':
                $links.addClass('create-issue').attr('href', AJS.contextPath() + "/secure/CreateIssue!default.jspa");
                break;
            case 'login':
                $links.attr('href', JIRA.Issues.LoginUtils.redirectUrlToCurrent()).addClass('login-link');
                break;
        }

        _.defer(jQuery.event.trigger, 'updateOffsets.popout');
        _.defer(JIRA.trace, 'jira.search.finished');
    }
}));