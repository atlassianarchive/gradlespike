;(function() {
    /**
     * Updates a <tt>ViewIssueData</tt> issue cache in response to changes in a <tt>SearchResults</tt> instance.
     * <p/>
     * This includes removing deleted issues from the cache and pre-fetching issues that are likely to be requested.
     */
    Backbone.define('JIRA.Issues.Cache.IssueCacheManager', JIRA.Issues.BaseEvented.extend({
        /**
         * @param {object} options
         * @param {JIRA.Issues.SearchResults} options.searchResults The application's <tt>SearchResults</tt> instance.
         * @param {JIRA.Components.IssueViewer.Legacy.ViewIssueData} options.viewIssueData The application's <tt>ViewIssueData</tt> instance.
         */
        initialize: function (options) {
            this.searchResults = options.searchResults;
            this.viewIssueData = options.viewIssueData;

            this.searchResults.bind("issueDeleted", this._onIssueDeleted, this);
            this.searchResults.bind("nextIssueSelected", this._onNextIssueSelected, this);
            this.searchResults.bind("prevIssueSelected", this._onPrevIssueSelected, this);

            this._prefetchCandidate = null;
        },

        /**
         * Triggers pre-fetching of issues that this IssueCacheManager sees fit to pre-fetch. Calling this method is
         * essentially a hint to the IssueCacheManager that now is a good time to fetch things from the server.
         */
        prefetchIssues: function() {
            if (this._prefetchCandidate && this._prefetchCandidate.key) {
                this._prefetchIssue(this._prefetchCandidate.key);
                this._prefetchCandidate = null;
            }
        },

        scheduleIssueToBePrefetched: function(issue) {
            this._prefetchCandidate = issue;
        },

        /**
         * Remove an issue from the cache in response to its deletion.
         *
         * @param {object} issue
         * @param {number} issue.id The issue's ID.
         * @param {string} issue.key The issue's key.
         * @private
         */
        _onIssueDeleted: function (issue) {
            this.viewIssueData.remove(issue.key);
        },

        /**
         * Handles a SearchResults "nextIssueSelected" event.
         *
         * @param event the event
         * @param event.selected the currently selected issue in the search results
         * @param event.next the next issue in the search results
         */
        _onNextIssueSelected: function(event) {
            // mark the issue just after the selected one as a candidate for pre-fetching
            this._prefetchCandidate = event.nextNextIssue;
        },

        /**
         * Handles a SearchResults "prevIssueSelected" event.
         *
         * @param event the event
         * @param event.selected the currently selected issue in the search results
         * @param event.next the next issue in the search results
         */
        _onPrevIssueSelected: function(event) {
            // mark the issue just previous to the selected one as a candidate for pre-fetching
            this._prefetchCandidate = event.prevPrevIssue;
        },

        /**
         * Pre-fetches an issue by getting it from the cache.
         */
        _prefetchIssue: function (issueKey) {
            if (!JIRA.Issues.DarkFeatures.NO_PREFETCH.enabled()) {
                this.viewIssueData.get(issueKey, false, {prefetch: true});
            }
        }
    }))
})();
