(function($) {
    AJS.namespace("JIRA.Issues.Browser");

    JIRA.Issues.Browser = {
        getWindowWidth: function () {
            return $(window).width();
        }
    }
}(AJS.$));