;(function() {
    var _getIssueId, _isNavigator, _updateIssue;

    /**
     * Patch issue dialogs with KickAss functionality.
     *
     * @param {object} options
     * @param {function} options.getIssueId Retrieves the current issue's ID.
     * @param {boolean} options.isNavigator Whether we're running issue search.
     * @param {function} options.updateIssue Update after a dialog closes.
     */
    JIRA.Issues.overrideIssueDialogs = function (options) {
        options = _.defaults({}, options, {
            isNavigator: false
        });

        _getIssueId = options.getIssueId;
        _isNavigator = !!options.isNavigator;
        _updateIssue = options.updateIssue;

        if (_isNavigator) {

            // JRADEV-14888
            // Labels dialog overrode the handleSubmitResponse handler to update labels without refreshing page
            // We do that anyway so we can just use the standard _handlerSubmitResponse.
            JIRA.LabelsDialog.prototype._handleSubmitResponse = JIRA.FormDialog.prototype._handleSubmitResponse;
        }

        // Override handlers for the Quick-Create dialog
        // Have to do it on DOM ready because that's how it's done in JIRA Core's IssueNavigator file.
        AJS.$(function() {
            var oldIssueNavApi = JIRA.IssueNavigator.Shortcuts; // TODO TF-711: Replace with AMD module call, or better still, delete old issue nav API.
            // Monkey patch quick edit
            JIRA.unbind("QuickEdit.sessionComplete", oldIssueNavApi._quickEditSessionCompleteHandler);
            JIRA.bind("QuickEdit.sessionComplete", function () {
                _updateIssue(JIRA.Dialog.current);
            });

            // Monkey patch quick create subtask
            JIRA.unbind("QuickCreateSubtask.sessionComplete", oldIssueNavApi._quickCreateSubtaskSessionCompleteHandler);
            JIRA.bind("QuickCreateSubtask.sessionComplete", function () {
                _updateIssue(JIRA.Dialog.current);
            });
        });

        // Store the issue we opened the dialog on so we can compare if we are on the same issue (hence refresh) when
        // we submit dialog.
        JIRA.bind("Dialog.show", function (e, $el, instance) {
            if (instance) {
                instance.issueId = _getIssueId(JIRA.Dialog.current);
            }
        });

        // Monkey patch issue transitions
        // atl_token is not written into the template onto the server for some reason, so ensure it exists on render
        JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function(e, $el) {
            $el.find(".issueaction-workflow-transition").each(function() {
                var $this = AJS.$(this);
                var href = $this.attr("href");
                href = href.replace(/atl_token=[^&]*/, "atl_token=" + atl_token());
                $this.attr("href", href);
            });
        });

        // Monkey patch the "Log In" link to make it redirect correctly. Internally, the API generates this link using
        // whatever URL was requested: in this case, that's the API and the user is redirected to a page-full of JSON.
        JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function(e, $el) {
            var $loginLink = AJS.$("#ops-login-lnk");
            if ($loginLink.length == 0) {
                return;
            }

            var location = window.location;
            var uriComponents = parseUri($loginLink.attr("href"));
            uriComponents.queryKey.os_destination = location.pathname.slice(contextPath.length) + location.search;
            $loginLink.attr("href", uriComponents.path + "?" + AJS.$.param(uriComponents.queryKey));
        });


    };

    // Override the default onDialogFinished callback that hides the dialog and
    // reloads the page. For non-issue dialogs, we just defer to the old one.
    var oldGetDefaultOptions = JIRA.FormDialog.prototype._getDefaultOptions;
    JIRA.FormDialog.prototype._getDefaultOptions = function() {
        var options = oldGetDefaultOptions.call(this),
            oldOnDialogFinished = options.onDialogFinished;

        options.onDialogFinished = function() {
            if (this.isIssueDialog()) {
                onDialogFinished.apply(this, arguments);
            } else {
                oldOnDialogFinished.apply(this, arguments);
            }
        };

        return options;
    };

    // Monkey patch _performRedirect to not actually redirect at all.
    var oldPerformRedirect = JIRA.FormDialog.prototype._performRedirect;
    JIRA.FormDialog.prototype._performRedirect = function() {
        if (this.isIssueDialog()) {
            onDialogFinished.apply(this, arguments);
        } else {
            oldPerformRedirect.apply(this, arguments);
        }
    };

    function onDialogFinished() {
        // JRADEV-11573
        if (this.options.id === "clone-issue-dialog") {
            // This works for standalone only, JRADEV-11618 to fix for splitview
            oldPerformRedirect.apply(this, arguments);
        } else if (this.options.id === "delete-issue-dialog") {
            if (_isNavigator) {
                this.hide();
                _updateIssue(this);
            } else {
                // JRADEV-10933
                var redirectUrl = this._getTargetUrlValue();
                if (redirectUrl && redirectUrl !== "") {
                    AJS.reloadViaWindowLocation(redirectUrl);
                } else if (AJS.$(".page-navigation #next-issue").length > 0) {
                    AJS.reloadViaWindowLocation(AJS.$(".page-navigation #next-issue").attr("href"));
                } else if (AJS.$(".page-navigation #return-to-search").length > 0) {
                    AJS.reloadViaWindowLocation(AJS.$(".page-navigation #return-to-search").attr("href"));
                } else {
                    AJS.reloadViaWindowLocation(AJS.$("#find_link").attr("href"));
                }
            }
        } else {
            var instance = this;
            this.showFooterLoadingIndicator();
            _updateIssue(this).always(function() {
                JIRA.trace("jira.issue.update");
                instance.hideFooterLoadingIndicator();
                instance.hide();
            });
        }
    }
})();
