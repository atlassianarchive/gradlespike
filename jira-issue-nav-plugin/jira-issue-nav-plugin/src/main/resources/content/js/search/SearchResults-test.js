AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:sinon");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");


module("JIRA.Issues.SearchResults", {
    setup: function () {
        this.canDismissCommentStub = sinon.stub(JIRA.Issues.Application, "request");
        this.canDismissCommentStub.returns(true);

        this.createSearchResults = function (attr) {
            return JIRA.Issues.TestUtils.mockSearchResults(attr);
        };
        this.createDefaultSearchResults = function (opts) {
            opts = opts || {};

            var totalIssues = opts.totalIssues || opts.issues || 12;
            var stableSearchIssues = ("issues" in opts) ? _.range(1, Math.max(1, opts.issues + 1)) : _.range(1, 10);

            return this.createSearchResults({
                startIndex: 0,
                issueIds: stableSearchIssues,
                total: totalIssues,
                pageSize: opts.pageSize || 3
            });
        };

    },
    teardown: function () {
        this.canDismissCommentStub.restore();
    }
});

test("getStartIndexForPage returns correct page", function () {
    var searchResults = this.createSearchResults({pageSize: 10});
    equal(10, searchResults.getStartIndexForPage(1));
    equal(20, searchResults.getStartIndexForPage(2));
    searchResults = this.createSearchResults({ pageSize: 20});
    equal(20, searchResults.getStartIndexForPage(1));
    equal(40, searchResults.getStartIndexForPage(2));
});

test("hasIssue returns correct value", function () {
    var searchResults = this.createSearchResults({pageSize: 2, issueIds: [1, 2, 3, 4, 5]});
    ok("Expected issue not to exist on page", !searchResults.hasIssue(10));
    ok("Expected issue to exist on page", searchResults.hasIssue(1));
    ok("Expected issue to exist on page", searchResults.hasIssue(5));
});

test("getPager returns valid pager", function () {

    var searchResults = this.createDefaultSearchResults();

    searchResults.selectIssueById(4);

    var pager = searchResults.getPager();

    equal(pager.position, 4, "Issue in the pager should be at the correct position");
    equal(pager.resultCount, 12, "Result count on the pager should reflect the correct issues count");
    equal(pager.nextIssue.id, 5, "The next issue should point to the correct issue");
    equal(pager.previousIssue.key, "HSP-3", "The prev issue should point to the correct issue");

    searchResults.selectIssueById(1);

    pager = searchResults.getPager(1);

    equal(pager.position, 1, "Issue in the pager should be at the correct position");
    equal(pager.resultCount, 12, "Result count on the pager should reflect the correct issues count");
    equal(pager.nextIssue.key, "HSP-2", "The next issue should point to the correct issue");
    equal(pager.previousIssue, undefined, "The prev issue should be undefined");

    searchResults.selectIssueById(9);

    pager = searchResults.getPager();

    equal(pager.position, 9, "Issue in the pager should be at the correct position");
    equal(pager.resultCount, 12, "Result count on the pager should reflect the correct issues count");
    equal(pager.nextIssue, undefined, "The next issue should be undefined");
    equal(pager.previousIssue.id, 8, "The prev issue should point to the correct issue");
});

test("getNumberOfPages gives correct number of pages", function () {
    var searchResults = this.createDefaultSearchResults();
    equal(searchResults.getNumberOfPages(), 2);
    searchResults = this.createDefaultSearchResults({issues: 50,pageSize: 2});
    equal(searchResults.getNumberOfPages(), 24);
    searchResults = this.createDefaultSearchResults({issues: 0,pageSize: 1});
    equal(searchResults.getNumberOfPages(), -1);
    searchResults = this.createDefaultSearchResults({issues: 50,pageSize: 50});
    equal(searchResults.getNumberOfPages(), 0);
});

test("Removing highlighted issue, highlights next issue if there is one", function () {
    var searchResults = this.createDefaultSearchResults({issues: 3});
    searchResults.highlightIssueById(1);
    searchResults.removeIssue(1);
    equal(searchResults.getHighlightedIssue().getId(), 2);
    searchResults.removeIssue(2);
    equal(searchResults.getHighlightedIssue().getId(), 3);
    searchResults.removeIssue(3);
    equal(searchResults.getIssueIds().length, 0);
});

test('Issue is deleted if issueUpdate is delete', function () {
    var deleteSpy = sinon.spy();
    var issueUpdatedSpy = sinon.spy();

    var searchResults = this.createDefaultSearchResults({issues: 3});

    searchResults.bindIssueDeleted(deleteSpy);
    searchResults.bindIssueUpdated(issueUpdatedSpy);

    searchResults.updateIssue({
        id: 1,
        action: JIRA.Issues.Actions.DELETE
    });

    ok(!searchResults.hasIssue(1));
    equal(issueUpdatedSpy.callCount, 0);
    equal(deleteSpy.callCount, 1);
});

test('Issue update issueUpdate goes to server', function () {
    var deleteSpy = sinon.spy();
    var issueUpdatedSpy = sinon.spy();
    var searchResults = this.createDefaultSearchResults({issues: 3});

    searchResults.bindIssueDeleted(deleteSpy);
    searchResults.onIssueUpdated(issueUpdatedSpy);

    searchResults.updateIssue({
        id: 1
    });

    ok(searchResults.hasIssue(1));
    equal(searchResults._issueSearchManager.getRowsForIds.callCount, 1);
    equal(issueUpdatedSpy.callCount, 1);
    equal(deleteSpy.callCount, 0);
});

test('UpdateIssueById calls updateIssue', function () {
    var filter = sinon.spy();
    var searchResults = this.createDefaultSearchResults({issues: 3});
    searchResults.updateIssue = sinon.spy();

    searchResults.updateIssueById({id: 123, action: JIRA.Issues.Actions.UPDATE}, {filter: filter});

    ok(searchResults.updateIssue.calledWith({id: 123, action: JIRA.Issues.Actions.UPDATE}, {filter: filter}));
});

test("Selecting Issue By Key", function () {
    var issueSelectedSpy = sinon.spy(),
        issueDoesNotExistSpy = sinon.spy();
    var searchResults = this.createDefaultSearchResults();
    searchResults.onSelectedIssueChange(function (model) {
        if (model.hasIssue()) {
            issueSelectedSpy(model);
        }
    });
    searchResults.bind("issueDoesNotExist", issueDoesNotExistSpy);

    searchResults.selectIssueByKey("HSP-3");
    equal(searchResults.getSelectedIssue().getId(), 3);
    searchResults.selectIssueByKey("HSP-3");
    equal(searchResults.getSelectedIssue().getId(), 3);
    equal(issueSelectedSpy.callCount, 1, "Selecting same issue doesn't fire another event");

    searchResults.selectIssueByKey("MON-10");
    equal(searchResults.getSelectedIssue().getId(), -1);
    equal(searchResults.getSelectedIssue().getKey(), "MON-10");
    equal(searchResults.getHighlightedIssue().getId(), null);
    equal(searchResults.getHighlightedIssue().getKey(), null);
    ok(issueDoesNotExistSpy.calledOnce, "issueDoesNotExist Event triggered");

    searchResults.selectIssueByKey("HSP-4");
    equal(searchResults.getSelectedIssue().getKey(), "HSP-4");
    equal(searchResults.getSelectedIssue().getId(), 4);

    searchResults.selectIssueByKey(null);
    equal(searchResults.getSelectedIssue().getKey(), null, "Selecting null issue key should remove selection");
    equal(searchResults.getSelectedIssue().getId(), null, "Selecting null issue key should remove selection");
});

test("Highlighting issue sets correct start index", function () {
    var searchResults = this.createDefaultSearchResults({issues: 50});
    searchResults.highlightIssueById(49);
    equal(searchResults.getStartIndex(), 48);
    searchResults.highlightIssueById(10);
    equal(searchResults.getStartIndex(), 9);
    searchResults.highlightIssueById(1);
    equal(searchResults.getStartIndex(), 0);
    searchResults.highlightIssueById(25);
    equal(searchResults.getStartIndex(), 24);
});

test("Highlighting first in page", function () {
    var searchResults = this.createDefaultSearchResults({issues: 10, pageSize: 5});
    searchResults.highlightIssueById(10);
    searchResults.highlightFirstInPage();
    equal(searchResults.getHighlightedIssue().getId(), 6);
    ok(!searchResults.isFirstIssueHighlighted());
    searchResults.highlightIssueById(3);
    searchResults.highlightFirstInPage();
    equal(searchResults.getHighlightedIssue().getId(), 1);
    ok(searchResults.isFirstIssueHighlighted());
});

test("Highlighting next and previous across page boundaries", function () {
    var searchResults = this.createDefaultSearchResults({issues: 50, pageSize: 5});
    searchResults.resetFromSearch(searchResults.toJSON());
    equal(searchResults.getHighlightedIssue().getId(), 1);
    searchResults.highlightNextIssue();
    equal(searchResults.getHighlightedIssue().getId(), 2);
    searchResults.highlightNextIssue();
    equal(searchResults.getHighlightedIssue().getId(), 3);
    searchResults.highlightNextIssue();
    equal(searchResults.getHighlightedIssue().getId(), 4);
    searchResults.highlightNextIssue();
    equal(searchResults.getHighlightedIssue().getId(), 5);
    equal(searchResults.getStartIndex(), 0);
    searchResults.highlightNextIssue();
    equal(searchResults.getHighlightedIssue().getId(), 6);
    equal(searchResults.getStartIndex(), 5);
    searchResults.highlightPrevIssue();
    equal(searchResults.getHighlightedIssue().getId(), 5);
    equal(searchResults.getStartIndex(), 0);
});

test("highlighted roof/floor", function () {
    var searchResults = this.createDefaultSearchResults({issues: 4, pageSize: 2});
    searchResults.highlightIssueById(1);
    searchResults.highlightPrevIssue();
    searchResults.highlightPrevIssue();
    equal(searchResults.getHighlightedIssue().getId(), 1);
    searchResults.highlightNextIssue();
    searchResults.highlightNextIssue();
    searchResults.highlightNextIssue();
    searchResults.highlightNextIssue();
    searchResults.highlightNextIssue();
    searchResults.highlightNextIssue();
    searchResults.highlightNextIssue();
    searchResults.highlightNextIssue();
    searchResults.highlightNextIssue();
    equal(searchResults.getHighlightedIssue().getId(), 4);
});

test("Detects if first issue is selected", function() {
    var searchResults = this.createDefaultSearchResults({issues: 4, pageSize: 2});
    searchResults.selectIssueById(1);
    ok(searchResults.isFirstIssueSelected());
    searchResults.selectNextIssue();
    ok(!searchResults.isFirstIssueSelected());
});

test("selected roof/floor", function () {
    var searchResults = this.createDefaultSearchResults({issues: 4, pageSize: 2});
    searchResults.selectIssueById(1);
    searchResults.selectPrevIssue();
    searchResults.selectPrevIssue();
    equal(searchResults.getSelectedIssue().getId(), 1);
    searchResults.selectNextIssue();
    searchResults.selectNextIssue();
    searchResults.selectNextIssue();
    searchResults.selectNextIssue();
    searchResults.selectNextIssue();
    searchResults.selectNextIssue();
    searchResults.selectNextIssue();
    equal(searchResults.getHighlightedIssue().getId(), 4);
});

test("Going to page highlights first issue on page", function () {
    var searchResults = this.createDefaultSearchResults();
    searchResults.goToPage(4);
    equal(searchResults.getHighlightedIssue().getId(), 5);
    searchResults.goToPage(0);
    equal(searchResults.getHighlightedIssue().getId(), 1);
});

test("Reset from search, clears and updates correct properties", function () {
    var searchResults = this.createDefaultSearchResults();
    searchResults.selectIssueById(4);
    searchResults.resetFromSearch({
        issueIds: [1,2,3],
        sortBy: "summary:DESC"
    });
    equal(searchResults.getSelectedIssue().getId(), null, "Selected issue should be set to null on new search");
    equal(searchResults.getSelectedIssue().getKey(), null, "Selected issue should be set to null on new search");
    equal(searchResults.getHighlightedIssue().getId(), 1, "Highlighted issue should be set to first issue on new search");
    equal(searchResults.getHighlightedIssue().getKey(), "HSP-1", "Highlighted issue should be set to first issue on new search");
});

test("Deleting an issue removes it from the cache", function () {
    var issueCacheManager,
        searchResults = this.createDefaultSearchResults(),
        viewIssueData = {remove: sinon.spy()};

    issueCacheManager = new JIRA.Issues.Cache.IssueCacheManager({
        searchResults: searchResults,
        viewIssueData: viewIssueData
    });

    searchResults.removeIssue(5);
    ok(viewIssueData.remove.called, "Issue was removed from the cache");
});

test("selectNextIssueTriggersTheRightEvents", function() {
    var event;
    var searchResults = this.createDefaultSearchResults({pageSize: 5});
    searchResults.bind("nextIssueSelected", function(e) { event = e; });
    searchResults.selectFirstInPage();
    searchResults.selectNextIssue();

    deepEqual(event.nextIssue, { "id": 2, "key": "HSP-2" }, "next should be HSP-2");
    deepEqual(event.nextNextIssue, { "id": 3, "key": "HSP-3" }, "next-next should be HSP-3");
});

test("selectPrevIssueTriggersTheRightEvents", function() {
    var event;
    var searchResults = this.createDefaultSearchResults({pageSize: 5});
    searchResults.bind("prevIssueSelected", function(e) { event = e; });
    searchResults.selectIssueByKey('HSP-5');
    searchResults.selectPrevIssue();

    deepEqual(event.prevIssue, { "id": 4, "key": "HSP-4" }, "next should be HSP-4");
    deepEqual(event.prevPrevIssue, { "id": 3, "key": "HSP-3" }, "next-next should be HSP-3");
});

test("getResultForId should pass filter's id only when it is not system filter", function() {
    var callArgs;
    var searchResults = this.createSearchResults();
    var filter = {
        getIsSystem : sinon.stub(),
        getId : function() { return 123; }
    };

    filter.getIsSystem.returns(true);
    searchResults.getResultForId(1001, filter);

    filter.getIsSystem.returns(false);
    searchResults.getResultForId(1001, filter);

    ok(searchResults._issueSearchManager.getRowsForIds.getCall(0).calledWithExactly([1001], {columnConfig: "user"}), "filterId should *not* exist in parameters for system filter");
    ok(searchResults._issueSearchManager.getRowsForIds.getCall(1).calledWithExactly([1001], {columnConfig: "user", filterId: 123}), "filterId should exist in parameters for user defined filter");
});

test("Dirty comment warning is respected when unselecting or selecting different issue", function() {
    var searchResults = this.createDefaultSearchResults();
    var callback = sinon.spy();
    searchResults.onSelectedIssueChange(callback, {});

    this.canDismissCommentStub.returns(false);
    searchResults.selectIssueById(1);
    equal(callback.callCount, 0, "Selected issue isn't changed when user doesn't want to dismiss the comment");
    searchResults.selectIssueById();
    equal(callback.callCount, 0, "Selected issue isn't changed when user doesn't want to dismiss the comment");

    this.canDismissCommentStub.returns(true);
    searchResults.selectIssueById(1);
    equal(callback.callCount, 1, "Selected issue is changed when user doesn't want to dismiss the comment");
    searchResults.selectIssueById();
    equal(callback.callCount, 2, "Selected issue is changed when user doesn't want to dismiss the comment");
});