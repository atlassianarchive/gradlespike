Backbone.define('JIRA.Issues.IssueNavRouter', JIRA.Issues.BaseRouter.extend({

    initialize: function (options) {
        _.extend(this, options);
        _.bindAll(this,
            "_restoreSessionSearch",
            "_route");

        this.route(/^(.*?)([\?]{1}.*)?$/, this._route);
        this.route(/^(issues\/)?$/, this._restoreSessionSearch);

        // backbone-query-parameters supports clever decoding of values into arrays, but we don't want this.
        delete Backbone.Router.arrayValueSplit;
    },

    /**
     * Overwrite JIRA.Issues.BaseRouter, now it fires an event each time the URL changes
     */
    navigate: function() {
        this.searchPageModule.removeOpenTipsies();
        this.trigger("navigate");
        JIRA.Issues.BaseRouter.prototype.navigate.apply(this, arguments);
    },

    /**
     * Navigate to a new state.
     *
     * @param {JIRA.Issues.URLSerializer.state} state
     */
    pushState: function (state) {
        this._setStatePermalink(state);
        this.navigate(JIRA.Issues.URLSerializer.getURLFromState(state), {trigger: false});
    },

    replaceState: function (state) {
        this._setStatePermalink(state);
        this.navigate(JIRA.Issues.URLSerializer.getURLFromState(state), {trigger: false, replace: true});
    },

    _restoreSessionSearch: function() {
        var sessionSearch = this.initialSessionSearchState,
            URL = JIRA.Issues.URLSerializer.getURLFromState(sessionSearch || this.searchPageModule.getState());

        this.navigate(URL, {
            replace: true,
            trigger: true
        });
    },

    /**
     * The "catch-all" route that distinguishes search and issue fragments.
     *
     * @param {string} path The path component of the URL (relative to the root)
     * @param {object} query The decoded querystring params
     * @private
     */
    _route: function (path, query) {
        // Re-encode back to a full fragment, since we do our own parsing in JIRA.Issues.URLSerializer
        var fragment = this.toFragment(path, query);

        if (JIRA.Issues.ignorePopState) {
            // Workaround for Chrome bug firing a null popstate event on page load.
            // Backbone should fix this!
            // @see http://code.google.com/p/chromium/issues/detail?id=63040
            // @see also JRADEV-14804
            return;
        }

        // Remove ignored parameters (e.g. focusedCommentId).
        var state = JIRA.Issues.URLSerializer.getStateFromURL(fragment);

        if (!this._navigateToLoginIfNeeded(state)) {
            this._navigateUsingState(state);
        }
    },

    _navigateToLoginIfNeeded: function(state, history) {
        if (!JIRA.Issues.IssueNavRouter.usePushState(history) && state.selectedIssueKey && !JIRA.Issues.LoginUtils.isLoggedIn()) {
            var instance = this;

            var requestParams = {};
            if (state.filter != null) {
                requestParams.filterId = state.filter;
            }

            jQuery.ajax({
                url: AJS.contextPath() + "/rest/issueNav/1/issueNav/anonymousAccess/" + state.selectedIssueKey,
                headers: { 'X-SITEMESH-OFF': true },
                data: requestParams,
                success: function() {
                    instance._navigateUsingState(state);
                },
                error: function(xhr) {
                    if (xhr.status === 401) {
                        instance._redirectToLogin(state);
                    } else {
                        instance._navigateUsingState(state);
                    }
                }
            });

            return true;
        }

        return false;
    },

    _navigateUsingState: function(state) {
        if (JIRA.Issues.Application.request("issueEditor:canDismissComment")) {
            this._setStatePermalink(state);
            this.navigate(JIRA.Issues.URLSerializer.getURLFromState(state), {replace: true, trigger: false});
            this.searchPageModule.updateFromRouter(state);
        }
    },

    _redirectToLogin: function(state) {
        var url = AJS.contextPath() + "/login.jsp?permissionViolation=true&os_destination=" +
            encodeURIComponent(JIRA.Issues.URLSerializer.getURLFromState(state));

        window.location.replace(url);
    },

    /**
     * Set the permalink for a given state into AJS.Meta to be rendered by the share plugin
     */
    _setStatePermalink: function(state) {
        var viewIssueState = _.pick(state, "selectedIssueKey");
        var baseUrl = AJS.Meta.get("jira-base-url");
        if (!_.isEmpty(viewIssueState)) {
            AJS.Meta.set("viewissue-permlink",
                baseUrl + "/" + JIRA.Issues.URLSerializer.getURLFromState(viewIssueState)
            );
        }
        var issueNavState = _.omit(state, "selectedIssueKey");
        if (!_.isEmpty(issueNavState)) {
            AJS.Meta.set("issuenav-permlink",
                baseUrl + "/" + JIRA.Issues.URLSerializer.getURLFromState(issueNavState)
            );
        }
    }
}, {
    /**
     * @param {object} [history=window.history] The history object to use.
     * @return The backbone history root that should be used for the current browser.
     */
    getBackboneRoot: function (history) {
        return AJS.contextPath() + (this.usePushState(history) ? "/" : "/i");
    },

    usePushState: function(history) {
        var hasPushState = (history = history || window.history) && history.pushState,
            iRootEnabled = AJS.DarkFeatures.isEnabled("ka.I_ROOT");

        return hasPushState && !iRootEnabled;
    }
}));
