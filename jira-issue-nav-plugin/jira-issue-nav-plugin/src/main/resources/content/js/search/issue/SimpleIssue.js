Backbone.define('JIRA.Issues.SimpleIssue', JIRA.Issues.BaseModel.extend({
    properties: ["id", "key"],
    defaults: {
        id: null,
        key: null
    },
    hasIssue: function () {
        return !!this.getId();
    }
}));