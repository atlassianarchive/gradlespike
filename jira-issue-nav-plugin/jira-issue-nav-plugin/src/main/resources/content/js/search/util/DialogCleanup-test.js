AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

//test dialogs are hidden on navigate event
module('JIRA.Dialog.DialogCleanupTest');

test("test dialog is hidden on navigate event", function() {
    //Mock router and navigate() method
    var router = JIRA.Issues.TestUtils.createIssueNavRouter( {
        searchPageModule: { bind: sinon.spy(), removeOpenTipsies: sinon.spy() }
    });

    router.navigate = sinon.spy(JIRA.Issues.IssueNavRouter.prototype, "navigate");

    //Mock the active dialog
    JIRA.Dialog.current = {hide: sinon.spy()};

    //Init the dialogCleaner
    JIRA.Issues.dialogCleaner(router);

    router.navigate('any-url.html');

    ok(JIRA.Dialog.current.hide.calledOnce,
        "dialog hide() has been called");

    JIRA.Dialog.current = null;
    JIRA.Issues.IssueNavRouter.prototype.navigate.restore()
});
