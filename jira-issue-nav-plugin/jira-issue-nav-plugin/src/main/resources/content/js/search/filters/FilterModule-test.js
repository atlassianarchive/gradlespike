AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:sinon");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:issuenav");

module('JIRA.Issues.FilterModule', {
    setup: function() {
        AJS.keys = {shortcuts: [{moduleKey: 'toggle-filter-panel', keys: '['}]};
        this.sandbox = sinon.sandbox.create({
            properties: ["spy", "stub", "server", "requests"],
            useFakeTimers: false,
            useFakeServer: true
        });
        this.server = this.sandbox.server;
        this.sandbox.stub(JIRA.Issues.FilterPanelView.prototype, "render");
        this.searchPageModule = JIRA.Issues.TestUtils.mockSearchPageModule();

        JIRA.Issues.Application.start();

        this.el = AJS.$("<div></div>");
        this.filterModule = new JIRA.Issues.FilterModule({
            searchPageModule : this.searchPageModule,
            systemFilters: [
                {"id":-1,"name":"My Open Issues","jql":"assignee = currentUser() AND resolution = Unresolved ORDER BY updatedDate DESC","isSystem":true,"sharePermissions":[],"requiresLogin":true},
                {"id":-2,"name":"Reported by Me","jql":"reporter = currentUser() ORDER BY createdDate DESC","isSystem":true,"sharePermissions":[],"requiresLogin":true},
                {"id":-3,"name":"Recently Viewed","jql":"issuekey in issueHistory() ORDER BY lastViewed DESC","isSystem":true,"sharePermissions":[]},
                {"id":-4,"name":"All Issues","jql":"ORDER BY createdDate DESC","isSystem":true,"sharePermissions":[]}
            ]
        });
        this.searchPageModule.filterModule = this.filterModule;
        var filterData = [
            {
                id: 10000,
                name: "Sample Search 1",
                jql: "project = HSP",
                sharePermissions: []
            },
            {
                id: 10001,
                name: "Sample Search 2",
                jql: "project = TST",
                sharePermissions: []
            },
            {
                id: 10002,
                name: "Sample Search 3",
                jql: "project = HSP and type = bug",
                sharePermissions: []
            }
        ];
        this.filterPanelView = this.filterModule.createView({
            $filterPanelEl: this.el
        });
        this.filterModule.filtersComponent.favouriteFiltersCollection.add(filterData);

        this._getFilter = function(id) {
            return this.filterModule.filtersComponent.favouriteFiltersCollection.get(id);
        };

        this._selectFilter = function(id) {
            var filter = this._getFilter(id);
            this.searchPageModule.resetToFilter(filter);
            return filter;
        };
    },
    teardown: function() {
        this.el.remove();
        this.sandbox.restore();
    }
});

test("FilterModule getFilterById for system filter", function() {
    var deferred = this.filterModule.getFilterById("-2");
    var spy = this.sandbox.spy();
    deferred.done(spy);

    equal(1, spy.callCount);
    var filter = spy.args[0][0];
    equal("-2", filter.getId());
    equal("reporter = currentUser() ORDER BY createdDate DESC", filter.getJql());
    ok(filter.getIsValid(), "The filter is valid.");
});

test("FilterModule getFilterById for favourite filter", function() {
    var deferred = this.filterModule.getFilterById("10001");
    var spy = this.sandbox.spy();
    deferred.done(spy);

    equal(1, spy.callCount);
    var filter = spy.args[0][0];
    equal("10001", filter.getId());
    equal("project = TST", filter.getJql());
    ok(filter.getIsValid(), "The filter is valid.");
});

test("FilterModule getFilterById for unknown filter", function() {
    var response = {
        id: "unknown",
        jql: "this is a chunk of jql"
    };

    this.server.respondWith("GET", new RegExp(JIRA.Components.Filters.Models.Filter.prototype.root),
        [200, { "Content-Type": "application/json" }, JSON.stringify(response)]);

    var deferred = this.filterModule.getFilterById("unknown");
    var spy = this.sandbox.spy();
    deferred.done(spy);

    this.server.respond();

    equal(1, spy.callCount);
    var filter = spy.args[0][0];
    equal("unknown", filter.getId());
    equal("this is a chunk of jql", filter.getJql());
    ok(filter.getIsValid(), "The filter is valid.");
});

test("FilterModel save updates filter collection", function() {
    var newJql = "project = HSP and assignee = currentUser()";
    var filterModel = this._selectFilter(10001);

    var serverData = filterModel.toJSON();
    serverData.jql = newJql;
    this.server.respondWith("PUT", filterModel.url(),
        [200, { "Content-Type": "application/json" }, JSON.stringify(serverData)]);

    filterModel.saveFilter(newJql);
    this.server.respond();

    equal(newJql, filterModel.getJql());
    equal(newJql, this._getFilter(10001).getJql());
});

test("getFilterById() returns invalid filter if private", function() {
    this.server.respondWith([400, {}, ""]);
    var request = this.filterModule.getFilterById("1");

    var spy = this.sandbox.spy();
    request.fail(spy);
    request.fail(function(filterModel) {
        ok(!filterModel.getIsValid());
    });

    this.server.respond();
    ok(spy.called);
});

test("should raise an event when filters are removed", function () {
    var onFilterRemoved = sinon.spy();
    this.filterModule.on('filterRemoved', onFilterRemoved);
    this.filterModule.filtersComponent.trigger('filterRemoved', {filterId: 1});
    equal(onFilterRemoved.callCount, 1, 'the number of times onFilterRemoved is called');
});

test("should reset the search to the selected filter when a filter is selected", function() {
    sinon.stub(this.searchPageModule, "resetToFilter");
    this.filterModule.filtersComponent.trigger('filterSelected', 1);
    equal(this.searchPageModule.resetToFilter.callCount, 1, 'resetToFilter is called once');
    ok(this.searchPageModule.resetToFilter.calledWith(1), 'resetToFilter is called with the right filter');
});

test("should clear the search when no filter is selected", function() {
    sinon.stub(this.searchPageModule, "resetToBlank");
    this.filterModule.filtersComponent.trigger('filterSelected', null);
    equal(this.searchPageModule.resetToBlank.callCount, 1, 'resetToBlank is called once');
});

test("should change the active filter when a new filter is selected", function() {
    var newFilter =  this._selectFilter(10001);
    this.searchPageModule.set("filter", newFilter);

    equal(this.filterModule.filterPanelModel.get("activeFilter").getId(), 10001, "The new filter is saved in filterPanelModel");
});

test("Saving a new filter sets the session search", function() {
    var filterModel = new JIRA.Components.Filters.Models.Filter({id: "-1"});
    this.filterModule.filtersComponent.trigger('newFilter', filterModel);
    equal(this.searchPageModule.getState().filter, "-1", "The saved filter is in the state");
});

test("Saving a new filter saves it as the session search on the server", function() {
    var filterModel = new JIRA.Components.Filters.Models.Filter({id: "1", name: "a filter", favourite: true});
    this.filterModule.filtersComponent.trigger('newFilter', filterModel);

    var lastRequest = _.last(this.server.requests);
    ok(lastRequest.url.match(/rest\/issueNav\/1\/issueTable\/sessionSearch\/$/), "Sends a request to the REST endpoint");
    equal(lastRequest.method, "PUT", "The request uses the PUT method");
    equal(lastRequest.requestBody.match(/\bfitlerId=1\b/, "The request includes the filterId"));
});

test("Saving a new filter respects dirty comment form", function() {
    var filterModel = new JIRA.Components.Filters.Models.Filter({id: "-1"});
    var canDismissCommentStub = this.sandbox.stub(JIRA.Issues.Application, "request").withArgs("issueEditor:canDismissComment");
    var setSessionSearchSpy = this.sandbox.spy(this.filterModule._searchPageModule, "setSessionSearch");

    canDismissCommentStub.returns(false);
    this.filterModule.filtersComponent.trigger('newFilter', filterModel);
    equal(setSessionSearchSpy.callCount, 0, "Keeping a comment doesn't set new Session Search");

    canDismissCommentStub.returns(true);
    this.filterModule.filtersComponent.trigger('newFilter', filterModel);
    equal(setSessionSearchSpy.callCount, 1, "Dismissing a comment sets new Session Search");
});

test("Saving a new filter marks it as active", function() {
    var filterModel = new JIRA.Components.Filters.Models.Filter({id: "-1"});
    this.stub(this.filterModule.filtersComponent, "highlightFilter");
    this.filterModule.filtersComponent.trigger('savedFilter', filterModel);

    sinon.assert.calledOnce(this.filterModule.filtersComponent.highlightFilter);
    sinon.assert.calledWith(this.filterModule.filtersComponent.highlightFilter, filterModel);
});