JIRA.Issues.hashTheUrl = function (uri) {
    var parsedUri = parseUri(uri);
    var retval = {
        changed : false,
        uri : parsedUri
    }
    if (parsedUri.anchor === "" && parsedUri.query !== "") {
        var baseURL = parsedUri.protocol + "://" + parsedUri.authority + parsedUri.path;
        retval.uri = baseURL + "#?" + parsedUri.query;
        retval.changed = true;
    }
    return retval;
};
