/**
 * The collapsible filters panel.
 * <p/>
 * Lists system filters and the user's favourite filters.
 */
Backbone.define("JIRA.Issues.FilterPanelView", JIRA.Issues.BaseView.extend({

    events: {
        "simpleClick .system-filters a.filter-link:not(.requires-login)": "_onClickFilter",
        "simpleClick .new-search": "_onClickNewSearch",
        "click .ui-dock" : "_onDockFilterPanelClick",
        "click .ui-undock" : "_onUndockFilterPanelClick"
    },

    collapsedTemplate: JIRA.Templates.IssueNavFilter.collapsedFiltersPanel,

    template: JIRA.Templates.IssueNavFilter.filtersPanel,

    /**
     * @param {object} options
     */
    initialize: function (options) {

        _.bindAll(this, "adjustFavouriteFiltersHeight");
        if (options.easeOff) {
            this._adjustFavouriteFiltersHeight = _.debounce(this._adjustFavouriteFiltersHeight, options.easeOff);
        }
        JIRA.Issues.onVerticalResize(this.adjustFavouriteFiltersHeight);

        // Resolved when system and favourite filters are rendered
        this.panelReady = jQuery.Deferred();

        JIRA.bind(JIRA.Events.LAYOUT_RENDERED, _.bind(function (e, layoutType) {
            if (layoutType) {
                delete this._splitViewSidebarElement;
            } else {
                this._splitViewSidebarElement = AJS.$(".list-results-panel:first");
            }
        }, this));
    },

    /**
     * Adjust the height of the favourite filters list.
     *
     * @private
     */
    adjustFavouriteFiltersHeight: function () {
        var feedbackLinkHeight = AJS.$(".jira-feedback-link-container.subtle-style").outerHeight(true),
            feedbackLinkOffset = feedbackLinkHeight ? feedbackLinkHeight + 10 : 0,
            filterList = this.$(".favourite-filters"),
            filterListTop = filterList.length && filterList.offset().top,
            windowHeight = jQuery(window).outerHeight();

        filterList.css("height",  windowHeight - filterListTop - feedbackLinkOffset);
    },

    /**
     * When the dock filter link is clicked
     * @private
     */
    _onDockFilterPanelClick: function() {
        AJS.$('.navigator-container').removeClass('navigator-sidebar-collapsed');
    },

    /**
     * When the undock filter link is clicked.
     * @private
     */
    _onUndockFilterPanelClick: function() {
        AJS.$('.navigator-container').addClass('navigator-sidebar-collapsed');
    },

    toggleDockState: function () {
        this.$el.popoutSidebar("toggle");
    },

    _onClickFilter: function (e) {
        e.preventDefault();
        var filterId = jQuery(e.target).data("id");
        this.trigger("filterSelected", filterId);
    },

    _onClickNewSearch: function (e) {
        e.preventDefault();
        this.trigger("filterSelected", null);
    },

    render: function() {
        if (!this.model.isDocked()) {
            AJS.$('.navigator-container').addClass('navigator-sidebar-collapsed');
            this._renderCollapsed();
        } else {
            AJS.$('.navigator-container').removeClass('navigator-sidebar-collapsed');
            this._renderOpen();
        }

        var dockingShortcut = AJS.KeyboardShortcut.getKeyboardShortcutKeys('toggle.filter.panel');
        this.$el.popoutSidebar({
            isDocked: this.model.isDocked(),
            expandedWidth: this.model.getWidth(),
            renderExpanded: _.bind(this._renderOpen, this),
            renderCollapsed: _.bind(this._renderCollapsed, this),
            undockText: AJS.I18n.getText("issue.nav.filters.undock"),
            undockTitle: AJS.I18n.getText('issue.nav.filters.undock.help', dockingShortcut),
            dockText: AJS.I18n.getText("issue.nav.filters.dock"),
            dockTitle: AJS.I18n.getText('issue.nav.filters.dock.help', dockingShortcut),
            toggleTarget: ".filter-title:first",
            persist: _.bind(function (isDocked, width) {
                this.model.setDocked(isDocked);
                this.model.setWidth(width);
            }, this),
            resize: _.bind(function () {
                this._splitViewSidebarElement && this._splitViewSidebarElement.sidebar("updatePosition");
            }, this)
        });

        if (this.model.shouldShowDockIntro()) {
            this.$el.popoutSidebar("collapse", false);
        }

        if (this.model.isDocked()) {
            this.$el.popoutSidebar("renderDockState");
        }
        this._fireDockAnalytics();
        this.panelReady.resolve();

        return this.$el;
    },

    _fireDockAnalytics: function () {
        if (localStorage.getItem("dockStatesAnalyticsEnabled")) {
            var docked = this.model.isDocked();
            if (localStorage.getItem("dockStateAnalytic") !== "" + docked) {
                localStorage.setItem("dockStateAnalytic", docked);
                JIRA.Issues.Application.execute("analytics:trigger", "kickass.filters.docked", {
                    isDocked: this.model.isDocked()
                });
            }
        }
    },

    /**
     * Render the collapsed filter panel.
     *
     * @return {JIRA.Issues.FilterPanelView} <tt>this</tt>
     * @private
     */
    _renderCollapsed: function () {
        var $feedback = jQuery(".jira-feedback-link-container.subtle-style").detach().addClass("left noborder");
        this.$el.addClass("collapsed").html(this.collapsedTemplate({isDocked: this.model.isDocked()}));
        this.trigger("renderCollapsed");
        JIRA.Issues.triggerHorizontalResize();
        $feedback.appendTo("body");
        return this;
    },

    /**
     * Render the open filter panel.
     *
     * @return {JIRA.Issues.FilterPanelView} <tt>this</tt>
     * @private
     */
    _renderOpen: function () {
        var $feedback = jQuery(".jira-feedback-link-container.subtle-style").detach().removeClass("left noborder");
        this.$el.removeClass("collapsed");

        this.$el.width(this.model.getWidth());
        this.$el.html(this.template({}));

        this.trigger("renderOpen");
        $feedback.appendTo(this.$el);

        JIRA.trigger(JIRA.Events.NEW_CONTENT_ADDED, [this.$el, JIRA.CONTENT_ADDED_REASON.filterPanelOpened]);
        JIRA.Issues.triggerHorizontalResize();
        return this;
    },

    _renderSelectedFilter: function () {
        //FIXME Disabled until both views are moved to FilterComponent
        //this.favouriteFiltersController.activateFilter(this.model.get("activeFilter"));
        //this.systemFilters.activateFilter(this.model.get("activeFilter"));
    }
}));
