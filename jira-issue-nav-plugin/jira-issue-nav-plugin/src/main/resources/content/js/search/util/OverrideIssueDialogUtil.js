/**
 * Overriding from old issue navigator api.
 * todo retire old getDefaultAjaxOptions
 *
 * @this JIRA.FormDialog
 * @return {Object}
 */
JIRA.Dialogs.getDefaultAjaxOptions = function () {

    var linkIssueURI = this.options.url || this.getRequestUrlFromTrigger();

    this.issueId = JIRA.Issues.Api.getSelectedIssueId();
    this.issueKey = JIRA.Issues.Api.getSelectedIssueKey();

    if (/id=\{0\}/.test(linkIssueURI)) {
        if (!this.issueId) {
            return false;
        } else {
            linkIssueURI = linkIssueURI.replace(/(id=\{0\})/, "id=" + this.issueId);
        }
    }
    return {
        data: {decorator: "dialog", inline: "true"},
        url: linkIssueURI
    };
};
