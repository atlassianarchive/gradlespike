(function () {
    var IS_DOCKED_STORAGE_KEY = "issues.sidebar.docked";
    var WIDTH_STORAGE_KEY = "issues.sidebar.width";

    /**
     * The state of the filters panel: favourite filter data, system filter data
     * .
     */
    Backbone.define("JIRA.Issues.FilterPanelModel", JIRA.Issues.BaseModel.extend({
        properties: [
            "activeFilter"
        ],

        defaults: {
            "activeFilter": null
        },

        /**
         * @param {object} attributes
         * @param {object} options
         * @param {object[]} options.systemFilters
         */
        initialize: function (attributes, options) {
            _.extend(this, _.defaults(options, {
                storage: window.localStorage
            }));
        },

        setDocked: function (state) {
            this._getStorage().setItem("dockStatesAnalyticsEnabled", true);
            return this._getStorage().setItem(IS_DOCKED_STORAGE_KEY, state);
        },

        setWidth: function (width) {
            return this._getStorage().setItem(WIDTH_STORAGE_KEY, width);
        },

        getWidth: function () {
            var storedWidth = parseInt(this._getStorage().getItem(WIDTH_STORAGE_KEY), 10);
            return ((storedWidth > 0) ? storedWidth : 200);
        },


        _getStorage: function () {
            return this.storage || window.localStorage;
        },

        isDockedPrefGiven: function () {
            return this._getStorage().getItem(IS_DOCKED_STORAGE_KEY) !== null;
        },

        shouldShowDockIntro: function () {
            return !this.isDocked() &&!this.isDockedPrefGiven();
        },

        isExpanded: function () {
            //Showing dock intro should collapse the filter panel
            return this.shouldShowDockIntro() ? false : this.isDocked();
        },

        /**
         *
         * @return {boolean}
         */
        isDocked: function () {
            var storage = this._getStorage();
            if (storage.getItem(IS_DOCKED_STORAGE_KEY) == null) {
                return JIRA.Issues.Browser.getWindowWidth() > 1200;
            } else {
                return storage.getItem(IS_DOCKED_STORAGE_KEY) !== "false";
            }
        }
    }));
}());