AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:viewissue");

module("JIRA.Components.IssueViewer", {
    setup: function() {
        this.metadata = {
            a: "b",
            c: "d"
        };
        this.sandbox = sinon.sandbox.create();
        JIRA.Issues.Application.start();
        this.issueModule = JIRA.Issues.Application.request("issueEditor");
        this.issueModule.model.updateFromEntity({metadata: this.metadata});
    },

    teardown: function () {
        this.sandbox.restore();
    }
});

test("beforeShow injects issue metadata into AJS.Meta", function () {
    var setStub = sinon.stub(AJS.Meta, "set");

    JIRA.Issues.Application.execute("issueEditor:beforeShow");
    equal(setStub.callCount, _.size(this.metadata),
        "AJS.Meta.set() was called the correct number of times");

    _.each(this.metadata, function (value, key) {
        ok(setStub.calledWith(key, value), [
            "AJS.Meta.set() was called with '", key, "' and '", value, "'."
        ].join(""));
    });

    setStub.restore();
});

test("beforeHide removes issue metadata from AJS.Meta", function () {
    var setStub = sinon.stub(AJS.Meta, "set");
    var hideLightboxStub = sinon.stub(AJS.$.fancybox, "close");

    JIRA.Issues.Application.execute("issueEditor:beforeHide");
    equal(setStub.callCount, _.size(this.metadata),
        "AJS.Meta.set() was called the correct number of times");

    _.each(this.metadata, function (value, key) {
        ok(setStub.calledWith(key, null), [
            "'", key, "' was removed from AJS.Meta."
        ].join(""));
    });

    setStub.restore();
    hideLightboxStub.restore();
});

test("beforeHide closes the lightbox", function () {
    var setStub = sinon.stub(AJS.Meta, "set");
    var hideLightboxStub = sinon.stub(AJS.$.fancybox, "close");

    JIRA.Issues.Application.execute("issueEditor:beforeHide");
    equal(hideLightboxStub.callCount, 1, "AJS.$.fancybox.close() was called");

    setStub.restore();
    hideLightboxStub.restore();
});

test("beforeHide sets the SkipSaveSuccess flag to true", function () {
    var setStub = sinon.stub(AJS.Meta, "set");
    var hideLightboxStub = sinon.stub(AJS.$.fancybox, "close");

    sinon.spy(this.issueModule.issueSaver, "setSkipSaveIssueSuccessHandler");
    JIRA.Issues.Application.execute("issueEditor:beforeHide");
    ok(this.issueModule.issueSaver.setSkipSaveIssueSuccessHandler.calledWith(true));

    this.issueModule.issueSaver.setSkipSaveIssueSuccessHandler.restore();
    setStub.restore();
    hideLightboxStub.restore();
});

test("getIssueId", function () {
    this.issueModule.model.set("entity", {id: 1});
    equal(this.issueModule.getIssueId(), 1, "Returns the selected issue's ID");

    this.issueModule.model.set("entity", {id: NaN});
    equal(this.issueModule.getIssueId(), null, "Returns null when no valid issue is selected");
});

test("getIssueKey", function () {
    this.issueModule.model.set("entity", {key: "JRA-1"});
    equal(this.issueModule.getIssueKey(), "JRA-1", "Returns the selected issue's key");

    this.issueModule.model.set("entity", {key: ""});
    equal(this.issueModule.getIssueKey(), null, "Returns null when no valid issue is selected");
});

/*
//TODO Moved to errorcontroller
test("renderIssueDoesNotExist creates error view", function () {
    var container = jQuery("<div>").appendTo("#qunit-fixture");

    this.issueModule.renderIssueDoesNotExist(container);

    ok(container.find("#issue-content .issue-body-content .error").length, "Element contains error view");
});
*/

test("editField() with immediately editable hidden field", function () {
    var field,
        modal;

    field = JIRA.Issues.TestUtils.mockIssueFieldModel("id", true, false);
    modal = {show: this.sandbox.stub()};

    this.sandbox.stub(JIRA.Components.IssueEditor.Views, "ModalFieldView").returns(modal);

    this.issueModule.editField(field);
    ok(JIRA.Components.IssueEditor.Views.ModalFieldView.calledOnce, "dialog triggered");
    ok(JIRA.Components.IssueEditor.Views.ModalFieldView.firstCall.args[0].model === field, "dialog editing correct field");
    ok(modal.show.calledOnce);
});

test("editField() with inline editable visible field", function () {
    var field;

    field = JIRA.Issues.TestUtils.mockIssueFieldModel("id", true, true);
    this.sandbox.spy(field, "edit");

    this.issueModule.editField(field);
    ok(field.edit.calledOnce, "field editing triggered");
});

test("refreshIssue() with no selected issue", function () {
    var payload,
        promise;

    payload = {foo: "bar"};
    this.issueModule.model.resetToDefault();

    promise = this.issueModule.refreshIssue(payload);
    equal(promise.state(), "resolved");
});

test("refreshIssue() promise resolved independently for multiple calls", function() {
    var first = this.sandbox.spy(),
        second = this.sandbox.spy();
    this.sandbox.useFakeServer();
    this.sandbox.stub(this.issueModule.model, "hasIssue").returns(true);

    this.issueModule.refreshIssue({foo: "1"}).always(first);
    this.issueModule.refreshIssue({foo: "2"}).always(second);

    equal(first.callCount, 1, "should be called because the second refresh will take precedence");
    equal(second.callCount, 0, "hasn't been resolved yet");

    this.sandbox.server.respond();

    equal(second.callCount, 1, "latest call to update should be honored");
    equal(first.callCount, 1, "should not be re-triggered");
});
