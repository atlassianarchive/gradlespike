(function () {

    var transaction;

    /**
     * A collection of events that *weren't* emitted.
     * @constructor
     */
    function EventLog () {
        this.modelEvents = {};
    }

    /**
     * Record the details of an event, so it can be replayed later.
     *
     * @param {Backbone.Model} model
     * @param {*[]} args
     */
    EventLog.prototype.captureEvent = function (model, args) {
        var name = args[0];

        // We only want to trigger each type of event at most once, so they're keyed on their name.
        this.modelEvents[model.cid] || (this.modelEvents[model.cid] = {
            model: model,
            events: {}
        });

        this.modelEvents[model.cid].events[name] = args;
    };

    /**
     * Replay all the events that were captured.
     */
    EventLog.prototype.replayEvents = function () {
        _.each(this.modelEvents, function (item) {
            _.each(item.events, function (args, name) {
                Backbone.Events.trigger.apply(item.model, args);
            });
        });
    };

    /**
     * A drop-in replacement for Backbone.Events.trigger that captures change[:] events rather
     * than emitting them.
     */
    function filteredTrigger (name) {
        var model = this;

        if (name === "change" || name.indexOf("change:") === 0) {
            transaction.log.captureEvent(model, _.toArray(arguments));
        } else {
            Backbone.Events.trigger.apply(model, arguments);
        }
    }

    var beginTransaction = function () {
        // if there is a batch already in progress then just piggy-back onto that one. this is
        // necessary so that changes to multiple Backbone models are all published as part of
        // a single batch.
        if (transaction) {
            return {
                commit: function () {} // the outer batch will publish everything
            };
        }

        transaction = {
            log: new EventLog()
        };

        var restoreTrigger = JIRA.Issues.Utils.patch(Backbone.Model.prototype, "trigger", filteredTrigger);

        return {
            commit: function () {
                restoreTrigger();
                transaction.log.replayEvents();
                transaction = null;
            }
        };
    };

    JIRA = JIRA || {};
    JIRA.Issues = JIRA.Issues || {};
    JIRA.Issues.ModelUtils = JIRA.Issues.ModelUtils || {};

    /**
     * Runs the given closure within a "transaction", meaning that all change events are fired atomically AFTER the
     * closure returns. Inside the closure, this==model.
     * <p/>
     * Calling this method when there is a transaction already in progress will make the inner transaction enlist in
     * the outer (all change events will be fired at the end of the outermost transaction).
     *
     * @param closure {function} the code to run in a transaction
     * @param context {object} an optional context to use when calling the closure (defaults to undefined)
     * @return {undefined}
     */
    JIRA.Issues.ModelUtils.transaction = function (closure, context) {
        var tx = beginTransaction();

        try {
            return closure.apply(context);
        } finally {
            tx.commit();
        }
    };

    /**
     * Makes the methods with the given names transactional. If no method names are provided then any method whose name
     * does not start with _ is made transactional. "Transactional" methods will not raise Backbone change events until
     * the transaction is finished (which happens when the outermost transactional method returns).
     *
     * @param {object} object the instance on which the methods are
     * @return {undefined}
     */
    JIRA.Issues.ModelUtils.makeTransactional = function (object) {
        var transactionalise = function(realMethod) {
            return function () {
                var args = arguments;

                return JIRA.Issues.ModelUtils.transaction(function () {
                    return realMethod.apply(this, args); // calls the original, non-transactional, method
                }, this);
            };
        };

        var methods = Array.prototype.slice.call(arguments, 1);
        if (methods.length === 0) { methods = _.functions(object); }
        _.each(methods, function(methodName) {
            // skip "private" methods
            if (methodName.indexOf('_') !== 0) {
                object[methodName] = transactionalise(object[methodName]);
            }
        });
    };

})();
