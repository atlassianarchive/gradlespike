AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:common");

module('JIRA.Issues.ModelUtils', {
    //
    // set up a simple Backbone model and view to test with
    //
    setup: function() {
        var SimpleModel = Backbone.Model.extend({
            methodThatSetsAttr1And2: function() {
                this.set("attr1", "attr1 method before");
                this.set("attr2", "attr2 method before");
                this.methodThatSetsAttr2();
                this.set("attr1", "attr1 method after");
                this.set("attr2", "attr2 method after");
            },

            methodThatSetsAttr2: function() {
                this.set("attr2", this.get("attr2") + " and then some more");
            }
        });
        var SimpleView = Backbone.View.extend({
            initialize: function(options) {
                this.model.on("change:attr1", this.change1, this);
                this.model.on("change:attr2", this.change2, this);
            },

            change1: function() {
                this.attr1FromEvent = this.attr1FromEvent || [];
                this.attr1FromEvent.push(this.model.get("attr1"));
            },

            change2: function() {
                this.attr2FromEvent = this.attr2FromEvent || [];
                this.attr2FromEvent.push(this.model.get("attr2"));
            }
        });

        this.model1 = new SimpleModel({ attr1: "one", attr2: "two" });
        this.model2 = new SimpleModel({ attr1: "a",   attr2: "b" });

        this.view1 = new SimpleView({ model: this.model1 });
        this.view2 = new SimpleView({ model: this.model2 });
    }
});

test("change events are delayed until the batch is published", function() {
    var modifiedAttr1 = "one+one";
    var modifiedAttr2 = "two+two";


    JIRA.Issues.ModelUtils.transaction(function() {
        this.model1.set("attr1", "should never be observed by listeners");
        strictEqual(this.view1.attr1FromEvent, undefined, "attr1 change event has not been fired yet");

        this.model1.set("attr1", modifiedAttr1);
        strictEqual(this.view1.attr1FromEvent, undefined, "attr1 change event has not been fired yet");

        this.model1.methodThatSetsAttr2();
        strictEqual(this.view1.attr2FromEvent, undefined, "methodThatSetsAttr2() should not fire change event");

        this.model1.set({"attr2": "value from batch set"});
        strictEqual(this.view1.attr2FromEvent, undefined, "set({}) should not fire change event");

        this.model1.set("attr2", modifiedAttr2);
        strictEqual(this.view1.attr2FromEvent, undefined, "attr2 change event has not been fired yet");
    }, this);

    // assert notifications were received
    deepEqual(this.view1.attr1FromEvent, [ modifiedAttr1 ], "attr1 change has been fired in publish()");
    deepEqual(this.view1.attr2FromEvent, [ modifiedAttr2 ], "attr2 change has been fired in publish()");
});

test("change events in sub-batches become a part of any batch that is already in progress", function() {
    var modification1 = "first mod";
    var modification2 = "second mod";

    JIRA.Issues.ModelUtils.transaction(function() {
        this.model1.set("attr1", modification1);
        strictEqual(this.view1.attr1FromEvent, undefined, "change event for model1 has not been fired yet");

        JIRA.Issues.ModelUtils.transaction(function() {
            this.model2.set("attr1", modification2);
            strictEqual(this.view1.attr1FromEvent, undefined, "change event for model2 has not been fired yet");
        }, this);
    }, this);

    // assert notifications were received
    deepEqual(this.view1.attr1FromEvent, [ modification1 ], "change for model1 has been fired");
    deepEqual(this.view2.attr1FromEvent, [ modification2 ], "change for model2 has been fired");
});

test("make transactional makes instance methods run in a transaction", function() {
    JIRA.Issues.ModelUtils.makeTransactional(this.model1, "methodThatSetsAttr1And2", "methodThatSetsAttr2");
    this.model1.methodThatSetsAttr1And2();

    deepEqual(this.view1.attr1FromEvent, [ "attr1 method after" ], "attr1 change event is only fired once with the latest change");
    deepEqual(this.view1.attr2FromEvent, [ "attr2 method after" ], "attr1 change event is only fired once with the latest change");
});

test("return values are propagated", function () {
    var foo = {
        bar: function () { return 1234; }
    };

    JIRA.Issues.ModelUtils.makeTransactional(foo, "bar");

    equal(foo.bar(), 1234);
});
