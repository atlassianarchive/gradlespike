AJS.namespace("JIRA.Issues.BaseModel");
AJS.namespace("JIRA.Issues.BaseCollection");
AJS.namespace("JIRA.Issues.BaseView");
AJS.namespace("JIRA.Issues.BaseRouter");

(function () {

    // Applies all mixins to the given constructor's prototype.
    function applyMixin(ctor, mixin) {
        _.forEach(_.keys(mixin), function(key) {
            var proto = ctor.prototype;

            // `initialize` is not mixed in - we compose the mixin's initialize with the existing initialize method (if it exists).
            if ("initialize" === key) {
                var oldInitialize = proto.initialize;
                proto.initialize = function() {
                    mixin.initialize.apply(this, arguments);
                    if (oldInitialize) {
                        oldInitialize.apply(this, arguments);
                    }
                };
                return;
            }
            // `validate` is not mixed in - we compose the mixin's validate with the existing validate method (if it exists).
            if ("validate" === key) {
                var oldValidate = proto.validate;
                proto.validate = function() {
                    var errors = mixin.validate.apply(this, arguments);
                    if (errors) {
                        return errors;
                    }
                    if (oldValidate) {
                        return oldValidate.apply(this, arguments);
                    }
                };
                return;
            }
            // `defaults` are not mixed in - we compose the mixin's defaults with existing defaults if they exist
            if ("defaults" === key) {
                var defaults = proto.defaults || (proto.defaults = {});
                var mixinDefaults = mixin[key];
                for (var id in mixinDefaults) {
                    if (defaults.hasOwnProperty(id)) {
                        throw "Mixin error: object " + ctor + " already has default " + id + " defined for mixin " + mixin;
                    }
                    defaults[id] = mixinDefaults[id];
                }
                return;
            }
            // `properties` are added to the mixin, and we mixin in getters and setters for each property.
            if ("properties" === key) {
                // `properties` must be an array
                if (!_.isArray(mixin[key])) {
                    throw "Expects properties member on mixin to be an array";
                }
                if (!proto.properties) {
                    proto.properties = [];
                }
                proto.properties = proto.properties.concat(mixin[key]);
                return;
            }

            // `namedEvents` are added to the mixin, and we mix in bind and trigger methods for each property.
            if ("namedEvents" === key) {
                // `events` must be an array
                if (!_.isArray(mixin[key])) {
                    throw "Expects events member on mixin to be an array";
                }
                if (!proto.namedEvents) {
                    proto.namedEvents = [];
                }
                proto.namedEvents = proto.namedEvents.concat(mixin[key]);
                return;
            }
            // Name collisions with other mixins or or the object we're mixing into result in violent and forceful disapproval.
            if (proto.hasOwnProperty(key)) {
                throw "Mixin error: object " + ctor + " already has property " + key + " for mixin " + mixin;
            }
            proto[key] = mixin[key];
        }, this);
    }

    /*
     * Generates an `extend` method that overrides Backbone's default `extend`. The new extend calls Backbone's `extend`, then:
     * <ul>
     *     <li>Adds all mixins specified in the `mixins` array.</li>
     *     <li>Adds a `JIRA.Issues.EventsMixinCreator` to mix in bind and trigger methods for events specified in the `namedEvents` array,</li>
     *     <li>Adds a `JIRA.Issues.AttributesMixinCreator` to mix in get and set methods for attributes specified in the `attributes` array,</li>
     * </ul>
     */
    function generateMixinExtend(oldExtend) {
        return function(protoProps, classProps) {
            var child;
            var cleanProtoProps = _.extend({}, protoProps);
            // Remove `mixins` - we don't want to see them on the created prototype. Note that we do want to see `properties` and `namedEvents` for debugging
            var mixins;
            if (protoProps && protoProps.mixins) {
                mixins = protoProps.mixins;
                delete cleanProtoProps.mixins;
            }
            child = oldExtend.call(this, cleanProtoProps, classProps);
            if (mixins) {
                _.each(protoProps.mixins, function(mixin) {
                    applyMixin(child, mixin);
                });
            }
            if (child.prototype.namedEvents) {
                applyMixin(child, JIRA.Issues.EventsMixinCreator.create(child.prototype.namedEvents));
            }
            if (child.prototype.properties) {
                applyMixin(child, JIRA.Issues.AttributesMixinCreator.create(child.prototype.properties));
            }
            //noinspection JSHint
            child.extend = arguments.callee;
            return child;
        };
    }

    // Overrides Backbone's default `get` and `set` methods to validate that the attribute being get / set is a valid property.
    function overrideSetGet(ctor, childCtor) {
        var proto = ctor.prototype;
        var childProto = childCtor.prototype;

        childProto.set = function(key, value, options) {
            // TODO: has, escape, unset
            var attrs,
                properties = this.properties;
            if (properties) {
                if (_.isObject(key) || key == null) {
                    attrs = key;
                } else {
                    attrs = {};
                    attrs[key] = value;
                }
                for (var attr in attrs) {
                    if (_.indexOf(properties, attr) < 0) {
                        throw "Property '" + attr + "' does not exist";
                    }
                }
            }

            // calls super.set
            return proto.set.apply(this, arguments);
        };

        childProto.get = function(attr) {
            if (this.properties && _.indexOf(this.properties, attr) < 0) {
                throw "Property '" + attr + "' does not exist";
            }

            // calls super.get
            return proto.get.apply(this, arguments);
        };
    }

    function addListenMethods(ctor, childCtor) {
        var proto = ctor.prototype;
        var childProto = childCtor.prototype;

        /**
         * Calls <code>other.onEvent(callback, context)</code> and adds the callback to the list of listeners so it can
         * be removed later by calling <code>removeListeners()</code>.
         *
         * @param other the object to call onEvent on
         * @param event the name of the event (used to determine the method name)
         * @param callback the callback
         * @param context an optional context to use when running the callback
         */
        childProto.addListener = function(other, event, callback, context) {
            if (arguments.length < 3) { throw "The 'other', 'event', and 'callback' arguments are mandatory"; }

            var capitalisedEvent = event.charAt(0).toUpperCase() + event.slice(1);
            var registerMethodName = "on" + capitalisedEvent;
            var unregisterMethodName = "off" + capitalisedEvent;
            var braceRegisterMethodName = "bind" + capitalisedEvent;
            var braceUnregisterMethodName = "unbind" + capitalisedEvent;
            var finalRegisterName;
            var finalUnRegisterName;

            if (other[registerMethodName]) {
                // listener for methods
                finalRegisterName = registerMethodName;
                finalUnRegisterName = unregisterMethodName || braceUnregisterMethodName;
                if (typeof other[finalRegisterName] !== 'function') { throw "object does not have method " + registerMethodName + "'"; }
                if (typeof other[finalUnRegisterName] !== 'function') { throw "object does not have method " + unregisterMethodName + "'"; }
            } else {
                // listener for brace events
                finalRegisterName = braceRegisterMethodName;
                finalUnRegisterName = braceUnregisterMethodName;
                if (typeof other[finalRegisterName] !== 'function') { throw "object does not have event [" + event + "] registered'"; }
            }

            // register using the listen method and add to the list so we can clean up in removeListeners()
            other[finalRegisterName](callback, context);
            this._cleanerUppers = this._cleanerUppers || [];
            this._cleanerUppers.push(function() { other[finalUnRegisterName](callback, context); });
        };

        /**
         * Removes all listeners added using <code>addListener()</code>.
         */
        childProto.removeListeners = function() {
            if (this._cleanerUppers) {
                _.each(this._cleanerUppers, function(cleanerUpper) {
                    cleanerUpper(); // un-register the listener
                });
            }
        };
    }

    /**
     * Adds a <code>deactivate()</code> method to the given constructor function and overrides its <code>remove()</code>
     * function to call <code>removeListeners()</code>. To be used in classes to which you have added the
     * <code>addListener()</code> and <code>removeListeners()</code> methods.
     *
     * @see addListenMethods
     * @param ctor
     * @param childCtor
     */
    function addViewCleanupMethods(ctor, childCtor) {
        var childProto = childCtor.prototype;

        var superRemove = ctor.prototype.remove;
        /**
         * Overrides <a href="http://backbonejs.org/#View-remove">Backbone.View.remove()</a> to also call
         * <code>this.removeListeners()</code>.
         */
        childProto.remove = function() {
            this.removeListeners();
            return superRemove.apply(this, arguments);
        };

        /**
         * Deactivates this view by calling the following methods:
         * <ul>
         *     <li><a href="http://backbonejs.org/#View-undelegateEvents">Backbone.Model.undelegateEvents()</a>
         *     <li>JIRA.Issues.BaseView.removeListeners()
         * </ul>
         */
        childProto.deactivate = function() {
            this.undelegateEvents();
            this.removeListeners();
        };
    }

    // Applies extensions to the given constructor function:
    // <ul>
    //   <li>Sets `extend` to a method generated by `generateMixinExtend`</li>
    // </ul>
    function applyExtensions(ctor) {
        var child = ctor.extend();
        var oldExtend = ctor.extend;
        child.extend = generateMixinExtend(oldExtend);
        return child;
    }


    // Applies extensions to the given constructor function:
    // <ul>
    //   <li>Sets `extend` to a method generated by `generateMixinExtend`</li>
    // </ul>
    function applyModelExtensions(ctor) {
        var child = applyExtensions(ctor);
        overrideSetGet(ctor, child);
        return child;
    }

    /**
     * Applies view-specific extensions to the given constructor function and returns a new constructor function. This
     * currently:
     * <ul>
     *   <li>adds <code>addListener()</code> and <code>removeListeners</code> methods
     *   <li>adds a <code>deactivate</code> method (similar to Backbone.View.remove() but doesn't remove 'el' from the DOM)
     *   <li>overrides <code>remove()</code> and makes it call <code>removeListeners()</code>
     * </ul>
     *
     * @param ctor {function} a constructor function for a view
     * @return {function} a new constructor function
     */
    function applyViewExtensions(ctor) {
        var child = applyExtensions(ctor);
        addListenMethods(ctor, child);
        addViewCleanupMethods(ctor, child);
        return child;
    }

    // Extend base Backbone classes
    JIRA.Issues.BaseModel = applyModelExtensions(Backbone.Model);
    JIRA.Issues.BaseCollection = applyExtensions(Backbone.Collection);
    JIRA.Issues.BaseView = applyViewExtensions(Backbone.View);
    JIRA.Issues.BaseRouter = applyExtensions(Backbone.Router);
    var Evented = function() {
        this.initialize.apply(this, arguments);
    };
    _.extend(Evented.prototype, Backbone.Events, {
        initialize: function() {}
    });
    Evented.extend = Backbone.Model.extend;
    JIRA.Issues.BaseEvented = applyExtensions(Evented);

    // add some mixins to underscore
    _.mixin({
        lambda: function(x) {
            return function() {return x;};
        },
        isNotBlank: function(object) {
            return !!object;
        },
        bindObjectTo: function (obj, context) {
        _.map(obj, function(value, key) {
            if (_.isFunction(value)) {
                obj[key] = _.bind(value, context);
            }
        })
    }
    });

    // Backbone.define() - alternative to AJS.namespace that also adds a 'name' property to
    // the ctors returned from Backbone's extend().
    // Also does some hacky eval to show the name of the object when using console.log() in Chrome.
    // Example: Backbone.define('JIRA.Issues.SuperModel', Backbone.Model.extend({ ... }));
    Backbone.define = function(name, ctor) {
        //noinspection JSHint
        eval("Backbone['Class: " + name + "'] = function() { Backbone['Class: " + name + "'].__ctor.apply(this, arguments); }");
        var cls = Backbone['Class: ' + name];
        cls.__ctor = ctor;
        ctor.prototype.name = name;
        cls.prototype = ctor.prototype;
        _.extend(cls, ctor);

        _.each(ctor.prototype, function(fn, fnName) {
            if (typeof fn === 'function') {
                fn.displayName = name + '.' + fnName;
            }
        });

        var context = window;
        var parts = name.split('.');
        _.each(parts, function(part, i) {
            if (i === parts.length - 1) {
                context[part] = cls;
            } else {
                context = context[part] == null ? (context[part] = {}) : context[part];
            }
        });

        return cls;
    };

})();
