AJS.namespace("JIRA.Issues.Application");

JIRA.Issues.Application = new JIRA.Marionette.Application();

JIRA.Issues.Application.addInitializer(function() {
    this.module("JIRA.Components.Pager", JIRA.Components.Pager.AppModule);

    if (AJS.Meta.get("is-inline-edit-enabled") !== false) {
        this.module("JIRA.Components.IssueEditor", new JIRA.Components.IssueEditor.AppModule().definition);
    } else {
        this.module("JIRA.Components.IssueEditor", new JIRA.Components.IssueEditor.NoInlineAppModule().definition);
    }

    this.module("JIRA.Components.Analytics", new JIRA.Components.Analytics.AppModule().definition);
});
