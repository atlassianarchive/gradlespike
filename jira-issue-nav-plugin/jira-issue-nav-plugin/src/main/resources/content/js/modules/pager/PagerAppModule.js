;(function(AJS, JIRA) {
    AJS.namespace("JIRA.Components.Pager.AppModule");

    JIRA.Components.Pager.AppModule = function(PagerModule, app) {
        var pager = new JIRA.Components.Pager({
            nextItem: function() {
                //TODO Refactor to app commands
                JIRA.Issues.Api.nextIssue();
            },
            previousItem: function() {
                //TODO Refactor to app commands
                JIRA.Issues.Api.prevIssue();
            },
            goBack: function() {
                app.execute("returnToSearch");
            }
        });

        app.addInitializer(function() {
            pager.initialize();
            app.commands.setHandlers({
                "pager:update": pager.update,
                "pager:render": pager.show,
                "pager:close":  pager.close
            });
        });
    };

})(AJS, JIRA);