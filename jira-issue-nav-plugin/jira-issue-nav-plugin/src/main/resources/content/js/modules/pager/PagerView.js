;(function(JIRA, $, Backbone) {
    AJS.namespace("JIRA.Components.Pager.View");

    JIRA.Components.Pager.View = JIRA.Marionette.ItemView.extend({
        template: function(data) {
            return _.isEmpty(data)?"":JIRA.Templates.ViewIssue.Header.pager(data);
        },

        events: {
            "simpleClick #return-to-search": "_onReturnToSearchClick"
        },

        triggers: {
            "simpleClick #next-issue": "nextItem",
            "simpleClick #previous-issue": "previousItem"
        },

        ui: {
            nextIssue: "#next-issue",
            previousIssue: "#previous-issue",
            returnToSearch: "#return-to-search"
        },

        modelEvents: {
            "change": "render"
        },

        onRender: function() {
            if (AJS.activeShortcuts) {
                AJS.activeShortcuts["j"] && AJS.activeShortcuts["j"]._addShortcutTitle(this.$el.find(this.ui.nextIssue));
                AJS.activeShortcuts["k"] && AJS.activeShortcuts["k"]._addShortcutTitle(this.$el.find(this.ui.previousIssue));
                AJS.activeShortcuts["u"] && AJS.activeShortcuts["u"]._addShortcutTitle(this.$el.find(this.ui.returnToSearch));
            }
        },

        _onReturnToSearchClick: function (e) {
            e.preventDefault();
            this.trigger("goBack");

            //IE8 and IE9 do not return focus back to the body after pressing the Return to Search link
            //Thus any subsequent shortcut operations will not work because the focus is wrong
            //This is to return focus back to body
            $("body").focus();
        }
    });
})(JIRA, AJS.$, Backbone);
