AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-plugin:pager");

module('JIRA.Components.Pager.View', {
    setup: function() {
        this.el = AJS.$("<div></div>");
        this.issuePagerModel = new JIRA.Components.Pager.Model();

        var shortcuts = [
            {
                "keys": [["u"]],
                "context": "issuenavigation",
                "op": "execute",
                "param": "if (JIRA.Issues && JIRA.Issues.Api.returnToSearch) { JIRA.Issues.Api.returnToSearch(); } else { this.followLink(\"link[rel=index]:first\"); this._executer();}"
            },
            {
                "keys": [["j"]],
                "context": "issuenavigation",
                "op": "evaluate",
                "param": "if(AJS.$(\".standalone-viewissue\").length > 0) { this.followLink(\"#next-issue\"); } else { this.execute(function () { jira.app.issuenavigator.shortcuts.selectNextIssue(); }); }"
            },
            {
                "keys": [["k"]],
                "context": "issuenavigation",
                "op": "evaluate",
                "param": "if(AJS.$(\".standalone-viewissue\").length > 0) { this.followLink(\"#previous-issue\"); } else { this.execute(function () { jira.app.issuenavigator.shortcuts.selectPreviousIssue(); }); }"
            }
        ];
        AJS.activeShortcuts = AJS.whenIType.fromJSON(shortcuts);
    },

    teardown: function() {
        this.el.remove();
    }
});

test("IssuePagerView adds shortcut information to the tooltip", function() {
    var issuePagerView = new JIRA.Components.Pager.View({el:this.el, model: this.issuePagerModel});
    this.issuePagerModel.set({
        previousIssue: {
            id: 1,
            key: "TST-1"
        },
        nextIssue: {
            id: 2,
            key: "TST-3"
        },
        position: 2,
        resultCount: 3,
        hasSearchLink: true
    });
    issuePagerView.render();

    // TODO In some cases, I18N transformation is applied here and the text reads
    //      "navigator.next.title 'TST-3' ( Type 'j' )" (note Type is transformed). Investigate.
    ok(this.el.find('#next-issue').attr('title').match(/navigator\.next\.title 'TST-3' \( .*? 'j' \)/),
            "Next arrow contains the keyboard shortcut 'j' in the tooltip");

    ok(this.el.find('#previous-issue').attr('title').match(/navigator\.previous\.title 'TST-1' \( .*? 'k' \)/),
            "Previous arrow contains the keyboard shortcut 'k' in the tooltip");

    ok(this.el.find('#return-to-search').attr('title').match(/viewissue\.return\.to\.search \( .*? 'u' \)/),
            "ReturnToSearch contains the keyboard shortcut 'u' in the tooltip");
});
