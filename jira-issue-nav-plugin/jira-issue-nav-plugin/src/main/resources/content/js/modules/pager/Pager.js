;(function(AJS, JIRA) {
    AJS.namespace("JIRA.Components.Pager");

    JIRA.Components.Pager = function(callbacks) {
        var pagerModel,
            pagerController;

        this.initialize = function() {
            pagerModel = new JIRA.Components.Pager.Model();
        };

        this.update =  function(data) {
            if (data) {
                pagerModel.update(data);
            }else{
                pagerModel.clear();
            }
        };

        this.show = function(container, element) {
            if (!pagerController) {
                pagerController = new JIRA.Components.Pager.Controller({
                    model: pagerModel
                });
                pagerController.on("goBack", callbacks.goBack);
                pagerController.on("nextItem", callbacks.nextItem);
                pagerController.on("previousItem", callbacks.previousItem);
            }
            pagerController.show(container, element);
        };

        this.close = function() {
            if (pagerController) {
                pagerController.close();
            }
            pagerController = null;
            pagerModel.clear();
        }
    }

})(AJS, JIRA);