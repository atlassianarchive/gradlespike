;(function(JIRA, Backbone) {

    Backbone.define('JIRA.Components.Pager.Model', JIRA.Issues.BaseModel.extend({
        properties: [
            "nextIssue",
            "previousIssue",
            "position",
            "resultCount",
            "hasSearchLink"
        ],

        update: function(properties) {
            // TODO Refactor this, as isSplitView is not a concern for this module
            properties.hasSearchLink = !properties.isSplitView;
            delete properties.isSplitView;
            this.set(properties);
        }
    }));

})(JIRA, Backbone);