package com.atlassian.jira.plugin.issuenav.service.issuetable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.components.orderby.OrderByUtil;
import com.atlassian.jira.components.orderby.SortByBean;
import com.atlassian.jira.components.util.SortJqlGenerator;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayout;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutException;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutItem;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.context.QueryContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.order.OrderBy;
import com.atlassian.query.order.SearchSort;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.issue.fields.layout.column.ColumnLayoutItem.TO_ID;
import static com.google.common.collect.Lists.transform;
import static java.util.Collections.emptyList;

/**
 * An {@link IssueTableCreator} that can handle normal and stable searches.
 * <p/>
 * Subclasses provide the {@link IssueTable#table} property by implementing {@link #getTable()}.
 *
 * @since v6.0
 */
abstract class AbstractIssueTableCreator extends IssueTableCreator
{
    private final ApplicationProperties applicationProperties;
    private final DisplayedColumnsHelper searchColumnsFinder;
    private Map<String, String> columnSortJql;
    private SortByBean sortBy;
    final IssueTableServiceConfiguration configuration;
    private final boolean fromIssueIds;
    private final IssueFactory issueFactory;
    private List<Long> issueIds;
    private List<String> issueKeys;
    final private SortJqlGenerator sortJqlGenerator;
    Query query;
    private Query originalQuery;
    private final boolean returnIssueIds;
    private final SearchHandlerManager searchHandlerManager;
    private final SearchProvider searchProvider;
    private final SearchProviderFactory searchProviderFactory;
    SearchRequest searchRequest;
    SearchResults searchResults;
    private final SearchService searchService;
    private final User user;
    private final FieldManager fieldManager;
    private OrderByUtil orderByUtil;

    /**
     * @param fromIssueIds Whether we are executing a search from issue IDs (stable update).
     * @param issueIds The requested issue IDs.
     * @param query The query whose results will form the table content.
     * @param returnIssueIds Whether issue IDs should be returned.
     * @param searchRequest The search request being executed (may differ from {@code query}).
     * @param user The user executing the search.
     */
    AbstractIssueTableCreator(
            final ApplicationProperties applicationProperties,
            final ColumnLayoutManager columnLayoutManager,
            final IssueTableServiceConfiguration configuration,
            final boolean fromIssueIds,
            final IssueFactory issueFactory,
            final List<Long> issueIds,
            final SortJqlGenerator sortJqlGenerator,
            final Query query,
            final boolean returnIssueIds,
            final SearchHandlerManager searchHandlerManager,
            final SearchProvider searchProvider,
            final SearchProviderFactory searchProviderFactory,
            final SearchRequest searchRequest,
            final SearchService searchService,
            final User user,
            final FieldManager fieldManager,
            final OrderByUtil orderByUtil)
    {
        this.applicationProperties = applicationProperties;
        this.configuration = configuration;
        this.fromIssueIds = fromIssueIds;
        this.issueFactory = issueFactory;
        this.issueIds = issueIds;
        this.sortJqlGenerator = sortJqlGenerator;
        this.query = query;
        this.returnIssueIds = returnIssueIds;
        this.searchHandlerManager = searchHandlerManager;
        this.searchProvider = searchProvider;
        this.searchProviderFactory = searchProviderFactory;
        this.searchRequest = searchRequest;
        this.searchService = searchService;
        this.user = user;
        this.fieldManager = fieldManager;
        this.orderByUtil = orderByUtil;
        this.searchColumnsFinder = new DisplayedColumnsHelper(columnLayoutManager, fieldManager);

        // query and searchRequest aren't provided as we're searching by ID. They're used
        // frequently while constructing the table, so set them here to make them available.
        if (fromIssueIds)
        {
            JqlQueryBuilder queryBuilder = getJqlQueryBuilder();
            queryBuilder.where().issue().in(new MultiValueOperand(issueIds));

            Query stableSearchQuery = queryBuilder.buildQuery();
            Long filterId = (this.searchRequest != null) ? this.searchRequest.getId() : null;
            this.searchRequest = new SearchRequest(stableSearchQuery, (ApplicationUser) null, null, null, filterId, 0L);

            // Internally stable search works by performing a JQL search with the following format: issuekey in ("KEY-1", "KEY-2")
            // In order to keep consistency with the {@link SearchRequest} object, the execution of non-stable search
            // and other classes extending this class, the 'query' variable is overriden.
            // The original query is still required to generate the correct sort jql for each of the column, thus it is preserved here
            this.originalQuery = query == null ? stableSearchQuery : query;
            this.query = stableSearchQuery;
        }
    }

    @VisibleForTesting
    JqlQueryBuilder getJqlQueryBuilder() {
        return JqlQueryBuilder.newBuilder(); // don't likes nasty staticses, do we, precious?
    }

    /**
     * Collect issue documents and IDs from Lucene.
     *
     * @param selectedIssueKey The selected issue's key.
     * @return Issue documents and IDs.
     */
    private IssueDocumentAndIdCollector collectIssues(final String selectedIssueKey) throws SearchException
    {
        final IndexSearcher issuesSearcher = searchProviderFactory.getSearcher(SearchProviderFactory.ISSUE_INDEX);
        final IssueDocumentAndIdCollector IDCollector = new IssueDocumentAndIdCollector(issuesSearcher,
                getStableSearchResultsLimit(), configuration.getNumberToShow(), selectedIssueKey,
                configuration.getStart());

        searchProvider.searchAndSort(query, user, IDCollector, new PagerFilter(0, getStableSearchResultsLimit()));
        return IDCollector;
    }

    /**
     * Convert a list of Lucene documents to a list of issues.
     *
     * @param issueDocuments The documents to convert.
     * @return {@code issueDocuments} as a list of {@link Issue}s.
     */
    private List<Issue> convertDocumentsToIssues(List<Document> issueDocuments)
    {
        List<Issue> issues = Lists.newArrayListWithCapacity(issueDocuments.size());
        for (Document document : issueDocuments)
        {
            issues.add(issueFactory.getIssue(document));
        }

        return issues;
    }

    @Override
    IssueTable create() throws SearchException
    {
        final ColumnLayout columnLayout = getColumnLayout();
        final List<ColumnLayoutItem> columns = getDisplayedColumns(columnLayout);
        final List<String> columnIds = transform(columns, TO_ID);

        configuration.setColumnNames(columnIds);

        if (fromIssueIds)
        {
            executeStableSearch(columns);
        }
        else
        {
            executeNormalSearch(columns);
        }

        return new IssueTable.Builder(getTable())
                .displayed(searchResults.getIssues().size())
                .total(searchResults.getTotal())
                .startIndex(searchResults.getStart())
                .end(searchResults.getEnd())
                .sortBy(sortBy)
                .pageSize(configuration.getNumberToShow())
                .columns(configuration.getColumnNames())
                .columnConfig(columnLayout.getColumnConfig())
                .columnSortJql(columnSortJql)
                .jiraHasIssues(getJIRAHasIssues())
                .issueIds(fromIssueIds ? null : issueIds) // Don't return issue IDs on stable update (it's unnecessary).
                .issueKeys(issueKeys)
                .build();
    }

    /**
     * Retrieve the issues matching {@link #query} and store them in {@link #searchResults}.
     * <p/>
     * If issue IDs were requested, data will also be stored in {@link #issueIds} and {@link #issueKeys}.
     *
     * @throws SearchException If searching the Lucene index fails.
     */
    private void executeNormalSearch(List<ColumnLayoutItem> columns) throws SearchException
    {
        columnSortJql = sortJqlGenerator.generateColumnSortJql(query, getSortableColumns(columns));
        sortBy = orderByUtil.generateSortBy(query);

        if (returnIssueIds)
        {
            final String selectedIssueKey = configuration.getSelectedIssueKey();
            IssueDocumentAndIdCollector IDCollector = collectIssues(selectedIssueKey);
            IssueDocumentAndIdCollector.Result collectedResult = IDCollector.computeResult();

            issueIds = collectedResult.getIssueIDs();
            issueKeys = collectedResult.getIssueKeys();
            searchResults = new SearchResults(
                    convertDocumentsToIssues(collectedResult.getDocuments()),
                    collectedResult.getTotal(),
                    configuration.getNumberToShow(),
                    collectedResult.getStart());
        }
        else
        {
            final PagerFilter pagerFilter = new PagerFilter(configuration.getStart(), configuration.getNumberToShow());
            searchResults = searchProvider.search(query, user, pagerFilter);

            // Ensure that the start index doesn't exceed the number of results.
            int pageSize = pagerFilter.getPageSize();
            int resultsCount = searchResults.getTotal();
            while (pagerFilter.getStart() > 0 && pagerFilter.getStart() >= searchResults.getTotal())
            {
                pagerFilter.setStart(Math.max(0, resultsCount - 1) / pageSize * pageSize);
                searchResults = searchProvider.search(query, user, pagerFilter);
            }
        }
    }

    /**
     * Retrieve and store the issues whose IDs were passed to the constructor from the Lucene index.
     * <p/>
     * The results are stored in {@link #searchResults} and they are sorted as they were in the ID list.
     *
     * @throws SearchException If searching the Lucene index fails.
     */
    private void executeStableSearch(List<ColumnLayoutItem> columns) throws SearchException
    {
        columnSortJql = sortJqlGenerator.generateColumnSortJql(originalQuery, getSortableColumns(columns));

        final PagerFilter pagerFilter = new PagerFilter(0, getStableSearchResultsLimit());
        searchResults = searchProvider.search(query, user, pagerFilter);

        // Sort results so they are in the same order as the list of issue IDs.
        final Map<Long, Issue> issueMap = new HashMap<Long, Issue>();
        for (Issue issue : searchResults.getIssues())
        {
            issueMap.put(issue.getId(), issue);
        }

        final List<Issue> sortedIssues = new ArrayList<Issue>();
        for (Long issueId : issueIds)
        {
            sortedIssues.add(issueMap.get(issueId));
        }

        searchResults = new SearchResults(sortedIssues, searchResults.getTotal(), pagerFilter);
    }


    /**
     * Returns the layout items for requested columns
     * @param columnNames of requested columns
     *
     * @return the columns that should be visible to the user.
     */
    final protected List<ColumnLayoutItem> getDisplayedColumns(List<String> columnNames)
    {
        return searchColumnsFinder.getDisplayedColumns(user, columnNames).getColumnLayoutItems();
    }

    /**
     * Returns the layout items for a given columnLayout
     * @param columns layout
     * @return the columns that should be visible to the user.
     */
    final List<ColumnLayoutItem> getDisplayedColumns(ColumnLayout columns)
    {
        final QueryContext queryContext = searchService.getQueryContext(user, searchRequest.getQuery());
        try
        {
            return columns.getVisibleColumnLayoutItems(user, queryContext);
        }
        catch (ColumnLayoutException e)
        {
            throw new RuntimeException("Exception thrown while retrieving columns for issue table", e);
        }
    }

    /**
     * Returns the columns that are displayed to the user in list view.
     *
     * @return the columns that should be visible to the user.
     */
    ColumnLayout getColumnLayout()
    {
        return searchColumnsFinder.getDisplayedColumns(user, searchRequest, configuration);
    }

    /**
     * @return whether the JIRA instance contains any issues.
     * @throws SearchException If querying the Lucene index fails.
     */
    private boolean getJIRAHasIssues() throws SearchException
    {
        if (searchResults.getTotal() > 0)
        {
            return true;
        }
        else
        {
            return searchProvider.searchCountOverrideSecurity(new QueryImpl(), user) > 0;
        }
    }

    /**
     * @return the maximum number of issues in a stable search.
     */
    private int getStableSearchResultsLimit()
    {
        return Integer.valueOf(applicationProperties.getDefaultBackedString(APKeys.JIRA_STABLE_SEARCH_MAX_RESULTS));
    }

    /**
     * Calculate the issue table's start index, taking the selected issue into consideration.
     *
     * @param issueKeys The keys of all issues matching the search.
     * @return The index of the first issue to show in the table.
     */
    private int getStartIndex(final List<String> issueKeys)
    {
        int pageSize = configuration.getNumberToShow();
        String selectedIssueKey = configuration.getSelectedIssueKey();

        if (selectedIssueKey != null)
        {
            return Math.max(issueKeys.indexOf(selectedIssueKey), 0) / pageSize * pageSize;
        }
        else
        {
            return configuration.getStart();
        }
    }

    abstract Object getTable();

    /**
     * Validate the issue table request.
     *
     * @return a message set containing any validation errors/warnings.
     */
    @Override
    MessageSet validate()
    {
        //Don't run validation stable search query (we assume its correct).
        if (!fromIssueIds)
        {
            Long filterId = searchRequest != null ? searchRequest.getId() : null;
            return searchService.validateQuery(user, query, filterId);
        }

        return new MessageSetImpl();
    }

    /**
     * Returns the fields for which we display sortable columns on the issue navigator. We generate sort JQL for these
     * columns so that when the user clicks them that instantly changes the sort.
     *
     * @param displayedColumns list of columns to be displayed.
     *
     * @return the columns to generate sort JQL for.
     */
    private List<NavigableField> getSortableColumns(List<ColumnLayoutItem> displayedColumns)
    {
        List<NavigableField> fields = Lists.newArrayList();

        // add the columns that are displayed in list view
        for (ColumnLayoutItem columnLayoutItem : displayedColumns)
        {
            fields.add(columnLayoutItem.getNavigableField());
        }

        // add the columns that are in the ORDER BY clause
        fields.addAll(getOrderByFields());

        return fields;
    }

    /**
     * Returns the NavigableField instances for the fields present in the query's ORDER BY clause.
     *
     * @return NavigableField instances for the fields present in the query's ORDER BY clause.
     */
    private List<NavigableField> getOrderByFields()
    {
        OrderBy orderBy = query.getOrderByClause();
        if (orderBy == null || orderBy.getSearchSorts() == null || orderBy.getSearchSorts().isEmpty())
        {
            return emptyList();
        }

        List<NavigableField> orderByFields = Lists.newArrayList();
        for (SearchSort sort : orderBy.getSearchSorts())
        {
            Collection<String> fieldIds = searchHandlerManager.getFieldIds(sort.getField());
            for (String fieldId : fieldIds)
            {
                Field field = fieldManager.getField(fieldId);
                if (field instanceof NavigableField)
                {
                    orderByFields.add((NavigableField) field);
                }
            }
        }

        return orderByFields;
    }
}
