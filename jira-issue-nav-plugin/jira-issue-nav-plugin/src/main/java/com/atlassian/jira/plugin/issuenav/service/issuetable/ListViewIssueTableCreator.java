package com.atlassian.jira.plugin.issuenav.service.issuetable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.components.orderby.OrderByUtil;
import com.atlassian.jira.components.util.SortJqlGenerator;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.FieldRenderingContext;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutItem;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutManager;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.web.component.IssuePager;
import com.atlassian.jira.web.component.IssueTableLayoutBean;
import com.atlassian.jira.web.component.IssueTableWebComponent;
import com.atlassian.query.Query;

import java.util.List;

/**
 * The list-view issue table.
 *
 * @since v6.0
 */
class ListViewIssueTableCreator extends AbstractIssueTableCreator
{
    public ListViewIssueTableCreator(
            final ApplicationProperties applicationProperties,
            final ColumnLayoutManager columnLayoutManager,
            final IssueTableServiceConfiguration configuration,
            final boolean fromIssueIds,
            final IssueFactory issueFactory,
            final List<Long> issueIds,
            final SortJqlGenerator sortJqlGenerator,
            final Query query,
            final boolean returnIssueIds,
            final SearchHandlerManager searchHandlerManager,
            final SearchProvider searchProvider,
            final SearchProviderFactory searchProviderFactory,
            final SearchRequest searchRequest,
            final SearchService searchService,
            final User user,
            final FieldManager fieldManager,
            final OrderByUtil orderByUtil)
    {
        super(applicationProperties, columnLayoutManager, configuration, fromIssueIds, issueFactory, issueIds,
                sortJqlGenerator, query, returnIssueIds, searchHandlerManager, searchProvider, searchProviderFactory,
                searchRequest, searchService, user, fieldManager, orderByUtil);
    }

    @Override
    protected Object getTable()
    {
        // Don't render if we have no results.
        if (searchResults.getIssues().isEmpty())
        {
            return null;
        }
        else
        {
            IssuePager issuePager = configuration.isPaging() ? searchResults : null;
            return new IssueTableWebComponent().getHtml(createLayout(), searchResults.getIssues(), issuePager);
        }
    }

    private IssueTableLayoutBean createLayout()
    {
        List<ColumnLayoutItem> displayedColumns = getDisplayedColumns(configuration.getColumnNames());

        final IssueTableLayoutBean layout = new IssueTableLayoutBean(displayedColumns, query.getOrderByClause().getSearchSorts());
        layout.addCellDisplayParam(FieldRenderingContext.NAVIGATOR_VIEW, Boolean.TRUE);
        layout.setDisplayHeader(configuration.isDisplayHeader());
        layout.setDisplayHeaderPager(false);
        layout.setShowActionColumn(configuration.isShowActions());
        layout.setShowExteriorTable(false);
        layout.setSortingEnabled(configuration.isEnableSorting());
        layout.setTableCssClass("grid issuetable-db maxWidth");
        return layout;
    }
}
