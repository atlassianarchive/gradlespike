package com.atlassian.jira.plugin.issuenav.util;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.plugin.issuenav.Search;
import com.atlassian.jira.components.issueviewer.service.SystemFilter;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.query.Query;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Calculates the URLs of default searches.
 *
 * @since v5.2
 */
@Named
public class DefaultSearchUtil
{
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final SearchProvider searchProvider;
    private final UserProjectHistoryManager userProjectHistoryManager;

    @Inject
    public DefaultSearchUtil(
            final JiraAuthenticationContext jiraAuthenticationContext,
            final SearchProvider searchProvider,
            final UserProjectHistoryManager userProjectHistoryManager)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.searchProvider = searchProvider;
        this.userProjectHistoryManager = userProjectHistoryManager;
    }

    /**
     * When a user has no session search we should try to show something useful.
     * <ul>
     *     <li>If they have open issues, show the "My Open Issues" filter.</li>
     *     <li>If their current project contains issues, show them.</li>
     *     <li>Otherwise, show all issues in the system.</li>
     * </ul>
     *
     * @return the default search that should be presented.
     */
    public Search getSearch()
    {
        if (currentUserHasOpenIssues())
        {
            return new Search(SystemFilter.MY_OPEN_ISSUES);
        }

        Project currentProject = getCurrentProject();
        if (currentProject != null && projectHasIssues(currentProject))
        {
            return new Search(null, "project = " + currentProject.getKey());
        }

        return new Search(SystemFilter.ALL_ISSUES);
    }

    private Project getCurrentProject()
    {
        User user = jiraAuthenticationContext.getLoggedInUser();
        return userProjectHistoryManager.getCurrentProject(
                Permissions.BROWSE, user);
    }

    private boolean currentUserHasOpenIssues()
    {
        User user = jiraAuthenticationContext.getLoggedInUser();
        if (user != null)
        {
            try
            {
                Query query = getOpenIssuesQuery();
                return searchProvider.searchCount(query, user) > 0;
            }
            catch (SearchException ignored)
            {
            }
        }

        return false;
    }

    // Builds a query for userHasOpenIssues(); this is separate for testing.
    protected Query getOpenIssuesQuery()
    {
        String username = jiraAuthenticationContext.getLoggedInUser().getName();
        return JqlQueryBuilder.newBuilder().where().assigneeUser(username)
                .and().unresolved().buildQuery();
    }

    // Builds a query for projectHasIssues(); this is separate for testing.
    protected Query getProjectQuery(final Project project)
    {
        return JqlQueryBuilder.newBuilder().where().project(project.getKey())
                .buildQuery();
    }

    private boolean projectHasIssues(final Project project)
    {
        try
        {
            User user = jiraAuthenticationContext.getLoggedInUser();
            Query query = getProjectQuery(project);

            return searchProvider.searchCount(query, user) > 0;
        }
        catch (SearchException e)
        {
            return false;
        }
    }
}