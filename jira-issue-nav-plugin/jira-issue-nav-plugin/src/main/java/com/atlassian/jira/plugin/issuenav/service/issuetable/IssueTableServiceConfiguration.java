package com.atlassian.jira.plugin.issuenav.service.issuetable;

import java.util.List;

import static com.atlassian.jira.issue.fields.layout.column.ColumnLayout.ColumnConfig;

public class IssueTableServiceConfiguration
{
    private static final int DEFAULT_NUM_TO_SHOW = 50;

    /*
     * Whether the default columns should be added.
     */
    private boolean addDefaults;

    /*
     * The columns that are to be displayed.
     */
    private List<String> columnNames;

    // The name of the application property that defines the default columns.
    private String context;

    // Whether the header should be displayed.
    private boolean displayHeader = true;

    // Whether sorting columns should be enabled.
    private boolean enableSorting = true;

    // Whether pagination should be enabled.
    private boolean isPaging = true;

    // The key of the layout to render.
    private String layoutKey;

    // The number of issues to show on a page.
    private int numberToShow = DEFAULT_NUM_TO_SHOW;

    // The key of the selected issue; if given, the page containing this issue is rendered and start is ignored
    private String selectedIssueKey;

    // Whether the actions (cog) column should be shown.
    private boolean showActions = true;

    private String sortBy = "";

    // The index of the first issue that is to be displayed.
    private int start;
    private String title ;

    // The requested columns used to generated the issue table
    private ColumnConfig columnConfig = null;

    public boolean isAddDefaults()
    {
        return addDefaults;
    }

    public void setAddDefaults(boolean addDefaults)
    {
        this.addDefaults = addDefaults;
    }

    public List<String> getColumnNames()
    {
        return columnNames;
    }

    public void setColumnNames(List<String> columnNames)
    {
        this.columnNames = columnNames;
    }

    public String getContext()
    {
        return context;
    }

    public void setContext(String context)
    {
        this.context = context;
    }

    public boolean isDisplayHeader()
    {
        return displayHeader;
    }

    public void setDisplayHeader(boolean displayHeader)
    {
        this.displayHeader = displayHeader;
    }

    public boolean isEnableSorting()
    {
        return enableSorting;
    }

    public void setEnableSorting(boolean enableSorting)
    {
        this.enableSorting = enableSorting;
    }

    public boolean isPaging()
    {
        return isPaging;
    }

    public void setPaging(boolean paging)
    {
        isPaging = paging;
    }

    public String getLayoutKey()
    {
        return layoutKey;
    }

    public void setLayoutKey(String layoutKey)
    {
        this.layoutKey = layoutKey;
    }

    public int getNumberToShow()
    {
        return numberToShow;
    }

    public void setNumberToShow(int numberToShow)
    {
        this.numberToShow = numberToShow;
    }

    public String getSelectedIssueKey()
    {
        return selectedIssueKey;
    }

    public void setSelectedIssueKey(final String selectedIssueKey)
    {
        this.selectedIssueKey = selectedIssueKey;
    }

    public boolean isShowActions()
    {
        return showActions;
    }

    public void setShowActions(boolean showActions)
    {
        this.showActions = showActions;
    }

    public String getSortBy()
    {
        return sortBy;
    }

    public void setSortBy(String sortBy)
    {
        this.sortBy = sortBy;
    }

    public int getStart()
    {
        return start;
    }

    public void setStart(int start)
    {
        this.start = start;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public void setColumnConfig(ColumnConfig columnConfig)
    {
        if (columnConfig != null)
        {
            this.columnConfig = columnConfig;
        }
    }

    public ColumnConfig getColumnConfig()
    {
        return columnConfig;
    }
}
