package com.atlassian.jira.plugin.issuenav;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestFactory;
import com.atlassian.jira.issue.search.util.SearchSortUtil;
import com.atlassian.jira.issue.search.util.SearchSortUtilImpl;
import com.atlassian.jira.issue.transport.ActionParams;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.query.Query;
import com.google.common.base.Function;

import java.util.Map;
import javax.annotation.Nullable;

/**
 * Translates old issuenav URLs into JQL.
 *
 * @since v5.2
 */
class LegacyUrlToJql implements Function<ActionParams, String>
{
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private SearchService searchService;
    private final SearchRequestFactory searchRequestFactory;

    LegacyUrlToJql(JiraAuthenticationContext jiraAuthenticationContext, SearchService searchService, SearchRequestFactory searchRequestFactory)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.searchService = searchService;
        this.searchRequestFactory = searchRequestFactory;
    }

    @Override
    public String apply(@Nullable final ActionParams actionParams)
    {
        final User loggedInUser = jiraAuthenticationContext.getLoggedInUser();
        // no existing search request as far as we care
        SearchRequest searchRequest = searchRequestFactory.createFromParameters(null, loggedInUser, actionParams);
        final Query query = searchRequest.getQuery();
        return searchService.getGeneratedJqlString(query);
    }
}
