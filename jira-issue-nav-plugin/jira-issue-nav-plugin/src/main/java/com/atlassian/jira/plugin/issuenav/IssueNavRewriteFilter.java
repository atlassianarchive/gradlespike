package com.atlassian.jira.plugin.issuenav;

import com.atlassian.core.filters.AbstractHttpFilter;
import com.atlassian.jira.bc.admin.ApplicationPropertiesService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.IssueManager;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Redirects to the plugin's issue navigator action {@link IssueNavAction} handling /browse URLs for view issue
 * and /issues URLs for issue search.
 *
 * @since v5.1
 */
public class IssueNavRewriteFilter extends AbstractHttpFilter
{
    private static final String BASE_URL = "/secure/IssueNavAction!default.jspa";
    private static final int GROUP_ROOT = 1;
    private static final int GROUP_ISSUE_KEY = 2;

    private final ApplicationPropertiesService applicationPropertiesService;
    private final IssueManager issueManager;

    public IssueNavRewriteFilter(ApplicationPropertiesService applicationPropertiesService,
                                 IssueManager issueManager)
    {
        this.applicationPropertiesService = applicationPropertiesService;
        this.issueManager = issueManager;
    }

    /**
     * Extract components from a request's URL.
     *
     * @param request The request whose URL is to be parsed.
     * @return A {@link URLComponents} object constructed from {@code request}.
     */
    private URLComponents getURLComponents(final HttpServletRequest request)
    {
        final String contextPath = request.getContextPath();
        final String projectKeyPattern = applicationPropertiesService.getApplicationProperty(APKeys.JIRA_PROJECTKEY_PATTERN).getCurrentValue();
        final String URL = request.getRequestURI();

        final Pattern nonPushStatePattern = Pattern.compile(Pattern.quote(contextPath) + "/(i)");
        final Pattern pushStatePattern = Pattern.compile(Pattern.quote(contextPath) + "/(browse|issues)/(" + projectKeyPattern + "-.*)?");

        Matcher matcher = nonPushStatePattern.matcher(URL);
        if (matcher.matches())
        {
            return new URLComponents(null, matcher.group(GROUP_ROOT));
        }

        matcher = pushStatePattern.matcher(URL);
        if (matcher.matches())
        {
            return new URLComponents(matcher.group(GROUP_ISSUE_KEY), matcher.group(GROUP_ROOT));
        }
        else
        {
            // Instances could have bad project key regex pattern, but we still want to return the view issue page if valid
            final Pattern issueKeyPattern = Pattern.compile(Pattern.quote(contextPath) + "/(browse)/([^/]+-[0-9]+)?");
            matcher = issueKeyPattern.matcher(URL);
            if (matcher.matches() && issueManager.getIssueObject(matcher.group(GROUP_ISSUE_KEY)) != null)
            {
                return new URLComponents(matcher.group(GROUP_ISSUE_KEY), matcher.group(GROUP_ROOT));
            }
        }

        return null;
    }

    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException
    {
        final URLComponents urlComponents = getURLComponents(request);

        if (urlComponents != null) {
            request.setAttribute(IssueNavAction.ISSUE_NAV_ROOT, "/" + urlComponents.getRoot() + "/");
            request.setAttribute(IssueNavAction.ORIGINAL_URL, request.getRequestURI());

            String issueKey = urlComponents.getIssueKey();
            boolean renderViewIssue = isNotBlank(issueKey);

            if (renderViewIssue)
            {
                String url = format("%s?issueKey=%s&serverRenderedViewIssue=true", BASE_URL, issueKey);
                request.getRequestDispatcher(url).forward(request, response);
            }
            else if (!"browse".equals(urlComponents.getRoot()))
            {
                request.getRequestDispatcher(BASE_URL).forward(request, response);
            }
        }
        else
        {
            // we must be on /browse/x where x is a project, component etc.
            filterChain.doFilter(request, response);
        }
    }

    private static class URLComponents
    {
        private final String issueKey;
        private final String root;

        public URLComponents(final String issueKey, final String root)
        {
            this.issueKey = issueKey;
            this.root = root;
        }

        public String getIssueKey()
        {
            return issueKey;
        }

        public String getRoot()
        {
            return root;
        }
    }
}
