package com.atlassian.jira.plugin.issuenav.service.issuetable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.components.util.FilterLookerUpper;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldException;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.index.SearchUnavailableException;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.issue.search.util.SearchSortUtil;
import com.atlassian.jira.jql.ClauseInformation;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.util.JqlCustomFieldId;
import com.atlassian.jira.plugin.issuenav.util.PreferredSearchLayoutService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.component.IssueTableLayoutBean;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.query.Query;
import com.atlassian.query.order.OrderBy;
import com.atlassian.query.order.OrderByImpl;
import com.atlassian.query.order.SearchSort;
import com.atlassian.query.order.SortOrder;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.issue.search.util.SearchSortUtil.SORTER_FIELD;
import static com.atlassian.jira.issue.search.util.SearchSortUtil.SORTER_ORDER;

/**
 * Executes issue searches and returns the results.
 *
 * @since v6.0
 */
@ExportAsService
@Named
public class DefaultIssueTableService implements IssueTableService
{
    private static final Logger log = Logger.getLogger(DefaultIssueTableService.class);

    private static final String COLUMN_NAMES = "columnNames";
    private static final int MAX_JQL_ERRORS = 10;

    private final JiraAuthenticationContext authenticationContext;
    private final FieldManager fieldManager;
    private final IssueTableCreatorFactory issueTableCreatorFactory;
    private final PreferredSearchLayoutService preferredSearchLayoutService;
    private final SearchService searchService;
    private final SearchSortUtil searchSortUtil;
    private final SearchHandlerManager searchHandlerManager;
    private final FilterLookerUpper filterLookerUpper;

    @Inject
    public DefaultIssueTableService(
            final JiraAuthenticationContext jiraAuthenticationContext,
            final FieldManager fieldManager,
            final IssueTableCreatorFactory issueTableCreatorFactory,
            final PreferredSearchLayoutService preferredSearchLayoutService,
            final FilterLookerUpper filterLookerUpper, final SearchService searchService,
            final SearchSortUtil searchSortUtil,
            final SearchHandlerManager searchHandlerManager)
    {
        this.authenticationContext = jiraAuthenticationContext;
        this.fieldManager = fieldManager;
        this.issueTableCreatorFactory = issueTableCreatorFactory;
        this.preferredSearchLayoutService = preferredSearchLayoutService;
        this.searchService = searchService;
        this.searchSortUtil = searchSortUtil;
        this.searchHandlerManager = searchHandlerManager;
        this.filterLookerUpper = filterLookerUpper;
    }

    @Override
    public ServiceOutcome<IssueTableServiceOutcome> getIssueTableFromFilterWithJql(
            String filterId,
            String jql,
            IssueTableServiceConfiguration configuration,
            boolean returnMatchingIssueIds)
    {
        if (null == filterId)
        {
            return getIssueTableFromJQL(jql, configuration, returnMatchingIssueIds);
        }
        else if (null == jql)
        {
            return getIssueTableFromFilter(filterId, configuration, returnMatchingIssueIds);
        }
        else
        {
            final ErrorCollection errorCollection = new SimpleErrorCollection();
            final SearchRequest searchRequest = filterLookerUpper.getSearchRequestFromFilterId(filterId, errorCollection);

            final User user = authenticationContext.getLoggedInUser();
            final SearchService.ParseResult parseResult = searchService.parseQuery(user, jql);

            if (!parseResult.isValid())
            {
                return buildJQLErrorServiceOutcome(parseResult.getErrors());
            }

            return getIssueTable(configuration, parseResult.getQuery(), returnMatchingIssueIds, searchRequest);
        }
    }

    @Override
    public ServiceOutcome<IssueTableServiceOutcome> getIssueTableFromIssueIds(
            String filterId,
            String jql,
            final List<Long> ids,
            final IssueTableServiceConfiguration configuration)
    {
        SearchRequest searchRequest;

        // Construct a search request object with filterId
        // This is to allow the correct retrieval of filter columns if specified in configuration.useFilterColumns
        if (filterId != null)
        {
            final ErrorCollection errorCollection = new SimpleErrorCollection();
            searchRequest = filterLookerUpper.getSearchRequestFromFilterId(filterId, errorCollection);

            if (errorCollection.hasAnyErrors())
            {
                return new ServiceOutcomeImpl<IssueTableServiceOutcome>(errorCollection);
            }
        }
        else
        {
            searchRequest = new SearchRequest();
        }

        Query query = null;
        if (jql != null)
        {
            SearchService.ParseResult parseResult = searchService.parseQuery(authenticationContext.getLoggedInUser(), jql);
            query = parseResult.isValid() ? parseResult.getQuery() : null;
        }
        setPreferredLayoutKey(configuration);
        return createIssueTableFromCreator(issueTableCreatorFactory.getStableIssueTableCreator(
                configuration,
                query,
                ids,
                searchRequest,
                authenticationContext.getLoggedInUser()
        ));
    }

    /**
     * Create an {@link IssueTable} via an {@link IssueTableCreator} and handle any errors.
     *
     * @param creator The object that validates the request and creates the {@link IssueTable}.
     * @return the result of attempting to create an {@link IssueTable} via {@code creator}.
     */
    ServiceOutcome<IssueTableServiceOutcome> createIssueTableFromCreator(IssueTableCreator creator)
    {
        ErrorCollection errorCollection = new SimpleErrorCollection();

        try
        {
            MessageSet validationResult = creator.validate();
            if (validationResult.hasAnyErrors())
            {
                return buildJQLErrorServiceOutcome(validationResult);
            }

            IssueTable issueTable = creator.create();

            // The JQL is valid and the search can be executed, but warnings may
            // have been generated (e.g. referencing a non-existent reporter).
            Collection<String> warnings = validationResult.getWarningMessages();
            return new ServiceOutcomeImpl<IssueTableServiceOutcome>(errorCollection,
                    new IssueTableServiceOutcome(issueTable, warnings));

        }
        catch (SearchUnavailableException e)
        {
            if (!e.isIndexingEnabled())
            {
                final String message = authenticationContext.getI18nHelper().getText("gadget.common.indexing.admin");
                errorCollection.addErrorMessage(message);
            }
            else
            {
                log.error(e);
                final String message = authenticationContext.getI18nHelper().getText("unknown.search.error");
                errorCollection.addErrorMessage(message);
            }
        }
        catch (SearchException e)
        {
            log.error(e);
            final String message = authenticationContext.getI18nHelper().getText("unknown.search.error");
            errorCollection.addErrorMessage(message);
        }

        return new ServiceOutcomeImpl<IssueTableServiceOutcome>(errorCollection);
    }

    ServiceOutcome<IssueTableServiceOutcome> getIssueTableFromJQL(
            String jql,
            IssueTableServiceConfiguration configuration,
            boolean returnMatchingIssueIds)
    {
        // Parse the JQL into a Query object (and to validate it).
        SearchService.ParseResult parseResult = searchService.parseQuery(authenticationContext.getLoggedInUser(), jql);

        if (!parseResult.isValid())
        {
            return buildJQLErrorServiceOutcome(parseResult.getErrors());
        }

        final SearchRequest searchRequest = new SearchRequest(parseResult.getQuery());
        return getIssueTable(configuration, parseResult.getQuery(), returnMatchingIssueIds, searchRequest);
    }

    ServiceOutcome<IssueTableServiceOutcome> getIssueTableFromFilter(
            String filterId,
            IssueTableServiceConfiguration configuration,
            boolean returnMatchingIssueIds)
    {
        final ErrorCollection errorCollection = new SimpleErrorCollection();
        final SearchRequest searchRequest = filterLookerUpper.getSearchRequestFromFilterId(filterId, errorCollection);

        if (errorCollection.hasAnyErrors())
        {
            return new ServiceOutcomeImpl<IssueTableServiceOutcome>(errorCollection);
        }

        return getIssueTable(configuration, searchRequest.getQuery(), returnMatchingIssueIds, searchRequest);
    }

    /**
     * Builds an "error" {@code ServiceOutcome} containing the given errors. If {@code errors} is empty, uses a generic
     * "JQL is invalid" error message.
     *
     * @param messageSet The errors to include.
     * @return An "error" {@code ServiceOutcome} containing {@code errors}.
     */
    private ServiceOutcome<IssueTableServiceOutcome> buildJQLErrorServiceOutcome(MessageSet messageSet)
    {
        final ErrorCollection errors = new SimpleErrorCollection();
        final Iterable<String> errorMessages = Iterables.limit(messageSet.getErrorMessages(), MAX_JQL_ERRORS);

        for (String error : errorMessages)
        {
            errors.addErrorMessage(error);
        }

        if (!errors.hasAnyErrors())
        {
            errors.addErrorMessage(authenticationContext.getI18nHelper().getText("jql.parse.unknown.no.pos"));
        }

        return new ServiceOutcomeImpl<IssueTableServiceOutcome>(errors);
    }

    /**
     * Attempt to construct an {@link IssueTable}.
     *
     * @param configuration The service configuration to use.
     * @param query The query whose results will form the table content.
     * @param returnIssueIds Whether the issues' IDs should be returned.
     * @param searchRequest The search request being executed (may differ from {@code query}).
     * @return the result of attempting to create an {@link IssueTable} from the given arguments.
     */
    ServiceOutcome<IssueTableServiceOutcome> getIssueTable(
            final IssueTableServiceConfiguration configuration,
            final Query query,
            final boolean returnIssueIds,
            SearchRequest searchRequest)
    {
        if (searchRequest == null)
        {
            searchRequest = new SearchRequest(query);
        }

        final Query queryWithOrderBy = addOrderByToSearchRequest(query, configuration.getSortBy());
        setPreferredLayoutKey(configuration);

        return createIssueTableFromCreator(issueTableCreatorFactory.getNormalIssueTableCreator(
                configuration,
                queryWithOrderBy,
                returnIssueIds,
                searchRequest,
                authenticationContext.getLoggedInUser()
        ));
    }

    /**
     * Adds (or updates) a field to the {@code ORDER BY} portion of a query. Maintains the name or cf[id] id of a custom
     * field depending on which was supplied.
     * <p/>
     * If the field is the first orderBy field, switches the direction of the order by query to make it the opposite of
     * the present direction.
     * <p/>
     * The following show the result of adding "project" to different queries:
     * <p/>
     * ORDER BY priority ASC -> ORDER BY project ASC, priority ASC ORDER BY project DESC -> ORDER BY project ASC
     *
     * @param orderBy The existing {@code ORDER BY} that is to be updated.
     * @param field The field that is to be added.
     * @return An updated {@code ORDER BY}.
     */
    @VisibleForTesting
    OrderBy buildOrderBy(final OrderBy orderBy, final NavigableField field)
    {
        final String columnName;

        // Custom fields must be in the format "cf[id]".
        final boolean isCustomField = field instanceof CustomField;
        if (isCustomField)
        {
            CustomField customField = (CustomField) field;
            columnName = JqlCustomFieldId.toString(customField.getIdAsLong());
        }
        else
        {
            Collection<ClauseNames> jqlClauseNames = searchHandlerManager.getJqlClauseNames(field.getId());
            columnName = !jqlClauseNames.isEmpty() ? jqlClauseNames.iterator().next().getPrimaryName() : field.getId();
        }

        final List<SearchSort> searchSorts = Lists.newArrayList(orderBy.getSearchSorts());
        final ClauseInformation clauseInformation = SystemSearchConstants.getClauseInformationById(columnName);
        final SearchSort matchingSearchSort = Iterables.find(searchSorts, new Predicate<SearchSort>()
        {
            @Override
            public boolean apply(SearchSort searchSort)
            {
                final String fieldInSearch = searchSort.getField();
                return (!isCustomField && clauseInformation != null && clauseInformation.getJqlClauseNames().contains(fieldInSearch))
                        || fieldInSearch.equals(columnName) || (isCustomField && ((CustomField) field).getFieldName().equals(fieldInSearch));
            }
        }, null);

        final SortOrder defaultSortOrder = SortOrder.parseString(field.getDefaultSortOrder());
        if (matchingSearchSort != null)
        {
            final boolean isFirstSort = searchSorts.indexOf(matchingSearchSort) == 0;
            final SortOrder oldSortOrder = matchingSearchSort.getSortOrder() == null ? defaultSortOrder : matchingSearchSort.getSortOrder();
            final SortOrder newSortOrder = isFirstSort ? reverseSortOrder(oldSortOrder) : defaultSortOrder;

            searchSorts.remove(matchingSearchSort);
            searchSorts.add(0, new SearchSort(matchingSearchSort.getField(), newSortOrder));
        }
        else
        {
            searchSorts.add(0, new SearchSort(columnName, defaultSortOrder));
        }

        return new OrderByImpl(searchSorts);
    }

    private static SortOrder reverseSortOrder(SortOrder sortOrder)
    {
        return sortOrder == SortOrder.ASC ? SortOrder.DESC : SortOrder.ASC;
    }

    Query addOrderByToSearchRequest(Query preOrderByQuery, String sortBy)
    {
        if (StringUtils.isNotBlank(sortBy))
        {
            String sortDirection = null;
            if (sortBy.endsWith(":DESC") || sortBy.endsWith(":ASC"))
            {
                sortDirection = sortBy.substring(sortBy.lastIndexOf(':') + 1);
                sortBy = sortBy.substring(0, sortBy.lastIndexOf(':'));
            }

            final JqlQueryBuilder queryBuilder = JqlQueryBuilder.newBuilder(preOrderByQuery);

            final String[] sortArray = { sortDirection };
            final String[] fieldArray = { sortBy };

            final MapBuilder<String, String[]> builder = MapBuilder.newBuilder();
            final Map<String, String[]> params = builder.add(SORTER_ORDER, sortArray).add(SORTER_FIELD, fieldArray).toMap();

            final OrderBy newOrder = searchSortUtil.getOrderByClause(params);
            final OrderBy oldOrder = queryBuilder.orderBy().buildOrderBy();

            final User user = authenticationContext.getLoggedInUser();
            final List<SearchSort> newSearchSorts = newOrder.getSearchSorts();
            final List<SearchSort> oldSearchSorts = oldOrder.getSearchSorts();
            final List<SearchSort> sorts = searchSortUtil.mergeSearchSorts(user, newSearchSorts, oldSearchSorts, 3);

            queryBuilder.orderBy().setSorts(sorts);
            return queryBuilder.buildQuery();
        }
        return preOrderByQuery;
    }

    void validateColumnNames(List<String> columnNames, ErrorCollection errors)
    {
        if (columnNames != null && !columnNames.isEmpty())
        {
            try
            {
                List<String> fieldsNotFound = new ArrayList<String>();
                final User user = authenticationContext.getLoggedInUser();
                final Set<NavigableField> availableFields = fieldManager.getAvailableNavigableFields(user);
                for (String columnName : columnNames)
                {
                    // skip the --Default-- column if present
                    if (IssueTableLayoutBean.DEFAULT_COLUMNS.equalsIgnoreCase(columnName))
                    {
                        continue;
                    }

                    boolean found = false;
                    for (NavigableField availableField : availableFields)
                    {
                        if (columnName.equals(availableField.getId()))
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        fieldsNotFound.add(columnName);
                    }
                }
                if (!fieldsNotFound.isEmpty())
                {
                    String fieldsNotFoundString = StringUtils.join(fieldsNotFound, ", ");
                    errors.addError(COLUMN_NAMES, authenticationContext.getI18nHelper().getText("issue.table.service.invalid.columns", fieldsNotFoundString));
                }
            }
            catch (FieldException e)
            {
                throw new RuntimeException(e);
            }
        }
    }

    private void setPreferredLayoutKey(final IssueTableServiceConfiguration configuration)
    {
        if (configuration.getLayoutKey() == null)
        {
            configuration.setLayoutKey(preferredSearchLayoutService.getPreferredSearchLayout());
        }
    }
}
