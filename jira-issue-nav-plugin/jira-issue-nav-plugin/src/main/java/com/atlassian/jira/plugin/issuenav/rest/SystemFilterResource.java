package com.atlassian.jira.plugin.issuenav.rest;

import com.atlassian.jira.components.issueviewer.service.ActionUtilsService;
import com.atlassian.jira.rest.api.http.CacheControl;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * A rest end point to get system filters
 *
 * @since v6.0
 */
@AnonymousAllowed
@Path("systemFilters")
public class SystemFilterResource {
    private final ActionUtilsService actionUtilsService;

    public SystemFilterResource(
            final ActionUtilsService actionUtilsService)
    {
        this.actionUtilsService = actionUtilsService;
    }

    @GET
    public Response get() throws JSONException {
        return Response.ok(actionUtilsService.getSystemFiltersJson())
                .cacheControl(CacheControl.never()).build();
    }
}
