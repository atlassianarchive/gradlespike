package com.atlassian.jira.plugin.issuenav.service.issuetable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.components.orderby.OrderByUtil;
import com.atlassian.jira.components.util.SortJqlGenerator;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutManager;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.query.Query;

import javax.inject.Inject;
import javax.inject.Named;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The default implementation of {@link IssueTableCreatorFactory}.
 *
 * @since v6.0
 */
@Named
public class DefaultIssueTableCreatorFactory implements IssueTableCreatorFactory
{

    final private ApplicationProperties applicationProperties;
    final private ColumnLayoutManager columnLayoutManager;
    final private IssueFactory issueFactory;
    final private SortJqlGenerator sortJqlGenerator;
    final private SearchHandlerManager searchHandlerManager;
    final private SearchProvider searchProvider;
    final private SearchProviderFactory searchProviderFactory;
    final private SearchService searchService;
    final private FieldManager fieldManager;
    final private OrderByUtil orderByUtil;
    private final Map<String, Class<? extends AbstractIssueTableCreator>> issueTableCreators;

    @Inject
    public DefaultIssueTableCreatorFactory(
            final ApplicationProperties applicationProperties,
            final ColumnLayoutManager columnLayoutManager,
            final IssueFactory issueFactory,
            final SortJqlGenerator sortJqlGenerator,
            final SearchHandlerManager searchHandlerManager,
            final SearchProvider searchProvider,
            final SearchProviderFactory searchProviderFactory,
            final SearchService searchService,
            final FieldManager fieldManager,
            final OrderByUtil orderByUtil)
    {
        this.applicationProperties = applicationProperties;
        this.columnLayoutManager = columnLayoutManager;
        this.issueFactory = issueFactory;
        this.sortJqlGenerator = sortJqlGenerator;
        this.searchHandlerManager = searchHandlerManager;
        this.searchProvider = searchProvider;
        this.searchProviderFactory = searchProviderFactory;
        this.searchService = searchService;
        this.fieldManager = fieldManager;
        this.orderByUtil = orderByUtil;
        this.issueTableCreators = new HashMap<String, Class<? extends AbstractIssueTableCreator>>();
        this.issueTableCreators.put("list-view", ListViewIssueTableCreator.class);
        this.issueTableCreators.put("split-view", SplitViewIssueTableCreator.class);
    }

    DefaultIssueTableCreatorFactory(
            final ApplicationProperties applicationProperties,
            final ColumnLayoutManager columnLayoutManager,
            final IssueFactory issueFactory,
            final SortJqlGenerator sortJqlGenerator,
            final SearchHandlerManager searchHandlerManager,
            final SearchProvider searchProvider,
            final SearchProviderFactory searchProviderFactory,
            final SearchService searchService,
            final FieldManager fieldManager,
            final OrderByUtil orderByUtil,
            final Map<String, Class<? extends AbstractIssueTableCreator>> issueTableCreators)
    {
        this.applicationProperties = applicationProperties;
        this.columnLayoutManager = columnLayoutManager;
        this.issueFactory = issueFactory;
        this.sortJqlGenerator = sortJqlGenerator;
        this.searchHandlerManager = searchHandlerManager;
        this.searchProvider = searchProvider;
        this.searchProviderFactory = searchProviderFactory;
        this.searchService = searchService;
        this.fieldManager = fieldManager;
        this.orderByUtil = orderByUtil;
        this.issueTableCreators = issueTableCreators;
    }

    @Override
    public AbstractIssueTableCreator getNormalIssueTableCreator(
            final IssueTableServiceConfiguration configuration,
            final Query query,
            final boolean returnIssueIds,
            final SearchRequest searchRequest,
            final User user)
    {
        return createIssueTableCreator(configuration, false, null, query, returnIssueIds, searchRequest, user);
    }

    @Override
    public AbstractIssueTableCreator getStableIssueTableCreator(
            final IssueTableServiceConfiguration configuration,
            final Query query,
            final List<Long> issueIds,
            final SearchRequest searchRequest,
            final User user)
    {
        return createIssueTableCreator(configuration, true, issueIds, query, true, searchRequest, user);
    }

    /**
     * Attempt to create an {@link IssueTableCreator}.
     *
     * @return a new {@link IssueTableCreator} instance, initialised with the given arguments.
     * @throws IllegalArgumentException If no {@link IssueTableCreator} corresponds to {@code layoutKey}.
     * @throws RuntimeException If creating the instance fails.
     */
    private AbstractIssueTableCreator createIssueTableCreator(
            final IssueTableServiceConfiguration configuration,
            final boolean fromIssueIds,
            final List<Long> issueIds,
            final Query query,
            final boolean returnIssueIds,
            final SearchRequest searchRequest,
            final User user
    )
    {
        Class<? extends AbstractIssueTableCreator> clazz;
        final String errorMessage = "Creating an IssueTableCreator failed.";
        final String layoutKey = configuration.getLayoutKey();

        try
        {
            clazz = issueTableCreators.get(layoutKey);
        }
        catch (NullPointerException e)
        {
            throw new IllegalArgumentException("Invalid layout key \"" + layoutKey + "\".");
        }

        if (clazz == null)
        {
            throw new IllegalArgumentException("Invalid layout key \"" + layoutKey + "\".");
        }

        try
        {
            return (AbstractIssueTableCreator) clazz.getConstructors()[0].newInstance(applicationProperties,
                    columnLayoutManager, configuration, fromIssueIds, issueFactory, issueIds, sortJqlGenerator, query,
                    returnIssueIds, searchHandlerManager, searchProvider, searchProviderFactory, searchRequest,
                    searchService, user, fieldManager, orderByUtil);
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(errorMessage, e);
        }
        catch (IndexOutOfBoundsException e)
        {
            throw new RuntimeException(errorMessage, e);
        }
        catch (InstantiationException e)
        {
            throw new RuntimeException(errorMessage, e);
        }
        catch (InvocationTargetException e)
        {
            throw new RuntimeException(errorMessage, e);
        }
    }
}
