package com.atlassian.jira.plugin.issuenav.rest;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutManager;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutStorageException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestInfo;
import com.atlassian.jira.components.issueviewer.service.SystemFilter;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.web.action.issue.IssueNavigatorToolsHelper;
import com.atlassian.jira.web.action.issue.IssueNavigatorViewsHelper;
import com.atlassian.jira.web.action.issue.IssueSearchLimits;
import com.atlassian.jira.web.action.issue.navigator.ToolOptionGroup;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.RequiresXsrfCheck;
import com.atlassian.query.Query;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

/**
 * Resource for getting issue navigator tools via REST
 */
@AnonymousAllowed
@Path("issueNav/operations")
public class IssueNavToolsResource
{
    private final JiraAuthenticationContext authContext;
    private final SearchService searchService;
    private final SearchRequestService searchRequestService;
    private final PermissionManager permissionManager;
    private final ApplicationProperties applicationProperties;
    private final ColumnLayoutManager columnLayoutManager;
    private final PluginAccessor pluginAccessor;
    private final IssueSearchLimits issueSearchLimits;

    public IssueNavToolsResource(JiraAuthenticationContext authContext,
            SearchService searchService,
            SearchRequestService searchRequestService,
            PermissionManager permissionManager, ApplicationProperties applicationProperties,
            ColumnLayoutManager columnLayoutManager, PluginAccessor pluginAccessor,
            IssueSearchLimits issueSearchLimits)
    {
        this.authContext = authContext;
        this.searchService = searchService;
        this.searchRequestService = searchRequestService;
        this.permissionManager = permissionManager;
        this.applicationProperties = applicationProperties;
        this.columnLayoutManager = columnLayoutManager;
        this.pluginAccessor = pluginAccessor;
        this.issueSearchLimits = issueSearchLimits;
    }

    @POST
    @RequiresXsrfCheck
    @Produces({ MediaType.APPLICATION_JSON })
    @Path("views")
    public Response views(
            @FormParam("filterId") Long filterId,
            @FormParam("jql") String jql,
            @FormParam("modified") boolean modified)
    {
        Map<String, Object> viewOptions = Maps.newHashMap();
        List<ToolOptionGroup> filterOptionGroup = getFilterViewOptions(filterId, jql, modified);

        viewOptions.put("filter", filterOptionGroup);

        return Response.ok(viewOptions).cacheControl(never()).build();
    }

    @POST
    @RequiresXsrfCheck
    @Produces({ MediaType.APPLICATION_JSON })
    @Path("tools")
    public Response tools(
            @FormParam("searchResultsTotal") int searchResultsTotal,
            @FormParam("searchResultsPages") int searchResultsPages,
            @FormParam("filterId") Long filterId,
            @FormParam("jql") String jql,
            @FormParam("useColumns") boolean useColumns,
            @FormParam("skipColumns") boolean skipColumns)
    {
        IssueNavigatorToolsHelper.SearchResultInfo searchResultInfo = new IssueNavigatorToolsHelper.SearchResultInfo(searchResultsTotal, searchResultsPages);

        IssueNavigatorToolsHelper.SearchRequestInfo searchRequestInfo = new IssueNavigatorToolsHelper.SearchRequestInfo(filterId, getQuery(jql),
                useColumns, skipColumns, getOwnerUserName(filterId));

        IssueNavigatorToolsHelper toolsHelper = new IssueNavigatorToolsHelper(authContext, permissionManager,
                applicationProperties, columnLayoutManager, searchRequestInfo, searchResultInfo);

        try
        {
            List<ToolOptionGroup> optionGroups = toolsHelper.getToolOptions();
            return Response.ok(optionGroups).cacheControl(never()).build();
        }
        catch (ColumnLayoutStorageException ex)
        {
            throwError(ErrorCollection.of(ex.getMessage()));
            return null;
        }
    }

    private Query getQuery(String jql)
    {
        if (null == jql)
        {
            return null;
        }

        final SearchService.ParseResult parseResult = searchService.parseQuery(authContext.getLoggedInUser(), jql);
        if (!parseResult.isValid())
        {
            throwError(ErrorCollection.of(parseResult.getErrors().getErrorMessages()));
        }
        return parseResult.getQuery();
    }

    private String getOwnerUserName(Long filterId)
    {

        if (null != filterId && !SystemFilter.isSystemFilter(filterId))
        {
            JiraServiceContext serviceCtx = new JiraServiceContextImpl(authContext.getLoggedInUser());
            SearchRequest searchRequest = searchRequestService.getFilter(serviceCtx, filterId);
            if (serviceCtx.getErrorCollection().hasAnyErrors())
            {
                throwError(ErrorCollection.of(serviceCtx.getErrorCollection()));
            }
            return searchRequest.getOwnerUserName();
        }
        else
        {
            return null;
        }
    }

    private List<ToolOptionGroup> getFilterViewOptions(Long filterId, String jql, boolean modified)
    {
        Query query;
        if (null != jql)
        {
            final SearchService.ParseResult parseResult = searchService.parseQuery(authContext.getLoggedInUser(), jql);
            if (!parseResult.isValid())
            {
                throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST).cacheControl(never()).build());
            }
            query = parseResult.getQuery();
        }
        else
        {
            query = null;
        }
        SearchRequestInfo searchRequestInfo = new SearchRequestInfo(query, filterId, modified);
        return new IssueNavigatorViewsHelper(authContext, pluginAccessor, issueSearchLimits, searchService, searchRequestInfo).getViewOptions(null);
    }

    private void throwError(ErrorCollection errors)
    {
        Response response = Response.status(Response.Status.BAD_REQUEST).entity(errors).cacheControl(never()).build();
        throw new WebApplicationException(response);
    }
}
