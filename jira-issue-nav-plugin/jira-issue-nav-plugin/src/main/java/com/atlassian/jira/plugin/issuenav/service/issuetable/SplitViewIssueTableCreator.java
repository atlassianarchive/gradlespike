package com.atlassian.jira.plugin.issuenav.service.issuetable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.components.orderby.OrderByUtil;
import com.atlassian.jira.components.util.SortJqlGenerator;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.plugin.issuenav.rest.SplitViewIssueBean;
import com.atlassian.query.Query;
import com.google.common.base.Function;

import java.util.List;
import javax.annotation.Nullable;

import static com.google.common.collect.Lists.transform;

/**
 * The split-view issue table.
 *
 * @since v6.0
 */
class SplitViewIssueTableCreator extends AbstractIssueTableCreator
{
    // Converts an Issue object to a SplitViewIssueBean.
    final private static Function<Issue, SplitViewIssueBean> CREATE_ISSUE_BEAN =
            new Function<Issue, SplitViewIssueBean>()
            {
                @Override
                public SplitViewIssueBean apply(@Nullable Issue issue)
                {
                    if (issue == null)
                    {
                        return null;
                    }

                    IssueType issueType = issue.getIssueTypeObject();
                    return new SplitViewIssueBean(
                            issue.getId(),
                            issue.getKey(),
                            issue.getStatusObject().getName(),
                            issue.getSummary(),
                            new SplitViewIssueBean.SplitViewIssueTypeBean(
                                    issueType.getDescription(),
                                    issueType.getName(),
                                    issueType.getCompleteIconUrl()
                            )
                    );
                }
            };

    public SplitViewIssueTableCreator(
            final ApplicationProperties applicationProperties,
            final ColumnLayoutManager columnLayoutManager,
            final IssueTableServiceConfiguration configuration,
            final boolean fromIssueIds,
            final IssueFactory issueFactory,
            final List<Long> issueIds,
            final SortJqlGenerator sortJqlGenerator,
            final Query query,
            final boolean returnIssueIds,
            final SearchHandlerManager searchHandlerManager,
            final SearchProvider searchProvider,
            final SearchProviderFactory searchProviderFactory,
            final SearchRequest searchRequest,
            final SearchService searchService,
            final User user,
            final FieldManager fieldManager,
            final OrderByUtil orderByUtil)
    {
        super(applicationProperties, columnLayoutManager, configuration, fromIssueIds, issueFactory, issueIds,
                sortJqlGenerator, query, returnIssueIds, searchHandlerManager, searchProvider, searchProviderFactory,
                searchRequest, searchService, user, fieldManager, orderByUtil);
    }

    @Override
    protected Object getTable()
    {
        return transform(searchResults.getIssues(), CREATE_ISSUE_BEAN);
    }
}
