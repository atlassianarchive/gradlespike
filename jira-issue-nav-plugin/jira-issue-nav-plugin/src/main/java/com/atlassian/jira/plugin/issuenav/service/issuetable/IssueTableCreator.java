package com.atlassian.jira.plugin.issuenav.service.issuetable;

import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;

/**
 * Strategy interface for creating an {@link IssueTable}.
 *
 * @since v6.0
 */
abstract class IssueTableCreator
{
    /**
     * Validates the search.
     *
     * @return a {@link MessageSet} containing validation errors
     */
    MessageSet validate()
    {
        return new MessageSetImpl();
    }

    /**
     * Runs the search and creates an {@link IssueTable}.
     *
     * @return an {@link IssueTable}.
     * @throws com.atlassian.jira.issue.search.SearchException
     */
    abstract IssueTable create() throws SearchException;
}