package com.atlassian.jira.plugin.issuenav.rest;

import com.atlassian.core.user.preferences.Preferences;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.components.issueviewer.service.SessionSearchService;
import com.atlassian.jira.plugin.issuenav.service.StableSearchResultBean;
import com.atlassian.jira.plugin.issuenav.service.StableSearchService;
import com.atlassian.jira.components.issueviewer.service.SystemFilter;
import com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTableService;
import com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTableServiceConfiguration;
import com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTableServiceOutcome;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.preferences.PreferenceKeys;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.RequiresXsrfCheck;

import java.util.List;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.issue.fields.layout.column.ColumnLayout.ColumnConfig;
import static com.atlassian.jira.plugin.issuenav.rest.Responses.error;
import static com.atlassian.jira.rest.api.http.CacheControl.never;

/**
 * A resource for retrieving issue tables. The table can be requested either with a query or a set of issue IDs.
 *
 * @since v5.1
 */
@AnonymousAllowed
@Path ("issueTable")
public class IssueTableResource
{
    private final JiraAuthenticationContext authContext;
    private final IssueTableService issueTableService;
    private final UserPreferencesManager preferencesManager;
    private final SearchRequestService searchRequestService;
    private final SearchService searchService;
    private final SessionSearchService sessionSearchService;
    private final StableSearchService stableSearchService;
    private final UserPreferencesManager userPreferencesManager;

    public IssueTableResource(
            final JiraAuthenticationContext authContext,
            final IssueTableService issueTableService,
            final UserPreferencesManager preferencesManager,
            final SearchRequestService searchRequestService,
            final SearchService searchService,
            final StableSearchService stableSearchService,
            final SessionSearchService sessionSearchService,
            final UserPreferencesManager userPreferencesManager)
    {
        this.authContext = authContext;
        this.issueTableService = issueTableService;
        this.preferencesManager = preferencesManager;
        this.searchRequestService = searchRequestService;
        this.searchService = searchService;
        this.stableSearchService = stableSearchService;
        this.sessionSearchService = sessionSearchService;
        this.userPreferencesManager = userPreferencesManager;
    }

    @POST
    @RequiresXsrfCheck
    @Produces ({ MediaType.APPLICATION_JSON })
    public Response getIssueTableHtml(
            @FormParam("filterId") Long filterId,
            @FormParam("jql") String jql,
            @FormParam("num") String numberToShow,
            @FormParam("startIndex") @DefaultValue("0") int startIndex,
            @FormParam("layoutKey") String layoutKey,
            @FormParam("columnConfig") String columnConfig)
    {
        if (filterId == null && jql == null)
        {
            return error();
        }

        if (userCanSeeFilter(filterId))
        {
            String filterString = filterId != null ? filterId.toString() : "";

            IssueTableServiceConfiguration config = buildConfiguration(layoutKey, numberToShow, startIndex, null, columnConfig);
            ServiceOutcome<IssueTableServiceOutcome> outcome = issueTableService.getIssueTableFromFilterWithJql(filterString, jql, config, true);

            if (outcome.isValid())
            {
                sessionSearchService.setSessionSearch(jql, filterId, startIndex, config.getNumberToShow());
                return Response.ok(outcome.getReturnedValue()).cacheControl(never()).build();
            }
            else
            {
                return error(outcome);
            }
        }
        else
        {
            return buildPrivateFilterResponse();
        }
    }

    /**
     * Returns the layout-specific issue table json for the list of issue {@code ids} Also known as "stable update"
     *
     * @param ids List of issue ids used to populate the issue table
     * @param jql The jql of the client state at the time of making this REST request. This is used to generate the correct sort JQL.
     * @param filterId The initial filter used to generate the list of ids. Used to generate issue table with
     * filter columns from {@code filterId}
     * @param layoutKey Requested layout
     * @param columns List of column ids used to generate the issue table with specific columns. {@code columnConfig}
     * must be set to "explicit" for this columns to be respected
     * @param columnConfig Requested column config to be used. Allows "user", "filter", "explicit" or "system".
     */
    @Path("stable")
    @POST
    @RequiresXsrfCheck
    @Produces ({ MediaType.APPLICATION_JSON })
    public Response getIssueTableHtml(
            @FormParam ("id") List<Long> ids,
            @FormParam ("jql") String jql,
            @FormParam ("filterId") String filterId,
            @FormParam ("layoutKey") String layoutKey,
            @FormParam ("columns") List<String> columns,
            @FormParam ("columnConfig") String columnConfig)
    {
        IssueTableServiceConfiguration config = buildConfiguration(layoutKey, String.valueOf(ids.size()), 0, columns, columnConfig);

        ServiceOutcome<StableSearchResultBean> outcome = stableSearchService.getIssueTableFromIssueIds(filterId, jql, ids, config);

        if (outcome.isValid())
        {
            return Response.ok(outcome.getReturnedValue()).cacheControl(never()).build();
        }
        else
        {
            return error(outcome);
        }
    }

    /**
     * Set the requesting user's session search.
     *
     * @param filterId The ID of the filter to put into session search.
     * @param jql The JQL query to put into session search.
     * @return 200 OK if session search was set; otherwise 400 Bad Request.
     */
    @PUT
    @Path ("sessionSearch")
    @Produces ({ MediaType.APPLICATION_JSON })
    public Response setSessionSearch(@FormParam ("filterId") Long filterId, @FormParam ("jql") String jql)
    {
        if (filterId != null)
        {
            User user = authContext.getLoggedInUser();
            SearchRequest filter = searchRequestService.getFilter(new JiraServiceContextImpl(user), filterId);

            if (filter == null)
            {
                return error();
            }

            if (jql != null)
            {
                SearchService.ParseResult result = searchService.parseQuery(user, jql);

                if (!result.isValid())
                {
                    return error();
                }

                if (filter.getQuery().equals(result.getQuery()))
                {
                    jql = null;
                }
            }
        }

        sessionSearchService.setSessionSearch(jql, filterId, getPageStart(), getPageSize());
        return Response.ok().build();
    }

    private int getPageSize()
    {
        Preferences preferences = userPreferencesManager.getExtendedPreferences(authContext.getUser());

        if (preferences != null)
        {
            return (int) preferences.getLong(PreferenceKeys.USER_ISSUES_PER_PAGE);
        }
        else
        {
            return 0;
        }
    }

    private int getPageStart()
    {
        PagerFilter pagerFilter = sessionSearchService.getPagerFilter();
        return pagerFilter != null ? pagerFilter.getStart() : 0;
    }

    private IssueTableServiceConfiguration createConfig(
            final String layoutKey,
            final String numberToShow,
            int startIndex,
            ColumnConfig columnConfig,
            List<String> columnNames)
    {
        final Preferences preferences = preferencesManager.getExtendedPreferences(authContext.getUser());
        IssueTableServiceConfiguration configuration = new IssueTableServiceConfiguration();
        configuration.setEnableSorting(true);
        configuration.setLayoutKey(layoutKey);
        configuration.setPaging(true);
        configuration.setStart(startIndex);
        configuration.setColumnNames(columnNames);
        configuration.setColumnConfig(columnConfig);

        if (preferences.getBoolean(PreferenceKeys.USER_SHOW_ACTIONS_IN_NAVIGATOR))
        {
            configuration.setShowActions(true);
        }
        // If it was explicitly requested that we display a certain number of
        // issues, then do that; otherwise fall back to the user's preference.
        if (numberToShow != null)
        {
            configuration.setNumberToShow(Integer.valueOf(numberToShow));
        }
        else
        {
            configuration.setNumberToShow(getPageSize());
        }
        return configuration;
    }

    private IssueTableServiceConfiguration buildConfiguration(String layoutKey, String numberToShow, int startIndex, List<String> explicitColumns, String columnConfigName)
    {
        ColumnConfig columnConfig = ColumnConfig.byValueIgnoreCase(columnConfigName);
        IssueTableServiceConfiguration config = createConfig(layoutKey, numberToShow, startIndex, columnConfig, null);

        if (ColumnConfig.EXPLICIT == columnConfig)
        {
            config.setColumnNames(explicitColumns);
        }
        return config;
    }

    /**
     * Determine if a filter is private.
     *
     * @param filterId The ID of the filter.
     * @return {@code true} if the filter cannot be seen by the current user.
     */
    private boolean userCanSeeFilter(Long filterId)
    {
        if (filterId != null && !SystemFilter.isSystemFilter(filterId))
        {
            ApplicationUser user = authContext.getUser();
            final JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(user);
            final SearchRequest filter = searchRequestService.getFilter(jiraServiceContext, filterId);
            return filter != null;
        }
        return true;
    }

    /**
     * @return an error response for when a private filter is requested.
     */
    private Response buildPrivateFilterResponse()
    {
        String key;
        if (authContext.isLoggedInUser())
        {
            key = "issue.nav.filters.filter.private";
        }
        else
        {
            key = "issue.nav.filters.filter.private.anonymous";
        }
        return error(authContext.getI18nHelper().getText(key));
    }
}
