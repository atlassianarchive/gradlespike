package com.atlassian.jira.plugin.issuenav.rest;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.components.issueviewer.service.SystemFilter;
import com.atlassian.jira.components.issueviewer.service.SystemFilterService;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

@AnonymousAllowed
@Path("issueNav/anonymousAccess")
public class IssueNavAnonymousAccessResource
{
    private final IssueService issueService;
    private final SystemFilterService systemFilterService;

    public IssueNavAnonymousAccessResource(IssueService issueService, SystemFilterService systemFilterService)
    {
        this.issueService = issueService;
        this.systemFilterService = systemFilterService;
    }

    @GET
    @Path("{issueKey}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response hasAccess(@PathParam("issueKey") String issueKey, @QueryParam("filterId") Long filterId)
    {
        IssueService.IssueResult result = issueService.getIssue(null, issueKey);

        if (!result.isValid())
        {
            ErrorCollection.Reason worstReason = ErrorCollection.Reason.getWorstReason(result.getErrorCollection().getReasons());

            if (worstReason == ErrorCollection.Reason.NOT_LOGGED_IN)
            {
                return createUnauthorizedResponse();
            }

            return Response.status(Response.Status.BAD_REQUEST).cacheControl(never()).build();
        }

        if (filterId != null)
        {
            SystemFilter systemFilter = systemFilterService.getById(filterId);
            if (systemFilter != null && systemFilter.isRequiresLogin())
            {
                return createUnauthorizedResponse();
            }
        }

        return Response.ok("{\"accessible\": \"true\"}").cacheControl(never()).build();
    }

    private static Response createUnauthorizedResponse()
    {
        return Response.status(Response.Status.UNAUTHORIZED).cacheControl(never()).build();
    }
}
