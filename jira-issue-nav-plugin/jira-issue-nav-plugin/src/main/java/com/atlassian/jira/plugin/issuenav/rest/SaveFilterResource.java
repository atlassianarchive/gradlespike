package com.atlassian.jira.plugin.issuenav.rest;

import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.rest.api.http.CacheControl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugins.rest.common.security.RequiresXsrfCheck;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

/**
 * @since v5.1
 */
@Path("saveFilter")
public class SaveFilterResource
{
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final SearchRequestService searchRequestService;
    private final SearchService searchService;

    public SaveFilterResource(JiraAuthenticationContext jiraAuthenticationContext,
                              SearchRequestService searchRequestService, SearchService searchService) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.searchRequestService = searchRequestService;
        this.searchService = searchService;
    }

    @POST
    @RequiresXsrfCheck
    public Response set(@FormParam("jql") String filterJql, @FormParam("name") String filterName)
    {
        try
        {
            ApplicationUser user = jiraAuthenticationContext.getUser();
            if (user == null)
            {
                return fail(UNAUTHORIZED);
            }
            else
            {
                //create search request
                SearchRequest filterQuery = new SearchRequest(searchService.parseQuery(user.getDirectoryUser(), filterJql).getQuery());
                filterQuery.setName(filterName);
                //set owner
                filterQuery.setOwner(user);
                //save the filter
                SearchRequest newSearchRequest = searchRequestService.createFilter(new JiraServiceContextImpl(user), filterQuery, true);

                //get the filter object
                return createResponse(newSearchRequest);
            }
        }
        catch (Exception e)
        {
            return fail(BAD_REQUEST);
        }
    }

    /**
     * Returns the new filter to the user.
     */
    private static Response createResponse(SearchRequest searchRequest)
    {
        return Response.ok(searchRequest).cacheControl(CacheControl.never()).build();
    }

    private static Response fail(Response.Status status)
    {
        return Response.status(status).cacheControl(CacheControl.never()).build();
    }
}
