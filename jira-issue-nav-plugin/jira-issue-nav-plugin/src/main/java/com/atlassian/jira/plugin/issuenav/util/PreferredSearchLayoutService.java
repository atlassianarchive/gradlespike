package com.atlassian.jira.plugin.issuenav.util;

import com.atlassian.core.AtlassianCoreException;
import com.atlassian.core.user.preferences.Preferences;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.components.issueviewer.service.ActionUtilsService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.preferences.UserPreferencesManager;

import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.util.velocity.VelocityRequestSession;
import org.apache.log4j.Logger;

import javax.inject.Named;
import javax.inject.Inject;

/**
 * Provides methods to get and set the current user's preferred search layout.
 * <p/>
 * The user's preference is stored in their session and, if they're authenticated, as a user preference.
 *
 * @since v6.0
 */
@Named
public class PreferredSearchLayoutService
{
    final private static Logger log = Logger.getLogger(PreferredSearchLayoutService.class);

    final public static String LIST_VIEW_LAYOUT = "list-view";
    final public static String PREFERRED_SEARCH_LAYOUT_KEY = "jira.issues.preferred.layout.key";
    final public static String SPLIT_VIEW_LAYOUT = "split-view";

    final private ActionUtilsService actionUtilsService;
    final private JiraAuthenticationContext jiraAuthenticationContext;
    final private UserPreferencesManager userPreferencesManager;
    final private VelocityRequestContextFactory velocityRequestContextFactory;

    @Inject
    public PreferredSearchLayoutService(
            final ActionUtilsService actionUtilsService,
            final JiraAuthenticationContext jiraAuthenticationContext,
            final UserPreferencesManager userPreferencesManager,
            final VelocityRequestContextFactory velocityRequestContextFactory)
    {
        this.actionUtilsService = actionUtilsService;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.userPreferencesManager = userPreferencesManager;
        this.velocityRequestContextFactory = velocityRequestContextFactory;
    }

    /**
     * Retrieve the default search layout based on dark feature and user
     *
     * @return the default preferred search layout.
     */
    private String getDefaultPreferredSearchLayout()
    {
        return SPLIT_VIEW_LAYOUT;
    }

    /**
     * Retrieve the current user's preferred search layout.
     * <p/>
     * For anonymous users, the default layout is returned.
     *
     * @return the current user's preferred search layout.
     */
    public String getPreferredSearchLayout()
    {
        String layoutKey;
        Preferences preferences = getPreferences();
        if (preferences != null)
        {
            layoutKey = preferences.getString(PREFERRED_SEARCH_LAYOUT_KEY);
            if (layoutKey != null)
            {
                return layoutKey;
            }
        }

        // Try to get it out of the session (handles anonymous users).
        layoutKey = (String)getSession().getAttribute(PREFERRED_SEARCH_LAYOUT_KEY);
        if (layoutKey != null)
        {
            return layoutKey;
        }

        return getDefaultPreferredSearchLayout();
    }

    /**
     * Set the current user's preferred search layout.
     * <p/>
     * If the current user is anonymous, this method does nothing.
     *
     * @param layoutKey The key of the user's preferred search layout.
     */
    public void setPreferredSearchLayout(final String layoutKey)
    {
        // Store it in the user's session (handles anonymous users).
        getSession().setAttribute(PREFERRED_SEARCH_LAYOUT_KEY, layoutKey);

        // And in their preferences.
        Preferences preferences = getPreferences();
        if (preferences != null)
        {
            try
            {
                preferences.setString(PREFERRED_SEARCH_LAYOUT_KEY, layoutKey);
            }
            catch (AtlassianCoreException e)
            {
                log.warn("Couldn't store a user's preferred search layout in user preferences.");
            }
        }
    }

    /**
     * @return the current user's preferences or {@code null} if anonymous.
     */
    private Preferences getPreferences()
    {
        final User user = jiraAuthenticationContext.getLoggedInUser();
        if (user != null)
        {
            return userPreferencesManager.getPreferences(user);
        }
        else
        {
            return null;
        }
    }

    /**
     * @return the current user's session.
     */
    private VelocityRequestSession getSession()
    {
        return velocityRequestContextFactory.getJiraVelocityRequestContext().getSession();
    }
}