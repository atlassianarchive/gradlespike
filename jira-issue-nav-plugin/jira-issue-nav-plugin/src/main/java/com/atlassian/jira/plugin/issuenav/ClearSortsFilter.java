package com.atlassian.jira.plugin.issuenav;

import com.atlassian.core.filters.AbstractHttpFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Redirects to the plugin's clear sorts handler.
 *
 * @since v5.2
 */
public class ClearSortsFilter extends AbstractHttpFilter
{
    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException
    {
        request.getRequestDispatcher("/secure/IssueNavAction!clearSorts.jspa").forward(request, response);
    }
}
