package com.atlassian.jira.plugin.issuenav.rest;

import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.rest.api.util.ErrorCollection;

import javax.ws.rs.core.Response;

import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

/**
 * Convenience methods for REST end points in JIRA.
 *
 * @since v6.1
 */
public class Responses
{
    public static Response error()
    {
        return Response.status(BAD_REQUEST).cacheControl(never()).build();
    }

    public static Response error(final String... message)
    {
        return error(ErrorCollection.of(message));
    }

    public static Response error(final ServiceResult outcome)
    {
        return error(ErrorCollection.of(outcome.getErrorCollection()));
    }

    public static Response error(ErrorCollection errors)
    {
        return Response.status(BAD_REQUEST).entity(errors).cacheControl(never()).build();
    }
}
