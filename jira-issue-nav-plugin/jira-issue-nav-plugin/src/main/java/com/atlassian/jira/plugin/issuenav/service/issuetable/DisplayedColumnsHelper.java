package com.atlassian.jira.plugin.issuenav.service.issuetable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayout;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutItem;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutItemImpl;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutManager;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutStorageException;
import com.atlassian.jira.issue.fields.layout.column.UserColumnLayoutImpl;
import com.atlassian.jira.issue.search.SearchRequest;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.jira.issue.fields.layout.column.ColumnLayout.ColumnConfig;

/**
 * Returns the displayed columns for a user's search.
 *
 * @since 5.2
 */
class DisplayedColumnsHelper
{
    private final ColumnLayoutManager columnLayoutManager;
    private final FieldManager fieldManager;

    DisplayedColumnsHelper(ColumnLayoutManager columnLayoutManager, FieldManager fieldManager)
    {
        this.columnLayoutManager = columnLayoutManager;
        this.fieldManager = fieldManager;
    }

    /**
     * Returns the columns that are displayed to a user for a given SearchRequest.
     *
     * @return the columns that should be visible to the user.
     */
    ColumnLayout getDisplayedColumns(User user, SearchRequest searchRequest, IssueTableServiceConfiguration configuration)
    {
        try
        {
            ColumnLayout columnLayout;
            ColumnConfig config = configuration.getColumnConfig();
            final boolean filterRequested = searchRequest.isLoaded();
            final List<String> columnNames = configuration.getColumnNames();
            if (columnNames != null)
            {
                columnLayout = getDisplayedColumns(user, columnNames);
            }
            else if (filterRequested && (config == ColumnConfig.FILTER || config == null))
            {
                columnLayout = columnLayoutManager.getColumnLayout(user, searchRequest);
            }
            else if (config == ColumnConfig.SYSTEM)
            {
                columnLayout = columnLayoutManager.getEditableDefaultColumnLayout();
            }
            else
            {
                columnLayout = columnLayoutManager.getColumnLayout(user);
            }

            return columnLayout;
        }
        catch (ColumnLayoutStorageException e)
        {
            throw new RuntimeException("Exception thrown while retrieving columns for issue table", e);
        }
    }

    /**
     * Gets columns without magic. Give it a list of columns to get and it will return column layout items for the ones
     * you have permission to view.
     *
     * @return column layout items user has permissions to view
     */
    ColumnLayout getDisplayedColumns(User user, List<String> columnNames)
    {
        List<ColumnLayoutItem> columnLayoutItems = new ArrayList<ColumnLayoutItem>();
        int pos = 0;

        for (String id : columnNames)
        {
            if (fieldManager.isNavigableField(id))
            {
                columnLayoutItems.add(new ColumnLayoutItemImpl(fieldManager.getNavigableField(id), pos));
                pos++;
            }
        }
        return new UserColumnLayoutImpl(columnLayoutItems, user, ColumnConfig.EXPLICIT);
    }
}
