package com.atlassian.jira.plugin.issuenav.service;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTable;
import com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTableServiceOutcome;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

/**
 * Response from StableSearchResource and result of StableSearchService
 *
 * @since v6.0
 */
@XmlRootElement
@SuppressWarnings ("UnusedDeclaration")
public class StableSearchResultBean
{
    @XmlElement
    private IssueTable issueTable;

    @XmlElement
    private Collection<String> warnings;

    public StableSearchResultBean()
    {
    }

    public StableSearchResultBean(ServiceOutcome<IssueTableServiceOutcome> issueTableOutcome)
    {
        if (issueTableOutcome.isValid())
        {
            IssueTableServiceOutcome value = issueTableOutcome.getReturnedValue();
            issueTable = value.getIssueTable();
            warnings = value.getWarnings();
        }
        else
        {
            issueTable = null;
            warnings = null;
        }
    }

    public IssueTable getIssueTable() {
        return issueTable;
    }

    public void setIssueTable(IssueTable issueTable) {
        this.issueTable = issueTable;
    }

    public Collection<String> getWarnings() {
        return warnings;
    }

    public void setWarnings(Collection<String> warnings) {
        this.warnings = warnings;
    }
}
