package com.atlassian.jira.plugin.issuenav;

import com.atlassian.analytics.api.annotations.Analytics;
import com.atlassian.core.filters.AbstractHttpFilter;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.search.SearchRequestFactory;
import com.atlassian.jira.issue.transport.impl.IssueNavigatorActionParams;
import com.atlassian.jira.components.query.util.UserSearchModeService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.UrlBuilder;
import org.apache.log4j.Logger;
import org.tuckey.web.filters.urlrewrite.utils.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Pattern;

import static com.atlassian.jira.util.JiraUrlCodec.encode;

/**
 * Redirects to kickass from the old issue nav
 *
 * @since v5.1
 */
public class KickassRedirectFilter extends AbstractHttpFilter
{
    private static final Logger log = Logger.getLogger(KickassRedirectFilter.class);

    private final JiraAuthenticationContext authContext;

    private final EventPublisher eventPublisher;
    private final UserSearchModeService userSearchModeService;
    private SearchRequestFactory searchRequestFactory;
    private SearchService searchService;

    // note the two layers of escaping
    private static final Pattern BACKSLASH_OR_DOUBLEQUOTE = Pattern.compile("([\\\\\"])");

    public KickassRedirectFilter(
            final JiraAuthenticationContext authContext,
            final EventPublisher eventPublisher,
            final SearchRequestFactory searchRequestFactory,
            final SearchService searchService,
            final UserSearchModeService userSearchModeService)
    {
        this.authContext = authContext;
        this.eventPublisher = eventPublisher;
        this.searchRequestFactory = searchRequestFactory;
        this.searchService = searchService;
        this.userSearchModeService = userSearchModeService;
    }

    @Override
    protected void doFilter(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException
    {
        final NavigateToIssueNavigatorEvent navigateToIssueNavigatorEvent = new NavigateToIssueNavigatorEvent();

        navigateToIssueNavigatorEvent.setKickassEnabled(true);
        UrlBuilder redirectUrl = new UrlBuilder(request.getContextPath() + "/issues/");

        // Set the user's preferred search mode if they requested a
        // mode-specific page like IssueNavigator!executeAdvanced.jspa.
        setSearchModeFromPath(request.getServletPath());

        // Get pager index
        final String pagerStart = request.getParameter("pager/start");

        // Check if the user was starting a new search, looking at a
        // filter, or looking at JQL. We want to redirect accordingly.
        final String filterId = request.getParameter("requestId");
        String jqlQuery = request.getParameter("jqlQuery");

        boolean createNew = "true".equals(request.getParameter("createNew"));
        if (createNew)
        {
            navigateToIssueNavigatorEvent.setCreateNewSearch(true);
            redirectUrl.addParameter("jql", "");
        }
        else if (filterId != null)
        {
            navigateToIssueNavigatorEvent.setSpecifiedFilter(true);
            redirectUrl.addParameter("filter", filterId);
        }
        else if (jqlQuery != null)
        {
            navigateToIssueNavigatorEvent.setSpecifiedJql(true);
            redirectUrl.addParameter("jql", jqlQuery);
        }
        else
        {
            // if there are parameters then they are assumed to be legacy search params (e.g. pid)
            final String searchString = request.getParameter("searchString");
            if ("true".equals(request.getParameter("usedQuickSearch")))
            {
                // this corresponds to a quicksearch request that was interpreted into a "smart query"
                LegacyUrlToJql lut = new LegacyUrlToJql(authContext, searchService, searchRequestFactory);
                String jql = lut.apply(new IssueNavigatorActionParams(request.getParameterMap()));

                redirectUrl
                        .addParameterUnsafe("jql", encodeRight(jql))
                        .addParameterUnsafe("quickSearchQuery", encodeRight(searchString));
                navigateToIssueNavigatorEvent.setUsedQuickSearch(true);
                navigateToIssueNavigatorEvent.setUsedSmartQuery(true);
            }
            else if (searchString != null)
            {
                // quicksearch with no smart querying
                String cleanQuery = "";
                if (!StringUtils.isBlank(searchString))
                {
                    cleanQuery = jqlEncode(searchString);
                    cleanQuery = encodeRight("text ~ \"" + cleanQuery + "\"");
                }
                redirectUrl.addParameterUnsafe("jql", cleanQuery);
                navigateToIssueNavigatorEvent.setUsedQuickSearch(true);
            }
            else
            {
                // legacy url (that is not a quicksearch)
                LegacyUrlToJql lut = new LegacyUrlToJql(authContext, searchService, searchRequestFactory);
                String jql = lut.apply(new IssueNavigatorActionParams(request.getParameterMap()));

                if (StringUtils.isBlank(jql))
                {
                    redirectUrl.addParameter("redirectedFromClassic", "");
                }
                else
                {
                    redirectUrl.addParameterUnsafe("jql", encodeRight(jql));
                    navigateToIssueNavigatorEvent.setUsedLegacyUrl(true);
                }
            }
        }

        // Add start index
        if (!StringUtils.isBlank(pagerStart))
        {
            redirectUrl.addParameter("startIndex", pagerStart);
        }

        response.sendRedirect(redirectUrl.toString());

        eventPublisher.publish(navigateToIssueNavigatorEvent);
    }

    private void setSearchModeFromPath(final String path)
    {
        if (path.contains("executeAdvanced.jspa"))
        {
            userSearchModeService.setSearchMode(UserSearchModeService.ADVANCED);
        }
    }

    private String jqlEncode(final String searchString)
    {
        if (StringUtils.isBlank(searchString)) return "";
        // warning: two layers of escaping here (RTFM)
        return BACKSLASH_OR_DOUBLEQUOTE.matcher(searchString.trim()).replaceAll("\\\\$1");
    }

    private String encodeRight(final String urlEncodeMe)
    {
        if (StringUtils.isBlank(urlEncodeMe)) return "";
        // + is toxic
        return encode(urlEncodeMe).replaceAll("\\+", "%20");
    }

    @Analytics("kickass.navigateToIssueNavigator")
    public static class NavigateToIssueNavigatorEvent
    {
        private boolean kickassEnabled = false;
        private boolean usedQuickSearch = false;
        private boolean usedSmartQuery = false;
        private boolean createNewSearch = false;
        private boolean specifiedJql = false;
        private boolean specifiedFilter = false;
        private boolean usedLegacyUrl;

        public boolean isKickassEnabled()
        {
            return kickassEnabled;
        }

        public void setKickassEnabled(boolean kickassEnabled)
        {
            this.kickassEnabled = kickassEnabled;
        }

        public boolean isUsedQuickSearch()
        {
            return usedQuickSearch;
        }

        public void setUsedQuickSearch(boolean usedQuickSearch)
        {
            this.usedQuickSearch = usedQuickSearch;
        }

        public boolean isUsedSmartQuery()
        {
            return usedSmartQuery;
        }

        public void setUsedSmartQuery(boolean usedSmartQuery)
        {
            this.usedSmartQuery = usedSmartQuery;
        }

        public boolean isCreateNewSearch()
        {
            return createNewSearch;
        }

        public void setCreateNewSearch(boolean createNewSearch)
        {
            this.createNewSearch = createNewSearch;
        }

        public boolean isSpecifiedJql()
        {
            return specifiedJql;
        }

        public void setSpecifiedJql(boolean specifiedJql)
        {
            this.specifiedJql = specifiedJql;
        }

        public boolean isSpecifiedFilter()
        {
            return specifiedFilter;
        }

        public void setSpecifiedFilter(boolean specifiedFilter)
        {
            this.specifiedFilter = specifiedFilter;
        }

        public boolean isUsedLegacyUrl() {
            return usedLegacyUrl;
        }

        public void setUsedLegacyUrl(boolean usedLegacyUrl) {
            this.usedLegacyUrl = usedLegacyUrl;
        }
    }
}
