package com.atlassian.jira.plugin.issuenav.action;

import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.operation.IssueOperation;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import java.util.HashMap;
import java.util.Map;

public class MoveIssueLinkAction extends JiraWebActionSupport implements OperationContext
{
    private final String ISSUE = "issue";

    private final SubTaskManager subTaskManager;
    private final IssueManager issueManager;

    private Long id;
    private Long currentSubTaskSequence;
    private Long subTaskSequence;
    private Boolean disableRedirect;
    private Map<String, Object> viewIssueRenderData;

    public MoveIssueLinkAction(final IssueManager issueManager, final SubTaskManager subTaskManager)
    {
        this.issueManager = issueManager;
        this.subTaskManager = subTaskManager;
    }

    public String doMoveIssueLink() throws Exception
    {
        final Long id = getId();
        if (id == null)
        {
            log.error("Cannot move sub-task when no parent issue exists.");
            return getError(getText("viewissue.error.moveissuelink.no.parent.exist"));
        }
        final MutableIssue issue = getIssue();
        if (!isAllowedReorderSubTasks(issue))
        {
            getError(getText("viewissue.error.moveissuelink.nopermission"));
        }

        if (getCurrentSubTaskSequence() == null)
        {
            log.error("Cannot move sub-task when current sequence is unset.");
            return getError(getText("viewissue.error.moveissuelink.no.sequence.unset"));
        }
        if (getSubTaskSequence() == null)
        {
            log.error("Cannot move sub-task when sequence is unset.");
            return getError(getText("viewissue.error.moveissuelink.no.sequence.unset"));
        }
        subTaskManager.moveSubTask(issue, getCurrentSubTaskSequence(), getSubTaskSequence());
        if (getDisableRedirect() != null && getDisableRedirect())
        {
            return getEmptyResponse();
        }
        return getRedirect("/browse/" + issue.getKey());
    }

    private String getError(String message)
    {
        viewIssueRenderData = new HashMap<String, Object>();
        viewIssueRenderData.put("errorViewIssue", true);
        viewIssueRenderData.put("errorType", "viewIssueError");
        viewIssueRenderData.put("title", getText("viewissue.error.moveissuelink.title"));
        viewIssueRenderData.put("body", message);
        viewIssueRenderData.put("error", "error");
        return ISSUE;
    }

    private MutableIssue getIssue()
    {
        return issueManager.getIssueObject(getId());
    }

    public boolean isServerRenderedViewIssue()
    {
        return true;
    }

    public Map<String, Object> getIssueSearchAndViewRenderData()
    {
        return viewIssueRenderData;
    }

    public boolean isAllowedReorderSubTasks(MutableIssue issue)
    {
        return hasIssuePermission(Permissions.EDIT_ISSUE, issue);
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getCurrentSubTaskSequence()
    {
        return currentSubTaskSequence;
    }

    public void setCurrentSubTaskSequence(final Long currentSubTaskSequence)
    {
        this.currentSubTaskSequence = currentSubTaskSequence;
    }

    public Long getSubTaskSequence()
    {
        return subTaskSequence;
    }

    public void setSubTaskSequence(final Long subTaskSequence)
    {
        this.subTaskSequence = subTaskSequence;
    }

    public Boolean getDisableRedirect()
    {
        return disableRedirect;
    }

    public void setDisableRedirect(final Boolean disableRedirect)
    {
        this.disableRedirect = disableRedirect;
    }

    @Override
    public Map<String, Object> getFieldValuesHolder()
    {
        return null;
    }

    @Override
    public IssueOperation getIssueOperation()
    {
        return null;
    }
}
