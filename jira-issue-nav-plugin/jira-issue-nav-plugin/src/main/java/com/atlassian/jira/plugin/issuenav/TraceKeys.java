package com.atlassian.jira.plugin.issuenav;

import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;

/**
 * This class is responsible for writing &lt;meta&gt; elements to the page, to be used by some of the KA page objects.
 */
class TraceKeys
{
    private final JiraWebResourceManager jiraWebResourceManager;

    /**
     * Writes the KA trace keys to the page using {@link JiraWebResourceManager#putMetadata(String, String)}.
     */
    static void write(JiraWebResourceManager jiraWebResourceManager)
    {
        new TraceKeys(jiraWebResourceManager).write();
    }

    TraceKeys(JiraWebResourceManager jiraWebResourceManager)
    {
        this.jiraWebResourceManager = jiraWebResourceManager;
    }

    /**
     * Writes the KA trace keys to the page using {@link JiraWebResourceManager#putMetadata(String, String)}.
     */
    void write()
    {
        Traces keys = new AjaxTraces();

        jiraWebResourceManager.putMetadata("view-issue-trace-key", keys.viewIssue().name());
        jiraWebResourceManager.putMetadata("view-issue-psycho-key", keys.viewIssue().psychoName());
        if (keys.viewIssue().refreshedCachedName() != null)
        {
            jiraWebResourceManager.putMetadata("view-issue-refreshed-cached-key", keys.viewIssue().refreshedCachedName());
        }
        jiraWebResourceManager.putMetadata("return-to-search-trace-key", keys.returnToSearch().name());
        jiraWebResourceManager.putMetadata("return-to-search-psycho-key", keys.returnToSearch().psychoName());
    }

    final class Trace
    {
        private final String name;
        private final String psychoName;
        private final String refreshedCachedName;

        Trace(String name)
        {
            this(name, name, null);
        }

        Trace(String name, String psychoName, String refreshedCachedName)
        {
            this.name = name;
            this.psychoName = psychoName;
            this.refreshedCachedName = refreshedCachedName;
        }

        String name() { return name; }
        String psychoName() { return psychoName; }
        String refreshedCachedName() { return refreshedCachedName; }
    }
    
    interface Traces
    {
        Trace viewIssue();
        Trace returnToSearch();
    }

    private class AjaxTraces implements Traces
    {
        @Override
        public Trace viewIssue()
        {
            return new Trace("jira.issue.refreshed", "jira.psycho.issue.refreshed", "jira.psycho.issue.refreshed.cached");
        }

        @Override
        public Trace returnToSearch()
        {
            return new Trace("jira.returned.to.search", "jira.psycho.returned.to.search", null);
        }
    }
}
