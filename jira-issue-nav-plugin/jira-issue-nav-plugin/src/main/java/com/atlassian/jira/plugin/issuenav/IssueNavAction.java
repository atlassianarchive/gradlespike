package com.atlassian.jira.plugin.issuenav;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import com.atlassian.core.user.preferences.Preferences;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.components.issueviewer.action.IssueBean;
import com.atlassian.jira.components.issueviewer.service.ActionUtilsService;
import com.atlassian.jira.components.issueviewer.service.SessionSearchService;
import com.atlassian.jira.components.issueviewer.service.SystemFilter;
import com.atlassian.jira.components.issueviewer.service.SystemFilterService;
import com.atlassian.jira.components.issueviewer.util.HashTransformer;
import com.atlassian.jira.components.issueviewer.viewissue.webpanel.IssueWebPanelsBean;
import com.atlassian.jira.components.issueviewer.viewissue.webpanel.WebPanelMapperUtil;
import com.atlassian.jira.components.query.SearchResults;
import com.atlassian.jira.components.query.SearcherService;
import com.atlassian.jira.components.query.util.UserSearchModeService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.operation.IssueOperation;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.util.IssueWebPanelRenderUtil;
import com.atlassian.jira.jql.util.JqlStringSupport;
import com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTable;
import com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTableService;
import com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTableServiceConfiguration;
import com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTableServiceOutcome;
import com.atlassian.jira.plugin.issuenav.util.DefaultSearchUtil;
import com.atlassian.jira.plugin.issuenav.util.PreferredSearchLayoutService;
import com.atlassian.jira.plugin.keyboardshortcut.KeyboardShortcutManager;
import com.atlassian.jira.plugin.webfragment.SimpleLinkManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.SimpleLink;
import com.atlassian.jira.plugin.webfragment.model.SimpleLinkSection;
import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.rest.v2.issue.OpsbarBean;
import com.atlassian.jira.rest.v2.issue.builder.BeanBuilderFactory;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.CachingUserHistoryStore;
import com.atlassian.jira.user.UserHistoryItem;
import com.atlassian.jira.user.UserIssueHistoryManager;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.user.preferences.PreferenceKeys;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.jira.util.BrowserUtils;
import com.atlassian.jira.util.UserAgentUtil;
import com.atlassian.jira.util.UserAgentUtilImpl;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.action.IssueActionSupport;
import com.atlassian.jira.web.action.issue.IssueMetadataHelper;
import com.atlassian.jira.web.action.issue.IssueNavigatorSearchResultsHelper;
import com.atlassian.jira.web.action.issue.SearchResultsInfo;
import com.atlassian.jira.web.component.ModuleWebComponent;
import com.atlassian.jira.web.session.SessionSelectedIssueManager;
import com.atlassian.jira.web.util.HelpUtil;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.plugins.rest.common.json.DefaultJaxbJsonMarshaller;
import com.atlassian.plugins.rest.common.json.JaxbJsonMarshaller;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.order.OrderBy;
import com.atlassian.query.order.OrderByImpl;
import com.atlassian.seraph.util.RedirectUtils;
import com.atlassian.soy.renderer.SoyException;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.ObjectMapper;

import webwork.action.ServletActionContext;

import static com.atlassian.jira.components.issueviewer.config.IssueNavFeatures.I_ROOT;
import static com.atlassian.jira.issue.fields.layout.column.ColumnLayout.ColumnConfig;
import static com.atlassian.jira.web.action.AjaxHeaders.isPjaxRequest;
import static com.atlassian.jira.web.action.AjaxHeaders.requestUsernameMatches;

public class IssueNavAction extends IssueActionSupport implements OperationContext
{
    private static final String ISSUE = "issue";
    private static final String TAB_CONTENT_ONLY = "tabContentOnly";
    static final String ISSUE_NAV_ROOT = "kickass-issue-nav-root";
    static final String ORIGINAL_URL = "kickass-redirect-original-url";
    static final String REDIRECT_FROM_I = "redirect-from-i";
    static final String REDIRECT_TO_I = "redirect-to-i";

    private final JiraWebResourceManager wrm;
    private final ApplicationProperties applicationProperties;
    private final KeyboardShortcutManager keyboardShortcutManager;
    private final IssueService issueService;
    private String issueKey;
    private final VelocityRequestContextFactory velocityRequestContextFactory;
    private final IssueTableService issueTableService;
    private ServiceOutcome<IssueTableServiceOutcome> issueTableOutcome;
    private final JqlStringSupport jqlStringSupport;
    private final UserPreferencesManager preferencesManager;
    private final HashTransformer hashTransformer;
    private final SessionSearchService sessionSearchService;
    private final SimpleLinkManager simpleLinkManager;
    private final SearcherService searcherService;
    private final SearchRequestService searchRequestService;
    private final SystemFilterService systemFilterService;
    private final PermissionManager permissionManager;
    private HelpUtil helpUtil;
    private final DefaultSearchUtil defaultSearchUtil;
    private final FeatureManager featureManager;
    private final UserSearchModeService userSearchModeService;
    private final AvatarService avatarService;
    private final IssueNavigatorSearchResultsHelper searchResultsHelper;
    private final BeanBuilderFactory beanBuilderFactory;
    private final IssueMetadataHelper issueMetadataHelper;
    private final WebPanelMapperUtil webPanelMapperUtil;
    private final ChangeHistoryManager changeHistoryManager;
    private final ActionUtilsService actionUtilsService;
    private IssueWebPanelRenderUtil issueWebPanelRenderUtil;
    private final WebInterfaceManager webInterfaceManager;
    private final ModuleWebComponent moduleWebComponent;
    private final UserIssueHistoryManager userIssueHistoryManager;
    private final PreferredSearchLayoutService preferredSearchLayoutService;

    private Long filter;
    private String jql;
    private Search search;
    private Integer startIndex;
    private String quickSearchQuery;
    private ServiceOutcome<SearchResults> searcherServiceOutcome;
    private boolean serverRenderedViewIssue;
    private final Map<String, Object> fieldValuesHolder = new HashMap<String, Object>();
    private Map<String, Object> viewIssueRenderData;
    private String hashURL;
    private boolean prefetch = false;

    public IssueNavAction(final ApplicationProperties applicationProperties,
            final KeyboardShortcutManager keyboardShortcutManager,
            final IssueService issueService, final IssueTableService issueTableService,
            final VelocityRequestContextFactory velocityRequestContextFactory,
            final JqlStringSupport jqlStringSupport, UserPreferencesManager preferencesManager,
            SessionSearchService sessionSearchService, PermissionManager permissionManager,
            final SimpleLinkManager simpleLinkManager,
            final DefaultSearchUtil defaultSearchUtil,
            final FeatureManager featureManager,
            final UserSearchModeService userSearchModeService,
            final SearcherService searcherService,
            final SearchRequestService searchRequestService,
            final SystemFilterService systemFilterService,
            final AvatarService avatarService,
            final IssueNavigatorSearchResultsHelper searchResultsHelper,
            final BeanBuilderFactory beanBuilderFactory,
            final WebPanelMapperUtil webPanelMapperUtil,
            final IssueMetadataHelper issueMetadataHelper,
            final ChangeHistoryManager changeHistoryManager,
            final ActionUtilsService actionUtilsService,
            WebInterfaceManager webInterfaceManager,
            ModuleWebComponent moduleWebComponent,
            final UserIssueHistoryManager userIssueHistoryManager,
            final PreferredSearchLayoutService preferredSearchLayoutService,
            final UserPropertyManager userPropertyManager)
    {
        this.issueService = issueService;
        this.issueTableService = issueTableService;
        this.velocityRequestContextFactory = velocityRequestContextFactory;
        this.jqlStringSupport = jqlStringSupport;
        this.preferencesManager = preferencesManager;
        this.sessionSearchService = sessionSearchService;
        this.simpleLinkManager = simpleLinkManager;
        this.featureManager = featureManager;
        this.searcherService = searcherService;
        this.searchRequestService = searchRequestService;
        this.systemFilterService = systemFilterService;
        this.webInterfaceManager = webInterfaceManager;
        this.moduleWebComponent = moduleWebComponent;
        this.hashTransformer = new HashTransformer();
        this.wrm = (JiraWebResourceManager) ComponentAccessor.getComponentOfType(WebResourceManager.class);
        this.applicationProperties = applicationProperties;
        this.keyboardShortcutManager = keyboardShortcutManager;
        this.permissionManager = permissionManager;
        this.defaultSearchUtil = defaultSearchUtil;
        this.userSearchModeService = userSearchModeService;
        this.avatarService = avatarService;
        this.searchResultsHelper = searchResultsHelper;
        this.beanBuilderFactory = beanBuilderFactory;
        this.webPanelMapperUtil = webPanelMapperUtil;
        this.issueMetadataHelper = issueMetadataHelper;
        this.changeHistoryManager = changeHistoryManager;
        this.actionUtilsService = actionUtilsService;
        this.userIssueHistoryManager = userIssueHistoryManager;
        this.preferredSearchLayoutService = preferredSearchLayoutService;
    }

    @Override
    public String doDefault() throws Exception
    {
        //Since JRADEV-19600, we are no longer loading issue nav data when going to standalone view issue
        //Redirect used to happen in doNav(), thus moving the redirect logic up to here so it covers both cases
        String shouldRedirect = handleRedirectsForIssueNav();
        if (!shouldRedirect.isEmpty())
        {
            return shouldRedirect;
        }

        loadAllRequiredResources();
        setAllRequiredMetaData();

        if (!isServerRenderedIssue())
        {
            setServerRenderedViewIssue(false);
            return doNav();
        }
        else
        {
            String doNavReturn = null;
            if (isInSearchContext())
            {
                doNavReturn = doNav();
            }

            viewIssueRenderData = new HashMap<String, Object>();
            if (SUCCESS.equals(doNavReturn) || !isInSearchContext())
            {
                if (isPjaxRequest(request))
                {
                    return doTabContent();
                }
                else
                {
                    return doIssue();
                }
            }

            return doNavReturn;
        }
    }

    private String handleRedirectsForIssueNav() throws Exception {
        // If we've been redirected from the old issue navigator, we should
        // always redirect to empty JQL (?jql=) or the user's session search.
        if (request.getParameterMap().containsKey("redirectedFromClassic"))
        {
            return handleRedirectFromClassic();
        }

		final IssueService.IssueResult result = getRequestedIssue();
		// If the project key was renamed there will be issue returned by it may not have the proper key, redirect to the current issue key
        if (result.isValid() && !result.getIssue().getKey().equals(getIssueKey())) {
			String issueNavRoot = (String) request.getAttribute(ISSUE_NAV_ROOT);
			String query = actionUtilsService.getQueryStringFromComponents(request.getParameterMap());
			String redirectUrl = issueNavRoot + result.getIssue().getKey() + query;
			return getRedirect(redirectUrl);
		}

        // If the browser doesn't support push state, redirect to the /i root, only if it's not a PJAX request.
        if (shouldRedirectToHashURL() && !isPjaxRequest(request))
        {
            // Before redirecting to /i, check if the user has permissions to view the requested issue
            // If we just redirect to /i the client will send an AJAX request that will end in the same error.
            // By doing this check now, we can serve the error faster.
            // This also prevents JRADEV-19476 from happening
            viewIssueRenderData = new HashMap<String, Object>();
            if (!result.isValid() || result.getIssue() == null)
            {
                String redirectToInvalidResult = handleInvalidIssue(result);
                if (!ISSUE.equals(redirectToInvalidResult))
                {
                    // If it not a generic error, return to the error page
                    return redirectToInvalidResult;
                }
            }

            hashURL = hashTransformer.transformURL(getCurrentUrlWithoutContextPath(), request.getParameterMap());
            return REDIRECT_TO_I;
        }
        // If an /i URL was requested and the browser supports pushState, redirect!
        else if (shouldRedirectToFullURL() && !isPjaxRequest(request))
        {
            return REDIRECT_FROM_I;
        }

        return new String();
    }

    public Boolean isInSearchContext()
        {
        return filter != null || jql != null;
    }

    private String getCurrentUrlWithoutContextPath()
    {
        String currentURL = (String) request.getAttribute(ORIGINAL_URL);
        return currentURL.replaceFirst(request.getContextPath(), "");
    }

    /**
     * @return The <tt>/i</tt> URL to redirect to (set in {@link #doNav()}).
     */
    public String getHashURL()
    {
        return request.getContextPath() + hashURL;
        }

    private String  doNav() throws Exception
    {
        velocityRequestContextFactory.setVelocityRequestContext(
                request.getContextPath(), request);

        if (shouldRedirectToLoginForSystemFilter(getFilter()))
        {
            return forceRedirect(RedirectUtils.getLoginUrl(request));
        }

        SearchRequest filterSearchRequest;
        SystemFilter systemFilter = systemFilterService.getById(filter);

        try
        {
            // If a filter was referenced, attempt to fetch it. Ignores system
            // filters, throws exception on error so we can show no results.
            filterSearchRequest = getFilterSearchRequest();
        }
        catch (IllegalArgumentException e)
        {
            // The user doesn't have permission to see the requested filter;
            // just show empty searchers in basic mode, clear session search.
            searcherServiceOutcome = searcherService.searchWithJql(this, "", null);
            sessionSearchService.setSessionSearch(null, null, 0, getPageSize());
            return SUCCESS;
        }

        if (jql != null)
        {
            // A system filter may have been requested, but this JQL overrides
            // it. The JS detects the filter and shows its name properly, etc.
            performSearch(filterSearchRequest, jql);
        }
        else if (filter != null)
        {
            if (filterSearchRequest != null)
            {
                performSearch(filterSearchRequest, null);
            }
            else if (systemFilter != null)
            {
                // JS detects the filter and shows its name properly, etc.
                performSearch(null, systemFilter.getJql());
            }
            else
            {
                issueTableOutcome = ServiceOutcomeImpl.error(null, null);
            }
        }
        else if (startIndex != null)
        {
            performSearch(null, "");
        }
        else
        {
            SearchRequest searchRequest = getSearchRequest();
            if (searchRequest != null)
            {
                updateSessionSelectedIssueId(searchRequest);
                search = new Search(searchRequest, jqlStringSupport,
                        getSessionPagerFilterManager().getCurrentObject());
            }
            else
            {
                search = defaultSearchUtil.getSearch();
            }
            setFilter(search.getFilter());
            setJql(search.getJql());
        }

        return SUCCESS;
    }

    /**
     * @return HTML to include resources required by the "redirect-from-i" page.
     */
    public String getClientRedirectResourcesHtml()
    {
        List<String> resourceKeys = Lists.newArrayList("com.atlassian.jira.jira-issue-nav-plugin:hash-rewrite");
        StringWriter stringWriter = new StringWriter();
        wrm.includeResources(resourceKeys, stringWriter, UrlMode.RELATIVE);

        return stringWriter.toString();
    }

    /**
     * Updates session selected issue id to ensure it is in the search request and on the correct page
     * @param searchRequest search request
     */
    private void updateSessionSelectedIssueId(SearchRequest searchRequest) throws SearchException
    {
        // Getting search results will set sessionPagerFilterManager.currentObject
        SearchResultsInfo searchResultsInfo = searchResultsHelper.getSearchResults(searchRequest.getQuery(), false);
        searchResultsHelper.ensureAnIssueIsSelected(searchResultsInfo, false);
    }

    public String getCriteriaJson()
    {
        if (searcherServiceOutcome != null)
        {
            final JaxbJsonMarshaller marshaller = new DefaultJaxbJsonMarshaller();
            if (searcherServiceOutcome.isValid())
            {
                return marshaller.marshal(searcherServiceOutcome.getReturnedValue());
            }
            else
            {
                final ErrorCollection errors = ErrorCollection.of(searcherServiceOutcome.getErrorCollection());
                return marshaller.marshal(errors);

            }
        }
        return null;
    }

    public String getSystemFiltersJson()
    {
        return actionUtilsService.getSystemFiltersJson();
    }

    /**
     * Returns a JSON object containing the selected issue's ID and key, if there is a selected issue.
     *
     * @return a string of JSON.
     */
    public String getSelectedIssueJson()
    {
        Issue issue = getIssueManager().getIssueObject(this.issueKey);
        JSONObject json = new JSONObject();

        try
        {
            if (issue != null)
            {
                json.put("id", issue.getId());
                json.put("key", issue.getKey());
                return json.toString();
    }
            else if (this.issueKey != null)
            {
                // To show an issue in a search context we need its ID. The requested issue is either forbidden or
                // non-existent, so we must pass a dud ID to make an error message appear on load. Please forgive me.
                json.put("id", -1);
                json.put("key", this.issueKey);
                return json.toString();
            }
        }
        catch (JSONException e)
        {
            // Only happens if a null key is passed to JSONObject.put.
        }

        return "";
    }

    public Collection<SimpleLink> getIssueOperations()
    {
        final List<SimpleLink> links = new ArrayList<SimpleLink>();
        final Map<String, Object> params = MapBuilder.<String, Object>newBuilder().
                add("issueId", "{0}").toMap();
        final JiraHelper helper = new JiraHelper(request, null, params);
        final List<SimpleLinkSection> sections = simpleLinkManager.getSectionsForLocation("opsbar-operations", getLoggedInUser(), helper);
        for (SimpleLinkSection section : sections)
        {
            links.addAll(simpleLinkManager.getLinksForSectionIgnoreConditions(section.getId(), getLoggedInUser(), helper));
        }
        return links;
    }

    /**
     * If the PDL Dark feature is enabled, do stuff
     *
     * @return whether the ADG/PDL is switched on.
     */
    public Boolean getPDL()
    {
        return featureManager.isEnabled("com.atlassian.jira.config.PDL");
    }

    /**
     * Check if the Project Shortcuts dark feature is enabled
     *
     * @return whether the rotp.project.shortcuts is switched on.
     */
    public Boolean isProjectShortcutEnabled()
    {
        return featureManager.isEnabled("rotp.project.shortcuts");
    }

    public List<SimpleLink> getPluggableItems()
    {
        User loggedInUser = getLoggedInUser();
        JiraHelper jiraHelper = new JiraHelper(ServletActionContext.getRequest());
        return simpleLinkManager.getLinksForSection("jira.navigator.pluggable.items", loggedInUser, jiraHelper, true);
    }

    public String getRemoteUserAvatarUrl()
    {
        final User user = getLoggedInUser();
        return user == null ? "" : avatarService.getAvatarAbsoluteURL(user, user.getName(), Avatar.Size.LARGE).toString();
    }

    private String handleRedirectFromClassic() throws SearchException
    {
        SearchRequest searchRequest = getSearchRequest();
        if (null == searchRequest)
        {
            return getRedirect("/issues/?jql=");
        }
        updateSessionSelectedIssueId(searchRequest);
        Search search = new Search(searchRequest, jqlStringSupport, getSessionPagerFilterManager().getCurrentObject());
        return getRedirect("/issues/" + search.toQueryString());
    }

    private IssueService.IssueResult getRequestedIssue()
    {
        return issueService.getIssue(getLoggedInUser(), getIssueKey());
    }

    private String handleInvalidIssue(IssueService.IssueResult result) throws Exception
    {
        MutableIssue issue;

        viewIssueRenderData.put("errorViewIssue", true);

        Reason worstReason = Reason.getWorstReason(result.getErrorCollection().getReasons());

        //FORBIDDEN, logged in but no permission
        if (worstReason == Reason.FORBIDDEN)
        {
            viewIssueRenderData.put("errorType", "NoPermission");
        }
        //NOT_FOUND, issue does not exist
        else if (worstReason == Reason.NOT_FOUND && issueKey != null)
        {
            issue = (MutableIssue) changeHistoryManager.findMovedIssue(issueKey);
            if (issue == null)
            {
                viewIssueRenderData.put("errorType", "DoesNotExist");
            }
            else
            {
                String issueNavRoot = (String) request.getAttribute(ISSUE_NAV_ROOT);
                String query = actionUtilsService.getQueryStringFromComponents(request.getParameterMap());
                String redirectUrl = issueNavRoot + issue.getKey() + query;
                return getRedirect(redirectUrl);
            }
        }
        //NOT_LOGGED_IN, not logged in, but issue exist
        else if (worstReason == Reason.NOT_LOGGED_IN)
        {
            //Returning PERMISSION_VIOLATION_RESULT is sufficient to trigger an URL redirect
            //to log in page via JiraWebActionSupport
            return PERMISSION_VIOLATION_RESULT;
        }
        //Generic
        else
        {
            viewIssueRenderData.put("errorType", "Generic");
        }

        return ISSUE;
    }


    public String doIssue() throws Exception
    {
        wrm.putMetadata("serverRenderedViewIssue", Boolean.toString(isServerRenderedViewIssue()));

        IssueService.IssueResult result = getRequestedIssue();
        viewIssueRenderData.put("serverRenderedViewIssue", isServerRenderedViewIssue());

        if (!result.isValid() || result.getIssue() == null)
        {
            return handleInvalidIssue(result);
        }

        MutableIssue issue = result.getIssue();

        OpsbarBean opsbarBean = beanBuilderFactory.newOpsbarBeanBuilder(issue).build();
        Map<String, String> metadata = issueMetadataHelper.getMetadata(issue, sessionSearchService.getSessionSearch());
        boolean isEditable = issueService.isEditable(issue, getLoggedInUser());
        IssueBean issueBean = new IssueBean(issue, metadata, opsbarBean, issue.getProjectObject(), issue.getStatusObject(), null, true, isEditable);
        IssueWebPanelsBean panels = webPanelMapperUtil.create(issue, this);

        // If we are prefetching issue user isn't actually seeing the issue so don't add it to history.
        if (!isPrefetch())
        {
            userIssueHistoryManager.addIssueToHistory(getLoggedInUser(), issue);
            setSelectedProjectId(issue.getProjectObject().getId());
        }

        for (Map.Entry<String, String> entry : metadata.entrySet())
        {
            wrm.putMetadata(entry.getKey(), entry.getValue());
        }
        wrm.putMetadata("server-view-issue-is-editable", Boolean.toString(isEditable));

        viewIssueRenderData.put("panels", panels);
        viewIssueRenderData.put("issue", issueBean);
        viewIssueRenderData.put("pageTitle", "[" + issueBean.getKey() + "] " + issueBean.getSummary());

        viewIssueRenderData.put("hasProjectShortcut", isProjectShortcutEnabled());

        return ISSUE;
    }

    private String doTabContent()
    {
        if (requestUsernameMatches(request, getLoggedInUser()))
        {
        IssueService.IssueResult result = issueService.getIssue(getLoggedInUser(), getIssueKey());
            MutableIssue issue = result.getIssue();
            viewIssueRenderData.put("issue", issue);
            viewIssueRenderData.put("pageTitle", "[" + issue.getKey() + "] " + issue.getSummary());
            return TAB_CONTENT_ONLY;
        }

        return ERROR;
    }

    public IssueWebPanelRenderUtil getRenderUtil()
    {
        if (issueWebPanelRenderUtil == null)
        {
            Issue issue = (Issue) viewIssueRenderData.get("issue");

            if (issue != null)
            {
                issueWebPanelRenderUtil = new IssueWebPanelRenderUtil(getLoggedInUser(),
                        issue, this, this.webInterfaceManager, this.moduleWebComponent);
            }
        }
        return issueWebPanelRenderUtil;
    }


    public String renderActivityModule()
    {
        List<WebPanelModuleDescriptor> webPanels = ComponentAccessor.getComponentOfType(PluginAccessor.class).getEnabledModuleDescriptorsByClass(WebPanelModuleDescriptor.class);
        for (WebPanelModuleDescriptor webPanel : webPanels)
        {
            if ("com.atlassian.jira.jira-view-issue-plugin:activitymodule".equals(webPanel.getCompleteKey()))
            {
                return webPanel.getModule().getHtml(getRenderUtil().getWebPanelContext());
            }
        }

        return "";
    }


    public Map<String, Object> getIssueSearchAndViewRenderData()
    {
        return viewIssueRenderData;
    }

    public boolean isServerRenderedViewIssue()
    {
        return serverRenderedViewIssue;
    }

    public void setServerRenderedViewIssue(boolean serverRenderedViewIssue)
    {
        this.serverRenderedViewIssue = serverRenderedViewIssue;
    }

    public boolean isEnableShortcutLinks()
    {
        return !actionUtilsService.noGlobalShortcutLinksIsEnabled();
    }

    /**
     * Constructs a configuration object for {@link IssueTableService}, using the current user's preferences and
     * information stored in their session.
     *
     * @return an appropriate configuration for {@link IssueTableService}.
     */
    private IssueTableServiceConfiguration getIssueTableConfig()
    {
        final Preferences preferences = preferencesManager.getPreferences(getLoggedInUser());
        IssueTableServiceConfiguration configuration = new IssueTableServiceConfiguration();

        configuration.setNumberToShow(getPageSize());
        configuration.setSelectedIssueKey(issueKey);

        if (preferences.getBoolean(PreferenceKeys.USER_SHOW_ACTIONS_IN_NAVIGATOR))
        {
            configuration.setShowActions(true);
        }

        if (startIndex != null && startIndex > 0)
        {
            configuration.setStart(startIndex);
        }
        else
        {
            configuration.setStart(0);
        }

        configuration.setColumnConfig(ColumnConfig.FILTER);
        return configuration;
    }

    /**
     * @return the number of issues to show per-page for the current user.
     */
    private int getPageSize()
    {
        return (int) preferencesManager.getPreferences(getLoggedInUser())
                .getLong(PreferenceKeys.USER_ISSUES_PER_PAGE);
    }

    /**
     * Returns the static HTML for the IssueTable.
     *
     * @return String The Issue Table
     */
    public Object getIssueNavHtml()
    {
        if (issueTableOutcome != null && issueTableOutcome.isValid())
        {
            if (isListLayout())
            {
                return issueTableOutcome.getReturnedValue().getIssueTable().getTable();
            }
            else if (isSplitLayout())
            {
                try
                {
                    return renderSplitView();
                }
                catch (SoyException e)
                {
                    log.error("Exception thrown when rendering the split view soy template " + e.getMessage());
                }
            }
        }
        return null;
    }

    private Boolean isListLayout()
    {
        return preferredSearchLayoutService.getPreferredSearchLayout()
                .equals(PreferredSearchLayoutService.LIST_VIEW_LAYOUT);
    }

    private Boolean isSplitLayout()
    {
        return preferredSearchLayoutService.getPreferredSearchLayout()
                .equals(PreferredSearchLayoutService.SPLIT_VIEW_LAYOUT);
    }

    private Boolean isServerRenderedIssue() {
        if (null == issueKey) {
            return false;
        } else if (!isInSearchContext()) {
            return true;
        } else if (isListLayout()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns the JSON representation of the IssueTable sans the table HTML property.
     * <p/>
     * Removing the table HTML property saves on duplication and prevents the need to ensure there are no XSS
     * vulnerabilities in the HTML stored in the JSON.
     *
     * @return String The JSON.
     */
    public String getIssueNavJson() throws IOException
    {
        if (issueTableOutcome != null && issueTableOutcome.isValid())
        {
            ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, JsonAutoDetect.Visibility.ANY);
            if (isListLayout()) {
                // If we are displaying html we append it to the page not to a data attribute
                // Use RemoveTableMixIn to ignore the IssueTable's table property.
                mapper.getSerializationConfig().addMixInAnnotations(IssueTable.class, RemoveTableMixIn.class);
            }
            return mapper.writeValueAsString(issueTableOutcome.getReturnedValue());
        }
        else
        {
            return null;
        }
    }

    public String getSearch() throws IOException
    {
        if (null != search)
        {
            final ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, JsonAutoDetect.Visibility.ANY);
            return mapper.writeValueAsString(search);
        }
        else
        {
            return null;
        }
    }

    public String getVisibleFieldNamesJson() throws JSONException
    {
        return actionUtilsService.getVisibleFieldNamesJson();
    }

    public String getVisibleFunctionNamesJson() throws JSONException
    {
        return actionUtilsService.getVisibleFunctionNamesJson();
    }

    public String getJqlReservedWordsJson() throws JSONException
    {
        return actionUtilsService.getJqlReservedWordsJson();
    }

    public boolean isAutocompleteEnabledForThisRequest()
    {
        // JRADEV-18221: there is no more per-user setting for auto-complete
        return !isAutocompleteDisabledGlobally();
    }

    private boolean isAutocompleteDisabledGlobally()
    {
        return applicationProperties.getOption(APKeys.JIRA_JQL_AUTOCOMPLETE_DISABLED);
    }

    public String getIssueKey()
    {
        return issueKey;
    }

    public void setIssueKey(String issueKey)
    {
        this.issueKey = issueKey;
    }

    public String getUserSearchMode()
    {
        return userSearchModeService.getSearchMode();
    }

    public HelpUtil.HelpPath getAdvancedSearchHelpPath()
    {
        if (null == helpUtil)
        {
            helpUtil = new HelpUtil();
        }
        return helpUtil.getHelpPath("advanced_search");
    }

    public HelpUtil.HelpPath getIssueSearchHelpPath()
    {
        if (null == helpUtil)
        {
            helpUtil = new HelpUtil();
        }
        return helpUtil.getHelpPath("issue_search");
    }

    public String getJql()
    {
        return jql;
    }

    public void setJql(String jql)
    {
        this.jql = jql;
    }

    public Long getFilter()
    {
        return filter;
    }

    public void setFilter(Long filter)
    {
        this.filter = filter;
    }

    public Integer getStartIndex()
    {
        return startIndex;
    }

    public void setStartIndex(Integer startIndex)
    {
        this.startIndex = startIndex;
    }

    public String getQuickSearchQuery()
    {
        return quickSearchQuery;
    }

    public void setQuickSearchQuery(final String quickSearchQuery)
    {
        this.quickSearchQuery = quickSearchQuery;
    }

    public boolean isPrefetch()
    {
        return prefetch;
    }

    public void setPrefetch(final boolean prefetch)
    {
        this.prefetch = prefetch;
    }

    @Override
    public Map getFieldValuesHolder()
    {
        return fieldValuesHolder;
    }

    @Override
    public IssueOperation getIssueOperation()
    {
        return IssueOperations.EDIT_ISSUE_OPERATION;
    }

    /**
     * A Jackson mix-in used to ignore {@code IssueTable}'s {@code table} property.
     */
    private abstract static class RemoveTableMixIn
    {
        @JsonIgnore
        private String table;

        @JsonIgnore
        public abstract String getTable();
    }

    public static class UserParms
    {
        public boolean createSharedObjects;

        public boolean createIssue;

        public UserParms setCreateSharedObjects(boolean createSharedObjects)
        {
            this.createSharedObjects = createSharedObjects;
            return this;
        }

        public UserParms setCreateIssue(boolean createIssue)
        {
            this.createIssue = createIssue;
            return this;
        }
    }

    public String getUserParms() throws IOException
    {
        ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, JsonAutoDetect.Visibility.ANY);
        UserParms userParms = new UserParms().
                setCreateSharedObjects(permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, getLoggedInUser())).
                setCreateIssue(permissionManager.hasProjects(Permissions.CREATE_ISSUE, getLoggedInUser()));
        return mapper.writeValueAsString(userParms);
    }

    private void loadAllRequiredResources()
    {
        keyboardShortcutManager.requireShortcutsForContext(KeyboardShortcutManager.Context.issuenavigation);
        keyboardShortcutManager.requireShortcutsForContext(KeyboardShortcutManager.Context.issueaction);
        wrm.requireResourcesForContext("jira.navigator");
        wrm.requireResourcesForContext("jira.navigator.simple");
        wrm.requireResourcesForContext("jira.navigator.advanced");
        wrm.requireResourcesForContext("jira.navigator.kickass");
        wrm.requireResourcesForContext("jira.view.issue");
    }

    private void setAllRequiredMetaData()
    {
        wrm.putMetadata("hasCriteriaAutoUpdate", String.valueOf(applicationProperties.getOption(APKeys.JIRA_ISSUENAV_CRITERIA_AUTOUPDATE)));
        wrm.putMetadata("viewissue-use-history-api", "false");
        wrm.putMetadata("viewissue-max-cache-size", applicationProperties.getString(APKeys.JIRA_SEARCH_CACHE_MAX_SIZE));
        TraceKeys.write(wrm);
        // Default Avatar URL is used for sharing with unknown users (ie email address)
        URI avatarURL = avatarService.getAvatarURL(getLoggedInUser(), null, Avatar.Size.SMALL);
        if (avatarURL != null)
        {
            String defaultAvatarUrl = avatarURL.toString();
            wrm.putMetadata("default-avatar-url", defaultAvatarUrl);
        }
        wrm.putMetadata(PreferredSearchLayoutService.PREFERRED_SEARCH_LAYOUT_KEY, preferredSearchLayoutService.getPreferredSearchLayout());
        wrm.putMetadata("max-recent-searchers", String.valueOf(CachingUserHistoryStore.getMaxItems(UserHistoryItem.ISSUESEARCHER, applicationProperties)));
        wrm.putMetadata("jira-base-url", applicationProperties.getString(APKeys.JIRA_BASEURL));
    }

    /**
     * Determine if we should redirect to a hash URL (if no push state).
     *
     * @return {@code true} iff we should redirect to a hash URL.
     */
    private boolean shouldRedirectToHashURL()
    {
        String userAgent = request.getHeader(BrowserUtils.USER_AGENT_HEADER);
        if (!hashTransformer.supportsPushState(userAgent) || featureManager.isEnabled(I_ROOT))
        {
            @SuppressWarnings ({ "unchecked" })
            Map<String, Object[]> parameters = (Map<String, Object[]>)
                    request.getParameterMap();

            String originalURL = getCurrentUrlWithoutContextPath();
            if (request.getQueryString() != null)
            {
                originalURL += "?" + request.getQueryString();
            }

            return !hashTransformer.transformURL(getCurrentUrlWithoutContextPath(), parameters)
                    .split("#")[0].equals(originalURL);
        }

        return false;
    }

    /**
     * Determine if we should redirect to a full URL.
     * <p/>
     * This happens if an <tt>/i</tt> URL was requested and the browser supports <tt>pushState</tt>.
     *
     * @return {@code true} iff we should redirect to a full URL.
     */
    private boolean shouldRedirectToFullURL()
    {
        String userAgent = request.getHeader(BrowserUtils.USER_AGENT_HEADER);
        return (
                hashTransformer.supportsPushState(userAgent) &&
                        hashTransformer.isHashedUrl(getCurrentUrlWithoutContextPath()) &&
                        !featureManager.isEnabled(I_ROOT)
        );
    }

    /**
     * @param filterId potential system filter id
     * @return true if the given system filter id requires login and there is no logged in user
     */
    private boolean shouldRedirectToLoginForSystemFilter(Long filterId)
    {
        if (null != getLoggedInUser() || null == filterId)
        {
            return false;
        }

        SystemFilter systemFilter = systemFilterService.getById(filterId);
        return null != systemFilter && systemFilter.isRequiresLogin();
    }

    /**
     * Attempt to retrieve the filter described by {@link #filter}.
     *
     * @return the filter or {@code null} if it's empty or a system filter.
     * @throws IllegalArgumentException if {@link #filter} isn't a valid filter ID or we don't have permission to see
     * the referenced filter.
     */
    private SearchRequest getFilterSearchRequest()
    {
        if (filter != null && !SystemFilter.isSystemFilter(filter))
        {
            SearchRequest filterSearchRequest = searchRequestService.getFilter(new JiraServiceContextImpl(getLoggedInUser()), filter);

            if (filterSearchRequest != null)
            {
                return filterSearchRequest;
            }
            else
            {
                throw new IllegalArgumentException("Filter ID is invalid or describes a private filter.");
            }
        }

        return null;
    }

    /**
     * Perform a search, storing issue table and searcher information.
     * <p/>
     * Also sets the user's session search.
     *
     * @param filterSearchRequest The filter to use.
     * @param jql The JQL query to execute.
     * @throws IllegalArgumentException if both arguments are {@code null}.
     */
    private void performSearch(@Nullable SearchRequest filterSearchRequest, @Nullable String jql)
    {
        if (filterSearchRequest == null && jql == null)
        {
            throw new IllegalArgumentException("At least one of filterSearchRequest and jql must be provided.");
        }

        if (sessionSearchChanged(filterSearchRequest, jql))
        {
            searchResultsHelper.resetPagerAndSelectedIssue();
        }

        String filterString = filter != null ? filter.toString() : "";

        IssueTableServiceConfiguration configuration = getIssueTableConfig();
        issueTableOutcome = issueTableService.getIssueTableFromFilterWithJql(filterString, jql, configuration, true);
        sessionSearchService.setSessionSearch(jql, filter, configuration.getStart(), configuration.getNumberToShow());

        if (jql == null)
        {
            jql = jqlStringSupport.generateJqlString(filterSearchRequest.getQuery());
        }

        searcherServiceOutcome = searcherService.searchWithJql(this, jql, filter);
    }

    @VisibleForTesting
    boolean sessionSearchChanged(@Nullable SearchRequest filterSearchRequest, @Nullable String jql)
    {
        SearchRequest sessionSearchRequest = getSearchRequest();
        return sessionSearchRequest == null || !(sessionSearchRequest.equals(filterSearchRequest) || jqlStringSupport.generateJqlString(sessionSearchRequest.getQuery()).equals(jql));
    }

    public final String doClearSorts()
    {
        final SearchRequest sr = getSearchRequest();
        if (sr != null)
        {
            OrderBy orderByClause = new OrderByImpl();

            if (sr.getId() != null)
            {
                if (SystemFilter.isSystemFilter(sr.getId()))
                {
                    SystemFilter systemFilter = SystemFilter.getSystemFilterById(sr.getId());

                    orderByClause = new OrderByImpl(systemFilter.getSearchSort());
                }
                else
                {
                    SearchRequest initialFilterRequest = searchRequestService.getFilter(getJiraServiceContext(), sr.getId());

                    if (initialFilterRequest != null && initialFilterRequest.getQuery() != null)
                    {
                        orderByClause = initialFilterRequest.getQuery().getOrderByClause();
                    }
                }
            }

            // Lets just nuke the sorts so we will use the defaults
            sr.setQuery(new QueryImpl(sr.getQuery().getWhereClause(), orderByClause, sr.getQuery().getQueryString()));
            setSearchRequest(sr);
        }
        return forceRedirect("IssueNavigator.jspa");
    }

    public boolean isInlineEditEnabled()
    {
        return actionUtilsService.isInlineEditEnabled();
    }

    public String renderSplitView() throws SoyException
    {
        IssueTable table = issueTableOutcome.getReturnedValue().getIssueTable();
        Map<String, Object> params = Maps.newHashMap();

        Map<String, Object> issuePanelData = Maps.newHashMap();
        final Integer pageSize = getPageSize();
        final Integer totalIssues = table.getEnd();
        final Integer startIndex = table.getStartIndex();
        final Integer endIndex = (startIndex + pageSize > totalIssues) ? totalIssues : startIndex + pageSize;

        issuePanelData.put("issues", table.getTable());
        issuePanelData.put("issueIDs", table.getIssueIds().subList(startIndex, endIndex));

        Map<String, Object> pagination = Maps.newHashMap();
        pagination.put("startIndex", startIndex);
        pagination.put("pageSize", pageSize);
        pagination.put("displayableTotal", totalIssues);
        String query = actionUtilsService.getQueryStringFromComponents(
                request.getParameterMap(),
                Lists.newArrayList("jql", "filter"));
        pagination.put("searchQuery", "issues/" + query);

        params.put("issuePanelData", issuePanelData);
        params.put("pagination", pagination);
        return actionUtilsService.getSoyRenderer().render(
                "com.atlassian.jira.jira-issue-nav-plugin:issuenav-common",
                "JIRA.Templates.SplitView.fullSplitView",
                params
        );
    }

    public String getIssueHeaderTemplateName()
    {
        if (getPDL())
        {
            return "JIRA.Components.IssueViewer.Templates.Header.issueHeader";
        }
        return "JIRA.Components.IssueViewer.Templates.Header.legacyIssueHeader";
    }
}
