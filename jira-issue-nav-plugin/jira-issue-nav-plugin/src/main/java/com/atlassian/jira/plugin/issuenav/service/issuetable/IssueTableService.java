package com.atlassian.jira.plugin.issuenav.service.issuetable;

import com.atlassian.jira.bc.ServiceOutcome;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Executes a search and returns a rendered issue table, stable search IDs, etc.
 *
 * @since v6.0
 */
public interface IssueTableService
{
    ServiceOutcome<IssueTableServiceOutcome> getIssueTableFromFilterWithJql(
            final String filterId,
            final String jql,
            IssueTableServiceConfiguration configuration,
            boolean isStableSearchFirstHit);

    /**
     * @param filterId The id of the filter used in the initial search for the list of {@code ids}
     * @param jql The jql of the client state at the time of making this service call. This is used for generating the correct sort JQL.
     */
    ServiceOutcome<IssueTableServiceOutcome> getIssueTableFromIssueIds(
            @Nullable final String filterId,
            @Nullable final String jql,
            final List<Long> ids,
            final IssueTableServiceConfiguration configuration);
}
