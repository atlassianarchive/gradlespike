package com.atlassian.jira.plugin.issuenav.service;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTableService;
import com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTableServiceConfiguration;
import com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTableServiceOutcome;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Returns details about stable searches.
 *
 * @since v6.0
 */
@Named
public class DefaultStableSearchService implements StableSearchService
{
    private final IssueTableService issueTableService;

    @Inject
    public DefaultStableSearchService(IssueTableService issueTableService)
    {
        this.issueTableService = issueTableService;
    }

    @Override
    public ServiceOutcome<StableSearchResultBean> getIssueTableFromIssueIds(String filterId, String jql, List<Long> ids, IssueTableServiceConfiguration config)
    {
        final ServiceOutcome<IssueTableServiceOutcome> issueTableOutcome = issueTableService.getIssueTableFromIssueIds(filterId, jql, ids, config);
        final ErrorCollection errorCollection = new SimpleErrorCollection();
        final StableSearchResultBean result = new StableSearchResultBean(issueTableOutcome);
        errorCollection.addErrorCollection(issueTableOutcome.getErrorCollection());

        return new ServiceOutcomeImpl<StableSearchResultBean>(errorCollection, result);
    }
}
