package com.atlassian.jira.plugin.issuenav.rest;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.issuetype.IssueType;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A simpler representation of an issue for returning to client on splitview
 *
 * @since v6.0
 */
@SuppressWarnings ("UnusedDeclaration")
@XmlRootElement
public class SplitViewIssueBean
{
    @XmlElement
    private Long id;
    @XmlElement
    private String key;
    @XmlElement
    private String status;
    @XmlElement
    private String summary;
    @XmlElement
    private SplitViewIssueTypeBean type;


    public SplitViewIssueBean(final Issue issue)
    {
        this(issue.getId(),
                issue.getKey(),
                issue.getStatusObject().getName(),
                issue.getSummary(),
                new SplitViewIssueBean.SplitViewIssueTypeBean(issue.getIssueTypeObject())
        );
    }

    public SplitViewIssueBean(Long id, String key, String status, String summary,
            SplitViewIssueTypeBean type)
    {
        this.id = id;
        this.key = key;
        this.status = status;
        this.summary = summary;
        this.type = type;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public SplitViewIssueTypeBean getType()
    {
        return type;
    }

    public void setType(SplitViewIssueTypeBean type)
    {
        this.type = type;
    }

    @XmlRootElement
    public static class SplitViewIssueTypeBean
    {
        @XmlElement
        private String description;
        @XmlElement
        private String name;
        @XmlElement
        private String iconUrl;

        public SplitViewIssueTypeBean(String description, String name, String iconUrl)
        {
            this.description = description;
            this.name = name;
            this.iconUrl = iconUrl;
        }

        public SplitViewIssueTypeBean(final IssueType type)
        {
            this(type.getDescription(), type.getName(), type.getCompleteIconUrl());
        }

        public String getDescription()
        {
            return description;
        }

        public void setDescription(String description)
        {
            this.description = description;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getIconUrl()
        {
            return iconUrl;
        }

        public void setIconUrl(String iconUrl)
        {
            this.iconUrl = iconUrl;
        }
    }
}
