package com.atlassian.jira.plugin.issuenav.service.issuetable;

import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.TotalHitsAwareCollector;
import com.google.common.annotations.VisibleForTesting;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.FieldSelector;
import org.apache.lucene.document.SetBasedFieldSelector;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Scorer;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static com.google.common.collect.Lists.newArrayListWithCapacity;
import static com.google.common.collect.Sets.newHashSet;

/**
 * Collects issue documents, IDs, and keys for use in an issue table.
 */
class IssueDocumentAndIdCollector extends Collector implements TotalHitsAwareCollector
{
    public static class Result {

        private final int start;
        private final int total;
        private final List<Long> issueIDs;
        private final List<String> issueKeys;
        private final List<Document> documents;

        private Result(int start, int total, List<Long> issueIDs, List<String> issueKeys, List<Document> documents) {
            this.start = start;
            this.total = total;
            this.issueIDs = issueIDs;
            this.issueKeys = issueKeys;
            this.documents = documents;
        }

        public int getStart() {
            return start;
        }

        /**
         * @return the total number of issues matching the search.
         */
        public int getTotal() {
            return total;
        }

        public List<Document> getDocuments() throws SearchException {
            return documents;
        }

        public List<Long> getIssueIDs()
        {
            return issueIDs;
        }

        public List<String> getIssueKeys()
        {
            return issueKeys;
        }
    }
    @VisibleForTesting
    static final FieldSelector ID_AND_KEY_FIELD_SELECTOR = new SetBasedFieldSelector(
            newHashSet(DocumentConstants.ISSUE_ID, DocumentConstants.ISSUE_KEY),
            Collections.<String>emptySet()
    );

    protected final IndexSearcher searcher;
    private int docBase;

    private final List<Integer> docIds;
    private final int maximumSize;
    private final int pageSize;
    private final String selectedIssueKey;
    private int startIndex;
    private int documentsCollected = 0;
    private int totalHits = 0;

    /**
     * @param issuesSearcher
     * @param maximumSize The maximum number of issue IDs / keys to collect (the stable search limit).
     * @param pageSize The number of issues to be shown on a page of the issue table.
     * @param selectedIssueKey The key of the selected issue (or {@code null}).
     * @param startIndex The index of the first issue to show (ignored if {@code selectedIssueKey} is given).
     */
    public IssueDocumentAndIdCollector(
            IndexSearcher issuesSearcher,
            final int maximumSize,
            final int pageSize,
            final String selectedIssueKey,
            final int startIndex)
    {
        this.searcher = issuesSearcher;
        this.docIds = newArrayListWithCapacity(maximumSize);
        this.maximumSize = maximumSize;
        this.pageSize = pageSize;
        this.selectedIssueKey = selectedIssueKey;
        this.startIndex = nearestStartIndex(startIndexWithinMaximumSize(startIndex, maximumSize), pageSize);
    }

    public void collect(int i)
    {
        if (documentsCollected < maximumSize) {
            int docid = docBase + i;
            docIds.add(docid);
        }

        ++documentsCollected;
    }

    @Override
    public void setScorer(Scorer scorer) throws IOException
    {
        // Do nothing
    }

    @Override
    public void setNextReader(IndexReader reader, int docBase) throws IOException
    {
        this.docBase = docBase;

    }

    @Override
    public boolean acceptsDocsOutOfOrder()
    {
        return false;
    }

    @Override
    public void setTotalHits(int i)
    {
        totalHits = i;
    }

    private static int nearestStartIndex(int startIndex, int pageSize)
    {
        return (pageSize == 0) ? 0 : startIndex / pageSize * pageSize;
    }

    private static int startIndexWithinMaximumSize(int startIndex, int maximumSize)
    {
        return (maximumSize == 0) ? 0 : Math.min(startIndex, maximumSize - 1);
    }


    /**
     *  Computes the results for the currently matching page
     *  Takes into consideration the startIndex, selectedIssueKey, etc
     */
    public Result computeResult() throws SearchException {

        try {
            int pageStartIdx = nearestStartIndex(startIndexWithinMaximumSize(startIndex, docIds.size()), pageSize);

            if (selectedIssueKey != null) {
                final TermDocs docs = searcher.getIndexReader().termDocs(new Term(DocumentConstants.ISSUE_KEY, selectedIssueKey));
                if (docs.next()) {
                    int selectedDocId = docs.doc();
                    int idx = docIds.indexOf(selectedDocId);
                    if (idx != -1) {
                        // found selected issue, create page for that:
                        pageStartIdx = nearestStartIndex(idx, pageSize);
                    }
                }
                docs.close();
            }

            List<Long> issueIds = newArrayListWithCapacity(docIds.size());
            List<String> issueKeys = newArrayListWithCapacity(docIds.size());
            List<Document> documentsInPage = newArrayListWithCapacity(Math.min(pageSize, docIds.size()));

            for (int i = 0; i < pageStartIdx; i++) {
                addMatch(i, issueIds, issueKeys);
            }
            int pageEndIdx = Math.min(pageStartIdx + pageSize, docIds.size());
            for (int i = pageStartIdx; i < pageEndIdx; i++) {
                addMatch(i, issueIds, issueKeys, documentsInPage);
            }
            for (int i = pageEndIdx; i < docIds.size(); i++) {
                addMatch(i, issueIds, issueKeys);
            }

            return new Result(pageStartIdx, totalHits, issueIds, issueKeys, documentsInPage);
        } catch (IOException e) {
            throw new SearchException(e);
        }
    }

    private void addMatch(int idx, List<Long> issueIds, List<String> issueKeys) throws IOException {
        Integer docId = docIds.get(idx);
        Document doc = searcher.doc(docId, ID_AND_KEY_FIELD_SELECTOR);
        Long e = Long.valueOf(doc.get(DocumentConstants.ISSUE_ID));
        issueIds.add(e);
        issueKeys.add(doc.get(DocumentConstants.ISSUE_KEY));
    }
    private void addMatch(int idx, List<Long> issueIds, List<String> issueKeys, List<Document> documents) throws IOException {
        Integer docId = docIds.get(idx);
        Document doc = searcher.doc(docId);
        issueIds.add(Long.valueOf(doc.get(DocumentConstants.ISSUE_ID)));
        issueKeys.add(doc.get(DocumentConstants.ISSUE_KEY));
        documents.add(doc);

    }

}