package com.atlassian.jira.plugin.issuenav.service;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTableServiceConfiguration;

import java.util.List;

/**
 * Returns details about stable searches.
 *
 * @since v6.0
 */
public interface StableSearchService
{
    ServiceOutcome<StableSearchResultBean> getIssueTableFromIssueIds(
            final String filterId,
            final String jql,
            final List<Long> ids,
            final IssueTableServiceConfiguration config);
}
