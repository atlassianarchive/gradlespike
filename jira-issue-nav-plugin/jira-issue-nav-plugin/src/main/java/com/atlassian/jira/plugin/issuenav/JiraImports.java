package com.atlassian.jira.plugin.issuenav;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.admin.ApplicationPropertiesService;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.components.issueviewer.service.ActionUtilsService;
import com.atlassian.jira.components.issueviewer.service.SessionSearchService;
import com.atlassian.jira.components.issueviewer.service.SystemFilterService;
import com.atlassian.jira.components.issueviewer.viewissue.webpanel.ContentRenderingInstructionProvider;
import com.atlassian.jira.components.issueviewer.viewissue.webpanel.WebPanelMapperUtil;
import com.atlassian.jira.components.orderby.OrderByUtil;
import com.atlassian.jira.components.query.SearchContextHelper;
import com.atlassian.jira.components.query.SearcherService;
import com.atlassian.jira.components.query.util.UserSearchModeService;
import com.atlassian.jira.components.util.FilterLookerUpper;
import com.atlassian.jira.components.util.SortJqlGenerator;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutManager;
import com.atlassian.jira.issue.fields.rest.FieldHtmlFactory;
import com.atlassian.jira.issue.fields.rest.IssueFinder;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.SearchRequestFactory;
import com.atlassian.jira.issue.search.managers.IssueSearcherManager;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.issue.search.util.SearchSortUtil;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.jql.util.JqlStringSupport;
import com.atlassian.jira.plugin.keyboardshortcut.KeyboardShortcutManager;
import com.atlassian.jira.plugin.webfragment.SimpleLinkManager;
import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.rest.v2.issue.builder.BeanBuilderFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.user.UserIssueHistoryManager;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.action.issue.IssueMetadataHelper;
import com.atlassian.jira.web.action.issue.IssueNavigatorSearchResultsHelper;
import com.atlassian.jira.web.action.issue.IssueSearchLimits;
import com.atlassian.jira.web.component.ModuleWebComponent;
import com.atlassian.jira.web.component.ModuleWebComponentFields;
import com.atlassian.jira.web.component.jql.AutoCompleteJsonGenerator;
import com.atlassian.jira.web.session.SessionSearchObjectManagerFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Generic class to centralize all imports
 *
 * @since v6.0
 */
@Named
public class JiraImports
{
    private final I18nResolver i18nResolver;
    private final BeanBuilderFactory beanBuilderFactory;
    private final IssueFinder issueFinder;
    private final PluginSettingsFactory pluginSettings;
    private final RequestFactory restRequestFactory;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final UserSearchModeService userSearchModeService;
    private final SearcherService searcherService;
    private final SearchContextHelper searchContextHelper;
    private final SortJqlGenerator sortJqlGenerator;
    private final FilterLookerUpper filterLookerUpper;
    private final OrderByUtil orderByUtil;
    private final SessionSearchService sessionSearchService;
    private final SystemFilterService systemFilterService;
    private final WebPanelMapperUtil webPanelMapperUtil;
    private final ContentRenderingInstructionProvider contentRenderingInstructionProvider;
    private final ActionUtilsService actionsUtilsService;
    private final IssueSearcherManager issueSearcherManager;
    private final SimpleLinkManager simpleLinkManager;
    private final FeatureManager featureManager;
    private final ApplicationPropertiesService applicationPropertiesService;
    private final EventPublisher eventPublisher;
    private final SessionSearchObjectManagerFactory sessionSearchObjectManagerFactory;
    private final ApplicationProperties applicationProperties;
    private final AutoCompleteJsonGenerator autoCompleteJsonGenerator;
    private final IssueManager issueManager;
    private final JiraWebResourceManager jiraWebResourceManager;
    private final VelocityTemplatingEngine velocityTemplatingEngine;
    private final SearchService searchService;
    private final PluginAccessor pluginAccessor;
    private final SubTaskManager subTaskManager;
    private final FieldHtmlFactory fieldHtmlFactory;
    private final SearchRequestFactory searchRequestFactory;
    private final FieldManager fieldManager;
    private final KeyboardShortcutManager keyboardShortcutManager;
    private final AvatarService avatarService;
    private final UserProjectHistoryManager userProjectHistoryManager;
    private final SearchSortUtil searchSortUtil;
    private final SearchRequestService searchRequestService;
    private final JqlQueryParser jqlQueryParser;
    private final IssueSearchLimits issueSearchLimits;
    private final SearchProvider searchProvider;
    private final PermissionManager permissionManager;
    private final SearchHandlerManager searchHandlerManager;
    private final BuildUtilsInfo buildUtilsInfo;
    private final ModuleWebComponent moduleWebComponent;
    private final JqlStringSupport jqlStringSupport;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final IssueFactory issueFactory;
    private final I18nHelper contextI18nHelper;
    private final UserPropertyManager userPropertyManager;
    private final IssueNavigatorSearchResultsHelper issueNavigatorSearchResultsHelper;
    private final ColumnLayoutManager columnLayoutManager;
    private final UserIssueHistoryManager userIssueHistoryManager;
    private final IssueService issueService;
    private final SearchProviderFactory searchProviderFactory;
    private final IssueMetadataHelper issueMetadataHelper;
    private final VelocityRequestContextFactory velocityRequestContextFactory;
    private final ChangeHistoryManager changeHistoryManager;
    private final ModuleWebComponentFields moduleWebComponentFields;
    private final WebInterfaceManager webInterfaceManager;
    private final UserPreferencesManager userPreferencesManager;

    @Inject
    public JiraImports(
            @ComponentImport final I18nResolver i18nResolver,
            @ComponentImport final BeanBuilderFactory beanBuilderFactory,
            @ComponentImport final IssueFinder issueFinder,
            @ComponentImport final PluginSettingsFactory pluginSettings,
            @ComponentImport final RequestFactory restRequestFactory,
            @ComponentImport final SoyTemplateRenderer soyTemplateRenderer,
            @ComponentImport final UserSearchModeService userSearchModeService,
            @ComponentImport final SearcherService searcherService,
            @ComponentImport final SearchContextHelper searchContextHelper,
            @ComponentImport final SortJqlGenerator sortJqlGenerator,
            @ComponentImport final FilterLookerUpper filterLookerUpper,
            @ComponentImport final OrderByUtil orderByUtil,
            @ComponentImport final SessionSearchService sessionSearchService,
            @ComponentImport final SystemFilterService systemFilterService,
            @ComponentImport final WebPanelMapperUtil webPanelMapperUtil,
            @ComponentImport final ContentRenderingInstructionProvider contentRenderingInstructionProvider,
            @ComponentImport final ActionUtilsService actionsUtilsService,
            @ComponentImport final IssueSearcherManager issueSearcherManager,
            @ComponentImport final SimpleLinkManager simpleLinkManager,
            @ComponentImport final FeatureManager featureManager,
            @ComponentImport final ApplicationPropertiesService applicationPropertiesService,
            @ComponentImport final EventPublisher eventPublisher,
            @ComponentImport final SessionSearchObjectManagerFactory sessionSearchObjectManagerFactory,
            @ComponentImport final ApplicationProperties applicationProperties,
            @ComponentImport final AutoCompleteJsonGenerator autoCompleteJsonGenerator,
            @ComponentImport final IssueManager issueManager,
            @ComponentImport("webResourceManager") final JiraWebResourceManager jiraWebResourceManager,
            @ComponentImport final VelocityTemplatingEngine velocityTemplatingEngine,
            @ComponentImport final SearchService searchService,
            @ComponentImport final PluginAccessor pluginAccessor,
            @ComponentImport final SubTaskManager subTaskManager,
            @ComponentImport final FieldHtmlFactory fieldHtmlFactory,
            @ComponentImport final SearchRequestFactory searchRequestFactory,
            @ComponentImport final FieldManager fieldManager,
            @ComponentImport final KeyboardShortcutManager keyboardShortcutManager,
            @ComponentImport final AvatarService avatarService,
            @ComponentImport final UserProjectHistoryManager userProjectHistoryManager,
            @ComponentImport final SearchSortUtil searchSortUtil,
            @ComponentImport final SearchRequestService searchRequestService,
            @ComponentImport final JqlQueryParser jqlQueryParser,
            @ComponentImport final IssueSearchLimits issueSearchLimits,
            @ComponentImport final SearchProvider searchProvider,
            @ComponentImport final PermissionManager permissionManager,
            @ComponentImport final SearchHandlerManager searchHandlerManager,
            @ComponentImport final BuildUtilsInfo buildUtilsInfo,
            @ComponentImport final ModuleWebComponent moduleWebComponent,
            @ComponentImport final JqlStringSupport jqlStringSupport,
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final IssueFactory issueFactory,
            @ComponentImport final I18nHelper contextI18nHelper,
            @ComponentImport final UserPropertyManager userPropertyManager,
            @ComponentImport final IssueNavigatorSearchResultsHelper issueNavigatorSearchResultsHelper,
            @ComponentImport final ColumnLayoutManager columnLayoutManager,
            @ComponentImport final UserIssueHistoryManager userIssueHistoryManager,
            @ComponentImport final IssueService issueService,
            @ComponentImport final SearchProviderFactory searchProviderFactory,
            @ComponentImport final IssueMetadataHelper issueMetadataHelper,
            @ComponentImport final VelocityRequestContextFactory velocityRequestContextFactory,
            @ComponentImport final ChangeHistoryManager changeHistoryManager,
            @ComponentImport final ModuleWebComponentFields moduleWebComponentFields,
            @ComponentImport final WebInterfaceManager webInterfaceManager,
            @ComponentImport final UserPreferencesManager userPreferencesManager
    ) {
        this.i18nResolver = i18nResolver;
        this.beanBuilderFactory = beanBuilderFactory;
        this.issueFinder = issueFinder;
        this.pluginSettings = pluginSettings;
        this.restRequestFactory = restRequestFactory;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.userSearchModeService = userSearchModeService;
        this.searcherService = searcherService;
        this.searchContextHelper = searchContextHelper;
        this.sortJqlGenerator = sortJqlGenerator;
        this.filterLookerUpper = filterLookerUpper;
        this.orderByUtil = orderByUtil;
        this.sessionSearchService = sessionSearchService;
        this.systemFilterService = systemFilterService;
        this.webPanelMapperUtil = webPanelMapperUtil;
        this.contentRenderingInstructionProvider = contentRenderingInstructionProvider;
        this.actionsUtilsService = actionsUtilsService;
        this.issueSearcherManager = issueSearcherManager;
        this.simpleLinkManager = simpleLinkManager;
        this.featureManager = featureManager;
        this.applicationPropertiesService = applicationPropertiesService;
        this.eventPublisher = eventPublisher;
        this.sessionSearchObjectManagerFactory = sessionSearchObjectManagerFactory;
        this.applicationProperties = applicationProperties;
        this.autoCompleteJsonGenerator = autoCompleteJsonGenerator;
        this.issueManager = issueManager;
        this.jiraWebResourceManager = jiraWebResourceManager;
        this.velocityTemplatingEngine = velocityTemplatingEngine;
        this.searchService = searchService;
        this.pluginAccessor = pluginAccessor;
        this.subTaskManager = subTaskManager;
        this.fieldHtmlFactory = fieldHtmlFactory;
        this.searchRequestFactory = searchRequestFactory;
        this.fieldManager = fieldManager;
        this.keyboardShortcutManager = keyboardShortcutManager;
        this.avatarService = avatarService;
        this.userProjectHistoryManager = userProjectHistoryManager;
        this.searchSortUtil = searchSortUtil;
        this.searchRequestService = searchRequestService;
        this.jqlQueryParser = jqlQueryParser;
        this.issueSearchLimits = issueSearchLimits;
        this.searchProvider = searchProvider;
        this.permissionManager = permissionManager;
        this.searchHandlerManager = searchHandlerManager;
        this.buildUtilsInfo = buildUtilsInfo;
        this.moduleWebComponent = moduleWebComponent;
        this.jqlStringSupport = jqlStringSupport;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.issueFactory = issueFactory;
        this.contextI18nHelper = contextI18nHelper;
        this.userPropertyManager = userPropertyManager;
        this.issueNavigatorSearchResultsHelper = issueNavigatorSearchResultsHelper;
        this.columnLayoutManager = columnLayoutManager;
        this.userIssueHistoryManager = userIssueHistoryManager;
        this.issueService = issueService;
        this.searchProviderFactory = searchProviderFactory;
        this.issueMetadataHelper = issueMetadataHelper;
        this.velocityRequestContextFactory = velocityRequestContextFactory;
        this.changeHistoryManager = changeHistoryManager;
        this.moduleWebComponentFields = moduleWebComponentFields;
        this.webInterfaceManager = webInterfaceManager;
        this.userPreferencesManager = userPreferencesManager;
    }
}
