package com.atlassian.jira.plugin.issuenav;

import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.jql.util.JqlStringSupport;
import com.atlassian.jira.components.issueviewer.service.SystemFilter;
import com.atlassian.jira.web.bean.PagerFilter;

import javax.ws.rs.core.UriBuilder;

/**
 * A search request.
 *
 * Used to pass search information into the velocity template. Similar to
 * {@link SearchRequest}, but specific to KickAss.
 *
 * @since v5.2
 */
public class Search
{
    private final Long filter;
    private final String jql;
    private final int startIndex;

    public Search(
            SearchRequest searchRequest,
            JqlStringSupport jqlStringSupport,
            PagerFilter<?> pagerFilter)
    {
        if (null != searchRequest.getId())
        {
            filter = searchRequest.getId();
            if (searchRequest.isModified())
            {
                jql = jqlStringSupport.generateJqlString(searchRequest.getQuery());
            }
            else
            {
                jql = null;
            }
        }
        else
        {
            filter = null;
            jql = jqlStringSupport.generateJqlString(searchRequest.getQuery());
        }
        startIndex = null == pagerFilter ? 0 : pagerFilter.getStart();
    }

    public Search(Long filter, String jql)
    {
        this.filter = filter;
        this.jql = jql;
        startIndex = 0;
    }

    public Search(SystemFilter filter)
    {
        this(filter.getId(), null);
    }

    /**
     * @return a URL query string representation of the search.
     */
    public String toQueryString()
    {
        UriBuilder uriBuilder = UriBuilder.fromPath("");
        if (null != filter)
        {
            uriBuilder.queryParam("filter", filter);
        }
        if (null != jql)
        {
            uriBuilder.queryParam("jql", urlEncode(jql));
        }
        if (startIndex > 0)
        {
            uriBuilder.queryParam("startIndex", startIndex);
        }
        // wtf, js is not decoding + in my querystrings to spaces. need to re-encode as %20.
        return uriBuilder.build().toString().replace("+", "%20");
    }

    /**
     * @return the ID of the selected filter (could be a system filter).
     */
    public Long getFilter()
    {
        return filter;
    }

    /**
     * @return the JQL being searched for or {@code null} if just a filter.
     */
    public String getJql()
    {
        return jql;
    }

    /**
     * @return the index of the first issue that should be shown.
     */
    public int getStartIndex()
    {
        return startIndex;
    }

    private String urlEncode(final String input)
    {
        String output = String.valueOf(input);
        /**
         * JRA-36281
         * Replace colon with %3A, since {@link com.atlassian.jira.web.action.RedirectSanitiser} doesn't like them.
         * As of JIRA 6.2, {@link com.atlassian.jira.web.action.JiraWebActionSupport#getRedirect(String)} will invoke the sanitiser.
         */
        output = output.replace(":", "%3A");
        return output;
    }
}