package com.atlassian.jira.plugin.issuenav;

import com.atlassian.core.filters.AbstractHttpFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * Implements a specific legacy URL transform as described in https://jdog.atlassian.net/browse/JRADEV-15755
 * which specifies text queries for the four fields summary, environment, description and body (referring to 
 * the body of any comment).
 *
 * @since v5.2
 */
public class LegacyTextQueryUrlRedirectFilter extends AbstractHttpFilter
{
    /**
     * Redirect only in the case where the 'query' parameter is present and one of the target legacy field names
     * is set to the value "true". The legacy field names are summary, environment, description and body. The
     * redirect should be sent to the new URL for this query, which sets the text value to each of the fields
     * which had the value "true" (except 'comment' is used instead of 'body') and the query param is removed.
     *
     */
    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException
    {
        String query = request.getParameter("query");
        if (query != null)
        {
            query = URLEncoder.encode(query, "UTF-8");
            final StringBuffer url = request.getRequestURL();
            if (request.getQueryString() != null) {
                url.append("?").append(request.getQueryString());
            }
            String newUrl = transform(url.toString(), query);
            response.sendRedirect(newUrl);
        } else {
            filterChain.doFilter(request, response);
        }
    }

    String transform(String requestURI, String query)
    {
        String newUri = requestURI.replaceAll("summary=true", "summary=" + query);
        newUri = newUri.replaceAll("description=true", "description=" + query);
        newUri = newUri.replaceAll("environment=true", "environment=" + query);
        newUri = newUri.replaceAll("body=true", "comment=" + query);
        newUri = newUri.replaceAll("query=[^&]*&?", "");
        return newUri;
    }
}
