package com.atlassian.jira.plugin.issuenav.service.issuetable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.query.Query;

import java.util.List;

/**
 * Creates {@link AbstractIssueTableCreator} instances.
 *
 * @since v6.0
 */
public interface IssueTableCreatorFactory
{
    /**
     * Create an {@link AbstractIssueTableCreator} for a normal (non-stable) search.
     *
     * @param configuration The {@link IssueTableService} configuration to use.
     * @param query The query whose results will form the table's content.
     * @param returnIssueIds Whether issue IDs should be returned.
     * @param searchRequest The search request being executed (may differ from {@code query}).
     * @param user The user requesting this information.
     * @return an {@link AbstractIssueTableCreator}.
     * @throws IllegalArgumentException If no {@link AbstractIssueTableCreator} corresponds to {@code layoutKey}.
     * @throws RuntimeException If creating the instance fails.
     */
    IssueTableCreator getNormalIssueTableCreator(
            final IssueTableServiceConfiguration configuration,
            final Query query,
            final boolean returnIssueIds,
            final SearchRequest searchRequest,
            final User user);

    /**
     * Create an {@link AbstractIssueTableCreator} for a stable search.
     *
     * @param configuration The {@link IssueTableService} configuration to use.
     * @param query The query object containing the JQL of the client state at the time of making this service call.
     *              Used to generate valid sort JQL.
     * @param issueIds The IDs of the issues to render in the table.
     * @param searchRequest The search request containing the id of the filter used in the initial search for the list
     *                      of {@code issueIds}
     * @param user The user requesting this information.
     * @return an {@link AbstractIssueTableCreator}.
     * @throws IllegalArgumentException If no {@link AbstractIssueTableCreator} corresponds to {@code layoutKey}.
     * @throws RuntimeException If creating the instance fails.
     */
    IssueTableCreator getStableIssueTableCreator(
            final IssueTableServiceConfiguration configuration,
            final Query query,
            final List<Long> issueIds,
            final SearchRequest searchRequest,
            final User user);
}
