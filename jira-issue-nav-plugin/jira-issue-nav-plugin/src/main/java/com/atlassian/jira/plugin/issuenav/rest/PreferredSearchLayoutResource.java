package com.atlassian.jira.plugin.issuenav.rest;

import com.atlassian.jira.plugin.issuenav.util.PreferredSearchLayoutService;
import com.atlassian.jira.rest.api.http.CacheControl;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.RequiresXsrfCheck;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * A REST endpoint to set the user's preferred search layout.
 *
 * @since v6.0
 */
@AnonymousAllowed
@Path("preferredSearchLayout")
public class PreferredSearchLayoutResource
{
    private final PreferredSearchLayoutService preferredSearchLayoutService;

    public PreferredSearchLayoutResource(
            final PreferredSearchLayoutService preferredSearchLayoutService)
    {
        this.preferredSearchLayoutService = preferredSearchLayoutService;
    }

    /**
     * @return Response containing the key of the user's preferred search layout
     */
    @GET
    public Response get()
    {
        String preferredLayout = preferredSearchLayoutService.getPreferredSearchLayout();
        return Response.ok(preferredLayout).cacheControl(CacheControl.never()).build();
    }

    /**
     * @param layoutKey The layout key to set.
     * @return 200 OK on success.
     */
    @POST
    @RequiresXsrfCheck
    public Response set(@FormParam("layoutKey") String layoutKey)
    {
        this.preferredSearchLayoutService.setPreferredSearchLayout(layoutKey);
        return Response.ok().cacheControl(CacheControl.never()).build();
    }
}