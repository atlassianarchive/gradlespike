package com.atlassian.jira.plugin.issuenav.service.issuetable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * The outcome of a successful request to {@code IssueTableService}, including
 * both the resulting {@code IssueTable} and any warnings that were generated.
 *
 * @since v6.0
 */
@XmlRootElement
public class IssueTableServiceOutcome
{
    @XmlElement private IssueTable issueTable;
    @XmlElement private Collection<String> warnings;

    public IssueTableServiceOutcome()
    {
    }

    public IssueTableServiceOutcome(IssueTable issueTable, Collection<String> warnings)
    {
        this.issueTable = issueTable;
        this.warnings = warnings == null ? Collections.<String>emptyList() : new ArrayList<String>(warnings);
    }

    public IssueTable getIssueTable()
    {
        return issueTable;
    }

    /**
     * @return All warnings that were generated while processing the request,
     *     e.g. "The value 'foo' does not exist for the field 'reporter'."
     */
    public Collection<String> getWarnings()
    {
        return warnings;
    }
}