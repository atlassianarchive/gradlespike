package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Represents the basic query component including text field, criteria and 'More Criteria'
 */
public class AddCriteria
{
    @Inject
    private PageElementFinder elementFinder;
    
    @Inject
    private PageBinder pageBinder;

    @Inject
    private SearcherFactory searcherFactory;

    @ElementBy(id = "criteria-multi-select")
    private PageElement searcherGroups;

    @ElementBy(id = "criteria-input")
    private PageElement criteriaInput;

    @ElementBy(id = "recent-criteria")
    private PageElement recentCriteria;

    @WaitUntil
    public void ready()
    {
        waitUntilTrue(searcherGroups.timed().isVisible());
    }
    
    public boolean searcherExists(String id)
    {
        return searcherGroups.find(By.cssSelector("input[value='" + id + "']")).isPresent();
    }

    public boolean searcherDisabled(String id)
    {
        return searcherGroups.find(By.cssSelector(".disabled input[value='" + id + "']")).isPresent();
    }

    public Set<String> getSelectedCriteria()
    {
        return ImmutableSet.copyOf(Lists.transform(searcherGroups.findAll(By.cssSelector(":checked")), new Function<PageElement, String>()
        {
            @Override
            public String apply(PageElement pageElement)
            {
                return pageElement.getValue();
            }
        }));
    }

    public List<String> getRecentCriteria()
    {
        if (recentCriteria.isPresent())
        {
            return Lists.transform(recentCriteria.findAll(By.tagName("input")), new Function<PageElement, String>()
            {
                @Override
                public String apply(PageElement pageElement)
                {
                    return pageElement.getValue();
                }
            });
        }
        else
        {
            return Collections.emptyList();
        }
    }

    public MultiSelectClauseDialog selectAndOpenFixVersion()
    {
        selectItem("Fix Version");
        return searcherFactory.fixVersion();
    }

    public MultiSelectClauseDialog selectAndOpenAffectsVersion()
    {
        selectItem("Affects Version");
        return searcherFactory.affectsVersion();
    }

    public MultiSelectClauseDialog selectAndOpenComponent()
    {
        selectItem("Component");
        return searcherFactory.component();
    }

    public MultiSelectClauseDialog selectAndOpenReporter()
    {
        selectItem("Reporter");
        return searcherFactory.reporter();
    }

    public MultiSelectClauseDialog selectAndOpenPriority()
    {
        selectItem("Priority");
        return searcherFactory.priority();
    }

    public MultiSelectClauseDialog selectAndOpenResolutions()
    {
        selectItem("Resolution");
        return searcherFactory.resolutions();
    }

    public DateSearcher selectAndOpenDueDate()
    {
        selectItem("Due Date");
        return searcherFactory.dueDate();
    }

    public TextClauseDialog selectAndOpenEnvironment()
    {
        selectItem("Environment");
        return searcherFactory.environment();
    }

    public AddCriteria deselectPriority()
    {
        deselectItem("Priority");
        return pageBinder.bind(this.getClass());
    }

    public AddCriteria deselectReporter()
    {
        deselectItem("Reporter");
        return pageBinder.bind(this.getClass());
    }

    public AddCriteria deselectResolutions()
    {
        deselectItem("Resolution");
        return pageBinder.bind(this.getClass());
    }

    public <T extends CriteriaDialog> T selectAndOpenSearcher(String label, String searcherId, Class<T> dialogClass, Object... params)
    {
        selectItem(label);
        return pageBinder.bind(dialogClass, params);
    }

    public void close()
    {
        if (!criteriaInput.getValue().isEmpty())
        {
            criteriaInput.type(Keys.ESCAPE);
        }
        criteriaInput.type(Keys.ESCAPE);
    }

    /**
     * Select an item given its display value.
     * This will select the item and open its searcher.
     *
     * Does nothing if the item is already selected.
     *
     * @param name The name of the item to select.
     */
    private void selectItem(String name)
    {
        PageElement moreCriteriaElement = elementFinder.find(By.className("more-criteria-footer"));
        MultiSelectClauseDialog dialog = pageBinder.bind(MultiSelectClauseDialog.class, "", "criteria", "");
        if (moreCriteriaElement.isVisible() || dialog.hasSearchText()) {
            dialog.typeKeywordAndSelectValues(name);
        } else {
            dialog.selectValues(name);
        }
    }

    private void deselectItem(String name)
    {
        PageElement moreCriteriaElement = elementFinder.find(By.className("more-criteria-footer"));
        MultiSelectClauseDialog dialog = pageBinder.bind(MultiSelectClauseDialog.class, "", "criteria", "");
        if (moreCriteriaElement.isVisible() || dialog.hasSearchText()) {
            dialog.typeSearchText(name);
        }
        dialog.deselectValues(name);
    }

    public MultiSelectClauseDialog getSparkler()
    {
        return pageBinder.bind(MultiSelectClauseDialog.class, "", "criteria", "");
    }
}
