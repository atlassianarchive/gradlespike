package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.apache.tools.ant.util.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Represents the JQL autocomplete input.
 */
public class AdvancedQueryComponent
{
    @Inject
    private PageElementFinder elementFinder;

    @Inject
    private PageBinder pageBinder;

    @Inject
    private TraceContext traceContext;

    @WaitUntil
    public void ready()
    {
        waitUntilTrue(getSearchInput().timed().isVisible());
    }

    public IssuesPage searchAndWait(String query)
    {
        Tracer tracer = traceContext.checkpoint();
        submit(query);
        return pageBinder.bind(IssuesPage.class).waitForResults(tracer);
    }

    public void typePartialQuery(String partialQuery)
    {
        final PageElement search = getSearchInput();
        search.clear().javascript().execute("jQuery(arguments[0]).keyup()");
        typeAdvancedQuery(partialQuery, search);
    }

    public PageElement getAutocompleteSuggestions()
    {
        waitUntilTrue(elementFinder.find(By.cssSelector(".atlassian-autocomplete .suggestions")).timed().isVisible());
        return elementFinder.find(By.cssSelector(".atlassian-autocomplete .suggestions"));
    }

    public boolean jqlQueryIsValid()
    {
        return getValidityIcon().hasClass("jqlgood");
    }

    public String getCurrentQuery()
    {
        return getSearchInput().getValue();
    }

    private PageElement getValidityIcon()
    {
        return elementFinder.find(By.id("jqlerrormsg"));
    }

    public PageElement getSearchInput()
    {
        return elementFinder.find(By.className("search-entry"));
    }

    public AdvancedQueryComponent submit()
    {
        return submit(null);
    }

    public IssuesPage submitAndWait()
    {
        Tracer tracer = traceContext.checkpoint();
        submit();
        return pageBinder.bind(IssuesPage.class).waitForResults(tracer);
    }

    private AdvancedQueryComponent submit(String query)
    {
        final PageElement search = getSearchInput();
        search.javascript().execute("jQuery(arguments[0]).focus()");
        //this should really type using web-driver but this doesn't work in FF since special characters like '(' and '&' don't send correct keycodes
        //for keydown events: https://code.google.com/p/selenium/issues/detail?id=1723 & https://extranet.atlassian.com/x/ZSDngg
        typeAdvancedQuery(query, search);
        search.type(Keys.RETURN);
        return this;
    }

    private void typeAdvancedQuery(final String query, final PageElement search)
    {
        if(query != null)
        {
            final String escapedQuery = StringUtils.replace(query, "\"", "\\\"");
            search.javascript().execute("jQuery(arguments[0]).val(\"" + escapedQuery + "\")");
            search.javascript().execute("jQuery(arguments[0]).keyup()");
        }
    }
}