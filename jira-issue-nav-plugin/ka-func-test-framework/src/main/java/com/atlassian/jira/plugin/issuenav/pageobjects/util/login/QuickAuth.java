package com.atlassian.jira.plugin.issuenav.pageobjects.util.login;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.sun.jersey.api.client.WebResource;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import javax.ws.rs.core.MediaType;
import java.util.Map;
import java.util.Set;

import static java.util.Collections.singletonMap;

/**
 * Client for logging in/out of JIRA.
 */
public class QuickAuth extends RestApiClient<QuickAuth>
{
    public static QuickAuth forProduct(JiraTestedProduct jiraTestedProduct)
    {
        return new QuickAuth(jiraTestedProduct);
    }

    private final JIRAEnvironmentData environmentData;
    private final JiraTestedProduct jiraTestedProduct;

    public QuickAuth(JiraTestedProduct jiraTestedProduct)
    {
        super(jiraTestedProduct.environmentData());
        this.jiraTestedProduct = jiraTestedProduct;
        this.environmentData = jiraTestedProduct.environmentData();
    }

    public QuickAuth login(String username, String password)
    {
        // we need to make sure that the browser is on the right domain before
        // we start setting cookies.
        ensureBrowserIsOnJiraPage();

        // log in using REST and set the JSESSIONID cookie in the browser
        Map<String, String> cookies = createSession(username, password);
        WebDriver.Options driverOpts = getDriver().manage();
        for (String cookie : cookies.keySet())
        {
            driverOpts.deleteCookieNamed(cookie);
            driverOpts.addCookie(new Cookie(cookie, cookies.get(cookie)));
        }

        return this;
    }

    public QuickAuth logout()
    {
        WebDriver.Options driverOpts = getDriver().manage();
        Set<org.openqa.selenium.Cookie> cookies = driverOpts.getCookies();

        // log out and clear the JSESSIONID cookie in the browser
        if (!cookies.isEmpty())
        {
            deleteSession(cookies);
            driverOpts.deleteAllCookies();
        }

        return this;
    }

    private void ensureBrowserIsOnJiraPage()
    {
        WebDriverTester tester = jiraTestedProduct.getTester();
        WebDriver driver = tester.getDriver();
        if (!driver.getCurrentUrl().startsWith(environmentData.getBaseUrl().toString()))
        {
            // any page will do, this one should be quick to render...
            jiraTestedProduct.getTester().gotoUrl(environmentData.getBaseUrl() + "/secure/AboutPage.jspa");
        }
    }

    private WebDriver getDriver()
    {
        return jiraTestedProduct.getTester().getDriver();
    }

    private Map<String, String> createSession(String username, String password)
    {
        LoginResponse created = session().type(MediaType.APPLICATION_JSON_TYPE).post(LoginResponse.class, new Credentials(username, password));

        return singletonMap(created.session.name, created.session.value);
    }

    private void deleteSession(Set<Cookie> cookies)
    {
        WebResource.Builder builder = session().getRequestBuilder();
        for (Cookie cookie : cookies)
        {
            builder = builder.cookie(new javax.ws.rs.core.Cookie(cookie.getName(), cookie.getValue()));
        }

        builder.delete(); // actually destroys the session
    }

    private WebResource session()
    {
        return resourceRoot(environmentData.getBaseUrl().toExternalForm()).path("rest/auth/1/session");
    }
}
