package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.Option;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.SelectElement;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * A criteria dialog based on a select model
 */
public class SelectClauseDialog extends CriteriaDialog
{

    @Inject
    protected PageElementFinder elementFinder;

    @Inject
    protected PageBinder pageBinder;

    protected final String selectElementId;

    protected SelectElement selectElement;

    public SelectClauseDialog(String expectedDialogTitle, String selectElementId)
    {
        super(expectedDialogTitle);
        this.selectElementId = selectElementId;
    }

    @WaitUntil
    public void ready()
    {
        selectElement = elementFinder.find(By.id(this.selectElementId), SelectElement.class);
        waitUntilTrue(selectElement.timed().isPresent());
    }

    public void selectAndWait(String... values)
    {
        if (getSubmitButton().isPresent()) {
            selectValues(values);
            submitAndWait();
        } else {
            // Auto-updating
            Tracer tracer = traceContext.checkpoint();
            selectValues(values);
            searcherWaiter.waitForSearcherUpdated(tracer);
        }
    }

    /**
     * Selects all of the given values, first deselecting everything.
     */
    public void selectValues(final String... values)
    {
        deselectAllOptions();
        for (String v : values)
        {
            selectOptionByValue(v);
        }
    }

    public List<Option> getAllOptions()
    {
        return selectElement.getAllOptions();
    }

    private void selectOptionByValue(String value)
    {
        // This is done intentionally, as writing tests for the sparkler (autocomplete) control will be flakey and
        // time consuming. There are tests in jira core CheckboxMultiSelect-tests.js that cover testing interactions.
        selectElement.find(By.cssSelector("[value='" + value  + "']")).javascript()
                .execute("jQuery(arguments[0]).attr('selected', 'selected')");
    }

    private void deselectAllOptions()
    {
        for (PageElement option : selectElement.findAll(By.tagName("option"))) {
            if (option.isSelected()) {
                // This is done intentionally, as writing tests for the sparkler (autocomplete) control will be flakey and
                // time consuming. There are tests in jira core CheckboxMultiSelect-tests.js that cover testing interactions.
                option.javascript().execute("jQuery(arguments[0]).removeAttr('selected')");
            }
        }
    }
}
