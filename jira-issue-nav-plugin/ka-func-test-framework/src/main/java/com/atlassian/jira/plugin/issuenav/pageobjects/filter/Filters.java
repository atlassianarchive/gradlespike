package com.atlassian.jira.plugin.issuenav.pageobjects.filter;

import com.atlassian.jira.pageobjects.components.DropDown;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementActions;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.webdriver.AtlassianWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.is;

/**
 * Represents the user's list of favourite filters.
 */
public class Filters
{
    @Inject
    protected AtlassianWebDriver driver;

    @Inject
    private PageElementFinder elementFinder;

    @Inject
    private PageBinder pageBinder;

    @Inject
    private TraceContext traceContext;

    @Inject
    private PageElementActions actions;

    @ElementBy(id = "navigator-sidebar")
    private PageElement filterPanel;

    @ElementBy(cssSelector = "#navigator-sidebar .ui-sidebar")
    private PageElement dragHandle;

    @ElementBy(className = "system-filters")
    private PageElement systemFilters;

    @ElementBy(cssSelector = ".filter-list.favourite-filters")
    private PageElement favouriteFilters;

    @ElementBy(className= "search-title")
    private PageElement searchTitle;

    @ElementBy(className = "save-changes")
    private PageElement saveChanges;

    @ElementBy(className = "save-as-new-filter")
    private PageElement saveCopy;

    @ElementBy(className = "js-edited-trigger")
    private PageElement filterOperationsMenu;

    @ElementBy(id = "save-filter-dialog")
    private PageElement saveFilterDialog;

    @ElementBy(className = "toggle-filter-panel")
    private PageElement toggleFilterPanelLink;

    @WaitUntil
    public void ready()
    {
        waitUntilTrue(filterPanel.timed().isVisible());
    }

    /**
     * Create a new filter from the current search.
     *
     * @param name The name of the new filter.
     * @return {@link com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage}
     */
    public IssuesPage createFilter(String name)
    {
        Tracer trace = traceContext.checkpoint();
        if (!saveCopy.isVisible()) {
            elementFinder.find(By.className("js-edited-trigger")).click();
        }
        saveCopy.click();
        pageBinder.bind(SaveFilterDialog.class).setName(name).submit();
        return pageBinder.bind(IssuesPage.class).waitForFilterSave(trace);
    }

    public IssuesPage createFilterFromOperationsMenu(String name)
    {
        Tracer trace = traceContext.checkpoint();
        filterOperationsMenu.click();
        saveCopy.click();
        pageBinder.bind(SaveFilterDialog.class).setName(name).submit();
        return pageBinder.bind(IssuesPage.class).waitForFilterSave(trace);
    }

    /**
     * @return The element representing the active filter in the sidebar.
     */
    public PageElement getActiveFilterElement()
    {
        return elementFinder.find(By.cssSelector(".filter-list a.active"));
    }

    /**
     * @return The name of the currently active filter or {@code null}.
     */
    public String getActiveFilterName()
    {
        PageElement activeFilter = getActiveFilterElement();
        return activeFilter.isPresent() ? activeFilter.getText() : "";
    }

    /**
     * Wait until a filter is marked as active.
     *
     * @return {@link Filters}
     */
    public Filters waitForActiveFilter()
    {
        waitUntilTrue(getActiveFilterElement().timed().isPresent());
        return this;
    }

    /**
     * Wait until no filter is marked as active.
     *
     * @return {@link Filters}
     */
    public Filters waitForNoActiveFilter()
    {
        waitUntilFalse(getActiveFilterElement().timed().isPresent());
        return this;
    }

    public boolean areSystemFiltersPresent()
    {
        return systemFilters.timed().isPresent().byDefaultTimeout();
    }

    public boolean areFavouriteFiltersPresent()
    {
        return favouriteFilters.timed().isPresent().byDefaultTimeout();
    }

    /**
     * Click the the save filter dialog and bind to it to ensure it opened.
     */
    public SaveFilterDialog openSaveFilterDialog()
    {
        saveCopy.click();
        return pageBinder.bind(SaveFilterDialog.class);
    }

    /**
     * Return immediately as to whether the save filter dialog is open or not.
     */
    public boolean saveFilterDialogIsOpen()
    {
        return saveFilterDialog.isPresent() && saveFilterDialog.isVisible();
    }

    public IssuesPage selectFavourite(String filterName)
    {
        Tracer tracer = traceContext.checkpoint();
        List<PageElement> filterElements = getFavouriteFilterElements();
        for (PageElement filterElement : filterElements)
        {
            if(filterElement.getText().equals(filterName))
            {
                filterElement.click();
                break;
            }
        }

        return pageBinder.bind(IssuesPage.class).waitForResults(tracer);
    }

    public List<String> getFavouriteFilters()
    {
        final List<String> ret = new ArrayList<String>();
        List<PageElement> all = getFavouriteFilterElements();
        for (PageElement pageElement : all)
        {
            ret.add(pageElement.getText());
        }
        return ret;
    }

    private List<PageElement> getFavouriteFilterElements()
    {
        return favouriteFilters.findAll(By.cssSelector("a.filter-link"));
    }

    private List<PageElement> getFavouriteFilterActionMenuElements()
    {
        return favouriteFilters.findAll(By.cssSelector("a.filter-actions"));
    }

    /**
     * Select the system filter with the given name (e.g. "My Open Issues").
     *
     * @param filterName The name of the system filter.
     * @return The issues page pageobject.
     */
    public IssuesPage selectSystem(String filterName)
    {
        Tracer tracer = traceContext.checkpoint();
        for (PageElement filterLink : getSystemFilterElements())
        {
            if (filterLink.getText().equals(filterName))
            {
                filterLink.click();
                break;
            }
        }

        return pageBinder.bind(IssuesPage.class).waitForResults(tracer);
    }

    public FilterActionsMenu openFilterActionsForFilter(int filterId)
    {
        List<PageElement> filterActionMenuElements = getFavouriteFilterActionMenuElements();
        for (PageElement filterActionMenuElement : filterActionMenuElements)
        {
            if (filterActionMenuElement.getAttribute("data-id").equals(String.valueOf(filterId)))
            {
                filterActionMenuElement.click();
                break;
            }
        }

        return pageBinder.bind(FilterActionsMenu.class);
    }

    private List<PageElement> getSystemFilterElements()
    {
        return systemFilters.findAll(By.tagName("a"));
    }

    /**
     * Selects a filter by id. This doesn't need to be a favourite.
     *
     * @param filterId the id of the filter you want to search by.
     */
    public IssuesPage selectFilter(long filterId)
    {
        return pageBinder.navigateToAndBind(IssuesPage.class, "?filter=" + filterId);
    }

    /**
     * Clicks the Save Filter button.
     *
     * @return {@link IssuesPage}
     */
    public IssuesPage saveFilter()
    {
        Tracer tracer = traceContext.checkpoint();
        saveChanges.click();
        return pageBinder.bind(IssuesPage.class).waitForFilterSave(tracer);
    }

    /**
     * Returns the title of the search, which is either the name of the filter if there is a current filter, or the
     * word "Search". Note that a current filter does not have to be in either the favourites or system filters list.
     *
     * @return the search title.
     */
    public String getSearchTitle()
    {
        return searchTitle.getText();
    }

    /**
     * @return {@code true} iff the changes that have been made to the currently
     * selected filter can be saved (they're valid, the user is logged in, etc.).
     */
    public boolean canSaveFilterChanges()
    {
        return saveChanges.isPresent() &&
                !saveChanges.getAttribute("class").contains("disabled");
    }

    /**
     * @return {@code true} iff it's possible to save a copy of the currently
     * selected filter (the search is valid, the user is logged in, etc.).
     */
    public boolean canSaveCopy()
    {
        return saveCopy.isPresent() &&
                !saveCopy.getAttribute("class").contains("disabled");
    }

    public boolean favouriteFilterExists(String name) {
        return getFavouriteFilters().contains(name);
    }


    public DropDown getUpdatedDropDown() {
        return pageBinder.bind(DropDown.class, By.className("js-edited-trigger"), By.id("js-edited-content"));
    }

    public FilterDetailsMenu getFilterDetailsMenu() {
        return pageBinder.bind(FilterDetailsMenu.class);
    }

    public FindFiltersPage getFindFiltersPage() {
        elementFinder.find(By.className("find-filters")).click();
        return pageBinder.bind(FindFiltersPage.class);
    }

    /**
     * Ensure that the filter panel is collapsed.
     */
    public void collapse()
    {
        if (!isCollapsed())
        {
            toggleFilterPanelLink.click();
            waitUntil("Filter panel is collapsed", filterPanel.timed().hasClass("collapsed"), is(true));
        }
    }

    /**
     * Ensure that the filter panel is open.
     */
    public void open()
    {
        if (isCollapsed())
        {
            // doing hover or clicking the element resulted in the sidebar showing and immediately hiding
            // only calling the JS method like this we make sure that sidebar stays expanded
            ((JavascriptExecutor)driver).executeScript("jQuery('#navigator-sidebar').popoutSidebar('expand');");
            waitUntil("Filter panel is open", filterPanel.timed().hasClass("ui-popout-expanded"), is(true));
        }
    }

    /**
     * @return whether the filter panel is collapsed.
     */
    public boolean isCollapsed()
    {
        return filterPanel.hasClass("collapsed");
    }

    /**
     * @return whether the filter panel is collapsed.
     */
    public TimedQuery isCollapsedTimed()
    {
        return filterPanel.withTimeout(TimeoutType.DIALOG_LOAD).timed().hasClass("collapsed");
    }

    public boolean isDocked()
    {
        return !filterPanel.hasClass("ui-popout-detached");
    }

    public void undock()
    {
        if (!isDocked())
        {
            throw new IllegalStateException("Filters are already undocked. Can't re-undock them.");
        }

        filterPanel.find(By.className("ui-undock")).click();
        Poller.waitUntilTrue(filterPanel.timed().hasClass("ui-popout-detached"));
    }

    public Filters dock()
    {
        if (isDocked())
        {
            throw new IllegalStateException("Filters are already docked. Can't re-dock them.");
        }

        filterPanel.type("[");
        Poller.waitUntilTrue(Conditions.not(filterPanel.timed().hasClass("ui-popout-detached")));
        return this;
    }

    /**
     * @return The element representing the active filter in the sidebar.
     */
    public int getWidth()
    {
        return filterPanel.getSize().getWidth();
    }


    public Filters expandBy(final int px)
    {
        actions.dragAndDropBy(dragHandle, px, 0).perform();
        return this;
    }

    public Filters contractBy(final int px)
    {
        actions.dragAndDropBy(dragHandle, -px, 0).perform();
        return this;
    }
}