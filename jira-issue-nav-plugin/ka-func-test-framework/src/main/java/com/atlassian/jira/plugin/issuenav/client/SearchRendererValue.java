package com.atlassian.jira.plugin.issuenav.client;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Value returned by search renderer
 * @since v5.1
 */
@XmlRootElement
public class SearchRendererValue
{
    @XmlElement
    public final String name;
    @XmlElement
    public final String viewHtml;
    @XmlElement
    public final String editHtml;
    @XmlElement
    public final String jql;
    @XmlElement
    public final boolean validSearcher;
    @XmlElement
    public final boolean isShown;

    public SearchRendererValue()
    {
        this.name = null;
        this.jql = null;
        this.viewHtml = null;
        this.editHtml = null;
        this.isShown = false;
        this.validSearcher = false;
    }

    public SearchRendererValue(String name, String jql, String viewHtml, String editHtml, boolean validSearcher, boolean isShown)
    {
        this.name = name;
        this.editHtml = editHtml;
        this.jql = jql;
        this.validSearcher = validSearcher;
        this.isShown = isShown;
        this.viewHtml = viewHtml;
    }
}
