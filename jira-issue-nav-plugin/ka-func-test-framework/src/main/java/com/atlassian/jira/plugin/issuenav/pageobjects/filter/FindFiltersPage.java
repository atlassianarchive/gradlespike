package com.atlassian.jira.plugin.issuenav.pageobjects.filter;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public class FindFiltersPage
{
    @Inject
    private PageBinder pageBinder;

    @Inject
    private PageElementFinder elementFinder;

    @ElementBy (name = "searchName")
    private PageElement searchFilterElement;

    @ElementBy (id = "filterSearchForm-submit")
    private PageElement searchButton;

    public FindFiltersPage()
    {
    }

    public PageElement searchAndGetFiltersName(final String query)
    {
        searchFilterElement.clear().type(query);
        searchButton.click();
        return elementFinder.find(By.className("favourite-item"));
    }

    public void searchAndMakeFavourite(final String query)
    {
        searchFilterElement.clear().type(query);
        searchButton.click();
        PageElement favouriteStatus = elementFinder.find(By.className("favourite-status"));
        waitUntilTrue(favouriteStatus.timed().isPresent());
        favouriteStatus.find(By.tagName("a")).click();
    }

}
