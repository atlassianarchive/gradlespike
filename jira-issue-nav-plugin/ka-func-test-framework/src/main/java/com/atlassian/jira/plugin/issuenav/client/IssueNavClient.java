package com.atlassian.jira.plugin.issuenav.client;

import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * @since v5.1
 */
public class IssueNavClient extends RestApiClient<IssueNavClient>
{
    public static final String REST_VERSION = "1";

    private final JIRAEnvironmentData environmentData;

    public IssueNavClient(final JIRAEnvironmentData environmentData)
    {
        super(environmentData);
        this.environmentData = environmentData;
    }

    public Response get()
    {
        return toResponse(new Method()
        {
            @Override
            public ClientResponse call()
            {
                return issueNav().get(ClientResponse.class);
            }
        });
    }

    private WebResource issueNav()
    {
        return createResource().path("issueNav");
    }

    protected WebResource createResource()
    {
        return resourceRoot(environmentData.getBaseUrl().toExternalForm()).path("rest").path("issueNav").path(REST_VERSION);
    }
}