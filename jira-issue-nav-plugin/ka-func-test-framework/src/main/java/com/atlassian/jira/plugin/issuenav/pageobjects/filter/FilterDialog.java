package com.atlassian.jira.plugin.issuenav.pageobjects.filter;

import com.atlassian.jira.pageobjects.dialogs.FormDialog;
import org.openqa.selenium.By;

/**
 * A Form Dialog for a filter action.
 *
 * @since v5.2
 */
public class FilterDialog extends FormDialog {

    public FilterDialog(String id) {
        super(id);
    }

    /**
     * Set the name of the new filter.
     *
     * @param name The name.
     * @return {@link SaveFilterDialog}
     */
    public FilterDialog setName(String name)
    {
        getDialogElement().find(By.id("filterName")).type(name);
        return this;
    }

    /**
     * Submits the dialog.
     *
     * @return {@code true} iff the dialog is still open.
     */
    public boolean submit()
    {
        return super.submit(getDialogElement().find(
                By.cssSelector("input[type=submit]")));
    }
}
