package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.pageobjects.elements.PageElement;

public class PrimeCriteriaDateComponent extends PrimeCriteriaComponent<DateSearcher>
{
    public PrimeCriteriaDateComponent(PageElement lozenge)
    {
        super(lozenge);
    }

    @Override
    public DateSearcher dialog()
    {
        return pageBinder.bind(DateSearcher.class);
    }
}
