package com.atlassian.jira.plugin.issuenav.pageobjects.util;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.NoSuchElementException;

public class BaseUrlBanner implements Page
{
    @ElementBy (id = "baseurl-dismiss")
    private PageElement dismissLink;

    /**
     * Clicks the "dismiss" link if the banner is present.
     */
    public void dismissIfPresent()
    {
        try
        {
            dismissLink.click();
        }
        catch (NoSuchElementException e)
        {
            // don't care
        }
    }

    @Override
    public String getUrl()
    {
        throw new UnsupportedOperationException("Not implemented");
    }
}
