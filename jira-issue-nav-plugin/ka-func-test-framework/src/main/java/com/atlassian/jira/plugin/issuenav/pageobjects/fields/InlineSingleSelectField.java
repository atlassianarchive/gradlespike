package com.atlassian.jira.plugin.issuenav.pageobjects.fields;

import com.atlassian.jira.plugin.issuenav.pageobjects.controls.SingleSelect;
import com.atlassian.pageobjects.PageBinder;

import javax.inject.Inject;

/**
 * Inline single select dropdown field
 */
public abstract class InlineSingleSelectField extends InlineField
{
    @Inject
    private PageBinder pageBinder;

    private SingleSelect singleSelect;

    @Override
    public Field fill(final String... values)
    {
        SingleSelect singleSelect = singleSelect();
        singleSelect.select(values[0]);
        return this;
    }

    public SingleSelect singleSelect()
    {
        if (null == this.singleSelect)
        {
            this.singleSelect = pageBinder.bind(SingleSelect.class, getId() + "-field", getId() + "-suggestions");
        }
        return this.singleSelect;
    }
}
