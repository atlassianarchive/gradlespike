package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.dialogs.IssueActionsUtil;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.0
 */
public class IssueCommentsPanelComponent
{
    @Inject protected AtlassianWebDriver driver;
    @Inject protected PageElementFinder elementFinder;
    @Inject protected IssueActionsUtil issueActionsUtil;
    @Inject protected PageBinder pageBinder;
    @Inject protected TraceContext traceContext;

    private final static String commentClassName = "activity-comment";

    @ElementBy (id = "comment-tabpanel")
    private PageElement commentTab;

    @ElementBy (id = "issue_actions_container")
    private PageElement commentPanel;

    @WaitUntil
    public boolean ready()
    {
        Poller.waitUntilTrue("Comment tab is not active", commentTab.timed().hasClass("active"));
        Poller.waitUntilTrue("Comments are not present", commentPanel.timed().isVisible());
        return true;
    }

    public List<IssueCommentComponent> getVisibleComments()
    {
        final List<IssueCommentComponent> comments = new ArrayList<IssueCommentComponent>();
        final List<PageElement> commentElements = commentPanel.findAll(By.className(commentClassName));
        for (PageElement commentElement : commentElements)
        {
            comments.add(new IssueCommentComponent(commentElement));
        }

        return comments;
    }

    public List<Long> getVisibleCommentsIds()
    {
        return ImmutableList.copyOf(Lists.transform(getVisibleComments(), new Function<IssueCommentComponent, Long>()
        {
            @Override
            public Long apply(@Nullable IssueCommentComponent input)
            {
                return input.getId();
            }
        }));
    }

    public IssueCommentsPanelComponent clickShowMoreComments()
    {
        PageElement moreCommentsLink = elementFinder.find(By.className("show-more-comments"));
        Poller.waitUntilTrue("More comments link not visible", moreCommentsLink.timed().isVisible());
        moreCommentsLink.click();
        Poller.waitUntilFalse("More comments link doesn't disappear", moreCommentsLink.timed().isVisible());
        return pageBinder.bind(IssueCommentsPanelComponent.class);
    }

    public IssueCommentsPanelComponent clickSort()
    {
        PageElement moreCommentsLink = elementFinder.find(By.className("show-more-comments"));
        Poller.waitUntilTrue("More comments link not visible", moreCommentsLink.timed().isVisible());
        moreCommentsLink.click();
        return pageBinder.bind(IssueCommentsPanelComponent.class);
    }

    public boolean isCommentVisible(long commentId)
    {
        return commentPanel.find(By.id("comment-" + String.valueOf(commentId))).timed().isVisible().byDefaultTimeout();
    }

    public boolean isShowMoreCommentsVisible()
    {
        return commentPanel.find(By.className("show-more-comments")).timed().isVisible().byDefaultTimeout();
    }
}
