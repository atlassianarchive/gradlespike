package com.atlassian.jira.plugin.issuenav.pageobjects.splitview;

import com.atlassian.jira.plugin.issuenav.pageobjects.ShareDialog;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueErrorComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * In interface to the split layout of issue search.
 *
 * @since v6.0
 */
public class SplitLayout
{

    @Inject
    private PageElementFinder elementFinder;
    @Inject
    private PageBinder pageBinder;

    @ElementBy (cssSelector = ".list-results-panel .refresh-table")
    private PageElement refreshLink;

    @ElementBy (cssSelector = ".navigator-content .pagination-view")
    private PageElement paginationView;

    @Inject
    private TraceContext traceContext;

    @WaitUntil
    public void ready()
    {
        waitUntilTrue(elementFinder.find(By.className("split-view")).timed().isPresent());
    }

    /**
     * Click on an issue in the list, causing it to show on the right.
     * <p/>
     * Waits for the issue to load.
     *
     * @param key The key of the issue to click.
     * @return {@code this}
     */
    public SplitLayout clickIssue(final String key)
    {
        Tracer tracer = traceContext.checkpoint();
        issueList().clickIssueAndWait(key);
        return waitForIssue(tracer);
    }

    /**
     * Click on an issue in the list, causing it to show on the right.
     * <p/>
     * Waits for the issue to load.
     *
     * @return {@code this}
     */
    public IssueDetailComponent clickNextIssue()
    {
        Tracer tracer = traceContext.checkpoint();
        String key = issueList().clickNextIssue();
        waitForIssue(tracer);
        return pageBinder.bind(IssueDetailComponent.class, key);
    }

    public IssueDetailComponent clickPrevIssue()
    {
        Tracer tracer = traceContext.checkpoint();
        String key = issueList().clickPrevIssue();
        waitForIssue(tracer);
        return pageBinder.bind(IssueDetailComponent.class, key);
    }

    /**
     * Click the refresh link to refresh the results.
     *
     * @return {@code this}
     */
    public SplitLayout clickRefresh()
    {
        Tracer tracer = traceContext.checkpoint();
        refreshLink.click();
        waitForIssueAndResults(tracer);
        return this;
    }

    /**
     * @return the current page index
     */
    public TimedQuery<String> getCurrentPageIndex()
    {
        return paginationView.find(By.cssSelector(".pagination strong")).timed().getText();
    }

    /**
     * Press the 'page number' link to navigate to that particular page
     *
     * @param pageNumber the page number to navigate into
     * @return {@code this}
     */
    public SplitLayout goToPage(int pageNumber, int pageSize)
    {
        Tracer tracer = traceContext.checkpoint();
        String selector = "a[data-start-index='" + ((pageNumber - 1) * pageSize) + "']";
        PageElement pageLink = paginationView.find(By.cssSelector(selector));
        Poller.waitUntilTrue(pageLink.timed().isVisible());
        pageLink.click();
        waitForIssueAndResults(tracer);
        return this;
    }

    /**
     * @return whether pagination links are visible (they disappear if there's only one page of results).
     */
    public boolean hasPagination()
    {
        return paginationView.isPresent() && !paginationView.findAll(By.cssSelector(".pagination strong")).isEmpty();
    }

    /**
     * @return the currently visible issue.
     */
    public IssueDetailComponent issue()
    {
        String key = elementFinder.find(By.id("key-val")).getText();
        return pageBinder.bind(IssueDetailComponent.class, key);
    }

    /**
     * @return the currently visible issue error.
     */
    public IssueErrorComponent issueError()
    {
        return pageBinder.bind(IssueErrorComponent.class);
    }

    /**
     * @return the list of search results.
     */
    public SplitLayoutIssueList issueList()
    {
        return pageBinder.bind(SplitLayoutIssueList.class);
    }

    /**
     * Press 'j' to view the next issue.
     *
     * @return {@code this}
     */
    public SplitLayout nextIssue(Boolean waitForLoad)
    {
        if (waitForLoad)
        {
            Tracer tracer = traceContext.checkpoint();
            issueList().highlightNextIssue();
            waitForIssue(tracer);
        }
        else
        {
            issueList().highlightNextIssue();
        }
        return this;
    }

    /**
     * Press 'j' to view the next issue.
     *
     * @return {@code this}
     */
    public SplitLayout nextIssue()
    {
        nextIssue(true);
        return this;
    }

    /**
     * Press the 'next' pagination arrow icon to navigate to next page
     *
     * @return {@code this}
     */
    public SplitLayout nextPage()
    {
        Tracer tracer = traceContext.checkpoint();
        PageElement nextIcon = paginationView.find(By.className("icon-next"));
        Poller.waitUntilTrue(nextIcon.timed().isVisible());
        nextIcon.click();
        waitForIssue(tracer);
        return this;
    }

    /**
     * Press 'k' to view the next issue.
     *
     * @return {@code this}
     */
    public SplitLayout prevIssue()
    {
        Tracer tracer = traceContext.checkpoint();
        issueList().highlightPrevIssue();
        waitForIssue(tracer);
        return this;
    }

    /**
     * Press the 'previous' pagination arrow icon to navigate to previous page
     *
     * @return {@code this}
     */
    public SplitLayout prevPage()
    {
        Tracer tracer = traceContext.checkpoint();
        PageElement prevIcon = paginationView.find(By.className("icon-previous"));
        Poller.waitUntilTrue(prevIcon.timed().isVisible());
        prevIcon.click();
        waitForIssue(tracer);
        return this;
    }

    /**
     * Wait for the layout to load an issue.
     *
     * @param tracer The trace checkpoint to poll.
     * @return {@code this}
     */
    private SplitLayout waitForIssue(final Tracer tracer)
    {
        traceContext.waitFor(tracer, "jira.issue.refreshed");
        return this;
    }

    /**
     * Wait for the layout to load an issue and results.
     *
     * @param tracer The trace checkpoint to poll.
     * @return {@code this}
     */
    public SplitLayout waitForIssueAndResults(final Tracer tracer)
    {
        traceContext.waitFor(tracer, "jira.search.finished");
        return waitForIssue(tracer);
    }

    public SplitViewSelectedIssue getSelectedIssue()
    {
        return pageBinder.bind(SplitViewSelectedIssue.class);
    }

    public OrderByPanel getOrderByPanel()
    {
        return pageBinder.bind(OrderByPanel.class);
    }

    public IssueDetailComponent getIssueDetail()
    {
        return pageBinder.bind(IssueDetailComponent.class, getSelectedIssue().getIssueKey());
    }

    public ResponsiveDesign getResponseDesign()
    {
        if (elementFinder.find(By.className("very-skinny")).isPresent())
        {
            return ResponsiveDesign.VERY_SKINNY;
        }
        else if (elementFinder.find(By.className("skinny")).isPresent())
        {
            return ResponsiveDesign.SKINNY;
        }
        else
        {
            return ResponsiveDesign.NORMAL;
        }
    }

    /**
     * Get the share dialog for split view page
     * This scopes the page element to bind onto the correct share button on issue nav page
     */
    public ShareDialog getShareDialog()
    {
        ShareDialog dialog = pageBinder.bind(ShareDialog.class);
        dialog.refineShareTarget("issuenav");
        return dialog;
    }

    /**
     * @return the element on which keyboard shortcuts are invoked.
     */
    public PageElement getKeyboardShortcutTarget()
    {
        return elementFinder.find(By.tagName("body"));
    }

    /**
     * @return the element on which keyboard shortcuts are invoked.
     */
    public IssuesPage switchLayouts()
    {
        getKeyboardShortcutTarget().type("t");
        return pageBinder.bind(IssuesPage.class);
    }

}
