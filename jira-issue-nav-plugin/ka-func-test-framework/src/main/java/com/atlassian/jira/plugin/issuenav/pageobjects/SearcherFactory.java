package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.pageobjects.PageBinder;

import javax.inject.Inject;

/**
 * Factory for getting searcher edit components
 * @since v5.1
 */
public class SearcherFactory
{
    @Inject
    protected PageBinder pageBinder;
    
    public MultiSelectClauseDialog component()
    {
        return pageBinder.bind(MultiSelectClauseDialog.class, "", "searcher-component", "");
    }

    public MultiSelectClauseDialog reporter()
    {
        return pageBinder.bind(MultiSelectClauseDialog.class, "", "reporter", "");
    }

    public MultiSelectClauseDialog priority()
    {
        return pageBinder.bind(MultiSelectClauseDialog.class, "Priorities", "searcher-priority", "All Priorities");
    }

    public MultiSelectClauseDialog resolutions()
    {
        return pageBinder.bind(MultiSelectClauseDialog.class, "Resolutions", "searcher-resolution", "All resolutions");
    }

    public MultiSelectClauseDialog fixVersion()
    {
        return pageBinder.bind(MultiSelectClauseDialog.class, "Fix Version", "searcher-fixfor", "");
    }

    public MultiSelectClauseDialog affectsVersion()
    {
        return pageBinder.bind(MultiSelectClauseDialog.class, "Affects Version", "searcher-version", "");
    }

    public DateSearcher dueDate()
    {
        return pageBinder.bind(DateSearcher.class);
    }

    public TextClauseDialog environment()
    {
        return pageBinder.bind(TextClauseDialog.class, "Environment", "searcher-environment");
    }
}
