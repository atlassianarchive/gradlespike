package com.atlassian.jira.plugin.issuenav.pageobjects.columns;

import com.atlassian.jira.pageobjects.elements.GlobalMessage;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.MultiSelectClauseDialog;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static com.atlassian.pageobjects.elements.query.Conditions.not;
import static com.atlassian.pageobjects.elements.query.Conditions.or;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static junit.framework.Assert.assertTrue;

/**
 * Represents the column picker in list view
 *
 * @since v6.1
 */
public class ColumnPicker
{
    @Inject
    private PageElementFinder elementFinder;
    @Inject
    private PageBinder pageBinder;
    @Inject
    private TraceContext traceContext;

    @ElementBy(className = "column-picker-trigger")
    private PageElement trigger;

    @ElementBy(className = "close-dialog")
    private PageElement cancel;

    
    private PageElement inlineDialog;
    private List<PageElement> configChoosers;
    private Map<String, MultiSelectClauseDialog> columnChoosers;
    private PageElement doneButton;
    private Tracer tracer;

    public TimedQuery<Boolean> canEditColumnChooser()
    {
        String cssClass = "." + getSelectedColumnConfig() + "-column-sparkler";

        //Currently there are 2 clues indicating a disabled sparkler. The error message and grayed out sparkler.
        PageElement message = getMessageInPicker();
        PageElement sparkler = inlineDialog().find(By.cssSelector(cssClass + " div.check-list-select.aui-disabled"));
        //!(message || sparkler)
        return not(or(message.timed().isVisible(), sparkler.timed().isVisible()));
    }

    public List<String> getAllAvailableColumnConfigs()
    {
        beOpen();
        List<String> columnConfigs = Lists.newArrayList();
        for (PageElement element : configChoosers())
        {
            waitUntilTrue(element.timed().isVisible());
            if (elementEnabled(element))
            {
                final String configName = element.getAttribute("data-value");
                assert !Strings.isNullOrEmpty(configName);
                columnConfigs.add(configName);
            }
        }
        return columnConfigs;
    }

    /**
     * Returns a column chooser for the currently active column config.
     *
     * @return a {@link MultiSelectClauseDialog} for choosing columns.
     */
    public MultiSelectClauseDialog getColumnChooser()
    {
        beOpen();
        return columnChoosers().get(getSelectedColumnConfig());
    }

    public String getSelectedColumnConfig()
    {
        beOpen();
        for (PageElement element : configChoosers())
        {
            waitUntilTrue(element.timed().isVisible());
            if (element.hasClass("active"))
            {
                return element.getAttribute("data-value");
            }
        }
        return null;
    }

    public SystemModeMessage getSystemColumnsWarningMessage()
    {
        return pageBinder.bind(SystemModeMessage.class, this);
    }

    public TimedQuery<Boolean> hasRestoreDefaultsLink()
    {
        beOpen();
        return getRestoreDefaultsButton().timed().isVisible();
    }

    public TimedQuery<Boolean> hasMessageInPicker()
    {
        return getMessageInPicker().timed().isVisible();
    }

    public boolean isOpen()
    {
        return inlineDialog().isPresent() && inlineDialog().isVisible();
    }

    public ColumnPicker open()
    {
        trigger.click();
        waitUntilTrue(inlineDialog().timed().isVisible());
        bindElementsInDialog();
        return this;
    }

    public ColumnPicker cancel()
    {   
        cancel.click();
        waitUntilFalse(inlineDialog().timed().isVisible());
        return this;
    }


    public ColumnPicker restoreCurrentConfigDefault()
    {
        beOpen();
        checkpoint();
        getRestoreDefaultsButton().click();
        waitForResult(tracer);
        return this;
    }


    public ColumnPicker selectColumnConfig(String configName)
    {
        return selectColumnConfig(configName, true);
    }


    public ColumnPicker selectColumnConfig(String configName, Boolean waitForResults)
    {
        beOpen();
        for (PageElement element : configChoosers())
        {
            if (element.getAttribute("data-value").equals(configName))
            {
                checkpoint();
                element.click();
                if (waitForResults)
                {
                    waitForResult(tracer);
                }
                columnChoosers().get(configName.toLowerCase()).ready();
                return this;
            }
        }
        assertTrue("There is no column config choice containing the name " + configName, false);
        return null;
    }

    public ColumnPicker submit()
    {
        checkpoint();
        doneButton().click();
        waitForResult(tracer);
        return this;
    }

    public ColumnPicker toggleColumnOptions(List<String> columnIds)
    {
        beOpen();
        MultiSelectClauseDialog sparkler = getColumnChooser();
        sparkler.toggleValues(columnIds, MultiSelectClauseDialog.ClickTarget.INPUT);
        return submit();
    }

    public ColumnPicker toggleColumnOptions(String... columnIds)
    {
        return toggleColumnOptions(Lists.newArrayList(columnIds));
    }

    private PageElement getRestoreDefaultsButton()
    {
        String currentColumns = getSelectedColumnConfig();
        PageElement restoreButton = elementFinder.find(By.cssSelector("." + currentColumns + "-column-sparkler .restore-defaults"));
        waitUntilTrue(restoreButton.timed().isVisible());

        return restoreButton;
    }

    private PageElement getMessageInPicker()
    {
        String currentColumns = getSelectedColumnConfig();
        return inlineDialog().find(By.cssSelector("." + currentColumns + "-column-sparkler div.aui-message"));
    }

    private void bindElementsInDialog()
    {
        configChoosers = inlineDialog().findAll(By.cssSelector(".config-chooser"));
        columnChoosers = Maps.newHashMap();
        for (String config : getAllAvailableColumnConfigs())
        {
            String name = config + "-column-sparkler";
            MultiSelectClauseDialog sparkler = pageBinder.bind(MultiSelectClauseDialog.class, "", name, "");
            columnChoosers.put(config, sparkler);
        }
        doneButton = inlineDialog().find(By.cssSelector(".button-panel input.submit"));
    }

    private static boolean elementEnabled(PageElement element)
    {
        final String disabled = element.getAttribute("aria-disabled");
        return disabled == null || !disabled.equalsIgnoreCase("true");
    }

    private void beOpen()
    {
        if (!isOpen())
        {
            open();
        }
        else
        {
            if (configChoosers == null)
            {
                bindElementsInDialog();
            }
        }

    }

    private void checkpoint()
    {
        tracer = traceContext.checkpoint();
    }

    private ColumnPicker waitForResult(Tracer tracer)
    {
        traceContext.waitFor(tracer, "jira.search.stable.update");
        return this;
    }

    private PageElement inlineDialog()
    {
        if (null == inlineDialog)
        {
            inlineDialog = elementFinder.find(By.id("inline-dialog-column-picker-dialog"));
        }
        return inlineDialog;
    }

    private List<PageElement> configChoosers()
    {
        if (null == configChoosers)
        {
            bindElementsInDialog();
        }
        return configChoosers;
    }

    private Map<String, MultiSelectClauseDialog> columnChoosers()
    {
        if (null == columnChoosers)
        {
            bindElementsInDialog();
        }
        return columnChoosers;
    }

    private PageElement doneButton()
    {
        if (null == doneButton)
        {
            bindElementsInDialog();
        }
        return doneButton;
    }

    public static class SystemModeMessage extends GlobalMessage
    {
        private ColumnPicker columnPicker;

        public SystemModeMessage(ColumnPicker columnPicker)
        {
            this.columnPicker = columnPicker;
        }

        public void exit()
        {
            columnPicker.checkpoint();
            message.find(By.className("exit")).click();
            columnPicker.waitForResult(columnPicker.tracer);
        }
    }
}
