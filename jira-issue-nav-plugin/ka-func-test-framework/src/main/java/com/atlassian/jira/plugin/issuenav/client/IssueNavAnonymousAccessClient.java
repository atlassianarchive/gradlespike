package com.atlassian.jira.plugin.issuenav.client;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class IssueNavAnonymousAccessClient extends RestApiClient<IssueNavAnonymousAccessClient>
{
    public static final String REST_VERSION = "1";

    private final JIRAEnvironmentData environmentData;

    public IssueNavAnonymousAccessClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
        this.environmentData = environmentData;
    }

    public AccessibleResponse getAccessible(final String issueKey, final Long filterId)
    {
        return createResource(issueKey, filterId).get(AccessibleResponse.class);
    }

    public Response getResponse(final String issueKey, final Long filterId)
    {
        return toResponse(new Method()
        {
            @Override
            public ClientResponse call()
            {
                return createResource(issueKey, filterId).get(ClientResponse.class);
            }
        });
    }

    protected WebResource createResource(String issueKey, Long filterId)
    {
        WebResource webResource = resourceRoot(environmentData.getBaseUrl().toExternalForm())
                .path("rest").path("issueNav").path(REST_VERSION)
                .path("issueNav").path("anonymousAccess").path(issueKey);

        if (filterId != null)
        {
            webResource = webResource.queryParam("filterId", String.valueOf(filterId));
        }

        return webResource;
    }
}
