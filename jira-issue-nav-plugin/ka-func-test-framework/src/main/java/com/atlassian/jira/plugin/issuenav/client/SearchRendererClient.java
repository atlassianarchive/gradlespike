package com.atlassian.jira.plugin.issuenav.client;

import com.atlassian.jira.plugin.issuenav.service.SearchRendererValueResults;
import com.atlassian.jira.plugin.issuenav.service.SearchResults;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import java.util.Map;

public class SearchRendererClient extends RestApiClient<SearchRendererClient>
{
    private final JIRAEnvironmentData environmentData;

    public SearchRendererClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
        this.environmentData = environmentData;
    }

    public String getEditHtml(final String fieldId)
    {
        return editHtml(fieldId).get(String.class);
    }
    
    public SearchRendererValueResults getValue(final String ... params)
    {
        return value(params).get(SearchRendererValueResults.class);
    }
    
    public SearchResults getMulti(final String ... params)
    {
        return multi(params).get(SearchResults.class);
    }
    public SearchResults getMultiForJql(final String jql)
    {
        return multiForJql(jql).get(SearchResults.class);
    }

    public Response getEditHtmlResponse(final String fieldId)
    {
        return toResponse(new Method()
        {
            @Override
            public ClientResponse call()
            {
                return editHtml(fieldId).get(ClientResponse.class);
            }
        }, String.class);
    }

    public Response getValueResponse(final String key, final String value)
    {
        return toResponse(new Method()
        {
            @Override
            public ClientResponse call()
            {
                return value(key, value).get(ClientResponse.class);
            }
        }, Map.class);
    }

    public Response getMultiResponse(final String key, final String value)
    {
        return toResponse(new Method()
        {
            @Override
            public ClientResponse call()
            {
                return multi(key, value).get(ClientResponse.class);
            }
        }, Map.class);
    }

    private WebResource editHtml(final String fieldId)
    {
        return createResource().path("QueryComponentRendererEdit!Default.jspa").queryParam("decorator", "none").queryParam("fieldId", fieldId);
    }
    
    private WebResource value(final String ... params)
    {
        WebResource resource = createResource().path("QueryComponentRendererValue!Default.jspa").queryParam("decorator", "none");
        for (int i = 0; i < params.length; i += 2)
        {
            resource = resource.queryParam(params[i], params[i + 1]);
        }
        return resource;
    }

    private WebResource multi(final String ... params)
    {
        WebResource resource = createResource().path("QueryComponent!Default.jspa").queryParam("decorator", "none");
        for (int i = 0; i < params.length; i += 2)
        {
            resource = resource.queryParam(params[i], params[i + 1]);
        }
        return resource;
    }

    private WebResource multiForJql(final String jql)
    {
        WebResource resource = createResource().path("QueryComponent!Jql.jspa").queryParam("decorator", "none");
        resource = resource.queryParam("jql", jql);
        return resource;
    }

    protected WebResource createResource()
    {
        return resourceRoot(environmentData.getBaseUrl().toExternalForm()).path("secure");
    }
}