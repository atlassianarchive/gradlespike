package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.util.concurrent.LazyReference;
import org.openqa.selenium.By;

/**
 * A future IssueDetailComponent, used for timing psycho latency.
 */
public final class FutureIssueDetailComponent
{
    private final PageElementFinder elementFinder;
    private final PageBinder pageBinder;
    private final TraceContext traceContext;

    private final Tracer checkpoint;
    private final String readyKey;
    private final String psychoReadyKey;
    private final String refreshedCachedKey;
    private final LazyReference<IssueDetailComponent> ref = new LazyReference<IssueDetailComponent>()
    {
        protected IssueDetailComponent create() throws Exception
        {
            return pageBinder.bind(IssueDetailComponent.class, elementFinder.find(By.id("key-val")).getText());
        }
    };

    public FutureIssueDetailComponent(PageElementFinder elementFinder, PageBinder pageBinder, TraceContext traceContext)
    {
        this.elementFinder = elementFinder;
        this.pageBinder = pageBinder;
        this.traceContext = traceContext;
        this.checkpoint = traceContext.checkpoint();

        // read stuff off page
        PageElement viewIssueElement = elementFinder.find(By.cssSelector("meta[name=ajs-view-issue-trace-key]"));
        PageElement viewIssuePsychoElement = elementFinder.find(By.cssSelector("meta[name=ajs-view-issue-psycho-key]"));
        PageElement viewIssueRefreshedCachedElement = elementFinder.find(By.cssSelector("meta[name=ajs-view-issue-refreshed-cached-key]"));
        readyKey = viewIssueElement.isPresent() ? viewIssueElement.getAttribute("content") : "jira.issue.standalone";
        psychoReadyKey = viewIssuePsychoElement.isPresent() ? viewIssuePsychoElement.getAttribute("content") : readyKey;
        refreshedCachedKey = viewIssueRefreshedCachedElement.isPresent() ? viewIssueRefreshedCachedElement.getAttribute("content") : null;
    }

    public FutureIssueDetailComponent readyFor(ReadyForAction rfa)
    {
        if (rfa == ReadyForAction.PSYCHO_READY)
        {
            traceContext.waitFor(checkpoint, psychoReadyKey);
        }
        else if (rfa == ReadyForAction.REFRESHED_CACHED_READY)
        {
            traceContext.waitFor(checkpoint, refreshedCachedKey);
        }

        return this;
    }

    public IssueDetailComponent ready()
    {
        traceContext.waitFor(checkpoint, readyKey);
        return ref.get();
    }
}
