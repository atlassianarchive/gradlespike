package com.atlassian.jira.plugin.issuenav.pageobjects.splitview;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementActions;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

/**
 * The list of issues on the left of the split layout.
 *
 * @since v6.0
 */
public class SplitLayoutIssueList
{
    @Inject PageElementFinder elementFinder;

    @Inject
    private PageElementActions actions;

    @ElementBy (cssSelector = ".navigator-content .split-view .list-panel")
    private PageElement listPanel;

    @ElementBy (cssSelector = ".navigator-content .pagination-view")
    private PageElement paginationView;


    @ElementBy(cssSelector = ".navigator-content .ui-sidebar")
    private PageElement dragHandle;

    @WaitUntil
    public void ready()
    {
        waitUntilTrue(listPanel.timed().isVisible());
    }

    /**
     * Click on an issue in the list and wait for it to become highlighted.
     *
     * @param ID The issue's ID.
     * @return {@code this}
     */
    public SplitLayoutIssueList clickIssueAndWait(Long ID)
    {
        listPanel.find(By.cssSelector(".issue-list li[data-id='" + ID + "']")).click();
        waitUntil(getHighlightedIssueElement().timed().getAttribute("data-id"), is(ID.toString()));
        return this;
    }

    /**
     * Click on an issue in the list, but don't wait for it to become highlighted.
     *
     * @param key The key of the issue to click.
     * @return {@code this}
     */
    public SplitLayoutIssueList clickIssue(final String key)
    {
        listPanel.find(By.cssSelector(".issue-list li[data-key='" + key + "']")).click();
        return this;
    }

    /**
     * Click on an issue in the list and wait for it to become highlighted.
     *
     * @param key The key of the issue to click.
     * @return {@code this}
     */
    public SplitLayoutIssueList clickIssueAndWait(final String key)
    {
        clickIssue(key);
        waitUntil(getHighlightedIssueElement().timed().getAttribute("data-key"), is(key));
        return this;
    }

    /**
     * Click on an issue in the list and wait for it to become highlighted.
     *
     * @return {@code this}
     */
    public String clickNextIssue()
    {
        List<PageElement> all = getIssueElements();
        String key = null;
        boolean selectNext = false;
        for (PageElement pageElement : all)
        {
            if (selectNext) {
                key = pageElement.getAttribute("data-key");
                clickIssueAndWait(pageElement.getAttribute("data-key"));
                break;
            }
            else if (pageElement.hasClass("focused"))
            {
                selectNext = true;
            }
        }
        return key;
    }

    public String clickPrevIssue()
    {
        List<PageElement> all = Lists.reverse(getIssueElements());
        String key = null;
        boolean selectNext = false;
        for (PageElement pageElement : all)
        {
            if (selectNext) {
                key = pageElement.getAttribute("data-key");
                clickIssueAndWait(pageElement.getAttribute("data-key"));
                break;
            }
            else if (pageElement.hasClass("focused"))
            {
                selectNext = true;
            }
        }
        return key;
    }

    /**
     * Get list of issues on the page in order
     */
    public List<String> getIssueKeys()
    {
        return ImmutableList.copyOf(Lists.transform(getIssueElements(), new Function<PageElement, String>() {
            @Override
            public String apply(PageElement issue)
            {
                return issue.getAttribute("data-key");
            }
        }));
    }

    private List<PageElement> getIssueElements()
    {
        return listPanel.findAll(By.cssSelector(".issue-list li"));
    }

    /**
     * @return the element corresponding to the currently highlighted issue.
     */
    private PageElement getHighlightedIssueElement()
    {
        return listPanel.find(By.cssSelector(".issue-list li.focused"));
    }

    /**
     * @return the ID of the currently highlighted issue.
     */
    public long getHighlightedIssueID()
    {
        return Long.valueOf(getHighlightedIssueElement().getAttribute("data-id"));
    }

    /**
     * @return the key of the currently highlighted issue.
     */
    public TimedQuery<String> getHighlightedIssueKey()
    {
        return getHighlightedIssueElement().timed().getAttribute("data-key");
    }

    /**
     * @return the summary of the highlighted issue in the list panel
     */
    public TimedQuery<String> getHighlightedIssueSummary()
    {
        return getHighlightedIssueElement().find(By.className("issue-link-summary")).timed().getText();
    }

    /**
     * @return an element on which we can execute keyboard shortcuts.
     */
    private PageElement getKeyboardShortcutTarget()
    {
        return elementFinder.find(By.tagName("body"));
    }

    /**
     * @return the total number of issues returned by the search.
     */
    public int getResultsCountTotal()
    {
        return Integer.parseInt(paginationView.find(By.className("pagination")).getAttribute("data-displayable-total"));
    }

    /**
     * Press 'j' to highlight the next issue in the list.
     *
     * @return {@code this}
     */
    public SplitLayoutIssueList highlightNextIssue()
    {
        final String highlightedIssueKey = getHighlightedIssueKey().now();
        getKeyboardShortcutTarget().type("j");
        waitUntil(getHighlightedIssueElement().timed().getAttribute("data-id"), not(highlightedIssueKey));
        return this;
    }


    /**
     * Press 'k' to highlight the next issue in the list.
     *
     * @return {@code this}
     */
    public SplitLayoutIssueList highlightPrevIssue()
    {
        final String highlightedIssueKey = getHighlightedIssueKey().now();
        getKeyboardShortcutTarget().type("k");
        waitUntil(getHighlightedIssueElement().timed().getAttribute("data-id"), not(highlightedIssueKey));
        return this;
    }

    /**
     * @return whether an issue is highlighted.
     */
    public boolean hasHighlightedIssue()
    {
        return getHighlightedIssueElement().isPresent();
    }

    /**
     * Check whether an issue row is marked as inaccessible.
     * <p/>
     * We can't identify rows by issue ID or key because they're not in the markup.
     *
     * @param index The 0-based index of the issue row to check.
     * @return Whether the issue row at the given index is marked as inaccessible.
     */
    public boolean issueIsInaccessible(final int index)
    {
        List<PageElement> rows = listPanel.findAll(By.cssSelector("li"));

        if (index >= 0 && rows.size() > index)
        {
            return rows.get(index).hasClass("inaccessible-issue");
        }
        else
        {
            throw new IllegalArgumentException("There is no issue row at index " + index + ".");
        }
    }


    public SplitLayoutIssueList expandBy(final int px)
    {
        actions.dragAndDropBy(dragHandle, px, 0).perform();
        return this;
    }

    public SplitLayoutIssueList contactBy(final int px)
    {
        actions.dragAndDropBy(dragHandle, -px, 0).perform();
        return this;
    }
}
