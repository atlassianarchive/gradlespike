package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public class TextClauseDialog extends CriteriaDialog
{

    @Inject
    private PageElementFinder elementFinder;

    @Inject
    private PageBinder pageBinder;

    private final String textElementId;

    private PageElement textElement;

    public TextClauseDialog(String expectedDialogTitle, String textElementId)
    {
        super(expectedDialogTitle);
        this.textElementId = textElementId;
    }
    
    @WaitUntil
    public void ready()
    {
        textElement = elementFinder.find(By.id(this.textElementId), PageElement.class);
        waitUntilTrue(textElement.timed().isVisible());
    }

    public void setTextAndWait(String value)
    {
        setText(value);
        submitAndWait();
    }

    public void setText(String value)
    {
        textElement.clear();
        textElement.type(value);
    }
}
