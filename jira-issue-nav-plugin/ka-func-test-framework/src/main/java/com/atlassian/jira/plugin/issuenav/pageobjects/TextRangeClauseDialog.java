package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public class TextRangeClauseDialog extends CriteriaDialog
{

    @Inject
    private PageElementFinder elementFinder;

    @Inject
    private PageBinder pageBinder;

    private final String textBeforeId;
    private final String textAfterId;

    private PageElement beforeElement;
    private PageElement afterElement;

    public TextRangeClauseDialog(String expectedDialogTitle, String textBeforeId, String textAfterId)
    {
        super(expectedDialogTitle);
        this.textBeforeId = textBeforeId;
        this.textAfterId = textAfterId;
    }
    
    @WaitUntil
    public void ready()
    {
        beforeElement = elementFinder.find(By.id(this.textBeforeId), PageElement.class);
        waitUntilTrue(beforeElement.timed().isVisible());

        afterElement = elementFinder.find(By.id(this.textAfterId), PageElement.class);
        waitUntilTrue(afterElement.timed().isVisible());
    }

    public void setRangeAndWait(String before, String after)
    {
        setText(beforeElement, before);
        setText(afterElement, after);
        submitAndWait();
    }

    public void setText(PageElement element, String value)
    {
        element.clear();
        element.type(value);
    }
}
