package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.columns.ColumnPicker;
import com.atlassian.jira.plugin.issuenav.pageobjects.filter.Filters;
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview.SplitLayout;
import com.atlassian.jira.plugin.issuenav.pageobjects.util.SearcherWaiter;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import java.util.List;
import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.equalTo;

/**
 * Entry page to kickass that you see after /issues
 */
public class IssuesPage extends AbstractJiraPage
{
    public static final String BROWSE_URI_PUSH_STATE = "/browse/";
    public static final String BROWSE_URI_NON_PUSH_STATE = "/i#browse/";
    public static final String ISSUES_URI_PUSH_STATE = "/issues/";
    public static final String ISSUES_URI_NON_PUSH_STATE = "/i#issues/";

    private final boolean hasPushState;
    private final String selectedIssueKey;
    private final String uriPattern;

    @ElementBy (className = "navigator-content")
    private PageElement navigatorContent;

    @ElementBy (className = "mode-switcher")
    private PageElement searchSwitcher;

    @ElementBy (className = "new-search")
    private PageElement newSearch;

    @ElementBy (className = "search-title")
    private PageElement searchTitle;

    @ElementBy (className = "refresh-table")
    private PageElement refreshTableButton;

    @ElementBy(className = "header-views")
    private PageElement viewsButton;

    // The notifications area containing errors and warnings.
    @ElementBy (cssSelector = ".navigator-search .notifications")
    private PageElement notifications;

    @ElementBy (cssSelector = "#user-options .login-link")
    private PageElement loginLink;

    @ElementBy (cssSelector = ".issue-table-info-bar .results-count-text")
    private PageElement resultsCount;

    @Inject
    private ProductInstance productInstance;

    @Inject
    private SearcherWaiter searcherWaiter;

    @Inject
    private TraceContext traceContext;

    public IssuesPage()
    {
        hasPushState = true;
        selectedIssueKey = null;
        uriPattern = null;
    }

    /**
     * @param uriPattern queryString to append to the url (eg "?jql=project=blah")
     */
    public IssuesPage(String uriPattern)
    {
        this(true, uriPattern);
    }

    public IssuesPage(boolean hasPushState, String uriPattern)
    {
        this(hasPushState, uriPattern, null);
    }

    public IssuesPage(boolean hasPushState, String uriPattern, String selectedIssueKey)
    {
        this.hasPushState = hasPushState;
        this.uriPattern = uriPattern;
        this.selectedIssueKey = selectedIssueKey;
    }

    @Override
    public TimedCondition isAt()
    {
        return navigatorContent.withTimeout(TimeoutType.PAGE_LOAD).timed().isVisible();
    }

    /**
     * Waits for search results
     * @param tracer checkpoint before search results loading was triggered
     */
    public IssuesPage waitForResults(Tracer tracer)
    {
        traceContext.waitFor(tracer, "jira.search.finished");
        return this;
    }

    /**
     * Waits for search results
     */
    public IssuesPage waitForResults()
    {
        return waitForResults(traceContext.checkpoint());
    }

    /**
     * Waits for search results when stable search is on
     * @param tracer checkpoint before search results loading was triggered
     * @return
     */
    public IssuesPage waitForStableUpdate(Tracer tracer)
    {
        traceContext.waitFor(tracer, "jira.search.stable.update");
        return this;
    }

    /**
     * Waits for search results
     * @param tracer checkpoint before search results loading was triggered
     */
    public IssuesPage waitForFilterSave(Tracer tracer)
    {
        traceContext.waitFor(tracer, "jira.filter.saved");
        return this;
    }

    /**
     * Waits for search results
     * @param tracer checkpoint before search results loading was triggered
     * @param waitForSearchers true to wait for searchers to load
     */
    public IssuesPage waitForResults(Tracer tracer, boolean waitForSearchers)
    {
        if (waitForSearchers)
        {
            searcherWaiter.waitForSearcherUpdated(tracer);
        }
        traceContext.waitFor(tracer, "jira.search.finished");
        return this;
    }

    public IssuesPage waitForSearchModeSet(Tracer tracer)
    {
        traceContext.waitFor(tracer, "jira.search.mode.changed");
        return this;
    }

    /**
     * @return the element on which keyboard shortcuts are invoked.
     */
    public PageElement getKeyboardShortcutTarget()
    {
        return elementFinder.find(By.tagName("body"));
    }

    /**
     * @return SplitLayout
     */
    public SplitLayout switchLayouts()
    {
        getKeyboardShortcutTarget().type("t");
        return pageBinder.bind(SplitLayout.class);
    }

    /**
     * Switch layouts without waiting for the bind, can be used to test switching layout shortcuts which shouldn't succeed.
     */
    public void switchLayoutsWithoutWait()
    {
        getKeyboardShortcutTarget().type("t");
    }

    /**
     * @return whether the layout switcher is disabled
     */
    public TimedCondition isLayoutSwitcherDisabled()
    {
        return getLayoutSwitcher().isDisabled();
    }

    @Override
    public String getUrl()
    {
        String URI;
        if (selectedIssueKey != null)
        {
            URI = (hasPushState ? BROWSE_URI_PUSH_STATE : BROWSE_URI_NON_PUSH_STATE) + selectedIssueKey;
        }
        else
        {
            URI = hasPushState ? ISSUES_URI_PUSH_STATE : ISSUES_URI_NON_PUSH_STATE;
        }

        if (null != uriPattern)
        {
            URI += uriPattern;
        }

        return URI;
    }

    public ResultsTableComponent getResultsTable()
    {
        return pageBinder.bind(ResultsTableComponent.class);
    }

    public IssuesPage waitForResultsTable()
    {
        getResultsTable(); // waits for results table to appear
        return this;
    }

    public BasicQueryComponent getBasicQuery()
    {
        switchSearchMode("basic");
        return pageBinder.bind(BasicQueryComponent.class);
    }

    public AdvancedQueryComponent getAdvancedQuery()
    {
        switchSearchMode("advanced");
        return pageBinder.bind(AdvancedQueryComponent.class);
    }

    public ColumnPicker getColumnPicker()
    {
        return pageBinder.bind(ColumnPicker.class);
    }

    public Filters getFilters()
    {
        return pageBinder.bind(Filters.class);
    }

    public IssuesPage back()
    {
        driver.navigate().back();
        return this;
    }

    public IssuesPage backAndWait(boolean waitForResults, boolean waitForSearchers)
    {
        Tracer tracer = traceContext.checkpoint();
        driver.navigate().back();
        if (waitForResults) {
            waitForResults(tracer);
        }
        if (waitForSearchers) {
            searcherWaiter.waitForSearcherUpdated(tracer);
        }
        return this;
    }

    public IssuesPage forward()
    {
        driver.navigate().forward();
        return this;
    }

    public IssuesPage forwardAndWait(boolean waitForResults, boolean waitForSearchers)
    {
        Tracer tracer = traceContext.checkpoint();
        driver.navigate().forward();
        if (waitForResults) {
            waitForResults(tracer);
        }
        if (waitForSearchers) {
            searcherWaiter.waitForSearcherUpdated(tracer);
        }
        return this;
    }

    /**
     * @return returns a relative window location with the application's baseurl stripped out.
     */
    public String getWindowLocation()
    {
        final String windowLocation = driver.getCurrentUrl();
        final String baseUrl = productInstance.getBaseUrl();
        if (windowLocation.startsWith(baseUrl))
        {
            return windowLocation.substring(baseUrl.length());
        }
        else
        {
            return windowLocation;
        }
    }

    public IssuesPage newSearch(boolean waitForResults)
    {
        Tracer tracer = traceContext.checkpoint();
        newSearch.click();

        if (waitForResults)
        {
            waitForResults(tracer);
        }

        return this;
    }

    public String getCurrentSearchTitle()
    {
        return searchTitle.getText();
    }

    /**
     * @return whether the "No Results" message is visible.
     */
    public boolean emptySearchResultsMessageIsVisible()
    {
        List<PageElement> elements = navigatorContent.findAll(By.cssSelector(".no-results"));
        return elements.size() > 0 && elements.get(0).isVisible();
    }

    public TimedQuery<String> getEmptySearchResultMessage()
    {
        return navigatorContent.find(By.cssSelector(".no-results h3")).timed().getText();
    }

    public IssuesPage newSearch()
    {
        return newSearch(true);
    }

    public TimedCondition isBasicSearch()
    {
        return searchSwitcher.find(By.cssSelector("[data-id=\"basic\"]")).timed().hasClass("active");
    }

    /**
     * @return whether the navigator content is pending (semi-transparent).
     */
    public boolean isPending()
    {
        return navigatorContent.hasClass("pending");
    }

    public TimedCondition isSwitcherDisabled()
    {
        return searchSwitcher.timed().hasClass("disabled");
    }

    private void switchSearchMode(final String toMode)
    {
        final PageElement activeSwitcher = searchSwitcher.find(By.className("active"));
        if (!activeSwitcher.getAttribute("data-id").equals(toMode))
        {
            activeSwitcher.click();
            waitUntil(searchSwitcher.find(By.className("active")).timed().getAttribute("data-id"), equalTo(toMode));
        }
    }

    public <T> T backToSearch(Class<T> result)
    {
        elementFinder.find(By.id("return-to-search")).click();
        return pageBinder.bind(result);
    }

    public IssuesPage quickSearch(final String quickSearchQuery)
    {
        elementFinder.find(By.id("quickSearchInput")).type(Keys.chord(quickSearchQuery, Keys.RETURN));
        return pageBinder.bind(IssuesPage.class);
    }

    /**
     * Refresh the search results by clicking the refresh button.
     * <p/>
     * Will time out if the button isn't visible or refreshing fails.
     */
    public void refreshTable()
    {
        Tracer tracer = traceContext.checkpoint();
        waitUntilTrue(refreshTableButton.timed().isVisible());
        refreshTableButton.click();
        waitForResults(tracer);
    }

    /**
     * @return The error message elements in the notification area.
     */
    private PageElement getJQLErrorElements()
    {
        return notifications.find(By.cssSelector(".aui-message.error"));
    }

    /**
     * @return {@code true} iff there are errors in the notification area.
     */
    public boolean hasJQLErrors()
    {
        return getJQLErrorElements().isPresent();
    }

    /**
     * @return The error text visible in the notification area.
     */
    public String getJQLErrors()
    {
        return getJQLErrorElements().getText();
    }

    /**
     * @return The warning message elements in the notification area.
     */
    private PageElement getJQLWarningElements()
    {
        return notifications.find(By.cssSelector(".aui-message.warning"));
    }

    /**
     * @return {@code true} iff there are warnings in the notification area.
     */
    public boolean hasJQLWarnings()
    {
        return getJQLWarningElements().isPresent();
    }

    /**
     * @return The warning text visible in the notification area.
     */
    public String getJQLWarnings()
    {
        return getJQLWarningElements().getText();
    }

    /**
     * @return the {@code href} attribute of the permalink element.
     */
    public String getPermalinkHref()
    {
        return getShareDialog().open().getSearchPermalinkValue().now();
    }

    /**
     * @return tools menu
     */
    public ToolsDropDown tools()
    {
        return pageBinder.bind(ToolsDropDown.class, By.className("header-tools"), By.className("header-tools-menu"));
    }

    /**
     * @return {@code true} iff the query controls are visible.
     */
    public boolean isQueryVisible()
    {
        return elementFinder.find(By.cssSelector(".search-container")).isVisible();
    }

    /**
     * Discard changes to the current filter and wait for results.
     */
    public void discardFilterChanges()
    {
        Tracer tracer = traceContext.checkpoint();
        elementFinder.find(By.className("js-edited-trigger")).click();
        PageElement discardChanges = elementFinder.find(By.className("discard-filter-changes"));
        waitUntilTrue(discardChanges.timed().isVisible());
        discardChanges.click();
        waitForResults(tracer, true);
    }

    public String getLoginLinkUrl()
    {
        return loginLink.getAttribute("href");
    }

    public void scrollToTop()
    {
        driver.executeScript("jQuery(window).scrollTop(0)");
    }

    public String getResultsCount()
    {
        return resultsCount.getText();
    }

    /**
     * @return an interface to the layout switcher control.
     */
    private LayoutSwitcher getLayoutSwitcher()
    {
        return pageBinder.bind(LayoutSwitcher.class);
    }

    /**
     * Ensure that the list layout is being used.
     *
     * @return an interface to the list layout.
     */
    public IssuesPage getListLayout()
    {
        Tracer tracer = traceContext.checkpoint();
        if (getLayoutSwitcher().selectListView())
        {
            // IssueTableView fires a stable update trace after rendering.
            waitForStableUpdate(tracer);
        }

        return this;
    }

    /**
     * Ensure that the split layout is being used.
     *
     * @return an interface to the split layout.
     */
    public SplitLayout getSplitLayout()
    {
        return getSplitLayout(traceContext.checkpoint());
    }

    /**
     * Ensure that the split layout is being used.
     *
     * @param tracer The trace checkpoint to poll.
     * @return an interface to the split layout.
     */
    public SplitLayout getSplitLayout(final Tracer tracer)
    {
        boolean switched = getLayoutSwitcher().selectSplitView();
        SplitLayout splitLayout = pageBinder.bind(SplitLayout.class);

        if (switched)
        {
            splitLayout.waitForIssueAndResults(tracer);
        }

        return splitLayout;
    }

    /**
     * @return whether the page is currently using list layout.
     */
    public boolean isListLayout()
    {
        return elementFinder.find(By.id("issuetable")).isVisible();
    }

    /**
     * @return whether the page is currently using split layout.
     */
    public boolean isSplitLayout()
    {
        return elementFinder.find(By.tagName("body")).hasClass("page-type-split");
    }

    /**
     * Click the views button and bind to the issuenav views options
     */
    public ViewsDropDown openViewsMenu()
    {
        ViewsDropDown dropDown = pageBinder.bind(ViewsDropDown.class, By.className("header-views"), By.id("viewOptions-dropdown"));
        dropDown.open();
        return dropDown;
    }

    /**
     * Click the views button and bind to the view issue issuenav views and export options
     */
    public ViewsDropDown openFullViewsMenu()
    {
        ViewsDropDown dropDown = pageBinder.bind(ViewsDropDown.class, By.className("header-views"), By.className("header-views-menu"));
        dropDown.open();
        return dropDown;
    }

    /**
     * @return whether the page has the first basic query criteria(project) focused
     */
    public TimedQuery<Boolean> isFirstCriteriaFocused()
    {
        return new AbstractTimedCondition(5000, Timeouts.DEFAULT_INTERVAL)
        {
            @Override
            protected Boolean currentValue()
            {
                String dataId = driver.switchTo().activeElement().getAttribute("data-id");

                return (dataId != null && dataId.equals("project"));
            }
        };
    }

    /**
     * @return whether the "end of stable search" message is visible.
     */
    public boolean endOfStableSearchMessageIsVisible()
    {
        return elementFinder.find(By.className("end-of-stable-message")).isVisible();
    }

    public Boolean hasVisibleDialog()
    {
        return elementFinder.find(By.className("aui-blanket")).isPresent();
    }

    public ShareDialog getShareDialog()
    {
        return pageBinder.bind(ShareDialog.class);
    }

    /**
     * Get the share dialog for view issue page, using a shortcut
     */
    public ShareDialog getShareDialogViaShortcut()
    {
        ShareDialog dialog = pageBinder.bind(ShareDialog.class);
        dialog.refineShareTarget("issuenav");
        dialog.openViaKeyboardShortcut();
        return dialog;
    }

}
