package com.atlassian.jira.plugin.issuenav.client;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

@JsonSerialize
public final class SlomoPattern
{
    public boolean enabled;
    public String pattern;
    public long delay;
    public int id;

    @SuppressWarnings("unused")
    public SlomoPattern()
    {
        // used by Jackson
    }

    public SlomoPattern(@Nonnull String pattern, long delay)
    {
        this.pattern = checkNotNull(pattern);
        this.delay = delay;
        this.enabled = true;
    }

    public SlomoPattern(@Nonnull String pattern, long delay, boolean enabled, int id)
    {
        this.pattern = checkNotNull(pattern);
        this.delay = delay;
        this.enabled = enabled;
        this.pattern = pattern;
        this.id = id;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SlomoPattern that = (SlomoPattern) o;

        return pattern.equals(that.pattern);
    }

    @Override
    public int hashCode()
    {
        return pattern.hashCode();
    }
}
