package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Option;
import com.atlassian.pageobjects.elements.PageElement;
import com.google.inject.Inject;

import java.util.List;

/**
 * Represents a prime criteria component whose searcher is an HTML select
 */
public class PrimeSelectCriteria extends PrimeCriteriaComponent<MultiSelectClauseDialog>
{
    @Inject
    private TraceContext traceContext;

    @ElementBy (tagName = "body")
    private PageElement body;

    protected String expectedDialogTitle;
    protected String searcherId;
    protected String allOptionsLabel;


    public PrimeSelectCriteria(PageElement lozenge, String expectedDialogTitle, String searcherId, String allOptionsLabel)
    {
        super(lozenge);
        this.expectedDialogTitle = expectedDialogTitle;
        this.searcherId = searcherId;
        this.allOptionsLabel = allOptionsLabel;
    }

    public IssuesPage selectValueAndCancelWithEsc(String value) {
        final MultiSelectClauseDialog dialog = open();
        dialog.selectValues(value);
        dialog.cancelWithEsc();
        return pageBinder.bind(IssuesPage.class);
    }

    public IssuesPage selectValueAndCancelWithClick(String value) {
        final MultiSelectClauseDialog dialog = open();
        dialog.selectValues(value);
        dialog.cancelWithClick();
        return pageBinder.bind(IssuesPage.class);
    }

    public IssuesPage setValueAndSubmit(String... values)
    {
        Tracer tracer = traceContext.checkpoint();
        // Wait for both searcher updated and search results
        if(values.length > 0) {
            open().selectAndWait(values);
            return pageBinder.bind(IssuesPage.class).waitForResults(tracer);
        } else {
            return pageBinder.bind(IssuesPage.class).waitForResultsTable();
        }
    }

    public IssuesPage removeValuesAndSubmit(String... values) {
        Tracer tracer = traceContext.checkpoint();
        if(values.length > 0) {
            open().deselectValues(values);
            return pageBinder.bind(IssuesPage.class).waitForResults(tracer);
        } else {
            return pageBinder.bind(IssuesPage.class).waitForResultsTable();
        }
    }

    public IssuesPage clear() {
        Tracer tracer = traceContext.checkpoint();
        open().clear();
        return pageBinder.bind(IssuesPage.class).waitForResults(tracer);
    }

    public IssuesPage setSearchText(final String value)
    {
        final MultiSelectClauseDialog dialog = open();
        dialog.typeSearchText(value);
        return pageBinder.bind(IssuesPage.class);
    }

    public IssuesPage clearSearchText()
    {
        final MultiSelectClauseDialog dialog = open();
        dialog.clearSearchTextWithButton();
        return pageBinder.bind(IssuesPage.class);
    }

    public IssuesPage setValueAndClickOutside(String value)
    {
        return setValueAndClickOutside(value, true);
    }

    public IssuesPage setValueAndClickOutside(String value, boolean waitForResults)
    {
        Tracer tracer = traceContext.checkpoint();
        final MultiSelectClauseDialog dialog = open();
        dialog.selectValues(value);

        // JRADEV-15994
        // IE Webdriver weirdness
        body.javascript().execute("AJS.$('html').scrollTop(0).click()");
        IssuesPage issuesPage = pageBinder.bind(IssuesPage.class);

        if (waitForResults) {
            return issuesPage.waitForResults(tracer);
        } else {
            return issuesPage;
        }

    }

    public int getAllOptionsCount()
    {
        return open().getAllOptionsCount();
    }

    public List<Option> getOptions()
    {
        return open().getAllOptions();
    }

    @Override
    public MultiSelectClauseDialog dialog()
    {
        return pageBinder.bind(MultiSelectClauseDialog.class, this.expectedDialogTitle, this.searcherId, this.allOptionsLabel);
    }

    /**
     * Deselect all options and submit.
     *
     * @return The issues page.
     */
    public IssuesPage deselectAllAndSubmit()
    {
        Tracer tracer = traceContext.checkpoint();
        if (open().getNumSelected() > 0) {
            open().deselectAll();
            return pageBinder.bind(IssuesPage.class).waitForResults(tracer);
        }
        else {
            return pageBinder.bind(IssuesPage.class);
        }
    }
}
