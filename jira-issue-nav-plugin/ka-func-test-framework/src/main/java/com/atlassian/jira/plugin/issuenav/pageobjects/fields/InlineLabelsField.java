package com.atlassian.jira.plugin.issuenav.pageobjects.fields;

import com.atlassian.jira.plugin.issuenav.pageobjects.controls.MultiSelect;

/**
 * The issue's labels field.
 */
public class InlineLabelsField extends InlineField
{
    private MultiSelect multiSelect;

    @Override
    protected String getTriggerSelector()
    {
        return "#wrap-labels .editable-field";
    }

    @Override
    public Field fill(final String... values)
    {
        MultiSelect multiSelect = multiSelect();
        for (String value : values)
        {
            multiSelect.add(value);
        }
        return this;
    }

    @Override
    public String getId()
    {
        return "labels";
    }

    public MultiSelect multiSelect()
    {
        if (null == multiSelect)
        {
            multiSelect = binder.bind(MultiSelect.class, getId());
        }
        return multiSelect;
    }
}