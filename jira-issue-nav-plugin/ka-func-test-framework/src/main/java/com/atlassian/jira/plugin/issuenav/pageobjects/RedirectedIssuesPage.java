package com.atlassian.jira.plugin.issuenav.pageobjects;

/**
 * Goes to classic issue navigator and expects to be redirect to kickass /issues/ (IssuesPage.java)
 */
public class RedirectedIssuesPage extends IssuesPage
{
    @Override
    public String getUrl()
    {
        return "/secure/IssueNavigator.jspa";
    }
}
