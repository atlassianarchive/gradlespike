package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Issue ops bar
 * TODO: this should be in JIRA core. Should move there.
 */
public class OpsBarComponent
{
    @Inject
    private PageBinder pageBinder;

    @Inject
    private PageElementFinder elementFinder;
    
    @ElementBy(id = "opsbar-operations_more")
    private PageElement moreActions;

    @ElementBy(id = "opsbar-operations_more_drop")
    private PageElement moreActionsDropDown;

    @ElementBy(id = "clone-issue")
    private PageElement clone;

    @ElementBy(id = "action_id_5")
    private PageElement resolve;

    public IssueDetailComponent cloneIssueExpectingKey(String expectedNewIssueKey)
    {
        // TODO: create cloneDialog in JIRA core
        openMoreActions();
        clone.click();
        PageElement submitCloneDialog = elementFinder.find(By.id("assign-issue-submit"));
        waitUntilTrue(submitCloneDialog.timed().isVisible());
        submitCloneDialog.click();

        return pageBinder.bind(IssueDetailComponent.class, expectedNewIssueKey);
    }

    public PageElement openResolveIssueDialog()
    {
        resolve.click();
        PageElement resolveIssueDialog = elementFinder.find(By.id("workflow-transition-5-dialog"));
        //wait until we can change the resolution
        waitUntilTrue(resolveIssueDialog.find(By.id("resolution")).timed().isVisible());
        return resolveIssueDialog;

    }

    public TimedCondition hasOptionInDropdown(String optionid)
    {
        openMoreActions();
        return getOptionInDropdown(optionid).timed().isVisible();
    }

    public void clickOptionInDropdown(String optionid)
    {
        openMoreActions();
        getOptionInDropdown(optionid).click();
    }

    private PageElement getOptionInDropdown(String optionId)
    {
        return moreActionsDropDown.find(By.cssSelector("li #" + optionId));
    }

    private void openMoreActions()
    {
        if (!moreActionsDropDown.isPresent() || !moreActionsDropDown.isVisible()) {
            moreActions.click();
            waitUntilTrue(moreActionsDropDown.timed().isVisible());
        }
    }
}
