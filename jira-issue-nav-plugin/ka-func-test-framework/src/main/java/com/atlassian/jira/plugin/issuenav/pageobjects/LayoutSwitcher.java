package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * The layout switcher control.
 *
 * @since v6.0
 */
public class LayoutSwitcher
{
    final private static String LIST_VIEW_LAYOUT_KEY = "list-view";
    final private static String SPLIT_VIEW_LAYOUT_KEY = "split-view";

    @ElementBy (cssSelector = "#layout-switcher-toggle .aui-button")
    private PageElement element;

    @Inject
    private PageElementFinder pageElementFinder;

    @WaitUntil
    public void ready()
    {
        waitUntilTrue(element.timed().isVisible());
    }

    private boolean selectLayout(final String layoutKey)
    {
        element.click();

        By locator = By.cssSelector(".ajs-layer.active [data-layout-key='" + layoutKey + "']");
        PageElement listItem = pageElementFinder.find(locator);

        boolean selected = listItem.find(By.className("aui-iconfont-success")).isPresent();

        if (!selected && listItem.isPresent())
        {
            listItem.click();
            return true;
        }
        else
        {
            // Close the drop down.
            element.click();
            return false;
        }
    }

    /**
     * @return if the layout switcher is disabled
     */
    public TimedCondition isDisabled()
    {
        return element.timed().hasClass("disabled");
    }


    /**
     * Select the list view layout.
     *
     * @return {@code true} if list view was selected, {@code false} if it was already selected.
     */
    public boolean selectListView()
    {
        return selectLayout(LIST_VIEW_LAYOUT_KEY);
    }

    /**
     * Select the split view layout.
     *
     * @return {@code true} if split view was selected, {@code false} if it was already selected.
     */
    public boolean selectSplitView()
    {
        return selectLayout(SPLIT_VIEW_LAYOUT_KEY);
    }
}