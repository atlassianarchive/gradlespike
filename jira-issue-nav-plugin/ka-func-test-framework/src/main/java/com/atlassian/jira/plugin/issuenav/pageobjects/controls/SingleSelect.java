package com.atlassian.jira.plugin.issuenav.pageobjects.controls;

import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * SingleSelect control
 * @since v5.1
 */
public class SingleSelect
{
    @Inject
    private PageElementFinder elementFinder;

    private final String inputId;
    private final String suggestionsId;

    private PageElement input;
    private PageElement suggestions;

    public SingleSelect(String inputId, String suggestionsId)
    {
        this.inputId = inputId;
        this.suggestionsId = suggestionsId;
    }

    @Init
    private void initialize()
    {
        this.input = elementFinder.find(By.id(inputId));
        this.suggestions = elementFinder.find(By.id(suggestionsId));
    }

    public void select(final String item)
    {
        waitUntilTrue(input.timed().isVisible());
        input.clear().type(item);
        waitUntilTrue("Expected suggestions to be present, but was not", suggestions.timed().isVisible());
        input.type(Keys.RETURN);
        waitUntilTrue(input.timed().hasValue(item));
    }

    public TimedCondition isSuggestionsVisible()
    {
        return suggestions.timed().isVisible();
    }

    public String getValue()
    {
        waitUntilTrue(input.timed().isVisible());
        return input.getValue();
    }
}
