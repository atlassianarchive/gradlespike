package com.atlassian.jira.plugin.issuenav.pageobjects.filter;

import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.SelectElement;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.openqa.selenium.By;

import java.util.List;
import javax.inject.Inject;

public class EditFilterPage
{

    @Inject
    private PageElementFinder pageElementFinder;

    @Inject
    private PageBinder pageBinder;

    @ElementBy (id = "share_type_selector")
    private SelectElement shareTypeSelector;

    @ElementBy (id="filter-edit-submit")
    private PageElement submitButton;

    public EditFilterPage()
    {
    }

    public IssuesPage setSharingGroupAndSubmit(final String group)
    {
        shareTypeSelector.select(Options.text("Group"));
        List<PageElement> options = pageElementFinder.findAll(By.tagName("option"));
        PageElement chosenGroup = Iterables.find(options, new Predicate<PageElement>()
        {
            @Override
            public boolean apply(final PageElement option)
            {
                return option.getText().equals(group);
            }
        });
        chosenGroup.click();
        pageElementFinder.find(By.id("share_add_group")).click();
        submitButton.click();
        return pageBinder.bind(IssuesPage.class);
    }

    public IssuesPage removeTheOnlyShareAndSubmit()
    {
        pageElementFinder.find(By.className("shareTrash")).click();
        submitButton.click();
        return pageBinder.bind(IssuesPage.class);
    }
}
