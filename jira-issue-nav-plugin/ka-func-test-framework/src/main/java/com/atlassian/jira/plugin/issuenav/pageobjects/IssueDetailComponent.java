package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.dialogs.FormDialog;
import com.atlassian.jira.pageobjects.dialogs.quickedit.WorkflowTransitionDialog;
import com.atlassian.jira.pageobjects.model.WorkflowIssueAction;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.plugin.issuenav.pageobjects.ShareDialog;
import com.atlassian.jira.pageobjects.dialogs.ShifterDialog;
import com.atlassian.jira.pageobjects.pages.viewissue.ActionTrigger;
import com.atlassian.jira.pageobjects.pages.viewissue.SubtaskModule;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.Field;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.InlineAssigneeField;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.InlineDescriptionField;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.InlineDueDateField;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.InlineLabelsCustomField;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.InlineLabelsField;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.InlinePriorityField;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.InlineReporterField;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.InlineSummaryField;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.InlineTextCustomField;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.VersionField;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.TimedElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.equalTo;

/**
 * Represents the detailed view of an issue.
 */
public class IssueDetailComponent extends AbstractIssueDetailComponent {
    private static final String FOCUS_SHIFTER_ACTIVATE = ",";

    @Inject
    private PageElementFinder pageElementFinder;

    @ElementBy (cssSelector = "meta[name=ajs-return-to-search-trace-key]")
    private PageElement returnToSearchElement;

    @ElementBy (cssSelector = "meta[name=ajs-return-to-search-psycho-key]")
    private PageElement returnToSearchPsychoElement;

    @ElementBy(id = "key-val")
    private PageElement issueKeyElem;

    private final String issueKey;

    public IssueDetailComponent(final String issueKey)
    {
        this.issueKey = issueKey;
    }

    public boolean isAt()
    {
        return issueKeyElem.isPresent();
    }

    @WaitUntil
    public boolean ready()
    {
        waitUntil(issueKeyElem.timed().getText(), equalTo(issueKey));
        return true;
    }

    public boolean readyForEdit()
    {
        waitUntil(issueKeyElem.timed().getText(), equalTo(issueKey));
        waitUntil(hasDescriptionModule(), equalTo(true));
        return waitUntil(hasEditableFields(), equalTo(true));
    }

    public IssueDetailComponent editComment(int id, String text)
    {
        Tracer checkpoint = traceContext.checkpoint();
        pageElementFinder.find(By.id("edit_comment_" + id)).javascript().mouse().click();
        pageBinder.bind(EditCommentDialog.class).setComment(text).submit();
        traceContext.waitFor(checkpoint, "jira.issue.refreshed");
        return this;
    }

    public IssueDetailComponent scrollTo(int pos, boolean isSplitView)
    {
        if (isSplitView)
        {
            driver.executeScript("jQuery(\".issue-container\").scrollTop(" + pos + ")");
        }
        else
        {
            driver.executeScript("jQuery(window).scrollTop(" + pos + ")");
        }
        return this;
    }

    public boolean scrollPositionAtTop(boolean isSplitView)
    {
        if (isSplitView)
        {
            return ((Long)driver.executeScript("return jQuery(\".issue-container\").scrollTop()") == 0);
        }
        else
        {
            return ((Long)driver.executeScript("return jQuery(window).scrollTop()") == 0);
        }
    }

    public void bringCommentButtonIntoView()
    {
        driver.executeScript("jQuery(\"#footer-comment-button\").scrollIntoView()");
    }

    public String getComment(final int id)
    {
        return pageElementFinder.find(By.cssSelector("#comment-" + id + " .action-body")).getText().trim();
    }

    public void startProgress(String traceKey) {
        if (hasMoreTransitions()) {
            clickMoreTransitions();
        }
        Tracer checkpoint = traceContext.checkpoint();
        issueActionsUtil.startProgress(ActionTrigger.MENU);
        traceContext.waitFor(checkpoint, traceKey);
    }

    public void stopProgress(String traceKey)
    {
        clickMoreTransitions();
        Tracer checkpoint = traceContext.checkpoint();
        issueActionsUtil.stopProgress(ActionTrigger.MENU);
        traceContext.waitFor(checkpoint, traceKey);
    }

    public void resolveIssue(String traceKey)
    {
        clickMoreTransitions();
        Tracer checkpoint = traceContext.checkpoint();
        issueActionsUtil.resolveIssue(ActionTrigger.MENU);
        traceContext.waitFor(checkpoint, traceKey);
    }

    public void closeIssue(String traceKey)
    {
        clickMoreTransitions();
        Tracer checkpoint = traceContext.checkpoint();
        issueActionsUtil.closeIssue(ActionTrigger.MENU);
        traceContext.waitFor(checkpoint, traceKey);
    }

    public WorkflowTransitionDialog openWorkflowTransitionDialog(long actionId, String uiName)
    {
        final ViewIssuePage viewIssuePage = pageBinder.bind(ViewIssuePage.class, this.issueKey);
        return viewIssuePage.getIssueMenu().invokeWorkflowAction(new WorkflowIssueAction(actionId, uiName));
    }

    /**
     * Delete the issue.
     *
     * @param traceKey The trace key that indicates the action has finished.
     */
    public void deleteIssue(String traceKey)
    {
        Tracer checkpoint = traceContext.checkpoint();
        issueActionsUtil.delete(ActionTrigger.ACTIONS_DIALOG);
        traceContext.waitFor(checkpoint, traceKey);
    }

    public boolean isStalkerPinnedToTopOfWindow()
    {
        // PageElement does not provide a reliable way to test if a element is visible
        // (isVisible() returns true even if the element is not visible due the scroll)
        // So we need to use JavaScript to know it.
        return (Boolean)driver.executeScript("return jQuery(\"#stalker\").offset().top === jQuery(document).scrollTop()");
    }

    public String getWindowTitle()
    {
        return (String) driver.executeScript("return document.title");
    }

    public String getUrlHashComponent()
    {
        ready();
        return (String) driver.executeScript("return window.location.hash");
    }

    public String getIssueKey()
    {
        return issueKeyElem.getText();
    }

    public TimedCondition hasEditableFields()
    {
        return elementFinder.find(By.className("editable-field")).timed().isPresent();
    }

    public TimedCondition hasDescriptionModule()
    {
        return elementFinder.find(By.id("descriptionmodule")).timed().isPresent();
    }

    public String getSummary()
    {
        return summary().getValue();
    }

    public InlineSummaryField summary()
    {
        return pageBinder.bind(InlineSummaryField.class);
    }

    public InlineDescriptionField description()
    {
        return pageBinder.bind(InlineDescriptionField.class);
    }

    public InlinePriorityField priority()
    {
        return pageBinder.bind(InlinePriorityField.class);
    }

    public VersionField fixVersion()
    {
        return pageBinder.bind(VersionField.class, VersionField.FIXVERSION_ID, VersionField.FIXVERSION_TRIGGER_SELECTOR);
    }

    public VersionField affectsVersion()
    {
        return pageBinder.bind(VersionField.class, VersionField.AFFECTSVERSION_ID, VersionField.AFFECTSVERSION_TRIGGER_SELECTOR);
    }

    public InlineAssigneeField assignee()
    {
        return pageBinder.bind(InlineAssigneeField.class);
    }

    public void waitUntilAssigneeIs(String assignee)
    {
        PageElement pageElement = pageElementFinder.find(By.cssSelector("#assignee-val"));
        Poller.waitUntilEquals(assignee, pageElement.timed().getText());
    }

    public InlineReporterField reporter()
    {
        return pageBinder.bind(InlineReporterField.class);
    }

    public InlineLabelsField labels()
    {
        return pageBinder.bind(InlineLabelsField.class);
    }

    public InlineDueDateField dueDate()
    {
        return pageBinder.bind(InlineDueDateField.class);
    }

    public List<String> getFieldsInError()
    {
        List<String> fields = new ArrayList<String>();
        final List<PageElement> errors = elementFinder.findAll(By.className("error"));
        for (PageElement error : errors)
        {
            fields.add(error.getAttribute("data-field"));
        }
        return fields;
    }

    public List<String> getLabels()
    {
        List<String> labels = new ArrayList<String>();
        final List<PageElement> lozenges = elementFinder.findAll(By.cssSelector("#wrap-labels .lozenge"));
        for (PageElement lozenge : lozenges)
        {
            labels.add(lozenge.getAttribute("title"));
        }
        return labels;
    }

    public TimedQuery<String> getBodyClasses()
    {
        return elementFinder.find(By.tagName("body")).timed().getAttribute("class");
    }

    public IssueDetailComponent clickIssueLink(String issueKey)
    {
        String selector = "#linkingmodule a[href][data-issue-key='" + issueKey + "']";
        PageElement issueLink = elementFinder.find(By.cssSelector(selector));
        waitUntilTrue(issueLink.timed().isVisible());
        waitUntilEquals(issueKey, issueLink.timed().getText());
        issueLink.click();
        return pageBinder.bind(IssueDetailComponent.class, issueKey);
    }

    public IssueDetailComponent clickSubtask(String issueKey)
    {
        String selector = "#view-subtasks a[data-issue-key='" + issueKey + "']";
        PageElement issueLink = elementFinder.find(By.cssSelector(selector));
        waitUntilTrue(issueLink.timed().isVisible());
        issueLink.click();
        return pageBinder.bind(IssueDetailComponent.class, issueKey);
    }

    public int addCommentUsingFooterLink(String comment)
    {
        return addComment(comment, "#footer-comment-button");
    }

    public int addCommentUsingHeaderLink(String comment)
    {
        return addComment(comment, "#comment-issue");
    }

    private int addComment(String comment, String triggerCssSelector)
    {
        setCommentValue(comment, triggerCssSelector);
        elementFinder.find(By.id("issue-comment-add-submit")).click();

        // Wait for the save to complete.
        By locator = By.cssSelector("#issue-comment-add .loading");
        waitUntilFalse(elementFinder.find(locator).timed().isPresent());
        PageElement focusedComment = elementFinder.find(By.cssSelector(".activity-comment.focused"));
        String domId = focusedComment.getAttribute("id");
        String dbId = domId.replace("comment-", "");
        return Integer.parseInt(dbId);
    }

    /**
     * @return whether the share button is displayed in the issue content page.
     */
    public TimedCondition hasShareButton()
    {
        return elementFinder.find(By.cssSelector("#issue-content #jira-share-trigger")).timed().isPresent();
    }

    /**
     * @return whether the view button is displayed in the issue content page.
     */
    public TimedCondition hasViewsButton()
    {
        return elementFinder.find(By.id("viewissue-export")).timed().isPresent();
    }

    /**
     * @return whether the login button is displayed in the issue content page.
     */
    public TimedCondition hasLoginButton()
    {
        return elementFinder.find(By.cssSelector("#issue-content #ops-login-link")).timed().isVisible();
    }

    /**
     * @return The header's comment form; clicks the "Comment" button if needed.
     */
    public PageElement getHeaderCommentForm()
    {
        By by = By.cssSelector("header.issue-header #issue-comment-add");
        PageElement commentForm = elementFinder.find(by);

        if (!commentForm.isPresent())
        {
            //Unfortunately, page object doesn't have context based selector
            //Have to narrow down the context in order to get the correct element
            PageElement commentButton = elementFinder.find(By.cssSelector("#issue-content #comment-issue"));
            waitUntilTrue(commentButton.timed().isPresent());
            commentButton.click();

            commentForm = elementFinder.find(by);
            waitUntilTrue(commentForm.timed().isPresent());
        }

        return commentForm;
    }

    private IssueDetailComponent setCommentValue(String comment, String triggerCssSelector)
    {
        final PageElement button = elementFinder.find(By.cssSelector(triggerCssSelector));
        button.timed().isPresent();
        button.click();
        final PageElement commentEl = elementFinder.find(By.id("comment"));
        commentEl.type(comment);
        return this;
    }

    private PageElement getCommentForId (String id) {
        return elementFinder.find(By.cssSelector("#comment-" + id));
    }
    /**
     * Waits for a comment with the given id to appear. This is kind of a pain in the arse to write but it's unflaky.
     *
     * @return comment text
     */
    public String waitForCommentWithId(String id)
    {
        PageElement element  = getCommentForId(id).find(By.className("action-body"));

        return element.getText();
    }

    public TimedCondition commentIsFocused(String id)
    {
        TimedElement comment = getCommentForId(id).timed();

        return Conditions.and(comment.isVisible(), comment.hasClass("focused"));
    }

    public IssueDetailComponent setXSRFTokenToJunk()
    {
        driver.executeScript("jQuery(\"#atlassian-token\").attr('content', 'zkjdfhsakjrfhdwkjhr');");
        return this;
    }

    public void waitForGlobalErrorMessage()
    {
        PageElement errorMessage = elementFinder.find(By.cssSelector("div.global-msg > div.error"));
        waitUntilTrue(errorMessage.timed().isPresent());
    }

    public IssueDetailComponent waitForXSRFRetryButton()
    {
        waitUntilTrue(xsrfRetryButton().timed().isVisible());
        return this;
    }

    public IssueDetailComponent clickXSRFRetryButton()
    {
        xsrfRetryButton().click();
        return this;
    }

    PageElement xsrfRetryButton()
    {
        return elementFinder.find(By.id("atl_token_retry_button"));
    }

    public IssueDetailComponent waitForEditButton()
    {
        waitUntilTrue(editButton().timed().isVisible());
        return this;
    }

    public IssueDetailComponent waitForNextIssueLink()
    {
        waitUntilTrue(getNextIssueLink().timed().isVisible());
        return this;
    }

    /**
     * Clicks in random white space to clause a blur.
     * WARNING: If the browser window does not have the focus, it will not do anything.
     */
    public IssueDetailComponent clickInWhitespace()
    {
        elementFinder.find(By.cssSelector("body")).click();
        return this;
    }

    /**
     * Clicks the "Edit" button, presenting the quick edit dialog.
     */
    public IssueDetailComponent clickEditButton()
    {
        editButton().click();
        return this;
    }

    /**
     * Clicks the "More Actions" button, presenting the More Actions menu.
     */
    public IssueDetailComponent clickMoreActions()
    {
        moreActionsButton().click();
        return this;
    }

    /**
     * Clicks the "Workflow" button, presenting the More Transitions menu.
     */
    public IssueDetailComponent clickMoreTransitions()
    {
        moreTransitionsButton().click();
        return this;
    }

    /**
     * @return whether the "Workflow" button is present
     */
    public boolean hasMoreTransitions()
    {
        return moreTransitionsButton().isPresent();
    }

    /**
     * Click the "Edit" button and bind to the quick edit dialog that opens.
     *
     * @return the quick edit dialog page object.
     */
    public FormDialog openEditDialog()
    {
        clickEditButton();
        return pageBinder.bind(FormDialog.class, "edit-issue-dialog");
    }

    public IssueDetailComponent clickDetailsTwixie()
    {
        detailsTwixieTitle().click();
        return this;
    }

    public ShifterDialog openFocusShifter()
    {
        PageElement body = elementFinder.find(By.tagName("body"));
        body.type(FOCUS_SHIFTER_ACTIVATE);
        return pageBinder.bind(ShifterDialog.class);
    }

    public boolean isDetailsTwixieOpen()
    {
        return !elementFinder.find(By.cssSelector("#details-module")).hasClass("collapsed");
    }

    PageElement editButton()
    {
        return elementFinder.find(By.cssSelector("#issue-content #edit-issue"));
    }

    PageElement moreActionsButton()
    {
        return elementFinder.find(By.cssSelector("#opsbar-operations_more"));
    }

    PageElement moreTransitionsButton()
    {
        return elementFinder.find(By.cssSelector("#opsbar-transitions_more"));
    }

    public boolean isMoreActionsOpen()
    {
        PageElement pageElement = elementFinder.find(By.id("opsbar-operations_more_drop"));
        return pageElement.isPresent() && pageElement.hasClass("active");
    }

    public boolean isMoreTransitionsOpen()
    {
        PageElement pageElement = elementFinder.find(By.id("opsbar-transitions_more_drop"));
        return pageElement.isPresent() && pageElement.hasClass("active");
    }

    PageElement detailsTwixieTitle()
    {
        return elementFinder.find(By.cssSelector("#details-module_heading > .toggle-title"));
    }

    public InlineLabelsCustomField getCustomLabelsField(long fieldId)
    {
        return pageBinder.bind(InlineLabelsCustomField.class, fieldId);
    }

    public InlineTextCustomField getCustomTextField(long fieldId)
    {
        return pageBinder.bind(InlineTextCustomField.class, fieldId);
    }

    public OpsBarComponent opsBar()
    {
        return pageBinder.bind(OpsBarComponent.class);
    }

    public boolean fieldIsInError(String fieldId)
    {
        return getFieldsInError().contains(fieldId);
    }

    public boolean fieldIsInError(Field field)
    {
        return fieldIsInError(field.getId());
    }

    /**
     * The next/previous pager contains a textual description of the issue's
     * position in session searchresults (e.g. 123 of 1234).
     *
     * @return a description of the issue's position.
     */
    public String getPositionText()
    {
        PageElement element = nextPreviousIssuePager.find(By.className("showing"));
        Matcher matcher = Pattern.compile("(\\d+ of \\d+)").matcher(element.getText());

        if (matcher.find())
        {
            return matcher.group(0);
        }

        return null;
    }

    public IssueDetailComponent clickIssueTabPanel(String tabPanelKey)
    {
        // click the switch tab link
        Tracer checkpoint = traceContext.checkpoint();
        elementFinder.find(By.id(tabPanelKey)).click();
        traceContext.waitFor(checkpoint, "jira.issue.tab.loaded");

        return rebindThisComponent();
    }

    public String getIssueTabPanel()
    {
        return elementFinder.find(By.cssSelector("#issue-tabs li.active")).getAttribute("id");
    }

    public Boolean activityModuleDoesNotContainModules()
    {
        return elementFinder.findAll(By.cssSelector("#activitymodule .module")).size() == 0;
    }

    public PageElement getHistoryIssueTabPanel()
    {
        return elementFinder.find(By.cssSelector("#issue-tabs li[data-id='changehistory-tabpanel']"));
    }

    /**
     * Select the custom field tab at a given index.
     *
     * @param index The 0-based index of the custom field tab to select (the leftmost tab has index 0).
     * @return the issue page.
     */
    public IssueDetailComponent selectCustomFieldTab(int index)
    {
        By locator = By.cssSelector("#customfield-tabs li.menu-item:nth-child(" + (index + 1) + ")");
        PageElement tab = elementFinder.find(locator);
        tab.find(By.tagName("a")).click();
        waitUntilTrue(tab.timed().hasClass("active-tab"));
        return this;
    }

    /**
     * @return the 0-based index of the currently selected custom field tab (the leftmost tab has index 0).
     */
    public int getSelectedCustomFieldTabIndex()
    {
        List<PageElement> tabs = elementFinder.findAll(By.cssSelector("#customfield-tabs li.menu-item"));
        for (int i = 0; i < tabs.size(); i++)
        {
            if (tabs.get(i).hasClass("active-tab"))
            {
                return i;
            }
        }

        return -1;
    }

    private IssueDetailComponent rebindThisComponent()
    {
        return pageBinder.bind(IssueDetailComponent.class, elementFinder.find(By.id("key-val")).getText());
    }

    /**
     * Click the "Return to Search" link and wait for the page to load.
     *
     * @return the issue search page.
     */
    public IssuesPage returnToSearch()
    {
        final String traceKey = returnToSearchTraceKey();

        Tracer checkpoint = traceContext.checkpoint();
        elementFinder.find(By.id("return-to-search")).click();
        traceContext.waitFor(checkpoint, traceKey);

        return pageBinder.bind(IssuesPage.class);
    }

    /**
     * Return to issue search by pressing the 'u' key.
     *
     * @return the issue search page.
     */
    public IssuesPage returnToSearchUsingKeyboardShortcut()
    {
        Tracer checkpoint = traceContext.checkpoint();
        getKeyboardShortcutTarget().type("u");
        traceContext.waitFor(checkpoint, returnToSearchTraceKey());

        return pageBinder.bind(IssuesPage.class);
    }

    public IssuesPage returnToSearchUsingIssuesMenu()
    {
        Tracer checkpoint = traceContext.checkpoint();
        pageBinder.bind(JiraHeader.class).getIssuesMenu().open().searchForIssues();
        traceContext.waitFor(checkpoint, returnToSearchTraceKey());

        return pageBinder.bind(IssuesPage.class);
    }

    private String returnToSearchTraceKey()
    {
        return returnToSearchElement.isPresent() ? returnToSearchElement.getAttribute("content") : "jira.search.finished";
    }

    /**
     * @return the DOM element used to handle keyboard shortcuts
     */
    private PageElement getKeyboardShortcutTarget()
    {
        return elementFinder.find(By.tagName("body"));
    }

    public IssueDetailComponent selectIssueUsingKey(CharSequence key)
    {
        return selectIssueUsingKey(key, ReadyForAction.READY).ready();
    }

    public FutureIssueDetailComponent selectIssueUsingKey(CharSequence key, ReadyForAction readyForAction)
    {
        final FutureIssueDetailComponent issue = new FutureIssueDetailComponent(elementFinder, pageBinder, traceContext);
        getKeyboardShortcutTarget().type(key);
        return issue.readyFor(readyForAction);
    }

    public IssueDetailComponent nextIssueUsingKeyboardShortcut()
    {
        return selectIssueUsingKey("j");
    }

    public FutureIssueDetailComponent nextIssueUsingKeyboardShortcut(ReadyForAction readyForAction)
    {
        return selectIssueUsingKey("j", readyForAction);
    }

    public IssueDetailComponent previousIssueUsingKeyboardShortcut()
    {
        return selectIssueUsingKey("k");
    }

    public FutureIssueDetailComponent previousIssueUsingKeyboardShortcut(ReadyForAction readyForAction)
    {
        return selectIssueUsingKey("k", readyForAction);
    }

    public IssueCommentsPanelComponent openCommentsTabPanel()
    {
        return openCommentsTabPanel(false);

    }

    public IssueCommentsPanelComponent openCommentsTabPanel(boolean ascendingSort)
    {
        elementFinder.find(By.id("comment-tabpanel")).click();

        PageElement sortIcon = elementFinder.find(By.cssSelector("#activitymodule .sortwrap .icon"));
        Poller.waitUntilTrue(sortIcon.timed().isVisible());
        boolean isAscending = sortIcon.hasClass("icon-sort-up");
        if (ascendingSort != isAscending)
        {
            sortIcon.click();
            Poller.waitUntilTrue(sortIcon.withTimeout(TimeoutType.COMPONENT_LOAD).timed().hasClass("icon-sort-down"));
        }
        return pageBinder.bind(IssueCommentsPanelComponent.class);
    }

    /**
     * @return an interface to the subtasks module.
     */
    public SubtaskModule subtasks()
    {
        return pageBinder.bind(SubtaskModule.class);
    }

    /**
     * Get the share dialog for view issue page
     * This scopes the page element to bind onto the correct share button on view issue page
     */
    public ShareDialog getShareDialog()
    {
        ShareDialog dialog = pageBinder.bind(ShareDialog.class);
        dialog.refineShareTarget("viewissue");
        return dialog;
    }

    /**
     * Get the share dialog for view issue page, using a shortcut
     */
    public ShareDialog getShareDialogViaShortcut()
    {
        ShareDialog dialog = getShareDialog();
        dialog.openViaKeyboardShortcut();
        return dialog;
    }

    /**
     * @return the element on which keyboard shortcuts are invoked.
     */
    public IssueDetailComponent switchLayoutsWithoutWait()
    {
        getKeyboardShortcutTarget().type("t");
        return this;
    }
}