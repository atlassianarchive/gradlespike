package com.atlassian.jira.plugin.issuenav.pageobjects.fields;

/**
 * The issue's priority field.
 */
public class InlinePriorityField extends InlineSingleSelectField
{
    @Override
    protected String getTriggerSelector()
    {
        return "#priority-val.editable-field";
    }

    @Override
    public String getId()
    {
        return "priority";
    }
}
