package com.atlassian.jira.plugin.issuenav.pageobjects.controls;

import com.google.common.base.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * This is the same as com.atlassian.jira.pageobjects.components.fields.MultiSelect
 * but not flaky!
 * @since v6.0
 */
public class MultiSelect extends com.atlassian.jira.pageobjects.components.fields.MultiSelect {
    public MultiSelect(String id) {
        super(id);
    }

    public MultiSelect(String id, Function<String, By> itemLocator) {
        super(id, itemLocator);
    }

    @Override
    public void addNotWait(final String item)
    {
        textArea.type(item);
        waitUntilTrue("Expected suggestions to be present, but was not", isSuggestionsPresent());
        textArea.type(Keys.RETURN);
    }
}
