package com.atlassian.jira.plugin.issuenav.pageobjects.fields;

/**
 * The issue's due date field.
 */
public class InlineDueDateField extends InlineField
{
    @Override
    protected String getTriggerSelector()
    {
        return "#due-date";
    }

    @Override
    public String getId()
    {
        return "duedate";
    }
}