package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.components.fields.MultiSelect;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.google.common.base.Function;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Represents the the sharing inline dialog on the view issue and issue navigator pages
 *
 * @since v6.0.4
 */
public class ShareDialog
{
    @ElementBy (cssSelector = "#jira-share-trigger")
    private PageElement trigger;

    @ElementBy(tagName = "body")
    protected PageElement body;

    @Inject protected PageBinder binder;
    @Inject protected PageElementFinder locator;
    @Inject protected TraceContext traceContext;

    protected PageElement dialog;
    protected MultiSelect shareMultiSeclect;

    private String targetContext;

    public boolean isOpen()
    {
        return dialog != null && dialog.isVisible();
    }

    public TimedQuery<Boolean> isOpenTimed()
    {
        if (dialog != null)
        {
            return dialog.timed().isVisible();
        }
        else
        {
            return Conditions.alwaysFalse();
        }
    }

    public Point getLocation()
    {
        return dialog.getLocation();
    }

    public List<String> getRecipients()
    {
        return getRecipients("data-username");
    }

    public List<String> getEmailRecipients()
    {
        return getRecipients("data-email");
    }

    public ShareDialog addRecipient(String recipient)
    {
        shareMultiSeclect.add(recipient);
        return this;
    }

    public ShareDialog removeRecipient(String username)
    {
        shareMultiSeclect.remove(username);
        return this;
    }

    public ShareDialog addNote(String text)
    {
        dialog.find(By.id("note")).type(text);
        return this;
    }

    public ShareDialog submit()
    {
        dialog.find(By.className("submit")).click();
        waitUntilFalse(dialog.timed().isVisible());
        return this;
    }

    public ShareDialog selectShareTarget(String target) {
        PageElement shareTargets = dialog.find(By.className("share-targets"));

        if (shareTargets.isVisible()) {
            PageElement shareTarget = shareTargets.find(By.cssSelector("input[value='" + target + "']"));
            waitUntilTrue(shareTarget.timed().isVisible());
            shareTarget.click();
        }

        return this;
    }

    // TODO: WRONG, the open methods do not belong here
    public ShareDialog openViaKeyboardShortcut()
    {
        body.type("s");
        bindElements();
        return this;
    }

    public ShareDialog open()
    {
        trigger.click();
        bindElements();
        return this;
    }

    public TimedQuery<Boolean> hasDisabledOutgoingMailMessage()
    {
        if (!isOpen())
        {
            open();
        }
        return dialog.find(By.className("disabled-mail-message")).timed().isVisible();
    }

    public TimedQuery<Boolean> hasSearchPermalink()
    {
        if (!isOpen())
        {
            open();
        }
        return getPermalinkElement().timed().isVisible();
    }

    public TimedQuery<String> getSearchPermalinkValue()
    {
        if (!isOpen())
        {
            open();
        }
        return getPermalinkElement().timed().getAttribute("value");
    }

    public ShareDialog waitForSubmit(Tracer tracer)
    {
        traceContext.waitFor(tracer, "jira.plugins.share.send");
        return this;
    }

    public Map<String, String> getShareRequestData(Tracer tracer)
    {
        Map<String, String> shareRequestData;
        List rawData = traceContext.getArguments(tracer, "jira.plugins.share.send");
        if (rawData.size() > 0)
        {
            shareRequestData = Maps.transformValues((Map<String, Object>) rawData.get(0), new Function<Object, String>()
            {
                @Override
                public String apply(@Nullable final Object input)
                {
                    return input.toString();
                }
            });
        } else {
            shareRequestData = Maps.newHashMap();
        }
        return shareRequestData;
    }

    private PageElement getPermalinkElement()
    {
        return dialog.find(By.cssSelector(".issuenav-permalink .permalink"));
    }

    private void bindElements()
    {
        String selector = "div[id^='inline-dialog-share-entity-popup']";

        if (targetContext != null)
        {
            selector = "#inline-dialog-share-entity-popup-" + targetContext + "";
        }

        dialog = locator.find(By.cssSelector(selector), TimeoutType.DIALOG_LOAD);
        waitUntilTrue(dialog.timed().isVisible());
        if (!hasDisabledOutgoingMailMessage().now())
        {
            shareMultiSeclect = binder.bind(MultiSelect.class, "sharenames", new Function<String, By>()
            {
                @Override
                public By apply(@Nullable String itemName)
                {
                    //means find all items
                    if(itemName == null)
                    {
                        return By.cssSelector(".recipients li span img");
                    }
                    else
                    {
                        return By.cssSelector(".recipients li[title=\"" + itemName + "\"]");
                    }

                }
            });
        }
    }

    public String getNote()
    {
        return dialog.find(By.id("note")).getValue();
    }

    public ShareDialog closeBySelectingOutside()
    {
        body.click();
        waitUntilFalse(dialog.timed().isVisible());
        return this;
    }

    public ShareDialog closeByCancelButton()
    {
        dialog.find(By.className("close-dialog")).click();
        waitUntilFalse(dialog.timed().isVisible());
        return this;
    }

    public boolean isTriggerPresent()
    {
        return trigger.isPresent();
    }

    private List<String> getRecipients(String attributeName)
    {
        final List<String> ret = new ArrayList<String>();
        List<PageElement> elements = dialog.find(By.className("recipients")).findAll(By.tagName("li"));
        for (PageElement element : elements)
        {
            if (StringUtils.isNotBlank(element.getAttribute(attributeName)))
            {
                ret.add(element.getAttribute(attributeName));
            }
        }
        return ret;
    }

    /**
     * It has now become a possibility to have multiple share button on a single page
     * For example split view
     *
     * By default, the trigger context is empty, the first element matching to an id will be chosen
     * This is to ensure that the correct element is being picked instead
     * @param targetContext
     */
    public void refineShareTarget(String targetContext)
    {
        trigger = locator.find(By.cssSelector("#jira-share-trigger." + targetContext + "-share"));
        this.targetContext = targetContext;
    }

}
