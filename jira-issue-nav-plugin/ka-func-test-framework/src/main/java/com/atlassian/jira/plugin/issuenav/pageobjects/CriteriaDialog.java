package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.util.SearcherWaiter;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.google.inject.Inject;
import org.openqa.selenium.Keys;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.equalToIgnoringCase;

public class CriteriaDialog
{

    @Inject
    protected SearcherWaiter searcherWaiter;

    @Inject
    protected TraceContext traceContext;

    @ElementBy(cssSelector = ".ajs-layer.active")
    protected PageElement dialogLayer;
    
    @ElementBy(cssSelector = ".ajs-layer.active label:first")
    private PageElement dialogTitle;

    @ElementBy(cssSelector = ".ajs-layer.active input[type='submit']")
    private PageElement submitButton;

    @ElementBy(cssSelector = ".ajs-layer.active a.aui-button.aui-button-link")
    private PageElement cancelButton;

    @ElementBy(cssSelector = ".ajs-layer.active a.clear-all")
    private PageElement clearButton;

    @ElementBy(cssSelector = ".ajs-layer.active .error")
    private PageElement errors;

    @ElementBy (tagName = "body")
    private PageElement body;

    private String expectedDialogTitle;

    public CriteriaDialog(String expectedDialogTitle)
    {
        this.expectedDialogTitle = expectedDialogTitle;
    }

    @WaitUntil
    public void ready()
    {
        waitUntil(getDialogTitle().timed().getText(), equalToIgnoringCase(this.expectedDialogTitle));
    }

    public void submitAndWait()
    {
        Tracer tracer = traceContext.checkpoint();
        getSubmitButton().click();
        searcherWaiter.waitForSearcherUpdated(tracer);
    }

    public boolean isOpen()
    {
        return dialogLayer.isPresent() && dialogLayer.isVisible();
    }

    public PageElement getDialogTitle()
    {
        return dialogTitle;
    }

    public PageElement getSubmitButton()
    {
        return submitButton;
    }

    public void cancelWithClick()
    {
        Tracer tracer = traceContext.checkpoint();
        cancelButton.click();
        traceContext.waitFor(tracer, "jira.search.searchers.hiddenWithoutUpdate");
    }

    public void cancelWithEsc()
    {
        Tracer tracer = traceContext.checkpoint();
        body.type(Keys.ESCAPE);
        traceContext.waitFor(tracer, "jira.search.searchers.hiddenWithoutUpdate");
    }


    public void clickBackButton()
    {
        waitUntilTrue(cancelButton.timed().isPresent());
        cancelButton.click();
    }

    public String errorText()
    {
        waitUntilTrue(errors.timed().isVisible());
        return errors.getText();
    }

    public void clear()
    {
        Tracer tracer = traceContext.checkpoint();
        clearButton.click();
        searcherWaiter.waitForSearcherUpdated(tracer);
    }

    public boolean isAutoUpdate()
    {
        return !getSubmitButton().isPresent();
    }
}
