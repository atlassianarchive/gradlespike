package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.by;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static org.hamcrest.Matchers.is;

/**
 * Standalone view issue page
 */
public class IssueDetailPage extends AbstractJiraPage
{
    private final String issueKey;
    private final String jqlContext;
    private final Long filterId;

    @Inject
    private ProductInstance productInstance;

    @ElementBy(id = "content")
    private PageElement content;

    public IssueDetailPage(String issueKey)
    {
        this.issueKey = issueKey;
        this.jqlContext = null;
        this.filterId = null;
    }

    public IssueDetailPage(String issueKey, String jqlContext)
    {
        this.issueKey = issueKey;
        this.jqlContext = jqlContext;
        this.filterId = null;
    }

    public IssueDetailPage(String issueKey, String jqlContext, long filterId)
    {
        this.issueKey = issueKey;
        this.jqlContext = jqlContext;
        this.filterId = filterId;
    }

    public IssueDetailComponent details()
    {
        return pageBinder.bind(IssueDetailComponent.class, issueKey);
    }

    @Override
    @WaitUntil
    public void doWait()
    {
        // When using the /i root, there's a long time between requesting /browse/JRA-123 and actually having the issue
        // on-screen. Here I'm increasing the bind timeout from 5 to 20 seconds in an attempt to reduce test flakiness.
        waitUntil(isAt(), is(true), by(20000));
    }

    /**
     * @return the visible error message.
     */
    public IssueErrorComponent error()
    {
        return pageBinder.bind(IssueErrorComponent.class);
    }

    @Override
    public String getUrl()
    {
        String url = "/browse/" + issueKey;
        if (jqlContext != null)
        {
            url = url + "?jql=" + jqlContext;
            if (filterId != null)
            {
                url = url + "&filter=" + filterId;
            }
        } else if (filterId != null)
        {
            url = url + "?filter=" + filterId;
        }
        return url;
    }

    @Override
    public TimedCondition isAt()
    {
        // TODO: should find by data-id on issue-content
        return content.timed().isVisible();
    }

}
