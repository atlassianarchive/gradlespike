package com.atlassian.jira.plugin.issuenav.pageobjects.filter;

/**
 * The "Rename Filter" dialog that creates a filter from a search.
 *
 * @since v5.2
 */
public class RenameFilterDialog extends FilterDialog
{
    public RenameFilterDialog()
    {
        super("rename-filter-dialog");
    }
}