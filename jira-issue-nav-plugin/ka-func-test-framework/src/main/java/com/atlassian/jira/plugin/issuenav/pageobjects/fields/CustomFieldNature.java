package com.atlassian.jira.plugin.issuenav.pageobjects.fields;

/**
 * Reusable field implementation methods for CustomFields. Maybe this should made into a generic type?
 *
 * @since v5.1
 */
class CustomFieldNature
{
    private String id;

    public CustomFieldNature(long fieldId)
    {
        this.id = "customfield_" + fieldId;
    }

    public String getId()
    {
        return id;
    }

    public String getTriggerSelector()
    {
        return "#" + getId() + "-val";
    }
}
