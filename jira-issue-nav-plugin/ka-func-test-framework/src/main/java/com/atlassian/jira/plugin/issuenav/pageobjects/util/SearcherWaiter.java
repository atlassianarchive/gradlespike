package com.atlassian.jira.plugin.issuenav.pageobjects.util;

import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.google.inject.Inject;

/**
 * Waits for search components to be updated
 */
public class SearcherWaiter
{
    @Inject
    private TraceContext traceContext;

    public void waitForSearcherUpdated(Tracer tracer)
    {
        traceContext.waitFor(tracer, "jira.search.searchers.updated");
    }
}
