package com.atlassian.jira.plugin.issuenav.pageobjects.fields;

import com.atlassian.jira.pageobjects.util.Tracer;

/**
 * A field on the issue page that supports inline editing.
 *
 * @since v5.0
 */
public interface Field
{
    /**
     * Clear, then fill the field with one or more values.
     *
     * The field must already be in edit mode. The field is not saved.
     *
     * @param values The value(s) to fill the field with.
     * @return {@code this}
     */
    public Field fill(String... values);

    /**
     * @return The field's "id"; used to identify the inline edit form.
     */
    public String getId();

    /**
     * @return The field's current value.
     */
    public String getValue();

    /**
     * Start editing the field.
     *
     * @return {@code this}
     */
    public Field switchToEdit();

    /**
     * Saves the field
     * @return {@code this}
     */
    public Field save();

    /**
     * Waits for the inline field to save
     * @return {@code this}
     */
    public Field waitToSave(Tracer tracer);

    /**
     * Convenience method for calling switchToEdit, fill(values), save and waitToSave
     * @param values The value(s) to fill the field with.
     * @return {@code this}
     */
    public Field editSaveWait(String ...values);

    /**
     * Convenience method for calling fill(values), save and waitToSave
     * @param values The value(s) to fill the field with.
     * @return {@code this}
     */
    public Field fillSaveWait(String ...values);

    /**
     * Returns true if the field is visible on the page.
     */
    public boolean isVisible();
}
