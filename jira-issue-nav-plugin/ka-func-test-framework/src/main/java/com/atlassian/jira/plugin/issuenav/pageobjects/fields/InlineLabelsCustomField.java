package com.atlassian.jira.plugin.issuenav.pageobjects.fields;

import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

/**
 * An inline editable Labels Custom Field.
 *
 * @since v5.1
 */
public class InlineLabelsCustomField extends InlineLabelsField
{

    private final CustomFieldNature customFieldNature;

    public InlineLabelsCustomField(long fieldId)
    {
        customFieldNature = new CustomFieldNature(fieldId);
    }

    @Override
    protected PageElement getInput()
    {
        return finder.find(By.id(getId() + "-textarea"));
    }

    @Override
    public String getId()
    {
        return customFieldNature.getId();
    }

    @Override
    protected String getTriggerSelector()
    {
        return customFieldNature.getTriggerSelector();
    }}
