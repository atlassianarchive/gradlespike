package com.atlassian.jira.plugin.issuenav.pageobjects.fields;

import com.atlassian.pageobjects.elements.PageElement;

/**
 * A text custom field.
 *
 * @since v5.1
 */
public class InlineTextCustomField extends InlineField
{
    private final CustomFieldNature customFieldNature;

    public InlineTextCustomField(long fieldId)
    {
        customFieldNature = new CustomFieldNature(fieldId);
    }

    @Override
    public String getId()
    {
        return customFieldNature.getId();
    }

    @Override
    protected String getTriggerSelector()
    {
        return customFieldNature.getTriggerSelector();
    }
}
