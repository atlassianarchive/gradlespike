package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.List;

/**
 * Represents extended criteria.
 * @since v5.1
 */
public class ExtendedCriteriaComponent
{
    @Inject
    private PageElementFinder elementFinder;

    @Inject
    private PageBinder pageBinder;

    @Inject
    private SearcherFactory searcherFactory;

    private PageElement clauseList;

    public ExtendedCriteriaComponent(PageElement clauseList)
    {
        this.clauseList = clauseList;
    }

    public boolean clauseExists(String id)
    {
        return findClauseElement(id).isPresent();
    }

    public List<Clause> getClauses()
    {
        return Lists.transform(clauseList.findAll(By.cssSelector("li")), new Function<PageElement, Clause>()
        {
            @Override
            public Clause apply(@Nullable PageElement from)
            {
                return pageBinder.bind(Clause.class, from);
            }
        });
    }
    
    public Clause getClause(String id)
    {
        return pageBinder.bind(Clause.class, findClauseElement(id));
    }
    
    public MultiSelectClauseDialog selectReporter()
    {
        getClause("reporter").openDialog();
        return searcherFactory.reporter();
    }

    private PageElement findClauseElement(String id)
    {
        return clauseList.find(By.cssSelector("[data-id='" + id + "']"));
    }

    public static class Clause
    {
        @Inject
        private PageBinder pageBinder;

        private PageElement clauseElement;

        public Clause(PageElement element)
        {
            this.clauseElement = element;
        }

        public String getViewHtml()
        {
            return clauseElement.getText();
        }

        //This will return the view content of a search in it's glorious raw html form
        public String getRawViewHtml()
        {
            return clauseElement.find(By.className("fieldValue")).getAttribute("innerHTML");
        }

        public boolean isValidClause()
        {
            return !clauseElement.hasClass("warning");
        }

        public String getId()
        {
            return clauseElement.getAttribute("data-id");
        }

        public void remove()
        {
            clauseElement.find(By.cssSelector(".remove-filter")).click();
        }
        
        public void openDialog()
        {
            valueElement().click();
        }
        
        private PageElement valueElement()
        {
            return clauseElement.find(By.cssSelector(".searcherValue"));
        }
    }
}
