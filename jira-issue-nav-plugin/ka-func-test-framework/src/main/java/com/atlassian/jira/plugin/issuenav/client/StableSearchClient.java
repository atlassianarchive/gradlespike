package com.atlassian.jira.plugin.issuenav.client;

import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.util.URLCodec;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * REST client for StableSearchResource
 *
 * @since v6.0
 */
public class StableSearchClient extends RestApiClient<IssueTableClient>
{
    private static final Logger log = Logger.getLogger(StableSearchClient.class);

    private final JIRAEnvironmentData environmentData;

    public StableSearchClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
        this.environmentData = environmentData;
    }

    public StableSearchIdListResponse getUnusedIds(String jql, List<Long> currentIds) throws UnsupportedEncodingException
    {
        return createResource().type(MediaType.APPLICATION_FORM_URLENCODED).post(StableSearchIdListResponse.class, buildRequestBody(jql, currentIds));
    }

    private String buildRequestBody(String jql, List<Long> ids) throws UnsupportedEncodingException
    {
        StringBuilder sb = new StringBuilder("jql=").append(URLCodec.encode(jql, "UTF-8")).append("&id=");

        sb.append(StringUtils.join(ids, "&id="));

        return sb.toString();
    }

    protected WebResource createResource()
    {
        return resourceRoot(environmentData.getBaseUrl().toExternalForm()).path("rest/issueNav/latest/stableSearch");
    }

    @XmlRootElement
    public static class StableSearchIdListResponse
    {
        @XmlElement
        private ArrayList<Long> stableSearchIds;

        public StableSearchIdListResponse()
        {
        }

        public StableSearchIdListResponse(final ArrayList<Long> ids)
        {
            this.stableSearchIds = ids;
        }

        public ArrayList<Long> getStableSearchIds()
        {
            return stableSearchIds;
        }

        public void setStableSearchIds(ArrayList<Long> stableSearchIds)
        {
            this.stableSearchIds = stableSearchIds;
        }
    }


}
