package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.components.DropDown;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 * @since v5.2
 */
public class ViewsDropDown extends DropDown
{
    // HACK: see isOpen
    @Inject private PageElementFinder elementFinder;
    @Inject private TraceContext traceContext;

    private By dropdownLocator;

    public ViewsDropDown(final By triggerLocator, final By dropdownLocator)
    {
        super(triggerLocator, dropdownLocator);
        this.dropdownLocator = dropdownLocator;
    }

    public List<String> getSectionLabels()
    {
        List<String> sectionLabels = new ArrayList<String>();
        List<PageElement> items = elementFinder.find(this.dropdownLocator).findAll(By.tagName("h5"));
        for (PageElement item : items)
        {
            sectionLabels.add(item.getText().trim());
        }
        return sectionLabels;
    }

    public List<String> getItemLabels()
    {
        List<String> itemLabels = new ArrayList<String>();
        List<PageElement> items = elementFinder.find(this.dropdownLocator).findAll(By.className("aui-list-item-link"));
        for (PageElement item : items)
        {
            itemLabels.add(item.getText().trim());
        }
        return itemLabels;
    }

    @Override
    public boolean isOpen()
    {
        // TODO: fix
        // HACK: superclass fails if dropdown is not visible. override; should fix in master
        return elementFinder.find(this.dropdownLocator).isPresent() && super.isOpen();
    }
}
