package com.atlassian.jira.plugin.issuenav.pageobjects.filter;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import org.openqa.selenium.By;

import java.util.List;
import javax.inject.Inject;

public class FilterDetailsMenu
{
    @Inject
    private PageBinder pageBinder;

    @Inject
    private PageElementFinder elementFinder;

    @ElementBy (className = "edit-permissions")
    private PageElement editPermissionsLink;

    @ElementBy (className = "shared-filters")
    private PageElement sharedFilters;

    private PageElement trigger;
    private PageElement dropDown;

    public FilterDetailsMenu()
    {
    }

    @Init
    public void init()
    {
        trigger = elementFinder.find(By.className("show-filter-details"));
        dropDown = elementFinder.find(By.id("inline-dialog-filter-details-overlay"));
    }

    public EditFilterPage editPermissions()
    {
        trigger.click();
        Poller.waitUntilTrue(dropDown.timed().isVisible());
        editPermissionsLink.click();
        return pageBinder.bind(EditFilterPage.class);
    }

    public Iterable<String> getPermissions()
    {
        trigger.click();
        List<PageElement> permissions = elementFinder.find(By.className("shared-filters")).findAll(By.tagName("li"));
        return Iterables.transform(permissions, new Function<PageElement, String>()
            {
                @Override
                public String apply(final PageElement element)
                {
                    return element.getText();
                }
        });
    }

}
