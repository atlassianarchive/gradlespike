package com.atlassian.jira.plugin.issuenav.pageobjects.fields;

import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.webdriver.AtlassianWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * A field on the issue page that supports inline editing.
 */
public abstract class InlineField implements Field {
    @Inject
    private AtlassianWebDriver driver;

    @Inject
    protected PageBinder binder;

    @Inject
    protected TraceContext traceContext;

    @Inject
    protected PageElementFinder finder;
    protected PageElement trigger;

    /**
     * @return The field's input element.
     */
    protected PageElement getInput()
    {
        return finder.find(By.id(getId()));
    }

    /**
     * @return The element that, when clicked, moves the field into edit mode.
     */
    protected PageElement getTrigger()
    {
        PageElement trigger = finder.find(By.cssSelector(this.getTriggerSelector()));
        waitUntilTrue(trigger.timed().isVisible());
        return trigger;
    }

    /**
     * @return A CSS selector that identifies the field's trigger.
     */
    abstract protected String getTriggerSelector();

    @Override
    public InlineField save()
    {
        final PageElement form = finder.find(By.id(getId() + "-form"));
        form.find(By.className("aui-iconfont-success")).click();
        return this;
    }

    @Override
    public InlineField waitToSave(Tracer tracer)
    {
        traceContext.waitFor(tracer, "jira.issue.refreshed");
        return this;
    }

    @Override
    public Field fill(final String... values)
    {
        getInput().clear().type(values[0]);
        return this;
    }

    @Override
    public String getValue()
    {
        // If we grab the trigger's text while in edit mode, it will include
        // text from the save/cancel buttons. Wait until the save is complete.
        waitUntilFalse(getInput().timed().isPresent());
        return getTrigger().getText();
    }

    @Override
    public Field switchToEdit()
    {
        waitUntilTrue(getTrigger().timed().hasClass("editable-field"));
        PageElement trigger = getTrigger();
        //hack to switch to edit mode in chrome
        WebElement editButton = driver.findElement(By.cssSelector(getTriggerSelector() + " span.overlay-icon"));
        new Actions(driver).moveToElement(driver.findElement(By.cssSelector(getTriggerSelector()))).perform();
//        final Tracer tracer = traceContext.checkpoint();
        try {
            editButton.click();
        } catch (ElementNotVisibleException enve) {
            trigger.click();
        }
        // need to wait until all the event handlers have run
//        traceContext.waitFor(tracer, "jira.field.edit.complete");
        return this;
    }

    public TimedCondition isEditable()
    {
        return getTrigger().timed().hasClass("editable-field");
    }

    @Override
    public Field editSaveWait(String... values)
    {
        switchToEdit();
        return fillSaveWait(values);
    }

    @Override
    public Field fillSaveWait(String... values)
    {
        fill(values);
        Tracer tracer = traceContext.checkpoint();
        return save().waitToSave(tracer);
    }

    @Override
    public boolean isVisible()
    {
        return getInput().timed().isVisible().byDefaultTimeout();
    }
}
