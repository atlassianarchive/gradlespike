package com.atlassian.jira.plugin.issuenav.pageobjects.fields;

import com.atlassian.jira.pageobjects.components.userpicker.MentionsUserPicker;
import com.atlassian.pageobjects.elements.PageElement;

/**
 * The issue's description field.
 *
 * @since v6.0
 */
public class InlineDescriptionField extends InlineField
{
    @Override
    protected String getTriggerSelector()
    {
        return "#description-val";
    }

    @Override
    public String getId()
    {
        return "description";
    }

    public InlineDescriptionField type(CharSequence... text)
    {
        getInput().type(text);
        return this;
    }

    public MentionsUserPicker mentions()
    {
        return binder.bind(MentionsUserPicker.class, getInput());
    }

}
