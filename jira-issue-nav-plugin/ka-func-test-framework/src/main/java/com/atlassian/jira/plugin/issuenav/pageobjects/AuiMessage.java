package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

/**
 *
 */
public class AuiMessage extends AbstractJiraPage
{
    private final String uriPattern;

    @ElementBy(className = "aui-message")
    private PageElement auiMessage;

    public AuiMessage()
    {
        this.uriPattern = null;
    }

    public AuiMessage(String uriPattern)
    {
        this.uriPattern = uriPattern;
    }

    public String getText()
    {
        return auiMessage.getText();
    }

    public boolean isError()
    {
        return auiMessage.hasClass("error");
    }

    @Override
    public TimedCondition isAt()
    {
        return auiMessage.timed().isVisible();
    }

    @Override
    public String getUrl()
    {
        return uriPattern;
    }
}
