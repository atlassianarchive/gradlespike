package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.dialogs.FormDialog;
import org.openqa.selenium.By;

/**
 * The keyboard shortcuts dialog, from which keyboard shortcuts can be disabled.
 *
 * @since v5.2
 */
public class KeyboardShortcutsDialog extends FormDialog
{
    public KeyboardShortcutsDialog()
    {
        super("keyboard-shortcuts-dialog");
    }

    /**
     * Click the "Disable/Enable Keyboard Shortcuts" link.
     * <p/>
     * This causes a page reload.
     */
    public void toggleKeyboardShortcuts()
    {
        submit(By.cssSelector(".submit-link"));
    }
}