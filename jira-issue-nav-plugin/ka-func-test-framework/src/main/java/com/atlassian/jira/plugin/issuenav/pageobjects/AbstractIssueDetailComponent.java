package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.dialogs.IssueActionsUtil;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.webdriver.AtlassianWebDriver;
import org.openqa.selenium.By;

import javax.inject.Inject;

public abstract class AbstractIssueDetailComponent {
    @Inject protected AtlassianWebDriver driver;
    @Inject protected PageElementFinder elementFinder;
    @Inject protected IssueActionsUtil issueActionsUtil;
    @Inject protected PageBinder pageBinder;
    @Inject protected TraceContext traceContext;

    @ElementBy(cssSelector = ".ops.page-navigation")
    protected PageElement nextPreviousIssuePager;

    @ElementBy(cssSelector = ".ops.page-navigation #return-to-search")
    protected PageElement returnToSearchIssuePager;

    /**
     * @return whether the next/previous issue pager is visible.
     *
     * TODO We should eventually phase out these non-timed version of the functions
     */
    public boolean hasNextPreviousPager()
    {
        return hasNextPreviousPagerTimed().now();
    }

    public TimedCondition hasNextPreviousPagerTimed()
    {
        return nextPreviousIssuePager.timed().isVisible();
    }

    public TimedCondition hasReturnToSearchPagerTimed()
    {
        return returnToSearchIssuePager.timed().isVisible();
    }


    public PageElement getNextIssueLink()
    {
        return nextPreviousIssuePager.find(By.id("next-issue"));
    }

    public String getNextIssueKey()
    {
        PageElement nextIssueLink = getNextIssueLink();
        return nextIssueLink.getAttribute("rel");
    }

    /**
     * @return whether the "Next Issue" link is present.
     *
     * TODO We should eventually phase out these non-timed version of the functions
     */
    public boolean hasNextIssue()
    {
        return hasNextIssueTimed().now();
    }

    public TimedCondition hasNextIssueTimed()
    {
        return getNextIssueLink().timed().isPresent();
    }

    /**
     * Navigate to the next issue.
     *
     * @return the issue page.
     */
    public IssueDetailComponent nextIssue()
    {
        return nextIssue(ReadyForAction.READY).ready();
    }

    public <T> T nextIssue(Class<T> nextPage)
    {
        getNextIssueLink().click();
        return pageBinder.bind(nextPage);
    }

    /**
     * Navigate to the next issue and return a future IssueDetailComponent. Depending on the {@code readyForAction}
     * parameter, the
     *
     * @param readyForAction
     * @return the issue page.
     */
    public FutureIssueDetailComponent nextIssue(ReadyForAction readyForAction)
    {
        final FutureIssueDetailComponent issue = new FutureIssueDetailComponent(elementFinder, pageBinder, traceContext);
        getNextIssueLink().click();

        // optionally waits for psycho ready here
        return issue.readyFor(readyForAction);
    }

    public PageElement getPreviousIssueLink()
    {
        return nextPreviousIssuePager.find(By.id("previous-issue"));
    }

    /**
     * @return whether the "Previous Issue" link is present.
     *
     * TODO We should eventually phase out these non-timed version of the functions
     */
    public boolean hasPreviousIssue()
    {
        return hasPreviousIssueTimed().now();
    }

    public TimedCondition hasPreviousIssueTimed()
    {
        return getPreviousIssueLink().timed().isPresent();
    }

    /**
     * Navigate to the previous issue.
     *
     * @return the issue page.
     */
    public IssueDetailComponent previousIssue()
    {
        return previousIssue(ReadyForAction.READY).ready();
    }

    /**
     * Navigate to the previous issue.
     *
     * @return the issue page.
     * @param readyForAction
     */
    public FutureIssueDetailComponent previousIssue(ReadyForAction readyForAction)
    {
        final FutureIssueDetailComponent issue = new FutureIssueDetailComponent(elementFinder, pageBinder, traceContext);
        getPreviousIssueLink().click();

        // optionally waits for psycho ready here
        return issue.readyFor(readyForAction);
    }
}
