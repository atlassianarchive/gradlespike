package com.atlassian.jira.plugin.issuenav.pageobjects;

/**
* Indicates whether to wait for a full "ready" or a psycho-"ready".
*/
public enum ReadyForAction
{
    READY,
    PSYCHO_READY,
    REFRESHED_CACHED_READY,
    NONE
}
