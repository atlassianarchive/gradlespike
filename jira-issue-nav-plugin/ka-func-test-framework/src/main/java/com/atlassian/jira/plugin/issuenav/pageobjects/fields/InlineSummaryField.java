package com.atlassian.jira.plugin.issuenav.pageobjects.fields;

import com.atlassian.pageobjects.elements.PageElement;

/**
 * The issue's summary field.
 */
public class InlineSummaryField extends InlineField
{
    @Override
    protected String getTriggerSelector()
    {
        return "#summary-val";
    }

    @Override
    public String getId()
    {
        return "summary";
    }

    // Make this method public to support an existing test.
    @Override
    public PageElement getInput()
    {
        return super.getInput();
    }

    /**
     * increase the maximum length allowed
     */
    public InlineSummaryField setMaxLength(int maxLength)
    {
        getInput().javascript().execute("jQuery(arguments[0]).attr(\"maxlength\", \"" + maxLength + "\")");
        return this;
    }
}
