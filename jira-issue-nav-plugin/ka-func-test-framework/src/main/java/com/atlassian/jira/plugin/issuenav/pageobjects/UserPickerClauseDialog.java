package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.Option;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.SelectElement;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public class UserPickerClauseDialog extends CriteriaDialog
{

    @Inject
    private PageElementFinder elementFinder;

    @Inject
    private PageBinder pageBinder;

    private final String selectElementId;

    private SelectElement selectElement;
    
    private final String inputFieldId;

    private PageElement inputField;

    public UserPickerClauseDialog(String expectedDialogTitle, String selectElementId, String inputFieldId)
    {
        super(expectedDialogTitle);
        this.selectElementId = selectElementId;
        this.inputFieldId = inputFieldId;
    }
    
    @WaitUntil
    public void ready()
    {
        selectElement = elementFinder.find(By.id(this.selectElementId), SelectElement.class);
        waitUntilTrue(selectElement.timed().isVisible());

        inputField = elementFinder.find(By.id(this.inputFieldId), PageElement.class);
        waitUntilTrue(inputField.timed().isVisible());
    }

    private void selectAnyUser()
    {
        selectOptionByValue("");
    }

    private void selectUnassigned()
    {
        selectOptionByValue("unassigned");
    }

    private void selectCurrentUser()
    {
        selectOptionByValue("issue_current_user");
    }

    private void selectSpecificUser()
    {
        selectOptionByValue("specificuser");
    }

    private void selectSpecificGroup()
    {
        selectOptionByValue("specificgroup");
    }

    private void selectOptionByValue(String value)
    {
        final Option option = Options.value(value);
        selectElement.select(option);
    }
    
    public void selectUserAndWait(String userName)
    {
        selectSpecificUser();
        inputField.clear();
        inputField.type(userName);
        submitAndWait();
    }

    public void selectGroupAndWait(String groupName)
    {
        selectSpecificGroup();
        inputField.clear();
        inputField.type(groupName);
        submitAndWait();
    }
}
