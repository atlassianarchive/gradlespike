package com.atlassian.jira.plugin.issuenav.client;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Map;

/**
 * A serialisable issue table object
 * <p/>
 * Copied from {@code jira-issue-nav-plugin} for testing purposes.
 *
 * @since v6.0
 */
@XmlRootElement
public class IssueTable
{
    public static class Builder
    {
        private Map<String, String> columnSortJql;
        private String description;
        private int displayed;
        private int end;
        private List<Long> issueIds;
        private List<String> issueKeys;
        private boolean jiraHasIssues;
        private Map<String, String> sortBy;
        private List<String> columns;
        private int page;
        private int pageSize;
        private int startIndex;
        private final Object table;
        private String title;
        private int total;
        private String url;
        private String columnConfig;

        public Builder(Object table)
        {
            this.table = table;
        }

        public IssueTable build()
        {
            return new IssueTable(this);
        }

        public Builder columnSortJql(Map<String, String> columnSortJql)
        {
            this.columnSortJql = columnSortJql;
            return this;
        }

        public Builder sortBy(final Map<String, String> sortBy)
        {
            this.sortBy = sortBy;
            return this;
        }


        public Builder description(String description)
        {
            this.description = description;
            return this;
        }

        public Builder displayed(int displayed)
        {
            this.displayed = displayed;
            return this;
        }

        public Builder end(int end)
        {
            this.end = end;
            return this;
        }

        public Builder issueIds(List<Long> issueIds)
        {
            this.issueIds = issueIds;
            return this;
        }

        public Builder issueKeys(List<String> issueKeys)
        {
            this.issueKeys = issueKeys;
            return this;
        }

        public Builder jiraHasIssues(boolean jiraHasIssues)
        {
            this.jiraHasIssues = jiraHasIssues;
            return this;
        }

        public Builder page(int page)
        {
            this.page = page;
            return this;
        }

        public Builder pageSize(int pageSize)
        {
            this.pageSize = pageSize;
            return this;
        }

        public Builder startIndex(int startIndex)
        {
            this.startIndex = startIndex;
            return this;
        }

        public Builder title(String title)
        {
            this.title = title;
            return this;
        }

        public Builder total(int total)
        {
            this.total = total;
            return this;
        }

        public Builder url(String url)
        {
            this.url = url;
            return this;
        }

        public Builder columns(List<String> columns)
        {
            this.columns = columns;
            return this;
        }

        public Builder columnConfig(String columnConfig)
        {
            this.columnConfig = columnConfig;
            return this;
        }
    }

    @XmlElement
    private Map<String, String> columnSortJql;
    @XmlElement private String description;
    @XmlElement private int displayed;
    @XmlElement private int end;
    @XmlElement private List<Long> issueIds;
    @XmlElement private List<String> issueKeys;
    @XmlElement private boolean jiraHasIssues;
    @XmlElement private int page;
    @XmlElement private int pageSize;
    @XmlElement private int startIndex;
    @XmlElement private Object table;
    @XmlElement private String title;
    @XmlElement private int total;
    @XmlElement private List<String> columns;
    @XmlElement private String url;
    @XmlElement private Map<String, String> sortBy;
    @XmlElement private String columnConfig;

    @SuppressWarnings ({ "UnusedDeclaration", "unused" })
    private IssueTable()
    {
    }

    private IssueTable(Builder builder)
    {
        columnSortJql = builder.columnSortJql;
        description = builder.description;
        displayed = builder.displayed;
        end = builder.end;
        issueIds = builder.issueIds;
        issueKeys = builder.issueKeys;
        jiraHasIssues = builder.jiraHasIssues;
        page = builder.page;
        pageSize = builder.pageSize;
        startIndex = builder.startIndex;
        table = builder.table;
        title = builder.title;
        total = builder.total;
        sortBy = builder.sortBy;
        url = builder.url;
        columns = builder.columns;
        columnConfig = builder.columnConfig;
    }

    public List<String> getColumns()
    {
        return columns;
    }

    public int getEnd()
    {
        return end;
    }

    public Map<String, String> sortBy()
    {
        return sortBy;
    }

    public List<Long> getIssueIds()
    {
        return issueIds;
    }

    public List<String> getIssueKeys()
    {
        return issueKeys;
    }

    public boolean getJiraHasIssues()
    {
        return jiraHasIssues;
    }

    public int getStartIndex()
    {
        return startIndex;
    }

    public Object getTable()
    {
        return table;
    }

    public int getTotal()
    {
        return total;
    }

    public String getColumnConfig()
    {
        return columnConfig;
    }
}