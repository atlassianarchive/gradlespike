package com.atlassian.jira.plugin.issuenav.pageobjects.fields;

import org.openqa.selenium.By;

/**
 * The issue's reporter field.
 */
public class InlineReporterField extends InlineField
{
    /**
     * @return The URL of the reporter's avatar image.
     */
    public String getAvatarURL()
    {
        return this.getTrigger().find(By.cssSelector(".aui-avatar img")).getAttribute("src");
    }

    @Override
    protected String getTriggerSelector()
    {
        return "#reporter-val";
    }

    @Override
    public String getId()
    {
        return "reporter";
    }
}