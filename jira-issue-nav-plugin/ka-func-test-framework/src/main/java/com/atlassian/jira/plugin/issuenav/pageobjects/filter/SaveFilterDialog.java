package com.atlassian.jira.plugin.issuenav.pageobjects.filter;

/**
 * The "Save Filter" dialog that creates a filter from a search.
 *
 * @since v5.2
 */
public class SaveFilterDialog extends FilterDialog
{
    public SaveFilterDialog()
    {
        super("save-filter-dialog");
    }
}