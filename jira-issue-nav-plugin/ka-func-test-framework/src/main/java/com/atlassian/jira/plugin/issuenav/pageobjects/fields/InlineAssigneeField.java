package com.atlassian.jira.plugin.issuenav.pageobjects.fields;

import org.openqa.selenium.By;

/**
 * The issue's assignee field.
 *
 * @since v5.0
 */
public class InlineAssigneeField extends InlineSingleSelectField
{
    /**
     * @return The URL of the assignee's avatar image.
     */
    public String getAvatarURL()
    {
        return this.getTrigger().find(By.cssSelector(".aui-avatar img")).getAttribute("src");
    }

    @Override
    protected String getTriggerSelector()
    {
        return "#assignee-val";
    }

    @Override
    public String getId()
    {
        return "assignee";
    }
}