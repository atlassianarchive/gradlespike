package com.atlassian.jira.plugin.issuenav.pageobjects.util.login;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Response sent by JIRA when logging in via REST.
 */
@JsonSerialize
@JsonIgnoreProperties(ignoreUnknown = true)
class LoginResponse
{
    public SetCookie session;

    @JsonSerialize
    @JsonIgnoreProperties(ignoreUnknown = true)
    static class SetCookie
    {
        public String name;
        public String value;
    }
}
