package com.atlassian.jira.plugin.issuenav.pageobjects.splitview;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedQuery;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Page object for the "Order by" panel in split view. This panel contains the order by text (e.g. "Order by Assignee")
 * and also an associated drop-down menu that people can use to change the sort columns.
 *
 * @since v2.0.21
 */
public class OrderByPanel
{
    @Inject
    PageBinder pageBinder;

    @ElementBy (cssSelector = ".list-ordering .order-by")
    PageElement orderByText;

    @ElementBy (cssSelector = ".list-ordering .order-options")
    PageElement dropDownTriangle;

    @ElementBy(tagName = "body")
    protected PageElement body;

    @WaitUntil
    public void ready()
    {
        waitUntilTrue(Conditions.and(
                orderByText.timed().isPresent(),
                dropDownTriangle.timed().isPresent()
        ));
    }

    /**
     * @return the order by text, e.g. "Order by Key"
     */
    public TimedQuery<String> getOrderByText()
    {
        return orderByText.timed().getText();
    }

    /**
     * @return the order by field if, or null if there is no order by clause
     */
    public TimedQuery<String> getOrderByFieldId()
    {
        return orderByText.timed().getAttribute("data-field-id");
    }

    /**
     * @return the order by direction (ASC/DESC), or null if there is no order by clause
     */
    public TimedQuery<String> getOrderByDirection()
    {
        return orderByText.timed().getAttribute("data-direction");
    }

    /**
     * Clicks the order by text (e.g. "Order by Assignee"). This has the effect of inverting the sort order if there is
     * any.
     *
     * @return this
     */
    public OrderByPanel clickOrderByText()
    {
        orderByText.click();
        return this;
    }

    /**
     * Clicks the order by drop-down triangle and waits for the drop-down to be shown.
     *
     * @return the OrderByDropDown page object
     */
    public OrderByDropDown openDropDown()
    {
        dropDownTriangle.click();
        return pageBinder.bind(OrderByDropDown.class);
    }

    public OrderByDropDown openViaKeyboardShortcut()
    {
        body.type("z");
        return pageBinder.bind(OrderByDropDown.class);
    }
}
