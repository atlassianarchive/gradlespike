package com.atlassian.jira.plugin.issuenav.pageobjects.splitview;

import com.atlassian.jira.plugin.issuenav.pageobjects.MultiSelectClauseDialog;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.Option;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Queries;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.google.common.base.Supplier;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.jira.plugin.issuenav.pageobjects.MultiSelectClauseDialog.ClickTarget;
import static java.util.Collections.singletonList;

/**
 * Page object for the order by drop-down menu.
 *
 * @since v2.0.21
 */
public class OrderByDropDown
{
    @Inject
    private PageBinder pageBinder;

    @Inject
    private Timeouts timeouts;

    @Inject
    private PageElementFinder elementFinder;

    // lazily-instantiated sparkler
    MultiSelectClauseDialog sparkler;

    /**
     * @return a TimedCondition indicating whether the drop-down is open and ready for interaction.
     */
    public TimedCondition isOpen()
    {
        return Conditions.forSupplier(new Supplier<Boolean>()
        {
            @Override
            public Boolean get()
            {
                return sparkler().isOpen();
            }
        });
    }

    /**
     * @return a TimedQuery containing a list of options available in the drop-down
     */
    public TimedQuery<List<Option>> getOptions()
    {
        return Queries.forSupplier(timeouts, new Supplier<List<Option>>()
        {
            @Override
            public List<Option> get()
            {
                return sparkler().getAllOptions();
            }
        });
    }

    /**
     * Selects an option by its text (i.e. its i18n'ed name).
     *
     * @param optionText the field name (i18n'ed)
     * @return this
     */
    public OrderByDropDown selectOption(String optionText)
    {
        sparkler().selectValues(singletonList(optionText), ClickTarget.LABEL);
        return this;
    }

    /**
     * Types the search text into the drop-down and waits for the sparkler to update.
     *
     * @param text a string to search for
     * @return this
     */
    public OrderByDropDown typeSearchText(String text)
    {
        sparkler().typeSearchText(text).waitForSearch(text);
        return this;
    }

    /**
     * Returns the dropdown footer text.
     *
     * @return the dropdown footer text
     */
    public TimedQuery<String> getFooterText()
    {
        return elementFinder.find(By.cssSelector(".order-dropdown .aui-list-section-footer")).timed().getText();
    }

    private MultiSelectClauseDialog sparkler()
    {
        if (sparkler == null)
        {
            sparkler = pageBinder.bind(MultiSelectClauseDialog.class, "", "order-by-options", "");
        }

        return sparkler;
    }
}
