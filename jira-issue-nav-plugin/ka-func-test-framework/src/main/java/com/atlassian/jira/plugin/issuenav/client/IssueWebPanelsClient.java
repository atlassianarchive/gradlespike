package com.atlassian.jira.plugin.issuenav.client;

import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class IssueWebPanelsClient extends RestApiClient<IssueWebPanelsClient>
{
    private final JIRAEnvironmentData environmentData;

    public IssueWebPanelsClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
        this.environmentData = environmentData;
    }

    public IssueWebPanels getPanelsForIssue(final String issueId)
    {
        return issueWebPanels(issueId).get(IssueWebPanels.class);
    }

    public Response getResponseForIssue(final String issueId)
    {
        return toResponse(new Method()
        {
            @Override
            public ClientResponse call()
            {
                return issueWebPanels(issueId).get(ClientResponse.class);
            }
        }, IssueWebPanels.class);
    }

    private WebResource issueWebPanels(String issueId)
    {
        return createResource().queryParam("id", issueId).queryParam("decorator", "none");
    }

    protected WebResource createResource()
    {
        return resourceRoot(environmentData.getBaseUrl().toExternalForm()).path("secure").
                path("ViewIssueWebPanels!default.jspa");
    }
}