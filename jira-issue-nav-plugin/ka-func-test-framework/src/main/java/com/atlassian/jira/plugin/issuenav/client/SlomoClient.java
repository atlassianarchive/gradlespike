package com.atlassian.jira.plugin.issuenav.client;

import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.sun.jersey.api.client.WebResource;
import org.apache.log4j.Logger;

import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * @since v5.2
 */
public class SlomoClient extends RestApiClient<IssueNavToolsClient>
{
    private static final Logger log = Logger.getLogger(SlomoClient.class);

    private final JIRAEnvironmentData environmentData;

    public SlomoClient(final JIRAEnvironmentData environmentData)
    {
        super(environmentData);
        this.environmentData = environmentData;
    }

    public SlomoRunner runner()
    {
        return new SlomoRunner();
    }

    public SlomoPattern setDelay(final String pattern, final long delay)
    {
        WebResource path = resourceRoot(environmentData.getBaseUrl().toExternalForm())
                .path("rest/func-test/latest/slomo/pattern");

        return path.type(MediaType.APPLICATION_JSON_TYPE).put(SlomoPattern.class, new SlomoPattern(pattern, delay));
    }

    public void removeDelay(final int id)
    {
        WebResource path = resourceRoot(environmentData.getBaseUrl().toExternalForm())
                .path("rest/func-test/latest/slomo/pattern/" + id);

        path.type(MediaType.APPLICATION_JSON_TYPE).delete();
    }

    /**
     * Runs a closure with Slomo delays enabled.
     */
    public class SlomoRunner
    {
        /**
         * The delays to apply when {@link #run(java.util.concurrent.Callable)} is called.
         */
        private final ImmutableSet<SlomoPattern> delays;

        /**
         * Creates a new SlomoRunner with no delays configured.
         */
        public SlomoRunner()
        {
            this(ImmutableSet.<SlomoPattern>of());
        }

        /**
         * Creates a new SlomoRunner with the given delays configured.
         *
         * @param delays    a set of of SlomoPattern (in MS)
         */
        public SlomoRunner(ImmutableSet<SlomoPattern> delays)
        {
            this.delays = delays;
        }

        /**
         * Adds a delay to this SlomoRunner.
         *
         * @param pattern a String containing a regex pattern
         * @param delay the delay in milliseconds
         * @return a new SlomoRunner
         */
        public SlomoRunner setDelay(String pattern, long delay)
        {
            ImmutableSet.Builder<SlomoPattern> delays = ImmutableSet.builder();
            delays.addAll(this.delays);
            delays.add(new SlomoPattern(pattern, delay));

            return new SlomoRunner(delays.build());
        }

        /**
         * Adds a delay to this SlomoRunner.
         *
         * @param pattern a String containing a regex pattern
         * @param delay the delay in the given <code>TimeUnit</code>
         * @param unit the unit of time that {@code delay} is in
         * @return a new SlomoRunner
         */
        public SlomoRunner setDelay(String pattern, long delay, TimeUnit unit)
        {
            return setDelay(pattern, unit.toMillis(delay));
        }

        /**
         * Sets Slomo delays and then runs the given {@code callable}, making sure to remove the delays afterwards.
         *
         * @param callable    a Callable to run
         * @param <T> the Callable return type
         * @return the value returned from {@code callable}
         * @throws Exception
         */
        public <T> T run(Callable<T> callable) throws Exception
        {
            final List<Integer> delayIds = Lists.newArrayList();
            try
            {
                // enable the delays and run the closure
                addDelays();
                return callable.call();
            }
            finally
            {
                // disable any delays that have been set
                removeDelays(delayIds);
            }
        }

        private void addDelays()
        {
            for (SlomoPattern delay : delays)
            {
                setDelay(delay.pattern, delay.delay);
            }
        }

        private void removeDelays(List<Integer> delayIds)
        {
            for (Integer delayId : delayIds)
            {
                try
                {
                    removeDelay(delayId);
                }
                catch (Exception e)
                {
                    log.error("Error removing delay: " + delayId);
                }
            }
        }
    }
}
