package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.components.CalendarPicker;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

import javax.inject.Inject;

public class DateSearcher extends CriteriaDialog
{
    public static enum DATE_TYPE {
        moreThan,
        dueInNext,
        withinTheLast,
    }

    @ElementBy(cssSelector = ".field-group[data-date-type='moreThan']")
    private PageElement moreThanFieldGroup;

    @ElementBy(cssSelector = ".field-group[data-date-type='dueInNext']")
    private PageElement dueInNextFieldGroup;

    @ElementBy(cssSelector = ".field-group[data-date-type='withinTheLast']")
    private PageElement withinTheLastFieldGroup;

    @ElementBy(cssSelector = ".field-group[data-date-type='dateBetween']")
    private PageElement dateBetweenFieldGroup;

    @ElementBy(cssSelector = ".field-group[data-date-type='inRange']")
    private PageElement inRangeFieldGroup;

    @Inject
    private PageBinder pageBinder;

    public DateSearcher()
    {
        super(" Now overdue");
    }

    @WaitUntil
    public void ready()
    {
        dialogLayer.find(By.className("js-picker-ui")).timed().isPresent();
    }

    public String getError()
    {
        PageElement error = dialogLayer.find(By.cssSelector(".error"));
        if (error.isPresent())
        {
            return error.getText();
        }
        else
        {
            return null;
        }
    }

    public void setMoreThanMinutes(String minutes) {
        moreThanFieldGroup.find(By.className("js-val")).type(minutes);
        submitAndWait();
    }

    public void setDueInNextMinutes(String minutes)
    {
        dueInNextFieldGroup.find(By.className("js-val")).type(minutes);
        submitAndWait();
    }

    public void setWithinTheLast(String minutes)
    {
        withinTheLastFieldGroup.find(By.className("js-val")).type(minutes);
        submitAndWait();
    }

    public void setDateBetween(String startDate, String endDate)
    {
        CalendarPicker startDateCalendarPicker = pageBinder.bind(CalendarPicker.class, By.className("js-start-date"),
                By.className("js-start-date-trigger"));
        CalendarPicker endDateCalendarPicker = pageBinder.bind(CalendarPicker.class, By.className("js-end-date"),
                By.className("js-end-date-trigger"));
        startDateCalendarPicker.setDate(startDate);
        endDateCalendarPicker.setDate(endDate);
        submitAndWait();
    }

    public void setInRange(String startRange, String endRange)
    {
        inRangeFieldGroup.find(By.className("js-start-range")).clear().type(startRange);
        inRangeFieldGroup.find(By.className("js-end-range")).clear().type(endRange);
        submitAndWait();
    }

    public DATE_TYPE getSelectedDateType() {

        final String isCheckedFieldGroup = "return arguments[0].querySelector('.js-dp-type-toggle').checked";

        if (moreThanFieldGroup.javascript().execute(Boolean.class, isCheckedFieldGroup))
        {
            return DATE_TYPE.moreThan;
        }

        if (dueInNextFieldGroup.isPresent() && dueInNextFieldGroup.javascript().execute(Boolean.class, isCheckedFieldGroup))
        {
            return DATE_TYPE.dueInNext;
        }

        if (withinTheLastFieldGroup.javascript().execute(Boolean.class, isCheckedFieldGroup)) {
            return DATE_TYPE.withinTheLast;
        }

        return null;
    }
}