package com.atlassian.jira.plugin.issuenav.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import static org.junit.Assert.assertEquals;

/**
 * Utilities for parsing and making assertions on search editHtml
 * @since v5.1
 */
public class EditHtmlUtils
{
    protected EditHtmlUtils(){}

    public static void assertSelectHtmlValues(String editHtml, String ... expectedSelectedValues)
    {
        Document doc = Jsoup.parse(editHtml);
        Elements elements = doc.select("option[selected]");
        assertEquals(expectedSelectedValues.length, elements.size());
        for (int i = 0; i < expectedSelectedValues.length; ++i)
        {
            assertEquals(expectedSelectedValues[i], elements.get(i).attr("value"));
        }
    }

    public static void assertDatePickerHtmlValues(String editHtml, String expectedCreatedBeforeDate, String expectedCreatedAfterDate)
    {
        Document doc = Jsoup.parse(editHtml);

        Element createdBefore = doc.select("[name=created:before]").get(0);
        assertEquals(expectedCreatedBeforeDate, createdBefore.attr("value"));

        Element createdAfter = doc.select("[name=created:after]").get(0);
        assertEquals(expectedCreatedAfterDate, createdAfter.attr("value"));
    }

    public static void assertTextFieldValues(String editHtml, String fieldName, String expectedValue)
    {
        Document doc = Jsoup.parse(editHtml);

        Element textField = doc.select("[name=" + fieldName + "]").get(0);
        assertEquals(expectedValue, textField.attr("value"));
    }
}
