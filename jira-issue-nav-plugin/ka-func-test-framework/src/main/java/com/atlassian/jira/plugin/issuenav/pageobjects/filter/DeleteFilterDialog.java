package com.atlassian.jira.plugin.issuenav.pageobjects.filter;

/**
 * The "Copy Filter" dialog that creates a filter from a search.
 *
 * @since v5.2
 */
public class DeleteFilterDialog extends FilterDialog
{

    public DeleteFilterDialog() {
        super("delete-filter-dialog");
    }

}
