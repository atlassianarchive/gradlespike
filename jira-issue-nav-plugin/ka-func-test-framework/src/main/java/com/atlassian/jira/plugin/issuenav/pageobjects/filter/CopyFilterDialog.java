package com.atlassian.jira.plugin.issuenav.pageobjects.filter;

/**
 * The "Copy Filter" dialog that copies a filter from a search.
 *
 * @since v5.2
 */
public class CopyFilterDialog extends FilterDialog
{
    public CopyFilterDialog()
    {
        super("copy-filter-dialog");
    }
}
