package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.0
 */
public class IssueCommentComponent
{
    private final PageElement element;
    public IssueCommentComponent(PageElement element)
    {
        this.element = element;
    }

    public Long getId()
    {
        return Long.valueOf(element.getAttribute("id").split("-")[1]);
    }

    public String getContents()
    {
        return element.find(By.className("action-body")).getText();
    }

    public String getAuthor()
    {
        return element.find(By.className("user-hover")).getAttribute("rel");
    }

    public Date getDate()
    {
        final String dateTimeString = element.find(By.tagName("time")).getAttribute("datetime");
        try
        {
            return DateFormat.getDateTimeInstance().parse(dateTimeString);
        }
        catch (ParseException e)
        {
            return null;
        }
    }

}
