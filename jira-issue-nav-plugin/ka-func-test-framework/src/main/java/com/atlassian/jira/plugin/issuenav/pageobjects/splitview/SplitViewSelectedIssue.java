package com.atlassian.jira.plugin.issuenav.pageobjects.splitview;

import com.atlassian.jira.pageobjects.components.menu.IssueActions;
import com.atlassian.jira.pageobjects.components.menu.IssueActionsMenu;
import com.atlassian.jira.pageobjects.dialogs.IssueActionsUtil;
import com.atlassian.jira.pageobjects.dialogs.quickedit.EditIssueDialog;
import com.atlassian.jira.pageobjects.pages.viewissue.ActionTrigger;
import com.atlassian.jira.pageobjects.pages.viewissue.AssignIssueDialog;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.Map;

public class SplitViewSelectedIssue
{
    IssueActionsMenu actionsMenu;

    @ElementBy (cssSelector = ".issue-list .focused")
    private PageElement row;

    @Inject
    PageBinder binder;

    @Inject
    PageElementFinder pageElementFinder;

    @Inject
    IssueActionsUtil issueActionsUtil;

    @Inject
    private TraceContext traceContext;

    @WaitUntil
    public void ready()
    {
        row.timed().isPresent();
    }


    public void assignIssue(String user, ActionTrigger trigger)
    {
        Tracer checkpoint = traceContext.checkpoint();
        if (trigger == ActionTrigger.MENU)
        {
            PageElement pageElement = pageElementFinder.find(By.cssSelector("#issue-content #assign-issue"));
            Poller.waitUntilTrue(pageElement.timed().isVisible());
            pageElement.click();
            AssignIssueDialog assignIssueDialog = binder.bind(AssignIssueDialog.class);
            assignIssueDialog.setAssignee(user);
            assignIssueDialog.submit();
        }
        else
        {
            issueActionsUtil.assignIssue(user, trigger);
        }
        traceContext.waitFor(checkpoint, "jira.search.stable.update");

    }

    public void editIssue(Map<String, String> values, ActionTrigger trigger)
    {
        Tracer checkpoint = traceContext.checkpoint();
        if (trigger == ActionTrigger.MENU)
        {
            PageElement pageElement = pageElementFinder.find(By.cssSelector("#issue-content #edit-issue"));
            Poller.waitUntilTrue(pageElement.timed().isVisible());
            pageElement.click();
            final EditIssueDialog editIssueDialog = binder.bind(EditIssueDialog.class);
            editIssueDialog.setFields(values);
            editIssueDialog.submit();
        }
        else
        {
            issueActionsUtil.editIssue(values, trigger);
        }
        traceContext.waitFor(checkpoint, "jira.search.stable.update");
    }

    public String getSummary()
    {
        return row.find(By.className("issue-link-summary")).getText().trim();
    }

    public void assignToMe(final ActionTrigger trigger)
    {
        Tracer checkpoint = traceContext.checkpoint();
        if (trigger == ActionTrigger.MENU)
        {
            PageElement pageElement = pageElementFinder.find(By.cssSelector("#issue-content #assign-to-me"));
            Poller.waitUntilTrue(pageElement.timed().isVisible());
            pageElement.click();
        }
        else
        {
            issueActionsUtil.invokeActionTrigger(IssueActions.ASSIGN_TO_ME, trigger);
        }
        traceContext.waitFor(checkpoint, "jira.search.stable.update");
    }

    public String getIssueKey()
    {
        return row.getAttribute("data-key");
    }

    public boolean isInView()
    {
        return (Boolean)row.javascript().execute("return jQuery(arguments[0]).isInView()", row);
    }
}
