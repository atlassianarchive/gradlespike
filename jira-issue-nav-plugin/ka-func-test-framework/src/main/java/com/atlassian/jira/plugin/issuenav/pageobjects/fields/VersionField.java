package com.atlassian.jira.plugin.issuenav.pageobjects.fields;



import com.atlassian.jira.pageobjects.components.fields.MultiSelect;

/**
 * A version field, inline edit style. This should be fine for Affects Version as well as Fixed Version, just the id
 * in.
 *
 * @since v5.1
 */
public class VersionField extends InlineField
{
    public static final String FIXVERSION_ID = "fixVersions";
    public static final String FIXVERSION_TRIGGER_SELECTOR = "#fixfor-val";

    public static final String AFFECTSVERSION_ID = "versions";
    public static final String AFFECTSVERSION_TRIGGER_SELECTOR = "#versions-val";

    private MultiSelect multiSelect;
    private String id;
    private String triggerSelector;

    public VersionField(String id, String triggerSelector)
    {
        this.id = id;
        this.triggerSelector = triggerSelector;
    }

    @Override
    protected String getTriggerSelector()
    {
        return triggerSelector;
    }

    @Override
    public Field fill(final String... values)
    {
        for (String value : values)
        {
            multiSelect().add(value);
        }
        return this;
    }

    @Override
    public String getId()
    {
        return id;
    }

    public MultiSelect multiSelect()
    {
        if (null == multiSelect)
        {
            multiSelect = binder.bind(MultiSelect.class, getId());
        }
        return multiSelect;
    }
}