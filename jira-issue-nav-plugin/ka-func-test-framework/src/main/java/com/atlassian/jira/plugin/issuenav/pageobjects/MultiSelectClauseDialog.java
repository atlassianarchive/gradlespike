package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.Option;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.SelectElement;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * A page object for the CheckboxMultiSelect (Sparkler) control.
 */
public class MultiSelectClauseDialog extends SelectClauseDialog
{
    public static final Function<Option, String> OPTION_TO_STRING = new Function<Option, String>()
    {
        @Override
        public String apply(Option option)
        {
            return option.text();
        }
    };

    private PageElement sparkler; // The root element of the Sparkler.
    private PageElement searchInput; // The input at the top of the Sparkler.
    private String allOptionsLabel;

    public MultiSelectClauseDialog(String expectedDialogTitle, String selectElementId, String allOptionsLabel)
    {
        super(expectedDialogTitle, selectElementId);
        this.allOptionsLabel = allOptionsLabel;
    }

    @WaitUntil
    @Override
    public void ready()
    {
        sparkler = elementFinder.find(By.id(selectElementId + "-multi-select"));
        waitUntilTrue(sparkler.timed().isPresent());

        searchInput = elementFinder.find(By.id(selectElementId+"-input"));
        waitUntilTrue(searchInput.timed().isPresent());

        selectElement = elementFinder.find(By.id(this.selectElementId), SelectElement.class);
        waitUntilTrue(selectElement.timed().isPresent());
    }

    @Override
    public void clear()
    {
        this.deselectAll();
    }

    /**
     * Deselect all options.
     */
    public void deselectAll()
    {
        List<PageElement> checkboxes = sparkler.findAll(By.cssSelector("input[type=checkbox]"));

        for (PageElement checkbox : checkboxes)
        {
            if (checkbox.hasAttribute("checked", "true"))
            {
                checkbox.click();
            }
        }
    }

    public MultiSelectClauseDialog typeSearchText(final String value)
    {
        searchInput.clear();
        searchInput.type(value);
        return this;
    }

    public boolean hasSearchText()
    {
        return searchInput.getValue() != "";
    }

    public void clearSearchTextWithButton()
    {
        sparkler.find(By.className("clear-field")).click();
    }

    /**
     * Clicks the "Clear selected items" item in the sparkler dropdown
     */
    public MultiSelectClauseDialog clearSelectedItems()
    {
        PageElement element = sparkler.find(By.className("clear-all"));
        waitUntilTrue(element.timed().isVisible());
        element.click();
        return this;
    }

    /**
     * Ensures that none of the given values are selected.
     *
     * @param values The labels of the values.
     */
    public void deselectValues(final String... values)
    {
        deselectValues(Arrays.asList(values), ClickTarget.defaultTarget());
    }

    /**
     * Ensures that none of the given values are selected.
     *
     * @param values The labels of the values.
     * @param clickTarget the target to click on when selecting values (input/label)
     */
    public void deselectValues(final Iterable<String> values, ClickTarget clickTarget)
    {
        List<String> selectedLabels = getSelectedLabels();
        for (String value : values)
        {
            if (selectedLabels.contains(value))
            {
                toggleOptionByString(value, clickTarget);
            }
        }
    }

    /**
     * @return All visible options.
     */
    @Override
    public List<Option> getAllOptions()
    {
        List<Option> options = new ArrayList<Option>();
        List<PageElement> labels = getLabels();
        List<PageElement> inputs = getInputs();

        for (int i = 0; i < labels.size(); i++)
        {
            PageElement label = labels.get(i);
            PageElement checkbox = inputs.get(i);
            options.add(Options.full("", checkbox.getValue(), label.getText()));
        }

        return options;
    }

    public int getAllOptionsCount()
    {
        return getLabels().size();
    }

    public List<String> getAllOptionLabels()
    {
        List<String> options = new ArrayList<String>();
        for (PageElement label : getLabels())
        {
            options.add(label.getText());
        }

        return options;
    }

    public PageElement getLabelByTitleAndImage(String title, String imagePath)
    {
        return sparkler.find(By.cssSelector("li label[title='"+title+"'] img[src*='"+imagePath+"']"));
    }

    /**
     * @return The number of currently selected options.
     */
    public int getNumSelected() {
        List<Option> selectedOptions = getSelectedOptions();

        // Having the "All" option selected is equivalent to no selection.
        if (selectedOptions.size() == 1 &&
                selectedOptions.get(0).text().equals(allOptionsLabel))
        {
            return 0;
        }

        return selectedOptions.size();
    }

    /**
     * @return All selected options.
     */
    public List<Option> getSelectedOptions()
    {
        List<Option> options = new ArrayList<Option>();
        List <PageElement> labels = getLabels();
        List <PageElement> inputs = getInputs();

        for (int i = 0; i < labels.size(); i++)
        {
            PageElement label = labels.get(i);
            PageElement checkbox = inputs.get(i);
            if (checkbox.hasAttribute("checked", "true"))
            {
                options.add(Options.full("", checkbox.getValue(),
                        label.getAttribute("title")));
            }
        }

        return options;
    }

    /**
     * Ensures that all of the given values are selected. Values are selected by clicking on the checkbox.
     *
     * @param values The labels of the values.
     * @see #selectValues(Iterable, com.atlassian.jira.plugin.issuenav.pageobjects.MultiSelectClauseDialog.ClickTarget)
     */
    public void selectValues(final String... values)
    {
        selectValues(Arrays.asList(values), ClickTarget.defaultTarget());
    }

    /**
     * Ensures that all of the given values are selected. Values will be selected by clicking on the provided
     * ClickTarget.
     *
     * @param values The labels of the values.
     * @param clickTarget the target to click on when selecting values (input/label)
     */
    public void selectValues(Iterable<String> values, ClickTarget clickTarget)
    {
        List<String> selectedLabels = getSelectedLabels();
        for (String value : values)
        {
            if (!selectedLabels.contains(value))
            {
                toggleOptionByString(value, clickTarget);
            }
        }
    }

    /**
     * Toggling options either on/off regardless of their previous state
     *
     * @param values List of option labels to toggle
     */
    public void toggleValues(final String... values)
    {
        toggleValues(Lists.newArrayList(values), ClickTarget.defaultTarget());
    }

    /**
     * Toggling options either on/off regardless of their previous state
     *
     * @param values List of option labels to toggle
     */
    public void toggleValues(Iterable<String> values, ClickTarget clickTarget)
    {
        for (String value : values)
        {
            toggleOptionByString(value, clickTarget);
        }
    }

    /**
     * First types the keyword into the search box before selecting it.
     * <p/>
     * This prevents issues finding an option if it is only accessible when searched for (i.e. when there are too many
     * options to display).
     *
     * @param values The labels of the values.
     */
    public void typeKeywordAndSelectValues(final String... values)
    {
        List<String> selectedLabels = getSelectedLabels();
        for (String value : values)
        {
            if (!selectedLabels.contains(value))
            {
                typeSearchText(value);
                toggleOptionByString(value, ClickTarget.defaultTarget());
            }
        }

    }

    /**
     * @return all invalid options (have the "invalid_sel" class).
     */
    public List<Option> getInvalidOptions()
    {
        List<Option> options = new ArrayList<Option>();
        List<PageElement> invalidOptions = sparkler.findAll(By.cssSelector("li.invalid_sel"));
        List<PageElement> checkboxes = sparkler.findAll(By.cssSelector("li.invalid_sel input"));

        for (int i = 0; i < invalidOptions.size(); i++)
        {
            PageElement pageElement = invalidOptions.get(i);
            PageElement checkbox = checkboxes.get(i);
            options.add(Options.full("", checkbox.getValue(), pageElement.getText()));
        }

        return options;
    }

    /**
     * Each {@code label} element in the Sparkler maps to an option that can be
     * selected. The label contains its checkbox and name (as text).
     *
     * @return All visible {@code label} elements.
     */
    private List<PageElement> getLabels()
    {
        return sparkler.findAll(By.cssSelector("li label"));
    }

    private List<PageElement> getIcons()
    {
        return sparkler.findAll(By.cssSelector("li label img"));
    }

    private List<PageElement> getInputs()
    {
        return sparkler.findAll(By.cssSelector("li label input"));
    }

    /**
     * @return The labels of all selected options.
     */
    private List<String> getSelectedLabels()
    {
        ArrayList<String> labels = new ArrayList<String>();
        List<Option> options = getSelectedOptions();

        for (Option option : options)
        {
            labels.add(option.text());
        }

        return labels;
    }

    /**
     * Toggle the option with the given name by clicking on the requested ClickTarget.
     *
     * @param name The name of the option to toggle.
     * @param clickTarget the target to click on, or null for the default target
     */
    private void toggleOptionByString(String name, ClickTarget clickTarget)
    {
        // either click on the <input> or on the <label>
        String selector = (clickTarget != null ? clickTarget : ClickTarget.defaultTarget()).cssSelector(name, true);

        // JRADEV-15994
        // IE Webdriver weirdness
        sparkler.javascript().execute(String.format("AJS.$(\"%s\")[0].scrollIntoView(true)", selector));
        sparkler.find(By.cssSelector(selector)).click();
    }

    /**
     * Wait until the Sparkler has executed a query.
     *
     * @param query The query.
     */
    public MultiSelectClauseDialog waitForSearch(String query)
    {
        waitUntilTrue(sparkler.timed().hasAttribute("data-query", query));
        return this;
    }

    public static enum ClickTarget
    {
        LABEL
                {
                    @Override
                    String cssSelector(String name)
                    {
                        return cssSelector(name, false);
                    }
                    @Override
                    String cssSelector(String name, Boolean strict)
                    {
                        return String.format("label[data-descriptor-title"+(strict?"":"*")+"='%s']", name);
                    }
                },
        INPUT
                {
                    @Override
                    String cssSelector(String name)
                    {
                        return cssSelector(name, false);
                    }
                    @Override
                    String cssSelector(String name, Boolean strict)
                    {
                        return String.format("label[data-descriptor-title"+(strict?"":"*")+"='%s'] input", name);
                    }
                };

        abstract String cssSelector(String name);
        abstract String cssSelector(String name, Boolean strict);

        static ClickTarget defaultTarget()
        {
            return INPUT;
        }
    }
}