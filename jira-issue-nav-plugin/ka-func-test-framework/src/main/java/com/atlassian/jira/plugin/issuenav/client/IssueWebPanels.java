package com.atlassian.jira.plugin.issuenav.client;

import com.atlassian.jira.testkit.client.restclient.LinkGroup;

import java.util.List;

/**
 * @since v5.1
 */
public class IssueWebPanels
{
    public List<IssueWebPanel> leftPanels;
    public List<IssueWebPanel> rightPanels;
    public List<IssueWebPanel> infoPanels;

    public static class IssueWebPanel
    {
        public String completeKey;
        public String prefix;
        public String id;
        public String styleClass;
        public String label;
        public boolean renderHeader;
        public LinkGroup headerLinks;
        public List<String> subpanelHtmls;
        public String html;
        public Integer weight;
    }
}
