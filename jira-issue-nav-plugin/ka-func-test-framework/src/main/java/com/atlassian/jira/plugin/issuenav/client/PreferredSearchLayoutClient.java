package com.atlassian.jira.plugin.issuenav.client;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.sun.jersey.api.client.WebResource;

/**
 * A wrapper around <tt>PreferredSearchLayoutResource</tt>.
 *
 * @since v6.0
 */
public class PreferredSearchLayoutClient extends RestApiClient<PreferredSearchLayoutClient>
{
    private final JIRAEnvironmentData jiraEnvironmentData;

    public PreferredSearchLayoutClient(final JIRAEnvironmentData jiraEnvironmentData)
    {
        super(jiraEnvironmentData);
        this.jiraEnvironmentData = jiraEnvironmentData;
    }

    /**
     * @return the key of the current user's preferred search layout.
     */
    public String getPreferredSearchLayout()
    {
        return createResource().get(String.class);
    }

    /**
     * @param layoutKey The layout key to set.
     */
    public void setPreferredSearchLayout(String layoutKey)
    {
        createResource().header("X-Atlassian-Token", "nocheck").post(String.class, "layoutKey=" + layoutKey);
    }

    protected WebResource createResource()
    {
        String URL = jiraEnvironmentData.getBaseUrl().toExternalForm();
        return resourceRoot(URL).path("rest/issueNav/latest/preferredSearchLayout");
    }
}