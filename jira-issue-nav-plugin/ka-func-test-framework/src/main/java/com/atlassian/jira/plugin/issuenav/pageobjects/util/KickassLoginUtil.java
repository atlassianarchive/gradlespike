package com.atlassian.jira.plugin.issuenav.pageobjects.util;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.jira.plugin.issuenav.pageobjects.util.login.QuickAuth;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.atlassian.pageobjects.Page;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.google.common.base.Objects;
import com.google.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;


/**
 * Kickass login utility that skips the dashboard after logging in
 * @since v5.1
 */
public class KickassLoginUtil
{
    @Inject
    private TraceContext traceContext;

    public static <P extends Page> P kickAssWithSysAdmin(JiraTestedProduct product, Class<P> pageClass, Object ... params)
    {
        return kickAssWith(product, "admin", "admin", pageClass, params);
    }

    public IssuesPage kickAssOnIssuesWithSysAdmin(JiraTestedProduct product, Object... params)
    {
        Tracer tracer = traceContext.checkpoint();
        IssuesPage issuesPage = kickAssWithSysAdmin(product, IssuesPage.class, params);
        return issuesPage.waitForResults(tracer, true);
    }

    public static <P extends Page> P kickAssWith(JiraTestedProduct product, Class<P> pageClass, Object ... params)
    {
        return kickAssWith(product, ADMIN_USERNAME, ADMIN_PASSWORD, pageClass, params);
    }

    public static <P extends Page> P kickAssWith(JiraTestedProduct product, String username, String password, Class<P> pageClass, Object ... params)
    {
        // skip login/logout dance if we're already logged in as the right user
        String loggedInUser = getLoggedInUser(product);
        if (!Objects.equal(username, loggedInUser))
        {
            // we may want to get a bit smarter about unnecessary logout/login
            QuickAuth.forProduct(product).logout().login(username, password);
        }

        return product.getPageBinder().navigateToAndBind(pageClass, params);
    }

    public IssuesPage kickAssOnIssuesWith(JiraTestedProduct product, String username, String password, Object... params)
    {
        Tracer tracer = traceContext.checkpoint();
        IssuesPage issuesPage = kickAssWith(product, username, password, IssuesPage.class, params);
        issuesPage.waitForResults(tracer, true);

        if (issuesPage.isSplitLayout() && !issuesPage.emptySearchResultsMessageIsVisible())
        {
            // Wait for the issue to load too.
            traceContext.waitFor(tracer, "jira.issue.refreshed");
        }

        return issuesPage;
    }

    /**
     * Returns the logged in user
     *
     * @param product a JiraTestedProduct
     * @return the logged in user
     */
    private static String getLoggedInUser(JiraTestedProduct product)
    {
        WebDriverTester tester = product.getTester();
        AtlassianWebDriver driver = tester.getDriver();

        JIRAEnvironmentData envData = product.environmentData();
        if (!driver.getCurrentUrl().startsWith(envData.getBaseUrl().toString()))
        {
            // any page will do, this one should be quick to render...
            product.getTester().gotoUrl(envData.getBaseUrl() + "/secure/AboutPage.jspa");
        }

        return (String) driver.executeScript("AJS && AJS.Meta && AJS.Meta.get('remote-user')");
    }
}
