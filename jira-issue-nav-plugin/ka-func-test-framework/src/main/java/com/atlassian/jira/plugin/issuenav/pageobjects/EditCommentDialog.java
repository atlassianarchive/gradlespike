package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.dialogs.FormDialog;
import org.openqa.selenium.By;

public class EditCommentDialog extends FormDialog
{
    public EditCommentDialog()
    {
        super("edit-comment");
    }

    public EditCommentDialog setComment(String text)
    {
        getDialogElement().find(By.id("comment")).type(text);
        return this;
    }

    public boolean submit()
    {
        return super.submit(By.id("comment-edit-submit"));
    }

}
