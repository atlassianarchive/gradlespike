package com.atlassian.jira.plugin.issuenav.pageobjects.filter;

import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssuesPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * TODO: Document this class / interface here
 *
 * @since v5.2
 */
public class FilterActionsMenu {

    @Inject
    private PageElementFinder elementFinder;

    @Inject
    private PageBinder pageBinder;

    @ElementBy(className = "ajs-layer")
    private PageElement filterActionMenu;

    @ElementBy(className = "rename-filter")
    private PageElement renameFilterLink;

    @ElementBy(className = "unfavourite-filter")
    private PageElement unfavouriteFilterLink;

    @ElementBy(className = "copy-filter")
    private PageElement copyFilterLink;

    @ElementBy(className = "delete-filter")
    private PageElement deleteFilterLink;

    @WaitUntil
    public void ready()
    {
        waitUntilTrue(filterActionMenu.timed().isVisible());
    }

    public IssuesPage rename(final String name, Tracer trace) {
        renameFilterLink.click();
        pageBinder.bind(RenameFilterDialog.class).setName(name).submit();
        return pageBinder.bind(IssuesPage.class).waitForFilterSave(trace);
    }

    public IssuesPage removeFromFavourites() {
        unfavouriteFilterLink.click();
        return pageBinder.bind(IssuesPage.class);
    }

    public IssuesPage copyFilter(final String name, Tracer trace) {
        copyFilterLink.click();
        pageBinder.bind(CopyFilterDialog.class).setName(name).submit();
        return pageBinder.bind(IssuesPage.class).waitForFilterSave(trace);
    }

    public IssuesPage deleteFilter(Tracer trace)
    {
        deleteFilterLink.click();
        pageBinder.bind(DeleteFilterDialog.class).submit();
        return pageBinder.bind(IssuesPage.class).waitForResults(trace);
    }

    public IssuesPage deleteFilterWithNoSubsequentSearch()
    {
        deleteFilterLink.click();
        pageBinder.bind(DeleteFilterDialog.class).submit();
        return pageBinder.bind(IssuesPage.class);
    }

}
