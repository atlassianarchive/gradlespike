package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.navigator.SelectedIssue;
import com.atlassian.jira.pageobjects.pages.viewissue.ActionTrigger;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.openqa.selenium.By;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.not;

/**
 * Represents the search results table below the query boxes.
 */
public class ResultsTableComponent
{
    private static final String DATA_ISSUE_KEY = "data-issuekey";

    @Inject
    private PageElementFinder pageElementFinder;

    @Inject private PageBinder binder;
    @Inject private PageElementFinder elementFinder;
    @Inject private PageBinder pageBinder;
    @Inject private TraceContext traceContext;

    public static enum SORT_ORDER {
        desc,
        asc
    }

    @WaitUntil
    public ResultsTableComponent ready()
    {
        waitUntilTrue(getResultsTable().timed().isVisible());
        // TODO: remove this, use TraceContext
        waitUntilFalse(getResultsTable().timed().hasClass("loading"));
        return this;
    }

    /**
     * @return The number of rows in the results table.
     */
    public int getResultCount()
    {
        return getResultRows().size();
    }

    public List<String> getColumnHeaders()
    {
        return Lists.transform(getColumnHeadings(), new Function<PageElement, String>()
        {
            @Override
            public String apply(@Nullable PageElement pageElement)
            {
                return pageElement.getText();
            }
        });
    }

    public PageElement getColumnHeadingByDataId(final String id)
    {
        Collection<PageElement> headings = Collections2.filter(getColumnHeadings(),  new Predicate<PageElement>()
        {
            @Override
            public boolean apply(PageElement element) {
                return id.equals(element.getAttribute("data-id"));
            }
        });

        return (com.atlassian.pageobjects.elements.PageElement)headings.toArray()[0];
    }


    public ResultsTableComponent next()
    {
        getKeyboardShortcutTarget().type("j");
        return this;
    }

    public ResultsTableComponent prev()
    {
        getKeyboardShortcutTarget().type("k");
        return this;
    }

    /**
     * @return the index of the first issue on the page.
     */
    public int getPageStartIndex()
    {
        return Integer.parseInt(getPageStartIndexTimed().now());
    }

    /**
     * @return the timed version of index of the first issue on the page.
     */
    public TimedQuery<String> getPageStartIndexTimed()
    {
        By locator = By.className("results-count-start");
        return elementFinder.find(locator).timed().getText();
    }

    /**
     * @return the index of the last issue on the page.
     */
    public int getPageEndIndex()
    {
        return Integer.parseInt(getPageStartIndexTimed().now());
    }

    /**
     * @return the timed version of index of the last issue on the page.
     */
    public TimedQuery<String> getPageEndIndexTimed()
    {
        By locator = By.className("results-count-end");
        return elementFinder.find(locator).timed().getText();
    }

    /**
     * @return the total number of issues matched by the search.
     */
    public int getResultsCountTotal()
    {
        By locator = By.className("results-count-total");
        return Integer.parseInt(elementFinder.find(locator).getText());
    }

    public ResultsTableComponent selectNextIssue()
    {
        final String oldIssueID = getSelectedIssueID();
        next();
        waitUntil(getActiveResultElement().timed().getAttribute("rel"), not(oldIssueID));
        return this;
    }

    public ResultsTableComponent selectPreviousIssue()
    {
        final String oldIssueID = getSelectedIssueID();
        prev();
        waitUntil(getActiveResultElement().timed().getAttribute("rel"), not(oldIssueID));
        return this;
    }

    public SelectedIssue getSelectedIssue()
    {
        return pageBinder.bind(SelectedIssue.class);
    }

    public IssueDetailComponent openSelectedIssue()
    {
        return openSelectedIssue(ReadyForAction.READY).ready();
    }

    public IssueDetailComponent openSelectedIssueViaIssueActionCog()
    {
        final SelectedIssue selectedIssue = this.getSelectedIssue();
        selectedIssue.prepareForAction(ActionTrigger.MENU);
        String selector = "#actions_" + selectedIssue.getIssueId() + "_drop .issue-link";

        FutureIssueDetailComponent issueDetailComponent = new FutureIssueDetailComponent(elementFinder, pageBinder, traceContext);

        PageElement viewIssueAction = elementFinder.find(By.cssSelector(selector));
        Poller.waitUntilTrue(viewIssueAction.timed().isVisible());
        viewIssueAction.click();

        return issueDetailComponent.readyFor(ReadyForAction.READY).ready();
    }

    public FutureIssueDetailComponent openSelectedIssue(ReadyForAction readyForAction)
    {
        String selectedIssueKey = getSelectedIssueKey();
        PageElement issueLink = pageElementFinder.find(By.cssSelector(String.format("tr[data-issuekey=\"%s\"] .summary a", selectedIssueKey)));
        waitUntilTrue(issueLink.timed().isPresent());

        FutureIssueDetailComponent issueDetailComponent = new FutureIssueDetailComponent(elementFinder, pageBinder, traceContext);
        issueLink.click();

        return issueDetailComponent.readyFor(readyForAction);
    }

    public String getSelectedIssueKey()
    {
        final PageElement currentSelection = getActiveResultElement();
        if (!currentSelection.isPresent())
        {
            return null;
        }
        else
        {
            return currentSelection.getAttribute(DATA_ISSUE_KEY);
        }
    }

    public String getSelectedIssueID()
    {
        PageElement selectedRow = getActiveResultElement();
        if (selectedRow.isPresent())
        {
            return selectedRow.getAttribute("rel");
        }
        else
        {
            return null;
        }
    }

    public ResultsTableComponent sortColumn(String column, SORT_ORDER sort_order)
    {
        final PageElement columnEl = getResultsTable().find(By.className("headerrow-" + column));

        if (sort_order == SORT_ORDER.desc) {
            if (columnEl.hasAttribute("rel", column + ":DESC")) {
                columnEl.click();
                waitUntilTrue(columnEl.timed().hasAttribute("rel", column + ":ASC"));
            }

        } else {
            if (columnEl.hasAttribute("rel", column + ":ASC")) {
                columnEl.click();
                waitUntilTrue(columnEl.timed().hasAttribute("rel", column + ":DESC"));
            }
        }
        return this;
    }

    public Map<String, SORT_ORDER> getSortedColumns()
    {
        Map<String, SORT_ORDER> sortedCols = new HashMap<String, SORT_ORDER>();
        final List<PageElement> all = getResultsTable().findAll(By.cssSelector(".colHeaderLink.sortable"));
        for (PageElement pageElement : all)
        {
            final String col = pageElement.getAttribute("data-id");
            if (pageElement.hasAttribute("rel", col + ":ASC")) {
                sortedCols.put(col, SORT_ORDER.desc);
            }
            else
            {
                sortedCols.put(col, SORT_ORDER.asc);
            }
        }
        return sortedCols;
    }

    public IssueDetailComponent navigateToIssueDetailPage(final String issueKey)
    {
        clickIssue(issueKey);
        return getIssueDetailPage(issueKey);
    }

    public void clickIssue(String issueKey)
    {
        getResultsTable().find(By.cssSelector("[" + DATA_ISSUE_KEY + "=" + issueKey + "] .summary a")).click();
    }

    /**
     * Highlight an issue row by clicking it.
     *
     * @param issueID The issue's ID.
     */
    public ResultsTableComponent highlightIssueViaClick(Long issueID)
    {
        String oldIssueID = getSelectedIssueID();
        getResultsTable().find(By.cssSelector("[rel='" + issueID.toString() + "']")).click();
        waitUntil(getActiveResultElement().timed().getAttribute("rel"), not(oldIssueID));
        return this;
    }

    /**
     * Scrolls to an issue
     *
     * @param issueID The issue's ID.
     */
    public ResultsTableComponent scollToIssue(Long issueID)
    {
        PageElement issue = getResultsTable().find(By.cssSelector("[rel='" + issueID.toString() + "']"));
        issue.javascript().execute("return jQuery(arguments[0])[0].scrollIntoView()", issue);
        return this;
    }


    public AuiMessage navigateAndExpectError(final String issueKey, final String uriPattern)
    {
        clickIssue(issueKey);
        return binder.bind(AuiMessage.class, uriPattern);
    }

    public IssueDetailComponent getIssueDetailPage(final String issueKey)
    {
        return binder.bind(IssueDetailComponent.class, issueKey);
    }

    public TimedCondition isDetailsPageVisible()
    {
        return elementFinder.find(By.className("content-container")).timed().hasClass("navigator-collapsed");
    }

    /**
     * Note that page numbers aren't rendered if the number of results is less
     * than the user's configured page size (and this method will time out).
     *
     * @return The index of the currently visible page (1-based).
     */
    public int getCurrentPageIndex()
    {
        By selector = By.cssSelector(".navigator-content .pagination strong");
        return Integer.parseInt(elementFinder.find(selector).getText());
    }

    /**
     * Go to the next page of results.
     */
    public void goToNextPage()
    {
        Tracer tracer = traceContext.checkpoint();
        goToNextPageWithoutWaiting();
        pageBinder.bind(IssuesPage.class).waitForStableUpdate(tracer);
    }

    /**
     * Go to the next page of results.
     */
    public void goToNextPageWithoutWaiting()
    {
        By selector = By.cssSelector(".navigator-content .pagination .icon-next");
        elementFinder.find(selector).click();
    }

    public ViewIssuePage selectIssueUsingO()
    {
        final String selectedIssueKey = getSelectedIssue().getSelectedIssueKey();
        getKeyboardShortcutTarget().type("o");
        return pageBinder.bind(ViewIssuePage.class, selectedIssueKey);
    }

    public IssueDetailComponent selectIssueUsingKey(CharSequence key)
    {
        final String selectedIssueKey = getSelectedIssue().getSelectedIssueKey();
        getKeyboardShortcutTarget().type(key);
        return pageBinder.bind(IssueDetailComponent.class, selectedIssueKey);
    }

    public void assignToMeTheCurrentIssue()
    {
        Tracer tracer = traceContext.checkpoint();
        getKeyboardShortcutTarget().type("i");
        pageBinder.bind(IssuesPage.class).waitForStableUpdate(tracer);
    }

    private PageElement getActiveResultElement()
    {
        return getResultsTable().find(By.className("focused"));
    }

    private List<PageElement> getResultRows()
    {
        return getResultsTable().find(By.tagName("tbody")).findAll(By.tagName("tr"));
    }

    private List<PageElement> getColumnHeadings()
    {
        return getResultsTable().findAll(By.cssSelector("th"));
    }

    private PageElement getResultsTable()
    {
        return elementFinder.find(By.id("issuetable"));
    }

    private PageElement getKeyboardShortcutTarget()
    {
        return elementFinder.find(By.tagName("body"));
    }

    /**
     * @param issueKey An issue key.
     * @return Whether the results table contains the issue with a given key.
     */
    public boolean containsIssue(String issueKey)
    {
        String selector = "tr[data-issuekey=" + issueKey + "]";
        return getResultsTable().find(By.cssSelector(selector)).isPresent();
    }

    /**
     * Check whether an issue row is marked as inaccessible.
     * <p/>
     * We can't identify rows by issue ID or key because they're not in the markup.
     *
     * @param index The 0-based index of the issue row to check.
     * @return Whether the issue row at the given index is marked as inaccessible.
     */
    public boolean issueIsInaccessible(final int index)
    {
        List<PageElement> rows = getIssueRowElements();

        if (rows.size() > index)
        {
            return rows.get(index).hasClass("inaccessible-issue");
        }
        else
        {
            throw new IllegalArgumentException("There is no issue row at index " + index + ".");
        }
    }

    /**
     * Get list of issues on the page in order
     */
    public List<String> getIssueKeys()
    {
        return ImmutableList.copyOf(Lists.transform(getIssueRowElements(), new Function<PageElement, String>()
        {
            @Override
            public String apply(PageElement issue)
            {
                return issue.getAttribute("data-issuekey");
            }
        }));
    }

    private List<PageElement> getIssueRowElements()
    {
        return getResultsTable().findAll(By.cssSelector("tbody > tr"));
    }

    public static final class ContainsIssuesMatcher extends TypeSafeMatcher<ResultsTableComponent>
    {
        private final String[] issueKeys;

        public ContainsIssuesMatcher(final String... issueKeys)
        {
            this.issueKeys = issueKeys;
        }

        @Override
        public void describeTo(final Description description)
        {
            description.appendText("Result table should contains following issues" + issueKeys);
        }

        @Override
        public boolean matchesSafely(final ResultsTableComponent resultsTableComponent)
        {
            for (String issueKey : issueKeys)
            {
                if (!resultsTableComponent.containsIssue(issueKey))
                {
                    return false;
                }
            }
            return true;
        }
    }
}