package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import javax.inject.Inject;

public class IssueErrorComponent extends AbstractIssueDetailComponent
{
    @Inject protected PageElementFinder elementFinder;

    public AuiMessage getErrorMessage()
    {
        return pageBinder.bind(AuiMessage.class, "");
    }

    /**
     * @return whether it's a full screen error message.
     */
    public boolean isFullScreen()
    {
        return !elementFinder.find(By.className("navigator-container")).isPresent();
    }

    /**
     * Click the 'Return to Search' button in the error message.
     * @return
     */
    public IssuesPage clickReturnToSearch()
    {
        Tracer checkpoint = traceContext.checkpoint();
        elementFinder.find(By.id("return-to-search")).click();
        traceContext.waitFor(checkpoint, "jira.returned.to.search");

        return pageBinder.bind(IssuesPage.class);
    }

    /**
     * @return whether it's a full screen error message.
     */
    public TimedCondition hasReturnToSearchLink()
    {
        return elementFinder.find(By.id("return-to-search")).timed().isPresent();
    }
}