package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.pageobjects.elements.PageElement;

/**
 * Represents a fixed lozenge component whose searcher is an HTML select
 */
public class PrimeCriteriaUserPickerComponent extends PrimeCriteriaComponent<UserPickerClauseDialog>
{
    private String expectedDialogTitle;
    private String searcherId;
    private String inputFieldId;

    public PrimeCriteriaUserPickerComponent(PageElement lozenge, String expectedDialogTitle, String searcherId, String inputFieldId)
    {
        super(lozenge);
        this.expectedDialogTitle = expectedDialogTitle;
        this.searcherId = searcherId;
        this.inputFieldId = inputFieldId;
    }

    public <T> T selectUserAndWait(String user, Class<T> pageToBind)
    {
        open().selectUserAndWait(user);
        return pageBinder.bind(pageToBind);
    }

    public <T> T selectGroupAndWait(String group, Class<T> pageToBind)
    {
        open().selectGroupAndWait(group);
        return pageBinder.bind(pageToBind);
    }

    @Override
    public UserPickerClauseDialog dialog()
    {
        return pageBinder.bind(UserPickerClauseDialog.class, this.expectedDialogTitle, this.searcherId, this.inputFieldId);
    }
}
