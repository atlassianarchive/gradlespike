package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.util.SearcherWaiter;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Represents the basic query component including text field, criteria and "add criteria" button
 */
public class BasicQueryComponent
{
    @Inject
    private PageElementFinder elementFinder;
    
    @Inject
    private PageBinder pageBinder;

    @Inject
    private SearcherWaiter searcherWaiter;

    @Inject
    private TraceContext traceContext;

    @ElementBy(cssSelector = "button.criteria-selector[data-id=project]")
    private PageElement projectCriteria;

    @ElementBy(cssSelector = "button.criteria-selector[data-id=issuetype]")
    private PageElement issueTypeCriteria;

    @ElementBy(cssSelector = "button.criteria-selector[data-id=status]")
    private PageElement statusCriteria;

    @ElementBy(cssSelector = "button.criteria-selector[data-id=assignee]")
    private PageElement assigneeCriteria;

    @ElementBy(cssSelector = "button.criteria-selector[data-id=duedate]")
    private PageElement duedate;

    @ElementBy(cssSelector = "button.criteria-selector[data-id=component]")
    private PageElement componentCriteria;

    @ElementBy(cssSelector = "button.criteria-selector[data-id=resolution]")
    private PageElement resolution;

    @ElementBy (className = "add-criteria")
    private PageElement addCriteriaButton;

    @ElementBy(cssSelector = ".search-criteria-extended .criteria-list")
    private PageElement variableClauseList;

    @WaitUntil
    public void ready()
    {
        waitUntilTrue(addCriteriaButton.timed().isPresent());
    }

    public IssuesPage searchAndWait(String query)
    {
        setCurrentQuery(query);
        Tracer tracer = traceContext.checkpoint();
        getSearchInput().type(Keys.chord(Keys.RETURN));

        return pageBinder.bind(IssuesPage.class).waitForResults(tracer);
    }

    public PrimeSelectCriteria project()
    {
        return pageBinder.bind(PrimeSelectCriteria.class, projectCriteria, "project", "searcher-pid", "All projects");
    }

    public PrimeSelectCriteria component()
    {
        return pageBinder.bind(PrimeSelectCriteria.class, componentCriteria, "component", "searcher-component", "");
    }

    public PrimeSelectCriteria issueType()
    {
        return pageBinder.bind(PrimeSelectCriteria.class, issueTypeCriteria, "issue type", "searcher-type", "All issue types");
    }

    public PrimeSelectCriteria resolution()
    {
        return pageBinder.bind(PrimeSelectCriteria.class, issueTypeCriteria, "resolution", "searcher-resolution", "");
    }

    public PrimeSelectCriteria status()
    {
        return pageBinder.bind(PrimeSelectCriteria.class, statusCriteria, "status", "searcher-status", "All Statuses");
    }

    public PrimeSelectCriteria assignee()
    {
        return pageBinder.bind(PrimeSelectCriteria.class, assigneeCriteria, "assignee", "assignee", "");
    }

    public PrimeCriteriaDateComponent duedate()
    {
        return pageBinder.bind(PrimeCriteriaDateComponent.class, duedate);
    }

    public PrimeCriteriaUserPickerComponent reporter()
    {
        PageElement reporter = elementFinder.find(By.cssSelector("button.criteria-selector[data-id=reporter]"));
        return pageBinder.bind(PrimeCriteriaUserPickerComponent.class, reporter, "Reporter", "searcher-reporterSelect", "searcher-reporter");
    }


    public <T extends PrimeCriteriaComponent> T getCriteria(String searcherId, Class<T> criteriaComponent, Object... params)
    {
        final PageElement lozenge = elementFinder.find(By.cssSelector("button.criteria-selector[data-id=" + searcherId + "]"));
        final List<Object> list = Lists.newArrayList(params);
        list.add(0, lozenge);
        return pageBinder.bind(criteriaComponent, list.toArray(new Object[list.size()]));
    }
    
    public void setCurrentQuery(String query) {
        getSearchInput().clear().type(Keys.chord(query));
    }
    
    public String getCurrentQuery()
    {
        return getSearchInput().getValue();
    }

    public PageElement getSearchInput()
    {
        return elementFinder.find(By.className("search-entry"));
    }

    public AddCriteria openAddCriteriaDialog()
    {
        if (!addCriteriaButton.hasClass("active")) {
            addCriteriaButton.click();
        }
        return pageBinder.bind(AddCriteria.class);
    }

    public void closeAddCriteriaDialog()
    {
        if (addCriteriaButton.hasClass("active")) {
            addCriteriaButton.click();
        }
    }

    public boolean canAddCriteria()
    {
        return addCriteriaButton.isVisible();
    }

    public boolean hasInvalidExtendedCriteria()
    {
        for (ExtendedCriteriaComponent.Clause clause : extendedCriteria().getClauses())
        {
            if (!clause.isValidClause())
            {
                return true;
            }
        }
        return false;
    }

    public int numExtendedCriteria()
    {
        return extendedCriteria().getClauses().size();
    }

    public boolean hasVariableFilters()
    {
        return numExtendedCriteria() > 0;
    }
    public ExtendedCriteriaComponent extendedCriteria()
    {
        return pageBinder.bind(ExtendedCriteriaComponent.class, variableClauseList);
    }
}
