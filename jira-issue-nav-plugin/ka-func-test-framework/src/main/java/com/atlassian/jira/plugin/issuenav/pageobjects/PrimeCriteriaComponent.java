package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Represents a prime criteria component in the basic query
 */
public abstract class PrimeCriteriaComponent<T extends CriteriaDialog>
{
    @Inject
    protected PageBinder pageBinder;

    protected PageElement lozenge;

    public PrimeCriteriaComponent(PageElement lozenge)
    {
        this.lozenge = lozenge;
    }

    public T open()
    {
        if (!isDialogOpen()) {
            this.lozenge.click();
        }
        waitUntilTrue(lozenge.timed().hasClass("active"));
        return dialog();
    }

    public abstract T dialog();

    public boolean isDialogOpen()
    {
        return lozenge.hasClass("active");
    }

    public String getSelectedValue()
    {
        PageElement value = lozenge.find(By.className("fieldValue"));
        if (value.isPresent()) {
            return value.getText();
        } else {
            return "All";
        }
    }
}
