package com.atlassian.jira.plugin.issuenav.client;

import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import java.util.List;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import static com.atlassian.jira.issue.fields.layout.column.ColumnLayout.ColumnConfig;

public class IssueTableClient extends RestApiClient<IssueTableClient>
{
    private final JIRAEnvironmentData environmentData;

    public IssueTableClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
        this.environmentData = environmentData;
    }

    public IssueTableServiceOutcome getIssueTable(final Long filterId, final String jql, final String num, final Integer startIndex, final ColumnConfig columnConfig)
    {
        return createResource()
                .header("X-Atlassian-Token", "nocheck")
                .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE)
                .post(IssueTableServiceOutcome.class, asForm(filterId, jql, num, startIndex, columnConfig));
    }

    public Response getResponse(final Long filterId, final String jql, final String num, final Integer startIndex, final ColumnConfig columnConfig)
    {
        return toResponse(new Method()
        {
            @Override
            public ClientResponse call()
            {
                return createResource()
                        .header("X-Atlassian-Token", "nocheck")
                        .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE)
                        .post(ClientResponse.class, asForm(filterId, jql, num, startIndex, columnConfig));
            }
        }, String.class);
    }

    public IssueTableServiceOutcome getIssueTable(List<Long> issueIds)
    {
        return getIssueTable(issueIds, null, null, ColumnConfig.USER);
    }

    public IssueTableServiceOutcome getIssueTable(List<Long> issueIds, String filterId, List<String> explicitColumns, ColumnConfig columnConfig)
    {
        WebResource resource = createResource().path("stable");
        for (Long id : issueIds) {
            resource = resource.queryParam("id", id.toString());
        }
        if (filterId != null)
        {
            resource = resource.queryParam("filterId", filterId);
        }
        if (explicitColumns != null)
        {
            for (String column : explicitColumns)
            {
                resource = resource.queryParam("columns", column);
            }
        }
        if (columnConfig != null)
        {
            resource = resource.queryParam("columnConfig", String.valueOf(columnConfig));
        }

        return resource
                .header("X-Atlassian-Token", "nocheck")
                .type(MediaType.APPLICATION_FORM_URLENCODED)
                .post(IssueTableServiceOutcome.class);
    }

    private static MultivaluedMap<String, String> asForm(final Long filterId, final String jql, final String num,
            final Integer startIndex, final ColumnConfig columnConfig)
    {
        MultivaluedMap<String, String> form = new MultivaluedMapImpl();
        if (null != filterId)
        {
            form.putSingle("filterId", filterId.toString());
        }
        if (null != jql)
        {
            form.putSingle("jql", jql);
        }
        if (null != num)
        {
            form.putSingle("num", num);
        }
        if (null != startIndex)
        {
            form.putSingle("startIndex", String.valueOf(startIndex));
        }
        if (null != columnConfig)
        {
            form.putSingle("columnConfig", String.valueOf(columnConfig));
        }
        return form;
    }

    protected WebResource createResource()
    {
        return resourceRoot(environmentData.getBaseUrl().toExternalForm()).path("rest/issueNav/latest/issueTable");
    }
}
