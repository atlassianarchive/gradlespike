package com.atlassian.jira.plugin.issuenav.client;

import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * Client for issue tools
 */
public class IssueNavToolsClient extends RestApiClient<IssueNavToolsClient>
{
    private final JIRAEnvironmentData environmentData;

    public IssueNavToolsClient(final JIRAEnvironmentData environmentData)
    {
        super(environmentData);
        this.environmentData = environmentData;
    }

    public Response getResponse(final int searchResultsTotal, final int searchResultsPages)
    {
        return toResponse(new Method()
        {
            @Override
            public ClientResponse call()
            {
                return createResource(searchResultsTotal, searchResultsPages).get(ClientResponse.class);
            }
        });
    }

    protected WebResource createResource(int searchResultsTotal, int searchResultsPages)
    {
        return resourceRoot(environmentData.getBaseUrl().toExternalForm()).path("secure")
                .path("IssueNavTools!Default.jspa")
                .queryParam("decorator", "none")
                .queryParam("searchResultsTotal", String.valueOf(searchResultsTotal))
                .queryParam("searchResultsPages", String.valueOf(searchResultsPages));
    }
}