package com.atlassian.jira.plugin.issuenav.pageobjects;

import com.atlassian.jira.pageobjects.components.DropDown;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.google.common.collect.Sets;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Set;

/**
 * Dropdown implementation for the issue navigator tools menu
 * @since v5.2
 */
public class ToolsDropDown extends DropDown
{
    public static final String BULK_EDIT_PAGE = "bulkedit_max";
    public static final String BULK_EDIT_ALL = "bulkedit_all";
    public static final String USE_YOUR_COLS = "use-cols";
    public static final String USE_FILTER_COLS = "use-filter-cols";
    public static final String CONFIGURE_COLS = "configure-cols";
    public static final String SET_FILTER_ORDER = "addFilterColumnsLink";
    public static final String EDIT_FILTER_ORDER = "editFilterColumnsLink";
    public static final String CLEAR_SORTS = "a[href*='clearSorts.jspa']";

    public static final Set<String> ALL_OPTIONS = Sets.newHashSet(Arrays.asList(new String[]{
            BULK_EDIT_PAGE,
            BULK_EDIT_ALL,
            USE_YOUR_COLS,
            USE_FILTER_COLS,
            CONFIGURE_COLS,
            SET_FILTER_ORDER,
            EDIT_FILTER_ORDER
    }));

    @Inject
    private TraceContext traceContext;

    // HACK: see isOpen
    @Inject
    private PageElementFinder elementFinder;

    // HACK: see isOpen
    private By dropdownLocator;

    public ToolsDropDown(By triggerLocator, By dropdownLocator)
    {
        super(triggerLocator, dropdownLocator);
        this.dropdownLocator = dropdownLocator;
    }

    public IssuesPage selectUseYourColumnOrder()
    {
        Tracer tracer = traceContext.checkpoint();
        return this.click(By.id(USE_YOUR_COLS), IssuesPage.class).waitForResults(tracer);
    }

    public IssuesPage selectUseFilterColumnOrder()
    {
        Tracer tracer = traceContext.checkpoint();
        return this.click(By.id(USE_FILTER_COLS), IssuesPage.class).waitForResults(tracer);
    }

    public IssuesPage selectClearSorts()
    {
        Tracer tracer = traceContext.checkpoint();
        return this.click(By.cssSelector(CLEAR_SORTS), IssuesPage.class).waitForResults(tracer);
    }

    public TimedCondition isConfigureColumnsAvailable()
    {
        if (!isOpen())
        {
            open();
        }
        return elementFinder.find(By.id(CONFIGURE_COLS)).timed().isVisible();
    }

    public TimedCondition isBulkEditAvailable()
    {
        if (!isOpen())
        {
            open();
        }
        return (Conditions.or(
                elementFinder.find(By.id(BULK_EDIT_PAGE)).timed().isVisible(),
                elementFinder.find(By.id(BULK_EDIT_ALL)).timed().isVisible()
        ));
    }

    public TimedCondition isClearSortAvailable()
    {
        if (!isOpen())
        {
            open();
        }
        return elementFinder.find(By.cssSelector(CLEAR_SORTS)).timed().isVisible();
    }

    public TimedQuery<String> getBulkEditAllText()
    {
        if (!isOpen())
        {
            open();
        }
        return elementFinder.find(By.id(BULK_EDIT_ALL), TimeoutType.SLOW_PAGE_LOAD).timed().getText();
    }

    public TimedQuery<String> getBulkEditMaxText()
    {
        if (!isOpen())
        {
            open();
        }
        return elementFinder.find(By.id(BULK_EDIT_PAGE)).timed().getText();
    }

    @Override
    public boolean isOpen()
    {
        // TODO: fix
        // HACK: superclass fails if dropdown is not visible. override; should fix in master
        return elementFinder.find(this.dropdownLocator).isPresent() && super.isOpen();
    }
}
