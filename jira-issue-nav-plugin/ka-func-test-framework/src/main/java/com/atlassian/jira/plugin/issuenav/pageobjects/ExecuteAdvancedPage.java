package com.atlassian.jira.plugin.issuenav.pageobjects;

/**
 * The old issue navigator's <tt>executeAdvanced</tt> command.
 */
public class ExecuteAdvancedPage extends IssuesPage
{
    @Override
    public String getUrl()
    {
        return "/secure/IssueNavigator!executeAdvanced.jspa";
    }
}