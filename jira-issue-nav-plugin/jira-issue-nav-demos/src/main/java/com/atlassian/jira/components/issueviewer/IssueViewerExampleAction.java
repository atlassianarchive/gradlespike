package com.atlassian.jira.components.issueviewer;

import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;


public class IssueViewerExampleAction extends JiraWebActionSupport
{
    private final JiraWebResourceManager jiraWebResourceManager;

    public IssueViewerExampleAction(JiraWebResourceManager jiraWebResourceManager)
    {
        this.jiraWebResourceManager = jiraWebResourceManager;
    }

    @Override
    public String doExecute() throws Exception
    {
        jiraWebResourceManager.requireResource("com.atlassian.jira.jira-issue-nav-components:issueviewer");
        return SUCCESS;
    }

}
