package com.atlassian.jira.components.query;

import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;

public class QueryExampleAction extends JiraWebActionSupport
{
    private final JiraWebResourceManager jiraWebResourceManager;

    private String jql;

    public QueryExampleAction(JiraWebResourceManager jiraWebResourceManager)
    {
        this.jiraWebResourceManager = jiraWebResourceManager;
    }

    @Override
    public String doExecute() throws Exception
    {
        jiraWebResourceManager.requireResource("com.atlassian.jira.jira-issue-nav-components:query");
        return super.doExecute();
    }


}
