package com.atlassian.jira.components.issuetable;

import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;

public class IssueTableExampleAction extends JiraWebActionSupport
{
    private final JiraWebResourceManager jiraWebResourceManager;

    public IssueTableExampleAction(JiraWebResourceManager jiraWebResourceManager)
    {
        this.jiraWebResourceManager = jiraWebResourceManager;
    }

    @Override
    public String doExecute() throws Exception
    {

        jiraWebResourceManager.requireResource("com.atlassian.jira.jira-issue-nav-components:issueeditor"); // Really shouldn't be a requirement :(
        jiraWebResourceManager.requireResource("com.atlassian.jira.jira-issue-nav-plugin:viewissue"); // Really shouldn't be a requirement :(
        jiraWebResourceManager.requireResource("com.atlassian.jira.jira-issue-nav-plugin:issuetable-component");
        jiraWebResourceManager.requireResource("com.atlassian.jira.jira-issue-nav-plugin:testutils");
        return super.doExecute();
    }

}
