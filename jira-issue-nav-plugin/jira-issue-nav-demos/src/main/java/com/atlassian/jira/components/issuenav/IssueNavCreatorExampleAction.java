package com.atlassian.jira.components.issuenav;

import com.atlassian.jira.plugin.keyboardshortcut.KeyboardShortcutManager;
import com.atlassian.jira.plugin.webfragment.SimpleLinkManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.SimpleLink;
import com.atlassian.jira.plugin.webfragment.model.SimpleLinkSection;
import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.action.ActionViewData;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class IssueNavCreatorExampleAction extends JiraWebActionSupport
{
    private final JiraWebResourceManager wrm;
    private final KeyboardShortcutManager keyboardShortcutManager;
    private final SimpleLinkManager simpleLinkManager;
    private String project;
    private String mode;

    public IssueNavCreatorExampleAction(JiraWebResourceManager jiraWebResourceManager, final KeyboardShortcutManager keyboardShortcutManager, final SimpleLinkManager simpleLinkManager)
    {
        this.wrm = jiraWebResourceManager;
        this.keyboardShortcutManager = keyboardShortcutManager;
        this.simpleLinkManager = simpleLinkManager;
        project = "";
        mode = "generic";
    }

    @Override
    public String doExecute() throws Exception
    {
        keyboardShortcutManager.requireShortcutsForContext(KeyboardShortcutManager.Context.issuenavigation);
        keyboardShortcutManager.requireShortcutsForContext(KeyboardShortcutManager.Context.issueaction);
        wrm.requireResourcesForContext("jira.navigator");
        wrm.requireResourcesForContext("jira.navigator.simple");
        wrm.requireResourcesForContext("jira.navigator.advanced");
        wrm.requireResourcesForContext("jira.view.issue");
        wrm.requireResource("com.atlassian.jira.jira-issue-nav-components:issueeditor");
        wrm.requireResource("com.atlassian.jira.jira-issue-nav-plugin:issuenav");
        return super.doExecute();
    }

    @ActionViewData
    public String getProject()
    {
        return project;
    }

    @ActionViewData
    public String getMode()
    {
        return StringUtils.lowerCase(mode);
    }

    @ActionViewData
    public Collection<SimpleLink> getIssueOperations()
    {
        final List<SimpleLink> links = new ArrayList<SimpleLink>();
        final Map<String, Object> params = MapBuilder.<String, Object>newBuilder().
                add("issueId", "{0}").toMap();
        final JiraHelper helper = new JiraHelper(getHttpRequest(), null, params);
        final List<SimpleLinkSection> sections = simpleLinkManager.getSectionsForLocation("opsbar-operations", getLoggedInUser(), helper);
        for (SimpleLinkSection section : sections)
        {
            links.addAll(simpleLinkManager.getLinksForSectionIgnoreConditions(section.getId(), getLoggedInUser(), helper));
        }
        return links;
    }

    public void setProject(final String project)
    {
        this.project = project;
    }

    public void setMode(final String mode)
    {
        this.mode = mode;
    }
}
