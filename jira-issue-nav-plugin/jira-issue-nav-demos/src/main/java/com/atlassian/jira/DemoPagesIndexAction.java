package com.atlassian.jira;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.plugin.webwork.WebworkModuleDescriptor;
import com.atlassian.jira.web.action.ActionViewDataMappings;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugin.web.api.model.WebFragmentBuilder;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import org.apache.commons.lang.StringUtils;
import webwork.config.util.ActionInfo;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.annotation.Nullable;

public class DemoPagesIndexAction extends JiraWebActionSupport
{
    private final static String DEMOS_PLUGINKEY = "com.atlassian.jira.jira-issue-nav-demos";
    private static final String DEMOS_ITEM_SECTION = "demopages/what-does-it-really-matter";

    private final String baseUrl;

    public DemoPagesIndexAction()
    {
        this.baseUrl = getApplicationProperties().getString(APKeys.JIRA_BASEURL);
    }

    @ActionViewDataMappings ({ "success" })
    public Map<String, Object> getDataMapSuccess()
    {
        PluginAccessor pa = getComponentInstanceOfType(PluginAccessor.class);
        Collection<WebworkModuleDescriptor> modules = pa.getEnabledModuleDescriptorsByClass(WebworkModuleDescriptor.class);
        Collection<WebworkModuleDescriptor> actions = Collections2.filter(modules, new Predicate<WebworkModuleDescriptor>()
        {
            @Override
            public boolean apply(@Nullable final WebworkModuleDescriptor module)
            {
                return (null != module) && DEMOS_PLUGINKEY.equals(module.getPluginKey());
            }
        });

        Map<String,Object> actionMap = new HashMap<String, Object>();

        actionMap.put("examples", linksForActions(actionKeyContains(actions, "example")));
        actionMap.put("experiments", linksForActions(actionKeyContains(actions, "experiment")));

        return actionMap;
    }

    private Collection<WebworkModuleDescriptor> actionKeyContains(final Collection<WebworkModuleDescriptor> modules, final String... needles)
    {
        return Collections2.filter(modules, new Predicate<WebworkModuleDescriptor>()
        {
            @Override
            public boolean apply(@Nullable final WebworkModuleDescriptor module)
            {
                boolean matches = false;
                if (null != module)
                {
                    for(String needle : needles)
                    {
                        matches = StringUtils.containsIgnoreCase(module.getKey(), needle);
                        if (matches) break;
                    }
                }
                return matches;
            }
        });
    }

    private Collection<WebItem> linksForActions(final Collection<WebworkModuleDescriptor> modules)
    {
        return Collections2.transform(modules, new Function<WebworkModuleDescriptor, WebItem>()
        {
            @Override
            public WebItem apply(@Nullable final WebworkModuleDescriptor module)
            {
                WebItem item = null;
                if (module != null)
                {
                    final String url = getActionAlias(module);

                    item = new WebFragmentBuilder(0)
                            .id(module.getKey())
                            .label(module.getName())
                            .title(module.getName())
                            .webItem(DEMOS_ITEM_SECTION)
                            .url(baseUrl + "/secure/" + url + ".jspa")
                            .build();
                }
                return item;
            }
        });
    }

    /**
     * Get the alias for a webwork action. Useful for knowing what its URL might be.
     * This could be pulled up in to the WebworkModuleDescriptor class itself.
     */
    private String getActionAlias(final WebworkModuleDescriptor module)
    {
        String url = "";
        Iterator i = module.listImpl();
        while (i.hasNext())
        {
            Object o = module.getImpl(String.valueOf(i.next()));
            if (o instanceof ActionInfo)
            {
                url = ((ActionInfo)o).getActionAlias();
                break;
            }
        }
        return url;
    }

}
