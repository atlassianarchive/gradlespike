package com.atlassian.jira.components.splitview;

import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;

public class SplitViewExampleAction extends JiraWebActionSupport
{
    private final JiraWebResourceManager jiraWebResourceManager;

    public SplitViewExampleAction(JiraWebResourceManager jiraWebResourceManager)
    {
        this.jiraWebResourceManager = jiraWebResourceManager;
    }

    @Override
    public String doExecute() throws Exception
    {
        jiraWebResourceManager.requireResource("com.atlassian.jira.jira-issue-nav-components:issueviewer");
        jiraWebResourceManager.requireResource("com.atlassian.jira.jira-issue-nav-components:issueeditor");
        jiraWebResourceManager.requireResource("com.atlassian.jira.jira-issue-nav-plugin:issuenav-common"); // because split-view isn't its own resource... wat?
        jiraWebResourceManager.requireResource("com.atlassian.jira.jira-issue-nav-plugin:testutils");
        return super.doExecute();
    }

}
