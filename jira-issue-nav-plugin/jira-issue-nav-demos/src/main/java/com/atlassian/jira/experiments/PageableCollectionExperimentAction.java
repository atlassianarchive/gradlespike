package com.atlassian.jira.experiments;

import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;

public class PageableCollectionExperimentAction extends JiraWebActionSupport
{
    private final JiraWebResourceManager jiraWebResourceManager;

    public PageableCollectionExperimentAction(JiraWebResourceManager jiraWebResourceManager)
    {
        this.jiraWebResourceManager = jiraWebResourceManager;
    }

    @Override
    public String doExecute() throws Exception
    {
        jiraWebResourceManager.requireResource("com.atlassian.jira.jira-issue-nav-components:issueeditor");
        jiraWebResourceManager.requireResource("com.atlassian.jira.jira-issue-nav-plugin:issuenav");
        return super.doExecute();
    }

}
