#!/bin/bash
set -o nounset

# needed by exit_with_error()
trap "exit 1" TERM
export PID=$$

function exit_with_error() {
    kill -s TERM $PID
}

function die() {
    local msg=${1:-}
    if [ -n "$msg" ]; then
        echo "$msg" >&2
    else
        echo "Error running last command!" >&2
    fi

    exit_with_error
}

