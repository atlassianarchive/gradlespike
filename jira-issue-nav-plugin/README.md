# JIRA Issue Search Plugin

Provides the view issue and issue nav pages in JIRA.

## Structure

There's three parts:

### jira-issue-nav-components

Contains reusable bits and bobs to do various things.

### jira-issue-nav-plugin

Renders the UI of the view issue and issue navigator pages.

This is the actual plugin that JIRA makes direct use of.

### jira-issue-nav-demos

Houses demo pages for various pieces of the issue nav components and plugin.

## How to Dev it

1. Clone the code somewhere, e.g., `src/atlassian/kickass`

2. In a terminal window,
    
   1. run `cd src/atlassian/kickass`
   2. run `cd jira-issue-nav-demos`
   3. run `atlas-debug -p 1234`
   
3. PROFIT!

The jira-issue-nav-demos plugin auto-reloads resources from the other two plugins,
so once you start the demos plugin you can edit files from any resource directory
and they will automatically update when you refresh in your browser. Neat!

By default, The demos project is not included in the base project. There is a
"with-demos" profile that you will want to activate when using an IDE.
