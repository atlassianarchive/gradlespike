package com.atlassian.jira.components.issueviewer.viewissue;

import com.atlassian.jira.components.issueeditor.action.ContentIdCollector;
import com.atlassian.jira.rest.v2.common.SimpleLinkBean;
import com.atlassian.jira.rest.v2.issue.LinkGroupBean;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.components.issueviewer.viewissue.IssueOperationLinksProvider.getLinkValueForContentIdCalculation;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestIssueOperationLinksProvider
{
    @Mock
    private IssueLinkGroupsProvider issueLinkGroupsProvider;
    @Mock
    private ContentIdCollector contentIdCollector;

    private IssueOperationLinksProvider issueOperationLinksProvider;

    @Before
    public void setUp()
    {
        issueOperationLinksProvider = new IssueOperationLinksProvider(issueLinkGroupsProvider);
    }

    @Test
    public void returnEmptyIfNoLinks()
    {
        setUpIssueLinkGroupsProvider(Collections.<LinkGroupBean>emptyList());

        assertTrue(getLinkGroups().isEmpty());
    }

    @Test
    public void returnsAllLinksIfMustLoadAll()
    {
        when(contentIdCollector.mustLoadAll()).thenReturn(true);

        LinkGroupBean groupOne = new LinkGroupBean.Builder()
                .id("id1")
                .styleClass("styleClass1")
                .weight(1)
                .addLinks(new SimpleLinkBeanWithContentId(new SimpleLinkBean("link11", "linkStyleClass11", "label11", "title11", "href11", "iconClass11"), null),
                        new SimpleLinkBeanWithContentId(new SimpleLinkBean("link12", "linkStyleClass12", "label12", "title12", "href12", "iconClass12"), null))
                .build();
        LinkGroupBean groupTwo = new LinkGroupBean.Builder()
                .id("id2")
                .styleClass("styleClass2")
                .weight(1)
                .addLinks(new SimpleLinkBeanWithContentId(new SimpleLinkBean("link2", "linkStyleClass2", "label2", "title2", "href2", "iconClass2"), null),
                        new SimpleLinkBeanWithContentId(new SimpleLinkBean("link22", "linkStyleClass22", "label22", "title22", "href22", "iconClass22"), null))
                .build();
        setUpIssueLinkGroupsProvider(asList(groupOne, groupTwo));

        List<LinkGroupBean> linkGroups = getLinkGroups();
        assertEquals(2, linkGroups.size());
        assertEqualGroups(groupOne, linkGroups.get(0));
        assertEqualGroups(groupTwo, linkGroups.get(1));
    }

    @Test
    public void returnsOnlyChangedLinks()
    {
        when(contentIdCollector.getCurrentLinkContentId("link11")).thenReturn("contentId11");
        when(contentIdCollector.getCurrentLinkContentId("link12")).thenReturn("contentId12");
        when(contentIdCollector.getCurrentLinkContentId("link21")).thenReturn("contentId21");
        when(contentIdCollector.getCurrentLinkContentId("link22")).thenReturn("contentId22");

        SimpleLinkBeanWithContentId link11 = new SimpleLinkBeanWithContentId(new SimpleLinkBean("link11", "linkStyleClass11", "label11", "title11", "href11", "iconClass11"), null);
        SimpleLinkBeanWithContentId link12 = new SimpleLinkBeanWithContentId(new SimpleLinkBean("link12", "linkStyleClass12", "label12", "title12", "href12", "iconClass12"), null);
        SimpleLinkBeanWithContentId link21 = new SimpleLinkBeanWithContentId(new SimpleLinkBean("link21", "linkStyleClass2", "label2", "title2", "href2", "iconClass2"), null);
        SimpleLinkBeanWithContentId link22 = new SimpleLinkBeanWithContentId(new SimpleLinkBean("link22", "linkStyleClass22", "label22", "title22", "href22", "iconClass22"), null);

        LinkGroupBean groupOne = new LinkGroupBean.Builder()
                .id("id1")
                .styleClass("styleClass1")
                .weight(1)
                .addLinks(link11, link12)
                .build();
        LinkGroupBean groupTwo = new LinkGroupBean.Builder()
                .id("id2")
                .styleClass("styleClass2")
                .weight(1)
                .addLinks(link21, link22)
                .build();
        setUpIssueLinkGroupsProvider(asList(groupOne, groupTwo));

        String calculateFrom11 = getLinkValueForContentIdCalculation(groupOne, link11);
        String calculateFrom12 = getLinkValueForContentIdCalculation(groupOne, link12);
        String calculateFrom21 = getLinkValueForContentIdCalculation(groupTwo, link21);
        String calculateFrom22 = getLinkValueForContentIdCalculation(groupTwo, link22);
        when(contentIdCollector.calculateContentId(calculateFrom11)).thenReturn("contentId11-changed");
        when(contentIdCollector.calculateContentId(calculateFrom12)).thenReturn("contentId12");
        when(contentIdCollector.calculateContentId(calculateFrom21)).thenReturn("contentId21");
        when(contentIdCollector.calculateContentId(calculateFrom22)).thenReturn("contentId22");

        List<LinkGroupBean> linkGroups = getLinkGroups();
        assertEquals(1, linkGroups.size());

        LinkGroupBean expectedGroup = new LinkGroupBean.Builder()
                .id("id1")
                .styleClass("styleClass1")
                .weight(1)
                .addLinks(link11)
                .build();
        assertEqualGroups(expectedGroup, linkGroups.get(0));
    }

    private void assertEqualGroups(LinkGroupBean expected, LinkGroupBean actual)
    {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getStyleClass(), actual.getStyleClass());
        assertEquals(expected.getWeight(), actual.getWeight());

        assertEquals(expected.getLinks(), actual.getLinks());
    }

    private List<LinkGroupBean> getLinkGroups()
    {
        return issueOperationLinksProvider.getOperations(null, contentIdCollector).getLinkGroups();
    }

    private void setUpIssueLinkGroupsProvider(List<LinkGroupBean> toReturn)
    {
        when(issueLinkGroupsProvider.getGroups(null)).thenReturn(toReturn);
    }

    @Test
    public void testGetLinkValueForContentIdCalculation()
    {
        SimpleLinkBeanWithContentId link11 = new SimpleLinkBeanWithContentId(new SimpleLinkBean("link11", "linkStyleClass11", "label11", "title11", "href11", "iconClass11"), null);
        SimpleLinkBeanWithContentId link12 = new SimpleLinkBeanWithContentId(new SimpleLinkBean("link12", "linkStyleClass12", "label12", "title12", "href12", "iconClass12"), null);
        SimpleLinkBeanWithContentId link21 = new SimpleLinkBeanWithContentId(new SimpleLinkBean("link21", "linkStyleClass2", "label2", "title2", "href2", "iconClass2"), null);
        SimpleLinkBeanWithContentId link22 = new SimpleLinkBeanWithContentId(new SimpleLinkBean("link22", "linkStyleClass22", "label22", "title22", "href22", "iconClass22"), null);

        LinkGroupBean groupOne = new LinkGroupBean.Builder()
                .id("id1")
                .styleClass("styleClass1")
                .weight(1)
                .addLinks(link11, link12)
                .build();
        LinkGroupBean groupTwo = new LinkGroupBean.Builder()
                .id("id2")
                .styleClass("styleClass2")
                .weight(1)
                .addLinks(link21, link22)
                .build();

        // Same group, different links
        assertNotSame(getLinkValueForContentIdCalculation(groupOne, link11), getLinkValueForContentIdCalculation(groupOne, link12));
        // Same link, different groups
        assertNotSame(getLinkValueForContentIdCalculation(groupTwo, link11), getLinkValueForContentIdCalculation(groupTwo, link11));
    }
}
