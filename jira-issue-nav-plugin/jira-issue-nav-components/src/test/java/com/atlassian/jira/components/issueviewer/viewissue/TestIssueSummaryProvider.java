package com.atlassian.jira.components.issueviewer.viewissue;

import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.components.issueeditor.action.ContentIdCollector;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestIssueSummaryProvider
{
    @Mock
    private ContentIdCollector contentIdCollector;

    private MockIssue issue;

    private IssueSummaryProvider issueSummaryProvider;

    @Before
    public void setUp()
    {
        issue = new MockIssue(1L);
        issue.setSummary("test summary");

        issueSummaryProvider = new IssueSummaryProvider();
    }

    @Test
    public void returnsIfMustLoadAll()
    {
        when(contentIdCollector.mustLoadAll()).thenReturn(true);
        when(contentIdCollector.calculateContentId(issue.getSummary())).thenReturn("contentId");

        IssueSummaryProvider.Result result = issueSummaryProvider.check(issue, contentIdCollector);

        assertTrue(result.changed);
        assertEquals("contentId", result.newContentId);
    }

    @Test
    public void doesNotReturnIfNotChanged()
    {
        when(contentIdCollector.calculateContentId(issue.getSummary())).thenReturn("contentId");
        when(contentIdCollector.getCurrentIssueContentId("summary")).thenReturn("contentId");

        IssueSummaryProvider.Result result = issueSummaryProvider.check(issue, contentIdCollector);

        assertFalse(result.changed);
        assertEquals("contentId", result.newContentId);
    }

    @Test
    public void returnsIfChanged()
    {
        when(contentIdCollector.calculateContentId(issue.getSummary())).thenReturn("contentId-changed");
        when(contentIdCollector.getCurrentIssueContentId("summary")).thenReturn("contentId");

        IssueSummaryProvider.Result result = issueSummaryProvider.check(issue, contentIdCollector);

        assertTrue(result.changed);
        assertEquals("contentId-changed", result.newContentId);
    }
}
