package com.atlassian.jira.components.issueviewer.viewissue;

import com.atlassian.jira.issue.fields.rest.FieldHtmlFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.FieldHtmlBean;
import com.atlassian.jira.components.issueeditor.action.ContentIdCollector;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestIssueFieldProvider
{
    @Mock
    private FieldHtmlFactory fieldHtmlFactory;
    @Mock
    private ContentIdCollector contentIdCollector;

    private IssueFieldProvider issueFieldProvider;

    @Before
    public void setUp()
    {
        issueFieldProvider = new IssueFieldProvider(fieldHtmlFactory);
    }

    @Test
    public void returnEmptyIfNoFields()
    {
        setUpFieldHtmlFactory(Collections.<FieldHtmlBean>emptyList());

        assertTrue(getFields().isEmpty());
    }

    @Test
    public void returnsAllFieldsIfMustLoadAll()
    {
        when(contentIdCollector.mustLoadAll()).thenReturn(true);

        FieldHtmlBean fieldOne = new FieldHtmlBeanWithContentId(new FieldHtmlBean("id1", "label1", true, "html1", null), null);
        FieldHtmlBean fieldTwo = new FieldHtmlBeanWithContentId(new FieldHtmlBean("id2", "label2", false, "html2", null), null);
        setUpFieldHtmlFactory(asList(fieldOne, fieldTwo));

        List<FieldHtmlBean> fields = getFields();
        assertEquals(asList(fieldOne, fieldTwo), fields);

    }

    @Test
    public void returnsOnlyChangedFields()
    {
        when(contentIdCollector.getCurrentFieldContentId("id1")).thenReturn("contentId1");
        when(contentIdCollector.getCurrentFieldContentId("id2")).thenReturn("contentId2");

        FieldHtmlBean fieldOne = new FieldHtmlBeanWithContentId(new FieldHtmlBean("id1", "label1", true, "html1", null), null);
        FieldHtmlBean fieldTwo = new FieldHtmlBeanWithContentId(new FieldHtmlBean("id2", "label2", false, "html2", null), null);
        setUpFieldHtmlFactory(asList(fieldOne, fieldTwo));

        String calculateFromOne = issueFieldProvider.getCalculateFrom(fieldOne);
        String calculateFromTwo = issueFieldProvider.getCalculateFrom(fieldTwo);
        when(contentIdCollector.calculateContentId(calculateFromOne)).thenReturn("contentId1");
        when(contentIdCollector.calculateContentId(calculateFromTwo)).thenReturn("contentId2-changed");

        List<FieldHtmlBean> fields = getFields();
        assertEquals(asList(fieldTwo), fields);
    }

    private List<FieldHtmlBean> getFields()
    {
        return issueFieldProvider.getEditFields(null, null, null, null, false, contentIdCollector);
    }

    private void setUpFieldHtmlFactory(List<FieldHtmlBean> toReturn)
    {
        when(fieldHtmlFactory.getEditFields(null, null, null, null, false)).thenReturn(toReturn);
    }
}
