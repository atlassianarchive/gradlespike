package com.atlassian.jira.components.issueeditor.action;

import com.atlassian.jira.components.issueeditor.action.DefaultContentIdCollector;
import com.atlassian.jira.util.collect.MapBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Map;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultContentIdCollector
{
    @Mock
    private HttpServletRequest request;

    @Test
    public void mustLoadAllIfNoParams()
    {
        DefaultContentIdCollector collector = create();

        assertTrue(collector.mustLoadAll());
    }

    @Test
    public void returnsSameValuesAsInRequest()
    {
        setParameters();

        DefaultContentIdCollector collector = create();

        assertFalse(collector.mustLoadAll());
        assertEquals("fieldValue1", collector.getCurrentFieldContentId("fieldKey1"));
        assertEquals("fieldValue2", collector.getCurrentFieldContentId("fieldKey2"));
        assertNull(collector.getCurrentFieldContentId("noSuchField"));
        assertEquals("linkValue1", collector.getCurrentLinkContentId("linkKey1"));
        assertEquals("linkValue2", collector.getCurrentLinkContentId("linkKey2"));
        assertNull(collector.getCurrentLinkContentId("noSuchLink"));
        assertEquals("panelValue1", collector.getCurrentPanelContentId("panelKey1"));
        assertEquals("panelValue2", collector.getCurrentPanelContentId("panelKey2"));
        assertNull(collector.getCurrentPanelContentId("noSuchPanel"));
    }

    @Test
    public void marksAllNotAddedKeysAsRemoved()
    {
        setParameters();

        DefaultContentIdCollector collector = create();

        collector.addNewFieldKey("fieldKey1");
        collector.addNewLinkKey("linkKey1");
        collector.addNewPanelKey("panelKey1");

        Map<String, Collection<String>> expectedRemovedKeys = MapBuilder.<String, Collection<String>>newBuilder()
                .add("fields", asList("fieldKey2"))
                .add("links", asList("linkKey2"))
                .add("panels", asList("panelKey2")).toMap();

        assertEquals(expectedRemovedKeys, collector.getRemovedContentIds());
    }

    private DefaultContentIdCollector create()
    {
        return new DefaultContentIdCollector(request, null);
    }

    private void setParameters()
    {
        when(request.getParameterValues("fields")).thenReturn(new String[] {"fieldKey1:fieldValue1", "fieldKey2:fieldValue2"});
        when(request.getParameterValues("links")).thenReturn(new String[] {"linkKey1:linkValue1", "linkKey2:linkValue2"});
        when(request.getParameterValues("panels")).thenReturn(new String[]{"panelKey1:panelValue1", "panelKey2:panelValue2"});
    }
}
