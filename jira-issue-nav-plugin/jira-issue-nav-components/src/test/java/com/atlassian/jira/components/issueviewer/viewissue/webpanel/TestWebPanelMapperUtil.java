package com.atlassian.jira.components.issueviewer.viewissue.webpanel;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.util.IssueWebPanelRenderUtil;
import com.atlassian.jira.components.issueeditor.action.ContentIdCollector;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.jira.web.component.ContentRenderingInstruction;
import com.atlassian.jira.web.component.ModuleWebComponent;
import com.atlassian.jira.web.component.ModuleWebComponentFields;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestWebPanelMapperUtil
{
    @Mock
    private WebInterfaceManager webInterfaceManager;
    @Mock
    private ModuleWebComponentFields moduleWebComponentFields;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private ModuleWebComponent moduleWebComponent;
    @Mock
    private ContentRenderingInstructionProvider contentRenderingInstructionProvider;
    @Mock
    private ContentIdCollector contentIdCollector;
    @Mock
    private Issue issue;
    @Mock
    private HttpServletRequest httpServletRequest;
    @Mock
    private WebPanelModuleDescriptor first;
    @Mock
    private WebPanelModuleDescriptor second;
    @Mock
    private WebPanelModuleDescriptor third;
    @Mock
    private WebPanelModuleDescriptor fourth;
    @Mock
    private WebPanelModuleDescriptor fifth;
    @Mock
    private WebPanelModuleDescriptor sixth;
    @Mock
    private WebPanelModuleDescriptor seventh;

    @Mock
    private HttpServletResponse httpServletResponse;
    private WebPanelMapperUtilImpl webPanelMapperUtil;

    private IssueWebPanelRenderUtil issueWebPanelRenderUtil;

    @Before
    @SuppressWarnings("unchecked")
    public void setUp()
    {
        webPanelMapperUtil = new WebPanelMapperUtilImpl(webInterfaceManager, moduleWebComponentFields,
                authenticationContext, moduleWebComponent, contentRenderingInstructionProvider);

        ExecutingHttpRequest.set(httpServletRequest, httpServletResponse);
        when(httpServletRequest.getHeader("X-PJAX")).thenReturn("true");

        issueWebPanelRenderUtil = new IssueWebPanelRenderUtil(authenticationContext.getLoggedInUser(),
                issue, null, this.webInterfaceManager, this.moduleWebComponent);

        when(first.getKey()).thenReturn("id1");
        when(first.getCompleteKey()).thenReturn("plugin:id1");
        when(second.getKey()).thenReturn("id2");
        when(second.getCompleteKey()).thenReturn("plugin:id2");
        when(third.getKey()).thenReturn("id3");
        when(third.getCompleteKey()).thenReturn("plugin:id3");
        when(fourth.getKey()).thenReturn("id4");
        when(fourth.getCompleteKey()).thenReturn("plugin:id4");
        when(fifth.getKey()).thenReturn("id5");
        when(fifth.getCompleteKey()).thenReturn("plugin:id5");
        when(sixth.getKey()).thenReturn("id6");
        when(sixth.getCompleteKey()).thenReturn("plugin:id6");
        when(seventh.getKey()).thenReturn("id7");
        when(seventh.getCompleteKey()).thenReturn("plugin:id7");

        when(moduleWebComponent.renderModule(eq(authenticationContext.getLoggedInUser()), eq(ExecutingHttpRequest.get()), eq(first), anyMap())).thenReturn("html1");
        when(moduleWebComponent.renderModule(eq(authenticationContext.getLoggedInUser()), eq(ExecutingHttpRequest.get()), eq(second), anyMap())).thenReturn("html2");
        when(moduleWebComponent.renderModule(eq(authenticationContext.getLoggedInUser()), eq(ExecutingHttpRequest.get()), eq(third), anyMap())).thenReturn("html3");
        when(moduleWebComponent.renderModule(eq(authenticationContext.getLoggedInUser()), eq(ExecutingHttpRequest.get()), eq(fourth), anyMap())).thenReturn("html4");
        when(moduleWebComponent.renderModule(eq(authenticationContext.getLoggedInUser()), eq(ExecutingHttpRequest.get()), eq(fifth), anyMap())).thenReturn("html5");
        when(moduleWebComponent.renderModule(eq(authenticationContext.getLoggedInUser()), eq(ExecutingHttpRequest.get()), eq(sixth), anyMap())).thenReturn("html6");
        when(moduleWebComponent.renderModule(eq(authenticationContext.getLoggedInUser()), eq(ExecutingHttpRequest.get()), eq(seventh), anyMap())).thenReturn("html7");
    }

    @Test
    public void returnsAllIfNoContentIdCollector()
    {
        List<WebPanelBean> result = webPanelMapperUtil.mapAndRenderPanels(issueWebPanelRenderUtil, asList(first, second), null);

        WebPanelBean expectedFirst = new WebPanelBean.Builder()
                .id("id1")
                .completeKey("plugin:id1")
                .prefix("")
                .label("id1")
                .styleClass("")
                .renderHeader(true)
                .weight(0)
                .html("html1")
                .build();
        WebPanelBean expectedSecond = new WebPanelBean.Builder().id("id2")
                .completeKey("plugin:id2")
                .prefix("")
                .label("id2")
                .styleClass("")
                .renderHeader(true)
                .weight(0)
                .html("html2")
                .build();

        assertEquals(asList(expectedFirst, expectedSecond), result);
    }

    @Test
    public void returnsAllIfMustLoadAll()
    {
        when(contentIdCollector.mustLoadAll()).thenReturn(true);

        List<WebPanelBean> result = webPanelMapperUtil.mapAndRenderPanels(issueWebPanelRenderUtil, asList(first, second), contentIdCollector);

        WebPanelBean expectedFirst = new WebPanelBean.Builder()
                .id("id1")
                .completeKey("plugin:id1")
                .prefix("")
                .label("id1")
                .styleClass("")
                .renderHeader(true)
                .weight(0)
                .html("html1")
                .build();
        WebPanelBean expectedSecond = new WebPanelBean.Builder().id("id2")
                .completeKey("plugin:id2")
                .prefix("")
                .label("id2")
                .styleClass("")
                .renderHeader(true)
                .weight(0)
                .html("html2")
                .build();

        assertEquals(asList(expectedFirst, expectedSecond), result);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void returnsOnlyChanged()
    {
        when(contentIdCollector.getCurrentPanelContentId("plugin:id1")).thenReturn("contentId1");
        when(contentIdCollector.getCurrentPanelContentId("plugin:id2")).thenReturn("contentId2");
        when(contentIdCollector.getCurrentPanelContentId("plugin:id3")).thenReturn("contentId3");
        when(contentIdCollector.getCurrentPanelContentId("plugin:id4")).thenReturn("contentId4");
        when(contentIdCollector.getCurrentPanelContentId("plugin:id5")).thenReturn("contentId5");
        when(contentIdCollector.getCurrentPanelContentId("plugin:id6")).thenReturn("contentId6");

        when(contentRenderingInstructionProvider.get(anyMap(), eq(third))).thenReturn(ContentRenderingInstruction.dontRender());
        when(contentRenderingInstructionProvider.get(anyMap(), eq(fourth))).thenReturn(ContentRenderingInstruction.renderHtml());
        when(contentRenderingInstructionProvider.get(anyMap(), eq(fifth))).thenReturn(ContentRenderingInstruction.renderHtml());
        when(contentRenderingInstructionProvider.get(anyMap(), eq(sixth))).thenReturn(ContentRenderingInstruction.customContentId("custom6"));
        when(contentRenderingInstructionProvider.get(anyMap(), eq(seventh))).thenReturn(ContentRenderingInstruction.customContentId("custom7"));

        WebPanelBean beanOne = webPanelMapperUtil.getWebPanelBeanBuilder(issueWebPanelRenderUtil, first).buildWithoutHtml();
        WebPanelBean beanTwo = webPanelMapperUtil.getWebPanelBeanBuilder(issueWebPanelRenderUtil, second).buildWithoutHtml();
        WebPanelBean beanFour = webPanelMapperUtil.getWebPanelBeanBuilder(issueWebPanelRenderUtil, fourth).buildWithoutHtml();
        WebPanelBean beanFive = webPanelMapperUtil.getWebPanelBeanBuilder(issueWebPanelRenderUtil, fifth).buildWithoutHtml();
        WebPanelBean beanSix = webPanelMapperUtil.getWebPanelBeanBuilder(issueWebPanelRenderUtil, sixth).buildWithoutHtml();
        WebPanelBean beanSeven = webPanelMapperUtil.getWebPanelBeanBuilder(issueWebPanelRenderUtil, seventh).buildWithoutHtml();

        String calculateFromOne = webPanelMapperUtil.getCalculateFrom("html1", beanOne);
        String calculateFromTwo = webPanelMapperUtil.getCalculateFrom("html2", beanTwo);
        String calculateFromFour = webPanelMapperUtil.getCalculateFrom("html4", beanFour);
        String calculateFromFive = webPanelMapperUtil.getCalculateFrom("html5", beanFive);
        String calculateFromSix = webPanelMapperUtil.getCalculateFrom("custom6", beanSix);
        String calculateFromSeven = webPanelMapperUtil.getCalculateFrom("custom7", beanSeven);
        when(contentIdCollector.calculateContentId(calculateFromOne)).thenReturn("contentId1-changed");
        when(contentIdCollector.calculateContentId(calculateFromTwo)).thenReturn("contentId2");
        when(contentIdCollector.calculateContentId(calculateFromFour)).thenReturn("contentId4-changed");
        when(contentIdCollector.calculateContentId(calculateFromFive)).thenReturn("contentId5");
        when(contentIdCollector.calculateContentId(calculateFromSix)).thenReturn("contentId6");
        when(contentIdCollector.calculateContentId(calculateFromSeven)).thenReturn("contentId7-changed");

        List<WebPanelBean> result = webPanelMapperUtil.mapAndRenderPanels(issueWebPanelRenderUtil, asList(first, second, third, fourth, fifth, sixth, seventh), contentIdCollector);

        WebPanelBean expectedOne = new WebPanelBean.Builder()
                .id("id1")
                .completeKey("plugin:id1")
                .prefix("")
                .label("id1")
                .styleClass("")
                .renderHeader(true)
                .weight(0)
                .html("html1")
                .build();
        WebPanelBean expectedFour = new WebPanelBean.Builder()
                .id("id4")
                .completeKey("plugin:id4")
                .prefix("")
                .label("id4")
                .styleClass("")
                .renderHeader(true)
                .weight(0)
                .html("html4")
                .build();
        WebPanelBean expectedSeven = new WebPanelBean.Builder()
                .id("id7")
                .completeKey("plugin:id7")
                .prefix("")
                .label("id7")
                .styleClass("")
                .renderHeader(true)
                .weight(0)
                .html("html7")
                .build();

        assertEquals(asList(expectedOne, expectedFour, expectedSeven), result);
    }
}
