package com.atlassian.jira.components.issueviewer.service;

import com.atlassian.jira.issue.pager.PagerManager;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.web.bean.PagerFilter;

/**
 * Utility for working with session search.
 *
 * @since v5.2
 */
public interface SessionSearchService
{
    /**
     * @return the {@link PagerFilter} from the current user's session.
     */
    public PagerFilter getPagerFilter();

    /**
     * @return the {@link PagerManager} used to access next/previous issues.
     */
    public PagerManager getPagerManager();

    /**
     * Retrieves the current session search request.
     * <p/>
     * Must be called from within a servlet request.
     *
     * @return the current session search request or {@code null}.
     */
    public SearchRequest getSessionSearch();

    /**
     * Stores a search request in the session.
     *
     * If a filter is requested and the user has permission to access it, it
     * is retrieved from the database and is used as the basis for the session
     * search. The session search JQL is then set to the given JQL (if any).
     *
     * If no filter is requested, the given JQL is simply used.
     *
     * @param jql The current jql being requested.
     * @param filterId The ID of the filter (or null if there is no filter)
     * @param start pager start index
     * @param max results to display
     */
    public void setSessionSearch(String jql, Long filterId, int start, int max);
}
