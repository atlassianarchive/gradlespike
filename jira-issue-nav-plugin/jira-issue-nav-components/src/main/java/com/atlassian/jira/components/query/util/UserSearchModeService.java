package com.atlassian.jira.components.query.util;

public interface UserSearchModeService {
    String ADVANCED = "advanced";
    String BASIC = "basic";

    String getSearchMode();

    void setSearchMode(String searchMode);
}
