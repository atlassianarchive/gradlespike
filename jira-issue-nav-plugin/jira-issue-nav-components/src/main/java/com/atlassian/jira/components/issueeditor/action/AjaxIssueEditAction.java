package com.atlassian.jira.components.issueeditor.action;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.components.issueviewer.viewissue.IssueFieldProvider;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.rest.json.beans.FieldHtmlBean;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.plugins.rest.common.json.DefaultJaxbJsonMarshaller;
import com.atlassian.plugins.rest.common.json.JaxbJsonMarshaller;
import org.apache.commons.httpclient.HttpStatus;
import webwork.action.ActionContext;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * A webwork action to produce JSON with field edit html.  This is an action and not a REST resource mainly because our
 * fields API is still so heavily tied to webwork.  All methods on this action should return JSON content.
 *
 * @since 5.0
 */
public class AjaxIssueEditAction extends BaseEditAction
{
    private EditFields fields;

    public AjaxIssueEditAction(final IssueService issueService, IssueFieldProvider issueFieldProvider)
    {
        super(issueService, issueFieldProvider);
    }

    public String doDefault() throws Exception
    {
        ActionContext.getResponse().setContentType("application/json");

        final IssueService.IssueResult result = issueService.getIssue(getLoggedInUser(), issueId);
        final Issue issue = result.getIssue();
        if (!result.isValid() || result.getIssue() == null)
        {
            this.fields = new EditFields(Collections.<FieldHtmlBean>emptyList(), getXsrfToken(),
                    ErrorCollection.of(result.getErrorCollection()));
            setErrorReturnCode(result, getLoggedInUser());
            return JSON();
        }

        if (!issueService.isEditable(issue, getLoggedInUser()))
        {
            this.fields = new EditFields(Collections.<FieldHtmlBean>emptyList(), getXsrfToken(),
                    ErrorCollection.of(getText("editissue.error.no.edit.permission")));
            ActionContext.getResponse().setStatus(HttpStatus.SC_UNAUTHORIZED);
            return JSON();
        }

        ContentIdCollector contentIdCollector = createContentIdCollector();

        List<FieldHtmlBean> editFields = issueFieldProvider.getEditFields(getLoggedInUser(), this, this, issue, false, contentIdCollector);
        fields = new EditFields(editFields, getXsrfToken(), ErrorCollection.of(result.getErrorCollection()));

        return JSON();
    }


    private String JSON() throws IOException
    {
        ActionContext.getResponse().getWriter().append(getJson());
        return null;
    }

    private static void setErrorReturnCode(IssueService.IssueResult result, User user)
    {
        if (user == null || result.getErrorCollection().getReasons().contains(Reason.FORBIDDEN))
        {
            ActionContext.getResponse().setStatus(HttpStatus.SC_UNAUTHORIZED);
        }
        else
        {
            ActionUtils.setErrorReturnCode(user);
        }
    }

    public String getJson()
    {
        final JaxbJsonMarshaller marshaller = new DefaultJaxbJsonMarshaller();
        return marshaller.marshal(fields);
    }
}
