package com.atlassian.jira.components.query.rest;

/**
 * TODO: Document this class / interface here
 *
 * @since v5.2
 */

import com.atlassian.jira.components.query.util.UserSearchModeService;
import com.atlassian.jira.rest.api.http.CacheControl;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.RequiresXsrfCheck;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@AnonymousAllowed
@Path ("userSearchMode")
public class UserSearchModeResource
{
    private final UserSearchModeService userSearchModeService;

    public UserSearchModeResource(final UserSearchModeService userSearchModeService)
    {
        this.userSearchModeService = userSearchModeService;
    }

    /**
     * Sets the search mode, must be {@code UserSearchModeService.ADVANCED} or
     * {@code UserSearchModeService.BASIC} and returns the latest search mode.
     *
     * @param searchMode The search mode to be set.
     * @return {@code 200 OK} if the request executed successfully,
     *         {@code 400 Bad Request} if {@code searchMode} is not valid.
     */
    @POST
    @RequiresXsrfCheck
    public Response set(@FormParam ("searchMode") String searchMode)
    {
        userSearchModeService.setSearchMode(searchMode);
        return get();
    }

    /**
     * Gets the user's search mode preference.
     */
    @GET
    public Response get()
    {
        String searchMode = userSearchModeService.getSearchMode();
        return Response.ok(searchMode).cacheControl(CacheControl.never()).build();
    }
}
