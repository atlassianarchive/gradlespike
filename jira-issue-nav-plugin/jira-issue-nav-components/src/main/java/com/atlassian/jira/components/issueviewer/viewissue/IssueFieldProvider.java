package com.atlassian.jira.components.issueviewer.viewissue;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.components.issueeditor.action.ContentIdCollector;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.fields.rest.FieldHtmlFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.FieldHtmlBean;
import com.google.common.collect.Lists;
import webwork.action.Action;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collections;
import java.util.List;

@Named
public class IssueFieldProvider
{
    private final FieldHtmlFactory fieldHtmlFactory;

    @Inject
    public IssueFieldProvider(FieldHtmlFactory fieldHtmlFactory)
    {
        this.fieldHtmlFactory = fieldHtmlFactory;
    }

    public List<FieldHtmlBean> getEditFields(User user, OperationContext operationContext,
                                      Action action, Issue issue, boolean retainValues,
                                      ContentIdCollector contentIdCollector)
    {
        List<FieldHtmlBean> fields = fieldHtmlFactory.getEditFields(user, operationContext, action, issue, retainValues);

        if (fields.isEmpty())
        {
            return Collections.emptyList();
        }

        List<FieldHtmlBean> toReturn = Lists.newArrayList();

        boolean loadAll = contentIdCollector.mustLoadAll();

        for (FieldHtmlBean field : fields)
        {
            String currentContentId = contentIdCollector.getCurrentFieldContentId(field.getId());

            String calculateFrom = getCalculateFrom(field);
            String newContentId = contentIdCollector.calculateContentId(calculateFrom);

            if (loadAll || !newContentId.equals(currentContentId))
            {
                toReturn.add(new FieldHtmlBeanWithContentId(field, newContentId));
            }

            contentIdCollector.addNewFieldKey(field.getId());
        }

        return toReturn;
    }

    String getCalculateFrom(FieldHtmlBean field)
    {
        StringBuilder calculateFrom = new StringBuilder(field.getEditHtml())
                .append(field.getLabel())
                .append(field.isRequired());
        if (field.getTab() != null)
        {
            calculateFrom.append(field.getTab().getLabel())
                    .append(field.getTab().getPosition());
        }

        return calculateFrom.toString();
    }
}
