package com.atlassian.jira.components.query.util;

/**
 * TODO: Document this class / interface here
 *
 * @since v5.2
 */

import com.atlassian.core.AtlassianCoreException;
import com.atlassian.core.user.preferences.Preferences;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.util.velocity.VelocityRequestSession;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Manages getting and setting user search modes.
 * <p/>
 * A user's preferred search mode is stored in both their session and preferences. This stores anonymous users'
 * preferences for their session.
 *
 * @since v5.2
 */
@ExportAsService
@Named
public class DefaultUserSearchModeService implements UserSearchModeService {
    private static final Logger log = Logger.getLogger(
            DefaultUserSearchModeService.class);

    private static final String SEARCH_MODE_KEY = "user.search.mode";

    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final UserPreferencesManager userPreferencesManager;
    private final VelocityRequestContextFactory velocityRequestContextFactory;

    @Inject
    public DefaultUserSearchModeService(
            final JiraAuthenticationContext jiraAuthenticationContext,
            final UserPreferencesManager userPreferencesManager,
            final VelocityRequestContextFactory velocityRequestContextFactory)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.userPreferencesManager = userPreferencesManager;
        this.velocityRequestContextFactory = velocityRequestContextFactory;
    }

    /**
     * @return the current user's search mode or {@link #BASIC} if it's not set.
     */
    @Override
    public String getSearchMode()
    {
        // First, try to get it out of the session (handles anonymous users).
        String searchMode = (String) getSession().getAttribute(SEARCH_MODE_KEY);
        if (searchMode != null)
        {
            return searchMode;
        }

        // Not in the session? Try in user preferences.
        final Preferences preferences = getPreferences();
        if (preferences != null)
        {
            searchMode = preferences.getString(SEARCH_MODE_KEY);
            if (searchMode != null)
            {
                return searchMode;
            }
        }

        return BASIC;
    }

    /**
     * Set the current user's search mode.
     *
     * @param searchMode The search mode.
     * @throws IllegalArgumentException if {@code searchMode} isn't either {@link #ADVANCED} or {@link #BASIC}.
     */
    @Override
    public void setSearchMode(final String searchMode)
    {
        if (!searchMode.equals(ADVANCED) && !searchMode.equals(BASIC))
        {
            throw new IllegalArgumentException(searchMode +
                    "is not a valid search mode.");
        }

        // Store it in the session (handles anonymous users)...
        getSession().setAttribute(SEARCH_MODE_KEY, searchMode);

        // ...and in user preferences.
        Preferences preferences = getPreferences();
        if (preferences != null)
        {
            try
            {
                preferences.setString(SEARCH_MODE_KEY, searchMode);
            }
            catch (AtlassianCoreException e)
            {
                log.warn("Couldn't store a user's preferred search mode in user preferences.");
            }
        }
    }

    /**
     * @return the current user's preferences or {@code null} if anonymous.
     */
    private Preferences getPreferences()
    {
        final User user = jiraAuthenticationContext.getLoggedInUser();
        if (user != null)
        {
            return userPreferencesManager.getPreferences(user);
        }
        else
        {
            return null;
        }
    }

    /**
     * @return the current user's session.
     */
    private VelocityRequestSession getSession()
    {
        return velocityRequestContextFactory.getJiraVelocityRequestContext().getSession();
    }
}
