package com.atlassian.jira.components.issueviewer.action;

import com.atlassian.jira.components.issueeditor.action.EditFields;
import com.atlassian.jira.issue.fields.rest.json.beans.FieldHtmlBean;
import com.atlassian.jira.components.issueviewer.viewissue.webpanel.IssueWebPanelsBean;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Contains all information needed to render the view issue page with inline
 * edit, including: fields, issue details, pager information, and web panels.
 *
 * @since v5.0.3
 */
@XmlRootElement
public class IssueFields extends EditFields
{
    @XmlElement private IssueBean issue;
    @XmlElement private IssuePager pager;
    @XmlElement private IssueWebPanelsBean panels;
    @XmlElement private Map<String, Collection<String>> removedContentIds;
    @XmlElement private long readTime;

    private IssueFields() {}

    public IssueFields(
            final String atlToken,
            final ErrorCollection errorCollection)
    {
        super(atlToken, errorCollection);
    }

    public IssueFields(
            final String atlToken,
            final ErrorCollection errorCollection,
            final List<FieldHtmlBean> fields,
            final IssueBean issue,
            final IssuePager issuePager,
            final IssueWebPanelsBean panels,
            final Map<String, Collection<String>> removedContentIds)
    {
        super(fields, atlToken, errorCollection);
        this.issue = issue;
        this.pager = issuePager;
        this.panels = panels;
        this.removedContentIds = removedContentIds;
        readTime = System.currentTimeMillis();
    }

    public IssueBean getIssue()
    {
        return issue;
    }

    public IssueWebPanelsBean getPanels()
    {
        return panels;
    }

    public Map<String, Collection<String>> getRemovedContentIds()
    {
        return removedContentIds;
    }

    public long getReadTime()
    {
        return readTime;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this).
                append("fields", getFields()).
                append("errors", getErrorCollection()).
                append("issue", issue).
                append("panels", panels).
                append("pager", pager).
                append("removedContentIds", removedContentIds).
                append("readTime", readTime).
                toString();
    }
}