package com.atlassian.jira.components.util;

import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.util.ErrorCollection;

public interface FilterLookerUpper {
    SearchRequest getSearchRequestFromFilterId(String filterId, ErrorCollection errors);
}
