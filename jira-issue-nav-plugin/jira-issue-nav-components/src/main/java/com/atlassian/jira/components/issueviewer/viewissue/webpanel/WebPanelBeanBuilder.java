package com.atlassian.jira.components.issueviewer.viewissue.webpanel;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.util.IssueWebPanelRenderUtil;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.SimpleLink;
import com.atlassian.jira.plugin.webfragment.model.SimpleLinkSection;
import com.atlassian.jira.rest.v2.common.SimpleLinkBean;
import com.atlassian.jira.rest.v2.issue.LinkGroupBean;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.jira.web.component.ModuleWebComponentFields;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.atlassian.plugin.web.model.WebLabel;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.web.component.ModuleWebComponentFields.*;

/**
 * Builder to create {@link WebPanelBean}.
 *
 * @since v5.0
 */
public class WebPanelBeanBuilder
{
    private final ModuleWebComponentFields moduleWebComponentFields;
    private final IssueWebPanelRenderUtil renderUtil;
    private final I18nHelper i18n;
    private final User user;
    private final WebPanelModuleDescriptor panel;

    public WebPanelBeanBuilder(ModuleWebComponentFields moduleWebComponentFields,
                               IssueWebPanelRenderUtil renderUtil, I18nHelper i18n, User user,
                               WebPanelModuleDescriptor panel)
    {
        this.moduleWebComponentFields = moduleWebComponentFields;
        this.renderUtil = renderUtil;
        this.i18n = i18n;
        this.user = user;
        this.panel = panel;
    }

    public WebPanelBean buildWithHtml()
    {
        String panelHtml = renderUtil.renderHeadlessPanel(panel);

        if (StringUtils.isNotBlank(panelHtml))
        {
            return builder().html(panelHtml).build();
        }

        return null;
    }

    public WebPanelBean buildWithoutHtml()
    {
        return builder().build();
    }

    private WebPanelBean.Builder builder()
    {
        Map<String, String> moduleParams = panel.getParams();

        boolean headless = moduleParams.containsKey(RENDER_PARAM_HEADLESS);
        String styleClass = moduleParams.containsKey(RENDER_PARAM_CONTAINER_CLASS) ? moduleParams.get(RENDER_PARAM_CONTAINER_CLASS) : "";
        return new WebPanelBean.Builder()
                .completeKey(panel.getCompleteKey())
                .prefix(getPrefix())
                .id(panel.getKey())
                .styleClass(styleClass)
                .label(getLabel())
                .renderHeader(!headless)
                .headerLinks(getHeaderLinks())
                .subpanelHtmls(getSubpanelHtmls())
                .weight(panel.getWeight());
    }

    private String getLabel()
    {
        try
        {
            final WebLabel webLabel = panel.getWebLabel();
            if (webLabel != null)
            {
                return webLabel.getDisplayableLabel(ExecutingHttpRequest.get(), renderUtil.getWebPanelContext());
            }
        }
        catch (Throwable t)
        {
            //ignore the exception and use fallbacks below.
        }

        if (panel.getI18nNameKey() != null)
        {
            return i18n.getText(panel.getI18nNameKey());
        }
        else
        {
            return panel.getKey();
        }
    }

    private List<String> getSubpanelHtmls()
    {
        final List<String> ret = new ArrayList<String>();
        final List<WebPanelModuleDescriptor> panels = moduleWebComponentFields.getPanels(panel.getCompleteKey(), renderUtil.getWebPanelContext());
        if (!panels.isEmpty())
        {
            for (WebPanelModuleDescriptor panel : panels)
            {
                String panelHtml = panel.getModule().getHtml(renderUtil.getWebPanelContext());
                if (StringUtils.isNotBlank(panelHtml))
                {
                    ret.add(panelHtml);
                }
            }
        }

        return ret;
    }

    private LinkGroupBean getHeaderLinks()
    {
        final List<SimpleLink> headerItems = moduleWebComponentFields.getHeaderItems(panel.getCompleteKey(), user, getHelper());
        final List<LinkGroupBean> dropdownGroups = getGroups(panel.getCompleteKey());

        return new LinkGroupBean.Builder()
                .addLinks(toBeans(headerItems))
                .addGroups(dropdownGroups)
                .build();
    }


    /*
    * Get the sections and links for the dropdown
    */
    private List<LinkGroupBean> getGroups(String key)
    {
        return Lists.transform(moduleWebComponentFields.getDropdownSections(key, user, getHelper()), new Function<SectionsAndLinks, LinkGroupBean>()
        {
            @Override
            public LinkGroupBean apply(SectionsAndLinks o)
            {
                return new LinkGroupBean.Builder()
                        .header(toBean(o.getSection()))
                        .addLinks(toBeans(o.getLinks()))
                        .build();
            }
        });
    }

    private SimpleLinkBean toBean(final SimpleLinkSection simpleLinkSection)
    {
        return new SimpleLinkBean(simpleLinkSection.getId(), simpleLinkSection.getStyleClass(), simpleLinkSection.getLabel(),
                simpleLinkSection.getTitle(), null, simpleLinkSection.getStyleClass(), simpleLinkSection.getWeight());
    }

    private List<SimpleLinkBean> toBeans(List<SimpleLink> input)
    {
        final List<SimpleLinkBean> ret = new ArrayList<SimpleLinkBean>();
        for (SimpleLink simpleLink : input)
        {
            ret.add(new SimpleLinkBean(simpleLink));
        }
        return ret;
    }

    private String getPrefix()
    {
        final String prefix = panel.getParams().get(RENDER_PARAM_PREFIX);
        return (prefix == null) ? "" : prefix;
    }

    public JiraHelper getHelper()
    {
        return (JiraHelper) renderUtil.getWebPanelContext().get("helper");
    }
}
