package com.atlassian.jira.components.query;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.search.searchers.IssueSearcher;

import java.util.Map;

public interface RecentSearchersService {
    Map<String, Long> getRecentSearchers(User user);
    void addRecentSearcher(User user, IssueSearcher<?> issueSearcher);
}
