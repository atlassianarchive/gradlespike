package com.atlassian.jira.components.query;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Searcher
 * @since v5.0
 */
@XmlRootElement
public class Searcher
{
    @XmlElement
    private String name;
    @XmlElement
    private String id;
    @XmlElement
    private String key;
    @XmlElement
    private Boolean isShown;
    @XmlElement
    private Long lastViewed;

    public Searcher()
    {
    }

    public Searcher(String id, String name, String key, Boolean isShown, Long lastViewed)
    {
        this.name = name;
        this.id = id;
        this.key = key;
        this.isShown = isShown;
        this.lastViewed = lastViewed;
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getKey()
    {
        return key;
    }

    public Boolean getShown()
    {
        return isShown;
    }

    public Long getLastViewed()
    {
        return lastViewed;
    }
}