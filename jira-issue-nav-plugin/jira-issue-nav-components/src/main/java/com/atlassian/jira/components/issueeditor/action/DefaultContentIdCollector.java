package com.atlassian.jira.components.issueeditor.action;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Map;

public class DefaultContentIdCollector implements ContentIdCollector
{
    private final Long lastReadTime;

    private final Map<String, Map<String, String>> currentContentIds;
    private final Map<String, Collection<String>> newKeys;

    DefaultContentIdCollector(HttpServletRequest request, Long lastReadTime)
    {
        this.lastReadTime = lastReadTime;

        Map<String, Map<String, String>> contentIdMap = Maps.newHashMap();

        populateCurrentMappingForCategory("issue", contentIdMap, request);
        populateCurrentMappingForCategory("fields", contentIdMap, request);
        populateCurrentMappingForCategory("links", contentIdMap, request);
        populateCurrentMappingForCategory("panels", contentIdMap, request);

        currentContentIds = contentIdMap.isEmpty() ? null : contentIdMap;

        newKeys = Maps.newHashMap();
    }

    private static void populateCurrentMappingForCategory(String category, Map<String, Map<String, String>> contentIdMap, HttpServletRequest request)
    {
        Map<String, String> mapping = Maps.newHashMap();

        String[] fieldsParams = request.getParameterValues(category);
        if (fieldsParams != null)
        {
            for (String value : fieldsParams)
            {
                int lastSeparatorIndex = value.lastIndexOf(":");
                String name = value.substring(0, lastSeparatorIndex);
                String contentId = value.substring(lastSeparatorIndex + 1);

                mapping.put(name, contentId);
            }
        }

        if (!mapping.isEmpty())
        {
            contentIdMap.put(category, mapping);
        }
    }

    public String calculateContentId(String calculateFrom)
    {
        return DigestUtils.md5Hex(calculateFrom);
    }

    public boolean mustLoadAll()
    {
        return currentContentIds == null;
    }

    public String getCurrentFieldContentId(String key)
    {
        return getCurrentContentId("fields", key);
    }

    public String getCurrentLinkContentId(String key)
    {
        return getCurrentContentId("links", key);
    }

    public String getCurrentPanelContentId(String key)
    {
        return getCurrentContentId("panels", key);
    }

    @Override
    public String getCurrentIssueContentId(String key)
    {
        return getCurrentContentId("issue", key);
    }

    private String getCurrentContentId(String category, String key)
    {
        if (currentContentIds == null)
        {
            return null;
        }

        Map<String, String> categoryMapping = currentContentIds.get(category);
        if (categoryMapping == null)
        {
            return null;
        }

        return categoryMapping.get(key);
    }

    public void addNewFieldKey(String key)
    {
        addNewKey("fields", key);
    }

    public void addNewLinkKey(String key)
    {
        addNewKey("links", key);
    }

    public void addNewPanelKey(String key)
    {
        addNewKey("panels", key);
    }

    private void addNewKey(String category, String key)
    {
        Collection<String> keys = newKeys.get(category);
        if (keys == null)
        {
            keys = Lists.newArrayList();
            newKeys.put(category, keys);
        }
        keys.add(key);
    }

    public Map<String, Collection<String>> getRemovedContentIds()
    {
        Map<String, Collection<String>> removedKeys = Maps.newHashMap();

        if (currentContentIds != null)
        {
            for (Map.Entry<String, Map<String, String>> currentEntry : currentContentIds.entrySet())
            {
                String category = currentEntry.getKey();
                if ("issue".equals(category))
                {
                    continue;
                }

                Map<String, String> mapping = currentEntry.getValue();

                Collection<String> keys = Lists.newArrayList(mapping.keySet());

                Collection<String> newKeysForCategory = newKeys.get(category);
                if (newKeysForCategory != null)
                {
                    keys.removeAll(newKeysForCategory);
                }

                removedKeys.put(category, keys);
            }
        }

        return removedKeys;
    }

    public Long getLastReadTime()
    {
        return lastReadTime;
    }
}
