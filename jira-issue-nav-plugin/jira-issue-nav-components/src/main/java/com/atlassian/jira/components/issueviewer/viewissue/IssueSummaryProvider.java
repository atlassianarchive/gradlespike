package com.atlassian.jira.components.issueviewer.viewissue;

import com.atlassian.jira.components.issueeditor.action.ContentIdCollector;
import com.atlassian.jira.issue.Issue;

import javax.inject.Named;

@Named
public class IssueSummaryProvider
{
    public Result check(Issue issue, ContentIdCollector contentIdCollector)
    {
        if (contentIdCollector == null)
        {
            return new Result(null, true);
        }

        String newContentId = contentIdCollector.calculateContentId(issue.getSummary());

        if (contentIdCollector.mustLoadAll())
        {
            return new Result(newContentId, true);
        }

        String currentContentId = contentIdCollector.getCurrentIssueContentId("summary");

        return new Result(newContentId, !newContentId.equals(currentContentId));
    }

    public static class Result
    {
        public final String newContentId;
        public final boolean changed;

        public Result(String newContentId, boolean changed)
        {
            this.newContentId = newContentId;
            this.changed = changed;
        }
    }
}
