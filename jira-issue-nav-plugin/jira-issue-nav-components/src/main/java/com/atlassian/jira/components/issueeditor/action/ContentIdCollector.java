package com.atlassian.jira.components.issueeditor.action;

import java.util.Collection;
import java.util.Map;

public interface ContentIdCollector
{
    String calculateContentId(String calculateFrom);

    boolean mustLoadAll();

    String getCurrentFieldContentId(String key);

    String getCurrentLinkContentId(String key);

    String getCurrentPanelContentId(String key);

    String getCurrentIssueContentId(String key);

    void addNewFieldKey(String key);

    void addNewLinkKey(String key);

    void addNewPanelKey(String key);

    Map<String, Collection<String>> getRemovedContentIds();

    Long getLastReadTime();
}
