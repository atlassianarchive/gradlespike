package com.atlassian.jira.components.issueviewer.viewissue.webpanel;

import com.atlassian.jira.web.component.ContentRenderingInstruction;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;

import java.util.Map;

public interface ContentRenderingInstructionProvider
{
    ContentRenderingInstruction get(Map<String, Object> context, WebPanelModuleDescriptor panel);
}
