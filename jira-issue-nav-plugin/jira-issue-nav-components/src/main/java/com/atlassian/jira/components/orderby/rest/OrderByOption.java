package com.atlassian.jira.components.orderby.rest;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * A single option that the user can click to sort.
 */
@JsonSerialize
public class OrderByOption
{
    public String fieldId;
    public String fieldName;
    public String sortJql;

    public OrderByOption()
    {
    }

    public OrderByOption fieldId(String fieldId)
    {
        this.fieldId = fieldId;
        return this;
    }

    public OrderByOption fieldName(String fieldName)
    {
        this.fieldName = fieldName;
        return this;
    }

    public OrderByOption sortJql(String sortJql)
    {
        this.sortJql = sortJql;
        return this;
    }
}
