package com.atlassian.jira.components.issueviewer.viewissue.webpanel;

import com.atlassian.jira.plugin.PluginInjector;
import com.atlassian.jira.web.component.ContentRenderingInstruction;
import com.atlassian.jira.web.component.ContentRenderingInstructionsProvider;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;

import javax.inject.Named;
import java.util.Map;

@ExportAsService
@Named
public class DefaultContentRenderingInstructionProvider implements ContentRenderingInstructionProvider
{
    private static final String RENDERING_INSTRUCTIONS_PROVIDER_CLASS_PARAM = "contentRenderingInstructionsProviderClass";

    @Override
    public ContentRenderingInstruction get(Map<String, Object> context, WebPanelModuleDescriptor panel)
    {
        Map<String, String> moduleParams = panel.getParams();

        // No reload checker.
        if (!moduleParams.containsKey(RENDERING_INSTRUCTIONS_PROVIDER_CLASS_PARAM))
        {
            return null;
        }

        String providerClassName = moduleParams.get(RENDERING_INSTRUCTIONS_PROVIDER_CLASS_PARAM);

        Class<ContentRenderingInstructionsProvider> providerClass;
        try
        {
            providerClass = panel.getPlugin().loadClass(providerClassName, getClass());
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }

        ContentRenderingInstructionsProvider provider = PluginInjector.newInstance(providerClass, panel.getPlugin());

        return provider.getInstruction(context);
    }
}
