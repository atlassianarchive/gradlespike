package com.atlassian.jira.components.issueviewer.viewissue;

import com.atlassian.jira.issue.fields.rest.json.beans.FieldHtmlBean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "field")
public class FieldHtmlBeanWithContentId extends FieldHtmlBean
{
    @XmlElement
    private String contentId;

    public FieldHtmlBeanWithContentId(FieldHtmlBean field, String contentId)
    {
        super(field.getId(), field.getLabel(), field.isRequired(), field.getEditHtml(), field.getTab());
        this.contentId = contentId;
    }

    public String getContentId()
    {
        return contentId;
    }
}
