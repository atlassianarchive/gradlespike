package com.atlassian.jira.components.query;

import com.atlassian.jira.issue.IssueFieldConstants;

public enum PrimarySearchers
{
    PROJECT(IssueFieldConstants.PROJECT),
    ISSUE_TYPE(IssueFieldConstants.ISSUE_TYPE),
    STATUS(IssueFieldConstants.STATUS),
    ASSIGNEE(IssueFieldConstants.ASSIGNEE);

    private final String id;

    PrimarySearchers(final String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
}
