package com.atlassian.jira.components.issueviewer.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

/**
 * Useful utils function for web actions
 *
 * @since v6.0
 */
public interface ActionUtilsService
{
    String getUserAgent(HttpServletRequest request);

    boolean isInlineEditEnabled();

    boolean noGlobalShortcutLinksIsEnabled();

    String getQueryStringFromComponents(Map<String, Object[]> parameters);

    String getQueryStringFromComponents(Map<String, Object[]> parameters, List<String> whiteList);

    Issue getIssue(Long issueId, User user);

    Issue getIssue(String issueKey, User user);

    String getVisibleFieldNamesJson() throws JSONException;

    String getVisibleFunctionNamesJson() throws JSONException;

    String getJqlReservedWordsJson() throws JSONException;

    String getSystemFiltersJson();

    SoyTemplateRenderer getSoyRenderer();
}
