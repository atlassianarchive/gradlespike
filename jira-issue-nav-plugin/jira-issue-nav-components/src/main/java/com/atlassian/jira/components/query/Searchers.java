package com.atlassian.jira.components.query;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

/**
 * Collection of searcher groups
 * @since v5.1
 */
@XmlRootElement
public class Searchers
{
    @XmlElement
    private List<FilteredSearcherGroup> groups;

    public Searchers()
    {
        this.groups = new ArrayList<FilteredSearcherGroup>();
    }

    public void addGroup(FilteredSearcherGroup group)
    {
        groups.add(group);
    }

    public void addGroups(Collection<FilteredSearcherGroup> groups)
    {
        this.groups.addAll(groups);
    }

    public List<FilteredSearcherGroup> getGroups()
    {
        return this.groups;
    }
}

