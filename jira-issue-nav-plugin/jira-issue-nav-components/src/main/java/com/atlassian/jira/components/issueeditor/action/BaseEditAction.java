package com.atlassian.jira.components.issueeditor.action;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.components.issueviewer.viewissue.IssueFieldProvider;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.operation.IssueOperation;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import webwork.action.ActionContext;

import java.util.HashMap;
import java.util.Map;

/**
 * Base action for retrieving field edit html
 *
 * @since 5.0
 */
public abstract class BaseEditAction extends JiraWebActionSupport implements OperationContext
{
    protected final IssueService issueService;
    protected final IssueFieldProvider issueFieldProvider;

    private final Map<String, Object> fieldValuesHolder = new HashMap<String, Object>();
    protected Long issueId;
    protected String issueKey;
    protected Long lastReadTime;

    public BaseEditAction(final IssueService issueService, final IssueFieldProvider issueFieldProvider)
    {
        this.issueService = issueService;
        this.issueFieldProvider = issueFieldProvider;
    }

    public Long getIssueId()
    {
        return issueId;
    }

    public void setIssueId(Long issueId)
    {
        this.issueId = issueId;
    }

    public String getIssueKey()
    {
        return issueKey;
    }

    public void setIssueKey(String issueKey)
    {
        this.issueKey = issueKey;
    }

    public void setLastReadTime(Long lastReadTime)
    {
        this.lastReadTime = lastReadTime;
    }

    @Override
    public Map getFieldValuesHolder()
    {
        return fieldValuesHolder;
    }

    public void setFieldValuesHolder(final Map<String, Object> fieldValuesHolder)
    {
        this.fieldValuesHolder.clear();
        this.fieldValuesHolder.putAll(fieldValuesHolder);
    }

    @Override
    public IssueOperation getIssueOperation()
    {
        return IssueOperations.EDIT_ISSUE_OPERATION;
    }

    protected DefaultContentIdCollector createContentIdCollector()
    {
        return new DefaultContentIdCollector(ActionContext.getRequest(), lastReadTime);
    }
}
