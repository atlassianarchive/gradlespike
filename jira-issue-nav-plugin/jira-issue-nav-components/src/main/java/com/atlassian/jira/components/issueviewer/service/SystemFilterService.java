package com.atlassian.jira.components.issueviewer.service;

import java.util.List;

/**
 * @since v5.2
 */
public interface SystemFilterService
{
    /**
     * Gets system filter by id
     * @param id filter id
     * @return System filter
     */
    public SystemFilter getById(Long id);

    /**
     * Gets system filter as a list of jaxb compatible beans
     * @return list of system filtes
     */
    public List<SystemFilterBean> getAllAsBeans();
}
