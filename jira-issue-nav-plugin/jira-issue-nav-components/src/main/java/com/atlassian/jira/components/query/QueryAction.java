package com.atlassian.jira.components.query;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.components.query.util.ActionUtils;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugins.rest.common.json.DefaultJaxbJsonMarshaller;
import com.atlassian.plugins.rest.common.json.JaxbJsonMarshaller;
import webwork.action.ActionContext;

import java.io.IOException;

public class QueryAction extends JiraWebActionSupport
{
    private final SearcherService searcherService;

    private String jql;
    private String filter;

    public QueryAction(SearcherService searcherService)
    {
        this.searcherService = searcherService;
//        this.searchResultsHelper = searchResultsHelper;
    }

    public String doDefault() throws IOException
    {
        ActionContext.getResponse().setContentType("application/json");
//        searchResultsHelper.resetPagerAndSelectedIssue();
        ServiceOutcome<SearchResults> outcome = searcherService.search(this, ActionContext.getParameters(), getFilter());
        if (outcome.isValid())
        {
            return JSON(outcome.getReturnedValue());
        }
        else
        {
            // Errors here are all from edit html, though may change in future
//            setErrorReturnCode(getLoggedInUser());
            return JSON(ErrorCollection.of(outcome.getErrorCollection()));
        }
    }

    public String doJql() throws IOException
    {
        ActionContext.getResponse().setContentType("application/json");
//        searchResultsHelper.resetPagerAndSelectedIssue();
        ServiceOutcome<SearchResults> outcome = searcherService.searchWithJql(this, jql, getFilter());
        if (outcome.isValid())
        {
            return JSON(outcome.getReturnedValue());
        }
        else
        {
            // Errors here are all from edit html, though may change in future
            ActionUtils.setErrorReturnCode(getLoggedInUser());
            return JSON(ErrorCollection.of(outcome.getErrorCollection()));
        }
    }

    private static String JSON(Object bean) throws IOException
    {
        final JaxbJsonMarshaller marshaller = new DefaultJaxbJsonMarshaller();
        final String json = marshaller.marshal(bean);
        ActionContext.getResponse().getWriter().append(json);
        return null;
    }

    public String getJql()
    {
        return jql;
    }

    public void setJql(String jql)
    {
        this.jql = jql;
    }

    /**
     * @return the filter ID or 0 if one wasn't given or it's a system filter.
     */
    public long getFilter()
    {
        try
        {
            return Long.valueOf(filter);
        }
        catch (NumberFormatException e)
        {
            return 0;
        }
    }

    /**
     * Set the action's filter ID.
     *
     * We use a string to support system filters.
     *
     * @param filter The filter ID.
     */
    public void setFilter(String filter)
    {
        this.filter = filter;
    }
}
