package com.atlassian.jira.components.issueviewer.viewissue.webpanel;

import com.atlassian.jira.rest.v2.issue.LinkGroupBean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Represents a web-panel on the view issue page.
 *
 * @since v5.1
 */
@XmlRootElement
public class WebPanelBean
{
    @XmlElement
    private String completeKey;
    @XmlElement
    private String prefix;
    @XmlElement
    private String id;
    @XmlElement
    private String styleClass;
    @XmlElement
    private String label;
    @XmlElement
    private boolean renderHeader;
    @XmlElement
    private LinkGroupBean headerLinks;
    @XmlElement
    private List<String> subpanelHtmls;
    @XmlElement
    private String html;
    @XmlElement
    private Integer weight;
    @XmlElement
    private String contentId;

    WebPanelBean() { }

    private WebPanelBean(final String completeKey, final String prefix, final String id, final String styleClass,
            final String label, final boolean renderHeader, final LinkGroupBean headerLinks,
            final List<String> subpanelHtmls, final String html, final Integer weight)
    {
        this.completeKey = completeKey;
        this.prefix = prefix;
        this.id = id;
        this.styleClass = styleClass;
        this.label = label;
        this.renderHeader = renderHeader;
        this.headerLinks = headerLinks;
        this.subpanelHtmls = subpanelHtmls;
        this.html = html;
        this.weight = weight;
    }

    public String getCompleteKey()
    {
        return completeKey;
    }

    public String getPrefix()
    {
        return prefix;
    }

    public String getId()
    {
        return id;
    }

    public String getStyleClass()
    {
        return styleClass;
    }

    public String getLabel()
    {
        return label;
    }

    public boolean isRenderHeader()
    {
        return renderHeader;
    }

    public LinkGroupBean getHeaderLinks()
    {
        return headerLinks;
    }

    public List<String> getSubpanelHtmls()
    {
        return subpanelHtmls;
    }

    public String getHtml()
    {
        return html;
    }

    public Integer getWeight()
    {
        return weight;
    }

    public String getContentId()
    {
        return contentId;
    }

    public void setContentId(String contentId)
    {
        this.contentId = contentId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        WebPanelBean that = (WebPanelBean) o;

        if (id != null ? !id.equals(that.id) : that.id != null) { return false; }
        if (completeKey != null ? !completeKey.equals(that.completeKey) : that.completeKey != null) { return false; }
        if (prefix != null ? !prefix.equals(that.prefix) : that.prefix != null) { return false; }
        if (styleClass != null ? !styleClass.equals(that.styleClass) : that.styleClass != null) { return false; }
        if (label != null ? !label.equals(that.label) : that.label != null) { return false; }
        if (renderHeader != that.renderHeader) { return false; }
        if (html != null ? !html.equals(that.html) : that.html != null) { return false; }
        if (weight != null ? !weight.equals(that.weight) : that.weight != null) { return false; }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (completeKey != null ? completeKey.hashCode() : 0);
        result = 31 * result + (prefix != null ? prefix.hashCode() : 0);
        result = 31 * result + (styleClass != null ? styleClass.hashCode() : 0);
        result = 31 * result + (label != null ? label.hashCode() : 0);
        result = 31 * result + (renderHeader ? 1 : 0);
        result = 31 * result + (html != null ? html.hashCode() : 0);
        result = 31 * result + (weight != null ? weight.hashCode() : 0);
        return result;
    }

    public static class Builder
    {
        private String completeKey;
        private String prefix;
        private String id;
        private String styleClass;
        private String label;
        private boolean renderHeader;
        private LinkGroupBean headerLinks;
        private List<String> subpanelHtmls;
        private String html;
        private Integer weight;

        public Builder()
        {
        }

        public Builder(WebPanelBean panel)
        {
            this.completeKey = panel.getCompleteKey();
            this.prefix = panel.getPrefix();
            this.id = panel.getId();
            this.styleClass = panel.getStyleClass();
            this.label = panel.getLabel();
            this.renderHeader = panel.isRenderHeader();
            this.headerLinks = panel.getHeaderLinks();
            this.subpanelHtmls = panel.getSubpanelHtmls();
            this.weight = panel.getWeight();
            this.html = panel.getHtml();
        }

        public Builder completeKey(final String completeKey)
        {
            this.completeKey = completeKey;
            return this;
        }

        public Builder prefix(final String prefix)
        {
            this.prefix = prefix;
            return this;
        }

        public Builder id(final String id)
        {
            this.id = id;
            return this;
        }

        public Builder styleClass(final String styleClass)
        {
            this.styleClass = styleClass;
            return this;
        }

        public Builder label(final String label)
        {
            this.label = label;
            return this;
        }

        public Builder renderHeader(final boolean renderHeader)
        {
            this.renderHeader = renderHeader;
            return this;
        }

        public Builder headerLinks(final LinkGroupBean headerLinks)
        {
            this.headerLinks = headerLinks;
            return this;
        }

        public Builder subpanelHtmls(final List<String> subpanelHtmls)
        {
            this.subpanelHtmls = subpanelHtmls;
            return this;
        }

        public Builder html(final String html)
        {
            this.html = html;
            return this;
        }

        public Builder weight(Integer weight)
        {
            this.weight = weight;
            return this;
        }

        public WebPanelBean build()
        {
            return new WebPanelBean(completeKey, prefix, id, styleClass, label, renderHeader, headerLinks, subpanelHtmls, html, weight);
        }
    }
}
