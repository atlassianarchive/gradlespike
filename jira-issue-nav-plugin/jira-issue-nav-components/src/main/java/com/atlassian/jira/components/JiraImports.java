package com.atlassian.jira.components;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.rest.FieldHtmlFactory;
import com.atlassian.jira.issue.fields.rest.IssueFinder;
import com.atlassian.jira.issue.search.managers.IssueSearcherManager;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.jql.util.JqlStringSupport;
import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.rest.v2.issue.builder.BeanBuilderFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.user.UserIssueHistoryManager;
import com.atlassian.jira.user.UserIssueSearcherHistoryManager;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.action.issue.IssueMetadataHelper;
import com.atlassian.jira.web.component.ModuleWebComponent;
import com.atlassian.jira.web.component.ModuleWebComponentFields;
import com.atlassian.jira.web.component.jql.AutoCompleteJsonGenerator;
import com.atlassian.jira.web.session.SessionSearchObjectManagerFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Generic class to centralize all imports
 *
 * @since v6.0
 */
@Named
public class JiraImports
{
    private final I18nResolver i18nResolver;
    private final BeanBuilderFactory beanBuilderFactory;
    private final IssueFinder issueFinder;
    private final PluginSettingsFactory pluginSettings;
    private final RequestFactory restRequestFactory;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final ModuleWebComponentFields moduleWebComponentFields;
    private final JqlQueryParser jqlQueryParser;
    private final VelocityRequestContextFactory velocityRequestContextFactory;
    private final IssueMetadataHelper issueMetadataHelper;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final SessionSearchObjectManagerFactory sessionSearchObjectManagerFactory;
    private final VelocityTemplatingEngine velocityTemplatingEngine;
    private final UserIssueHistoryManager userIssueHistoryManager;
    private final UserPreferencesManager userPreferencesManager;
    private final I18nHelper contextI18nHelper;
    private final SearchRequestService searchRequestService;
    private final JqlStringSupport jqlStringSupport;
    private final FieldManager fieldManager;
    private final BuildUtilsInfo buildUtilsInfo;
    private final IssueService issueService;
    private final PermissionManager permissionManager;
    private final IssueSearcherManager issueSearcherManager;
    private final EventPublisher eventPublisher;
    private final SearchService searchService;
    private final SearchHandlerManager searchHandlerManager;
    private final ModuleWebComponent moduleWebComponent;
    private final FieldHtmlFactory fieldHtmlFactory;
    private final WebInterfaceManager webInterfaceManager;
    private final FeatureManager featureManager;
    private final AutoCompleteJsonGenerator autoCompleteJsonGenerator;
    private final ApplicationProperties applicationProperties;
    private final JiraWebResourceManager jiraWebResourceManager;
    private UserIssueSearcherHistoryManager userIssueSearcherHistoryManager;

    @Inject
    public JiraImports(
            @ComponentImport final I18nResolver i18nResolver,
            @ComponentImport final BeanBuilderFactory beanBuilderFactory,
            @ComponentImport final IssueFinder issueFinder,
            @ComponentImport final PluginSettingsFactory pluginSettings,
            @ComponentImport final RequestFactory restRequestFactory,
            @ComponentImport final SoyTemplateRenderer soyTemplateRenderer,
            @ComponentImport final ModuleWebComponentFields moduleWebComponentFields,
            @ComponentImport final JqlQueryParser jqlQueryParser,
            @ComponentImport final VelocityRequestContextFactory velocityRequestContextFactory,
            @ComponentImport final IssueMetadataHelper issueMetadataHelper,
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final SessionSearchObjectManagerFactory sessionSearchObjectManagerFactory,
            @ComponentImport final VelocityTemplatingEngine velocityTemplatingEngine,
            @ComponentImport final UserIssueHistoryManager userIssueHistoryManager,
            @ComponentImport final UserPreferencesManager userPreferencesManager,
            @ComponentImport final I18nHelper contextI18nHelper,
            @ComponentImport final SearchRequestService searchRequestService,
            @ComponentImport final JqlStringSupport jqlStringSupport,
            @ComponentImport final FieldManager fieldManager,
            @ComponentImport final BuildUtilsInfo buildUtilsInfo,
            @ComponentImport final IssueService issueService,
            @ComponentImport final PermissionManager permissionManager,
            @ComponentImport final IssueSearcherManager issueSearcherManager,
            @ComponentImport final EventPublisher eventPublisher,
            @ComponentImport final SearchService searchService,
            @ComponentImport final SearchHandlerManager searchHandlerManager,
            @ComponentImport final ModuleWebComponent moduleWebComponent,
            @ComponentImport final FieldHtmlFactory fieldHtmlFactory,
            @ComponentImport final WebInterfaceManager webInterfaceManager,
            @ComponentImport final FeatureManager featureManager,
            @ComponentImport final AutoCompleteJsonGenerator autoCompleteJsonGenerator,
            @ComponentImport final ApplicationProperties applicationProperties,
            @ComponentImport("webResourceManager") final JiraWebResourceManager jiraWebResourceManager,
            @ComponentImport final UserIssueSearcherHistoryManager userIssueSearcherHistoryManager
    ) {
        this.i18nResolver = i18nResolver;
        this.beanBuilderFactory = beanBuilderFactory;
        this.issueFinder = issueFinder;
        this.pluginSettings = pluginSettings;
        this.restRequestFactory = restRequestFactory;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.moduleWebComponentFields = moduleWebComponentFields;
        this.jqlQueryParser = jqlQueryParser;
        this.velocityRequestContextFactory = velocityRequestContextFactory;
        this.issueMetadataHelper = issueMetadataHelper;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.sessionSearchObjectManagerFactory = sessionSearchObjectManagerFactory;
        this.velocityTemplatingEngine = velocityTemplatingEngine;
        this.userIssueHistoryManager = userIssueHistoryManager;
        this.userPreferencesManager = userPreferencesManager;
        this.contextI18nHelper = contextI18nHelper;
        this.searchRequestService = searchRequestService;
        this.jqlStringSupport = jqlStringSupport;
        this.fieldManager = fieldManager;
        this.buildUtilsInfo = buildUtilsInfo;
        this.issueService = issueService;
        this.permissionManager = permissionManager;
        this.issueSearcherManager = issueSearcherManager;
        this.eventPublisher = eventPublisher;
        this.searchService = searchService;
        this.searchHandlerManager = searchHandlerManager;
        this.moduleWebComponent = moduleWebComponent;
        this.fieldHtmlFactory = fieldHtmlFactory;
        this.webInterfaceManager = webInterfaceManager;
        this.featureManager = featureManager;
        this.autoCompleteJsonGenerator = autoCompleteJsonGenerator;
        this.applicationProperties = applicationProperties;
        this.jiraWebResourceManager = jiraWebResourceManager;
        this.userIssueSearcherHistoryManager = userIssueSearcherHistoryManager;
    }
}
