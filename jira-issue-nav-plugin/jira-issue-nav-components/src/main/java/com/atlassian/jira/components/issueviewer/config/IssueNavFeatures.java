package com.atlassian.jira.components.issueviewer.config;

import com.atlassian.jira.config.Feature;
import com.atlassian.jira.config.FeatureManager;

/**
 * Issue navigation plugin features.
 */
public enum IssueNavFeatures implements Feature
{
    /**
     * If enabled, kills issue prefetching.
     */
    NO_PREFETCH,

    /**
     * Force the navigation using /i root
     */
    I_ROOT,

    /**
     * If enabled, kills the rendering of global hidden shortcut links from KA pages
     *
     * Used to remove multiple elements matching the same ID to aid functional (non-webdriver) tests.
     */
    NO_GLOBAL_SHORTCUT_LINKS;

    /**
     * Returns the feature's key. The key has the form "ka.X" where X is the name of the enum.
     *
     * @return the feature's key; used for dark features.
     */
    public String featureKey()
    {
        return "ka." + name();
    }

    public boolean isEnabled(FeatureManager featureManager)
    {
        return featureManager.isEnabled(this);
    }
}
