package com.atlassian.jira.components.issueviewer.util;

import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.UserAgentUtilImpl;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static com.atlassian.jira.util.UserAgentUtil.*;

/**
 * Provides logic to determine if a browser supports {@code pushState} and, if not, to transform a URL into an
 * equivalent {@code /i} root URL (e.g. from {@code /issues/?jql=type = Bug} to {@code /i#issues/?jql=type = Bug}.
 */
public class HashTransformer
{
    private final static Pattern HASH_URL_PATTERN = Pattern.compile(".*/i($|\\?.*)");
    private final static String HASH_URL_ROOT = "/i";

    // Parameters that must remain in the query string sent to the server.
    private final static List<String> PARAMETER_WHITE_LIST = Lists.newArrayList(
            "jwupdated",
            "quickSearchQuery"
    );

    /**
     * Does the browser with the given user-agent header support push state?
     *
     * @param userAgent The browser's user-agent header.
     * @return {@code true} iff the browser supports push state.
     */
    public boolean supportsPushState(String userAgent)
    {
        Browser browser = new UserAgentUtilImpl().getUserAgentInfo(userAgent).getBrowser();
        BrowserMajorVersion version = browser.getBrowserMajorVersion();

        //Unsupported browsers (http://caniuse.com/#feat=history)
        return !(  BrowserMajorVersion.MSIE8.equals(version)
                || BrowserMajorVersion.MSIE9.equals(version)
                || BrowserMajorVersion.SAFARI32.equals(version)
                || BrowserMajorVersion.SAFARI4.equals(version)
                || BrowserMajorVersion.SAFARI41.equals(version)
                || BrowserMajorVersion.OPERAMINI5.equals(version)
                || BrowserMajorVersion.OPERAMINI6.equals(version)
                || BrowserMajorVersion.OPERAMINI7.equals(version)
                || BrowserMajorVersion.ANDROID21.equals(version)
                || BrowserMajorVersion.ANDROID3.equals(version)
                || BrowserMajorVersion.ANDROID4.equals(version)
                || BrowserMajorVersion.ANDROID41.equals(version)
        );
    }

    public boolean isHashedUrl(String URL)
    {
        return (URL != null && HASH_URL_PATTERN.matcher(URL).matches());
    }

    /**
     * Transform a URL into an equivalent {@code /i} root URL.
     * <p/>
     * Some parameters are white listed and won't be moved into the hash.
     *
     * @param URL The URL to transform (not including context path).
     * @return The result of the transformation.
     */
    public String transformURL(final String URL, final Map<String, Object[]> parameters)
    {
        if (parameters == null && URL == null)
        {
            return "";
        }

        String URLAsFragment = URL.startsWith("/") ? URL.replaceFirst("/", "#") : "#" + URL;
        UrlBuilder realQueryString = new UrlBuilder(false);
        UrlBuilder fragmentQueryString = new UrlBuilder(false);

        if (URL != null && (parameters == null || parameters.size() == 0))
        {
            return HASH_URL_ROOT + URLAsFragment;
        }

        for (String key : parameters.keySet())
        {
            if (PARAMETER_WHITE_LIST.contains(key))
            {
                addParameter(realQueryString, key, parameters.get(key));
            }
            else
            {
                addParameter(fragmentQueryString, key, parameters.get(key));
            }
        }

        String query = realQueryString.asUrlString();
        String fragment = fragmentQueryString.asUrlString();

        return HASH_URL_ROOT + query + URLAsFragment + fragment;
    }

    protected void addParameter(UrlBuilder urlBuilder, String key,
            Object[] values)
    {
        for (Object value : values)
        {
            urlBuilder.addParameter(key, value);
        }
    }
}