package com.atlassian.jira.components.orderby;

import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.components.util.SortJqlGenerator;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.query.Query;
import com.atlassian.query.order.OrderBy;
import com.atlassian.query.order.SearchSort;
import com.google.common.collect.Lists;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.List;

@ExportAsService
@Named
public class DefaultOrderByUtil implements OrderByUtil {

    private SearchHandlerManager searchHandlerManager;
    private FieldManager fieldManager;
    private final SortJqlGenerator sortJqlGenerator;

    @Inject
    public DefaultOrderByUtil(SearchHandlerManager searchHandlerManager, FieldManager fieldManager, SortJqlGenerator sortJqlGenerator) {
        this.searchHandlerManager = searchHandlerManager;
        this.fieldManager = fieldManager;
        this.sortJqlGenerator = sortJqlGenerator;
    }

    @Override
    public SortByBean generateSortBy(final Query query)
    {
        OrderBy orderByClause = query.getOrderByClause();
        if (orderByClause != null)
        {
            List<SearchSort> searchSorts = orderByClause.getSearchSorts();
            if (searchSorts.size() > 0)
            {
                SearchSort searchSort = searchSorts.get(0);

                // this may return multiple field IDs if there are multiple custom fields that have the same name. in
                // that case we just consider that the search results are sorted by the 1st field that has the name
                Collection<String> fieldIds = searchHandlerManager.getFieldIds(searchSort.getField());
                if (!fieldIds.isEmpty())
                {
                    String fieldId = fieldIds.iterator().next();
                    Field field = fieldManager.getField(fieldId);
                    String order = searchSort.getOrder();
                    if (order == null || order.isEmpty())
                    {
                        // if no order is provided, return the default sort order. it's likely that I'm being overly
                        // defensive here, but I'm not sure if it's possible for this to *not* be navigable.

                        order = field instanceof NavigableField ? ((NavigableField) field).getDefaultSortOrder() : "ASC";
                    }
                    String sortJql = null;
                    if (field instanceof NavigableField) {
                        sortJql = sortJqlGenerator.generateColumnSortJql(query, Lists.newArrayList((NavigableField) field))
                                .get(fieldId);
                    }
                    return new SortByBean(field.getId(), field.getName(), order, sortJql);
                }
            }
        }
        return null;
    }
}
