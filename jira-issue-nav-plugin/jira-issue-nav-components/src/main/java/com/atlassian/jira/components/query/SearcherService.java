package com.atlassian.jira.components.query;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import java.util.Map;

/**
 * Kickass interface to searchers.
 *
 * @since v5.1
 */
public interface SearcherService
{
    public ServiceOutcome<SearchResults> search(JiraWebActionSupport action, Map<String, String[]> params, Long filterId);

    /**
     * Try the given JQL as a basic mode search. If it can't be represented as a basic search, the result
     * is an error.
     * @param action The Action
     * @param jql The JQL requested
     * @param filterId The optional Filter ID
     *
     * @return ServiceOutcome<SearchResults> The result or error from the search.
     */
    public ServiceOutcome<SearchResults> searchWithJql(JiraWebActionSupport action, String jql, Long filterId);

    public ServiceOutcome<SearchRendererValueResults> getViewHtml(JiraWebActionSupport action, Map<String, String[]> params);

    public ServiceOutcome<String> getEditHtml(String searcherId, String jqlContext, JiraWebActionSupport action);

    public Searchers getSearchers(String jqlContext);
}