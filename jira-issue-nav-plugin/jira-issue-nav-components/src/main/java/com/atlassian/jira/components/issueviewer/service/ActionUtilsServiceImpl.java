package com.atlassian.jira.components.issueviewer.service;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.components.issueviewer.config.IssueNavFeatures;
import com.atlassian.jira.components.issueviewer.util.HashTransformer;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.BrowserUtils;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.web.component.jql.AutoCompleteJsonGenerator;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugins.rest.common.json.DefaultJaxbJsonMarshaller;
import com.atlassian.plugins.rest.common.json.JaxbJsonMarshaller;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import com.google.common.collect.Lists;

import org.apache.log4j.Logger;

/**
 * Useful utils function for web actions
 *
 * @since v6.0
 */
@ExportAsService
@Named
public class ActionUtilsServiceImpl implements ActionUtilsService
{
    private static final Logger log = Logger.getLogger(ActionUtilsServiceImpl.class);
    public static final String JIRA_OPTION_DISABLE_INLINE_EDIT = "jira.issue.inline.edit.disabled";
    private final AutoCompleteJsonGenerator autoCompleteJsonGenerator;
    private final FeatureManager featureManager;
    private final HashTransformer hashTransformer;
    private final IssueService issueService;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final SystemFilterService systemFilterService;

    private static final List<String> QUERY_PARAMETERS = Lists.newArrayList("jql", "filter", "startIndex", "page");
    private final ApplicationProperties applicationProperties;

    @Inject
    public ActionUtilsServiceImpl(
            final IssueService issueService,
            final JiraAuthenticationContext jiraAuthenticationContext,
            final SystemFilterService systemFilterService,
            final FeatureManager featureManager,
            final AutoCompleteJsonGenerator autoCompleteJsonGenerator,
            final SoyTemplateRenderer soyTemplateRenderer,
            final ApplicationProperties applicationProperties)
    {
        this.autoCompleteJsonGenerator = autoCompleteJsonGenerator;
        this.featureManager = featureManager;
        this.applicationProperties = applicationProperties;
        this.hashTransformer = new HashTransformer();
        this.issueService = issueService;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.systemFilterService = systemFilterService;
    }

    @Override
    public String getUserAgent(HttpServletRequest request)
    {
        return request.getHeader(BrowserUtils.USER_AGENT_HEADER);
    }

    /**
     * @return whether inline edit has been disabled for this JIRA
     */
    @Override
    public boolean isInlineEditEnabled()
    {
        return !this.applicationProperties.getOption(JIRA_OPTION_DISABLE_INLINE_EDIT);
    }

    /**
     * @return whether no global shortcut links dark feature is enabled.
     */
    @Override
    public boolean noGlobalShortcutLinksIsEnabled()
    {
        return IssueNavFeatures.NO_GLOBAL_SHORTCUT_LINKS.isEnabled(featureManager);
    }

    @Override
    public String getQueryStringFromComponents(Map<String, Object[]> parameters)
    {
        return getQueryStringFromComponents(parameters, QUERY_PARAMETERS);
    }

    @Override
    public String getQueryStringFromComponents(Map<String, Object[]> parameters, List<String> whiteList)
    {
        if (parameters == null)
        {
            return "";
        }

        UrlBuilder query = new UrlBuilder(false);

        for (String key : parameters.keySet())
        {
            if (whiteList.contains(key))
            {
                this.addParameter(query, key, parameters.get(key));
            }
        }

        return query.asUrlString();
    }

    private void addParameter(UrlBuilder urlBuilder, String key,
                                Object[] values)
    {
        for (Object value : values)
        {
            urlBuilder.addParameter(key, value);
        }
    }

    /**
     * Retrieve an issue given its id.
     * <p/>
     * Returns {@code null} if {@code issueId} is {@code null} or if the
     * current user doesn't have permission to access the referenced issue.
     *
     * @param issueId The issue's id.
     * @return the issue or {@code null}.
     */
    @Override
    public Issue getIssue(Long issueId, User user)
    {
        if (issueId != null)
        {
            IssueService.IssueResult issueResult = issueService.getIssue(user, issueId);

            if (issueResult.isValid())
            {
                return issueResult.getIssue();
            }
        }
        return null;
    }

    /**
     * Retrieve an issue given its key.
     * <p/>
     * Returns {@code null} if {@code issueKey} is {@code null} or if the
     * current user doesn't have permission to access the referenced issue.
     *
     * @param issueKey The issue's key.
     * @return the issue or {@code null}.
     */
    @Override
    public Issue getIssue(String issueKey, User user)
    {
        if (issueKey != null)
        {
            IssueService.IssueResult issueResult = issueService.getIssue(user, issueKey);

            if (issueResult.isValid())
            {
                return issueResult.getIssue();
            }
        }
        return null;
    }

    @Override
    public String getVisibleFieldNamesJson() throws JSONException
    {
        return autoCompleteJsonGenerator.getVisibleFieldNamesJson(
                jiraAuthenticationContext.getLoggedInUser(), jiraAuthenticationContext.getLocale());
    }

    @Override
    public String getVisibleFunctionNamesJson() throws JSONException
    {
        return autoCompleteJsonGenerator.getVisibleFunctionNamesJson(
                jiraAuthenticationContext.getLoggedInUser(), jiraAuthenticationContext.getLocale());
    }

    @Override
    public String getJqlReservedWordsJson() throws JSONException
    {
        return autoCompleteJsonGenerator.getJqlReservedWordsJson();
    }

    @Override
    public String getSystemFiltersJson()
    {
        final JaxbJsonMarshaller marshaller = new DefaultJaxbJsonMarshaller();
        return marshaller.marshal(systemFilterService.getAllAsBeans());
    }

    @Override
    public SoyTemplateRenderer getSoyRenderer()
    {
        return soyTemplateRenderer;
    }
}
