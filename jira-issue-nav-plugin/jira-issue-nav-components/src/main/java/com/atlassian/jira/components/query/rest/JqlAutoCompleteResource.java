package com.atlassian.jira.components.query.rest;

import com.atlassian.jira.rest.api.http.CacheControl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.web.component.jql.AutoCompleteJsonGenerator;
import com.atlassian.plugins.rest.common.json.DefaultJaxbJsonMarshaller;
import com.atlassian.plugins.rest.common.json.JaxbJsonMarshaller;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.collect.Maps;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.Map;

/**
 * A rest end point to get the jql auto complete meta data
 *
 * @since v6.0
 */
@AnonymousAllowed
@Path("jqlAutoComplete")
public class JqlAutoCompleteResource {

    private AutoCompleteJsonGenerator autoCompleteJsonGenerator;
    private JiraAuthenticationContext jiraAuthenticationContext;

    public JqlAutoCompleteResource(final AutoCompleteJsonGenerator autoCompleteJsonGenerator,
                                   final JiraAuthenticationContext jiraAuthenticationContext) {
        this.autoCompleteJsonGenerator = autoCompleteJsonGenerator;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @GET
    public Response get() throws JSONException {
        return Response.ok(getJqlAutoCompleteJson()).cacheControl(CacheControl.never()).build();
    }

    private String getJqlAutoCompleteJson() throws JSONException {
        Map<String, String> jqlAutoCompleteJson = Maps.newHashMap();

        jqlAutoCompleteJson.put("jqlFieldz", getVisibleFieldNamesJson());
        jqlAutoCompleteJson.put("jqlFunctionNamez", getVisibleFunctionNamesJson());
        jqlAutoCompleteJson.put("jqlReservedWordz", getJqlReservedWordsJson());

        final JaxbJsonMarshaller marshaller = new DefaultJaxbJsonMarshaller();
        return marshaller.marshal(jqlAutoCompleteJson);
    }

    public String getVisibleFieldNamesJson() throws JSONException {
        return autoCompleteJsonGenerator.getVisibleFieldNamesJson(
                jiraAuthenticationContext.getLoggedInUser(), jiraAuthenticationContext.getLocale());
    }

    public String getVisibleFunctionNamesJson() throws JSONException {
        return autoCompleteJsonGenerator.getVisibleFunctionNamesJson(
                jiraAuthenticationContext.getLoggedInUser(), jiraAuthenticationContext.getLocale());
    }

    public String getJqlReservedWordsJson() throws JSONException {
        return autoCompleteJsonGenerator.getJqlReservedWordsJson();
    }


}
