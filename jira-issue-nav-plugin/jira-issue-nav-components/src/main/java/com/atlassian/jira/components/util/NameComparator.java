package com.atlassian.jira.components.util;

import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.util.I18nHelper;

import java.util.Comparator;

public class NameComparator implements Comparator {
    private final I18nHelper i18n;

    public NameComparator(I18nHelper i18n) {
        this.i18n = i18n;
    }

    public int compare(Object o1, Object o2) {
        if (o1 == null)
            throw new IllegalArgumentException("The first parameter is null");
        if (!(o1 instanceof Field))
            throw new IllegalArgumentException("The first parameter " + o1 + " is not an instance of Field");
        if (o2 == null)
            throw new IllegalArgumentException("The second parameter is null");
        if (!(o2 instanceof Field))
            throw new IllegalArgumentException("The second parameter " + o2 + " is not an instance of Field");

        String name1 = i18n.getText(((Field) o1).getNameKey());
        String name2 = i18n.getText(((Field) o2).getNameKey());
        return name1.compareTo(name2);
    }
}