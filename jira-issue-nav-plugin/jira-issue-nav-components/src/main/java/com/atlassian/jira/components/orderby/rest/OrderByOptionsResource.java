package com.atlassian.jira.components.orderby.rest;

import com.atlassian.jira.components.orderby.OrderByUtil;
import com.atlassian.jira.components.orderby.SortByBean;
import com.atlassian.jira.components.util.FilterLookerUpper;
import com.atlassian.jira.components.util.NameComparator;
import com.atlassian.jira.components.util.SortJqlGenerator;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldException;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.query.Query;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.emptyList;
import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Returns the fields that users can sort by along with the corresponding sort JQL.
 *
 * @since v2.0.12
 */
@Path ("orderByOptions")
@AnonymousAllowed
@Consumes (MediaType.APPLICATION_JSON)
@Produces (MediaType.APPLICATION_JSON)
public class OrderByOptionsResource
{
    /**
     * Logger for OrderByOptionsResource.
     */
//    private static final Logger log = LoggerFactory.getLogger(OrderByOptionsResource.class);

    /**
     * The maximum number of options that this resource will ever return.
     */
    private static final int MAX_RESULTS_HARD_LIMIT = 50;

    private final FieldManager fieldManager;
    private final SortJqlGenerator sortJqlGenerator;
    private final JqlQueryParser jqlQueryParser;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final FilterLookerUpper filterLookerUpper;
    private final OrderByUtil orderByUtil;

    /**
     * Creates a new OrderByOptionsResource.
     */
    public OrderByOptionsResource(FieldManager fieldManager, JqlQueryParser jqlQueryParser,  SortJqlGenerator sortJqlGenerator, JiraAuthenticationContext jiraAuthenticationContext, FilterLookerUpper filterLookerUpper, OrderByUtil orderByUtil)
    {
        this.fieldManager = fieldManager;
        this.jqlQueryParser = jqlQueryParser;
        this.sortJqlGenerator = sortJqlGenerator;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.filterLookerUpper = filterLookerUpper;
        this.orderByUtil = orderByUtil;
    }

    /**
     * Returns the order-by options that are available that match <code>fieldNamePrefix</code>. If
     * <code>fieldNamePrefix</code> is null or a blank String then this method returns the first <code>maxResults</code>
     * options (defaults to 10 and is hard-limited to {@value #MAX_RESULTS_HARD_LIMIT}).
     *
     * @param request the body of the request.
     * @return OrderByOptions
     */
    @POST
    public OrderByOptions getOrderByOptions(OrderByRequest request)
    {
        int effectiveMaxResults = Math.min(request.maxResults, MAX_RESULTS_HARD_LIMIT);
        try
        {
            final Query query = getQuery(request.jql, request.filter);
            final SuggestedFields fields = new SuggestedFields(getSortableColumns());

            // filter by field name prefix from the suggestions
            SuggestedFields matchingFields = fields;
            if (isNotBlank(request.query))
            {
                matchingFields = matchingFields.filterBy(new FieldNameStartsWith(request.query));
            }

            // remove the current sortBy field from the suggestions
            if (isNotBlank(request.sortBy))
            {
                matchingFields = matchingFields.filterBy(new FieldIdIsNot(request.sortBy));
            }

            // return the top N fields that match
            SuggestedFields topMatchingFields = matchingFields.sortBy(new NameComparator(jiraAuthenticationContext.getI18nHelper())).selectTop(effectiveMaxResults);

            List<OrderByOption> orderOptions = buildOrderOptions(topMatchingFields.fields, query);
            return new OrderByOptions()
                    .fields(orderOptions)
                    .totalCount(fields.size())
                    .matchesCount(matchingFields.size())
                    .maxResults(effectiveMaxResults);
        }
        catch (JqlParseException e)
        {
            throw new RuntimeException("Error parsing JQL: " + request.jql, e);
        }
    }


    /**
     * Returns the order-by options that are available that match <code>fieldNamePrefix</code>. If
     * <code>fieldNamePrefix</code> is null or a blank String then this method returns the first <code>maxResults</code>
     * options (defaults to 10 and is hard-limited to {@value #MAX_RESULTS_HARD_LIMIT}).
     *
     *
     * @param request the body of the request.
     * @return OrderByOptions
     */
    @POST
    @Path("primary")
    public SortByBean getPrimary(OrderByRequest request)
    {
        try
        {
            final Query query = getQuery(request.jql, request.filter);
            return orderByUtil.generateSortBy(query);
        }
        catch (JqlParseException e)
        {
            throw new RuntimeException("Error parsing JQL: " + request.jql, e);
        }
    }

    private Query getQuery(String jql, String filter) throws JqlParseException
    {
        if (isNotBlank(jql))
        {
            return jqlQueryParser.parseQuery(jql);
        }

        if (isNotBlank(filter))
        {
            ErrorCollection errors = new SimpleErrorCollection();
            SearchRequest searchRequest = filterLookerUpper.getSearchRequestFromFilterId(filter, errors);

            if (searchRequest != null)
            {
                return searchRequest.getQuery();
            }
        }

        // default to an empty query.
        return JqlQueryBuilder.newBuilder().buildQuery();
    }

    private Collection<NavigableField> getSortableColumns()
    {
        try
        {
            final List<NavigableField> navigableFields = Lists.newArrayList(fieldManager.getAvailableNavigableFields(jiraAuthenticationContext.getLoggedInUser()));
            return Collections2.filter(navigableFields, new FieldIsSortable());
        }
        catch (FieldException e)
        {
            throw new RuntimeException("Error getting all navigable fields");
        }
    }

    /**
     * Returns a list of OrderByOption for the given <code>fields</code>.
     *
     * @param fields the fields to generate an OrderByOption for
     * @param query the query to use for generating the sort JQL
     * @return a list of OrderByOption
     */
    private List<OrderByOption> buildOrderOptions(List<NavigableField> fields, Query query)
    {
        if (fields == null || fields.isEmpty())
        {
            return emptyList();
        }


        Map<String, String> sortJql = sortJqlGenerator.generateColumnSortJql(query, fields);
        List<OrderByOption> orderOptions = Lists.newArrayListWithCapacity(fields.size());
        for (NavigableField field : fields)
        {
            if (!sortJql.containsKey(field.getId()))
            {
//                log.debug("Couldn't generate sort JQL for field '{}' and query: {}", field.getId(), query);
                continue;
            }
            orderOptions.add(new OrderByOption()
                    .fieldId(field.getId())
                    .fieldName(field.getName())
                    .sortJql(sortJql.get(field.getId()))
            );
        }

        return orderOptions;
    }

    private ApplicationUser getLoggedInUser()
    {
        return ApplicationUsers.from(jiraAuthenticationContext.getLoggedInUser());
    }

    /**
     * A field list that supports filtering, sorting, topN, etc.
     */
    class SuggestedFields
    {
        private final ImmutableList<NavigableField> fields;

        public SuggestedFields(Collection<NavigableField> fields)
        {
            this.fields = ImmutableList.copyOf(fields);
        }

        public int size()
        {
            return fields.size();
        }

        public SuggestedFields filterBy(Predicate<? super NavigableField> predicate)
        {
            return new SuggestedFields(Collections2.filter(fields, predicate));
        }

        /**
         * Ranks the fields using the given comparator and returns a new FieldList.
         *
         * @param comparator the Comparator to use for ranking
         * @return the top {@code maxResults} fields
         */
        public SuggestedFields sortBy(Comparator<Field> comparator)
        {
            List<NavigableField> ranked = Lists.newArrayList(fields);
            Collections.sort(ranked, comparator);

            return new SuggestedFields(ranked);
        }

        /**
         * Returns the top {@code maxResults} fields.
         */
        public SuggestedFields selectTop(int maxResults)
        {
            return new SuggestedFields(fields.subList(0, Math.min(fields.size(), maxResults)));
        }
    }

    private class FieldNameStartsWith implements Predicate<Field>
    {
        private final String prefix;

        private FieldNameStartsWith(String prefix) {this.prefix = checkNotNull(prefix).toLowerCase();}

        @Override
        public boolean apply(@Nullable Field field)
        {
            return field != null && field.getName() != null && field.getName().toLowerCase().startsWith(prefix);

        }
    }

    private class FieldIsSortable implements Predicate<NavigableField>
    {
        @Override
        public boolean apply(@Nullable NavigableField field)
        {
            return field != null && field.getSorter() != null;
        }
    }

    private static class FieldIdIsNot implements Predicate<Field>
    {
        private final String fieldId;

        public FieldIdIsNot(@Nonnull String fieldId) { this.fieldId = checkNotNull(fieldId); }

        @Override
        public boolean apply(@Nullable Field field)
        {
            return field != null && !fieldId.equals(field.getId());
        }
    }

}
