package com.atlassian.jira.components.util;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Looks up a filter by its id, returning the relevant SearchRequest.
 */
@ExportAsService
@Named
public class DefaultFilterLookerUpper implements FilterLookerUpper {
    private static final String FILTER_ID = "filterId";
    private static final String FILTER_PREFIX = "filter-";
    private static final String JQL_PREFIX = "jql-";

    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final SearchService searchService;
    private final SearchRequestService searchRequestService;

    @Inject
    public DefaultFilterLookerUpper(JiraAuthenticationContext jiraAuthenticationContext, SearchService searchService, SearchRequestService searchRequestService)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.searchService = searchService;
        this.searchRequestService = searchRequestService;
    }

    @Override
    public SearchRequest getSearchRequestFromFilterId(String filterId, ErrorCollection errors)
    {
        final User user = jiraAuthenticationContext.getLoggedInUser();
        SearchRequest searchRequest = null;
        final JiraServiceContextImpl serviceContext = new JiraServiceContextImpl(user);
        if (StringUtils.isBlank(filterId))
        {
            errors.addError(FILTER_ID, jiraAuthenticationContext.getI18nHelper().getText("gadget.common.no.filter.id"));
        }
        else
        {
            // if the filter id starts with "jql-" then we are using this gadget in "Preview Mode" for the Charts Popup
            // in IssueNav; chop off the prefix and validate the JQL query
            if (filterId.startsWith(JQL_PREFIX))
            {
                final SearchService.ParseResult parseResult = searchService.parseQuery(user, filterId.substring(JQL_PREFIX.length()));
                if (parseResult.isValid())
                {
                    searchRequest = new SearchRequest(parseResult.getQuery());
                }
                else
                {
                    for (String errorMessage : parseResult.getErrors().getErrorMessages())
                    {
                        errors.addError(FILTER_ID, jiraAuthenticationContext.getI18nHelper().getText("gadget.common.invalid.filter.validationfailed", CollectionBuilder.newBuilder(filterId, errorMessage).asList()));
                    }
                }
            }
            else
            {
                searchRequest = searchRequestService.getFilter(serviceContext, stripFilterPrefix(filterId, FILTER_PREFIX));
            }

            if (searchRequest == null)
            {
                errors.addErrorMessage(jiraAuthenticationContext.getI18nHelper().getText("admin.errors.filters.nonexistent"));
            }
        }
        return searchRequest;
    }

    private Long stripFilterPrefix(String filterId, String prefix)
    {
        if (filterId.startsWith(prefix))
        {
            final String numPart = filterId.substring(prefix.length());
            return Long.valueOf(numPart);
        }
        else
        {
            return Long.valueOf(filterId);
        }
    }
}
