package com.atlassian.jira.components.query;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.components.query.util.ActionUtils;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugins.rest.common.json.DefaultJaxbJsonMarshaller;
import com.atlassian.plugins.rest.common.json.JaxbJsonMarshaller;
import webwork.action.ActionContext;

import java.io.IOException;

/**
 * A webwork action to produce JSON with search field rendered value html and jql response. This is an action and not a
 * REST resource mainly because our fields API is still so heavily tied to webwork.  All methods on this action should
 * return JSON content.
 *
 * @since 5.0
 */
public class QueryRendererValueAction extends JiraWebActionSupport
{
    private final SearcherService searcherService;
//    private final IssueNavigatorSearchResultsHelper searchResultsHelper;

    private SearchRendererValueResults results;
    private ErrorCollection errors;

    public QueryRendererValueAction(SearcherService searcherService)
    {
        this.searcherService = searcherService;
//        this.searchResultsHelper = searchResultsHelper;
    }

    public String doDefault() throws IOException
    {
        ActionContext.getResponse().setContentType("application/json");
//        searchResultsHelper.resetPagerAndSelectedIssue();
        ServiceOutcome<SearchRendererValueResults> outcome = searcherService.getViewHtml(this, ActionContext.getParameters());
        if (outcome.isValid())
        {
            this.results = outcome.getReturnedValue();
            return JSON();
        }
        else
        {
            this.errors = ErrorCollection.of(outcome.getErrorCollection());
            // Errors here are all from edit html, though may change in future
            ActionUtils.setErrorReturnCode(getLoggedInUser());
            return ERROR_JSON();
        }
    }

    private String ERROR_JSON() throws IOException
    {
        ActionContext.getResponse().getWriter().append(getErrorJson());
        return NONE;
    }

    private String JSON() throws IOException
    {
        ActionContext.getResponse().getWriter().append(getJson());
        return NONE;
    }

    public String getJson()
    {
        final JaxbJsonMarshaller marshaller = new DefaultJaxbJsonMarshaller();
        return marshaller.marshal(this.results);
    }

    public String getErrorJson()
    {
        final JaxbJsonMarshaller marshaller = new DefaultJaxbJsonMarshaller();
        return marshaller.marshal(errors);
    }

}
