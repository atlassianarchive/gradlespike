package com.atlassian.jira.components.issueviewer.service;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.components.issueviewer.service.SessionSearchService;
import com.atlassian.jira.components.issueviewer.service.SystemFilter;
import com.atlassian.jira.components.issueviewer.service.SystemFilterService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.pager.PagerManager;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.jira.web.session.SessionPagerFilterManager;
import com.atlassian.jira.web.session.SessionSearchObjectManagerFactory;
import com.atlassian.jira.web.session.SessionSearchRequestManager;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.query.Query;
import webwork.action.ActionContext;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

/**
 * @since v5.2
 */
@ExportAsService
@Named
public class DefaultSessionSearchService implements SessionSearchService
{
    private final JiraAuthenticationContext authenticationContext;
    private final SystemFilterService systemFilterService;
    private final PagerManager pagerManager;
    private final SearchRequestService searchRequestService;
    private final SearchService searchService;
    private final SessionSearchObjectManagerFactory sessionSearchObjectManagerFactory;

    @Inject
    public DefaultSessionSearchService(ApplicationProperties applicationProperties,
                SessionSearchObjectManagerFactory sessionSearchObjectManagerFactory,
                SearchRequestService searchRequestService,
                SearchService searchService,
                JiraAuthenticationContext authenticationContext,
                SystemFilterService systemFilterService
            )
    {
        this.authenticationContext = authenticationContext;
        this.systemFilterService = systemFilterService;
        this.pagerManager = new PagerManager(applicationProperties, sessionSearchObjectManagerFactory);
        this.searchRequestService = searchRequestService;
        this.searchService = searchService;
        this.sessionSearchObjectManagerFactory = sessionSearchObjectManagerFactory;
    }

    @Override
    public PagerFilter getPagerFilter()
    {
        return getPagerFilterManager().getCurrentObject();
    }

    @Override
    public PagerManager getPagerManager()
    {
        return ComponentAccessor.getComponentOfType(PagerManager.class);
    }

    @Override
    public SearchRequest getSessionSearch()
    {
        HttpServletRequest request = ActionContext.getRequest();
        SearchRequest searchRequest = sessionSearchObjectManagerFactory
                .createSearchRequestManager(request).getCurrentObject();

        if (searchRequest != null && searchRequest.isLoaded())
        {
            ErrorCollection errorCollection = new SimpleErrorCollection();
            JiraServiceContext serviceContext = new JiraServiceContextImpl(
                    authenticationContext.getLoggedInUser(), errorCollection);

            SearchRequest databaseSearchRequest = searchRequestService
                .getFilter(serviceContext, searchRequest.getId());

            if (databaseSearchRequest != null)
            {
                if (searchRequest.isModified())
                {
                    searchRequest.setPermissions(
                            databaseSearchRequest.getPermissions());
                }
                else
                {
                    databaseSearchRequest.setUseColumns(
                            searchRequest.useColumns());
                    searchRequest = databaseSearchRequest;
                }
            }
        }

        return searchRequest;
    }

    @Override
    public void setSessionSearch(String jql, Long filterId, int startIndex, int max)
    {
        final SessionSearchRequestManager sessionSearchRequestManager = sessionSearchObjectManagerFactory.createSearchRequestManager();
        SearchRequest sessionSearchRequest = null;
        if (filterId != null)
        {
            if (SystemFilter.isSystemFilter(filterId))
            {
                SystemFilter systemFilter = systemFilterService.getById(filterId);
                sessionSearchRequest = new SearchRequest(searchService.parseQuery(null, SystemFilter.getSystemFilterById(filterId).getJql()).getQuery(), (String) null, authenticationContext.getI18nHelper().getText(systemFilter.getName()), null, filterId, 0) {
                    @Override
                    public boolean isLoaded()
                    {
                        return super.isLoaded() && !SystemFilter.isSystemFilter(getId());
                    }
                };
            }
            else
            {
                sessionSearchRequest = searchRequestService.getFilter(new JiraServiceContextImpl(authenticationContext.getLoggedInUser()), filterId);
            }
        }

        if (jql != null)
        {
            User user = authenticationContext.getLoggedInUser();
            final SearchService.ParseResult parseResult = searchService.parseQuery(user, jql);
            if (!parseResult.isValid())
            {
                // ignore
                return;
            }

            Query query = parseResult.getQuery();

            if (sessionSearchRequest == null)
            {
                sessionSearchRequest = new SearchRequest(query);
            }
            // setQuery() marks the search as dirty, so we only want to call it
            // if the current JQL is actually different from the filter's.
            else if (!query.equals(sessionSearchRequest.getQuery()))
            {
                sessionSearchRequest.setQuery(query);
            }
        }

        sessionSearchRequestManager.setCurrentObject(sessionSearchRequest);

        pagerManager.clearPager();

        SessionPagerFilterManager sessionPagerFilterManager = getPagerFilterManager();
        PagerFilter pager = getPagerFilterManager().getCurrentObject();
        if (null == pager)
        {
            pager = new PagerFilter(startIndex, max);
            sessionPagerFilterManager.setCurrentObject(pager);
        }
        else
        {
            pager.setStart(startIndex);
            pager.setMax(max);
        }
    }

    private SessionPagerFilterManager getPagerFilterManager()
    {
        return sessionSearchObjectManagerFactory.createPagerFilterManager();
    }
}