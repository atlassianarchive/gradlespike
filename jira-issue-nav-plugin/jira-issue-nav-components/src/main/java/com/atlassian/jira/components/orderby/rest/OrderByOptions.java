package com.atlassian.jira.components.orderby.rest;

import java.util.Collections;
import java.util.List;

/**
 * The "order by" options. That is, the options that the user can click on to change the sort.
 *
 * @since v2.0.12
 */
public class OrderByOptions
{
    /**
     * The number of fields that match {@code fieldNameQuery}.
     */
    public Integer matchesCount = null;

    /**
     * The total number of fields.
     */
    public Integer totalCount = null;

    /**
     * The <em>effective</em> maximum number of options returned.
     */
    public Integer maxResults = null;

    /**
     * The fields that the user can click on.
     */
    public List<OrderByOption> fields = Collections.emptyList();


    public OrderByOptions()
    {
    }

    public OrderByOptions fields(List<OrderByOption> fields)
    {
        this.fields = fields;
        return this;
    }

    public OrderByOptions totalCount(Integer total)
    {
        this.totalCount = total;
        return this;
    }

    public OrderByOptions matchesCount(Integer matches)
    {
        this.matchesCount = matches;
        return this;
    }

    public OrderByOptions maxResults(Integer maxResults)
    {
        this.maxResults = maxResults;
        return this;
    }
}
