package com.atlassian.jira.components.issueviewer.action;

import com.atlassian.analytics.api.annotations.Analytics;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.components.issueeditor.action.BaseEditAction;
import com.atlassian.jira.components.issueeditor.action.ContentIdCollector;
import com.atlassian.jira.components.issueviewer.service.ActionUtilsService;
import com.atlassian.jira.components.issueviewer.service.SessionSearchService;
import com.atlassian.jira.components.issueviewer.viewissue.webpanel.WebPanelMapperUtil;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.IssueInputParametersImpl;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.rest.IssueFinder;
import com.atlassian.jira.issue.fields.rest.json.beans.FieldHtmlBean;
import com.atlassian.jira.components.issueviewer.viewissue.IssueFieldProvider;
import com.atlassian.jira.components.issueviewer.viewissue.IssueOperationLinksProvider;
import com.atlassian.jira.components.issueviewer.viewissue.IssueSummaryProvider;
import com.atlassian.jira.components.issueviewer.viewissue.webpanel.IssueWebPanelsBean;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.rest.v2.issue.OpsbarBean;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.UserIssueHistoryManager;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.action.issue.IssueMetadataHelper;
import com.atlassian.plugins.rest.common.json.DefaultJaxbJsonMarshaller;
import com.atlassian.plugins.rest.common.json.JaxbJsonMarshaller;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.log4j.Logger;
import webwork.action.ActionContext;
import webwork.action.ResultException;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.components.issueeditor.action.ActionUtils.setErrorReturnCode;

/**
 * A webwork action to produce JSON with field edit html.  This is an action and not a REST resource mainly because our
 * fields API is still so heavily tied to webwork.  All methods on this action should return JSON content.
 *
 * Note that validation failure logic has been overridden to ensure an action view is not returned as the response
 * is rendered directly.
 *
 * @since 5.0
 */
public class AjaxIssueAction extends BaseEditAction
{
    private static final Logger log = Logger.getLogger(AjaxIssueAction.class);

    private final EventPublisher eventPublisher;
    private final IssueFinder issueFinder;
    private final IssueMetadataHelper issueMetadataHelper;
    private final SessionSearchService sessionSearchService;
    private final UserIssueHistoryManager userIssueHistoryManager;
    private final WebPanelMapperUtil webPanelMapperUtil;
    private final ActionUtilsService actionUtilsService;
    private final IssueOperationLinksProvider issueOperationLinksProvider;
    private final IssueSummaryProvider issueSummaryProvider;

    private boolean singleFieldEdit = false;
    private boolean prefetch = false;
    private boolean loadFields = true;

    private boolean shouldUpdateCurrentProject = true;

    private IssueService.UpdateValidationResult updateValidationResult;

    // Return values
    private IssueFields fields;

    public AjaxIssueAction(
            final EventPublisher eventPublisher,
            final IssueFinder issueFinder,
            final IssueMetadataHelper issueMetadataHelper,
            final IssueService issueService,
            final SessionSearchService sessionSearchService,
            final UserIssueHistoryManager userIssueHistoryManager,
            final WebPanelMapperUtil webPanelMapperUtil,
            final ActionUtilsService actionUtilsService,
            IssueFieldProvider issueFieldProvider,
            IssueOperationLinksProvider issueOperationLinksProvider,
            IssueSummaryProvider issueSummaryProvider
    )
    {
        super(issueService, issueFieldProvider);

        this.eventPublisher = eventPublisher;
        this.issueFinder = issueFinder;
        this.issueMetadataHelper = issueMetadataHelper;
        this.sessionSearchService = sessionSearchService;
        this.userIssueHistoryManager = userIssueHistoryManager;
        this.webPanelMapperUtil = webPanelMapperUtil;
        this.actionUtilsService = actionUtilsService;
        this.issueOperationLinksProvider = issueOperationLinksProvider;
        this.issueSummaryProvider = issueSummaryProvider;
    }

    public String doDefault() throws Exception
    {
        ActionContext.getResponse().setContentType("application/json");

        String idOrKey = null;
        if(issueId != null)
        {
            idOrKey = Long.toString(issueId);
        }
        else if(issueKey != null)
        {
            idOrKey = issueKey;
        }

        com.atlassian.jira.util.ErrorCollection errors = new SimpleErrorCollection();
        MutableIssue issue = (MutableIssue) this.issueFinder.findIssue(idOrKey, errors);
        IssueService.IssueResult result = new IssueService.IssueResult(issue, errors);

        if (!result.isValid() || result.getIssue() == null)
        {
            ErrorCollection errorCollection = ErrorCollection.of(result.getErrorCollection());
            this.fields = new IssueFields(getXsrfToken(), errorCollection);
            ActionContext.getResponse().setStatus(errorCollection.getStatus());
            return JSON();
        }

        // If we are prefetching issue user isn't actually seeing the issue so don't add it to history.
        if (!isPrefetch())
        {
            userIssueHistoryManager.addIssueToHistory(getLoggedInUser(), issue);

            // jra-36659: don't set the selected project if we are looking at the detail view in issue nav
            if (shouldUpdateCurrentProject())
            {
                setSelectedProjectId(issue.getProjectObject().getId());
            }
        }

        populateIssueFields(issue, false, ErrorCollection.of(result.getErrorCollection()));

        return JSON();
    }

    private void populateIssueFields(Issue issue, boolean retainValues, ErrorCollection errorCollection)
    {
        ContentIdCollector contentIdCollector = createContentIdCollector();

        long start = System.nanoTime();
        boolean isEditable = issueService.isEditable(issue, getLoggedInUser());

        List<FieldHtmlBean> editFields;
        if (loadFields && isEditable)
        {
            editFields = issueFieldProvider.getEditFields(getLoggedInUser(), this, this, issue, retainValues, contentIdCollector);
        }
        else
        {
            editFields = Collections.emptyList();
        }
        long fieldEnd = System.nanoTime();

        OpsbarBean opsbarBean = issueOperationLinksProvider.getOperations(issue, contentIdCollector);
        Map<String, String> metadata = issueMetadataHelper.getMetadata(issue, sessionSearchService.getSessionSearch());

        IssueSummaryProvider.Result summaryCheckResult = issueSummaryProvider.check(issue, contentIdCollector);
        IssueBean issueBean = new IssueBean(issue, metadata, opsbarBean, issue.getProjectObject(), issue.getStatusObject(),
                summaryCheckResult.newContentId, summaryCheckResult.changed, isEditable);
        long issueEnd = System.nanoTime();

        IssueWebPanelsBean panels = webPanelMapperUtil.create(issue, this, contentIdCollector);
        long panelsEnd = System.nanoTime();

        eventPublisher.publish(new IssueRenderTime(fieldEnd - start, issueEnd - fieldEnd, panelsEnd - issueEnd));

        fields = new IssueFields(getXsrfToken(), errorCollection, editFields,
                issueBean, null, panels, contentIdCollector.getRemovedContentIds());
    }

    protected void doValidation()
    {
        ActionContext.getResponse().setContentType("application/json");

        final IssueService.IssueResult result = issueService.getIssue(getLoggedInUser(), issueId);
        final Issue issue = result.getIssue();
        if (!result.isValid() || result.getIssue() == null)
        {
            addErrorCollection(result.getErrorCollection());
            this.fields = new IssueFields(getXsrfToken(), ErrorCollection.of(result.getErrorCollection()));
            setErrorReturnCode(getLoggedInUser());
            JSON();
            return;
        }

        final IssueInputParameters issueInputParameters = new IssueInputParametersImpl(ActionContext.getParameters());
        if (singleFieldEdit)
        {
            issueInputParameters.setRetainExistingValuesWhenParameterNotProvided(singleFieldEdit, true);
        }
        updateValidationResult = issueService.validateUpdate(getLoggedInUser(), issue.getId(), issueInputParameters);
        setFieldValuesHolder(updateValidationResult.getFieldValuesHolder());
        if (!updateValidationResult.isValid())
        {
            final com.atlassian.jira.util.ErrorCollection errors = updateValidationResult.getErrorCollection();
            addErrorCollection(errors);
            populateIssueFields(issue, true, ErrorCollection.of(errors));
            setErrorReturnCode(getLoggedInUser());
            JSON();
        }
    }

    /**
     * Nasty hack to prevent webwork from returning HTML.
     *
     * @throws ResultException
     */
    protected void validate() throws ResultException
    {
        doValidation();

        if (invalidInput())
        {
            throw new ResultException(NONE);
        }
    }
    private String JSON()
    {
        try
        {
            ActionContext.getResponse().getWriter().append(getJson());
            return null;
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    @RequiresXsrfCheck
    protected String doExecute() throws Exception
    {
        try
        {
            IssueService.IssueResult result = issueService.update(getLoggedInUser(), updateValidationResult);
            result = issueService.getIssue(getLoggedInUser(), getIssueId());
            Issue issue = result.getIssue();

            if (!result.isValid())
            {
                addErrorCollection(result.getErrorCollection());
                this.fields = new IssueFields(getXsrfToken(), ErrorCollection.of(result.getErrorCollection()));
                setErrorReturnCode(getLoggedInUser());
                return JSON();
            }

            userIssueHistoryManager.addIssueToHistory(getLoggedInUser(), issue);

            populateIssueFields(issue, false, ErrorCollection.of(result.getErrorCollection()));

            return JSON();
        }
        catch (Throwable e)
        {
            addErrorMessage(getText("admin.errors.issues.exception.occured", e));
            log.error("Exception occurred editing issue: " + e, e);
            this.fields = new IssueFields(getXsrfToken(), ErrorCollection.of(getText("admin.errors.issues.exception.occured", e)));
            ActionContext.getResponse().setStatus(HttpStatus.SC_BAD_REQUEST);
            return JSON();
        }
    }

    public String getJson()
    {
        final JaxbJsonMarshaller marshaller = new DefaultJaxbJsonMarshaller();
        return marshaller.marshal(fields);
    }

    public boolean isSingleFieldEdit()
    {
        return singleFieldEdit;
    }

    public void setSingleFieldEdit(boolean singleFieldEdit)
    {
        this.singleFieldEdit = singleFieldEdit;
    }

    public boolean isPrefetch()
    {
        return prefetch;
    }

    public void setPrefetch(final boolean prefetch)
    {
        this.prefetch = prefetch;
    }

    public void setLoadFields(boolean loadFields)
    {
        this.loadFields = loadFields;
    }

    public void setShouldUpdateCurrentProject(boolean shouldUpdateCurrentProject)
    {
        this.shouldUpdateCurrentProject = shouldUpdateCurrentProject;
    }

    public boolean shouldUpdateCurrentProject()
    {
        return shouldUpdateCurrentProject;
    }

    @Analytics("kickass.issueRenderTime")
    public static class IssueRenderTime
    {
        public final double fieldsTimeMs;
        public final double issueTimeMs;
        public final double panelsTimeMs;

        public IssueRenderTime(long fieldsTimeNano, long issueTimeNano, long panelsTimeNano)
        {
            this.fieldsTimeMs = fieldsTimeNano / (1000.0 * 1000.0);
            this.issueTimeMs = issueTimeNano / (1000.0 * 1000.0);
            this.panelsTimeMs = panelsTimeNano / (1000.0 * 1000.0);
        }

        public double getFieldsTimeMs()
        {
            return fieldsTimeMs;
        }

        public double getIssueTimeMs()
        {
            return issueTimeMs;
        }

        public double getPanelsTimeMs()
        {
            return panelsTimeMs;
        }
    }
}
