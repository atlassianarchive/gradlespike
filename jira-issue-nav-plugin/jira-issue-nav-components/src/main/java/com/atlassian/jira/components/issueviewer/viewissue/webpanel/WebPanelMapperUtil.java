package com.atlassian.jira.components.issueviewer.viewissue.webpanel;

import com.atlassian.jira.components.issueeditor.action.ContentIdCollector;
import com.atlassian.jira.issue.Issue;
import webwork.action.Action;

/**
 * Returns a mapping of panel key -> panel html in order of webpanels.
 * @since v5.1
 */
public interface WebPanelMapperUtil
{
    IssueWebPanelsBean create(Issue issue, Action action);

    IssueWebPanelsBean create(Issue issue, Action action, ContentIdCollector contentIdCollector);
}
