package com.atlassian.jira.components.issueviewer.service;

import com.atlassian.jira.components.issueviewer.service.SystemFilter;
import com.atlassian.jira.components.issueviewer.service.SystemFilterBean;
import com.atlassian.jira.components.issueviewer.service.SystemFilterService;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * @since v5.2
 */
@ExportAsService
@Named
public class DefaultSystemFilterService implements SystemFilterService
{
    @Override
    public SystemFilter getById(Long id)
    {
        final SystemFilter[] values = SystemFilter.values();
        for (SystemFilter value : values)
        {
            if (value.getId().equals(id))
            {
                return value;
            }
        }
        return null;
    }

    @Override
    public List<SystemFilterBean> getAllAsBeans()
    {
        final SystemFilter[] values = SystemFilter.values();
        final List<SystemFilterBean> beans = new ArrayList<SystemFilterBean>();
        for (SystemFilter value : values)
        {
            beans.add(new SystemFilterBean(value));
        }
        return beans;
    }
}
