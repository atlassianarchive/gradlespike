package com.atlassian.jira.components.query;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.SearchContextImpl;
import com.atlassian.jira.issue.search.managers.IssueSearcherManager;
import com.atlassian.jira.issue.search.searchers.IssueSearcher;
import com.atlassian.jira.issue.transport.FieldValuesHolder;
import com.atlassian.jira.issue.transport.impl.FieldValuesHolderImpl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.query.Query;
import org.apache.commons.lang.StringUtils;

import javax.inject.Named;
import javax.inject.Inject;
import java.util.Collections;

/**
 * Default implementation of SearchContextHelper
 *
 * @since v5.1
 */
@ExportAsService
@Named
public class DefaultSearchContextHelper implements SearchContextHelper
{
    private final JiraAuthenticationContext authenticationContext;
    private final SearchService searchService;
    private final IssueSearcherManager issueSearcherManager;

    @Inject
    public DefaultSearchContextHelper(
            JiraAuthenticationContext authenticationContext,
            SearchService searchService,
            IssueSearcherManager issueSearcherManager)
    {
        this.authenticationContext = authenticationContext;
        this.searchService = searchService;
        this.issueSearcherManager = issueSearcherManager;
    }

    public SearchContextWithFieldValues getSearchContextWithFieldValuesFromJqlString(final String query)
    {
        if (StringUtils.isNotBlank(query))
        {
            final SearchService.ParseResult jqlQuery = searchService.parseQuery(getLoggedInUser(), query);
            if (jqlQuery.isValid())
            {
                SearchContext searchContext = searchService.getSearchContext(getLoggedInUser(), jqlQuery.getQuery());
                return getSearchContextWithFieldValuesFromQuery(searchContext, jqlQuery.getQuery());
            }
        }
        return new SearchContextWithFieldValues(createSearchContext(), new FieldValuesHolderImpl());
    }

    public SearchContextWithFieldValues getSearchContextWithFieldValuesFromQuery(SearchContext searchContext, Query query)
    {
        FieldValuesHolder fieldValuesHolder = createFieldValuesHolderFromQuery(query, searchContext);
        return new SearchContextWithFieldValues(searchContext, fieldValuesHolder);
    }

    public SearchContext getSearchContextFromJqlString(final String query)
    {
        if (StringUtils.isNotBlank(query))
        {
            final SearchService.ParseResult jqlQuery = searchService.parseQuery(getLoggedInUser(), query);
            if (jqlQuery.isValid())
            {
                return searchService.getSearchContext(getLoggedInUser(), jqlQuery.getQuery());
            }
        }
        return createSearchContext();
    }

    private FieldValuesHolder createFieldValuesHolderFromQuery(final Query query, final SearchContext searchContext)
    {
        FieldValuesHolder fieldValuesHolder = new FieldValuesHolderImpl();
        for (IssueSearcher<?> searcher : this.issueSearcherManager.getAllSearchers())
        {
            searcher.getSearchInputTransformer().populateFromQuery(getLoggedInUser(), fieldValuesHolder, query, searchContext);
        }
        return fieldValuesHolder;
    }

    private SearchContext createSearchContext()
    {
        return new SearchContextImpl(Collections.emptyList(), null, null);
    }

    private User getLoggedInUser()
    {
        return authenticationContext.getLoggedInUser();
    }
}
