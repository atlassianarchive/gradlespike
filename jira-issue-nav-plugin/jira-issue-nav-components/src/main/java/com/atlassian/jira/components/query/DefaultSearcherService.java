package com.atlassian.jira.components.query;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.SearchableField;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.managers.IssueSearcherManager;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.issue.search.searchers.IssueSearcher;
import com.atlassian.jira.issue.search.searchers.IssueSearcherPanelMap;
import com.atlassian.jira.issue.search.searchers.information.SearcherInformation;
import com.atlassian.jira.issue.search.searchers.renderer.SearchRenderer;
import com.atlassian.jira.issue.search.searchers.transformer.SearchInputTransformer;
import com.atlassian.jira.issue.transport.ActionParams;
import com.atlassian.jira.issue.transport.FieldValuesHolder;
import com.atlassian.jira.issue.transport.impl.ActionParamsImpl;
import com.atlassian.jira.issue.transport.impl.FieldValuesHolderImpl;
import com.atlassian.jira.jql.util.JqlStringSupport;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.Clause;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import webwork.action.Action;

import javax.inject.Named;
import javax.inject.Inject;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.template.TemplateSources.file;

/**
 * Kickass search utility service.
 *
 * @since v5.1
 */
@ExportAsService
@Named
public class DefaultSearcherService implements SearcherService
{
    private static final String JQL_PREFIX = "__jql_";
    private static final String JQL_CONTEXT = "jqlContext";

    private final SearchContextHelper searchContextHelper;
    private final IssueSearcherManager issueSearcherManager;
    private final SearchHandlerManager searchHandlerManager;
    private final JqlStringSupport jqlStringSupport;
    private final JiraAuthenticationContext authenticationContext;
    private final SearchService searchService;
    private final VelocityTemplatingEngine templatingEngine;
    private RecentSearchersService recentSearchersService;
    private static final String JQL_INVALID_ERROR_MESSAGE = "jqlInvalid";
    private static final String JQL_TOO_COMPLEX_ERROR_MESSAGE = "jqlTooComplex";

    private final EmptySearchResultHtmlProvider emptySearchResultHtmlProvider;


    @Inject
    public DefaultSearcherService(final SearchContextHelper searchContextHelper,
                                  final IssueSearcherManager issueSearcherManager,
                                  final JqlStringSupport jqlStringSupport,
                                  final JiraAuthenticationContext authenticationContext, final SearchHandlerManager searchHandlerManager,
                                  final SearchService searchService, final VelocityTemplatingEngine templatingEngine, final RecentSearchersService recentSearchersService)
    {
        this.searchContextHelper = searchContextHelper;
        this.issueSearcherManager = issueSearcherManager;
        this.jqlStringSupport = jqlStringSupport;
        this.authenticationContext = authenticationContext;
        this.searchHandlerManager = searchHandlerManager;
        this.searchService = searchService;
        this.templatingEngine = templatingEngine;
        this.recentSearchersService = recentSearchersService;
        emptySearchResultHtmlProvider = new EmptySearchResultHtmlProvider();
    }

    @Override
    public ServiceOutcome<SearchRendererValueResults> getViewHtml(JiraWebActionSupport action, Map<String, String[]> params)
    {
        final User user = authenticationContext.getLoggedInUser();
        final Collection<IssueSearcher<?>> searchers = issueSearcherManager.getAllSearchers();

        final ServiceOutcome<Map<String, SearchRendererHolder>> clausesOutcome = generateQuery(params, user, searchers);
        if (!clausesOutcome.isValid())
        {
            return new ServiceOutcomeImpl<SearchRendererValueResults>(clausesOutcome.getErrorCollection());
        }
        Map<String, SearchRendererHolder> clauses = clausesOutcome.getReturnedValue();
        String[] jqlContext = params.get(JQL_CONTEXT);
        final Query query = buildQuery(clauses);
        SearchContext searchContext;
        if (jqlContext != null)
        {
            searchContext = searchContextHelper.getSearchContextFromJqlString(jqlContext[0]);
        }
        else
        {
            searchContext = searchService.getSearchContext(user, query);
        }


        return getValueResults(false, action, user, searchers, clauses, query, searchContext);
    }

    @Override
    public ServiceOutcome<SearchResults> search(JiraWebActionSupport action, Map<String, String[]> params, Long filterId)
    {
        final User user = authenticationContext.getLoggedInUser();
        final Collection<IssueSearcher<?>> searchers = issueSearcherManager.getAllSearchers();

        final ServiceOutcome<Map<String, SearchRendererHolder>> clausesOutcome = generateQuery(params, user, searchers);
        if (!clausesOutcome.isValid())
        {
            return new ServiceOutcomeImpl<SearchResults>(clausesOutcome.getErrorCollection());
        }
        Map<String, SearchRendererHolder> clauses = clausesOutcome.getReturnedValue();
        final Query query = buildQuery(clauses);

        SearchContext searchContext = searchService.getSearchContext(user, query);
        return getSearchResults(true, action, user, searchers, clauses, query, searchContext);
    }

    @Override
    public ServiceOutcome<SearchResults> searchWithJql(JiraWebActionSupport action, String jqlContext, Long filterId)
    {
        final User user = authenticationContext.getLoggedInUser();
        final SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlContext);

        if (!parseResult.isValid())
        {
            ErrorCollection errors = new SimpleErrorCollection();
            errors.addErrorMessage(JQL_INVALID_ERROR_MESSAGE);

            for (String error : parseResult.getErrors().getErrorMessages())
            {
                errors.addError("jql", error);
            }

            if (!errors.hasAnyErrors())
            {
                errors.addError("jql", authenticationContext.getI18nHelper().getText("jql.parse.unknown.no.pos"));
            }

            return new ServiceOutcomeImpl<SearchResults>(errors);
        }

        // Is the query too complex to be expressed with searchers?
        final Query query = parseResult.getQuery();
        if (!searchService.doesQueryFitFilterForm(user, query))
        {
            return ServiceOutcomeImpl.error(JQL_TOO_COMPLEX_ERROR_MESSAGE);
        }

        final Collection<IssueSearcher<?>> searchers = issueSearcherManager.getAllSearchers();
        final SearchContext searchContext = searchService.getSearchContext(user, parseResult.getQuery());
        final Map<String, SearchRendererHolder> clauses = generateQuery(searchContext, user, query, searchers);

        return getSearchResults(true, action, user, searchers, clauses, query, searchContext);
    }

    private ServiceOutcome<SearchResults> getSearchResults(Boolean includePrimes, JiraWebActionSupport action, User user, Collection<IssueSearcher<?>> searchers,
            Map<String, SearchRendererHolder> clauses, Query query, SearchContext searchContext)
    {
        final ServiceOutcome<SearchRendererValueResults> outcome = getValueResults(includePrimes, action, user, searchers, clauses, query, searchContext);
        if (!outcome.isValid())
        {
            return new ServiceOutcomeImpl<SearchResults>(outcome.getErrorCollection());
        }

        Searchers renderableSearchers = getSearchers(searchContext, user);

        return ServiceOutcomeImpl.ok(new SearchResults(renderableSearchers, outcome.getReturnedValue()));
    }

    @Override
    public ServiceOutcome<String> getEditHtml(String searcherId, String jqlContext, JiraWebActionSupport action)
    {
        final User user = authenticationContext.getLoggedInUser();
        SearchContextHelper.SearchContextWithFieldValues searchContextWithFieldValues = searchContextHelper.getSearchContextWithFieldValuesFromJqlString(jqlContext);

        return getEditHtml(searcherId, action, user, searchContextWithFieldValues, createDisplayParams());
    }

    @Override
    public Searchers getSearchers(String jqlContext)
    {
        SearchContext searchContext = searchContextHelper.getSearchContextFromJqlString(jqlContext);

        return getSearchers(searchContext, authenticationContext.getLoggedInUser());
    }

    private ServiceOutcome<SearchRendererValueResults> getValueResults(Boolean includePrimes, JiraWebActionSupport action, User user, Collection<IssueSearcher<?>> searchers, Map<String, SearchRendererHolder> clauses, Query query, SearchContext searchContext)
    {
        Map<String, String> displayParams = createDisplayParams();
        SearchRendererValueResults results = new SearchRendererValueResults();
        final I18nHelper i18nHelper = authenticationContext.getI18nHelper();

        for (IssueSearcher issueSearcher : searchers)
        {
            String id = issueSearcher.getSearchInformation().getId();
            SearchRendererHolder clause = clauses.get(id);
            SearchRenderer searchRenderer = issueSearcher.getSearchRenderer();
            String name = i18nHelper.getText(issueSearcher.getSearchInformation().getNameKey());
            boolean validSearcher = searchRenderer.isShown(user, searchContext);
            if ((includePrimes && isPrime(issueSearcher) && validSearcher) || searchRenderer.isRelevantForQuery(user, query))
            {


                String viewHtml = null;
                String editHtml;

                SearchInputTransformer searchInputTransformer = issueSearcher.getSearchInputTransformer();

                FieldValuesHolder params;
                if (null == clause)
                {
                    params = new FieldValuesHolderImpl();
                }
                else if (clause.valid)
                {
                    params = clause.params;
                }
                else
                {
                    params = new FieldValuesHolderImpl();
                    searchInputTransformer.populateFromQuery(user, params, new QueryImpl(clause.clause), searchContext);
                }

                searchInputTransformer.validateParams(user, searchContext, params, i18nHelper, action);

                SearchContextHelper.SearchContextWithFieldValues searchContextWithFieldValues;
                if (searchRenderer.isRelevantForQuery(user, query))
                {
                    searchContextWithFieldValues = new SearchContextHelper.SearchContextWithFieldValues(searchContext, params);
                    viewHtml = searchRenderer.getViewHtml(user, searchContext, params, displayParams, action);
                }
                else
                {
                    searchContextWithFieldValues = searchContextHelper.getSearchContextWithFieldValuesFromQuery(searchContext, query);
                }


                ServiceOutcome<String> outcome = getEditHtml(id, action, user, searchContextWithFieldValues, displayParams);
                if (outcome.isValid())
                {
                    editHtml = outcome.getReturnedValue();
                }
                else
                {
                    return new ServiceOutcomeImpl<SearchRendererValueResults>(outcome.getErrorCollection());
                }

                String jql = clause != null ? jqlStringSupport.generateJqlString(clause.clause) : null;
                results.put(id, new SearchRendererValue(name, jql, viewHtml, editHtml, validSearcher, validSearcher));
            }
            else if (clause != null && !clause.valid)
            {
                results.put(id, new SearchRendererValue(name, jqlStringSupport.generateJqlString(clause.clause), null, null, false, validSearcher));
            }
        }

        return ServiceOutcomeImpl.ok(results);
    }

    private Map<String, SearchRendererHolder> generateQuery(SearchContext searchContext, User user, Query query, Collection<IssueSearcher<?>> searchers)
    {
        Map<String, SearchRendererHolder> clauses = Maps.newHashMap();

        // TODO: copy from DefaultSearchRequestFactory.getClausesFromSearchers
        for (IssueSearcher issueSearcher : searchers)
        {
            SearchInputTransformer searchInputTransformer = issueSearcher.getSearchInputTransformer();
            FieldValuesHolder fieldValuesHolder = new FieldValuesHolderImpl();


            searchInputTransformer.populateFromQuery(user, fieldValuesHolder, query, searchContext);
            Clause clause = searchInputTransformer.getSearchClause(user, fieldValuesHolder);
            if (null != clause)
            {
                String id = issueSearcher.getSearchInformation().getId();
                clauses.put(id, SearchRendererHolder.valid(clause, fieldValuesHolder));
            }
        }

        return clauses;
    }

    /**
     * @return invalid if any __jql parameter contains invalid jql
     */
    private ServiceOutcome<Map<String, SearchRendererHolder>> generateQuery(Map<String, String[]> params, User user, Collection<IssueSearcher<?>> searchers)
    {
        Map<String, SearchRendererHolder> clauses = Maps.newHashMap();

        ActionParams actionParams = getActionParameters(params);
        Map<String, String[]> jqlParams = getJqlParameters(params);

        // TODO: copy from DefaultSearchRequestFactory.getClausesFromSearchers
        for (IssueSearcher issueSearcher : searchers)
        {
            String id = issueSearcher.getSearchInformation().getId();
            SearchInputTransformer searchInputTransformer = issueSearcher.getSearchInputTransformer();
            // For invalid clauses, we send jql as a param as we are not able to retrieve form params from an invalid clause (as we don't
            // call getEditHtml() if a clause is invalid for context.
            if (jqlParams.containsKey(id))
            {
                // We will only ever be sent the 0th result
                final SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlParams.get(id)[0]);
                if (parseResult.isValid())
                {
                    Clause clause = parseResult.getQuery().getWhereClause();
                    clauses.put(id, SearchRendererHolder.invalid(clause));
                }
                else
                {
                    // ignore invalid jql query from invalid searcher
                    ErrorCollection errors = new SimpleErrorCollection();
                    errors.addErrorMessages(parseResult.getErrors().getErrorMessages());
                    return ServiceOutcomeImpl.from(errors, null);
                }
            }
            else
            {
                FieldValuesHolder fieldValuesHolder = new FieldValuesHolderImpl();
                searchInputTransformer.populateFromParams(user, fieldValuesHolder, actionParams);
                Clause clause = searchInputTransformer.getSearchClause(user, fieldValuesHolder);
                if (null != clause)
                {
                    clauses.put(id, SearchRendererHolder.valid(clause, fieldValuesHolder));
                }
            }
        }

        return ServiceOutcomeImpl.ok(clauses);
    }

    private Query buildQuery(Map<String, SearchRendererHolder> clauses)
    {
        final Collection<Clause> actualClauses = Collections2.transform(clauses.values(), new Function<SearchRendererHolder, Clause>() {
            @Override
            public Clause apply(SearchRendererHolder input) {
                return input.clause;
            }
        });

        Clause jqlClause = clauses.size() > 0 ? new AndClause(actualClauses) : null;
        return new QueryImpl(jqlClause);
    }

    /**
     * Container for returned values
     */
    private static class SearchRendererHolder
    {
        public final boolean valid;
        public final Clause clause;
        public final FieldValuesHolder params;

        public static SearchRendererHolder invalid(Clause clause)
        {
            return new SearchRendererHolder(false, clause, null);
        }

        public static SearchRendererHolder valid(Clause clause, FieldValuesHolder params)
        {
            return new SearchRendererHolder(true, clause, params);
        }

        private SearchRendererHolder(boolean valid, Clause clause, FieldValuesHolder params)
        {
            this.valid = valid;
            this.clause = clause;
            this.params = params;
        }
    }


    private boolean isPrime(IssueSearcher searcher)
    {
        PrimarySearchers[] primes = PrimarySearchers.values();
        for (PrimarySearchers prime : primes)
        {
            if (prime.getId().equals(searcher.getSearchInformation().getId()))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * @return All searchers that should be visible to {@code user} in the given context, grouped as they would be on
     *         the view issue page (panels).
     */
    private Searchers getSearchers(SearchContext searchContext, User user)
    {
        I18nHelper i18nHelper = authenticationContext.getI18nHelper();
        Map<String, FilteredSearcherGroup> searcherGroups = new HashMap<String, FilteredSearcherGroup>();


        Map<String, Long> recentSearchers = recentSearchersService.getRecentSearchers(user);

        for (IssueSearcher<?> searcher : searchHandlerManager.getAllSearchers())
        {
            IssueSearcherPanelMap.Panel panel = IssueSearcherPanelMap.getPanel(searcher.getClass());
            FilteredSearcherGroup group = searcherGroups.get(panel.name());

            if (group == null)
            {
                group = new FilteredSearcherGroup(panel.name());
                if (panel.getTitleKey() != null)
                {
                    group.setTitle(i18nHelper.getText(panel.getTitleKey()));
                }

                searcherGroups.put(panel.name(), group);
            }

            SearcherInformation<?> searcherInfo = searcher.getSearchInformation();
            SearchableField field = searcherInfo.getField();
            String fieldKey = "text";

            if (field != null) {
                if (field instanceof CustomField) {
                    fieldKey = ((CustomField) field).getCustomFieldType().getKey();
                } else {
                    fieldKey = field.getNameKey();
                }
            }

            String searcherId = searcherInfo.getId();
            group.addSearcher(new Searcher(searcherId, i18nHelper.getText(searcherInfo.getNameKey()), fieldKey, searcher.getSearchRenderer().isShown(user, searchContext), recentSearchers.get(searcherId)));
        }

        Searchers searchers = new Searchers();
        searchers.addGroups(searcherGroups.values());
        return searchers;
    }

    private ServiceOutcome<String> getEditHtml(String searcherId, Action action, User user, SearchContextHelper.SearchContextWithFieldValues searchContextWithFieldValues, Map<String, String> displayParams)
    {
        final I18nHelper i18nHelper = authenticationContext.getI18nHelper();

        if (null == searcherId)
        {
            return ServiceOutcomeImpl.error(i18nHelper.getText("issues.components.query.searcher.no.search.renderer"));
        }

        IssueSearcher<?> issueSearcher = issueSearcherManager.getSearcher(searcherId);
        if (null == issueSearcher)
        {
            return ServiceOutcomeImpl.error(i18nHelper.getText("issues.components.query.searcher.no.search.renderer"));
        }

        recentSearchersService.addRecentSearcher(user, issueSearcher);

        String editHtml = issueSearcher.getSearchRenderer().getEditHtml(user, searchContextWithFieldValues.searchContext, searchContextWithFieldValues.fieldValuesHolder, displayParams, action);
        if (StringUtils.isEmpty(editHtml))
        {
            editHtml = emptySearchResultHtmlProvider.getHtml(searcherId);
        }

        return ServiceOutcomeImpl.ok(editHtml);
    }

    private Map<String, String> createDisplayParams()
    {
        Map<String, String> displayParams = new HashMap<String, String>();
        displayParams.put("theme", "aui");
        displayParams.put("checkboxmultiselect", "on");
        return displayParams;
    }

    private ActionParams getActionParameters(Map<String, String[]> params)
    {
        return new ActionParamsImpl(Maps.filterKeys(params, new Predicate<String>()
        {
            @Override
            public boolean apply(String input)
            {
                return !input.startsWith(JQL_PREFIX);
            }
        }));
    }

    private Map<String, String[]> getJqlParameters(Map<String, String[]> params)
    {
        Map<String, String[]> jqlParams = Maps.newHashMap();
        for (Map.Entry<String, String[]> entry : params.entrySet())
        {
            if (entry.getKey().startsWith(JQL_PREFIX))
            {
                jqlParams.put(entry.getKey().substring(JQL_PREFIX.length()), entry.getValue());
            }
        }
        return jqlParams;
    }


    private class EmptySearchResultHtmlProvider
    {
        private final Map<String, MessageProvider> providers = Maps.newHashMap();

        EmptySearchResultHtmlProvider()
        {
            providers.put("project", new MessageProvider()
            {
                @Override
                public String getMessage()
                {
                    return authenticationContext.getI18nHelper().getText("issues.components.query.searcher.no.projects.found");
                }
            });
        }

        String getHtml(String searcherId)
        {
            if (!providers.containsKey(searcherId))
            {
                return "";
            }

            String message = providers.get(searcherId).getMessage();

            return templatingEngine.render(file("query/templates/ka-no-results-message.vm"))
                    .applying(ImmutableMap.<String, Object>of("messageHtml", message))
                    .asHtml();
        }
    }

    private interface MessageProvider
    {
        String getMessage();
    }
}

