package com.atlassian.jira.components.util;

import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.query.Query;
import com.google.common.annotations.VisibleForTesting;

import java.util.List;
import java.util.Map;

public interface SortJqlGenerator {
    @VisibleForTesting
    Map<String, String> generateColumnSortJql(Query query, List<NavigableField> fields);
}
