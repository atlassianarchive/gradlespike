package com.atlassian.jira.components.issueviewer.service;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.sal.api.component.ComponentLocator;
import com.google.common.collect.Lists;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 * @since v5.2
 */
public class SystemFilterBean
{
    @XmlElement
    public final Long id;
    @XmlElement
    public final String name;
    @XmlElement
    public final String jql;
    @XmlElement
    public final boolean isSystem = true;
    @XmlElement
    public final List<String> sharePermissions = Lists.newArrayList();
    @XmlElement
    public final boolean requiresLogin;

    public SystemFilterBean(SystemFilter systemFilter)
    {
        final JiraAuthenticationContext i18n = ComponentLocator.getComponent(JiraAuthenticationContext.class);
        id = systemFilter.getId();
        name = i18n.getI18nHelper().getText(systemFilter.getName());
        jql = systemFilter.getJql();
        requiresLogin = systemFilter.isRequiresLogin();
    }
}
