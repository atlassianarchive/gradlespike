package com.atlassian.jira.components.issueviewer.service;

import com.atlassian.query.order.SearchSort;
import com.atlassian.query.order.SortOrder;
import com.google.common.base.Function;
import com.google.common.collect.Maps;

import java.util.Arrays;
import java.util.Map;

public enum SystemFilter
{
    MY_OPEN_ISSUES(
            -1L,
            "issue.nav.filters.my.open.issues",
            "assignee = currentUser() AND resolution = Unresolved",
            "updatedDate", SortOrder.DESC,
            true
    ),
    REPORTED_BY_ME(
            -2L,
            "issue.nav.filters.reported.by.me",
            "reporter = currentUser()",
            "createdDate", SortOrder.DESC,
            true
    ),
    RECENTLY_VIEWED(
            -3L,
            "issue.nav.filters.recently.viewed",
            "issuekey in issueHistory()",
            "lastViewed", SortOrder.DESC,
            false
    ),
    ALL_ISSUES(
            -4L,
            "issue.nav.filters.all.issues",
            null,
            "createdDate", SortOrder.DESC,
            false
    );

    private static final Map<Long, SystemFilter> SYSTEM_FILTER_MAP = Maps.uniqueIndex(Arrays.asList(SystemFilter.values()), new Function<SystemFilter, Long>()
    {
        @Override
        public Long apply(SystemFilter input)
        {
            return input.getId();
        }
    });

    private final Long id;
    private final String name;
    private final String whereClause;
    private final SearchSort searchSort;
    private final boolean requiresLogin;

    private SystemFilter(Long id, String name, String whereClause, String orderByField, SortOrder order, boolean requiresLogin)
    {
        this.id = id;
        this.name = name;
        this.whereClause = whereClause;
        this.searchSort = new SearchSort(orderByField, order);
        this.requiresLogin = requiresLogin;
    }

    public Long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getJql()
    {
        StringBuilder sb = new StringBuilder();
        if (whereClause != null)
        {
            sb.append(whereClause).append(" ");
        }

        sb.append("ORDER BY ").append(searchSort);

        return sb.toString();
    }

    public SearchSort getSearchSort()
    {
        return searchSort;
    }

    public boolean isRequiresLogin()
    {
        return requiresLogin;
    }

    public static boolean isSystemFilter(Long id)
    {
        return getSystemFilterById(id) != null;
    }

    public static SystemFilter getSystemFilterById(Long id)
    {
        return SYSTEM_FILTER_MAP.get(id);
    }
}
