package com.atlassian.jira.components.issueviewer.viewissue.webpanel;

import com.atlassian.jira.components.issueeditor.action.ContentIdCollector;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.util.IssueWebPanelRenderUtil;
import com.atlassian.jira.components.issueviewer.viewissue.IssueOperationLinksProvider;
import com.atlassian.jira.rest.v2.common.SimpleLinkBean;
import com.atlassian.jira.rest.v2.issue.LinkGroupBean;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.component.ContentRenderingInstruction;
import com.atlassian.jira.web.component.ModuleWebComponent;
import com.atlassian.jira.web.component.ModuleWebComponentFields;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import org.apache.commons.lang.StringUtils;
import webwork.action.Action;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Returns a mapping of panel key -> panel html in order of webpanels.
 * @since v5.1
 */
@ExportAsService
@Named
public class WebPanelMapperUtilImpl implements WebPanelMapperUtil
{
    private final WebInterfaceManager webInterfaceManager;
    private final ModuleWebComponentFields moduleWebComponentFields;
    private final JiraAuthenticationContext authenticationContext;
    private final ModuleWebComponent moduleWebComponent;
    private final ContentRenderingInstructionProvider contentRenderingInstructionProvider;

    @Inject
    public WebPanelMapperUtilImpl(WebInterfaceManager webInterfaceManager, ModuleWebComponentFields moduleWebComponentFields,
                                  JiraAuthenticationContext authenticationContext, ModuleWebComponent moduleWebComponent,
                                  ContentRenderingInstructionProvider contentRenderingInstructionProvider)
    {
        this.webInterfaceManager = webInterfaceManager;
        this.moduleWebComponentFields = moduleWebComponentFields;
        this.authenticationContext = authenticationContext;
        this.moduleWebComponent = moduleWebComponent;
        this.contentRenderingInstructionProvider = contentRenderingInstructionProvider;
    }

    @Override
    public IssueWebPanelsBean create(Issue issue, Action action)
    {
        return create(issue, action, null);
    }

    @Override
    public IssueWebPanelsBean create(Issue issue, Action action, ContentIdCollector contentIdCollector)
    {
        final IssueWebPanelRenderUtil issueWebPanelRenderUtil = new IssueWebPanelRenderUtil(authenticationContext.getLoggedInUser(),
                issue, action, this.webInterfaceManager, this.moduleWebComponent);

        if (contentIdCollector != null && contentIdCollector.getLastReadTime() != null)
        {
            issueWebPanelRenderUtil.getWebPanelContext().put("lastReadTime", new Date(contentIdCollector.getLastReadTime()));
        }

        List<WebPanelBean> leftPanels = mapAndRenderPanels(issueWebPanelRenderUtil, issueWebPanelRenderUtil.getLeftWebPanels(), contentIdCollector);
        List<WebPanelBean> rightPanels = mapAndRenderPanels(issueWebPanelRenderUtil, issueWebPanelRenderUtil.getRightWebPanels(), contentIdCollector);
        List<WebPanelBean> infoPanels = mapAndRenderPanels(issueWebPanelRenderUtil, issueWebPanelRenderUtil.getInfoWebPanels(), contentIdCollector);

        return new IssueWebPanelsBean(leftPanels, rightPanels, infoPanels);
    }

    List<WebPanelBean> mapAndRenderPanels(IssueWebPanelRenderUtil issueWebPanelRenderUtil,
                                          List<WebPanelModuleDescriptor> webPanels,
                                          ContentIdCollector contentIdCollector)
    {
        final List<WebPanelBean> panels = new ArrayList<WebPanelBean>();

        if (contentIdCollector == null)
        {
            for (WebPanelModuleDescriptor webPanel : webPanels)
            {
                WebPanelBean bean = getWebPanelBeanBuilder(issueWebPanelRenderUtil, webPanel).buildWithHtml();
                if (bean != null)
                {
                    panels.add(bean);
                }
            }
        }
        else
        {
            boolean loadAll = contentIdCollector.mustLoadAll();

            for (WebPanelModuleDescriptor webPanel : webPanels)
            {
                WebPanelBean bean = getWebPanelBeanBuilder(issueWebPanelRenderUtil, webPanel).buildWithoutHtml();

                boolean render = true;

                String currentContentId = contentIdCollector.getCurrentPanelContentId(webPanel.getCompleteKey());
                String newContentId = null;

                // Check for custom content ID-s.
                ContentRenderingInstruction instruction = contentRenderingInstructionProvider.get(issueWebPanelRenderUtil.getWebPanelContext(), webPanel);

                if (instruction != null)
                {
                    if (instruction.isDontRender() && currentContentId != null)
                    {
                        // Has custom content ID provider and it instructs to not reload the panel.
                        render = false;
                        newContentId = currentContentId;
                    }
                    else if (!instruction.isRenderHtml() && instruction.getContentId() != null)
                    {
                        newContentId = calculateFullContentId(instruction.getContentId(), bean, contentIdCollector);

                        if (!loadAll && newContentId.equals(currentContentId))
                        {
                            // Has custom content ID provider and the value has not changed.
                            render = false;
                        }
                    }
                }

                if (render)
                {
                    String panelHtml = issueWebPanelRenderUtil.renderHeadlessPanel(webPanel);
                    if (StringUtils.isNotBlank(panelHtml))
                    {
                        bean = new WebPanelBean.Builder(bean).html(panelHtml).build();

                        boolean addPanel = true;

                        if (newContentId == null)
                        {
                            newContentId = calculateFullContentId(panelHtml, bean, contentIdCollector);

                            if (!loadAll && newContentId.equals(currentContentId))
                            {
                                // Rendered content has not changed.
                                addPanel = false;
                            }
                        }

                        if (addPanel)
                        {
                            bean.setContentId(newContentId);

                            panels.add(bean);
                        }

                        contentIdCollector.addNewPanelKey(bean.getCompleteKey());
                    }
                }
                else
                {
                    contentIdCollector.addNewPanelKey(bean.getCompleteKey());
                }
            }
        }

        return panels;
    }

    private String calculateFullContentId(String contentId, WebPanelBean bean, ContentIdCollector contentIdCollector)
    {
        String calculateFrom = getCalculateFrom(contentId, bean);

        return contentIdCollector.calculateContentId(calculateFrom);
    }

    String getCalculateFrom(String contentId, WebPanelBean bean)
    {
        StringBuilder builder = new StringBuilder(contentId);

        builder.append(bean.getLabel())
                .append(bean.getPrefix())
                .append(bean.getStyleClass())
                .append(bean.isRenderHeader())
                .append(bean.getWeight());

        appendGroup(bean.getHeaderLinks(), builder);

        for (String subPanelHtml : bean.getSubpanelHtmls())
        {
            builder.append(subPanelHtml);
        }

        return builder.toString();
    }

    private void appendGroup(LinkGroupBean group, StringBuilder builder)
    {
        if (group == null)
        {
            return;
        }

        builder.append(group.getStyleClass())
                .append(group.getWeight());

        appendLink(group, group.getHeader(), builder);

        for (SimpleLinkBean link : group.getLinks())
        {
            appendLink(group, link, builder);
        }

        for (LinkGroupBean subGroup : group.getGroups())
        {
            appendGroup(subGroup, builder);
        }
    }

    private void appendLink(LinkGroupBean group, SimpleLinkBean link, StringBuilder builder)
    {
        String value = IssueOperationLinksProvider.getLinkValueForContentIdCalculation(group, link);
        if (value != null)
        {
            builder.append(value);
        }
    }

    WebPanelBeanBuilder getWebPanelBeanBuilder(final IssueWebPanelRenderUtil issueWebPanelRenderUtil,
                                                       final WebPanelModuleDescriptor webPanel)
    {
        return new WebPanelBeanBuilder(moduleWebComponentFields, issueWebPanelRenderUtil,
                authenticationContext.getI18nHelper(), authenticationContext.getLoggedInUser(), webPanel);
    }
}