package com.atlassian.jira.components.issueviewer.viewissue;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.rest.v2.issue.LinkGroupBean;
import com.atlassian.jira.rest.v2.issue.OpsbarBean;
import com.atlassian.jira.rest.v2.issue.builder.BeanBuilderFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
public class DefaultIssueLinkGroupsProvider implements IssueLinkGroupsProvider
{
    private final BeanBuilderFactory beanBuilderFactory;

    @Inject
    public DefaultIssueLinkGroupsProvider(BeanBuilderFactory beanBuilderFactory)
    {
        this.beanBuilderFactory = beanBuilderFactory;
    }

    @Override
    public List<LinkGroupBean> getGroups(Issue issue)
    {
        OpsbarBean opsbarBean = beanBuilderFactory.newOpsbarBeanBuilder(issue).build();

        return opsbarBean.getLinkGroups();
    }
}
