package com.atlassian.jira.components.issueviewer.action;

import com.atlassian.jira.issue.Issue;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Contains issue pager information: the next/previous issues.
 *
 * @since v6.0
 */
@XmlRootElement
public class IssuePager
{
    @XmlRootElement
    public static class IssueDescriptor
    {
        @XmlElement private Long id;
        @XmlElement private String key;

        public IssueDescriptor(final Issue issue)
        {
            id = issue.getId();
            key = issue.getKey();
        }

        public Long getId()
        {
            return id;
        }

        public String getKey()
        {
            return key;
        }
    }

    @XmlElement private IssueDescriptor nextIssue;
    @XmlElement private int position;
    @XmlElement private IssueDescriptor previousIssue;
    @XmlElement private int resultCount;

    /**
     * Initialise with next and previous issues.
     *
     * @param nextIssue The next issue (if any).
     * @param position The current issue's position in the search results.
     * @param previousIssue The previous issue (if any).
     * @param resultCount The total number of search results.
     */
    public IssuePager(
            final Issue nextIssue,
            final int position,
            final Issue previousIssue,
            final int resultCount)
    {
        this.position = position;
        this.resultCount = resultCount;

        if (nextIssue != null)
        {
            this.nextIssue = new IssueDescriptor(nextIssue);
        }

        if (previousIssue != null)
        {
            this.previousIssue = new IssueDescriptor(previousIssue);
        }
    }

    public IssueDescriptor getNextIssue()
    {
        return nextIssue;
    }

    public int getPosition()
    {
        return position;
    }

    public IssueDescriptor getPreviousIssue()
    {
        return previousIssue;
    }

    public int getResultCount()
    {
        return resultCount;
    }
}