package com.atlassian.jira.components.issueviewer.viewissue;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.rest.v2.issue.LinkGroupBean;

import java.util.List;

public interface IssueLinkGroupsProvider
{
    List<LinkGroupBean> getGroups(Issue issue);
}
