package com.atlassian.jira.components.issueviewer.action;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.rest.json.beans.ProjectJsonBean;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.rest.v2.issue.OpsbarBean;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;

/**
 * Representation of issue for returning to client
 *
 * @since v5.0.3
 */
@XmlRootElement
public class IssueBean
{
    @XmlElement private Long id;
    @XmlElement private String key;
    @XmlElement private Map<String, String> metadata;
    @XmlElement private OpsbarBean operations;
    @XmlElement private IssueBean parent;
    @XmlElement private IssueProjectBean project;
    @XmlElement private IssueStatusBean status;
    @XmlElement private String summary;
    @XmlElement private String summaryContentId;
    @XmlElement private boolean isEditable;

    private IssueBean() {}

    public IssueBean(final Issue issue,boolean includeSummary)
    {
        this.id = issue.getId();
        this.key = issue.getKey();
        if (includeSummary)
        {
            this.summary = issue.getSummary();
        }
    }

    public IssueBean(
            final Issue issue,
            final Map<String, String> metadata,
            final OpsbarBean operations,
            final Project project,
            final Status status,
            final String summaryContentId,
            boolean includeSummary,
            boolean isEditable)

    {
        this(issue, includeSummary);
        this.metadata = metadata;
        this.operations = operations;
        this.summaryContentId = summaryContentId;
        this.project = new IssueProjectBean(project);
        this.status = new IssueStatusBean(status);
        this.isEditable = isEditable;
        Issue parent = issue.getParentObject();
        if (null != parent)
        {
            this.parent = new IssueBean(parent, true);
        }
    }

    public Long getId()
    {
        return id;
    }

    public String getKey()
    {
        return key;
    }

    public String getSummary()
    {
        return summary;
    }

    public OpsbarBean getOperations()
    {
        return operations;
    }

    public IssueProjectBean getProject()
    {
        return project;
    }

    public IssueStatusBean getStatus()
    {
        return status;
    }

    public IssueBean getParent()
    {
        return parent;
    }

    public String getSummaryContentId()
    {
        return summaryContentId;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this).
                append("id", id).
                append("key", key).
                append("operations", operations).
                toString();
    }

    @XmlRootElement
    public static class IssueProjectBean
    {
        @XmlElement (name = "id")
        private Long id;

        @XmlElement (name = "key")
        private String key;

        @XmlElement (name = "name")
        private String name;

        @XmlElement (name = "avatarUrls")
        private Map<String, String> avatarUrls;

        @XmlElement
        private boolean systemAvatar;

        private IssueProjectBean() {}

        public IssueProjectBean(Project project)
        {
            this.id = project.getId();
            this.key = project.getKey();
            this.name = project.getName();
            this.avatarUrls = ProjectJsonBean.getAvatarUrls(project);
            this.systemAvatar = project.getAvatar().isSystemAvatar();
        }

        public Long getId()
        {
            return id;
        }

        public String getKey()
        {
            return key;
        }

        public String getName()
        {
            return name;
        }

        public Map<String, String> getAvatarUrls()
        {
            return avatarUrls;
        }

        public boolean isSystemAvatar()
        {
            return systemAvatar;
        }
    }

    @XmlRootElement
    public static class IssueStatusBean
    {
        @XmlElement (name = "description")
        private String description;

        @XmlElement (name = "iconUrl")
        private String iconUrl;

        @XmlElement (name = "name")
        private String name;

        @XmlElement (name = "id")
        private String id;

        private IssueStatusBean() {}

        public IssueStatusBean(Status status)
        {
            this.description = status.getDescription();
            this.iconUrl = status.getIconUrl();
            this.name = status.getName();
            this.id = status.getId();
        }

        public String getDescription()
        {
            return description;
        }

        public String getIconUrl()
        {
            return iconUrl;
        }

        public String getName()
        {
            return name;
        }

        public String getId()
        {
            return id;
        }
    }
}
