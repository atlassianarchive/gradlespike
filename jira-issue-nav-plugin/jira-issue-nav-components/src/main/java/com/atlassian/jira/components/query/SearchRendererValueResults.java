package com.atlassian.jira.components.query;

import java.util.HashMap;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Maps of values returned by search renderer, indexed by searcher id
 * @since v5.1
 */
@XmlRootElement
public class SearchRendererValueResults extends HashMap<String, SearchRendererValue>
{
}

