package com.atlassian.jira.components.orderby;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Transfer object for the "sortBy" clause in the search.
 *
 * @since 5.2
 */
@JsonSerialize
public class SortByBean
{
    public String fieldId;
    public String fieldName;
    public String order;
    public String toggleJql;

    public SortByBean()
    {
    }

    public SortByBean(String fieldId, String fieldName, String order, String toggleJql)
    {
        this.fieldId = fieldId;
        this.fieldName = fieldName;
        this.order = order;
        this.toggleJql = toggleJql;
    }

}
