package com.atlassian.jira.components.orderby;

import com.atlassian.query.Query;

public interface OrderByUtil {
    SortByBean generateSortBy(Query query);
}
