package com.atlassian.jira.components.query;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.search.searchers.IssueSearcher;
import com.atlassian.jira.user.UserHistoryItem;
import com.atlassian.jira.user.UserIssueSearcherHistoryManager;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.google.common.base.Function;
import com.google.common.collect.Maps;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

@ExportAsService
@Named
public class DefaultRecentSearchersService implements RecentSearchersService
{

    private UserIssueSearcherHistoryManager userIssueSearcherHistoryManager;

    @Inject
    public DefaultRecentSearchersService(UserIssueSearcherHistoryManager userIssueSearcherHistoryManager)
    {
        this.userIssueSearcherHistoryManager = userIssueSearcherHistoryManager;
    }

    @Override
    public Map<String, Long> getRecentSearchers(User user)
    {
        return Maps.transformValues(Maps.uniqueIndex(userIssueSearcherHistoryManager.getUserIssueSearcherHistory(user), new Function<UserHistoryItem, String>()
        {
            public String apply(UserHistoryItem userHistoryItem)
            {
                return userHistoryItem.getEntityId();
            }
        }), new Function<UserHistoryItem, Long>()
        {
            @Override
            public Long apply(UserHistoryItem userHistoryItem)
            {
                return userHistoryItem.getLastViewed();
            }
        });

    }

    @Override
    public void addRecentSearcher(User user, IssueSearcher<?> issueSearcher)
    {
        userIssueSearcherHistoryManager.addIssueSearcherToHistory(user, issueSearcher);
    }

}
