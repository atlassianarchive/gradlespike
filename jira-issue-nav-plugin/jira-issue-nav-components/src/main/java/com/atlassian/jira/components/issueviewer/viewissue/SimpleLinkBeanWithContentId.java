package com.atlassian.jira.components.issueviewer.viewissue;

import com.atlassian.jira.rest.v2.common.SimpleLinkBean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="link")
public class SimpleLinkBeanWithContentId extends SimpleLinkBean
{
    @XmlElement
    private String contentId;

    public SimpleLinkBeanWithContentId(SimpleLinkBean link, String contentId)
    {
        super(link.getId(), link.getStyleClass(), link.getLabel(), link.getTitle(),
                link.getHref(), link.getIconClass(), link.getWeight());
        this.contentId = contentId;
    }

    public String getContentId()
    {
        return contentId;
    }
}
