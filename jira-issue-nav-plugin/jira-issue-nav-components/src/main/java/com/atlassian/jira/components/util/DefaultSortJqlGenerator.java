package com.atlassian.jira.components.util;

import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.jql.ClauseInformation;
import com.atlassian.jira.jql.util.JqlCustomFieldId;
import com.atlassian.jira.jql.util.JqlStringSupport;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.order.OrderBy;
import com.atlassian.query.order.OrderByImpl;
import com.atlassian.query.order.SearchSort;
import com.atlassian.query.order.SortOrder;
import com.google.common.annotations.VisibleForTesting;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Builds sort JQL for each column displayed in the issue navigator.
 *
 * @since 2.0.21
 */
@ExportAsService
@Named
public class DefaultSortJqlGenerator implements SortJqlGenerator {
    private final JqlStringSupport jqlStringSupport;
    private final SearchHandlerManager searchHandlerManager;

    @Inject
    public DefaultSortJqlGenerator(JqlStringSupport jqlStringSupport, SearchHandlerManager searchHandlerManager)
    {
        this.jqlStringSupport = jqlStringSupport;
        this.searchHandlerManager = searchHandlerManager;
    }

    /**
     * Generate the JQL to be contained in the column headers.
     * <p/>
     * This JQL is the current query with an additional ORDER BY applied.
     *
     * @return the JQL to be contained in the column headers.
     */
    @Override
    @VisibleForTesting
    public Map<String, String> generateColumnSortJql(Query query, List<NavigableField> fields)
    {
        Map<String, String> columnSortJql = new HashMap<String, String>();
        for (NavigableField field : fields)
        {
            OrderBy orderBy = buildOrderBy(query.getOrderByClause(), field);

            columnSortJql.put(field.getId(), jqlStringSupport.generateJqlString(
                    new QueryImpl(query.getWhereClause(), orderBy, null)));
        }

        return columnSortJql;
    }

    /**
     * Adds (or updates) a field to the {@code ORDER BY} portion of a query.
     * Maintains the name or cf[id] id of a custom field depending on which was
     * supplied.
     *
     * Switches the direction of the order by query to make it the opposite of the
     * present direction.
     *
     * Updates a field with no explicit ORDER BY to be opposite of the default.
     *
     * The following show the result of adding "project" to different queries:
     *
     *   ORDER BY priority ASC -> ORDER BY project ASC, priority ASC
     *   ORDER BY project DESC -> ORDER BY project ASC
     *
     * @param orderBy The existing {@code ORDER BY} that is to be updated.
     * @param field The field that is to be added.
     * @return An updated {@code ORDER BY}.
     */
    private OrderBy buildOrderBy(OrderBy orderBy, NavigableField field) {
        SortOrder columnDirection = SortOrder.parseString(field.getDefaultSortOrder());

        String columnName;

        // Custom fields must be in the format "cf[id]".
        final boolean isCustomField = field instanceof CustomField;
        if (isCustomField)
        {
            CustomField customField = (CustomField)field;
            columnName = JqlCustomFieldId.toString(customField.getIdAsLong());
        }
        else
        {
            Collection<ClauseNames> jqlClauseNames = searchHandlerManager.getJqlClauseNames(field.getId());
            columnName = !jqlClauseNames.isEmpty() ? jqlClauseNames.iterator().next().getPrimaryName() : field.getId();
        }

        List<SearchSort> searchSorts = new ArrayList<SearchSort>();

        // inverts the sort sort order if orderBy already contains a search sort for this field (either using its
        // primary clause name or any of the alternates).
        for (SearchSort searchSort : orderBy.getSearchSorts())
        {
            final String fieldInSearch = searchSort.getField();

            if (fieldInSearch.equals(columnName) ||
                    (!isCustomField && isAlias(field.getId(), fieldInSearch)) ||
                    (isCustomField && ( ((CustomField)field).getUntranslatedName() ).equals(fieldInSearch)))
            {
                columnName = fieldInSearch;

                SortOrder sortOrder = searchSort.getSortOrder();

                // If no sortOrder direction has been specified
                if (sortOrder == null)
                {
                    sortOrder = SortOrder.parseString(field.getDefaultSortOrder());
                }

                // Switch the direction so that when the field is clicked the alternate to current is requested.
                if (sortOrder == SortOrder.ASC)
                {
                    columnDirection = SortOrder.DESC;
                }
                else
                {
                    columnDirection = SortOrder.ASC;
                }
            }
            else
            {
                searchSorts.add(searchSort);
            }
        }

        searchSorts.add(0, new SearchSort(columnName, columnDirection));
        return new OrderByImpl(searchSorts);
    }

    /**
     * Determines whether the <code>columnName</code> is an alias for a field by checking if it is among the
     * JQL clause's names.
     *
     * @param fieldId of column being sorted
     * @param columnName field in search
     * @return columnName is alias for columnName
     */
    private boolean isAlias(String fieldId, String columnName)
    {
        ClauseInformation clauseInformation = SystemSearchConstants.getClauseInformationById(fieldId);

        return (clauseInformation != null && clauseInformation.getJqlClauseNames().contains(columnName));
    }
}
