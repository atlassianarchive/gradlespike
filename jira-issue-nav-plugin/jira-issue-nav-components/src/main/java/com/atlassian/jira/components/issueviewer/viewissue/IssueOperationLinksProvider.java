package com.atlassian.jira.components.issueviewer.viewissue;

import com.atlassian.jira.components.issueeditor.action.ContentIdCollector;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.rest.v2.common.SimpleLinkBean;
import com.atlassian.jira.rest.v2.issue.LinkGroupBean;
import com.atlassian.jira.rest.v2.issue.OpsbarBean;
import com.google.common.collect.Lists;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collections;
import java.util.List;

@Named
public class IssueOperationLinksProvider
{
    private final IssueLinkGroupsProvider issueLinkGroupsProvider;

    @Inject
    public IssueOperationLinksProvider(IssueLinkGroupsProvider issueLinkGroupsProvider)
    {
        this.issueLinkGroupsProvider = issueLinkGroupsProvider;
    }

    public OpsbarBean getOperations(Issue issue, ContentIdCollector contentIdCollector)
    {
        List<LinkGroupBean> linkGroups = issueLinkGroupsProvider.getGroups(issue);

        if (linkGroups.isEmpty())
        {
            return new OpsbarBean(Collections.<LinkGroupBean>emptyList());
        }

        List<LinkGroupBean> topLevelGroupsToReturn = Lists.newArrayList();

        for (LinkGroupBean group : linkGroups)
        {
            group = filter(group, contentIdCollector);

            if (group != null)
            {
                topLevelGroupsToReturn.add(group);
            }
        }

        return new OpsbarBean(topLevelGroupsToReturn);
    }

    private LinkGroupBean filter(LinkGroupBean group, ContentIdCollector contentIdCollector)
    {
        LinkGroupBean.Builder builder = new LinkGroupBean.Builder()
                .id(group.getId())
                .header(group.getHeader())
                .styleClass(group.getStyleClass())
                .weight(group.getWeight());

        boolean loadAll = contentIdCollector.mustLoadAll();

        for (SimpleLinkBean link : group.getLinks())
        {
            String currentContentId = contentIdCollector.getCurrentLinkContentId(link.getId());

            String calculateFrom = getLinkValueForContentIdCalculation(group, link);
            String newContentId = contentIdCollector.calculateContentId(calculateFrom);

            if (loadAll || !newContentId.equals(currentContentId))
            {
                builder.addLinks(new SimpleLinkBeanWithContentId(link, newContentId));
            }

            contentIdCollector.addNewLinkKey(link.getId());
        }

        for (LinkGroupBean subGroup : group.getGroups())
        {
            subGroup = filter(subGroup, contentIdCollector);

            if (subGroup != null)
            {
                builder.addGroups(subGroup);
            }
        }

        LinkGroupBean filteredGroup = builder.build();

        return filteredGroup.getLinks().isEmpty() && filteredGroup.getGroups().isEmpty() ? null : filteredGroup;
    }

    public static String getLinkValueForContentIdCalculation(LinkGroupBean group, SimpleLinkBean link)
    {
        if (link == null)
        {
            return null;
        }

        return group.getId() + link.getLabel() + link.getHref() + link.getTitle() + link.getStyleClass() + link.getIconClass();
    }
}
