package com.atlassian.jira.components.orderby.rest;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderByRequest
{
    /**
     * The JQL that we are currently sorting by (used to build the new sort JQL)
     */
    @JsonProperty
    public String jql = "";
    @JsonProperty
    public String filter = "";

    /**
     * A partial field name query that we will <em>try</em> to match against (optional)
     */
    @JsonProperty
    public String query = "";
    @JsonProperty
    public String sortBy;

    /**
     * The maximum number of results to return.
     * optional)
     */
    @JsonProperty
    public Integer maxResults = 10;
}