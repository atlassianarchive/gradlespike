AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueeditor");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueeditor-test");

module('JIRA.Components.IssueEditor.AppModule', {
    setup: function () {
        JIRA.Issues.LayoutPreferenceManager = JIRA.Issues.LayoutPreferenceManager || {
            getPreferredLayoutKey: function() { return "list-view"; }
        };

        this.sandbox = sinon.sandbox.create();
        this.application = JIRA.Tests.Utils.AppModule.startApplication();
    },

    createModule: function() {
        return JIRA.Tests.Utils.AppModule.createModule({
            sinon: this.sandbox,
            application: this.application,
            moduleName: "IssueEditor"
        });
    },

    stubMethods: function(methods) {
        JIRA.Tests.Utils.AppModule.stubMethods({
            sinon: this.sandbox,
            methods: methods,
            moduleClass: JIRA.Components.IssueEditor
        });
    },

    teardown: function () {
        this.sandbox.restore();
        this.application.stop();
        delete this.application;
    }
});

test("It creates a instance of IssueEditor", function() {
    var appModule = new JIRA.Components.IssueEditor.AppModule();
    ok(appModule.create() instanceof JIRA.Components.IssueEditor, "Initialize creates a instance of IssueEditor");
});

test("Commands are registered in the application", function() {
    var commands = [
        "abortPending",
        "beforeHide",
        "beforeShow",
        "removeIssueMetadata",
        "updateIssueWithQuery",
        "close",
        "setContainer",
        "dismiss",
        "editField"
    ];

    this.stubMethods(commands);
    JIRA.Tests.Utils.AppModule.assertCommands({
        commands: commands,
        commandsNamespace: "issueEditor",
        application: this.application,
        module: this.createModule()
    });
});

test("Requests are registered in the application", function() {
    var requests = [
        "loadIssue",
        "canDismissComment",
        "getIssueId",
        "getIssueKey",
        "refreshIssue",
        "isCurrentlyLoading"
    ];

    this.stubMethods(requests);
    var module = this.createModule();
    JIRA.Tests.Utils.AppModule.assertRequests({
        requests: requests,
        requestsNamespace: "issueEditor",
        application: this.application,
        module: module
    });

    // We need to test this request manually because the module method (getField) does not match the request
    // (fields)
    this.stubMethods(["getFields"]);
    this.application.request("issueEditor:fields");
    ok(module["getFields"].calledOnce, "The request 'fields' calls the method 'getFields'");
});

test("Events are registered in the application", function() {
    var events = [
        "loadComplete",
        "loadError",
        "close",
        "replacedFocusedPanel",
        "saveSuccess",
        "saveError",
        "fieldSubmitted",
        "editField"
    ];
    JIRA.Tests.Utils.AppModule.assertEvents({
        events: events,
        sinon: this.sandbox,
        application: this.application,
        eventsNamespace: "issueEditor",
        module: this.createModule()
    });
});