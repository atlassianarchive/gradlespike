
JIRA.Events.INLINE_EDIT_STARTED = "inlineEditStarted";
JIRA.Events.BEFORE_INLINE_EDIT_CANCEL= "inlineEditCancelled";
JIRA.Events.INLINE_EDIT_BLURRED = "inlineEditBlurred";
JIRA.Events.INLINE_EDIT_FOCUSED = "inlineEditFocused";
JIRA.Events.INLINE_EDIT_REQUESTED = "inlineEditRequested";
JIRA.Events.LOCK_PANEL_REFRESHING = "lockPanelRefreshing";
JIRA.Events.UNLOCK_PANEL_REFRESHING = "unlockPanelRefreshing";
JIRA.Events.REFRESH_ISSUE_PAGE = "refreshIssuePage";

// Reasons for cancelling edit
JIRA.Issues.CANCEL_REASON = {
    escPressed: "escPressed"
};

AJS.$(function() {

    AJS.namespace("JIRA.Components.IssueEditor.InlineEditUtils");

    /**
     * The time required to wait between blur and focus
     */
    JIRA.Components.IssueEditor.InlineEditUtils.BLUR_FOCUS_TIMEOUT = 50;

    var BlurTriggers = JIRA.Components.IssueEditor.InlineEditUtils.BlurTriggers = {
        Default: function(fieldId, $container) {

            // Include save buttons
            $container = $container.nextAll(".save-options").andSelf();

            var focusables = ':input, a[href], [tabindex]';
            var timeout;
            var containerHasFocus = hasFocus($container);
            var eventsMap = {
                focus: function() {
                    if (!containerHasFocus) {
                        containerHasFocus = true;
                        JIRA.trigger(JIRA.Events.INLINE_EDIT_FOCUSED, [fieldId]);
                    }
                },
                blur: function() {
                    if (timeout) clearTimeout(timeout);
                    timeout = setTimeout(triggerIfBlurred, JIRA.Components.IssueEditor.InlineEditUtils.BLUR_FOCUS_TIMEOUT);
                }
            };

            // Make container focusable
            $container.attr('tabindex', 1)
            // Bind to container
                .bind(eventsMap)
            // Bind to focusable elements in container
                .delegate(focusables, eventsMap);

            function triggerIfBlurred() {
                if (!hasFocus($container)) {
                    containerHasFocus = false;
                    JIRA.trigger(JIRA.Events.INLINE_EDIT_BLURRED, [fieldId]);
                }
            }
        }
    };

    function hasFocus($container) {
        var activeElement = document.activeElement;
        return $container.find(activeElement).length > 0 || $container.filter(activeElement).length > 0;
    }

    JIRA.Components.IssueEditor.InlineEditUtils.BlurTriggerMapping = {
        system: {
            "summary": BlurTriggers.Default,
            "priority": BlurTriggers.Default,
            "issuetype": BlurTriggers.Default,
            "components": BlurTriggers.Default,
            "versions": BlurTriggers.Default,
            "fixVersions": BlurTriggers.Default,
            "assignee": BlurTriggers.Default,
            "reporter": BlurTriggers.Default,
            "environment": BlurTriggers.Default,
            "description": BlurTriggers.Default,
            "labels": BlurTriggers.Default,
            "duedate": BlurTriggers.Default
        },
        custom: {
            "com.atlassian.jira.plugin.system.customfieldtypes:cascadingselect": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:datepicker": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:datetime": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:float": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:grouppicker": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:labels": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:multicheckboxes": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:multigrouppicker": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:multiselect": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:multiuserpicker": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:multiversion": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:project": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:select": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:radiobuttons": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:textarea": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:textfield": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:url": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:userpicker": BlurTriggers.Default,
            "com.atlassian.jira.plugin.system.customfieldtypes:version": BlurTriggers.Default
        }
    };

    JIRA.bind(JIRA.Events.INLINE_EDIT_STARTED, function(e, fieldId, fieldType, $container) {
        var blurTrigger;
        if (fieldType) {
            blurTrigger = JIRA.Components.IssueEditor.InlineEditUtils.BlurTriggerMapping.custom[fieldType];
            if (!blurTrigger) {
                // in 5.1, we told devs to put the "short" key into the custom mapping, not the complete key
                // if we can't find the complete key, try the short key
                var shortKey = fieldType.replace(/.+:(.+)/, "$1"); // extract after the colon
                blurTrigger = JIRA.Components.IssueEditor.InlineEditUtils.BlurTriggerMapping.custom[shortKey];
            }
        } else {
            blurTrigger = JIRA.Components.IssueEditor.InlineEditUtils.BlurTriggerMapping.system[fieldId];
        }

        if (blurTrigger) {
            blurTrigger(fieldId, $container);
        }
    });
});
