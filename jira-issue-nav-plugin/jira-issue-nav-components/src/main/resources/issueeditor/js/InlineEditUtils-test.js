AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueeditor");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueeditor-test");

module('JIRA.Components.IssueEditor.InlineEditUtils', {

    setup: function () {
        this.$randomInput = jQuery("<input />").appendTo("#qunit-fixture");
        this.$elContainer = jQuery("<div>").appendTo("#qunit-fixture");
        this.$el = jQuery("<div>").appendTo(this.$elContainer);
    },

    teardown: function () {
        this.$el.remove();
        this.$randomInput.remove();
    }

});

test("Blur triggers INLINE_EDIT_BLURRED", function () {
    var clock = sinon.useFakeTimers(),
        $input = jQuery("<input />").appendTo(this.$el).focus();
    JIRA.trigger(JIRA.Events.INLINE_EDIT_STARTED, ["priority", null, this.$el, this.$elContainer]);

    var spy = sinon.spy();
    JIRA.bind(JIRA.Events.INLINE_EDIT_BLURRED, spy);
    $input.blur();
    clock.tick(JIRA.Components.IssueEditor.InlineEditUtils.BLUR_FOCUS_TIMEOUT + 1);

    equal(spy.callCount, 1);
    equal(spy.args[0][1], "priority");
    clock.restore();
});

test("INLINE_EDIT_BLURRED is triggered when input is root element", function () {
    var clock = sinon.useFakeTimers(),
        $input = jQuery("<input>").appendTo(this.$el).focus();
    JIRA.trigger(JIRA.Events.INLINE_EDIT_STARTED, ["priority", null, $input, this.$elContainer]);

    var spy = sinon.spy();
    JIRA.bind(JIRA.Events.INLINE_EDIT_BLURRED, spy);
    $input.blur();
    clock.tick(JIRA.Components.IssueEditor.InlineEditUtils.BLUR_FOCUS_TIMEOUT + 1);

    equal(spy.callCount, 1);
    equal(spy.args[0][1], "priority");
    clock.restore();
});

test("INLINE_EDIT_BLURRED is NOT triggered when switching between inputs in same field", function () {
    var clock = sinon.useFakeTimers(),
        $inputA = jQuery("<input>").appendTo(this.$el),
        $inputB = jQuery("<input>").appendTo(this.$el);
    JIRA.trigger(JIRA.Events.INLINE_EDIT_STARTED, ["priority", null, this.$el, this.$elContainer]);
    $inputA.focus();

    var spy = sinon.spy();
    JIRA.bind(JIRA.Events.INLINE_EDIT_BLURRED, spy);
    $inputA.blur();
    $inputB.focus();
    clock.tick(JIRA.Components.IssueEditor.InlineEditUtils.BLUR_FOCUS_TIMEOUT + 1);

    equal(spy.callCount, 0);
    clock.restore();
});

test("INLINE_EDIT_BLURRED is triggered when all inputs in field are blurred", function () {
    var clock = sinon.useFakeTimers(),
        $container = jQuery("<div>").appendTo(this.$el),
        $inputA = jQuery("<input>").appendTo($container),
        $inputB = jQuery("<input>").appendTo($container);

    JIRA.trigger(JIRA.Events.INLINE_EDIT_STARTED, ["priority",null, $container, this.$elContainer]);
    $inputA.focus();

    var spy = sinon.spy();
    JIRA.bind(JIRA.Events.INLINE_EDIT_BLURRED, spy);
    $inputA.blur();
    $inputB.focus();
    $inputB.blur();
    clock.tick(JIRA.Components.IssueEditor.InlineEditUtils.BLUR_FOCUS_TIMEOUT + 1);

    equal(spy.callCount, 1);
    equal(spy.args[0][1], "priority");
    clock.restore();
});

test("INLINE_EDIT_BLURRED is NOT triggered when inputs are blurred, but container has focus", function () {
    var clock = sinon.useFakeTimers(),
        $container = jQuery("<div>").appendTo(this.$el),
        $input = jQuery('<input>').appendTo($container);
    
    JIRA.trigger(JIRA.Events.INLINE_EDIT_STARTED, ["priority", null, $container, this.$elContainer]);
    $input.focus();

    var spy = sinon.spy();
    JIRA.bind(JIRA.Events.INLINE_EDIT_BLURRED, spy);
    $input.blur();
    $container.focus();
    clock.tick(JIRA.Components.IssueEditor.InlineEditUtils.BLUR_FOCUS_TIMEOUT + 1);

    equal(spy.callCount, 0);
    clock.restore();
});

test("INLINE_EDIT_BLURRED handlers are called with appropriate fieldId argument", function () {
    var clock = sinon.useFakeTimers();
    var $inputA = jQuery("<input>").appendTo(this.$el);
    var $inputB = jQuery("<input>").appendTo(this.$el);

    var spyFocused = sinon.spy();
    var spyBlurred = sinon.spy();

    JIRA.bind(JIRA.Events.INLINE_EDIT_FOCUSED, spyFocused);
    JIRA.bind(JIRA.Events.INLINE_EDIT_BLURRED, spyBlurred);

    JIRA.trigger(JIRA.Events.INLINE_EDIT_STARTED, ["summary", null, $inputA, this.$elContainer]);
    $inputA.focus();
    $inputA.blur();

    JIRA.trigger(JIRA.Events.INLINE_EDIT_STARTED, ["priority", null, $inputB, this.$elContainer]);
    $inputB.focus();
    $inputB.blur();

    clock.tick(JIRA.Components.IssueEditor.InlineEditUtils.BLUR_FOCUS_TIMEOUT + 1);

    equal(spyFocused.getCall(0).args[1], "summary", "Focused field is identified correctly");
    equal(spyBlurred.getCall(0).args[1], "summary", "Blurred field is identified correctly");

    equal(spyFocused.getCall(1).args[1], "priority", "Focused field is identified correctly");
    equal(spyBlurred.getCall(1).args[1], "priority", "Blurred field is identified correctly");
    clock.restore();
});
