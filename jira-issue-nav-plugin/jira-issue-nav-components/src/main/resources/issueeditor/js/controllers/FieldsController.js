AJS.namespace("JIRA.Components.IssueEditor.Controllers.Fields");

/**
 * @class JIRA.Components.IssueEditor.Controllers.Fields
 * @extends Marionette.Controller
 *
 * Controller for the fields of an issue. This class does not render the fields but the errors when loading/saving
 * fields
 */

JIRA.Components.IssueEditor.Controllers.Fields = JIRA.Marionette.Controller.extend({
    showError: function(errorCollection, isTimeout) {
        if (errorCollection || isTimeout) {
            var errorMessages = errorCollection.errorMessages || [];

            // Close previous view, if any
            this.close();

            // Let others know we are rendering, Marionette style
            this.triggerMethod("before:render");

            this.view = new JIRA.Components.IssueEditor.Views.LoadingError(errorCollection, isTimeout);
            this.view.render();

            // Listen for view events
            this.listenTo(this.view, "close", this.close);

            // Let others know we have rendered our view, Marionette style
            this.triggerMethod("render");
        }
    },

    /**
     * Closes and deletes the view
     */
    close: function () {
        if (this.view) {
            this.view.close();
            this.stopListening(this.view);
            delete this.view;
        }
    }
});
