AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueeditor");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueeditor-test");

module('JIRA.Components.IssueEditor.Controllers.Fields', {
    setup: function() {
        this.controller = new JIRA.Components.IssueEditor.Controllers.Fields();
        this.sandbox = sinon.sandbox.create();

        this.sandbox.stub(JIRA.Messages, "showErrorMsg");
        this.sandbox.spy(JIRA.Components.IssueEditor.Views, "LoadingError");
    },

    teardown: function() {
        this.controller.close();
        this.sandbox.restore();
    }
});

test("It renders an error message if there are errors", function() {
    this.controller.showError({});

    ok(JIRA.Components.IssueEditor.Views.LoadingError.calledWithNew(), "The view is created");
    ok(JIRA.Messages.showErrorMsg.calledOnce, "The view is rendered as a ErrorMsg");
});

test("It renders an error message if it is a timeout", function() {
    this.controller.showError({}, true);

    ok(JIRA.Components.IssueEditor.Views.LoadingError.calledWithNew(), "The view is created");
    ok(JIRA.Messages.showErrorMsg.calledOnce, "The view is rendered as a ErrorMsg");
});

test("It does not render an error message if it is not a timeout and there are no errors", function() {
    this.controller.showError(null, false);

    ok(!JIRA.Components.IssueEditor.Views.LoadingError.called, "The view is created");
    ok(!JIRA.Messages.showErrorMsg.called, "The view is rendered as a ErrorMsg");
});
