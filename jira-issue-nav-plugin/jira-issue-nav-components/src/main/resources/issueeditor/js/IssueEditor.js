AJS.namespace("JIRA.Components.IssueEditor");

/**
 * A module that handles loading and showing issues in a given container.
 */
JIRA.Components.IssueEditor = JIRA.Components.IssueViewer.extend({
    namedEvents: [
        /*
         Triggered when an issue has loaded successfully.
         */
        "loadComplete",

        /*
         Triggered when there is an error when loading an issue.
         */
        "loadError",

        /*
         We should return to issue search in response to some action.
         */
        "returnToSearch",

        /**
         * Triggered when inline edit is successful
         * .. {number} the issue id
         */
        "saveSuccess",

        "saveError",

        "editField",

        /**
         * Field has been submitted by user.
         */
        "fieldSubmitted",

        /**
         * The issue panel that previously had focus was replaced.
         */
        "replacedFocusedPanel"
    ],

    /**
     * @constructor
     * Initialize this module and all the services/controllers
     *
     * //TODO When this module is transformed into a Marionette.Module, this should be onStart()
     *
     * @param {Object} options
     * @param {boolean|function} [options.showReturnToSearchOnError=false] Whether the error views should display a 'Return to Search' link
     */
    initialize: function (options) {
        JIRA.Components.IssueViewer.prototype.initialize.call(this, options);

        // Services
        this._buildFieldsLoader();
        this._buildIssueSaver();

        // Controllers
        this._buildEditIssueController();
        this._buildFieldsController();

        // Other services
        this._handleUnload();

        this.listenAndRethrow(this.eventBus, "fieldSubmitted");
    },

    _buildFieldsLoader: function() {
        this.fieldsLoader = new JIRA.Components.IssueEditor.Services.FieldsLoader({
            contextPath: contextPath
        });

        this.listenTo(this.fieldsLoader, "fieldsLoaded", function(result) {
            this.viewIssueData.updateIssue(result.issueKey, result);

            // Ensure issueID is a number, otherwise some checks might fail
            result.issueId = Number(result.issueId);

            var editable = result.fields && result.fields.length;
            if (editable) {
                this.editIssueController.update(result);
            } else {
                this.editIssueController.reset();
            }

            JIRA.trace("jira.issue.fields.loaded", { id: result.issueId });
        });

        this.listenTo(this.fieldsLoader, "fieldsError", function(result) {
            this.fieldsController.showError(result.errorCollection, result.isTimeout);
            JIRA.trace("jira.issue.fields.loaded", { id: result.issueId });
        });
    },

    _buildIssueSaver: function() {
        this.saveInProgressManager = new JIRA.Components.IssueEditor.Services.SaveInProgressManager();

        this.issueSaver = new JIRA.Components.IssueEditor.Services.IssueSaver({
            saveInProgressManager: this.saveInProgressManager,
            model: this.model
        });

        this.listenTo(this.issueSaver, "error", function(options){
            var issueId = options.issueId;
            var attemptedSavedIds = options.attemptedSavedIds;
            var response = options.response;
            var duration = options.duration;

            // Show a global error if the issue is no longer visible.
            if (!this.model.isCurrentIssue(issueId)) {
                this._showSaveError({
                    issueId: issueId,
                    issueKey: options.issueKey,
                    response: response
                });
                return;
            }

            this.eventBus.triggerSaveError(issueId, attemptedSavedIds, response);

            // if no longer editable, reload our model
            var isEditable = response && response.fields && response.fields.length;
            if (!isEditable && response) {
                this.editIssueController.reset();
                this.model.update(response, {
                    fieldsSaved: [],
                    fieldsInProgress: []
                });
            }

            this.trigger("saveError", {
                issueId: issueId,
                duration: duration,
                deferred: false
            });

            JIRA.trace("jira.issue.refreshed", { id: issueId });
        });

        this.listenTo(this.issueSaver, "saveStarted", function(issueId, savedFieldIds) {
            this.eventBus.triggerSavingStarted(savedFieldIds);
        });

        this.listenTo(this.issueSaver, "save", function(options) {
            var issueId = options.issueId;
            var issueKey = options.issueKey;
            var savedFieldIds = options.savedFieldIds;
            var response = options.response;
            var shouldSkipSaveIssueSuccessHandler = options.shouldSkipSaveIssueSuccessHandler;
            var duration = options.duration;

            if (this.model.isCurrentIssue(issueId) && !shouldSkipSaveIssueSuccessHandler) {
                this.eventBus.triggerSaveSuccess(issueId, issueKey, savedFieldIds, response);

                // Updating view issue cache with the new data we get back from a successful save.
                this.viewIssueData.set(issueKey, response);

                // Update the model with the new data, including the list of fields still in progress
                var fieldsInProgress = this.editIssueController.getEditsInProgress();
                this.model.update(response, {
                    fieldsSaved: savedFieldIds,
                    fieldsInProgress: this.editIssueController.getEditsInProgress()
                });

                JIRA.trace("jira.psycho.issue.refreshed", { id: issueId });

                // Don't check for the meta value if the page is standalone view issue page
                // because it doesn't exist
                var editable = response.fields && response.fields.length;
                if (editable) {
                    this.editIssueController.update({
                        fields: response.fields,
                        issueId: issueId,
                        issueKey: issueKey
                    }, {
                        editable: true,
                        editIssueController: this.editIssueController,
                        issueModel: this.model,
                        issueId: issueId,
                        fieldsSaved: savedFieldIds,
                        initialize: false,
                        fieldsInProgress: this.editIssueController.getEditsInProgress()
                    });
                }

                JIRA.trace("jira.issue.refreshed", { id: issueId });
            }
            this.trigger("saveSuccess", {
                issueId: issueId,
                issueKey: issueKey,
                savedFieldIds: savedFieldIds,
                duration: duration
            });
        });
    },

    _buildEditIssueController: function () {
        this.editIssueController = new JIRA.Components.IssueEditor.Controllers.EditIssue({
            issueEventBus: this.eventBus
        });
        this.listenTo(this.editIssueController, "save", function (issueId, issueKey, toSaveIds, params, ajaxProperties) {
            //Make sure we don't skip the SaveIssueHandler
            //While the save request is being processed, this handler will be set to being skipped if the ViewIssue
            //module has been closed
            this.issueSaver.setSkipSaveIssueSuccessHandler(false);
            this.issueSaver.save(issueId, issueKey, toSaveIds, params, ajaxProperties);
        });
        this.listenAndRethrow(this.editIssueController, "editField");
    },

    _buildFieldsController: function () {
        this.fieldsController = new JIRA.Components.IssueEditor.Controllers.Fields();
    },

    _onIssueLoaded: function(data, meta, options) {
        if (data.issue.isEditable) {
            // If this issue is NOT from the cache and this is NOT an update, request the fields from the server
            var isPrefetchEnabled = !JIRA.Components.IssueViewer.Services.DarkFeatures.NO_PREFETCH.enabled();
            if (isPrefetchEnabled && !meta.error && !meta.fromCache) {
                this.fieldsLoader.load({
                    viewIssueData: this.viewIssueData,
                    issueEntity: options.issueEntity
                });
            }
        }

        var initialize = !meta.mergeIntoCurrent && options.initialize !== false;
        if (!meta.isUpdate || initialize) {
            this.editIssueController.reset();
        }
        JIRA.Components.IssueViewer.prototype._onIssueLoaded.call(this, data, meta, options);

        // If we have fields data, update it
        // This needs to be done after calling the original onIssueLoaded, as that method can reset
        // the editIssue controller
        if (data.fields) {
            this.editIssueController.update({
                fields: data.fields,
                issueId: data.issue.id,
                issueKey: data.issue.key
            });
            JIRA.trace("jira.issue.fields.loaded", { id: data.issue.id });
        }
    },

    _handleUnload: function () {
        var unloadHandler = _.bind(function () {
            var result;
            if (this.editIssueController.getDirtyEditsInProgress().length > 0) {
                result = AJS.I18n.getText("viewissue.editing.leave");
            }
            return result;
        }, this);
        this.unloadInterceptor = new JIRA.Components.IssueEditor.Services.UnloadInterceptor();

        this.unloadInterceptor.addAfterEvent(unloadHandler);
        this.on("destroy", function () {
            this.unloadInterceptor.removeAfterEvent(unloadHandler);
        });
    },

    _updateModel: function (data, options) {
        // Update editIssueController when whe update the model
        var editable = data.fields && data.fields.length;
        if (editable) {
            this.editIssueController.update(data.fields, {
                fieldsInProgress: this.editIssueController.getEditsInProgress(),
                changed: options.changed
            });
        }

        options = _.extend({},options,{
            fieldsInProgress: this.editIssueController.getEditsInProgress()
        });
        JIRA.Components.IssueViewer.prototype._updateModel.call(this, data, options);
    },

    _loadIssueFromDom: function(issueEntity) {
        if (AJS.Meta.get("server-view-issue-is-editable")) {
            // Edit Issue Controller
            this.editIssueController.setIssueId(issueEntity.id);
            this.editIssueController.setIssueKey(issueEntity.key);

            this.fieldsLoader.load({
                viewIssueData: this.viewIssueData,
                issueEntity: issueEntity
            });
            AJS.Meta.set("server-view-issue-is-editable", null);
        }
        JIRA.Components.IssueViewer.prototype._loadIssueFromDom.call(this, issueEntity);
    },

    /**
     * Initiate the editing of a field.
     *
     * @param {JIRA.Components.IssueEditor.Models.Field} field The field to edit.
     */
    editField: function (field) {
        if (!field.isEditable()) return;

        var issue = this.model.getEntity();
        // Defer determining whether the field is present until
        // after the save completes; it may be removed by the save.
        var execute = _.bind(function() {
            if (field.matchesFieldSelector()) {
                field.edit();
            } else {
                new JIRA.Components.IssueEditor.Views.ModalFieldView({
                    issueEventBus: this.eventBus,
                    model: field,
                    issue: issue
                }).show();
            }
        }, this);

        if (!field.getSaving()) {
            execute()
        } else {
            JIRA.one(JIRA.Events.ISSUE_REFRESHED, function() {
                execute()
            });
        }
    },

    /**
     * Cancels any pending load so that their handlers aren't called
     */
    abortPending: function () {
        this.fieldsLoader.cancel();
        JIRA.Components.IssueViewer.prototype.abortPending.call(this);
    },

    /**
     * Clean up before hiding an issue (hide UI widgets, remove metadata, etc.).
     */
    beforeHide: function () {
        this.issueSaver.setSkipSaveIssueSuccessHandler(true);
        JIRA.Components.IssueViewer.prototype.beforeHide.call(this);
    },

    /**
     * Set the container that the issue should be rendered into.
     *
     * @param {element} container The container the issue should be rendered into.
     */
    setContainer: function (container) {
        this.editIssueController.setIssueViewContext(container);
        JIRA.Components.IssueViewer.prototype.setContainer.call(this, container);
    },

    dismiss: function() {
        this.editIssueController.reset();
        JIRA.Components.IssueViewer.prototype.dismiss.call(this);
    },

    getFields: function() {
        return this.editIssueController.getFields();
    },

    hasSavesInProgress: function() {
        return this.saveInProgressManager.hasSavesInProgress();
    },

    /**
     * Create and show a `SaveError`.
     *
     * @param {object} options
     * @param {string} options.issueId The ID of the issue that failed to save.
     * @param {string} options.issueKey The key of the issue that failed to save.
     * @param {object} options.response An `IssueSaverService` response.
     * @private
     */
    _showSaveError: function (options) {
        var saveError, stopListening;

        saveError = new JIRA.Components.IssueEditor.Views.SaveError(options).render();
        stopListening = _.partial(this.stopListening, saveError);

        this.listenTo(saveError, {
            close: stopListening,
            issueLinkClick: this.loadIssue
        });
    }
});
