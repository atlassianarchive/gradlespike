AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueeditor");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueeditor-test");

module('JIRA.Components.IssueEditor', {
    setup: function () {
        this.sandbox = sinon.sandbox.create();
        this.sandbox.useFakeServer();
        this.sandbox.stub(JIRA.Messages, "showErrorMsg");

        this.module = new JIRA.Components.IssueEditor();
    },

    teardown: function () {
        this.sandbox.restore();
    }
});

test("When loading fields, the issue cache is updated", function(){
    var updateIssue = this.sandbox.stub(this.module.viewIssueData,"updateIssue");

    var data = {issueKey: "KEY-1", fields:[]};
    this.module.fieldsLoader.trigger("fieldsLoaded", data);

    ok(updateIssue.calledOnce, "The issue has been updated");
    equal(updateIssue.firstCall.args[0], data.issueKey, "The correct issueKey has been updated");
    deepEqual(updateIssue.firstCall.args[1], data, "All the event data is passed to the cache updater");
});

test("When loading fields, editIssueController is updated with the right values", function(){
    this.sandbox.stub(this.module.editIssueController,"update");

    var data = {issueKey: "KEY-1", issueId: 1, fields:[{id: '1'}]};
    this.module.fieldsLoader.trigger("fieldsLoaded", data);

    ok(this.module.editIssueController.update.calledOnce, "The controller has been updated");
    deepEqual(this.module.editIssueController.update.firstCall.args[0], data, "The data is passed to the controller");
});

test("When loading fields, editIssueController is not updated if the issue is not editable anymore", function(){
    this.sandbox.stub(this.module.editIssueController,"update");

    var data = {issueKey: "KEY-1", issueId: 1, fields:[]};
    this.module.fieldsLoader.trigger("fieldsLoaded", data);

    ok(!this.module.editIssueController.update.calledOnce, "The controller has not been updated");
});

test("When loading fields, editIssueController is reset if the issue is not editable anymore", function(){
    this.sandbox.stub(this.module.editIssueController,"reset");

    var data = {issueKey: "KEY-1", issueId: 1, fields:[]};
    this.module.fieldsLoader.trigger("fieldsLoaded", data);

    ok(this.module.editIssueController.reset.calledOnce, "The controller is reset");
});

test("When loading fields, the trace 'jira.issue.fields.loaded' is thrown", function(){
    this.sandbox.stub(JIRA,"trace");

    var data = {issueKey: "KEY-1", issueId: 1, fields:[{id: '1'}]};
    this.module.fieldsLoader.trigger("fieldsLoaded", data);

    ok(JIRA.trace.calledOnce, "A trace has been fired");
    equal(JIRA.trace.firstCall.args[0], "jira.issue.fields.loaded", "A trace has been fired");
});

test("When loading fields with errors, the errors are shown", function(){
    this.sandbox.stub(this.module.fieldsController, "showError");

    var data = {errorCollection: ["error1"], isTimeout: false};
    this.module.fieldsLoader.trigger("fieldsError", data);

    ok(this.module.fieldsController.showError.calledOnce, "The errors are rendered");
    deepEqual(this.module.fieldsController.showError.firstCall.args[0], data.errorCollection, "The list of errors is passed to the controller");
    equal(this.module.fieldsController.showError.firstCall.args[1], data.isTimeout, "The timeout flag is passed to the controller");
});

test("When loading fields with errors, the trace 'jira.issue.fields.loaded' is thrown", function(){
    this.sandbox.stub(JIRA,"trace");

    var data = {errorCollection: ["error1"], isTimeout: false};
    this.module.fieldsLoader.trigger("fieldsError", data);

    ok(JIRA.trace.calledOnce, "A trace has been fired");
    equal(JIRA.trace.firstCall.args[0], "jira.issue.fields.loaded", "A trace has been fired");
});

test("Shows a SaveError when a non-visible issue fails to save", function () {
    var loadIssueStub = this.stub(this.module, "loadIssue"),
        options,
        saveError = new Backbone.View(),
        saveErrorStub = this.sandbox.stub(JIRA.Components.IssueEditor.Views, "SaveError").returns(saveError);

    options = {
        issueId: 10000,
        issueKey: "ABC-123",
        response: {}
    };

    this.module.model.set("id", 1);
    this.module.issueSaver.trigger("error", options);
    ok(saveErrorStub.calledWithNew(), "A new SaveError was created");
    sinon.assert.calledWithExactly(saveErrorStub, options);

    options = {
        id: 10000,
        key: "ABC-123"
    };

    saveError.trigger("issueLinkClick", options);
    ok(loadIssueStub.calledWithExactly(options), "Clicking the issue link loaded the issue");
});

test("When there is an error saving an issue, the edition is reset if the issue is not editable anymore", function() {
    this.sandbox.stub(this.module.editIssueController, "reset");

    var data = {
        issueId: 1,
        issueKey: "KEY-1",
        attemptedSavedIds: [1],
        response: {errorCollection:{}}
    };

    this.module.model.set("id", 1);
    this.module.issueSaver.trigger("error", data);

    ok(this.module.editIssueController.reset.calledOnce, "The edition is reset");
});

test("When there is an error saving an issue, the model is updated if the issue is not editable anymore", function() {
    this.sandbox.stub(this.module.model, "update");

    var data = {
        issueId: 1,
        issueKey: "KEY-1",
        attemptedSavedIds: [1],
        response: {errorCollection:{}}
    };

    this.module.model.set("id", 1);
    this.module.issueSaver.trigger("error", data);

    ok(this.module.model.update.calledOnce, "The model is updated");
    deepEqual(this.module.model.update.firstCall.args[0], data.response, "The model is updated with the correct response");
    deepEqual(this.module.model.update.firstCall.args[1], {fieldsSaved:[],fieldsInProgress:[]}, "The model is updated with the correct fields");
});

test("When there is an error saving an issue, the trace 'jira.issue.refreshed' is thrown", function(){
    this.sandbox.stub(JIRA,"trace");

    var data = {
        issueId: 1,
        issueKey: "KEY-1",
        attemptedSavedIds: [1],
        response: {errorCollection:{}}
    };

    this.module.model.set("id", 1);
    this.module.issueSaver.trigger("error", data);

    ok(JIRA.trace.calledOnce, "A trace has been fired");
    equal(JIRA.trace.firstCall.args[0], "jira.issue.refreshed", "A trace has been fired");
    deepEqual(JIRA.trace.firstCall.args[1], {id: data.issueId}, "The trace includes information about the issue");
});


test("When successfully saving an issue, the issue is updated", function() {
    var update = this.sandbox.stub(this.module.model,"update");
    this.sandbox.stub(this.module.editIssueController,"getEditsInProgress").returns([2,3]);

    var data = {
        issueId: 1,
        issueKey: "KEY-1",
        savedFieldIds: [1],
        response: {fields:{}}
    };
    this.module.model.setId(1);
    this.module.issueSaver.trigger("save", data);

    ok(update.calledOnce, "The issue has been updated");
    deepEqual(update.firstCall.args[0], data.response, "The issue data is passed to the model");
    deepEqual(update.firstCall.args[1], {fieldsSaved: [1], fieldsInProgress: [2,3]}, "The fields are passed to the model");
});

test("When successfully saving an issue, the edit controller is updated with the new data if the issue is editable", function() {
    var update = this.sandbox.stub(this.module.editIssueController,"update");

    var data = {
        issueId: 1,
        issueKey: "KEY-1",
        savedFieldIds: [1],
        response: {fields:[{id:1}]}
    };
    this.module.model.setId(1);
    this.module.issueSaver.trigger("save", data);

    ok(update.calledOnce, "The edit controller has been updated");
});

test("When successfully saving an issue, the edit controller is not updated with the new data if the issue is not editable", function() {
    var update = this.sandbox.stub(this.module.editIssueController,"update");

    var data = {
        issueId: 1,
        issueKey: "KEY-1",
        savedFieldIds: [1],
        response: {fields:{}}
    };
    this.module.model.setId(1);
    this.module.issueSaver.trigger("save", data);

    ok(!update.calledOnce, "The edit controller has not been updated");
});

test("When successfully saving an issue, JIRA traces are thrown", function() {
    this.sandbox.stub(JIRA,"trace");

    var data = {
        issueId: 1,
        issueKey: "KEY-1",
        savedFieldIds: [1],
        response: {fields:{}}
    };
    this.module.model.setId(1);
    this.module.issueSaver.trigger("save", data);

    ok(JIRA.trace.calledTwice, "Two traces have been fired");

    equal(JIRA.trace.firstCall.args[0], "jira.psycho.issue.refreshed", "The trade 'jira.psycho.issue.refreshed' has been fired");
    deepEqual(JIRA.trace.firstCall.args[1], {id: data.issueId}, "The psycho trace includes information about the issue");

    equal(JIRA.trace.secondCall.args[0], "jira.issue.refreshed", "The trade 'jira.issue.refreshed' has been fired");
    deepEqual(JIRA.trace.firstCall.args[1], {id: data.issueId}, "The trace includes information about the issue");
});

test("When successfully saving an issue, the event 'saveSuccess' is triggered", function() {
    var spy = this.sandbox.spy();
    this.module.listenTo(this.module, "saveSuccess", spy);

    var data = {
        issueId: 1,
        issueKey: "KEY-1",
        savedFieldIds: [1],
        response: {fields:{}},
        duration: 123
    };
    this.module.model.setId(1);
    this.module.issueSaver.trigger("save", data);

    ok(spy.calledOnce, "The event have been triggered");
    equal(spy.firstCall.args[0].issueId, data.issueId, "The event includes the issue id");
    equal(spy.firstCall.args[0].duration, data.duration, "The event includes the issue key");
});

test("When successfully saving an issue, it updates the issue data", function () {
    this.sandbox.stub(this.module.viewIssueData, "set");

    var data = {
        issueId: 1,
        issueKey: "KEY-1",
        savedFieldIds: ["summary"],
        response: {fields:[{id:"summary"}]}
    };
    this.module.model.setId(1);
    this.module.issueSaver.trigger("save", data);

    ok(this.module.viewIssueData.set.calledOnce, "viewIssueData.set was called");
    ok(this.module.viewIssueData.set.calledWithExactly(data.issueKey, data.response), "viewIssueData.set was called with the right arguments");
});

test("When successfully saving an issue, nothing is done if the issue is not the current issue in the model", function(){
    this.sandbox.stub(this.module.viewIssueData,"set");
    this.sandbox.stub(this.module.model,"update");
    this.sandbox.stub(this.module.editIssueController,"update");

    var data = {
        issueId: 1,
        issueKey: "KEY-1",
        savedFieldIds: ["summary"],
        response: {fields:[{id:"summary"}]}
    };
    this.module.model.setId(2);
    this.module.issueSaver.trigger("save", data);

    ok(!this.module.viewIssueData.set.called, "The issue has not been set");
    ok(!this.module.model.update.called, "The issue has not been updated");
    ok(!this.module.editIssueController.update.called, "The edit controller has not been updated");
});

test("When successfully saving an issue, nothing is done if the issue if the handler has been marked as skippable", function(){
    this.sandbox.stub(this.module.viewIssueData,"set");
    this.sandbox.stub(this.module.model,"update");
    this.sandbox.stub(this.module.editIssueController,"update");

    var data = {
        issueId: 1,
        issueKey: "KEY-1",
        savedFieldIds: ["summary"],
        response: {fields:[{id:"summary"}]},
        shouldSkipSaveIssueSuccessHandler: true
    };
    this.module.model.setId(1);
    this.module.issueSaver.trigger("save", data);

    ok(!this.module.viewIssueData.set.called, "The issue has not been set");
    ok(!this.module.model.update.called, "The issue has not been updated");
    ok(!this.module.editIssueController.update.called, "The edit controller has not been updated");
});


test("When the user wants to save an issue, ensure the save actions are not skipped", function() {
    this.sandbox.stub(this.module.issueSaver, "setSkipSaveIssueSuccessHandler");

    this.module.editIssueController.trigger("save", 1, "KEY", [], {}, {});

    ok(this.module.issueSaver.setSkipSaveIssueSuccessHandler.calledOnce, "The method has been called");
    ok(this.module.issueSaver.setSkipSaveIssueSuccessHandler.calledWith(false), "The method has been called with the right values");
});

test("When there is an edit in progress, we show a message on page unload", function () {
    equal(typeof this.module.unloadInterceptor.onBeforeUnload(), "undefined");
    this.sandbox.stub(this.module.editIssueController,"getDirtyEditsInProgress").returns(["summary"]);
    equal(typeof this.module.unloadInterceptor.onBeforeUnload(), "string");
});

test("It updates the container of the editIssueController", function() {
    this.module.setContainer(AJS.$("#qunit-fixture"));
    ok(this.module.editIssueController.getIssueViewContext().is(AJS.$("#qunit-fixture")), "The EditIssueController's element was updated");
});

test("When dismissing the issue, editIssueController is reset", function () {
    this.sandbox.stub(this.module.editIssueController, "reset");
    this.module.dismiss();
    ok(this.module.editIssueController.reset.calledOnce);
});

test("When loading an issue, requests the fields if the issue is editable", function() {
    this.sandbox.stub(this.module.fieldsLoader, "load");
    this.module._onIssueLoaded(
        { issue: { id: 12345, key: "JRA-123", isEditable: true} },
        {},
        { issueEntity: {}}
    );
    ok(this.module.fieldsLoader.load.calledOnce, "Fields are loaded");
});

test("When loading an issue, don't requests the fields if the issue is not editable", function() {
    this.sandbox.stub(this.module.fieldsLoader, "load");
    this.module._onIssueLoaded(
        { issue: { id: 12345, key: "JRA-123", isEditable: false} },
        {},
        { issueEntity: {}}
    );
    ok(!this.module.fieldsLoader.load.called, "Fields are loaded");
});

test("When updating an issue details and merging into current, when loaded, don't reset the editIssueController.", function () {
    var resetEditIssueControllerSpy = this.sandbox.spy(this.module.editIssueController, "reset");
    this.sandbox.stub(this.module.editIssueController, "update");

    this.module._onIssueLoaded({
        issue: {id: 12345, key: "JRA-123", isEditable: false},
        fields: "something"
    }, {
        isUpdate: true,
        mergeIntoCurrent: true
    }, { issueEntity: {}});

    ok(!resetEditIssueControllerSpy.called, "reset() should not be called on the editIssueController.");
});

test("When editing a field, it does nothing if the field is not editable", function () {
    var field = new JIRA.Components.IssueEditor.Models.Field({
        editHtml: ''
    });
    this.sandbox.stub(field, "matchesFieldSelector").returns(true);
    this.sandbox.stub(field, "edit");

    this.module.editField(field);

    ok(!field.edit.called, "Field is not edited");
});