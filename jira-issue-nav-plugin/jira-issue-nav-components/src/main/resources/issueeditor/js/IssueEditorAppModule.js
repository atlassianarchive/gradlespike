AJS.namespace("JIRA.Components.IssueEditor.AppModule");

JIRA.Components.IssueEditor.AppModule = JIRA.Components.IssueViewer.AppModule.extend({
    name: "issueEditor",
    generateMasterRequest: true,

    create: function(options) {
        options = _.defaults({}, options, {
            showReturnToSearchOnError: function() { return false;}
        });

        return new JIRA.Components.IssueEditor({
            showReturnToSearchOnError: options.showReturnToSearchOnError
        });
    },

    commands: function(module) {
        var viewerCommands = JIRA.Components.IssueViewer.AppModule.prototype.commands.call(this, module);
        return _.extend(viewerCommands, {
            "editField": true
        })
    },

    requests: function(module) {
        var viewerRequests = JIRA.Components.IssueViewer.AppModule.prototype.requests.call(this, module);
        return _.extend(viewerRequests, {
            "fields": function () {
                return module.getFields();
            },
            "hasSavesInProgress": true
        });
    },

    events: function(module) {
        var viewerEvents = JIRA.Components.IssueViewer.AppModule.prototype.events.call(this, module);

        return viewerEvents.concat([
            "saveSuccess",
            "saveError",
            "editField",
            "fieldSubmitted"
        ]);
    }
});