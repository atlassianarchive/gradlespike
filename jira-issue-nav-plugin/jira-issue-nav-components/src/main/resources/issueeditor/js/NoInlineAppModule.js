AJS.namespace("JIRA.Components.IssueEditor.NoInlineAppModule");

JIRA.Components.IssueEditor.NoInlineAppModule = JIRA.Components.IssueViewer.AppModule.extend({
    name: "issueEditor",

    requests: function(module) {
        var viewerRequests = JIRA.Components.IssueViewer.AppModule.prototype.requests.call(this, module);
        return _.extend(viewerRequests, {
            "fields": function() {
                return [];
            },
            "hasSavesInProgress": function() {
                return false;
            }
        })
    }
});