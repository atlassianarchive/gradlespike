AJS.namespace("JIRA.Components.IssueEditor.Views.FieldView");

/**
 * Handles the saving, editing and canceling of an Issue Field.
 */
JIRA.Components.IssueEditor.Views.FieldView = JIRA.Marionette.ItemView.extend({

    events: {
        "click .cancel" : "onClickCancel",
        "keyup" : "onKeyCancel",
        "click .submit" : "onSubmit",
        "click" : "onEdit",
        "mousedown": "saveSelectedText",
        "submit form" : "onSubmit",
        'keydown select[multiple="multiple"]': "_enterToSubmitMultiSelect",
        'keypress select': "_enterToSubmitSelect",
        "beforeBlurInput *": "_preventBlurByEsc"
    },

    initialize: function (options) {

        this.model.on("validationError",  this.handleValidationError, this);
        this.model.on("editingStarted",   this.switchToEdit,          this);
        this.model.on("focusRequested",   this.focus,                 this);
        this.model.on("editingCancelled", this.switchToRead,          this);
        this.model.on("saveError",        this.handleSaveError,       this);
        this.model.on("updateRequired",   this.updateModel,           this);
        this.model.on("savingStarted",    this.handleSavingStarted,   this);
        this.model.on("modelDestroyed",   this.destroy,               this);

        this.decorate();

        this.issueEventBus = options.issueEventBus;
        this.issueEventBus.on("panelRendered", this.handlePanelRendered, this);

        this.model.setFieldType(this.$el.data('fieldtypecompletekey'));

        this._editDelay = 0;

        var instance = this;

        JIRA.bind(JIRA.Events.INLINE_EDIT_BLURRED, function(e, fieldId) {
            if (fieldId === instance.model.getId()) {
                instance._onPossibleBlur();
            }
        });

        JIRA.bind(JIRA.Events.INLINE_EDIT_REQUESTED, function(e, fieldId) {
            if (fieldId !== instance.model.getId() && instance._editDelay !== 0) {
                clearTimeout(instance._editDelay);
                instance._editDelay = 0;
            }
        });
    },

    /**
     *  Unbinds events for collection
     */
    destroy: function () {
        // all model handlers are unbound as a result of the collection being reset. We just need to handle the event bus
        this.issueEventBus.off("panelRendered", this.handlePanelRendered);
    },

    /**
     * Wraps the display value of a field to offer :hover edit prompts.
     */
    decorate: function () {
        if (this.model.isEditable() && !this.model.getEditing()) {
            this.$el.addClass("editable-field inactive");
            this.$el.append('<span class="overlay-icon aui-icon aui-icon-small aui-iconfont-edit" />');
            this._addToolTip();
        }
    },

    /**
     * Returns field's edit elements, i.e. excluding save options and throbber
     */
    getEditElements: function() {
        return this.$el.find(".inline-edit-fields");
    },

    /**
     * Adds an edit prompting tooltip to the issue field value.
     */
    _addToolTip: function() {
        this.$el.attr("title", AJS.I18n.getText("viewissue.start.inline.edit"));
        // override the tooltip on anchors without
        jQuery("a:not([title])", this.$el).attr("title", AJS.I18n.getText("viewissue.follow.link"));
    },

    _removeToolTip: function() {
        this.$el.removeAttr("title");
    },

    _stealAccessKeys: function() {
        jQuery("[accessKey='" + AJS.I18n.getText("AUI.form.submit.button.accesskey") + "']").attr("accessKey", "_s");
        jQuery("[accessKey='" + AJS.I18n.getText("AUI.form.cancel.link.accesskey") + "']").attr("accessKey", "_x");
    },

    _returnAccessKeys: function() {
        jQuery("[accessKey=_s]").attr("accessKey", AJS.I18n.getText("AUI.form.submit.button.accesskey"));
        jQuery("[accessKey=_x]").attr("accessKey", AJS.I18n.getText("AUI.form.cancel.link.accesskey"));
    },

    _handleEditingStarted: function (focus) {
        var $fieldTools = this.$el.find('.field-tools');

        JIRA.trigger(JIRA.Events.NEW_CONTENT_ADDED, [this.$el, JIRA.CONTENT_ADDED_REASON.inlineEditStarted]);
        JIRA.trigger(JIRA.Events.INLINE_EDIT_STARTED, [this.model.getId(), this.model.getFieldType(), this.getEditElements(), this.$el]);

        // Resize textareas, if any, to the right size
        this.$el.find(".textarea")
            .css("height", Math.max(this._originalHeight, 60))
            // 100 is roughly enough space, plus a bit more, for a heading (in the case of description),
            // a wrapped field label (for example Assignee if the window is narrow enough), and the save/cancel controls
            .expandOnInput(jQuery(window).height() - jQuery('#stalker').height() - 100);

        if (focus) {
            this.$el.find(":input:visible:first").focus();
        }
        if (!this.$el.find(".error").size()) {
            this.$el.find("select.aui-ss-select").each(function () {
                var $this = jQuery(this);
                if ($this.data("static-suggestions")) {
                    $this.trigger("showSuggestions")
                }
            });
        }
        this.$el.find('.save-options').attr('tabindex', 1).prepend($fieldTools);
    },

    handleValidationError: function (errorHtml, focus) {
        this.$el.html(JIRA.Templates.IssueEditor.Fields.field({
            issue: {
                id: this.model.id,
                editHtml: errorHtml
            },
            accessKey: JIRA.Components.IssueViewer.Legacy.IssueFieldUtil.getAccessKeyModifier()
        }));
        this.$el.find(".error").addClass("inline-edit-error").attr("data-field", this.model.id);
        this._handleEditingStarted(focus);
    },

    handlePanelRendered: function (panel, $ctx) {
        var $newEl = jQuery(JIRA.Components.IssueViewer.Legacy.IssueFieldUtil.getFieldSelector(this.model.id), $ctx);
        if ($newEl.length === 1) {
            this.$el = $newEl;
            this.el = this.$el[0];
            this.decorate();
            this.delegateEvents();
        }
    },

    /**
     * Called in the event of a save error.
     *
     * Ensures the view is in edit mode so the error can be rectified.
     */
    handleSaveError: function () {
        this.$el.find(":input").removeAttr("disabled").trigger("enable");
        this.$el.removeClass("saving saving-" + this.model.id);
    },

    /**
     * The view is put into saving mode and any input is disabled to prevent changes whilst the save request is in flight.
     */
    handleSavingStarted: function () {
        this.$el.find(":input").attr("disabled", "disabled").trigger("disable");
        this.$el.addClass("saving saving-" + this.model.id);
    },

    updateModel: function () {
        this.model.update(this.$el);
    },

    /**
     * Reveals the Issue Field View's element.
     *
     * Reveal the element in the following situations (or a combination of them) by triggering.
     *  - Scrolled off screen.
     *  - Scrolled behind the stalker.
     *  - Toggle open a closed twixie container which contains the field.
     *  - Select an unselected tab of containing the field.
     *  - Toggle open a closed twixie container containing a closed tab which contains the element.
     *
     * This is done by:
     *  - Using scrollIntoView to eagerly reveal the position of the element on the screen.
     *  - Triggering a reveal event on the element which is subscribed to by parent elements to reveal if necessary.
     *
     *  Additional marginTop is given to scrollIntoView to ensure the field itself and it's label element are in view.
     *  i.e. Assignee and Reporter have stacked labels in certain situations.
     */
    reveal: function() {
        var padding = 40;
        this.$el.scrollIntoView({
            direction: "y",
            marginBottom: padding,
            marginTop: AJS.$("#stalker").outerHeight(true) + padding
        });
        this.$el.trigger("reveal");
    },

    focus: function () {
        this.reveal();
        // Ensure the field being focused is not disabled (throws an error in IE8)
        this.$el.find(":input").removeAttr("disabled");

        // Don't focus if a modal dialog is present. Note: this doesn't affect
        // ModalIssueFieldView as it doesn't use IssueFieldView.
        if (jQuery(".aui-blanket").length === 0) {
            var $element = this.$el.find(":input:visible:first");
            $element.focus();
            if (!this.$el.hasClass("field-ignore-highlight")) {
                $element.select();
            }
        }
    },

    /**
     * Transitions the view into the edit state.
     */
    switchToEdit: function () {
        this.trigger("editField", { fieldId: this.model.getId() } );
        this._stealAccessKeys();
        this._removeToolTip();
        this.$el.removeClass("inactive saving").addClass("active");
        this._originalHeight = this.$el.height();
        this.model.switchElToEdit(this.$el);
        // NOTE:
        // This order is important! The INLINE_EDIT_STARTED event *must* be dispatched before the
        // edited field is focused, otherwise no INLINE_EDIT_FOCUSED event will be dispatched.
        this._handleEditingStarted();
        this.focus();
    },

    /**
     * Transitions the view into the view state.
     */
    switchToRead: function () {
        this._addToolTip();
        this.$el.addClass("inactive").removeClass("active");
        this.$el.html(this.model.getViewHtml());
        this.$el.closest("form").unbind("submit", this.onSubmit);
        this._returnAccessKeys();
    },

    /**
     * Cancels editing when an escape key is encountered
     * @param e {Event}
     */
    onKeyCancel: function (e) {
        if(e.keyCode === 27) {
            this.model.cancelEdit(JIRA.Issues.CANCEL_REASON.escPressed);
            e.preventDefault();
        }
    },

    /**
     * Cancels editing when cancel button is clicked.
     * @param e {Event}
     */
    onClickCancel: function (e) {
        this.model.cancelEdit();
        e.preventDefault();
    },

    onSubmit: function (e) {
        var event = new jQuery.Event("before-submit");
        this.$el.find("form").trigger(event);
        if (!event.isDefaultPrevented()) {
            this.$el.find(':focus').blur(); // JRADEV-10807 Make sure no inputs are capturing events while a save is in progress

            this.issueEventBus.triggerFieldSubmitted();

            // Note: Even though the above blur() will trigger a save on its own,
            // the blur handler checks this.model.getSaving() to ensure we don't
            // fire duplicate save requests.
            this.model.save();
            this._returnAccessKeys();
        }
        e.preventDefault();

    },

    onEdit: function(event) {
        var iconWasClicked = AJS.$(event.target).is(".overlay-icon.aui-iconfont-edit");
        var isEditable = AJS.$(event.target).closest("a, .uneditable").length === 0 && this.$el.hasClass("inactive");
        var noTextSelected = this._getCurrentlySelectedText() === "" && !this._previouslySelectedText;
        var shouldEnterEditMode = isEditable && (iconWasClicked || noTextSelected);

        if (this._editDelay !== 0) {

            // A click event was received while a pending inline edit request was
            // in progress. Cancel this request, since the user is double-clicking
            // or triple-clicking instead.

            clearTimeout(this._editDelay);
            this._editDelay = 0;

        } else if (shouldEnterEditMode) {

            JIRA.trigger(JIRA.Events.INLINE_EDIT_REQUESTED, [this.model.getId()]);

            var self = this;

            if (iconWasClicked) {

                // If this click event occurred directly on the pencil icon, enter
                // inline edit mode straight away.
                //
                // We handle this event on the document to allow event listeners
                // registered on ancestors to have the opportunity to preventDefault()
                // and thus cancel inline edit.

                jQuery(document).one("click", function(event) {
                    if (!event.isDefaultPrevented()) {
                        self.model.edit();
                    }
                });

            } else {

                // If this click occurs somewhere else on the field besides the pencil
                // icon, wait briefly to allow the user the opportunity to double-click.
                // Double-clicks will abort the pending transition to inline edit mode.

                this._editDelay = setTimeout(function() {
                    if (!event.isDefaultPrevented() && self.$el.hasClass("inactive") && self._getCurrentlySelectedText() === "") {
                        self.model.edit();
                    }
                    self._editDelay = 0;
                }, 250);
            }
        }
    },

    /**
     * Either the field inputs have blurred, or the save options have blurred,
     * but we need to check if both are blurred
     */
    _onPossibleBlur: function() {
        if (this.model.getSaving()) {
            // Saving is already in progress. No need to trigger save-on-blur.
            return;
        }
        if (jQuery(".aui-blanket").length > 0) {
            // A modal dialog interrupted; don't try to save. This doesn't
            // affect ModalIssueFieldView as it doesn't use IssueFieldView.
            return;
        }
        this.model.blurEdit();
    },

    /**
     * Prevent AUI's default handler from blurring inputs when pressing the Esc key, since we handle this ourselves in onCancel.
     * This stops the field from being blurred by pressing Esc when the calendar dropdown is shown.
     */
    _preventBlurByEsc: function(e) {
        e.preventDefault();
    },

    /**
     * @private
     * @return {string}
     */
    _getCurrentlySelectedText: function() {
        if (jQuery(document.activeElement).is(":input")) {
            // Text selections inside form elements are not considered.
            return "";
        }
        if (document.selection && document.selection.createRange) {
            return document.selection.createRange().text || "";
        }
        if (window.getSelection) {
            return window.getSelection().toString();
        }
        return "";
    },

    /**
     * JRA-30597 Cause Enter key on <select>s to submit the form (save inline edit).
     *
     * Chrome does this by itself but not Firefox or IE.
     */
    _enterToSubmitSelect: function(e) {
        if (!e.isDefaultPrevented()) {
            if (e.which === 13) {
                e.preventDefault(); // Chrome already submits on Enter key
                jQuery(e.target).closest('form').submit();
            }
        }
    },

    /**
     * JRA-30597 Cause Enter key on <select multiple="multiple">s to blur the <select>, causing a save-on-blur.
     *
     * This is necessary because if multiple options are selected, pressing enter will unselect all of them and select only
     * one option. This is confusing for users who expect enter to preserve their current selection and submit the inline edit.
     */
    _enterToSubmitMultiSelect: function(e) {
        if (!e.isDefaultPrevented()) {
            if (e.which === 13) {
                jQuery(e.target).blur();
            }
        }
    },

    /**
     * Text selection is cleared after mousedown; this method stores it in an
     * instance variable so we can determine in onEdit if selection was cleared.
     */
    saveSelectedText: function() {
        this._previouslySelectedText = this._getCurrentlySelectedText();
    }
});
