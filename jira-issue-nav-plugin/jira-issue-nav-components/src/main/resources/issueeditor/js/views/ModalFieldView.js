AJS.namespace("JIRA.Components.IssueEditor.Views.ModalFieldView");

/**
 * A modal dialog for editing the value of a field.
 *
 * @param {JIRA.Components.IssueViewer.Legacy.IssueEventBus} options.issueEventBus
 * @param {JIRA.Components.IssueEditor.Models.Field} options.model The field to edit.
 * @param {{id: string, key: string}} options.issue The issue on which the field will be edited
 */
JIRA.Components.IssueEditor.Views.ModalFieldView = JIRA.Marionette.ItemView.extend({
    errorsTemplate: JIRA.Templates.IssueEditor.Fields.modalFieldGeneralErrors,
    template: JIRA.Templates.IssueEditor.Fields.modalField,

    initialize: function(options) {
        _.bindAll(this,
            "_onHide",
            "_onSubmit",
            "_remove",
            "_showGeneralError",
            "_showValidationError",
            "_updateModel");

        // Use JIRA.bind to avoid a jQuery bug.
        JIRA.bind("Dialog.hide", this._onHide);

        this.issueEventBus = options.issueEventBus;
        this.issueEventBus.on("saveError", this._showGeneralError);

        this.model.onSaveSuccess(this._remove);
        this.model.onUpdateRequired(this._updateModel);
        this.model.onValidationError(this._showValidationError);
    },

    /**
     * Prepare the dialog for editing.
     *
     * To be called after a request completes.
     *
     * @private
     */
    _enable: function() {
        var dialog = this._getDialog();
        dialog.hideFooterLoadingIndicator();

        var $el = dialog.$popup;
        $el.find(":input").removeAttr("disabled").trigger("enable");
        $el.find(":input:first").focus().select();
        $el.find(":submit").removeAttr("disabled");
    },

    _getDialog: function() {
        if (!this._dialog) {
            this._dialog = new JIRA.FormDialog({
                id: "modal-field-view",
                submitHandler: this._onSubmit,
                content: _.bind(function (callback) {
                    callback(this.template({
                        field: this.model.toJSON(),
                        issue: this.options.issue
                    }));
                }, this)
            });

            // Attach the field's ID to the dialog so ViewIssueController can
            // determine whether a deferred error needs to be shown or not.
            this._dialog.get$popup().data("field-id", this.model.getId());
        }

        return this._dialog;
    },

    _onHide: function(e, $popup) {
        if ($popup === this._getDialog().get$popup()) {
            this._remove();
        }
    },

    /**
     * Attempts to save the IssueFieldModel.
     *
     * @private
     */
    _onSubmit: function(e, ready) {
        e.preventDefault();
        this.model.save();
        ready();

        var dialog = this._getDialog();
        dialog.showFooterLoadingIndicator();
        dialog.$popup.find(":input").attr("disabled", "disabled")
            .trigger("disable");
    },

    close: function() {
        this._remove();
    },

    /**
     * Destroy the dialog and unbind all event handlers.
     *
     * @private
     */
    _remove: function(e) {
        var dialog = this._getDialog();
        if (dialog.get$popup().is(":visible")) {
            dialog.hide();
        }

        dialog.destroy();
        this.model.cancelEdit();

        JIRA.unbind("Dialog.hide", this._onHide);
        this.issueEventBus.off("saveError", this._showGeneralError);
        this.model.off("saveSuccess",this._remove);
        this.model.off("updateRequired",this._updateModel);
        this.model.off("validationError",this._showValidationError);
    },

    /**
     * Show the view.
     */
    show: function() {
        this.model.edit();
        this._getDialog().show();
    },

    /**
     * Show a "general" (non-field-specific) error if necessary.
     *
     * @param issueId The ID of the issue we tried to save.
     * @param fieldIds The IDs of the fields we tried to save.
     * @param response The HTTP response that was returned.
     * @private
     */
    _showGeneralError: function(issueId, fieldIds, response) {
        // We're responding to a general error, so re-enable the controls.
        this._enable();

        var $el = this._getDialog().$popup;

        var errors = response && response.errorCollection;
        if (errors) {
            var hasFieldErrors = !_.isEmpty(errors.errors),
                hasGeneralErrors = !_.isEmpty(errors.errorMessages);

            if (!hasFieldErrors && hasGeneralErrors) {
                $el.find(".form-body").append(this.errorsTemplate({
                    messages: errors.errorMessages
                }));
            }
        }
    },

    /**
     * Show a validation error.
     *
     * @param editHtml The field's edit HTML (including the validation error).
     * @private
     */
    _showValidationError: function(editHtml) {
        this._enable();
        this._getDialog().$popup.find(".form-body").html(editHtml);
    },

    /**
     * Update the model with the user's input.
     *
     * @private
     */
    _updateModel: function() {
        this.model.update(this._getDialog().$popup);
    }
});
