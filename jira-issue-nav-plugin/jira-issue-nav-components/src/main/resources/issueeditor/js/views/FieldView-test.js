AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueeditor");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueeditor-test");

module('JIRA.Components.IssueEditor.Views.FieldView', {
    setup: function() {
        //Stop inline layer from appending to window.top so qunit iframe runner works
        AJS.params.ignoreFrame = true;
        this.$el = jQuery("<div><a href='#'><span class='link-child'>initial</span><strong class='twixi'></strong></a></div>").appendTo("#qunit-fixture");
        this.collection = new JIRA.Components.IssueEditor.Collections.Fields();
        this.collection.add({
            id: "summary",
            editHtml: "<div class='field-group'><input type=\"text\" name=\"myinput\" value=\"initial\"></div>"
        });
        this.model = this.collection.at(0);
        this.view = new JIRA.Components.IssueEditor.Views.FieldView({
            issueEventBus: new JIRA.Components.IssueViewer.Legacy.IssueEventBus(),
            model: this.model,
            el: this.$el
        });

        this.assertInEditMode = _.bind(function () {
            equal(this.model.getEditing(), true, "Model says we are editing");
            ok(this.$el.find("[name=myinput]"), "Input element exists");
            equal(this.$el.hasClass("inactive"), false, "Inactive class is not present on the element");
        }, this);

        this.assertInReadMode = _.bind(function () {
            equal(!!this.model.getEditing(), false, "Model says we are not editing");
            equal(this.$el.find("span").html(), "initial", "Span exists");
            equal(this.$el.hasClass("inactive"), true, "Inactive class is present on the element");
        }, this);
    },
    teardown: function() {
        this.$el.remove();
    }
});

test("Readonly mode is default state", function () {
    this.assertInReadMode();
});

asyncTest("when readonly view is clicked, we switch to edit", function () {
    this.$el.click();
    var tester = this;
    setTimeout(function() {
        tester.assertInEditMode();
        start();
    }, 400);
});

test("when model is set to editing, so is the view", function () {
    this.model.edit();
    this.assertInEditMode();
});

test("when model is set to editing, inline edit started event is fired", function () {
    var spy = sinon.spy();
    JIRA.bind(JIRA.Events.INLINE_EDIT_STARTED, spy);
    this.model.edit();
    equal(spy.callCount, 1);
    equal(spy.args[0][1], "summary");
    var $inputEls = spy.args[0][3].children();
    equal($inputEls.length, 1);
    ok($inputEls.is(".field-group"));
});

test("when model is set to editing, inline edit receives multiple elements", function() {
    this.model.setEditHtml("<input type=\"text\" name=\"myinput1\" ><input type=\"text\" name=\"myinput2\" ><input type=\"text\" name=\"myinput3\" >");
    var spy = sinon.spy();
    JIRA.bind(JIRA.Events.INLINE_EDIT_STARTED, spy);
    this.model.edit();
    equal(spy.callCount, 1);
    var $inputEls = spy.args[0][3].children();
    equal($inputEls.length, 3);
});

test("When cancel event is triggered on collection we switch to readonly mode", function () {
    this.model.edit(); // switch to edit
    this.collection.cancelEdit();
    this.assertInReadMode();
});

test("Update causes params to be updated in model", function () {
    this.model.edit(); // switch to edit
    this.$el.find(":input").val("My New Val");
    this.model.update(this.$el);
    deepEqual(this.model.getParams(), {myinput: "My New Val"});
});

test("Cancel is triggered either by ESC or via onCancel call", function() {

    this.model.edit(); // switch to edit
    var spy = sinon.spy();

    this.model.cancelEdit = spy;
    var e = jQuery.Event("keyup");
    e.keyCode = 50;
    this.$el.trigger(e);
    equal(spy.callCount, 0, "Random keyup shouldn't cancel the edit.");
    e = jQuery.Event("keyup");
    e.keyCode = 27;
    this.$el.trigger(e);
    equal(spy.callCount, 1, "ESC should cancel the edit.");

    this.model.edit({}); // switch to edit

    this.$el.find(".cancel").click();
    equal(spy.callCount, 2, "Clicking on cancel should also cancel the edit.");
});

test("Validation error in model switches view to edit mode", function () {
    this.model.setValidationError("<input/>", "An error occur");
    equal(this.view.$el.find(":text").length, 1, "Expected field to be shown");
    equal(this.model.getEditing(), true, "Expected model to be in edit mode");
});

test("Validation error should be appended", function () {
    this.model.setValidationError("<input/><div class=\"error\">An error occur</div>", "An error occur");
    equal(this.view.$el.find(".error").length, 1, "Expected only on error message");
    equal(this.view.$el.find(".error").text(), "An error occur");
});

asyncTest("Clicking on link doesn't enter edit", function () {
    var spy = sinon.spy();
    var $el = this.$el;
    this.model.edit = spy;
    $el.find("span.link-child").click();
    setTimeout(function() {
        equal(spy.callCount, 0, "Expected edit not to be called because we clicked a child of a link");
        $el.find("summary").click();
        setTimeout(function() {
            equal(spy.callCount, 0, "Expected edit not to be called because we clicked a link");
            $el.click();
            setTimeout(function() {
                equal(spy.callCount, 1, "Expected edit to to be called because we DIDN'T click a link");
                start();
            }, 400);
        }, 400);
    }, 400);
});

asyncTest("No duplicate INLINE_EDIT_FOCUSED and INLINE_EDIT_BLURRED events are fired", function() {

    var focusedSpy = sinon.spy();
    var blurredSpy = sinon.spy();
    var blurDelay = JIRA.Components.IssueEditor.InlineEditUtils.BLUR_FOCUS_TIMEOUT + 1;

    JIRA.bind(JIRA.Events.INLINE_EDIT_FOCUSED, focusedSpy);
    JIRA.bind(JIRA.Events.INLINE_EDIT_BLURRED, blurredSpy);

    this.model.edit();

    var $input = this.$el.find("input");
    $input.focus();
    $input.focus();

    setTimeout(function() {
        $input.blur();
        setTimeout(function() {
            $input.blur();
            setTimeout(function() {
                equal(focusedSpy.callCount, 1, "Only one INLINE_EDIT_FOCUSED event fired");
                equal(blurredSpy.callCount, 1, "Only one INLINE_EDIT_BLURRED event fired");
                start();
            }, blurDelay);
        }, blurDelay);
    }, 0);
});

test("Clicking on element that prevents default doesn't enter edit", function () {
    var spy = sinon.spy();
    var handlerRan = false;

    this.model.edit = spy;

    this.$el.click(function (e) {
        handlerRan = true;
        e.preventDefault();
    });
    this.$el.find(".twixi").click();
    ok(handlerRan);
    equal(spy.callCount, 0, "Expected edit not to be called because we clicked a child of a link");
});

test("Cancel on blur when clean", function () {
    var cancelSpy = sinon.spy(),
        saveSpy = sinon.spy();

    this.model.cancelEdit = cancelSpy;
    this.model.onSave(saveSpy);
    this.model.edit();

    JIRA.trigger(JIRA.Events.INLINE_EDIT_FOCUSED, ["summary"]);
    JIRA.trigger(JIRA.Events.INLINE_EDIT_BLURRED, ["summary"]);

    equal(cancelSpy.callCount, 1, "Expected cancel to occur since field is clean");
    equal(saveSpy.callCount, 0, "Expected save to NOT occur since field is clean");
});

test("Save on blur when dirty", function () {
    var cancelSpy = sinon.spy(),
        saveSpy = sinon.spy();

    this.model.cancelEdit = cancelSpy;
    this.model.onSave(saveSpy);
    this.model.edit();
    this.$el.find('input').val('dirty');

    JIRA.trigger(JIRA.Events.INLINE_EDIT_FOCUSED, ["summary"]);
    JIRA.trigger(JIRA.Events.INLINE_EDIT_BLURRED, ["summary"]);

    equal(cancelSpy.callCount, 0, "Expected cancel to NOT occur since field is dirty");
    equal(saveSpy.callCount, 1, "Expected save to occur since field is dirty");
});

test("Blurring twice does not trigger two saves", function () {
    var cancelSpy = sinon.spy(),
        saveSpy = sinon.spy();

    this.model.cancelEdit = cancelSpy;
    this.model.onSave(function () {
        // mock out what the EditIssueController would do
        this.handleSaveStarted();
    });
    this.model.onSave(saveSpy);
    this.model.edit();
    this.$el.find('input').val('dirty');

    JIRA.trigger(JIRA.Events.INLINE_EDIT_FOCUSED, ["summary"]);
    JIRA.trigger(JIRA.Events.INLINE_EDIT_BLURRED, ["summary"]);
    JIRA.trigger(JIRA.Events.INLINE_EDIT_BLURRED, ["summary"]);

    equal(cancelSpy.callCount, 0, "Expected cancel to NOT occur since field is dirty");
    equal(saveSpy.callCount, 1, "Expected save to occur only once");
});

test("Save or cancel is not triggered when save options gain focus", function() {
    var clock = sinon.useFakeTimers(),
        cancelSpy = sinon.spy(),
        saveSpy = sinon.spy();

    this.model.cancelEdit = cancelSpy;
    this.model.onSave(saveSpy);
    this.model.edit();
    this.$el.find('input').val('dirty');

    this.$el.find("input").focus();
    this.$el.find("input").blur();
    this.$el.find('.save-options button:first').focus();

    clock.tick(JIRA.Components.IssueEditor.InlineEditUtils.BLUR_FOCUS_TIMEOUT + 1);

    equal(cancelSpy.callCount, 0, "Expected cancel to NOT occur since save options has focus");
    equal(saveSpy.callCount, 0, "Expected save to NOT occur since save options has focus");

    clock.restore();
});

test("Save or cancel is not triggered when an input regains focus from save options", function() {
    var clock = sinon.useFakeTimers(),
        cancelSpy = sinon.spy(),
        saveSpy = sinon.spy();

    this.model.cancelEdit = cancelSpy;
    this.model.onSave(saveSpy);
    this.model.edit();
    this.$el.find('input').val('dirty');
    this.$el.find('.save-options button:first').focus();
    this.$el.find('.save-options button:first').blur();
    this.$el.find('input').focus();

    clock.tick(JIRA.Components.IssueEditor.InlineEditUtils.BLUR_FOCUS_TIMEOUT + 1);

    equal(cancelSpy.callCount, 0, "Expected cancel to NOT occur since save options has focus");
    equal(saveSpy.callCount, 0, "Expected save to NOT occur since save options has focus");

    clock.restore();
});

test("reveal is triggered on both edit and focus", function() {
    var revealSpy = sinon.spy();
    this.$el.bind("reveal", revealSpy);
    this.view.switchToEdit();
    equal(revealSpy.callCount, 1);
    this.view.focus();
    equal(revealSpy.callCount, 2);

});

test("JRA-28241 can prevent submit through event", function () {
    var cancelSpy = sinon.spy(),
        saveSpy = sinon.spy();

    this.model.cancelEdit = cancelSpy;
    this.model.onSave(saveSpy);
    this.model.edit();
    this.$el.find('input').val('dirty');

    this.$el.find("form").bind("before-submit", function (e) {
        e.preventDefault();
    });

    this.$el.find("form").submit();

    equal(cancelSpy.callCount, 0, "Expected cancel to NOT occur since save options has focus");
    equal(saveSpy.callCount, 0, "Expected save to NOT occur since save options has focus");
});

asyncTest("JRADEV-12018 - Inline edit is not enabled when event is transmitted from within a parent with class uneditable", function () {
    var spy = sinon.spy();
    var $el = this.$el;
    this.model.edit = spy;
    var uneditableElement = jQuery("<div class='uneditable'>This is an uneditable div</div>");
    $el.append(uneditableElement);
    uneditableElement.click();
    setTimeout(function() {
        equal(spy.callCount, 0, "Expected edit not to be called because we clicked an uneditable element");
        start();
    }, 400);
});

test("_handleEditingStarted() should trigger a JIRA.Events.NEW_CONTENT_ADDED", function () {
    var triggerSpy = sinon.spy(JIRA, "trigger");

    this.view._handleEditingStarted();

    equal(triggerSpy.callCount, 2, "Two events are triggered");
    equal(triggerSpy.firstCall.args[0], JIRA.Events.NEW_CONTENT_ADDED, "Event is JIRA.Events.NEW_CONTENT_ADDED");
    equal(typeof triggerSpy.firstCall.args[1][1], "string", "Ensure event has a reason argument");
    equal(triggerSpy.firstCall.args[1][1], JIRA.CONTENT_ADDED_REASON.inlineEditStarted, "Event reason is JIRA.CONTENT_ADDED_REASON.inlineEditStarted");
    equal(triggerSpy.secondCall.args[0], JIRA.Events.INLINE_EDIT_STARTED, "Second event is JIRA.Events.INLINE_EDIT_STARTED");

    triggerSpy.restore();
});

test("_handleEditingStarted() should trigger showSuggestions for single selects only", function () {

    var showSingleSuggestionsSpy = sinon.spy();
    var showMultiSuggestionsSpy = sinon.spy();
    var $singleSelect = jQuery("<select><option>blah</option></select>").bind("showSuggestions", showSingleSuggestionsSpy);
    var $multiSelect = jQuery("<select multiple='multiple'><option>blah</option><option>blah2</option></select>").bind("showSuggestions", showMultiSuggestionsSpy);

    this.view.$el.append($singleSelect);
    this.view.$el.append($multiSelect);

    new AJS.MultiSelect({
        element: $multiSelect
    });

    new AJS.SingleSelect({
        element: $singleSelect
    });

    this.view._handleEditingStarted();


    equal(showSingleSuggestionsSpy.callCount, 1, "Expected event to fire for single-selects");
    equal(showMultiSuggestionsSpy.callCount, 0, "Expected event not to fire for multi-selects")
});
