AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueeditor");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueeditor-test");

module("JIRA.Components.IssueEditor.Views.ModalFieldView", {
    setup: function() {
        this.sandbox = sinon.sandbox.create();

        this.fieldModel = new JIRA.Components.IssueEditor.Models.Field({
            id: "summary",
            label: "Summary",
            required: true,
            editHtml: [
                '<div class="field-group">',
                    '<label for="summary">Summary<span class="aui-icon icon-required">Required</span></label>',
                    '<input class="text long-field" id="summary"  name="summary" type="text" value="default"/>',
                '</div>'
                ].join("\n")
        });
    },

    showDialog: function() {
        var view = this.view = new JIRA.Components.IssueEditor.Views.ModalFieldView({
            issueEventBus: new JIRA.Components.IssueViewer.Legacy.IssueEventBus(),
            model: this.fieldModel,
            issue: {id: "1000", key: "BULK-15"}
        });

        view.show();

        // Unfortunately, there is no better way to avoid polluting the DOM, as JIRA.Dialog always injects the
        // content into <body>
        var dialog = view._getDialog().$popup;
        dialog.css('position', 'relative');
        AJS.$("#qunit-fixture").append(dialog);
    },

    submitDialog: function() {
        this.view._getDialog().$popup.find('input[type=submit]').click();
    },

    teardown: function() {
        this.sandbox.restore();
        if (this.view) {
            this.view.close();
        }
    }
});

test("Saves the IssueFieldModel", function() {
    this.sandbox.stub(this.fieldModel, "save");

    this.showDialog();
    this.submitDialog();

    ok(this.fieldModel.save.calledOnce);
});
