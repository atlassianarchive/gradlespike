AJS.namespace("JIRA.Components.IssueEditor.Views.LoadingError");

JIRA.Components.IssueEditor.Views.LoadingError = JIRA.Marionette.ItemView.extend({
    template: JIRA.Templates.IssueEditor.Fields.errorsLoading,

    initialize: function(errorMessages, isTimeout) {
        this.errorMessages = errorMessages;
        this.isTimeout = isTimeout;
    },

    serializeData: function() {
        return {
            errorMessages: this.errorMessages,
            isTimeout: this.isTimeout
        }
    },

    onRender: function() {
        this.setElement(JIRA.Messages.showErrorMsg(this.$el.html(), {
            closeable: true,
            timeout: 0
        }));
    }
});