AJS.namespace("JIRA.Components.IssueEditor.Views.SaveError");

/**
 * A global "saving failed" message.
 */
JIRA.Components.IssueEditor.Views.SaveError = JIRA.Marionette.ItemView.extend({
    events: {
        "simpleClick .issue": "onIssueLinkClick"
    },

    template: JIRA.Templates.IssueEditor.Fields.saveErrorMessage,

    /**
     * Extract error messages from the `IssueSaverService` response.
     *
     * @private
     * @returns {string[]} The error messages in `this.options.response`.
     */
    getErrorMessages: function () {
        var errorCollection,
            errors = [];

        errorCollection = this.options.response.errorCollection;
        errorCollection && (errors = errorCollection.errorMessages.concat(
            _.values(errorCollection.errors)));

        return errors;
    },

    /**
     * Close the view and trigger an "issueLinkClick" event.
     *
     * @param {jQuery.Event} e A jQuery click event.
     * @private
     */
    onIssueLinkClick: function (e) {
        var options;

        options = {
            id: this.options.issueId,
            key: this.options.issueKey
        };

        e.preventDefault();
        this.trigger("issueLinkClick", options);
        this.close();
    },

    onRender: function () {
        this.setElement(JIRA.Messages.showErrorMsg(this.$el.html(), {
            closeable: true,
            timeout: 0
        }));
    },

    serializeData: function () {
        return {
            errors: this.getErrorMessages(),
            issueKey: this.options.issueKey
        };
    }
});