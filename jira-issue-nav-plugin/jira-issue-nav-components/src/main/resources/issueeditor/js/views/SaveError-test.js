AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueeditor");

(function ($) {
    module("JIRA.Components.IssueEditor.Views.SaveError", {
        showView: function () {
            return new JIRA.Components.IssueEditor.Views.SaveError({
                issueId: "10000",
                issueKey: "ABC-123",
                response: {
                    errorCollection: {
                        errorMessages: ["General error message."],
                        errors: {field: "Field error message."}
                    }
                }
            }).render();
        },

        showErrorMessage: function (content) {
            return $("<div/>").html(content);
        }
    });

    test("Shows a global error message on render", function () {
        var showErrorMessageStub = this.stub(JIRA.Messages, "showErrorMsg", this.showErrorMessage),
            view = this.showView();

        ok(view.$el.html().indexOf("General error message.") > -1, "General error messages are rendered.");
        ok(view.$el.html().indexOf("Field error message.") > -1, "Field error messages are rendered");
        deepEqual(showErrorMessageStub.getCall(0).args[1], {
            closeable: true,
            timeout: 0
        }, "A global error message was shown with the correct arguments");
    });

    test("Triggers an 'issueLinkClick' event when the link is clicked", function () {
        var spy = sinon.spy(),
            view;

        this.stub(JIRA.Messages, "showErrorMsg", this.showErrorMessage);
        view = this.showView();
        view.on("issueLinkClick", spy);

        // The link is rendered through an i18n string which doesn't work in tests.
        $("<a/>").addClass("issue").appendTo(view.$el).click();

        ok(spy.calledWithExactly({
            id: "10000",
            key: "ABC-123"
        }), "An 'issueLinkClick' event was triggered");
    });
}(AJS.$));