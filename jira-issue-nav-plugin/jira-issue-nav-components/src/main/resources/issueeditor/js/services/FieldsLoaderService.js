AJS.namespace("JIRA.Components.IssueEditor.Services.FieldsLoader");

/**
 * @class JIRA.Components.IssueEditor.Services.FieldsLoader
 * @extends Marionette.Controller
 *
 * Service responsible for loading the fields of an issue
 */
JIRA.Components.IssueEditor.Services.FieldsLoader = JIRA.Marionette.Controller.extend({
    /**
     * Initialise this service
     *
     * @constructor
     * @param {object} options Options
     * @param {string} options.contextPath ContextPath used in this JIRA installation (e.g. "/jira")
     */
    initialize: function(options) {
        this.recurringPromise = jQuery.RecurringPromise().sub();
        this.recurringPromise
            .done(_.bind(function(args) {
                this.trigger("fieldsLoaded", args);
            },this))
            .fail(_.bind(function(args) {
                this.trigger("fieldsError", args);
            },this));

        this.contextPath = options.contextPath;
    },

    /**
     * Load the fields for an issue
     *
     * @param options
     */
    load: function(options) {
        var task = this._buildUpdateTask(options);
        this.recurringPromise.add(task);
    },

    /**
     * Cancel the any ongoing requests
     */
    cancel: function () {
        this.recurringPromise.reset();
    },

    /**
     * Gets the edit fields data from server, including edit html, if it is required etc.
     *
     * @param {number} issueId ID of the issue to load
     * @param {string} issueKey Key of the issue to load
     */
    _getFieldsData: function (issueId, issueKey) {
        var jqXhr = jQuery.ajax({
            url: this.contextPath + "/secure/AjaxIssueEditAction!default.jspa?decorator=none",
            headers: { 'X-SITEMESH-OFF': true },
            data: { issueId: issueId }
        });

        var deferred = jqXhr.pipe(
            function (data) {
                var resp = JIRA.Components.IssueViewer.Legacy.IssueFieldUtil.transformFieldHtml(data);
                return {
                    issueId: issueId,
                    issueKey: issueKey,
                    fields: resp.fields
                };
            },
            function (xhr) {
                var errorCollection = {};
                var fatalError;

                switch (xhr.status) {
                    case 400:
                        fatalError = false;
                        try {
                            var response = JSON.parse(xhr.responseText) || {};
                            errorCollection = response.errorCollection;
                        } catch (e) {}
                        break;

                    case 401:
                        fatalError = false;
                        errorCollection = null;
                        break;

                    default:
                        fatalError = true;
                        errorCollection = {};
                        break;
                }

                return {
                    issueId: issueId,
                    fields: null,
                    errorCollection: errorCollection,
                    isTimeout: "timeout" === xhr.statusText,
                    fatalError: fatalError
                };
            }
        );

        deferred.abort = function () {
            jqXhr.abort.apply(jqXhr, arguments);
        };

        return deferred;
    },

    /**
     * Builds the update task
     * @param options
     * @returns {jQuery.Promise}
     * @private
     */
    _buildUpdateTask: function(options) {
        var viewIssueData = options.viewIssueData;
        var issueEntity = options.issueEntity;
        var lastEditData = options.lastEditData;

        var deferred = jQuery.Deferred();

        viewIssueData.setPending(
            issueEntity.id,
            this._getFieldsData(issueEntity.id, issueEntity.key),
            function (result) { deferred.resolve(result); },
            function (result) {
                if (result.fatalError) deferred.reject(result);
                else deferred.resolve(result);
            }
        );

        return deferred.promise();

    }
});
