AJS.namespace("JIRA.Components.IssueEditor.Services.UnloadInterceptor");

// Why not just use jQuery I hear you say?? Well it doesn't work for IE!
// JRADEV-11612

JIRA.Components.IssueEditor.Services.UnloadInterceptor = JIRA.Marionette.Controller.extend({
    initialize: function() {
        this._afterEventHandlers = [];
        this.originalBeforeUnload = window.onbeforeunload;
        window.onbeforeunload = _.bind(this.onBeforeUnload, this);
    },

    addAfterEvent: function(handler) {
        this._afterEventHandlers.push(handler);
    },

    removeAfterEvent: function(handler) {
        this._afterEventHandlers = _.without(this._afterEventHandlers, handler);
    },

    onBeforeUnload: function() {
        var result,
            args = arguments;

        // Run the original handler (if present)
        if (_.isFunction(this.originalBeforeUnload)) {
            result = this.originalBeforeUnload.apply(window, args);
        }

        // If the original handler returned a truty value, don't process the afterEventHandlers
        if (!result) {
            result = _.reduce(this._afterEventHandlers, function(memo, value, index, list) {
                // If the previous handler returned anything, skip the rest of handlers
                if (memo) return memo;
                return value.apply(window, args);
            }, result);
        }

        return result;
    }
});

