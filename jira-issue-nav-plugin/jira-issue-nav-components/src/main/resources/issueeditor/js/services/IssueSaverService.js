AJS.namespace("JIRA.Components.IssueEditor.Services.IssueSaver");

/**
 * @class JIRA.Components.IssueEditor.Services.IssueSaver
 * @extends Marionette.Controller
 *
 * Service responsible for saving an issue
 *
 * This is just a wrapper for saveInProgressManager, we should refactor it and join these two services
 */
JIRA.Components.IssueEditor.Services.IssueSaver = JIRA.Marionette.Controller.extend({
    /**
     * Initialize the service
     *
     * @constructor
     * @param {object} options
     * @param {JIRA.Components.IssueEditor.Services.SaveInProgressManager} object.saveInProgressManager
     * @param {JIRA.Modules.IssueViewer.Models.Issue} object.model
     */
    initialize: function(options) {
        this.saveInProgressManager = options.saveInProgressManager;
        this.model = options.model;

        // TODO: flaky. Assumes that requests will always come back in the order in which they were started.
        // This is ok as are refactoring save & reload to be in the same request, which will make this much easier.
        this._saveStarted = [];

        this.saveInProgressManager.onSavingStarted(_.bind(function (issueId, savedFieldIds) {
            this._saveStarted.push((new Date()).getTime());
            this.trigger("saveStarted", issueId, savedFieldIds);
        },this));

        this.saveInProgressManager.onSaveSuccess(_.bind(function (issueId, issueKey, savedFieldIds, response) {
            if (response && response.fields) {
                JIRA.Components.IssueViewer.Legacy.IssueFieldUtil.transformFieldHtml(response);
            }
            this.trigger("save", {
                issueId: issueId,
                issueKey: issueKey,
                savedFieldIds: savedFieldIds,
                response: response,
                shouldSkipSaveIssueSuccessHandler: this._shouldSkipSaveIssueSuccessHandler,
                duration: (new Date()).getTime() - this._saveStarted.shift()
            });
        },this));

        this.saveInProgressManager.onSaveError(_.bind(function (issueId, issueKey, attemptedSavedIds, response) {
            if (response && response.fields) {
                JIRA.Components.IssueViewer.Legacy.IssueFieldUtil.transformFieldHtml(response);
            }

            this.trigger("error", {
                issueId: issueId,
                issueKey: issueKey,
                attemptedSavedIds: attemptedSavedIds,
                response: response,
                duration: (new Date()).getTime() - this._saveStarted.shift()
            });
        },this));
    },

    /**
     * Skip the next SaveIssueSuccess request
     * @param skipHandler {Boolean} True to skip the handler
     */
    setSkipSaveIssueSuccessHandler: function (skipHandler) {
        this._shouldSkipSaveIssueSuccessHandler = skipHandler;
    },

    /**
     * Saves an issue
     * @param issueId
     * @param issueKey
     * @param toSaveIds
     * @param params
     * @param ajaxProperties
     */
    save: function(issueId, issueKey, toSaveIds, params, ajaxProperties) {
        this.saveInProgressManager.saveIssue(issueId, issueKey, toSaveIds, params, ajaxProperties);
    }
});