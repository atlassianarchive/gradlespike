AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueeditor");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueeditor-test");

module("JIRA.Components.IssueEditor.Services.IssueSaver", {
    setup: function () {
        this.sandbox = sinon.sandbox.create();
        this.sandbox.useFakeServer();
        this.sandbox.useFakeTimers();

        this.model = new JIRA.Components.IssueViewer.Models.Issue();
        this.service = new JIRA.Components.IssueEditor.Services.IssueSaver({
            saveInProgressManager: new JIRA.Components.IssueEditor.Services.SaveInProgressManager(),
            model: this.model
        });
    },

    teardown: function() {
        this.sandbox.restore();
    }
});

test("It fires the event 'saveStarted' when saving an issue", function() {
    var spy = this.sandbox.spy();

    this.service.listenTo(this.service, "saveStarted", spy);
    this.service.save(1, "KEY-1", [1], {}, {});

    ok(spy.calledOnce, "The event saveStarted is fired");
    equal(spy.firstCall.args[0], 1, "It passes the issueId to the event handlers");
    deepEqual(spy.firstCall.args[1], [1], "It passes the list of fields to the event handlers");
});

test("It fires the event 'save' after successfully saving an issue", function() {
    var spy = this.sandbox.spy();
    var response = [
        { contentId: 1, editHTML: "<div/>", id: 1, label: "Field1", required: true },
        { contentId: 2, editHTML: "<div/>", id: 2, label: "Field2", required: false }
    ];

    this.service.listenTo(this.service, "save", spy);

    this.model.setId(1);
    this.service.save(1, "KEY-1", [1], {}, {});
    this.sandbox.server.requests[0].respond(200, {"Content-Type": "application/json"}, JSON.stringify(response));

    ok(spy.calledOnce, "The event 'save' is fired");
    equal(spy.firstCall.args[0].issueId, 1, "It passes the issueId to the event handlers");
    equal(spy.firstCall.args[0].issueKey, "KEY-1", "It passes the issueKey to the event handlers");
    deepEqual(spy.firstCall.args[0].savedFieldIds, [1], "It passes the saved fields ids to the event handlers");
    deepEqual(spy.firstCall.args[0].response, response, "It passes the response to the event handlers");
});

test("It fires the event 'error' when there is an error saving an issue", function() {
    this.sandbox.stub(JIRA.Components.IssueViewer.Legacy.IssueFieldUtil, "matchesFieldSelector").returns(true);
    var spy = this.sandbox.spy();
    this.service.listenTo(this.service, "error", spy);

    var response = {"fields":[]};
    this.model.setId(1);
    this.service.save(1, "KEY-1", [1], {}, {});
    this.sandbox.server.requests[0].respond(400, {"Content-Type": "application/json"}, JSON.stringify(response));

    ok(spy.calledOnce, "The event 'error' is fired");
    equal(spy.firstCall.args[0].issueId, 1, "It passes the issueId to the event handlers");
    equal(spy.firstCall.args[0].issueKey, "KEY-1", "It passes the issueKey to the event handlers");
    deepEqual(spy.firstCall.args[0].attemptedSavedIds, [1], "It passes the saved fields ids to the event handlers");
    deepEqual(spy.firstCall.args[0].response, response, "It passes the response to the event handlers");
});

test("It fires the event 'error' when there is an error in a modal field and there are just one field", function() {
    jQuery("#qunit-fixture").html("<div class='aui-blanket'></div><div id='modal-field-view' data-field-id='environment'></div>");
    var spy = this.sandbox.spy();
    this.service.listenTo(this.service, "error", spy);

    var response = {"fields":[]};
    this.model.setId(1);
    this.service.save(1, "HSP-1", ["environment"], {}, {});
    this.sandbox.server.requests[0].respond(400, {"Content-Type": "application/json"}, JSON.stringify(response));

    ok(spy.calledOnce, "The event 'error' is fired");
    equal(spy.firstCall.args[0].issueId, 1, "It passes the issueId to the event handlers");
    equal(spy.firstCall.args[0].issueKey, "HSP-1", "It passes the issueKey to the event handlers");
    deepEqual(spy.firstCall.args[0].attemptedSavedIds, ["environment"], "It passes the saved fields ids to the event handlers");
    deepEqual(spy.firstCall.args[0].response, response, "It passes the response to the event handlers");
});