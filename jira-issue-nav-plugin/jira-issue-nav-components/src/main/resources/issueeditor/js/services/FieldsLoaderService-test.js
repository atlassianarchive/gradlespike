AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueeditor");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueeditor-test");

module("JIRA.Components.IssueEditor.Services.FieldsLoader", {
    setup: function () {
        this.sandbox = sinon.sandbox.create();
        this.sandbox.useFakeServer();

        this.service = new JIRA.Components.IssueEditor.Services.FieldsLoader({
            contextPath: "/jira"
        });
        this.viewIssueData = new JIRA.Components.IssueViewer.Legacy.ViewIssueData();
    },

    teardown: function() {
        this.sandbox.restore();
    }
});

test("It loads the fields using AJAX", function() {
    this.service.load({
        viewIssueData: this.viewIssueData,
        issueEntity: {id: 1, key: "KEY-1"},
        lastEditData: {}
    });

    var requests = this.sandbox.server.requests;
    equal(requests.length, 1, "It creates an AJAX request to get the fields");
    ok(requests[0].url.match(/^\/jira/), "It includes the contextPath at the beginning of the URL");
    ok(requests[0].url.match("/jira/secure/AjaxIssueEditAction!default.jspa"), "It sends a request to AjaxIssueEditAction");
});

test("It triggers the event fieldsLoaded when the fields are loaded", function() {
    var spy = this.sandbox.spy();

    this.service.listenTo(this.service,"fieldsLoaded", spy);
    this.service.load({
        viewIssueData: this.viewIssueData,
        issueEntity: {id: 1, key: "KEY-1"}
    });

    this.sandbox.server.requests[0].respond(200,{},'{"fields":[]}');

    ok(spy.calledOnce, "The event fieldsLoaded has been fired");
    deepEqual(spy.firstCall.args[0], {issueId: 1, issueKey: "KEY-1", fields:[]}, "The event handler receives the response from the AJAX request");
});

test("It triggers the event fieldsError when there is a fatal error loading the fields", function() {
    var spy = this.sandbox.spy();

    this.service.listenTo(this.service,"fieldsError", spy);
    this.service.load({
        viewIssueData: this.viewIssueData,
        issueEntity: {id: 1, key: "KEY-1"}
    });
    this.sandbox.server.requests[0].respond(403,{},'{"errorCollection":["error"]}');

    ok(spy.calledOnce, "The event fieldsError has been fired");
    var expectedOptions = {
        issueId: 1,
        errorCollection:{},
        fields: null,
        fatalError: true,
        isTimeout: false
    };
    deepEqual(spy.firstCall.args[0], expectedOptions, "The event handler receives the response from the AJAX request");
});

test("It triggers the event fieldsLoaded event when there is a not-fatal error loading the fields", function() {
    var spy = this.sandbox.spy();

    this.service.listenTo(this.service,"fieldsLoaded", spy);
    this.service.load({
        viewIssueData: this.viewIssueData,
        issueEntity: {id: 1, key: "KEY-1"}
    });
    this.sandbox.server.requests[0].respond(400,{},'{"errorCollection":["error"]}');

    ok(spy.calledOnce, "The event fieldsLoaded has been fired");
    var expectedOptions = {
        issueId: 1,
        errorCollection: ["error"],
        fields: null,
        fatalError: false,
        isTimeout: false
    };
    deepEqual(spy.firstCall.args[0], expectedOptions, "The event handler receives the response from the AJAX request");
});

test("It cancels pending requests", function() {
    var spy = this.sandbox.spy();

    this.service.listenTo(this.service,"fieldsLoaded", spy);
    this.service.load({
        viewIssueData: this.viewIssueData,
        issueEntity: {id: 1, key: "KEY-1"}
    });
    this.service.cancel();
    this.sandbox.server.requests[0].respond(200,{},'{"fields":[]}');

    ok(!spy.calledOnce, "The event fieldsLoaded has not been fired");
});