AJS.namespace("JIRA.Components.IssueEditor.Services.SaveInProgressManager");

JIRA.Components.IssueEditor.Services.SaveInProgressManager = JIRA.Issues.Brace.Model.extend({

    namedAttributes: [
        "savesInProgress"
    ],

    namedEvents:["beforeSaving", "savingStarted", "saveSuccess", "saveError"],

    initialize: function () {
        this.setSavesInProgress([]);

    },

    saveIssue:function(issueId, issueKey, fieldsToSave, data, ajaxProperties) {
        this.triggerBeforeSaving();

        var instance = this,
            saveInProgress,
            allParams;

        allParams = _.extend(data, {
            issueId: issueId,
            atl_token: atl_token(),
            singleFieldEdit: true,
            fieldsToForcePresent: fieldsToSave
        });

        var ajaxOpts = _.extend({
            type: "POST",
            url: contextPath + "/secure/AjaxIssueAction.jspa?decorator=none",
            headers: { 'X-SITEMESH-OFF': true },
            error: function (xhr) {
                instance._handleSaveError(issueId, issueKey, fieldsToSave, xhr);
            },
            success: function (resp, statusText, xhr, smartAjaxResult) {
                var responseData = smartAjaxResult.data;
                // Was the response HTML?
                if (typeof responseData == "string") {
                    instance._handleHtmlResponse(issueId, issueKey, fieldsToSave, responseData);
                } else {
                    JIRA.Components.IssueViewer.Legacy.IssueFieldUtil.transformFieldHtml(responseData);
                    instance.triggerSaveSuccess(issueId, issueKey, fieldsToSave, responseData);
                }
            },
            complete: function () {
                instance.removeSaveInProgress(saveInProgress);
                JIRA.trigger(JIRA.Events.INLINE_EDIT_SAVE_COMPLETE);
            },
            data: allParams
        }, ajaxProperties);


        saveInProgress = JIRA.SmartAjax.makeRequest(ajaxOpts);
        this.addSaveInProgress(saveInProgress);
        this.triggerSavingStarted(issueId, fieldsToSave, data);

    },

    hasSavesInProgress: function () {
        return this.getSavesInProgress().length > 0;
    },

    removeSaveInProgress: function (saveInProgress) {
        this.setSavesInProgress(_.without(this.getSavesInProgress(), saveInProgress));
    },

    addSaveInProgress: function (saveInProgress) {
        var savesInProgress = this.getSavesInProgress();
        savesInProgress.push(saveInProgress);
        this.setSavesInProgress(savesInProgress);
    },

    _handleHtmlResponse: function(issueId, issueKey, fieldsToSave, responseData) {
        var instance = this;
        var responseBody = AJS.$(AJS.extractBodyFromResponse(responseData));
        var updatedXSRFToken = responseBody.find("#atl_token").val();

        // If we've received an XSRF token error, an updated token will be in the response.
        if (updatedXSRFToken) {
            AJS.$("#atlassian-token").attr("content", updatedXSRFToken);
        }

        var dialog = new JIRA.FormDialog({
            offsetTarget: "body",
            content: responseBody
        });

        this.triggerSaveError(issueId, issueKey, fieldsToSave);

        // If clicking the XSRF dialog's "Retry" button worked, continue.
        dialog._handleServerSuccess = function (xsrfResponseData) {
            dialog.hide();
            var data = instance._parseResponse(xsrfResponseData);
            if (data) {
                JIRA.Components.IssueViewer.Legacy.IssueFieldUtil.transformFieldHtml(responseData);
                instance.triggerSaveSuccess(issueId, issueKey, fieldsToSave, data);
            }
        };

        // If clicking the XSRF dialog's "Retry" button didn't work, trigger a save error
        dialog._handleServerError = function (xhr) {
            dialog.hide();
            var data = instance._parseResponse(xhr.responseText);
            if (data) {
                JIRA.Components.IssueViewer.Legacy.IssueFieldUtil.transformFieldHtml(data);
                instance.triggerSaveError(issueId, issueKey, fieldsToSave, data);
            }
        };

        dialog.show();
    },

    _handleSaveError: function (issueId, issueKey, fieldsToSave, xhr) {
        var data = this._parseResponse(xhr.responseText);
        if (data) {
            JIRA.Components.IssueViewer.Legacy.IssueFieldUtil.transformFieldHtml(data);
            this.triggerSaveError(issueId, issueKey, fieldsToSave, data);
        }
    },

    /**
     * Attempts to parse raw response to JSON. If parsing fails, shows a global error message and returns null
     * @param responseText raw http response data
     */
    _parseResponse: function(responseText) {
        try {
            return JSON.parse(responseText);
        } catch (e) {
            // parse JSON failed
            this._showFatalErrorMessage();
            return null;
        }
    },

    _showFatalErrorMessage: function() {
        // TODO: would be nice to extract this error from smartAjax and make it uniform in JIRA
        var msg = '<p>' + AJS.I18n.getText("common.forms.ajax.error.dialog.heading") + '</p>' +
            '<p>' + AJS.I18n.getText("common.forms.ajax.error.dialog") + '</p>';
        JIRA.Messages.showErrorMsg(msg, {
            closeable: true
        });
    }
});

// Events
JIRA.Events.INLINE_EDIT_SAVE_COMPLETE = "inlineEditSaveComplete";