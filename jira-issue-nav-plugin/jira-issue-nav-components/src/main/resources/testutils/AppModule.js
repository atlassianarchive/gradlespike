AJS.namespace("JIRA.Tests.Utils.AppModule");


JIRA.Tests.Utils.AppModule = {

    /**
     * Creates a new JIRA.Marionette.Application and starts it.
     *
     * @returns {JIRA.Marionette.Application}
     */
    startApplication: function(options) {
        var application = new JIRA.Marionette.Application();
        application.start(options);
        return application;
    },

    /**
     * Creates a appModule and installs it into an application.
     *
     * @param {Object} options
     * @param {Object} [options.componentsContainer=JIRA.Components] - Namespace where the components reside.
     * @param {Object} [options.sinon=window.sinon] - Sinon instance (or sandbox) to use for creating the stubs.
     * @param {Object} [options.application] - Application where this appModule should be installed. By default it creates a new application.
     * @param {string} options.moduleName - Name of the module to install.
     * @param {string} [options.appModule] - AppModule to install. By default it is componentsContainer[moduleName].AppModule.
     *
     * @returns {Object} Internal module created by the appModule.
     */
    createModule: function(options) {
        var componentsContainer = options.componentsContainer || JIRA.Components;
        var sandbox = options.sinon || sinon;
        var application = options.application || this.startApplication();

        var moduleName = options.moduleName;
        if (!moduleName) throw new Error("Missing options.moduleName");

        var appModule = options.appModule || componentsContainer[moduleName].AppModule;
        var spy = sandbox.spy(componentsContainer, moduleName);
        application.module(moduleName, new appModule().definition);

        //Can't use returnValues here: https://github.com/cjohansen/Sinon.JS/issues/351
        return spy.firstCall.thisValue;
    },

    /**
     * Creates a stub for each method in the list.
     *
     * @param {Object} options
     * @param {string[]} options.methods - List of methods to stub.
     * @param {Function} options.moduleClass - Main class where methods reside. The methods will be stub from the class' prototype.
     * @param {Object} [options.sinon=window.sinon] - Sinon instance (or sandbox) to use for creating the stubs.
     */
    stubMethods: function(options) {
        var methods = options.methods;
        if (!methods) throw new Error("Missing options.methods");

        var moduleClass = options.moduleClass;
        if (!moduleClass) throw new Error("Missing options.moduleClass");

        var sandbox = options.sinon || window.sinon;

        _.each(methods, function(method) {
            if (!(method in moduleClass.prototype)) {
                throw new Error("Method '"+method+"' not found in the module");
            }
            sandbox.stub(moduleClass.prototype, method);
        });
    },

    /**
     * Asserts that a list of commands are implemented by the application, and they call the method with the same name
     * in the internal object created by the AppModule.
     *
     * @param {Object} options
     * @param {string[]} options.commands - Commands to check.
     * @param {string} options.commandsNamespace - Namespace used for the commands (a command is in the form <namespace>:<command>).
     * @param {JIRA.Marionette.Application} options.application - Application where the commands have been installed.
     * @param {Object} options.module - Object that implements the handlers for the commands. These methods must be a spy/stub.
     */
    assertCommands: function(options) {
        var commands = options.commands;
        if (!commands) throw new Error("Missing options.commands");

        var commandsNamespace = options.commandsNamespace;
        if (!commandsNamespace) throw new Error("Missing options.commandsNamespace");

        var application = options.application;
        if (!application) throw new Error("Missing options.application");

        var module = options.module;
        if (!module) throw new Error("Missing options.module");

        _.each(commands, function(command) {
            application.execute(commandsNamespace+":"+command);
            ok(module[command].calledOnce, "The command '"+command+"' calls the method with the same name");
        });
    },

    /**
     * Asserts that a list of requests are implemented by the application, and they call the method with the same name
     * in the internal object created by the AppModule.
     *
     * @param {Object} options
     * @param {string[]} options.requests Requests to check
     * @param {string} options.requestsNamespace Namespace used for the requests (a request is in the form <namespace>:<request>)
     * @param {JIRA.Marionette.Application} options.application Application where the request have been installed
     * @param {Object} options.module Object that implements the handlers for the request. These methods must be a spy/stub
     */
    assertRequests: function(options) {
        var request = options.requests;
        if (!request) throw new Error("Missing options.request");

        var requestNamespace = options.requestsNamespace;
        if (!requestNamespace) throw new Error("Missing options.requestNamespace");

        var application = options.application;
        if (!application) throw new Error("Missing options.application");

        var module = options.module;
        if (!module) throw new Error("Missing options.module");

        _.each(request, function(request) {
            application.request(requestNamespace+":"+request);
            ok(module[request].calledOnce, "The request '"+request+"' calls the method with the same name");
        });
    },

    /**
     * Asserts that a list of events fire an event in the Application when the event with the same name is fired
     * in the internal object created by the AppModule.
     *
     * @param {Object} options
     * @param {string[]} options.events - Events to check.
     * @param {Object} [options.sinon=window.sinon] - Sinon instance (or sandbox) to use for creating the stubs.
     * @param {JIRA.Marionette.Application} options.application - Application where the commands have been installed.
     * @param {string} options.eventsNamespace - Namespace used for the events (a event is in the form <namespace>:<event>).
     * @param {Object} options.module - Object that will fire the events.
     */
    assertEvents: function(options) {
        var events = options.events;
        if (!events) throw new Error("Missing options.events");

        var sandbox = options.sinon || window.sinon;

        var application = options.application;
        if (!application) throw new Error("Missing options.application");

        var eventsNamespace = options.eventsNamespace;
        if (!eventsNamespace) throw new Error("Missing options.eventsNamespace");

        var module = options.module;
        if (!module) throw new Error("Missing options.module");

        _.each(events, function(event) {
            var spy = sandbox.spy();
            application.on(eventsNamespace+":"+event, spy);
            module.trigger(event);
            ok(spy.calledOnce, "The event '"+event+"' was triggered in the application");
        });
    }
};