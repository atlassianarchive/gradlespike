JIRA.Components.Query = function () {


    var CLAUSES = {
        project: AJS.I18n.getText("issues.components.query.searcher.project"),
        issuetype: AJS.I18n.getText("issues.components.query.searcher.issuetype"),
        status: AJS.I18n.getText("issues.components.query.searcher.status"),
        assignee: AJS.I18n.getText("issues.components.query.searcher.assignee"),
        reporter: AJS.I18n.getText("issues.components.query.searcher.reporter"),
        labels: AJS.I18n.getText("issues.components.query.searcher.labels"),
        resolution: AJS.I18n.getText("issues.components.query.searcher.resolution"),
        created: AJS.I18n.getText("issues.components.query.searcher.created.date"),
        updated: AJS.I18n.getText("issues.components.query.searcher.updated.date"),
        resolutiondate: AJS.I18n.getText("issues.components.query.searcher.resolution.date"),
        description: AJS.I18n.getText("issues.components.query.searcher.description"),
        duedate: AJS.I18n.getText("issues.components.query.searcher.due.date"),
        comment: AJS.I18n.getText("issues.components.query.searcher.comment"),
        fixfor: AJS.I18n.getText("issues.components.query.searcher.fix.version"),
        version: AJS.I18n.getText("issues.components.query.searcher.affects.version"),
        component: AJS.I18n.getText("issues.components.query.searcher.component")
    };

    return {

        DEFAULT_CLAUSES: ["project", "issuetype", "status", "assignee"],

        create: function (options) {

            options = _.defaults(options, {
                primaryClauses : this.DEFAULT_CLAUSES,
                without: [],
                style: "generic",
                /* This has to be true :( - If issue-nav-components is anything below 6.2, the layoutSwitcher option
                 * didn't exist when it was first consumed in 6.1. */
                layoutSwitcher: true,
                autocompleteEnabled: true,
                advancedAutoUpdate: false,
                basicOrderBy: false,
                basicAutoUpdate: true,
                preferredSearchMode: "basic"
            });


            options.primaryClauses = _.reject(options.primaryClauses, function (clause) {
                return _.contains(options.without, clause.id);
            });

            _.each(options.primaryClauses, function (clause, idx) {
                if (typeof clause === "string") {
                    if (CLAUSES[clause]) {
                        options.primaryClauses[idx] = {id: clause, name: CLAUSES[clause]};
                    } else {
                        console.error("JIRA.Components.Query: You have specified clause [" + clause + "]. " +
                            "But we do not have the i18n string for it, probably a custom field. Instead use {id:" + clause + ", name: '[NAME_HERE]'}")
                    }
                }
            });

            var queryModule = new JIRA.Issues.QueryModule({
                queryStateModel: new JIRA.Issues.QueryStateModel({
                    jql: options.jql,
                    without: options.without,
                    style: options.style,
                    layoutSwitcher: options.layoutSwitcher,
                    autocompleteEnabled: options.autocompleteEnabled,
                    advancedAutoUpdate: options.advancedAutoUpdate,
                    basicAutoUpdate: options.basicAutoUpdate,
                    preferredSearchMode: options.preferredSearchMode,
                    basicOrderBy: options.basicOrderBy

                }),
                primaryClauses: options.primaryClauses,
                searchers: options.searchers
            });

            jQuery(options.el).addClass("query-component " + options.style + "-styled");

            queryModule.createAndRenderView(options.el);

            if (options.jql || options.jql === "") {
                queryModule.resetToQuery(options.jql).always(function () {
                    jQuery(options.el).addClass("ready");
                    // Consumers of this component want to know when the jql (given at construction) is represented in the ui.
                    queryModule.triggerInitialized(options.jql);
                });
            }
            return queryModule;
        }
    };
}();