/**
 * A singleton that reuses the same InlineLayer for many different criteria selectors.
 */
JIRA.Issues.SearcherDialog = function () {

    return {
        initialize: function(options) {
            if (!this.instance) {
                this.instance = initSearcherDialog(options);
            }
        }
    };

    function initSearcherDialog(options) {
        AJS.HIDE_REASON.switchLozenge = "switchLozenge";

        var reasonsToFocusCriteria = [
            AJS.HIDE_REASON.escPressed,
            AJS.HIDE_REASON.toggle,
            AJS.HIDE_REASON.cancelClicked,
            AJS.HIDE_REASON.submit,
            AJS.HIDE_REASON.tabbedOut
        ];

        function findLozenge(searcher) {
            return searcher ? jQuery(".criteria-selector[data-id='" + searcher.getId() + "']") : jQuery();
        }

        var promise;
        var _currentSearcher; // JIRA.Issues.SearcherModel
        var _currentView;

        var _dialog = new AJS.InlineLayer({
            width: "auto",
            alignment: AJS.LEFT,
            offsetTarget: function () {
                // Don't cache the lozenge under the current implementation since it can get reblatted
                return findLozenge(_currentSearcher);
            },
            // Uses AJS.DeferredContentRetriever.
            content: function () {
                _currentView = new JIRA.Issues.SearcherEditDialogView({
                    model: _currentSearcher,
                    queryStateModel: options.queryStateModel
                });
                _currentView.onHideRequested(function(reason) {
                    _dialog.hide(reason);
                });
                return _currentView.renderDeferred();
            }
        });

        _dialog.bind(AJS.InlineLayer.EVENTS.show, function(e, $layer) {
            var input = $layer.find(":input:not(submit):visible:first");
            input.focus();

            /**
             * This is really bad.
             *
             * Here we're treating the symptom, not the cause.
             * Essentially when switching directly from one searcher dialog to another
             * the input is not focused in IE8. Focusing twice 'fixes' it.
             * i.e. Creates the right final behaviour.
             *
             * I feel that this is another ugly face of this issue: https://jdog.atlassian.net/browse/JRADEV-13374
             *
             * We need to sanitise the use of setTimeout/_.defer in core libraries. The more they are used the more
             * they propagate into client code and their tests. See the _show function below for such an example. If
             * the _.defer is removed then in IE the dialog will appear and immediately disappear if opened when another
             * is already open.
             *
             * TODO Replace this after 5.2 release with a proper more wholistic solution.
             */
            if ((AJS.$.browser.msie && parseInt(AJS.$.browser.version, 10) < 9)) {
                input.focus();
            }

            // List.js also resets the scrollTop but because the dialog is still hidden at that point, the browser won't actually do any scrolling.
            // @see JRADEV-15097
            $layer.find(".aui-list-scroll").scrollTop(0);
        });

        _dialog.bind(AJS.InlineLayer.EVENTS.beforeHide, function (e, layer, reason, id, originalTarget) {
            if (reason === AJS.HIDE_REASON.clickOutside && originalTarget && jQuery(originalTarget).closest(".calendar").length) {
                e.preventDefault();
            }
        });

        _dialog.bind(AJS.InlineLayer.EVENTS.hide, function (e, layer, reason) {
            var _searcher = _currentSearcher;

            if (_.contains(reasonsToFocusCriteria, reason)) {
                findLozenge(_searcher).focus();
            }

            function doSearch() {
                // A searcher has been submitted. Create a clause and add it to the clause collection
                promise = _currentSearcher.createOrUpdateClauseWithQueryString(reason === AJS.HIDE_REASON.submit);
                promise.done(function() {
                    // Check for an "error" class in the searcher's editHtml.
                    // If so, leave the dialog open and rerender to the the updated editHtml.
                    // Otherwise close the dialog.
                    if (_searcher.hasErrorInEditHtml()) {
                        // Prevent displaying the error dialog if the mode has switched to advanced as a
                        // result of the click outside (i.e. on the Switch to Advanced link)
                        if (options.queryStateModel.getSearchMode() === "basic") {
                            JIRA.Issues.SearcherDialog.instance.show(_searcher);
                        }
                    }

                    if (AJS.InlineLayer.current) {
                        // page layout might have changed as a result of updating a lozenge content (need to make sure dialog is in correct position)
                        AJS.InlineLayer.current.setPosition();
                    }
                });

                promise.always(function() {
                    promise = null;
                });

                _searcher.clearEditHtml();
            }

            if (reason === AJS.HIDE_REASON.submit) {
                // If this is a submit, always do de search
                _currentView.applyFilter();
                doSearch();
            } else if (reason !== AJS.HIDE_REASON.cancelClicked && reason !== AJS.HIDE_REASON.escPressed &&
                reason !== AJS.HIDE_REASON.tabbedOut) {
                // If the dialog has not been cancelled, do the search only if the selector has not changed.
                if (_currentView.applyFilter()) {
                    doSearch();
                }
            } else {
                JIRA.trace("jira.search.searchers.hiddenWithoutUpdate");
            }

            _currentView.$el.remove();
            _currentView = null;
            _currentSearcher = null;
        });

        /**
         * JRADEV-15697 - Ensure all the dialog is closed when we switch search modes.
         */
        options.queryStateModel.on("change:searchMode", function() {
            _dialog.hide();
        });

        return  {
            /**
             * @return {SearcherModel} the searcher for which the dialog was last shown.
             */
            getCurrentSearcher: function () {
                return _currentSearcher;
            },

            /**
             * Hide and show dialog
             */
            toggle: function (searcher) {
                if (_currentSearcher != null) {
                    if (_currentSearcher !== searcher) {
                        _dialog.hide(AJS.HIDE_REASON.switchLozenge);
                        this.show(searcher);
                    } else {
                        _dialog.hide(AJS.HIDE_REASON.toggle);
                    }
                } else {
                    this.show(searcher);
                }
            },

            _show: function (searcher) {
                _.defer(function() {
                    _currentSearcher = searcher;
                    _dialog.show();
                    _dialog.setPosition();
                });
            },

            /**
             * shows dialog with correct searcher
             * @param {JIRA.Issues.SearcherModel} searcher
             */
            show: function (searcher) {
                var waitingToShow;
                if (JIRA.Issues.SearcherDialog.waitingToShow) {
                    waitingToShow = true;
                }

                var instance = this;
                JIRA.Issues.SearcherDialog.waitingToShow = function () {
                    instance._show(searcher);
                    JIRA.Issues.SearcherDialog.waitingToShow = null;
                };

                if (!waitingToShow) {
                    // Defer showing until all searchers are ready.
                    searcher.searchersReady().done(function () {
                        if (promise) {
                            promise.done(function () {
                                JIRA.Issues.SearcherDialog.waitingToShow();
                            });
                        } else {
                            JIRA.Issues.SearcherDialog.waitingToShow();
                        }
                    });
                }
            },

            /**
             * Bind a handler to be called when the dialog is shown.
             *
             * @param handler The function to call when the dialog is shown.
             */
            onShow: function (handler) {
                _dialog.bind(AJS.InlineLayer.EVENTS.show, handler);
            },

            /**
             * Hide the current dialog. Drrrrr!
             */
            hide: function () {
                _dialog.hide();
            },

            /**
             * Bind a handler to be called when the dialog is hidden.
             *
             * @param handler The function to call when the dialog is hidden.
             */
            onHide: function (handler) {
                _dialog.bind(AJS.InlineLayer.EVENTS.hide, handler);
            }
        }
    }
}();