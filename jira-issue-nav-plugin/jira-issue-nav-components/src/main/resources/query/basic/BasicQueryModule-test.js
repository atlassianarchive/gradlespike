AJS.test.require("com.atlassian.jira.jira-issue-nav-components:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:query");

module("JIRA.Issues.BasicQueryModule", {

    setup: function() {
        this.queryStateModel = JIRA.Issues.Components.TestUtils.mockQueryStateModel();
        JIRA.Issues.SearcherDialog.initialize({
            queryStateModel: this.queryStateModel
        });
        this.basicQueryModule = new JIRA.Issues.BasicQueryModule({
            queryStateModel: this.queryStateModel
        });
        this.searchRequestedSpy = sinon.spy();
        this.basicQueryModule.onSearchRequested(this.searchRequestedSpy);
    },

    teardown: function() {
        JIRA.Issues.SearcherDialog.instance = null
    }
});
// Tests integration of IssueNavRouter, queryStateModel, FilterModule and IssueTableModule:
// 1. Changing URL changes JQL and current filter
// 2. Searching with JQL or filter changes URL

test("Setting basic search criteria triggers search requested", function() {
    this.basicQueryModule.searcherCollection.updateTextQuery("sometext");
    this.basicQueryModule.view.search();
    equal(this.searchRequestedSpy.callCount, 1);
    equal(this.searchRequestedSpy.firstCall.args[0], "text ~ \"sometext\"");
});

test("Setting multiple basic search criteria triggers search requested", function() {
    this.basicQueryModule.searcherCollection.updateTextQuery("sometext");
    this.basicQueryModule.searcherCollection.add({
        id: "project",
        jql: "project = runway",
        editHtml: "<input type='text' name='project' value='runway' />"
    });
    this.basicQueryModule.view.search();
    equal(this.searchRequestedSpy.callCount, 1);
    equal(this.searchRequestedSpy.firstCall.args[0], 'text ~ "sometext" AND project = runway');
});

test("attachOrderByClause(jql without ORDER BY), when current jql has ORDER BY", function() {
    this.queryStateModel.setJql("ORDER BY foo");
    var jql = this.basicQueryModule._attachOrderByClause("project = HSP");
    equal(jql, "project = HSP ORDER BY foo", "Order by clause was appended");
});

test("attachOrderByClause(jql without ORDER BY), when current jql has OrDeR By", function() {
    this.queryStateModel.setJql("OrDeR By a");
    var jql = this.basicQueryModule._attachOrderByClause("project = HSP");
    equal(jql, "project = HSP OrDeR By a", "Order by clause was appended");
});

test("attachOrderByClause(empty jql), when current jql has ORDER BY", function() {
    this.queryStateModel.setJql('ORDER BY "project"');
    var jql = this.basicQueryModule._attachOrderByClause("");
    equal(jql, 'ORDER BY "project"', "Order by clause was appended");
});

test("attachOrderByClause(jql with ORDER BY), when current jql has different ORDER BY", function() {
    this.queryStateModel.setJql('ORDER BY a, b, c DESC');
    var jql = this.basicQueryModule._attachOrderByClause('issuetype=Open ORDER BY a, x, y ASC, z DESC');
    equal(jql, 'issuetype=Open ORDER BY a, x, y ASC, z DESC', "Order by clause remained the same");
});
