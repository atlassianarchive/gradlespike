AJS.test.require("com.atlassian.jira.jira-issue-nav-components:query");

module("JIRA.Issues.TextQueryBuilder");

test("Empty", function() {
    equal(JIRA.Issues.TextQueryBuilder.buildJql(), "");
});

test("Single term", function() {
    equal(JIRA.Issues.TextQueryBuilder.buildJql("hi"), "text ~ \"hi\"");
});

test("Multiple terms", function() {
    equal(JIRA.Issues.TextQueryBuilder.buildJql("hi everybody"), "text ~ \"hi everybody\"");
});

test("Reserved word", function() {
    equal(JIRA.Issues.TextQueryBuilder.buildJql("and"), "text ~ \"and\"");
});

test("Whitespace in query", function() {
    equal(JIRA.Issues.TextQueryBuilder.buildJql(" hi"), "text ~ \"hi\"");
    equal(JIRA.Issues.TextQueryBuilder.buildJql("hi "), "text ~ \"hi\"");
    equal(JIRA.Issues.TextQueryBuilder.buildJql("hi   everybody"), "text ~ \"hi everybody\"");
    equal(JIRA.Issues.TextQueryBuilder.buildJql("  hi   dr   nic  "), "text ~ \"hi dr nic\"");
});

test("backslash escaping", function() {
    equal(JIRA.Issues.TextQueryBuilder.buildJql("something with a backslash\\"), "text ~ \"something with a backslash\\\\\"");

});
