AJS.test.require("com.atlassian.jira.jira-issue-nav-components:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:query");

module("JIRA.Issues.SearcherModel", {
    setup: function() {
        this.searcherCollection = JIRA.Issues.Components.TestUtils.createSearcherCollection();
    }
});

test("clearSearchState doesn't remove invalid searchers", function() {
    var searcher = new JIRA.Issues.SearcherModel({
        id: "project",
        isSelected: true,
        name: "Project",
        validSearcher: true
    });

    this.searcherCollection.add(searcher);
    searcher.clearSearchState();
    equal(this.searcherCollection.length, 1, "Valid searchers aren't removed");

    // Searchers whose validity is undefined shouldn't be removed.
    searcher.set({isSelected: true, validSearcher: undefined});
    searcher.clearSearchState();
    equal(this.searcherCollection.length, 1, "Searchers with undefined " +
            "validity aren't removed");

    // Invalid searchers shouldn't be removed.
    searcher.set({isSelected: true, validSearcher: false});
    searcher.clearSearchState();
    equal(this.searcherCollection.length, 1, "Invalid searchers aren't removed");
});

test("text searcher's viewHtml and editHtml is html-encoded, but its queryString is not", function() {
    var searcher = new JIRA.Issues.SearcherModel({
        id: "text",
        isSelected: true,
        name: "Text",
        validSearcher: true
    });

    this.searcherCollection.add(searcher);
    this.searcherCollection.updateTextQuery('<a>');

    equal(searcher.getViewHtml(), '&lt;a&gt;', "viewHtml is html-encoded");
    equal(searcher.getEditHtml(), '&lt;a&gt;', "editHtml is html-encoded");
    equal(searcher.getQueryString(), 'text=%3Ca%3E', "queryString is url-encoded, NOT html-encoded");
});

test("selecting the searcher sets the lastViewed flag", function() {
    var searcher = new JIRA.Issues.SearcherModel();
    searcher.collection = {
        getNextPosition: AJS.$.noop
    };
    searcher.set = sinon.spy();
    searcher.select();
    ok(searcher.set.called, "set() is called when select() is called");
    ok(searcher.set.getCall(0).args[0].lastViewed, "lastViewed flag is set");
});
