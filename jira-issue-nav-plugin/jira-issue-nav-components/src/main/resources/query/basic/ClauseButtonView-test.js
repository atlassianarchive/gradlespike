AJS.test.require("com.atlassian.jira.jira-issue-nav-components:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:query");

module('JIRA.Issues.ClauseButtonView', {
    setup: function() {
        this.$el = jQuery("<div></div>").appendTo("#qunit-fixture");
    },
    teardown: function() {
        this.$el.remove();
    }
});

test("Add filters button doesn't exist when there aren't any more secondary criteria to add", function() {

    var searcherGroupCollection = JIRA.Issues.Components.TestUtils.createSearcherCollection();

    var view = new JIRA.Issues.ClauseButtonView({
        searcherCollection: searcherGroupCollection,
        queryStateModel: new JIRA.Issues.QueryStateModel(),
        el: this.$el
    });
    view.render();
    equal(this.$el.find(".add-criteria:visible").length, 0, "Add criteria button shouldn't exist.");
});

test("Add filters button exists when there are more secondary criteria to add", function() {

    var searcherGroupCollection = JIRA.Issues.Components.TestUtils.createSearcherCollection([
        {
            id: "selectable",
            groupId: "groupId",
            groupName: "groupName",
            name: "searcherName"
        }
    ]);

    var view = new JIRA.Issues.ClauseButtonView({
        searcherCollection: searcherGroupCollection,
        queryStateModel: new JIRA.Issues.QueryStateModel(),
        el: this.$el
    });
    view.render();
    equal(this.$el.find(".add-criteria:visible").length, 1, "Add criteria button should exist.");

});

test("'More Criteria' button is disabled while SearcherCollection is noninteractive", function() {
    var searcherCollection = JIRA.Issues.Components.TestUtils.createSearcherCollection();
    var view = new JIRA.Issues.ClauseButtonView({
        searcherCollection: searcherCollection,
        queryStateModel: new JIRA.Issues.QueryStateModel(),
        el: this.$el
    });
    view.render();
    view.dialog.toggle = sinon.spy();
    searcherCollection.setInteractive(false);
    view.$(".add-criteria").click();
    equal(view.dialog.toggle.callCount, 0, "'More Criteria' dropdown cannot be opened when noninteractive");
    searcherCollection.setInteractive(true);
    view.$(".add-criteria").click();
    equal(view.dialog.toggle.callCount, 1, "'More Criteria' dropdown can be opened when interactive");
});
