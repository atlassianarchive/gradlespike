AJS.test.require("com.atlassian.jira.jira-issue-nav-components:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:query");

module('JIRA.Issues.BasicQueryView', {
    setup: function() {
        var queryStateModel = new JIRA.Issues.QueryStateModel();
        JIRA.Issues.SearcherDialog.initialize({
            queryStateModel: queryStateModel
        });

        this.$el = jQuery("<div>").appendTo("#qunit-fixture");
        this.searcherCollection = JIRA.Issues.Components.TestUtils.createSearcherCollection();
        this.view = new JIRA.Issues.BasicQueryView({
            queryStateModel: queryStateModel,
            searcherCollection: this.searcherCollection,
            el: this.$el
        });

        this.searcherCollection._searcherCache = {};

        // Adds a searcher and selects it
        this.addSearcher = _.bind(function(id, viewHtml, editHtml) {
            var rawSearcher = {
                id: id,
                name: 'Sample',
                groupId: 'group',
                groupName: 'A Group',
                viewHtml: viewHtml,
                jql: 'sample=yum',
                validSearcher: true,
                editHtml: editHtml,
                isSelected: true
            };
            this.searcherCollection._searcherCache[id] = rawSearcher;

            var searcher = new JIRA.Issues.SearcherModel(rawSearcher);

            // Don't clear the edit html as the searcher collection isn't going back to the server to get it for us in test.
            searcher.clearEditHtml = jQuery.noop;

            // Don't AJAX the edit HTML.
            searcher.retrieveEditHtml = function() {
                return jQuery.Deferred().resolve(editHtml).promise();
            };

            this.searcherCollection.add(searcher);
        }, this);

        this.triggerBackspace = function($target) {
            var e = jQuery.Event('keydown');
            e.which = jQuery.ui.keyCode.BACKSPACE;
            $target.trigger(e);
        };

        this.triggerDelete = function($target) {
            var e = jQuery.Event('keydown');
            e.which = jQuery.ui.keyCode.DELETE;
            $target.trigger(e);
        };

        // set sinon timers crap because remove-filter handler uses _.defer
        this.clock = sinon.useFakeTimers();

        this.queryEl = function() {
            return this.$el.find("input[type='text']");
        };

        this.server = sinon.fakeServer.create();
    },
    teardown: function() {
        this.server.restore();
        this.clock.restore();
        jQuery(".ajs-layer").remove();
    }
});

test("Template for simple query view exists", function() {
    ok(JIRA.Templates.IssueNavQueryBasic.basicQueryView, "Template does not exist");
});

test("View renders an input field", function() {
    this.view.render();

    equal(this.queryEl().length, 1, "View renders something");
});

test("Empty text builds jql correctly", function() {
    this.view.render();
    this.view.search();
    equal(this.searcherCollection.getTextQuery(), "");
});

test("Setting text builds jql correctly", function() {
    this.view.render();

    this.queryEl().val("jira 4ever");
    this.view.search();

    equal(this.searcherCollection.createJql(), "text ~ \"jira 4ever\"");
});

test("Resetting text to empty builds jql correctly", function() {
    this.view.render();

    this.queryEl().val("jira 4ever");
    this.view.search();
    this.queryEl().val("");
    this.view.search();

    equal(this.searcherCollection.getTextQuery(), "");
    equal(this.searcherCollection.createJql(), "");
});

test("Adding to query collection property updates input", function() {
    this.view.render();
    this.searcherCollection.updateTextQuery("this is a freakin query");

    equal(this.queryEl().val(), "this is a freakin query");
});

test("Adding to query collection property updates input", function() {
    this.view.render();
    this.searcherCollection.updateTextQuery("this is a freakin query");
    this.searcherCollection.updateTextQuery("this is another freakin query");

    equal(this.queryEl().val(), "this is another freakin query");
});

test("Triggering a requestUpdateFromView on the searcherCollection reads the text input", function() {
    this.view.render();
    ok(!this.searcherCollection.getTextQuery());

    this.queryEl().val("This is a test search");
    this.searcherCollection.triggerRequestUpdateFromView();
    equal("This is a test search", this.searcherCollection.getTextQuery());
});

test("Clearing the filters clears the input", function() {
    this.view.render();
    this.queryEl().val("This is a test search");
    this.searcherCollection.triggerRequestUpdateFromView();
    equal(this.queryEl().val(), this.searcherCollection.getTextQuery());

    this.searcherCollection.clearSearchState();

    equal(this.queryEl().val(), "");
});

test("Removing from query collection property updates input", function() {
    this.view.render();
    this.searcherCollection.remove(this.searcherCollection.QUERY_ID);

    equal(this.queryEl().val(), "");
});

test("Updating query property before rendering succeeds", function() {
    this.searcherCollection.updateTextQuery("this is some freakin jql");
    this.view.render();

    equal(this.queryEl().val(), "this is some freakin jql");
});

test("Adding extended searcher renders", function() {
    this.addSearcher('sample', '<span class="sampleViewHtml">Sample: yum</span>', '<input type="text" value="yum" />');
    this.view.render();

    equal(this.$el.find(".sampleViewHtml").html(), "Sample: yum");
});

test("Adding extended searcher after render causes rerender", function() {
    this.view.render();
    this.addSearcher('sample', '<span class="sampleViewHtml">Sample: yum</span>', '<input type="text" value="yum" />');
    this.searcherCollection.triggerCollectionChanged();

    equal(this.$el.find(".sampleViewHtml").html(), "Sample: yum");
});

test("Removing extended searcher", function() {
    this.addSearcher('sample', '<span class="sampleViewHtml">Sample: yum</span>', '<input type="text" value="yum" />');
    this.view.render();

    equal(1, this.$el.find(".search-criteria-extended li[data-id='sample']").length);
    this.$el.find("[data-id='sample'] .remove-filter").click();
    this.clock.tick(1000);

    var model = this.searcherCollection.get("sample");
    ok(!model.getViewHtml());
    ok(!model.getEditHtml());
    ok(!model.getJql());

    equal(0, this.$el.find(".search-criteria-extended li").length);
});

test("Removing extended searcher when multiple extended searchers exist", function() {
    this.addSearcher('sample1', '<span class="sampleViewHtml">Sample1: yum</span>', '<input type="text" value="yum" />');
    this.addSearcher('sample2', '<span class="sampleViewHtml">Sample2: yum</span>', '<input type="text" value="yum" />');
    this.view.render();

    equal(2, this.$el.find(".search-criteria-extended li").length);
    this.$el.find("[data-id='sample1'] .remove-filter").click();
    this.clock.tick(1000);

    equal(1, this.$el.find(".search-criteria-extended li").length);
});

asyncTest("Dialog sends values on submit", 3, function() {
    this.clock.restore();
    this.addSearcher('sample', '<div class="searcherValue">Sample1: yum</div>', '<input type="text" class="sample-editHtml" name="sample" value="yum" />');
    this.view.render();
    this.$el.appendTo("#qunit-fixture");
    this.$el.find("[data-id='sample'] .criteria-selector").click();
    var searcherCollection = this.searcherCollection;

    setTimeout(function() {
        equal(jQuery(".ajs-layer .sample-criteria").length, 1);

        jQuery(".ajs-layer .sample-criteria .sample-editHtml").val("yummier");

        // The SearcherModel shouldn't be changed until "Update" is clicked.
        equal(searcherCollection.get("sample").getQueryString(), "sample=yum");


        jQuery(".ajs-layer .sample-criteria input[type=submit]").click();
        equal(searcherCollection.get("sample").getQueryString(), "sample=yummier");
        start();
    }, 0);
});

test("Changes are discarded on close", function() {
    this.addSearcher('sample', '<div class="searcherValue">Sample1: yum</div>', '<input type="text" class="sample-editHtml" name="sample" value="yum" />');
    this.view.render();
    this.$el.find("[data-id='sample'] .searcherValue").click();
    jQuery(".ajs-layer .sample-criteria .sample-editHtml").val("yummier");
    jQuery(".ajs-layer .sample-criteria .aui-button.aui-button-link").click();

    equal(this.searcherCollection.get("sample").getQueryString(), "sample=yum");
});

test("Text field changes are discarded on resetting collection", function() {

    this.view.render();
    this.queryEl().val("u better believe that's a paddlin'");
    this.searcherCollection.clear();

    ok(!this.queryEl().val(), "text query view must be emptied");
    ok(!this.searcherCollection.getTextQuery(), "text query model must be cleared");
});

test("Text field changes are not discarded on createOrUpdateClauseWithQueryString", function() {
    this.view.render();
    this.queryEl().val("u better believe that's a paddlin'");
    this.searcherCollection.createOrUpdateClauseWithQueryString();

    equal(this.queryEl().val(), "u better believe that's a paddlin'");
    equal(this.searcherCollection.getTextQuery(), "u better believe that&#39;s a paddlin&#39;");
});

test("Text field value is correctly set when no text element in searcher collection", function() {
    this.view.render();

    ok(!this.queryEl().val());
    ok(!this.searcherCollection.getTextQuery());
});

test("Text field value is correctly set when empty text in searcher collection", function() {
    this.addSearcher('text', '', '');
    this.view.render();

    ok(!this.queryEl().val());
    ok(!this.searcherCollection.getTextQuery());
});
test("Text field value is correctly set when undefined text in searcher collection", function() {
    this.addSearcher('text', undefined, undefined);
    this.view.render();

    ok(!this.queryEl().val());
    ok(!this.searcherCollection.getTextQuery());
});

test("Text field value is correctly set from searcher collection", function() {
    this.addSearcher('text', '&lt;script&gt;alert(&#39;u call that a paddle? this is a paddle&#39;);&lt;/script&gt;', '&lt;script&gt;alert(&#39;u call that a paddle? this is a paddle&#39;);&lt;/script&gt;');
    this.view.render();

    equal(this.queryEl().val(), "<script>alert('u call that a paddle? this is a paddle');</script>");
    equal(this.searcherCollection.getTextQuery(), "&lt;script&gt;alert(&#39;u call that a paddle? this is a paddle&#39;);&lt;/script&gt;");

    // Attempt resubmitting
    this.view.search();

    equal(this.queryEl().val(), "<script>alert('u call that a paddle? this is a paddle');</script>");
    equal(this.searcherCollection.getTextQuery(), "&lt;script&gt;alert(&#39;u call that a paddle? this is a paddle&#39;);&lt;/script&gt;");
});

test("Text field value is correctly encoded on form search", function() {
    this.view.render();
    this.queryEl().val("<script>alert('u call that a paddle? this is a paddle');</script>");
    this.view.search();

    equal(this.queryEl().val(), "<script>alert('u call that a paddle? this is a paddle');</script>");
    equal(this.searcherCollection.getTextQuery(), "&lt;script&gt;alert(&#39;u call that a paddle? this is a paddle&#39;);&lt;/script&gt;");
});

asyncTest("Remove extended criteria with backspace", function() {
    this.clock.restore();
    this.view.render();
    this.addSearcher('extended-1', 'View html', 'Edit html');
    this.searcherCollection.triggerCollectionChanged();
    this.addSearcher('extended-2', 'View html', 'Edit html');
    this.searcherCollection.triggerCollectionChanged();

    var $criteria = this.view.extendedCriteriaContainerView.getFocusableForCriteria('extended-2').focus();
    this.triggerBackspace($criteria);
    var instance = this;
    _.defer(function() {
        ok(!instance.searcherCollection.get('extended-2').getIsSelected(), "Criteria was removed");
        ok(!instance.searcherCollection.get('extended-2').getJql(), "Searcher jql was cleared");

        equal(jQuery(document.activeElement).data("id"), instance.view.extendedCriteriaContainerView.getFocusableForCriteria('extended-1').data("id"), "Previous criteria was focused");

        $criteria = instance.view.extendedCriteriaContainerView.getFocusableForCriteria('extended-1').focus();
        instance.triggerBackspace($criteria);

        _.defer(function() {
            ok(jQuery(document.activeElement).hasClass("search-button"), "'Search button' was focused");
            start();
        });
    });
});

asyncTest("Remove extended criteria with delete", function() {
    this.clock.restore();

    this.view.render();
    this.addSearcher('extended-1', 'View html', 'Edit html');
    this.searcherCollection.triggerCollectionChanged();
    this.addSearcher('extended-2', 'View html', 'Edit html');
    this.searcherCollection.triggerCollectionChanged();
    this.addSearcher('extended-3', 'View html', 'Edit html');
    this.searcherCollection.triggerCollectionChanged();

    var $criteria = this.view.extendedCriteriaContainerView.getFocusableForCriteria('extended-2').focus();
    this.triggerDelete($criteria);

    var instance = this;
    _.defer(function() {
        ok(!instance.searcherCollection.get('extended-2').getIsSelected(), "Criteria was removed");
        ok(!instance.searcherCollection.get('extended-2').getJql(), "Searcher jql was cleared");
        equal(jQuery(document.activeElement).data("id"), instance.view.extendedCriteriaContainerView.getFocusableForCriteria('extended-3').data("id"), "Next criteria was focused");

        $criteria = instance.view.extendedCriteriaContainerView.getFocusableForCriteria('extended-3').focus();
        instance.triggerDelete($criteria);

        _.defer(function() {
            equal(jQuery(document.activeElement).data("id"), instance.view.extendedCriteriaContainerView.getFocusableForCriteria('extended-1').data("id"), "Previous criteria was focused");

            $criteria = instance.view.extendedCriteriaContainerView.getFocusableForCriteria('extended-1').focus();
            instance.triggerDelete($criteria);

            _.defer(function() {
                ok(jQuery(document.activeElement).hasClass("search-button"), "'Search Button' was focused");
                start();
            });
        });
    });
});
//
test("BasicQueryView repsonds to interactive state changes", function() {
    this.view.render();
    this.searcherCollection.setInteractive(false);
    equal(!this.view.$el.hasClass("loading"), false, "BasicQueryView is noninteractive");
    this.searcherCollection.setInteractive(true);
    equal(!this.view.$el.hasClass("loading"), true, "BasicQueryView is interactive");
});
