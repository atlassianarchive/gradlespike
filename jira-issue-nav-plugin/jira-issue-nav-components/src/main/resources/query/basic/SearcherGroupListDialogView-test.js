AJS.test.require("com.atlassian.jira.jira-issue-nav-components:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:query");

module("JIRA.Issues.SearcherSuggestHandler", {
    setup: function() {
        this.displayLimit = JIRA.Issues.SearcherGroupListDialogView.CRITERIA_DISPLAY_LIMIT;
        this.suggestHandler = new JIRA.Issues.SearcherGroupListDialogView.SuggestHandler({}, new AJS.SelectModel({
            element: jQuery([
                "<select multiple='true'>",
                   "<optgroup label='otherstuff'>",
                        "<option data-meta='{\"isShown\":true}' value='xxx1'></option>",
                        "<option data-meta='{\"isShown\":true}' value='xxx2'></option>",
                        "<option data-meta='{\"isShown\":false}' value='xxx3'></option>",
                    "</optgroup>",
                "</select>"
            ].join(""))
        }));
    },

    teardown: function () {
        JIRA.Issues.SearcherGroupListDialogView.CRITERIA_DISPLAY_LIMIT = this.displayLimit;
    }
});

test("With no query, out-of-context criteria aren't shown", function () {
    this.suggestHandler.execute("").done(function (groups) {
        equal(AJS.SuggestHelper.extractItems(groups).length, 2);
    });
});

test("With a query, out-of-context criteria are disabled", function () {
    // JRADEV-20120 Invalid criteria above the display limit should be disabled.
    JIRA.Issues.SearcherGroupListDialogView.CRITERIA_DISPLAY_LIMIT = 1;

    this.suggestHandler.execute("x").done(function (groups) {
        var items = AJS.SuggestHelper.extractItems(groups),
            disabledItems = _.filter(items, function (item) { return item.disabled(); });

        equal(items.length, 3, "The correct number of suggestions are shown");
        equal(disabledItems.length, 1, "The correct number of items are disabled");
        equal(disabledItems[0].properties.value, "xxx3", "The correct item is disabled");
    });
});