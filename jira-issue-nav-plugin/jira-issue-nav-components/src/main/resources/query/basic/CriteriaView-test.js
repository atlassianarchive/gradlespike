AJS.test.require("com.atlassian.jira.jira-issue-nav-components:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:query");

module('JIRA.Issues.CriteriaView', {
    setup: function() {
        this.searchMode = "basic";
        JIRA.Issues.SearcherDialog.initialize({
            queryStateModel: new JIRA.Issues.QueryStateModel()
        });
        this.$el = jQuery("<div></div>").appendTo("#qunit-fixture");
        this.searcherCollection = JIRA.Issues.Components.TestUtils.createSearcherCollection();
        this.model = new JIRA.Issues.CriteriaModel({
            id: "test-id"
        });
        this.view = new JIRA.Issues.CriteriaView({
            model: this.model,
            searcherCollection: this.searcherCollection
        });
        this.server = sinon.fakeServer.create();

        // Adds a searcher that doesn't try to hit the server.
        this.addSearcher = _.bind(function(id, viewHtml, editHtml) {
            var searcher = new JIRA.Issues.SearcherModel({
                id: id,
                name: 'Sample',
                groupId: 'group',
                groupName: 'A Group',
                viewHtml: viewHtml,
                jql: 'sample=yum',
                validSearcher: true,
                editHtml: editHtml,
                isSelected: true
            });

            // Don'r clear the edit html as the searcher collection isn't going back to the server to get it for us in test.
            searcher.clearEditHtml = jQuery.noop;

            this.searcherCollection.add(searcher);
        }, this);
    },
    teardown: function() {
        AJS.$(".ajs-layer").remove();
        this.server.restore();
        this.$el.remove();
    }
});

test("Lozenge view renders correctly", function() {
    this.view.render();

    ok(this.view.$el.text(), "Lozenge contains text");
});

asyncTest("Inline dialog focuses button on hide", 1, function() {
    this.view.$el.appendTo(this.$el);

    this.addSearcher("test-id", "<div class='searcherValue'>View HTML</div>", "<div>Edit HTML</div>");
    this.view.render();

    this.view.$el.find(".criteria-selector").click();
    var instance = this;
    setTimeout(function() {
        jQuery(".ajs-layer.active .aui-button.aui-button-link").click();
        setTimeout(function() {
            equal(instance.view.$el.find("button").data("id"), jQuery(document.activeElement).data("id"), "Expected to focus if we click cancel");
            start();
        }, 0);
    }, 0);
});

asyncTest("Focus is restored on update", function() {
    var view = this.view;
    var $el = view.$el;
    $el.appendTo(this.$el);

    this.addSearcher("test-id", "<div class='searcherValue'>View HTML</div>", "<div>Edit HTML</div>");

    view.render();
    $el.find("button").focus();
    setTimeout(function() {
        view.update();
        setTimeout(function() {
            equal(jQuery(document.activeElement).data("id"), $el.find("button").data("id"), "Expected to retain focus after update");
            start();
        }, 0);
    }, 0);
});

asyncTest("Inline dialog default renderer displays content returned from server", function() {
    // We don't use `addSearcher` here as we want to hit the server.
    this.addSearcher("test-id", "<div class='searcherValue'>View HTML</div>", null);
    this.view.render();

    var content = '<p>my content</p>';
    this.server.respondWith("POST", /.*QueryComponentRendererEdit.*/,
        [200, {},
            content]);

    this.view.$el.appendTo("#qunit-fixture");

    this.view.$el.find(".criteria-selector").click();
    var instance = this;
    setTimeout(function() {
        instance.server.respond();

        equal(jQuery(".ajs-layer.active").find("form").length, 1 ,"Inline dialog rendered into a form");
        equal(jQuery(".ajs-layer.active").find("form p").text(),"my content", "Inline dialog contained response from server");
        start()
    }, 0);
});

test("Invalid criteria is rendered, but has invalid class", function() {
    this.addSearcher("test-id", "<div class='searcherValue'>View HTML</div>", "<div>Edit HTML</div>");
    var searcher = this.searcherCollection.get("test-id");
    searcher.setValidSearcher(false);
    this.view.render();
    ok(this.view.$el.hasClass('invalid-searcher'), "Criteria selector has invalid class");
    equal(this.view.$('.searcherValue').text(), 'View HTML', "Criteria selector shows view html despite being invalid");
});

test("Renders default 'All' value when there are no searchers present", function() {
    this.view.render();
    equal(this.view.$('button').attr('aria-disabled'), 'true', "Rendered disabled");
    ok(this.view.$('button:contains("issues.components.query.search.all")').length, "Renders default 'All' value");
});

test("Primary criteria are disabled while SearcherCollection is noninteractive", function() {
    this.addSearcher("test-id", "<div class='searcherValue'>View HTML</div>", "<div>Edit HTML</div>");
    this.view.render();
    JIRA.Issues.SearcherDialog.instance.toggle = sinon.spy();
    this.searcherCollection.setInteractive(false);
    this.view.$(".criteria-selector").click();
    equal(JIRA.Issues.SearcherDialog.instance.toggle.callCount, 0, "SearcherDialog cannot be opened when noninteractive");
    this.searcherCollection.setInteractive(true);
    this.view.$(".criteria-selector").click();
    equal(JIRA.Issues.SearcherDialog.instance.toggle.callCount, 1, "SearcherDialog can be opened when interactive");
});

asyncTest("Error-laden searcher dialog will not show when clicking Switch to Advanced mode to close searcher dialog", function() {
    this.searchMode = "advanced";
    this.addSearcher("test-id", "<div class='searcherValue'>View HTML</div>", "<div class='has-errors'><form><input id='testValue' type='hidden' name='test' value='foo'/></form>Edit HTML</div>");
    this.view.render();
    // Open the searcher
    this.view.$(".criteria-selector").click();
    _.defer(function() {
        JIRA.Issues.SearcherDialog.instance.show = sinon.spy();
        // Set the value
        jQuery("#testValue").val("test");
        // Close by clicking outside
        AJS.$(document).click();
        equal(JIRA.Issues.SearcherDialog.instance.show.callCount, 0, "SearcherDialog is not opened when search mode is not basic");
        start();
    });
});
