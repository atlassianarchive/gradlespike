AJS.test.require("com.atlassian.jira.jira-issue-nav-components:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:query");

module("JIRA.Issues.SearcherCollection", {
    setup: function() {
        this.queryStateModel = JIRA.Issues.Components.TestUtils.mockQueryStateModel();
        this.searcherCollection = new JIRA.Issues.SearcherCollection([], {
            fixedLozenges: [],
            queryStateModel: this.queryStateModel
        });
    }
});

test("Test NO context change calls _querySearchersByValue", function () {
    var valueCalls = 0;
    var valuesCalls = 0;
    this.searcherCollection._querySearchersByValue = function () {
        ++valueCalls;
        return jQuery.Deferred();
    };
    this.searcherCollection._querySearchersAndValues = function () {
        ++valuesCalls;
        return jQuery.Deferred();
    };
    this.searcherCollection.createOrUpdateClauseWithQueryString("component");
    equal(valueCalls, 1);
    equal(valuesCalls, 0);
});

test("Test context change calls _querySearchersByValues", function () {
    var valueCalls = 0;
    var valuesCalls = 0;
    this.searcherCollection._querySearchersByValue = function () {
        ++valueCalls;
        return jQuery.Deferred();
    };
    this.searcherCollection._querySearchersAndValues = function () {
        ++valuesCalls;
        return jQuery.Deferred();
    };
    this.searcherCollection.createOrUpdateClauseWithQueryString("project");
    this.searcherCollection.createOrUpdateClauseWithQueryString("issuetype");
    equal(valueCalls, 0);
    equal(valuesCalls, 2);
});


test("getAddMenuGroupDescriptors()", function () {
    var stub = sinon.stub(AJS.Meta, "getNumber");
    stub.withArgs("max-recent-searchers").returns(10);
    var searchers = [{
        id: "c",
        groupId: "group_1",
        groupName: "Group 1",
        name: "c",
        lastViewed: 2
    }, {
        id: "b",
        groupId: "group 2",
        groupName: "Group 2",
        name: "B",
        lastViewed: 1
    }, {
        id: "z",
        groupId: "group 3",
        groupName: "Group 3",
        name: "a"
    }];
    this.searcherCollection.add(searchers);
    this.searcherCollection._searcherCache = searchers;

    var groupDescriptors = this.searcherCollection.getAddMenuGroupDescriptors();
    equal(groupDescriptors.length, 2, "2 group descriptors: one for recent, one for all.");

    var recentItemDescriptors = groupDescriptors[0].items();
    equal(recentItemDescriptors.length, 2, "2 recent item descriptors.");
    equal(recentItemDescriptors[0].value(), "c", "Reverse sorted by lastViewed, not by name or ID.");
    equal(recentItemDescriptors[1].value(), "b", "Reverse sorted by lastViewed, not by name or ID.");

    var allItemDescriptors = groupDescriptors[1].items();
    equal(allItemDescriptors.length, 3, "3 item descriptors.");
    equal(allItemDescriptors[0].value(), "z", "Sorted by name, not ID.");
    equal(allItemDescriptors[1].value(), "b", "Sorting is case insensitive.");
    equal(allItemDescriptors[2].value(), "c", "Sorting works.");
    stub.restore();
});

test("SearcherCollection.getVariableClauses", function() {
    var searcherCollection = new JIRA.Issues.SearcherCollection([], {
        fixedLozenges: [{
            id: "fixie"
        }]
    });

    searcherCollection.add(new JIRA.Issues.SearcherModel({
        id: "fixie"
    }));
    searcherCollection.add(new JIRA.Issues.SearcherModel({
        id: "no-query-string"
    }));
    searcherCollection.add(new JIRA.Issues.SearcherModel({
        id: "variable speed",
        validSearcher: true,
        editHtml: "<input type='text' name='speed' value='variable' />"
    }));

    var variableClauses = searcherCollection.getVariableClauses();
    equal(variableClauses.length, 1);
    equal(variableClauses[0].getId(), "variable speed");
});

test("SearcherCollection.createJql empty", function() {
    equal(this.searcherCollection.createJql(), "");
});

test("SearcherCollection.createJql 1", function() {
    this.searcherCollection.reset([{
        id: "1",
        viewHtml: "some name",
        jql: "aaa = bbb"
    }]);

    equal(this.searcherCollection.createJql(), "aaa = bbb");
});

test("SearcherCollection.createJql 2", function() {
    this.searcherCollection.reset([{
        id: "1",
        jql: "aaa = bbb"
    }, {
        id: "2",
        jql: "ccc = ddd"
    }]);

    equal(this.searcherCollection.createJql(), "aaa = bbb AND ccc = ddd");
});

test("SearcherCollection.getQueryString empty", function() {
    deepEqual(this.searcherCollection.getQueryString(), "");
});

test("SearcherCollection.getQueryString text", function() {
    this.searcherCollection.updateTextQuery("hello people");

    equal(this.searcherCollection.getQueryString(), "text=hello+people");
});

test("SearcherCollection.getQueryString multiple", function() {
    this.searcherCollection.add([{
        id: "wolverine",
        editHtml: "<input type='text' name='healing' value='super' /><br /><input type='text' name='claws' value='adamantium' />"
    }, {
        id: "xavier",
        editHtml: "<input type='text' name='mental' value='telepathy' />"
    }, {
        id: "magneto"  // SearcherModels with empty queryString values should not be included in the SearcherCollection's cumulative queryString.
    }]);

    equal(this.searcherCollection.getQueryString(), "healing=super&claws=adamantium&mental=telepathy");
});

test("SearcherCollection.getTextQuery", function() {
    this.searcherCollection.updateTextQuery();
    equal(this.searcherCollection.getTextQuery(), "");

    this.searcherCollection.updateTextQuery("");
    equal(this.searcherCollection.getTextQuery(), "");

    this.searcherCollection.updateTextQuery("tacos and burritos");
    equal(this.searcherCollection.getTextQuery(), "tacos and burritos");
});

test("SearcherCollection.updateTextQuery empty", function() {
    this.searcherCollection.updateTextQuery("");

    ok(!this.searcherCollection.get(this.searcherCollection.QUERY_ID));
});

test("SearcherCollection.updateTextQuery 1", function() {
    this.searcherCollection.updateTextQuery("hello");

    var queryStateModel = this.searcherCollection.get(this.searcherCollection.QUERY_ID);
    equal(queryStateModel.getViewHtml(), "hello");
    equal(queryStateModel.getJql(), "text ~ \"hello\"");
});

test("SearcherCollection.updateTextQuery 2", function() {
    this.searcherCollection.updateTextQuery("hello people");

    var queryStateModel = this.searcherCollection.get(this.searcherCollection.QUERY_ID);
    equal(queryStateModel.getViewHtml(), "hello people");
    equal(queryStateModel.getJql(), "text ~ \"hello people\"");
});

test("getNextPosition", function() {
    equal(0, this.searcherCollection.getNextPosition());

    this.searcherCollection.add({position: 0});
    equal(1, this.searcherCollection.getNextPosition());

    this.searcherCollection.add({position: 1336});
    equal(1337, this.searcherCollection.getNextPosition());

    this.searcherCollection.pop();
    equal(1, this.searcherCollection.getNextPosition());

    this.searcherCollection.pop();
    equal(0, this.searcherCollection.getNextPosition());
});

test("getSelectedCriteria", function() {
    this.searcherCollection = new JIRA.Issues.SearcherCollection([], {
        fixedLozenges: [{
            id: "project"
        }]
    });

    this.searcherCollection.add([{
        id: "project",
        isSelected: true,
        name: "Project",
        position: 0
    }, {
        isSelected: true,
        name: "b",
        position: 2
    }, {
        isSelected: true,
        name: "a",
        position: 1
    }, {
        isSelected: true,
        name: "c",
        position: 3
    }]);

    // Fixed searchers (project, etc.) shouldn't be included and the others
    // should be returned in ascending order of position.
    var searchers = this.searcherCollection.getSelectedCriteria();
    deepEqual(["a", "b", "c"], _.map(searchers, function(searcher) {
        return searcher.getName();
    }));
});

test("clearClause doesn't request a search for empty searchers", function() {
    this.searcherCollection.add({
        id: "project",
        isSelected: true,
        validSearcher: true
    });

    var searchWasRequested = false;
    this.searcherCollection.onSearchRequested(function() {
        searchWasRequested = true;
    });

    this.searcherCollection.clearClause("project");
    ok(!searchWasRequested, "No search was requested.");
});
