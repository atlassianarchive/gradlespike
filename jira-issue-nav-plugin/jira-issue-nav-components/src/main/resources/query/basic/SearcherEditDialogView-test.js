AJS.test.require("com.atlassian.jira.jira-issue-nav-components:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:query");

module("JIRA.Issues.SearcherEditDialogView", {
    setup: function() {
        this.searcherEditDialogView = new JIRA.Issues.SearcherEditDialogView({
            queryStateModel: new JIRA.Issues.QueryStateModel(),
            model: {
                id: 'id',
                setInitParams: function(){}
            }
        });
    }
});

test("render() should trigger a JIRA.Events.NEW_CONTENT_ADDED", function () {
    var triggerSpy = sinon.spy(JIRA, "trigger");

    this.searcherEditDialogView.render();

    ok(triggerSpy.calledOnce, "One event is triggered");
    equal(triggerSpy.firstCall.args[0], JIRA.Events.NEW_CONTENT_ADDED, "Event is JIRA.Events.NEW_CONTENT_ADDED");
    equal(typeof triggerSpy.firstCall.args[1][1], "string", "Ensure event has a reason argument");
    equal(triggerSpy.firstCall.args[1][1], JIRA.CONTENT_ADDED_REASON.criteriaPanelRefreshed, "Event reason is JIRA.CONTENT_ADDED_REASON.criteriaPanelRefreshed");

    triggerSpy.restore();
});
