AJS.test.require("com.atlassian.jira.jira-issue-nav-components:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:query");


module("JIRA.Issues.QueryModule", {
    setup: function() {
        this.queryModule = new JIRA.Issues.QueryModule({
            primaryClauses: [],
            preferredSearchMode: 'basic',
            queryStateModel: JIRA.Issues.Components.TestUtils.mockQueryStateModel()
        });
        this.queryModule.createAndRenderView();
        this.server = sinon.fakeServer.create();
    },
    teardown: function () {
        this.server.restore();
    }
});

test("QueryModule.resetToQuery clears all searcher collection searchers", function() {
    var searcherCollection = this.queryModule._basicQueryModule.searcherCollection;
    searcherCollection.add([{
        id: 'blah',
        editHtml: '',
        jql: null
    }, {
        id: "foo",
        editHtml: "<input name=\"foo\" type=\"text\" value=\"bar\"/>", // Has a clause.
        jql: null,
        validSearcher: true
    }]);

    var response = JSON.stringify({
        searchers: {
            groups: []
        },
        values: {}
    });

    this.server.respondWith("GET", /.*/, [200, { "Content-Type": "application/json" }, response]);
    this.queryModule.resetToQuery();
    this.server.respond();

    equal(searcherCollection.size(), 0);
});

test("QueryModule.queryChanged does not clear empty searchers", function() {
    var searcherCollection = this.queryModule._basicQueryModule.searcherCollection;
    searcherCollection.add({
        id: 'blah',
        editHtml: '',
        jql: null
    });

    var response = JSON.stringify({
        searchers: {
            groups: [{
                searchers: [{
                    id: 'blah',
                    editHtml: '',
                    jql: null
                }]
            }]
        },
        values: {}
    });

    this.server.respondWith("GET", /.*/, [200, { "Content-Type": "application/json" }, response]);
    this.queryModule.queryChanged();
    this.server.respond();

    equal(searcherCollection.size(), 1);
});

test("JQL errors are only shown in advanced search mode", function() {

    this.queryModule.createAndRenderView(jQuery("<div>").appendTo("#qunit-fixture"));

    this.queryModule._basicQueryModule.searcherCollection.add({
        id: "duedate",
        editHtml: '<div class="has-errors">"Last Thursday" is not a valid date.</div>',
        jql: 'due >= "Last Thursday"'
    });

    var queryView = this.queryModule._queryView;
    var BASIC = this.queryModule._queryStateModel.BASIC_SEARCH;
    var ADVANCED = this.queryModule._queryStateModel.ADVANCED_SEARCH;

    this.queryModule.onSearchError({
        "errorMessages": ["Unable to establish connection to server."],
        "errors": {}
    });

    this.queryModule.setSearchMode(ADVANCED);
    equal(queryView.$(".aui-message.error").length, 1, "General error is visible after switching to advanced search mode");
    this.queryModule.setSearchMode(BASIC);
    equal(queryView.$(".aui-message.error").length, 1, "General error is visible after switching to basic search mode");

    this.queryModule.onSearchError({
        "errorMessages": [],
        "errors": {
            "jql": "\"Last Thursday\" is not a valid date."
        }
    });

    equal(queryView.$(".aui-message.error").length, 0, "JQL error is not visible in basic search mode");
    this.queryModule.setSearchMode(ADVANCED);
    equal(queryView.$(".aui-message.error").length, 1, "JQL error is shown visible switching to advanced search mode");

    this.queryModule.onSearchError({
        "errorMessages": [],
        "errors": {
            "jql": "ERROR CODE 4096!"
        }
    });

    equal(!!queryView.switcherViewModel.getDisabled(), false, "Switching is enabled if both basic and advanced search mode render errors");

    this.queryModule.setSearchMode(BASIC);
    this.queryModule._basicQueryModule.searcherCollection.get("duedate").setEditHtml("<div>No errors here!</div>");
    equal(this.queryModule._basicQueryModule.hasErrors(), false, "Sanity check: Basic mode searchers are error-free");


    this.queryModule._basicQueryModule.searcherCollection.triggerJqlTooComplex("ERROR CODE 4096!");
    this.queryModule.onSearchError({
        "errorMessages": [],
        "errors": {
            "jql": "ERROR CODE 4096!"
        }
    });

    equal(this.queryModule.getSearchMode(), ADVANCED, "Automatically switches to advanced search mode when error is not displayed in basic search mode");
    equal(queryView.$(".aui-message.error").length, 1, "JQL error is visible after automatically switching to advanced search mode");
    equal(!!queryView.switcherViewModel.getDisabled(), true, "Switching search mode is disabled while JQL-only errors are visible");
});

test("Search success show warnings and triggers JQL Success" , function() {
    var queryView = this.queryModule._queryView;

    var warnings = ["Warning 1", "Warning 2"];
    queryView.showWarnings = sinon.spy();

    var eventSpy = sinon.spy();
    this.queryModule.onJqlSuccess(eventSpy);

    this.queryModule.onSearchSuccess(warnings);

    ok(queryView.showWarnings.calledWith(warnings), "success should show warnings on the view");
    ok(eventSpy.calledOnce, "JqlSuccess event should be fired");
});