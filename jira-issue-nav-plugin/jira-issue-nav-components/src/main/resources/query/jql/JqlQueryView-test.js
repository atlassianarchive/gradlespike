AJS.test.require("com.atlassian.jira.jira-issue-nav-components:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:query");

module('JIRA.Issues.JqlQueryView', {
    setup: function() {
        this.$el = jQuery('<div>').appendTo('body');

        this.queryModule = JIRA.Issues.Components.TestUtils.mockQueryModule();
        this.queryStateModel = JIRA.Issues.Components.TestUtils.mockQueryStateModel();



        this.queryModule.searchersReady = function() {
            return jQuery.Deferred().resolve();
        };

        this.view = new JIRA.Issues.JqlQueryView({
            queryStateModel: this.queryStateModel,
            el: this.$el
        });
    },
    teardown: function() {
        this.$el.remove();
    }
});

test("Jql query view template exists", function() {
    ok(JIRA.Templates.IssueNavQueryJql.jqlQueryView, "Template exists");
});

test("View renders something", function() {
    this.view.render();
    equal(this.$el.find("textarea").length, 1, "View renders something");
});

test("Empty text builds jql correctly", function() {
    this.view.render();

    equal(this.view.readJql(), "");
});

test("Setting text builds jql correctly", function() {
    this.view.render();

    this.$el.find("textarea").val("project = 'world domination' AND fixVersion = 6.6.6");

    equal(this.view.readJql(), "project = 'world domination' AND fixVersion = 6.6.6");
});

test("Resetting text to empty builds jql correctly", function() {
    this.view.render();

    this.$el.find("input").val("text ~ ohyeah");
    this.$el.find("input").val("");

    equal(this.view.readJql(), "");
});

test("Updating jql property updates text area", function() {
    this.view.render();
    this.queryStateModel.setJql("this is some freakin jql");
    equal(this.$el.find("textarea").val(), "this is some freakin jql");
});




// Tests for auto-resizing textarea

module('JIRA.Issues.JqlQueryView (Resizing)', {
    setup: function() {
        this.$el = jQuery('<div>').appendTo('body');
        this.queryModule = JIRA.Issues.Components.TestUtils.mockQueryModule();
        this.queryStateModel = JIRA.Issues.Components.TestUtils.mockQueryStateModel();
        this.view = new JIRA.Issues.JqlQueryView({
            queryStateModel: this.queryStateModel,
            el: this.$el
        });
    },
    teardown: function() {
        this.$el.remove();
    }
});

var getSingleLineHeight = function() {
    var queryStateModel = JIRA.Issues.Components.TestUtils.mockQueryStateModel();
    queryStateModel.setJql('a');
    var $el = jQuery('<div>').appendTo('body');
    var view = new JIRA.Issues.JqlQueryView({
        queryStateModel: queryStateModel,
        el: $el
    });
    var height = view._getInputField().height();
    $el.remove();
    return height;
};

// Call immediately after rendering to get fitted initial height since -
// expandOnInput doesn't shrink - only expands, and
// different browsers have different default heights for <textarea>
var resize = function(view) {
    view._getInputField().height(0)
        .trigger('keydown')
        .trigger('keypress')
        .trigger('keyup')
        .trigger('input');
};

var simulateInput = function(view, text) {
    view._getInputField().val(text)
        .trigger('keydown')
        .trigger('keypress')
        .trigger('keyup')
        .trigger('input');
};

test("Auto-resize on input", 2, function() {
    this.view.render();
    resize(this.view);
    simulateInput(this.view, "assignee = \n admin");
    var $input = this.view.$el.find("textarea");
    ok($input.height() > getSingleLineHeight(), "Input expanded");
    equal($input.innerHeight(), $input[0].scrollHeight, "No scrollbars");
});

test("Auto-resize on input is stable", function() {
    var instance = this;
    this.view.render();
    var $input = instance.view._getInputField();
    resize(instance.view);
    simulateInput(instance.view, "a");
    var height = $input.height();
    simulateInput(instance.view, "aa");
    equal($input.height(), height, "Input height stayed the same");
});

test("Auto-resize on render", 2, function() {
    this.queryStateModel.setJql("aaaaaaaa\nbbbbbbbb\nccccccc\ndddddddd\neeeeeeee\nffffffff\nggggggggg");
    var instance = this;
    this.view.render();
    var singleLineHeight = getSingleLineHeight();
    var $input = instance.view._getInputField();
    ok($input.height() > singleLineHeight, "Input expanded");
    equal($input.innerHeight(), $input[0].scrollHeight, "No scrollbars");
});

test("Auto-resize keeps same height on search", function() {
    var instance = this;
    this.view.render();
    var $input = instance.view._getInputField();
    resize(instance.view);
    simulateInput(instance.view, "assignee \n= admin");
    var twoLineHeight = $input.height();
    instance.view.readJql();
    equal($input.height(), twoLineHeight, "Same height");
});

test("Auto-resize to trimmed JQL on search", function() {
    var instance = this;
    this.view.render();
    var $input = instance.view._getInputField();
    resize(instance.view);

    simulateInput(instance.view, "assignee \n= admin");
    var twoLineHeight = $input.height();

    simulateInput(instance.view, "assignee \n= admin\n\n\n");
    ok($input.height() > twoLineHeight, "Expanded to accomodate trailing new lines");
    equal($input.innerHeight(), $input[0].scrollHeight, "No scrollbars for trailing new lines");
    instance.view.readJql();
    equal($input.height(), twoLineHeight, "Resized to fit trimmed JQL on search");
    equal($input.innerHeight(), $input[0].scrollHeight, "No scrollbars after trimming trailing new lines");
});

test("Auto-resize on model's jql change", function() {
    this.queryStateModel.setJql('foo');
    var $input = this.view.render()._getInputField();
    resize(this.view);
    var initialHeight = $input.height();
    this.queryStateModel.setJql('foo\nbar');
    ok($input.height() > initialHeight, "Expanded in response to model change");
});
