JIRA.Issues.SwitcherModel = JIRA.Issues.Brace.Model.extend({

    /**
     * id: id of the switcher
     * name: switcher name (displayed in switcher view)
     * view: backbone view object
     */
    namedAttributes: ["id", "name", "view", "text"]

});
