AJS.test.require("com.atlassian.jira.jira-issue-nav-components:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:query");

module("JIRA.Issues.QueryStateModel", {
    setup: function() {
        this.searchSpy = sinon.spy();
        this.server = sinon.fakeServer.create();

        this.queryStateModel = new JIRA.Issues.QueryStateModel();
    },
    teardown: function () {
        jQuery(".aui-loading").remove();
        this.server.restore();
    }
});

test("JRADEV-12250 Switch between basic and advanced", function () {
    equal(this.queryStateModel.getSearchMode(), "basic", "Inital Search Mode is basic");
    this.queryStateModel.switchToSearchMode("advanced");
    equal(this.queryStateModel.getSearchMode(), "advanced", "switched to advanced");
    this.queryStateModel.switchToSearchMode("basic");
    equal(this.queryStateModel.getSearchMode(), "basic", "switched to basic");
});

test("JRADEV-12250 Switch preferred search mode", function () {
    var saveSpy = sinon.spy();
    this.queryStateModel._savePreferredSearchMode = saveSpy;
    equal(this.queryStateModel.getPreferredSearchMode(), "basic", "Inital Preferred Search Mode is basic");

    this.queryStateModel.switchPreferredSearchMode("advanced");

    equal(this.queryStateModel.getPreferredSearchMode(), "advanced", "preferred switched to advanced");
    equal(this.queryStateModel.getSearchMode(), "advanced", "actual search mode switched to advanced");
    equal(saveSpy.callCount, 1, "preferred search mode should have been saved");

});
