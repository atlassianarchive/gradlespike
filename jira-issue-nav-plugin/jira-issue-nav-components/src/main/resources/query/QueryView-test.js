AJS.test.require("com.atlassian.jira.jira-issue-nav-components:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:query");
AJS.test.require("jira.webresources:set-focus");

module('JIRA.Issues.QueryView', {
    setup: function() {
        this.el = AJS.$("<form></form>");
        this.fakeXhr = sinon.useFakeXMLHttpRequest();
        this.fakeXhr.onCreate = function (xhr) {
            xhr.respond(200, {}, "");
        };

        this.queryStateModel = JIRA.Issues.Components.TestUtils.mockQueryStateModel();

        this.queryStateModel.setJql("");

        this.queryModule = new JIRA.Issues.QueryModule({
            queryStateModel: this.queryStateModel
        });

        this.queryModule.createAndRenderView(this.el);
    },

    teardown: function() {
        this.el.remove();
        this.fakeXhr.restore();
    }
});

test("Model is updated when the switcher is changed", function() {
    this.el.find(".switcher-item.active").trigger(new jQuery.Event("click"));

    equal(this.queryModule.getSearchMode(), "advanced");
});

test("Models are updated when the keyword search form is submitted", function() {
    this.queryStateModel.setJql = sinon.spy();
    var query = "some query";
    this.el.find("input").val(query);

    var e = new jQuery.Event("keypress");
    e.keyCode = 13;
    this.el.find("input").trigger(e);

    var searchRequestedSpy = this.queryStateModel.setJql;
    equal(searchRequestedSpy.callCount, 1);
    equal(searchRequestedSpy.args[0][0], "text ~ \"some query\"", "Search Results Models jql property and textarea's jql value are identical.");
});

test("When the model triggers an error even the view should render them", function() {

    equal(0, this.el.find(".notifications .aui-message").length, "No errors so far");
    this.queryModule.onSearchError({errors:{field:"Something bad happened."}});
    equal(1, this.el.find(".notifications .aui-message").length, "Should have one error now.");
    equal("Something bad happened.", this.el.find(".notifications .aui-message").text(), "Correct error is shown");

    this.queryModule.onSearchError({errors:{field:"Something else bad happened."}});
    equal(1, this.el.find(".notifications .aui-message").length, "Should have one error now.");
    equal("Something else bad happened.", this.el.find(".notifications .aui-message").text(), "Correct error is shown");

    equal(1, this.el.find(".notifications .aui-message").length, "Should have only one errors now.");

});
