AJS.test.require("com.atlassian.jira.jira-issue-nav-components:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issuenav");
AJS.test.require("jira.webresources:set-focus");

module('JIRA.Issues.SwitcherView', {
    setup: function() {
        this.$el = jQuery('<form><div class="aui-group search-container"></div><div class="notifications"></div><div class="mode-switcher"></div></form>');
        this.switchers = new JIRA.Issues.SwitcherCollection();

        this.fakeXhr = sinon.useFakeXMLHttpRequest();
        this.fakeXhr.onCreate = function (xhr) {
            xhr.respond(200, {}, "");
        };

        this.queryStateModel = new JIRA.Issues.QueryStateModel();
        this.queryStateModel.switchToSearchMode("x");

        this.switcherViewModel = new JIRA.Issues.QuerySwitcherViewModel({
            collection: this.switchers
        }, {
            queryStateModel: this.queryStateModel
        });
        this.switcherView = new JIRA.Issues.SwitcherView({
            el: this.$el,
            template: JIRA.Templates.IssueNavQuery.searchSwitcher,
            model: this.switcherViewModel,
            containerClass: ".search-container"
        });

        this.twoSwitchersData = [{
            id: "x",
            name: "xxx",
            text: "xxx",
            view: {
                setElement: function(el) { this.$el = jQuery(el); return this; },
                render: function() { this.$el.append("<div>first</div>"); return this; },
                focus: function() { return this; }
            }
        }, {
            id: "y",
            name: "yyy",
            text: "yyy",
            view: {
                setElement: function(el) { this.$el = jQuery(el); return this; },
                render: function() { this.$el.append("<div>second</div>"); return this; },
                focus: function() { return this; }
            }
        }];
    },
    teardown: function() {
        this.$el.remove();
        this.fakeXhr.restore();
    }
});

test("Search switcher template exists", function() {
    ok(JIRA.Templates.IssueNavQuery.searchSwitcher, "Template does not exist");
});

test("View renders a switcher element", function() {
    this.switcherView.render();

    equal(this.switcherView.switchEl.length, 1, "View rendered a switcher element");
});

test("View renders first member only", function() {
    this.switchers.reset(this.twoSwitchersData);
    this.switcherView.render();

    var switcher = this.switcherView.switchEl;
    equal(switcher.find(".switcher-item.active").length, 1, "Only 1 active element");
    ok(jQuery.trim(switcher.find(".switcher-item.active").data("id")).indexOf("x") > -1, "Shown element is not first in collection");
});

test("View switch renders second member only", function() {
    this.switchers.reset(this.twoSwitchersData);
    this.switcherView.render();
    this.switcherViewModel.selectById("y");

    var switcher = this.switcherView.switchEl;
    equal(switcher.find(".switcher-item.active").length, 1, "Only 1 active element");
    ok(jQuery.trim(switcher.find(".switcher-item.active").data("id")).indexOf("y") > -1, "Shown element is not first in collection");
});

test("Clicking on switcher element switches view", function() {
    this.switchers.reset(this.twoSwitchersData);
    this.switcherView.render();

    this.switcherView.getSwitcherTrigger().trigger('click');

    var switcher = this.switcherView.switchEl;
    equal(switcher.find(".switcher-item.active").length, 1, "Only 1 active element");
    ok(jQuery.trim(switcher.find(".switcher-item.active").data("id")).indexOf("y") > -1, "Shown element is not first in collection");
});

test("Switcher renders elements container", function() {
    this.switchers.reset(this.twoSwitchersData);
    this.switcherView.render();

    equal(this.$el.find(".search-container").children().length, 1, "Should only be one element in container");
    equal(this.$el.find(".search-container").children().text(),'first',"Shown element has incorrect children");
});

test("Switcher renders elements container after click", function() {
    this.switchers.reset(this.twoSwitchersData);
    this.switcherView.render();

    this.switcherView.getSwitcherTrigger().trigger('click');

    equal(this.$el.find(".search-container").children().length, 1, "Should only be one element in container") ;
    equal(this.$el.find(".search-container").children().text(), "second" ,"Shown element has incorrect children");
});

test("Selecting view before render renders correct item", function() {
    this.switchers.reset(this.twoSwitchersData);
    this.switcherViewModel.selectById("y");
    this.switcherView.render();

    var switcher = this.switcherView.switchEl;
    equal(switcher.find(".switcher-item.active").length, 1, "Only 1 active element");
    ok(jQuery.trim(switcher.find(".switcher-item.active").data("id")).indexOf("y") > -1, "Renders selected item");
    equal(this.$el.find(".search-container").children().length, 1, "Should only be one element in container") ;
    equal(this.$el.find(".search-container").children().text(), "second" ,"Shows selected view");
});

test("Cannot switch when switching is disabled", function() {
    this.switchers.reset(this.twoSwitchersData);
    this.switcherView.render();

    this.switcherViewModel.disableSwitching();

    var switcher = this.switcherView.switchEl;
    ok(switcher.hasClass("disabled"), "Switcher element has class disabled");

    switcher.trigger('click');
    ok(jQuery.trim(switcher.find(".switcher-item.active").data("id")).indexOf("x") > -1, "Initial element is still selected");
});

test("Title text updates correctly when disabling then re-enabling switching", function() {
    this.twoSwitchersData[0].id = 'simple';
    this.twoSwitchersData[1].id = 'advanced';
    this.switchers.reset(this.twoSwitchersData);
    this.switcherView.render();

    //simulate going into advanced mode with a query that doesn't allow switching back to simple
    this.queryStateModel.switchToSearchMode("advanced");
    this.switcherViewModel.disableSwitching();

    var switcher = this.switcherView.switchEl;
    equal(switcher.find(".switcher-item.active").attr("original-title"), "jira.jql.query.too.complex");

    //now re-enable switching and ensure the title has changed to the appropriate text.
    this.switcherViewModel.enableSwitching();
    equal(switcher.find(".switcher-item.active").attr("original-title"), "issues.components.query.switchto.basic.description");
});
