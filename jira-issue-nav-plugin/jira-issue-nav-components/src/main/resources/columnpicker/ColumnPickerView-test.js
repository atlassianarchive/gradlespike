AJS.test.require("com.atlassian.jira.jira-issue-nav-components:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:columnpicker");
module("JIRA.Issues.ColumnPickerView", {
    setup: function() {
        //Stop inline layer from appending to window.top so qunit iframe runner works
        AJS.params.ignoreFrame = true;

        this.$container = jQuery([
            '<div></div>'
        ].join(""));

        //Helper method to create our model
        this.createModel = function() {
            return new JIRA.Issues.ColumnPickerModel({
                columnConfig: "user"
            })
        };
        //Default model
        this.model = this.createModel();

        this.model.addColumnProvider("user", JIRA.Issues.ColumnConfigModel.create("user", "User"));
        this.model.addColumnProvider("filter", JIRA.Issues.ColumnConfigModel.create("filter", "Filter"));

        //Helper method to create a view
        this.createView = _.bind(function(model) {
            return new JIRA.Issues.ColumnPickerView({
                el: jQuery("#qunit-fixture"),
                columnPickerModel: model || this.model
            });
        }, this);
        this.view = this.createView(this.model);

        this.selectors = {
            bothButtons: ".check-list-top-panel button[type=button]",
            userButton: ".check-list-top-panel button[type=button][data-value=user]",
            filtersButton: ".check-list-top-panel button[type=button][data-value=filter]"
        }
    },

    teardown: function() {
        if (this.view.dialog) {
            this.view.dialog.hide();
        }
        this.view = null;
        this.model = null;

    }
});

test("_onDOMConfigChooserClick() change the buttons to reflect the new columnConfig state", function() {
    this.view.render();
    this.view._onDOMTriggerClick();

    ok(this.view.dialog.find("#columns-chooser-user").hasClass("active"),
        "User button is active"
    );
    ok(!this.view.dialog.find("#columns-chooser-filter").hasClass("active"),
        "Filter button is not active"
    );

    var ev = new jQuery.Event();
    ev.target = this.view.dialog.find("#columns-chooser-filter");
    ev.target.removeAttr("aria-disabled");
    this.view._onDOMConfigChooserClick(ev);

    ok(!this.view.dialog.find("#columns-chooser-user").hasClass("active"),
        "User button is not active after change:columnConfig"
    );
    ok(this.view.dialog.find("#columns-chooser-filter").hasClass("active"),
        "Filter button is active after change:columnConfig"
    );
    equal(this.model.getColumnConfig(), "filter",
        "The new column config has been saved into the model"
    )
});

test("_onDOMConfigChooserClick() does nothing if the button is disabled", function() {
    sinon.stub(this.model.columnsData.filter, "isDisabled").returns(true);
    this.view.render();
    this.view._onDOMTriggerClick();

    var ev = new jQuery.Event();
    ev.target = this.view.dialog.find("#columns-chooser-filter");
    this.view._onDOMConfigChooserClick(ev);

    ok(this.view.dialog.find("#columns-chooser-user").hasClass("active"),
        "User button is still active after the click"
    );
    ok(!this.view.dialog.find("#columns-chooser-filter").hasClass("active"),
        "Filter button is still inactive after the click"
    );
    equal(this.model.getColumnConfig(), "user",
        "ColumnConfig has not changed"
    )
});

test("_activateNewSubView() should refresh the search if the column config is ready to be used", function() {
    this.view.render();
    this.view._onDOMTriggerClick();

    var userView = this.view.activeSubview;

    sinon.stub(this.model, "shouldCloseOnActivation").returns(true);
    sinon.stub(this.model, "shouldRefreshSearchOnActivation").returns(true);
    sinon.stub(this.model, "refreshSearchWithColumns");
    sinon.stub(this.view.dialog, "hide");

    this.view._activateNewSubView(userView);
    ok(this.model.refreshSearchWithColumns.calledOnce,
        "The search has been refreshed"
    );
    ok(this.view.dialog.hide.calledOnce,
        "The search has been refreshed"
    );

    this.view.dialog.hide.restore(); //Restore this method because it will be used in teardown()
});

test("_activateNewSubView() should load the default columns and display the new subview if it is not ready", function() {
    this.view.render();
    this.view._onDOMTriggerClick();

    var userView = this.view.activeSubview;

    sinon.stub(this.model, "shouldCloseOnActivation").returns(false);
    sinon.stub(this.model, "shouldLoadDefaultsOnActivation").returns(true);

    sinon.stub(this.model, "loadDefaultColumns");
    sinon.stub(this.view, "adjustHeight");
    sinon.stub(userView, "show");

    this.view._activateNewSubView(userView);
    ok(this.model.loadDefaultColumns.calledOnce,
        "The model has loaded the default columns"
    );
    ok(userView.show.calledOnce,
        "The new subView is displayed"
    );
    ok(userView === this.view.activeSubview,
        "The new subView is saved as active"
    );
    ok(this.view.adjustHeight,
        "The dialog height should be corrected"
    );
});

test("_onDOMFormSubmit() should save the columns of the active view and close the dialog", function() {
    this.view.render();
    this.view._onDOMTriggerClick();

    sinon.stub(this.view.activeSubview, "saveColumns");
    sinon.stub(this.view.dialog, "hide");

    this.view._onDOMFormSubmit(new jQuery.Event());

    ok(this.view.activeSubview.saveColumns.calledOnce);
    ok(this.view.dialog.hide.calledOnce);

    this.view.dialog.hide.restore(); //Restore this method because it will be used in teardown()
});

test("_onDOMCloseDialogClick() should close the dialog", function() {
    this.view.render();
    this.view.dialog.show();

    sinon.stub(this.view.dialog, "hide");
    this.view._onDOMCloseDialogClick();

    ok(this.view.dialog.hide.calledOnce);

    this.view.dialog.hide.restore(); //Restore this method because it will be used in teardown()
});

test("_onDOMTriggerClick() should request the available columns", function() {
    sinon.stub(this.model, "fetchAvailableColumnsIfNeeded");
    this.view.render();

    this.view._onDOMTriggerClick();

    ok(this.model.fetchAvailableColumnsIfNeeded.calledOnce);
});

test("_onDOMTriggerClick() should set the correct subView", function() {
    this.view.render();

    equal(this.view.activeSubview, null);

    this.view._onDOMTriggerClick();

    ok(this.view.activeSubview instanceof JIRA.Issues.ColumnPickerSparklerView,
        "Should create a reference to the active subView"
    );
    equal(this.view.activeSubview.model.getName(), "user",
        "The active subView should render the 'user' model"
    );
});

test("_onDOMDialogHide() should hide the current subView", function() {
    this.view.render();
    this.view.$trigger.click();

    sinon.stub(this.model,"revertColumnConfig");
    sinon.stub(this.view.activeSubview, "hide");
    this.view._onDOMDialogHide();

    ok(this.view.activeSubview.hide.calledOnce)
});

test("_onModelChangeColumnConfig() should change the buttons to reflect the new columnConfig state", function() {
    this.view.render();
    this.view.dialog.show();

    ok(this.view.dialog.find("#columns-chooser-user").hasClass("active"),
        "User button is active"
    );
    ok(!this.view.dialog.find("#columns-chooser-filter").hasClass("active"),
        "Filter button is not active"
    );

    sinon.stub(this.model, "previous").returns("user");
    this.view._onModelChangeColumnConfig(this.model, "filter");

    ok(!this.view.dialog.find("#columns-chooser-user").hasClass("active"),
        "User button is not active after change:columnConfig"
    );
    ok(this.view.dialog.find("#columns-chooser-filter").hasClass("active"),
        "Filter button is active after change:columnConfig"
    );
});

test("_onModelChangeAvailableColumns() should create the sparkerl control in the subviews", function() {
    var $content = jQuery("<div></div>");
    this.view._createSubViews($content);
    var availableColumns = [
        {label: "label 1", value: "value 1"},
        {label: "label 2", value: "value 2"}
    ];

    _.each(this.view.subViews, function(subView) {
        sinon.stub(subView, "createSparklerControl");
    });

    this.view._onModelChangeAvailableColumns(this.model, availableColumns);

    _.each(this.view.subViews, function(subView) {
        ok(subView.createSparklerControl.calledOnce);
        ok(subView.createSparklerControl.calledWith(availableColumns));
    });
});

test("_onModelChangeColumnConfigDisabled() should disable the button if the model is disabled", function(){
    sinon.stub(this.model.columnsData.filter, "isDisabled").returns(true);
    this.view.render();
    this.view._onDOMTriggerClick();

    equal(this.view.dialog.find("#columns-chooser-filter").attr("aria-disabled"), "true",
        "Filter button is initially disabled"
    );

    this.view._onModelChangeColumnConfigDisabled(this.model.columnsData.filter, false);
    ok(!this.view.dialog.find("#columns-chooser-filter").attr("aria-disabled"),
        "Filter button is enabled after a change in the model"
    );

    this.view._onModelChangeColumnConfigDisabled(this.model.columnsData.filter, true);
    equal(this.view.dialog.find("#columns-chooser-filter").attr("aria-disabled"), "true",
        "Filter button is disabled after a change in the model"
    );
});

test("render() builds the trigger icon", function() {
    this.view.render();
    ok(this.view.$trigger.is('.column-picker-trigger'))
});

test("render() binds the trigger to the handler", function() {
    sinon.stub(this.view, "_onDOMTriggerClick");
    this.view.render();

    this.view.$trigger.click();

    ok(this.view._onDOMTriggerClick.calledOnce,
        "Click on the trigger should call the handler"
    );
});

test("_generateInlineDialogContent() generates the dialog content", function() {
    var $content = jQuery('<div></div>');
    var selectors = this.selectors;

    this.view._generateInlineDialogContent($content, null, function() {

        ok($content.find("form.column-picker").length,
            "It generates a form");

        ok($content.find(selectors.bothButtons).length == 2,
            "It generates two buttons inside the form");

        ok($content.find(selectors.bothButtons).eq(0).attr("data-value") == "user",
            "The first button is bound to user columns");

        ok($content.find(selectors.bothButtons).eq(1).attr("data-value") == "filter",
            "The second button is bound to filter columns");
    });
});

test("_generateInlineDialogContent() disables the filter option if there is no active filter", function() {
    var $content = jQuery('<div></div>');
    var selectors = this.selectors;
    var isDisabledStub = sinon.stub(this.model.columnsData.filter, "isDisabled").returns(true);

    this.view._generateInlineDialogContent($content, null, function() {
        equal($content.find(selectors.filtersButton).attr("aria-disabled"), "true",
            "Button for filter is disabled"
        );
    });

    isDisabledStub.restore();
});

test("_generateInlineDialogContent() enables the filter option if there is an active filter", function() {
    var $content = jQuery('<div></div>');
    var selectors = this.selectors;
    var isDisabledStub = sinon.stub(this.model.columnsData.filter, "isDisabled").returns(false);

    this.view._generateInlineDialogContent($content, null, function() {
        ok(!$content.find(selectors.filtersButton).attr("aria-disabled"),
            "Button for filter is not disabled"
        );
    });

    isDisabledStub.restore();
});

test("_generateInlineDialogContent() selects the 'user' sparkler if the column config is 'user'", function() {
    var $content = jQuery('<div></div>');
    var selectors = this.selectors;
    this.model.setColumnConfig("user");

    this.view._generateInlineDialogContent($content, null, function() {
        ok($content.find(selectors.userButton).hasClass("active"));
        ok(!$content.find(selectors.filtersButton).hasClass("active"));
    });
});

test("_generateInlineDialogContent() selects the 'filter' option if useUserColumns is false", function() {
    var $content = jQuery('<div></div>');
    var selectors = this.selectors;
    this.model.setColumnConfig("filter");

    this.view._generateInlineDialogContent($content, null, function() {
        ok(!$content.find(selectors.userButton).hasClass("active"));
        ok($content.find(selectors.filtersButton).hasClass("active"));
    });
});

test("_generateInlineDialogContent() sets the handler for the close link", function() {
    var $content = jQuery('<div></div>');
    var closeDialog = sinon.stub(this.view, "_onDOMCloseDialogClick");

    this.view._generateInlineDialogContent($content, null, function() {
        $content.find(".close-dialog").click();
        ok(closeDialog.calledOnce);
    });
});

test("_generateInlineDialogContent() sets the handler for form submission", function() {
    var $content = jQuery('<div></div>');
    var formSubmit = sinon.stub(this.view, "_onDOMFormSubmit");

    this.view._generateInlineDialogContent($content, null, function() {
        //Ensure we don't submit the form, it will create problems with qUnit
        $content.find("form").submit(function(ev) {ev.preventDefault()});

        $content.find("form").submit();
        ok(formSubmit.calledOnce);
    });
});

test("_renderInlineDialog() creates the dialog the first time", function() {
    sinon.spy(AJS, "InlineDialog");
    this.view._renderInlineDialog();

    ok(AJS.InlineDialog.calledOnce,
        "Should create a AJS.InlineDialog"
    );
    ok(this.view.dialog instanceof jQuery,
        "Should store the dialog in view.dialog"
    );

    var currentDialog = this.view.dialog;
    this.view._renderInlineDialog();
    ok(currentDialog === this.view.dialog,
        "Should reuse the current dialog if present"
    );

    AJS.InlineDialog.restore();
});

test("_createSubViews() creates a sparkler for each submodel", function() {
    var $content = jQuery("<div></div>");
    this.view._createSubViews($content);

    ok(this.view.subViews.filter instanceof JIRA.Issues.ColumnPickerSparklerView);
    ok(this.view.subViews.user instanceof JIRA.Issues.ColumnPickerSparklerView);
});

test("_createSubViews() adds a listener for change:columnConfig", function() {
    sinon.stub(this.view, "_onModelChangeColumnConfig");
    var $content = jQuery("<div></div>");
    this.view._createSubViews($content);

    this.model.trigger("change:columnConfig");
    ok(this.view._onModelChangeColumnConfig.calledOnce);
});

test("_renderInlineDialog() adds a listener for change:availableColumns", function() {
    sinon.stub(this.view, "_onModelChangeAvailableColumns");
    var $content = jQuery("<div></div>");
    this.view._createSubViews($content);

    this.model.trigger("change:availableColumns");
    ok(this.view._onModelChangeAvailableColumns.calledOnce);
});

