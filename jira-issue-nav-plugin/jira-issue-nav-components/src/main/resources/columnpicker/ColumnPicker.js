if (!JIRA.Components) {
    JIRA.Components = {};
}

/**
 * A module that controls the column picker model and the column picker view in a single interface.
 */
JIRA.Components.ColumnPicker = JIRA.Issues.Brace.Model.extend({

    initialize: function (attr, options) {

        this.columnPickerModel = new JIRA.Issues.ColumnPickerModel({autoUpdate: options.autoUpdate});
        this.columnPickerView = new JIRA.Issues.ColumnPickerView({columnPickerModel: this.columnPickerModel});

        _.each(options.providers, _.bind(function (descriptor) {
            if (descriptor instanceof JIRA.Issues.ColumnConfigModel) {
                this.columnPickerModel.addColumnProvider(descriptor.getName(), descriptor);
            } else {
                this.columnPickerModel.addColumnProvider(descriptor.id, this._createProviderModel(descriptor));
                if (descriptor.columns) {
                    this.columnPickerModel.syncColumns(descriptor.id, descriptor.columns);
                }
            }
        }, this));

        this.setCurrentColumnConfig(options.providers[0].id);
        if (options.el) {
            this.columnPickerView.setElement(options.el).render();
        }
    },

    _createProviderModel: function (descriptor) {
        return JIRA.Issues.ColumnConfigModel.create(descriptor.id, descriptor.label,
            _.omit(descriptor, "id", "label", "columns"))
    },

    getCurrentColumnConfig: function () {
        return this.columnPickerModel.getCurrentColumnConfig();
    },

    setElement: function ($el) {
        this.columnPickerView.setElement($el);
        return this;
    },
    render: function () {
        this.columnPickerView.render();
        return this;
    },
    clearFilterConfiguration: function () {
        this.columnPickerModel.clearFilterConfiguration();
        return this;
    },
    adjustHeight: function () {
        this.columnPickerView.adjustHeight();
        return this;
    },
    setCurrentColumnConfig: function (name) {
        this.columnPickerModel.setCurrentColumnConfig(name);
        return this;
    },
    saveColumns: function (cols) {
        this.columnPickerModel.saveColumns(cols);
        return this;
    },
    syncColumns: function (name, columns) {
        this.columnPickerModel.syncColumns(name, columns);
        return this;
    },
    getColumnConfig: function () {
        this.columnPickerModel.getColumnConfig();
    },
    onColumnsSync: function (func, ctx) {
        this.columnPickerModel.onColumnsSync(func, ctx);
        return this;
    },
    on: function (evt, func, ctx) {
        this.columnPickerModel.on(evt, func, ctx);
        return this;
    },
    off: function (evt, func) {
        this.columnPickerModel.off(evt, func);
        return this;
    }

});

JIRA.Components.ColumnPicker.create = function (options) {
    return new JIRA.Components.ColumnPicker(null, options);
};





