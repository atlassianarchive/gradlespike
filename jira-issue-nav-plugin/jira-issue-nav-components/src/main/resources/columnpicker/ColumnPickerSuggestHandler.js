JIRA.Issues.ColumnPickerSuggestHandler = AJS.SelectSuggestHandler.extend({

    /**
     * Creates a actionBar item that contains a link. This link is usually used
     * to restore the defaults items in the sparkler
     *
     * @param {string} [text] Text to render inside the link
     *
     * @return {jQuery} LI element that contains the link
     */
    createActionBar: function(text) {
        //If there are no text, create nothing
        if (!text) {
            return null
        }

        var $element = jQuery(JIRA.Templates.Dialogs.ColumnPicker.restoreDefaultsLink({
            linkText: text
        }));

        $element.click(function(ev) {
            if (jQuery(ev.target).attr("aria-disabled") !== "true") {
                $element.trigger("actionclick");
            }
            ev.preventDefault();
        });

        return jQuery("<li class='check-list-group-actions'></li>").append($element);
    },

    /**
     * Formats descriptors for display in checkbox multiselect
     *
     * @param descriptors
     * @return {Array} formatted descriptors
     */
    formatSuggestions: function(descriptors, query) {
        var selectedItems = AJS.SuggestHelper.removeDuplicates(this.model.getDisplayableSelectedDescriptors());
        var selectedGroup = new AJS.GroupDescriptor({
            styleClass: "selected-group",
            items: selectedItems
        });
        descriptors.splice(0, 0, selectedGroup);

        //Add the actionBar to the first group with items, so it is rendered as the first item in the scroll list
        //If there are selected elements, it will be the selectedItems group
        //If not, it will be the uneslectedItems group.
        var firstGroupWithItems = _.find(descriptors, function(group) {return group.items().length});
        if (firstGroupWithItems) {
            firstGroupWithItems.actionBarHtml(this.createActionBar(this.options.actionBar));
        }

        if (query.length > 0) {
            descriptors = AJS.SuggestHelper.removeDuplicates(descriptors);
            // Extract all items from the descriptors and sort them by label.
            var items = AJS.SuggestHelper.extractItems(descriptors).sort(function(a, b) {
                a = a.label().toLowerCase();
                b = b.label().toLowerCase();
                return a.localeCompare(b);
            });
            descriptors = [new AJS.GroupDescriptor({items: items})];
        }
        return descriptors;
    }
});
