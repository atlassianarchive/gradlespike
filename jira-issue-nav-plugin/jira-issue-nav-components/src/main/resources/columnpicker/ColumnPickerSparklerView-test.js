AJS.test.require("com.atlassian.jira.jira-issue-nav-components:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:columnpicker");

module("JIRA.Issues.ColumnPickerSparklerView", {
    setup: function () {
        //Stop inline layer from appending to window.top so qunit iframe runner works
        AJS.params.ignoreFrame = true;

        this.$container = jQuery([
            '<div id="sparkler_container">',
            '</div>'
        ].join("")).appendTo("#qunit-fixture");

        //Helper method to create our model
        this.createModel = function(overriddenMethods) {
            return JIRA.Issues.ColumnConfigModel.create("mock","Mock Model", overriddenMethods);   
        };

        //Default model
        this.model = this.createModel({
            url: '/default/url'
        });
        this.model.set({
            columns: ["val1","val3"],
            actionBarText: "Reset all the columns!"
        },{silent: true});

        //Helper method to create a view
        this.createView = _.bind(function(model, maxResults) {
            return new JIRA.Issues.ColumnPickerSparklerView({
                el: this.$container,
                model: model || this.model,
                maxResults: maxResults
            });
        },this);
        
        //Default view
        this.view = this.createView(this.model);

        this.xhr = sinon.useFakeXMLHttpRequest();
        var requests = this.requests = [];
        this.xhr.onCreate = function (xhr) {
            requests.push(xhr);
        };
    },

    teardown: function () {
        this.xhr.restore();
    }
});

test("when created, it locks the scroll on the $container", function () {
    var spy = sinon.spy(AJS.$.fn, "scrollLock");

    this.createView();

    ok(spy.calledOnce);
    spy.restore();
});

test("when created, it uses the 'maxResults' value from the config", function () {
    var view = this.createView(this.createModel(), 10);
    equal(view._maxResults, 10);
});

test("when created, it uses the default 'maxResults'", function () {
    var view = this.createView();
    equal(view._maxResults, 25);
});

test("when created, it listents to the change:columns event", function () {
    sinon.spy(JIRA.Issues.ColumnPickerSparklerView.prototype,"_setSelectedColumns");
    
    var view = this.createView();
    this.model.set("columns", ["a","b","c"]);
    
    ok(view._setSelectedColumns.calledOnce);
    
    JIRA.Issues.ColumnPickerSparklerView.prototype._setSelectedColumns.restore()
});

/** FAIL: change:columns is being fired when we call hide() because hide sets 
 * another property, and Backbone will fire change events for all pending (i.e. silent)
 * changes
test("method hide() do not fire a change:columns event", function () {
    sinon.spy(JIRA.Issues.ColumnPickerSparklerView.prototype,"_setSelectedColumns");

    var view = this.createView();
    this.model.set("savedColumns", ["a","b"], {silent: true});
    this.model.set("columns", ["a","b","c","d"], {silent: true});
    view.hide();
        
    ok(!view._setSelectedColumns.called);

    JIRA.Issues.ColumnPickerSparklerView.prototype._setSelectedColumns.restore()
});
*/

test("method _buildQueryableSelect() creates a AJS.Templates.queryableSelect", function() {
    var queryableSelect = sinon.spy(AJS.Templates, "queryableSelect");
    var itemDescriptor = sinon.spy(AJS, "ItemDescriptor");
    var groupDescriptor = sinon.spy(AJS, "GroupDescriptor");

    this.view._buildQueryableSelect([
        {label: "label 1", value: "val1"},
        {label: "label 2", value: "val2"}
    ]);

    //Test internal call to AJS.Templates.queryableSelect
    ok(queryableSelect.calledOnce,
        "AJS.Templates.queryableSelect is called");
    equal(queryableSelect.firstCall.args[0].id, "mock-column-sparkler",
        "Created element ID matches the sparkler id");
    ok(queryableSelect.firstCall.args[0].descriptors[0] instanceof groupDescriptor,
        "Created element descriptor uses AJS.GroupDescriptor");

    //Test internal call to AJS.GroupDescriptor
    ok(groupDescriptor.called,
        "AJS.GroupDescriptor is called");
    equal(groupDescriptor.firstCall.args[0].items.length, 2,
        "AJS.GroupDescriptor is created with two elements (one per column)");
    ok(groupDescriptor.firstCall.args[0].items[0] instanceof itemDescriptor,
        "AJS.GroupDescriptor is created with an instance of itemDescriptor as first argument");
    ok(groupDescriptor.firstCall.args[0].items[1] instanceof itemDescriptor,
        "AJS.GroupDescriptor is created with an instance of itemDescriptor as second argument");

    //Test internal call to AJS.ItemDescriptor
    deepEqual(itemDescriptor.firstCall.args[0], {label: "label 1", value: "val1", title: "label 1"},
        "AJS.ItemDescriptor is created with the first column");
    deepEqual(itemDescriptor.secondCall.args[0], {label: "label 2", value: "val2", title: "label 2"},
        "AJS.ItemDescriptor is created with the second column");

    queryableSelect.restore();
    itemDescriptor.restore();
    groupDescriptor.restore();
});

test("method createSparklerControl() creates a CheckboxMultiSelect", function() {
    var checkboxMultiSelect = sinon.spy(AJS, "CheckboxMultiSelect");
    var _buildQueryableSelect = sinon.spy(this.view, "_buildQueryableSelect");

    var availableColumns = [
        {label: "label 1", value: "val1"},
        {label: "label 2", value: "val2"},
        {label: "label 3", value: "val3"}
    ];
    this.view.createSparklerControl(availableColumns);
    var columns = this.model.getColumns();

    //Test internal call to _buildQueryableSelect
    ok(_buildQueryableSelect.calledOnce,
        "Internal _buildQueryableSelect is called");
    deepEqual(_buildQueryableSelect.firstCall.args[0], availableColumns,
        "Internal _buildQueryableSelect is called with the columns options");
    ok(AJS.$.contains(this.$container[0], _buildQueryableSelect.firstCall.returnValue[0]),
        "QueryableSelect element is appended to the DOM");

    //Test call to CheckboxMultiSelect
    ok(checkboxMultiSelect.calledOnce,
        "AJS.CheckboxMultiSelect is called once");
    equal(checkboxMultiSelect.firstCall.args[0].maxInlineResultsDisplayed, 25,
        "AJS.CheckboxMultiSelect is called with the right limit for displayed results");
    equal(checkboxMultiSelect.firstCall.args[0].actionBar, "Reset all the columns!",
        "AJS.CheckboxMultiSelect is called with the right text for actionBar");
    
    checkboxMultiSelect.restore();
    _buildQueryableSelect.restore();
});

test("method saveColumns() saves the columns in the model and in the backend", function() {
    var availableColumns = [
        {label: "label 1", value: "val1"},
        {label: "label 2", value: "val2"},
        {label: "label 3", value: "val3"}
    ];
    this.view.createSparklerControl(availableColumns);
    sinon.stub(this.view._sparkler.model, "getSelectedValues").returns(["val3","val2","val1"]);
    sinon.stub(this.model,"save");
    
    this.view.saveColumns();
    
    deepEqual(this.model.getColumns(), ["val1","val3","val2"],
        "Our model contains the selected columns in the right order");
    
    ok(this.model.save.calledOnce, 
        "Model save() method has been called");

    this.view._sparkler.model.getSelectedValues.restore();
});

test("when the sparkler is created, it marks as selected the sparkler's columns", function(){
    var availableColumns = [
        {label: "label 1", value: "val1"},
        {label: "label 2", value: "val2"},
        {label: "label 3", value: "val3"}
    ];
    this.view.createSparklerControl(availableColumns);

    //Select default columns when sparkler is created
    deepEqual(this.view._sparkler.model.getSelectedValues(), [ "val1", "val3" ],
        "Columns 'label 1' and 'label 3' should be selected");
});

test("when the sparkler is created, it is marked as disabled if needed", function(){
    sinon.stub(this.model,"isEditDisabled").returns(true);
    sinon.spy(this.view,"_disableEdit");

    this.view.createSparklerControl([
        {label: "label 1", value: "val1"},
        {label: "label 2", value: "val2"},
        {label: "label 3", value: "val3"}
    ]);

    //Select default columns when sparkler is created
    ok(this.view._disableEdit.calledOnce,
        "_disableEdit should be called once");

    this.model.isEditDisabled.restore();
    this.view._disableEdit.restore();
});

test("when the model's columns property change, they should be selected in the sparkler's columns", function(){
    var availableColumns = [
        {label: "label 1", value: "val1"},
        {label: "label 2", value: "val2"},
        {label: "label 3", value: "val3"}
    ];
    this.view.createSparklerControl(availableColumns);

    this.model.setColumns(["val2"]);
    deepEqual(this.view._sparkler.model.getSelectedValues(), [ "val2" ],
        "Columns 'label 2' should be selected");
});

test("when the model's isEditDisabled property change, the new state should be reflected in the sparkler", function() {
    sinon.stub(this.model,"isEditDisabled");
    sinon.spy(this.view,"_disableEdit");
    sinon.spy(this.view,"_enableEdit");

    this.view.createSparklerControl([
        {label: "label 1", value: "val1"},
        {label: "label 2", value: "val2"},
        {label: "label 3", value: "val3"}
    ]);

    this.view._disableEdit.reset();
    this.view._enableEdit.reset();
    this.model.isEditDisabled.reset();
    this.model.isEditDisabled.returns(true);
    this.model.trigger("change:editDisabled");
    ok(!this.view._enableEdit.called,
        "_enableEdit should not be called");
    ok(this.view._disableEdit.calledOnce,
        "_disableEdit should be called once");

    this.view._disableEdit.reset();
    this.view._enableEdit.reset();
    this.model.isEditDisabled.reset();
    this.model.isEditDisabled.returns(false);
    this.model.trigger("change:editDisabled");
    ok(!this.view._disableEdit.called,
        "_disableEdit should not be called");
    ok(this.view._enableEdit.calledOnce,
        "_enableEdit should be called once");

});

/*
// TODO MOVE TO ColumnConfigModel-test 
test("method _getSelectedValuesPreservingTheOrder() returns the new columns preserving the order", function(){
    //Available columns
    this.columnPickerSparkler.setColumns([
        {label: "label 1", value: "a"},
        {label: "label 2", value: "b"},
        {label: "label 3", value: "c"},
        {label: "label 4", value: "d"}
    ]);
    //Already selected columns
    this.columnPickerSparkler._setSelectedColumns([
        {label: "label 4", value: "d"},
        {label: "label 4", value: "c"},
        {label: "label 2", value: "b"}
    ]);
    //Values selected by the user
    var getSelectedValues = sinon.stub(this.columnPickerSparkler._sparkler.model, "getSelectedValues");
    getSelectedValues.returns(["a","b","d"]);

    var columns = this.columnPickerSparkler._getSelectedValuesPreservingTheOrder();

    //"d" and "b" were already selected
    //"c" has been unselected by the user
    //"a" has been selected by the user
    deepEqual(columns, ["d","b","a"],
        "It preserves the right order");
});
*/

/*
 // TODO MOVE TO ColumnConfigModel-test 
test("method fetchSelectedColumns() retrieves the selected columns from the server", function() {
    this.setDefaultColumns();
    var promise = this.columnPickerSparkler.fetchSelectedColumns();
    var setSelectedColumns = sinon.stub(this.columnPickerSparkler, "_setSelectedColumns");

    ok(this.requests.length,
        "It does a request to the server");
    equal(this.requests[0].method, "GET",
        "It does a request GET to the server");
    ok(this.requests[0].url.match("^/default/url"),
        "It does a request to the configured URL");

    this.requests[0].respond(200, { "Content-Type": "application/json" },
        '[{ "label":"Label 1", "value": "val1" }]');

    ok(setSelectedColumns.calledOnce,
        "It calls _setSelectedColumns when the response arrives");
    deepEqual(setSelectedColumns.firstCall.args[0], [ { "label":"Label 1", "value": "val1" } ],
        "It calls _setSelectedColumns with the response from the server");
    equal(promise.state(), "resolved",
        "It resolves the fetchSelectedColumns promise");

    setSelectedColumns.restore();
});
*/

/*
 // TODO MOVE TO ColumnConfigModel-test 
test("method fetchSelectedColumns() calls the defaultColumnsStrategy if there are no columns in the server", function() {
    //Compose the defaultColumns strategy
    var deferred = AJS.$.Deferred();
    var defaultColumnsStrategy = sinon.spy(function() {
        deferred.resolve([ { "label":"Label 1", "value": "val1" } ]);
        return deferred;
    });

    //Build the ColumnPickerSparklerView using the defaultColumns strategy
    this.columnPickerSparkler = new JIRA.Issues.ColumnPickerSparklerView({
        $el: this.$container,
        name: "default",
        url: "/default/url",
        maxResults: 23,
        defaultColumns: defaultColumnsStrategy
    });
    this.setDefaultColumns();
    var promise = this.columnPickerSparkler.fetchSelectedColumns();
    var setSelectedColumns = sinon.stub(this.columnPickerSparkler, "_setSelectedColumns");

    ok(this.requests.length,
        "It does a request to the server");
    equal(this.requests[0].method, "GET",
        "It does a request GET to the server");
    ok(this.requests[0].url.match("^/default/url"),
        "It does a request to the configured URL");

    // Reject the request
    this.requests[0].respond(404);

    ok(defaultColumnsStrategy.calledOnce,
        "It calls defaultColumnsStrategy() when the response arrives");
    deepEqual(setSelectedColumns.firstCall.args[0], [ { "label":"Label 1", "value": "val1" } ],
        "It calls _setSelectedColumns with the response from the defaultColumns strategy");
    equal(promise.state(), "resolved",
        "It resolves the fetchSelectedColumns promise");


    setSelectedColumns.restore();
});
*/


