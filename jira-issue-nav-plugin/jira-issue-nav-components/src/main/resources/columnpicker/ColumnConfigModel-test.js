AJS.test.require("com.atlassian.jira.jira-issue-nav-components:testutils");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:columnpicker");

module("JIRA.Issues.ColumnConfigModel", {
    setup: function () {
        this.model = new JIRA.Issues.ColumnConfigModel();
    },

    teardown: function() {

    }
});

test("parse() should extract the values from descriptors", function() {
    deepEqual(this.model.parse([{value: 1},{value: 2}]), {columns: [1,2]});
    deepEqual(this.model.parse([]), {columns: []});
    deepEqual(this.model.parse([{}]), {columns: []});
});

test("parse() should return an empty object if the response is not valid", function() {
    deepEqual(this.model.parse(), {});
    deepEqual(this.model.parse(null), {});
});

test("_sortColumnsUsingOriginalOrder() should not modify existing order", function() {
    var currentColumns = ["a", "b", "c", "d", "e"];
    var newColumns = currentColumns;
    var resultColumns = this.model._sortColumnsUsingOriginalOrder(currentColumns, newColumns);

    deepEqual(resultColumns, currentColumns);
});

test("_sortColumnsUsingOriginalOrder() should remove deleted columns", function() {
    var currentColumns = ["a", "b", "c", "d", "e"];
    var newColumns = ["e", "c", "a"];
    var resultColumns = this.model._sortColumnsUsingOriginalOrder(currentColumns, newColumns);

    deepEqual(resultColumns, ["a", "c", "e"]);
});

test("_sortColumnsUsingOriginalOrder() should append new columns at the end", function() {
    var currentColumns = ["a", "b", "c", "d", "e"];
    var newColumns = ["j", "e", "c", "a", "f"];
    var resultColumns = this.model._sortColumnsUsingOriginalOrder(currentColumns, newColumns);

    deepEqual(resultColumns, ["a", "c", "e", "j", "f"]);
});

test("setUnsortedColumns() should set the new columns of the model after sorting them", function() {
    this.model.setColumns(["a", "b", "c", "d", "e"]);

    this.model.setUnsortedColumns(["j", "e", "c", "a", "f"]);

    deepEqual(this.model.getColumns(), ["a", "c", "e", "j", "f"]);
});

test("toJSON() returns a representation of the columns", function() {
    this.model.setColumns(["a", "b", "c", "d", "e"]);

    deepEqual(this.model.toJSON(), {columns: ["a", "b", "c", "d", "e"]});
});

test("getDefaultColumns() returns a promise with the default columns", function() {
    expect(2);
    this.model.defaultColumns = function() {
        var defer = jQuery.Deferred();
        defer.resolve([
            {value: 1},
            {value: 2}
        ]);
        return defer.promise();
    };

    var columnsPromise = this.model.getDefaultColumns();
    ok(columnsPromise.done, "It is a promise");

    columnsPromise.done(function(columns) {
        deepEqual(columns, [1, 2]);
    });
});
