/**
 * Modified by Atlassian
 *
 * Backport of listenTo, listenToOnce and stopListening methods, from
 * Backbone 0.9.9 to Backbone 0.9.2
 *
 * Should remove when we update to Backbone >=0.9.9
 */
;(function() {
    var objectsToPatch = [
        Backbone.View.prototype, Backbone.Events, Backbone.Model.prototype
    ];

    if (!Backbone.Events.listenTo) patch("listenTo","on");
    if (!Backbone.Events.listenToOnce) patch("listenToOnce","once");

    function patch(method, implementation) {
        // Inversion-of-control versions of `on` and `once`. Tell *this* object to
        // listen to an event in another object ... keeping track of what it's
        // listening to.
        _.each(objectsToPatch, function(object) {
            object[method] = function(obj, name, callback) {
                var listeners = this._listeners || (this._listeners = {});
                var id = obj._listenerId || (obj._listenerId = _.uniqueId('l'));
                listeners[id] = obj;
                if (typeof name === 'object') callback = this;
                obj[implementation](name, callback, this);
                return this;
            }
        });
    }

    _.each(objectsToPatch, function(object) {
        // Tell this object to stop listening to either specific events ... or
        // to every object it's currently listening to.
        object["stopListening"] = function(obj, name, callback) {
            var listeners = this._listeners;
            if (!listeners) return this;
            var deleteListener = !name && !callback;
            if (typeof name === 'object') callback = this;
            if (obj) (listeners = {})[obj._listenerId] = obj;
            for (var id in listeners) {
                listeners[id].off(name, callback, this);
                if (deleteListener) delete this._listeners[id];
            }
            return this;
        }
    });

}());
