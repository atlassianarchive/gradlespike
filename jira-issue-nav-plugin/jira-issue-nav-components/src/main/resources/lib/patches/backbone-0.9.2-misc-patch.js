/**
 * Modified by Atlassian
 *
 * Minor patches for Backbone 0.9.2 to mimic 1.0.0 functionality
 *
 * Should remove when we update to Backbone >=0.9.9
 */
;(function() {
    // Backbone.View.remove now calls stopListening()
    Backbone.View.prototype.remove = _.compose(
        function(result) {
            this.stopListening();
            return result;
        },
        Backbone.View.prototype.remove
    );

    // Exposing jQuery to Backbone
    Backbone.$ = AJS.$;
}());
