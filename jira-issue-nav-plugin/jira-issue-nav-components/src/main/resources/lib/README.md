Marionette
==========

We include a patched version of Marionette to provide the noConflict mode. The local version is saved in
`JIRA.Libs.Marionette`.

The base/ directory contains base classes for Marionette, saved in the namespace `JIRA.Marionette`. If your module uses
Marionette, you should extend from those classes. If a Marionette class is missing, please create a base class in
`JIRA.Marionette` instead of extending from `JIRA.Libs.Marionette` (even if your `JIRA.Marionette` class is just an alias
for its counterpart in `JIRA.Libs.Marionette`).

We also provide a few mixins that can be used in several Marionette classes. They are located in
`JIRA.Marionette.Mixins`. To apply a mixin, ensure your base class extends from JIRA.Libs.Marionette (aliasing is
not allowed in this scenario) and use _.extend() to apply the mixins to the class prototype.