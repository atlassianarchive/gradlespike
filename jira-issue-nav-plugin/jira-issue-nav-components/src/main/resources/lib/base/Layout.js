AJS.namespace("JIRA.Marionette.Layout");
JIRA.Marionette.Layout = JIRA.Libs.Marionette.Layout.extend({
    applyToDom: function () {
        this.delegateEvents();
        this.bindUIElements();
        this._reInitializeRegions();
        this.triggerMethod("applyToDom");
    }
});
_.extend(JIRA.Marionette.Layout.prototype, JIRA.Marionette.Mixins);