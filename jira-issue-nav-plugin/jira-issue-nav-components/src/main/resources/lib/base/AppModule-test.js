AJS.test.require("com.atlassian.jira.jira-issue-nav-components:marionette");

module("JIRA.Marionette.AppModule", {
    setup: function () {
        this.sandbox = sinon.sandbox.create();
    },

    startModule: function(appModule, options) {
        this.application = new JIRA.Marionette.Application();
        this.application.module("appModule", new appModule().definition);
        this.application.start(options);
    },

    teardown: function() {
        this.sandbox.restore();
        this.application.stop();
        delete this.application;
    }
});

test("When defining an AppModule, create must have been defined by the subclass", function() {
    var instance = this;
    var myModule = JIRA.Marionette.AppModule.extend({
        name: "test"
    });

    raises(
        function() {
            instance.startModule(myModule);
        },
        /create\(\) must be implemented by AppModules/,
       "Throws an error if create has not been implemented"
    );
});

test("When defining an AppModule, name must have been defined by the subclass", function() {
    var instance = this;
    var myModule = JIRA.Marionette.AppModule.extend({
        create: function() { return {} }
    });

    raises(
        function() {
            instance.startModule(myModule);
        },
        /'name' must be defined by AppModules/,
        "Throws an error if name has not been defined"
    );
});

test("When defining an AppModule, it can create a global request for the internal object", function() {
    var obj = {};
    var myModule = JIRA.Marionette.AppModule.extend({
        create: function() { return obj },
        name: "test",
        generateMasterRequest: true
    });

    this.startModule(myModule);

    deepEqual(this.application.request("test"), obj, "There is a command to retrieve the internal object");
});

test("When defining an AppModule, it pass the arguments to the object factory", function() {
    var spy = this.sandbox.spy();
    var options = {
        data: 42
    };
    var myModule = JIRA.Marionette.AppModule.extend({
        create: spy,
        name: "test",
        generateMasterRequest: true
    });

    this.startModule(myModule, options);

    ok(spy.calledWith(options), "It pass the options to the factory");
});

test("When defining an AppModule, it adds the requests defined as an object", function() {
    var obj = {
        bye: function(world) {
            return "Bye bye, "+ world;
        }
    };
    var myModule = JIRA.Marionette.AppModule.extend({
        create: function() { return obj },
        name: "test",
        requests: {
            hello: function(world) {
                return "Hello, "+world;
            },
            bye: true
        }
    });

    this.startModule(myModule);

    deepEqual(this.application.request("test:hello", "world"), "Hello, world", "The request is defined as a callback");
    deepEqual(this.application.request("test:bye", "world"), "Bye bye, world", "The request is defined as true");
});

test("When defining an AppModule, it adds the requests defined as function", function() {
    var obj = {
        bye: function(world) {
            return "Bye bye, "+ world;
        }
    };
    var myModule = JIRA.Marionette.AppModule.extend({
        create: function() { return obj },
        name: "test",
        requests: function(module) {
            return {
                hello: function(world) {
                    return "Hello, "+world;
                },
                bye: true,
                module: function() {
                    return module;
                }
            }
        }
    });

    this.startModule(myModule);

    deepEqual(this.application.request("test:hello", "world"), "Hello, world", "The request is defined as a callback");
    deepEqual(this.application.request("test:bye", "world"), "Bye bye, world", "The request is defined as true");
    deepEqual(this.application.request("test:module"), obj, "The request receives the module");
});

test("When defining an AppModule, it adds the commands defined as an object", function() {
    var spy1 = this.sandbox.spy();
    var spy2 = this.sandbox.spy();

    var obj = {
        bye: spy1
    };
    var myModule = JIRA.Marionette.AppModule.extend({
        create: function() { return obj },
        name: "test",
        commands: {
            hello: spy2,
            bye: true
        }
    });

    this.startModule(myModule);

    this.application.execute("test:hello");
    this.application.execute("test:bye");

    ok(spy1.calledOnce, "The command defined in the module was executed");
    ok(spy2.calledOnce, "The command defined as a callback was executed");
});

test("When defining an AppModule, it adds the commands defined as a function", function() {
    var spy1 = this.sandbox.spy();
    var spy2 = this.sandbox.spy();
    var spy3 = this.sandbox.spy();

    var obj = {
        bye: spy1
    };
    var myModule = JIRA.Marionette.AppModule.extend({
        create: function() { return obj },
        name: "test",
        commands: function(module) {
            return {
                hello: spy2,
                bye: true,
                module: function() {
                    spy3(module);
                }
            }
        }
    });

    this.startModule(myModule);

    this.application.execute("test:hello");
    this.application.execute("test:bye");
    this.application.execute("test:module");

    ok(spy1.calledOnce, "The command defined in the module was executed");
    ok(spy2.calledOnce, "The command defined as a callback was executed");
    ok(spy3.calledWith(obj), "The command receives the module");
});

test("When defining an AppModule, it listens for the events defined as an array", function() {
    var spy1 = this.sandbox.spy();
    var spy2 = this.sandbox.spy();

    var obj = _.extend(Backbone.Events,{});
    var myModule = JIRA.Marionette.AppModule.extend({
        create: function() { return obj },
        name: "test",
        events: [ "ev1", "ev2"]
    });
    this.startModule(myModule);

    this.application.on("test:ev1", spy1);
    this.application.on("test:ev2", spy2);
    obj.trigger("ev1");
    obj.trigger("ev2");

    ok(spy1.calledOnce, "The event test:ev1 was fired");
    ok(spy2.calledOnce, "The event test:ev2 was fired");
});