AJS.namespace("JIRA.Marionette.InlineDialogView");

/**
 * @class JIRA.Marionette.InlineDialogView
 * @abstract
 *
 * Wraps a AJS.InlineDialog in a Marionette view
 *
 * @extends JIRA.Marionette.InlineDialogView
 */
JIRA.Marionette.InlineDialogView = JIRA.Marionette.ItemView.extend({

    /**
     * Internal AJS.InlineDialog instance.
     * @type {AJS.InlineDialog}
     */
    dialog: null,

    /**
     * Template used by this dialog.
     * @type {Function}
     */
    template: jQuery.noop,

    _getDialogOptions: function() {
        // Get dialog options
        var defaultDialogOptions = {
            noBind: true
        };

        var dialogOptions = JIRA.Libs.Marionette.getOption(this, "dialogOptions") || {};
        if (_.isFunction(dialogOptions)) {
            dialogOptions = dialogOptions.apply(this);
        }

        dialogOptions = _.extend({}, defaultDialogOptions, dialogOptions);
        dialogOptions.hideCallback = _.bind(function() {
            this.close();
        }, this);

        return dialogOptions;
    },

    _getTrigger: function() {
        // Get dialog trigger
        var trigger = JIRA.Libs.Marionette.getOption(this, "trigger");
        if (_.isFunction(trigger)) {
            trigger = trigger.apply(this);
        }
        return trigger;
    },

    /**
     * Generates the internal AJS.InlineDialog.
     *
     * @returns {AJS.InlineDialog} The dialog.
     * @private
     */
    _generateDialog: function() {
        var data = this.serializeData();
        var template = this.getTemplate();
        var dialogOptions = this._getDialogOptions();
        var trigger = this._getTrigger();


        return AJS.InlineDialog(trigger, this.id, function($content, $triggerElement, showPopup) {
            $content.html(template(data));
            showPopup();
        }, dialogOptions);
    },

    /**
     * Main render method.
     *
     * @returns {JIRA.Libs.Marionette.DialogView} This view.
     */
    render: function() {
        this.isClosed = false;

        this.triggerMethod("before:render", this);

        this.dialog = this._generateDialog();
        this.$el = this.dialog;
        this.id = this.dialog.id;
        this.dialog.show();

        this.triggerMethod("render", this);

        return this;
    },

    close: function() {
        if (this.isClosed) {
            return;
        }

        var shouldClose = this.triggerMethod("before:close");
        if (shouldClose === false) {
            return;
        }

        this.dialog.hide();
        this.isClosed = true;

        this.triggerMethod('close');

        // remove the view from the DOM
        this.remove();
    }
});