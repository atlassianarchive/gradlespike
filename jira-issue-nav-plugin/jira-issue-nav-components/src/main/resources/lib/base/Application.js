AJS.namespace("JIRA.Marionette.Application");

/**
 * @class JIRA.Marionette.Application
 * @extends JIRA.Libs.Marionette.Application
 *
 * Main Application for JIRA.
 */
JIRA.Marionette.Application = JIRA.Libs.Marionette.Application.extend({
    /**
     * Override the constructor. We need this dumb method to be able to tests
     * the constructor.
     *
     * @constructor
     * @param {Object} [options] Options to pass to the real Application constructor.
     */
    constructor: function(options) {
        JIRA.Libs.Marionette.Application.call(this, options);
    },

    /**
     * Starts the application
     *
     * @param {Object} [options] Object to pass to all initializer functions and initialize events.
     * @param {Object} [options.forceStop=true] Call stop() on all modules before starting them again.
     */
    start: function(options) {
        if (!options || options.forceStop !== false) {
            this.stop();
        }
        JIRA.Libs.Marionette.Application.prototype.start.call(this, options);
    },

    /**
     * Stops all the modules
     */
    stop: function(){
        _.invoke(this.submodules, "stop");
    }
});