AJS.namespace("JIRA.Marionette.CollectionView");

/**
 * @class JIRA.Marionette.CollectionView
 *
 * @extends JIRA.Libs.Marionette.CollectionView
 */
JIRA.Marionette.CollectionView = JIRA.Libs.Marionette.CollectionView.extend({
    /**
     * Appends an itemView to a container in the specified position
     *
     * @param {jQuery} container Element where the itemView will be inserted
     * @param {JIRA.Libs.Marionette.View} itemView View being added
     * @param {number} index Position of the itemView
     */
    _appendHtmlWithIndex: function(container, itemView, index) {
        var children = container.children();
        var childrenLength = children.length;

        if (index <= 0) {
            // If we want to insert the element at the beginning, just prepend it.
            container.prepend(itemView.$el);
        } else if (!childrenLength || index >= childrenLength) {
            // If the collection has no children, or the desired position is bigger than the number of children,
            // append it to the end
            container.append(itemView.$el);
        } else {
            // Insert the child at the requested index
            itemView.$el.insertBefore(children.eq(index));
        }
    },

    /**
     * Appends an itemView to a collectionView in the specified position
     *
     * This method overwrites Marionette's default. The default implementation always appends
     * the itemView at the end, ignoring the index.
     *
     * See https://github.com/marionettejs/backbone.marionette/blob/master/docs/marionette.collectionview.md#collectionviews-appendhtml
     *
     * @param {JIRA.Libs.Marionette.CollectionView} collectionView This view
     * @param {JIRA.Libs.Marionette.View} itemView View being added
     * @param {number} index Position of the itemView
     */
    appendHtml: function(collectionView, itemView, index) {
        this._appendHtmlWithIndex(collectionView.$el, itemView, index);
    },

    unwrapTemplate: function() {
        JIRA.Marionette.ItemView.prototype.unwrapTemplate.call(this);
    }
});