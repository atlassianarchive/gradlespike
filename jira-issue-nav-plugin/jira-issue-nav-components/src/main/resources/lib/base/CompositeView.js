AJS.namespace("JIRA.Marionette.CompositeView");

/**
 * @class JIRA.Marionette.CompositeView
 *
 * @extends JIRA.Libs.Marionette.CompositeView
 */
JIRA.Marionette.CompositeView = JIRA.Libs.Marionette.CompositeView.extend({
    /**
     * Appends an itemView to a collectionView in the specified position
     *
     * This method overwrites Marionette's default. The default implementation always appends
     * the itemView at the end, ignoring the index.
     *
     * See https://github.com/marionettejs/backbone.marionette/blob/master/docs/marionette.collectionview.md#collectionviews-appendhtml
     *
     * @param {JIRA.Libs.Marionette.CollectionView} collectionView This view
     * @param {JIRA.Libs.Marionette.View} itemView View being added
     * @param {number} index Position of the itemView
     */
    appendHtml: function(collectionView, itemView, index) {
        JIRA.Marionette.CollectionView.prototype._appendHtmlWithIndex(this.getItemViewContainer(collectionView), itemView, index);
    },

    unwrapTemplate: function() {
        JIRA.Marionette.ItemView.prototype.unwrapTemplate.call(this);
    }
});
