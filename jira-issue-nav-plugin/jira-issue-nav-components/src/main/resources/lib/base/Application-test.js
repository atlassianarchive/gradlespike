AJS.test.require("com.atlassian.jira.jira-issue-nav-components:marionette");

module("JIRA.Marionette.Application", {
    setup: function () {
        this.sandbox = sinon.sandbox.create();
    },

    createAppWithModules: function(options) {
        this.app = new JIRA.Marionette.Application(options);
        this.modules = {
            "module1": this.app.module("mock1", function(){}),
            "module2": this.app.module("mock2", function(){})
        };
    },

    teardown: function() {
        this.sandbox.restore();
        delete this.app;
    }
});

test("When creating an application, it creates a new JIRA.Libs.Marionette.Application", function() {
    this.sandbox.spy(JIRA.Libs.Marionette, "Application");

    this.createAppWithModules();

    ok(this.app instanceof JIRA.Libs.Marionette.Application, "The application is an instance of JIRA.Libs.Marionette.Application");
    ok(JIRA.Libs.Marionette.Application.calledWithNew(), "The original constructor was called with 'new'");
});

test("When creating an application, it passes all the arguments to JIRA.Libs.Marionette.Application constructor", function() {
    var options = {test:"a"};
    this.sandbox.spy(JIRA.Libs.Marionette, "Application");

    this.createAppWithModules(options);

    ok(JIRA.Libs.Marionette.Application.alwaysCalledWithExactly(options), "Arguments are passed to the constructor");
});

test("When stopping, it stops all submodules", function() {
    this.createAppWithModules();
    this.sandbox.spy(this.modules.module1, "stop");
    this.sandbox.spy(this.modules.module2, "stop");

    this.app.stop();

    ok(this.modules.module1.stop.calledOnce, "The first module is stopped");
    ok(this.modules.module2.stop.calledOnce, "The second module is stopped");
});

test("When starting, by default it stops all the modules before starting them", function() {
    this.createAppWithModules();
    this.sandbox.spy(this.modules.module1, "stop");
    this.sandbox.spy(this.modules.module2, "stop");

    this.app.start();

    ok(this.modules.module1.stop.calledOnce, "The first module is stopped");
    ok(this.modules.module2.stop.calledOnce, "The second module is stopped");
});

test("When starting, it can skip the stopping of existing modules", function() {
    this.createAppWithModules();
    this.sandbox.spy(this.modules.module1, "stop");
    this.sandbox.spy(this.modules.module2, "stop");

    this.app.start({forceStop: false});

    ok(!this.modules.module1.stop.called, "The first module is not stopped");
    ok(!this.modules.module2.stop.called, "The second module is not stopped");
});