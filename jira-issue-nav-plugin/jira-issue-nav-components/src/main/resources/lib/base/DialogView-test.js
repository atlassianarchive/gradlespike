AJS.test.require("com.atlassian.jira.jira-issue-nav-components:marionette");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:testutils");

module('JIRA.Marionette.DialogView', {
    setup: function() {
    },

    teardown: function() {
    },

    renderDialog: function(dialog) {
        dialog.render();
        JIRA.Issues.Components.TestUtils.moveDialogToQunitFixture(dialog);
    },

    closeDialogWithCancel: function(dialog) {
        dialog.dialog.$popup.find("#aui-dialog-close").click();
    },

    submitDialog: function(dialog) {
        dialog.dialog.$form.submit();
    }
});

test("When rendered, it triggers the before:render event", function() {
    var dialog = new JIRA.Marionette.DialogView();
    var beforeRender = this.spy();
    dialog.on("before:render", beforeRender);

    this.renderDialog(dialog);

    sinon.assert.calledOnce(beforeRender);
});

test("When rendered, it runs the onBeforeRender method", function() {
    var onBeforeRender = this.spy();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        onBeforeRender: onBeforeRender
    }))();
    this.renderDialog(dialog);

    sinon.assert.calledOnce(onBeforeRender);
});

test("When rendered, it triggers the render event", function() {
    var dialog = new JIRA.Marionette.DialogView();
    var render = this.spy();
    dialog.on("render", render);

    this.renderDialog(dialog);

    sinon.assert.calledOnce(render);
});

test("When rendered, it runs the onRender method", function() {
    var onRender = this.spy();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        onRender: onRender
    }))();
    this.renderDialog(dialog);

    sinon.assert.calledOnce(onRender);
});

test("When rendered, it creates a new FormDialog when rendering", function() {
    var dialog = new JIRA.Marionette.DialogView();
    this.spy(JIRA, "FormDialog");
    this.renderDialog(dialog);

    ok(JIRA.FormDialog.calledWithNew());
});

test("When rendered, it passes the id the FormDialog", function() {
    var dialog = new (JIRA.Marionette.DialogView.extend({
        id: "myDialog"
    }))();
    this.spy(JIRA, "FormDialog");
    this.renderDialog(dialog);

    var generatedDialog = JIRA.FormDialog.firstCall.thisValue;
    equal(generatedDialog.options.id, "myDialog");
});

test("When rendered, it uses the dialogOptions to create the FormDialog", function() {
    var dialog = new (JIRA.Marionette.DialogView.extend({
        dialogOptions: this.stub().returns({randomOption: 42})
    }))();
    this.spy(JIRA, "FormDialog");

    this.renderDialog(dialog);
    var generatedDialog = JIRA.FormDialog.firstCall.thisValue;
    equal(generatedDialog.options.randomOption, "42");
});

test("When rendered, it uses the dialogOptions to override default properties for the FormDialog", function() {
    var dialog = new (JIRA.Marionette.DialogView.extend({
        id: "myDialog",
        dialogOptions: this.stub().returns({id: "myRealId"})
    }))();
    this.spy(JIRA, "FormDialog");

    this.renderDialog(dialog);
    var generatedDialog = JIRA.FormDialog.firstCall.thisValue;
    equal(generatedDialog.options.id, "myRealId");
});

test("When rendered, it exposes the internal FormDialog in the dialog property", function() {
    var dialog = new JIRA.Marionette.DialogView();

    this.renderDialog(dialog);

    ok(dialog.dialog instanceof JIRA.FormDialog);
});

test("When rendered, It serialises the data from the model", function() {
    var model = { toJSON: this.spy() };
    var dialog = new JIRA.Marionette.DialogView({ model: model });

    this.renderDialog(dialog);

    sinon.assert.calledOnce(model.toJSON);
});

test("When rendered, it uses the template to generate the dialog's content", function() {
    var template = this.spy();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        template: template
    }))();

    this.renderDialog(dialog);

    sinon.assert.calledOnce(template);
});

test("When rendered, it displays the dialog", function() {
    var dialog = new JIRA.Marionette.DialogView();
    this.stub(JIRA.FormDialog.prototype, "show");

    this.renderDialog(dialog);

    sinon.assert.calledOnce(JIRA.FormDialog.prototype.show);
});

test("When rendered, it captures the main element", function() {
    var dialog = new JIRA.Marionette.DialogView();
    this.renderDialog(dialog);

    equal(dialog.$el.length, 1);
});

test("When closed, it closes the view if the dialog has been closed because the 'Cancel' link", function() {
    var dialog = new JIRA.Marionette.DialogView();
    this.spy(JIRA.Marionette.DialogView.prototype, "close");
    this.renderDialog(dialog);

    this.closeDialogWithCancel(dialog);

    sinon.assert.calledOnce(JIRA.Marionette.DialogView.prototype.close);
});

test("When closed, it triggers the before:close event", function() {
    var dialog = new JIRA.Marionette.DialogView();
    var beforeClose = this.spy();
    dialog.on("before:close", beforeClose);
    this.renderDialog(dialog);

    dialog.close();

    sinon.assert.calledOnce(beforeClose);
});

test("When closed, it triggers the close event", function() {
    var dialog = new JIRA.Marionette.DialogView();
    var close = this.spy();
    dialog.on("close", close);
    this.renderDialog(dialog);

    dialog.close();

    sinon.assert.calledOnce(close);
});

test("When closed, it runs the onBeforeClose method", function() {
    var onBeforeClose = this.spy();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        onBeforeClose: onBeforeClose
    }))();
    this.renderDialog(dialog);

    dialog.close();

    sinon.assert.calledOnce(onBeforeClose);
});

test("When closed, it runs the onClose method", function() {
    var onClose = this.spy();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        onClose: onClose
    }))();
    this.renderDialog(dialog);

    dialog.close();

    sinon.assert.calledOnce(onClose);
});

test("When closed, it stops the closing action when instructed by the beforeClose method", function() {
    var onBeforeClose = this.stub().returns(false);
    var onClose = this.spy();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        onBeforeClose: onBeforeClose,
        onClose: onClose
    }))();
    this.renderDialog(dialog);

    dialog.close();

    sinon.assert.calledOnce(onBeforeClose);
    sinon.assert.notCalled(onClose);
});

test("When closed, it hides the dialog", function() {
    var dialog = new JIRA.Marionette.DialogView();
    this.stub(JIRA.FormDialog.prototype, "hide");
    this.renderDialog(dialog);

    dialog.close();

    sinon.assert.calledWith(JIRA.FormDialog.prototype.hide, true);
});

test("When closed, it destroys the dialog", function() {
    var dialog = new JIRA.Marionette.DialogView();
    this.stub(JIRA.FormDialog.prototype, "destroy");
    this.renderDialog(dialog);

    dialog.close();

    sinon.assert.called(JIRA.FormDialog.prototype.destroy);
});

test("When closed, it removes itself from the DOM", function() {
    var dialog = new JIRA.Marionette.DialogView();
    this.spy(JIRA.Marionette.DialogView.prototype, "remove");
    this.renderDialog(dialog);

    dialog.close();

    sinon.assert.calledOnce(JIRA.Marionette.DialogView.prototype.remove);
});

test("When submitting, it delegates the form data generation to the formToRequestData method", function() {
    var formToRequestData = this.spy();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        formToRequestData: formToRequestData,
        template: function() {
            return "<form></form>";
        }
    }))();
    this.renderDialog(dialog);

    this.submitDialog(dialog);

    sinon.assert.calledOnce(formToRequestData);
});

test("When submitting, it passes the form element to the formToRequestData method", function() {
    var formToRequestData = this.spy();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        formToRequestData: formToRequestData,
        template: function() {
            return "<form></form>";
        }
    }))();
    this.renderDialog(dialog);

    this.submitDialog(dialog);

    sinon.assert.calledWith(formToRequestData, dialog.dialog.$form);
});

test("When submitting the form successfully, it triggers the submit:success event", function() {
    this.server = this.sandbox.useFakeServer();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        template: function() {
            return "<form method='POST'></form>";
        }
    }))();
    var submitSuccess = this.spy();
    dialog.on("submit:success", submitSuccess);
    this.renderDialog(dialog);

    this.submitDialog(dialog);
    _.last(this.server.requests).respond(200);

    sinon.assert.calledOnce(submitSuccess);
});

test("When submitting the form successfully, it runs the onSubmitSuccess method", function() {
    this.server = this.sandbox.useFakeServer();
    var onSubmitSuccess = this.spy();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        template: function() {
            return "<form method='POST'></form>";
        },
        onSubmitSuccess: onSubmitSuccess
    }))();
    this.renderDialog(dialog);

    this.submitDialog(dialog);
    _.last(this.server.requests).respond(200);

    sinon.assert.calledOnce(onSubmitSuccess);
});

test("When submitting the form successfully, it closes the form", function() {
    this.server = this.sandbox.useFakeServer();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        template: function() {
            return "<form method='POST'></form>";
        }
    }))();
    var close = this.spy();
    dialog.on("close", close);
    this.renderDialog(dialog);

    this.submitDialog(dialog);
    _.last(this.server.requests).respond(200);

    sinon.assert.calledOnce(close);
});

test("When submitting the form unsuccessfully, it triggers the submit:error event", function() {
    this.server = this.sandbox.useFakeServer();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        template: function() {
            return "<form method='POST'></form>";
        }
    }))();
    var submitError = this.spy();
    dialog.on("submit:error", submitError);
    this.renderDialog(dialog);

    this.submitDialog(dialog);
    _.last(this.server.requests).respond(500);

    sinon.assert.calledOnce(submitError);
});

test("When submitting the form unsuccessfully, it runs the onSubmitError method", function() {
    this.server = this.sandbox.useFakeServer();
    var onSubmitError = this.spy();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        template: function() {
            return "<form method='POST'></form>";
        },
        onSubmitError: onSubmitError
    }))();
    this.renderDialog(dialog);

    this.submitDialog(dialog);
    _.last(this.server.requests).respond(500);

    sinon.assert.calledOnce(onSubmitError);
});

test("When submitting the form unsuccessfully, it does not close the form", function() {
    this.server = this.sandbox.useFakeServer();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        template: function() {
            return "<form method='POST'></form>";
        }
    }))();
    var close = this.spy();
    dialog.on("close", close);
    this.renderDialog(dialog);

    this.submitDialog(dialog);
    _.last(this.server.requests).respond(500);

    sinon.assert.notCalled(close);
});

test("When submitting the form unsuccessfully, it clean ups the DOM", function() {
    this.server = this.sandbox.useFakeServer();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        template: function() {
            return "<form method='POST'></form>";
        }
    }))();
    this.renderDialog(dialog);

    this.submitDialog(dialog);
    _.last(this.server.requests).respond(500);

    equal(dialog.dialog.getButtonsContainer().find('input:disabled').length, 0, "No disabled buttons");
    equal(dialog.dialog.getButtonsContainer().find('.throbber.loading').length, 0, "No spinner");
    ok(!dialog.dialog.$form.hasClass("submitting"), "No submitting class");
});

test("When submitting the form unsuccessfully, it shows the field errors", function() {
    this.server = this.sandbox.useFakeServer();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        template: function() {
            return "<form method='POST'><input id='name'/></form>";
        }
    }))();
    this.renderDialog(dialog);

    this.submitDialog(dialog);
    _.last(this.server.requests).respond(500, {}, JSON.stringify({errors: {
        name: "Field is duplicated"
    }}));

    equal(dialog.dialog.$form.find(".error").text(), "Field is duplicated");
});

test("When submitting the form unsuccessfully, it shows the generic errors", function() {
    this.server = this.sandbox.useFakeServer();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        template: function() {
            return "<form method='POST'><div class='aui-messages'></div></form>";
        }
    }))();
    this.renderDialog(dialog);

    this.submitDialog(dialog);
    _.last(this.server.requests).respond(500, {}, JSON.stringify({errorMessages: [
        "Server is down"
    ]}));

    equal(dialog.dialog.$form.find(".aui-messages").text(), "Server is down");
});

test("When submitting the form unsuccessfully, it shows the XHR errors", function() {
    this.server = this.sandbox.useFakeServer();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        template: function() {
            return "<form method='POST'><div class='aui-messages'></div></form>";
        }
    }))();
    this.renderDialog(dialog);

    this.submitDialog(dialog);
    _.last(this.server.requests).respond(500);

    equal(dialog.dialog.$form.find(".aui-messages").find(".ajaxerror").length, 1);
});

test("When submitting the form unsuccessfully twice, it clean the old field errors", function() {
    this.server = this.sandbox.useFakeServer();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        template: function() {
            return "<form method='POST'><input id='name'/></form>";
        }
    }))();
    this.renderDialog(dialog);

    this.submitDialog(dialog);
    _.last(this.server.requests).respond(500, {}, JSON.stringify({errors: {
        name: "Field is duplicated"
    }}));
    equal(dialog.dialog.$form.find(".error").text(), "Field is duplicated", "First error is rendered");

    this.submitDialog(dialog);
    _.last(this.server.requests).respond(500, {}, JSON.stringify({errors: {
        name: "Field is duplicated dude!"
    }}));

    equal(dialog.dialog.$form.find(".error").text(), "Field is duplicated dude!", "The second error overwrites the first one");
});

test("When submitting the form unsuccessfully twice, it clean the old generic errors", function() {
    this.server = this.sandbox.useFakeServer();
    var dialog = new (JIRA.Marionette.DialogView.extend({
        template: function() {
            return "<form method='POST'><div class='aui-messages'></div></form>";
        }
    }))();
    this.renderDialog(dialog);

    this.submitDialog(dialog);
    _.last(this.server.requests).respond(500, {}, JSON.stringify({errorMessages: [
        "Server is down"
    ]}));
    this.submitDialog(dialog);
    _.last(this.server.requests).respond(500, {}, JSON.stringify({errorMessages: [
        "Server is still down"
    ]}));

    equal(dialog.dialog.$form.find(".aui-messages").text(), "Server is still down");
});
