AJS.namespace("JIRA.Marionette.ItemView");

/**
 * @class JIRA.Marionette.ItemView
 *
 * @extends JIRA.Libs.Marionette.ItemView
 */
JIRA.Marionette.ItemView = JIRA.Libs.Marionette.ItemView.extend({

    renderTemplate: function() {
        var data = this.serializeData();
        data = this.mixinTemplateHelpers(data);

        var template = this.getTemplate();
        var html = JIRA.Libs.Marionette.Renderer.render(template, data);

        return JIRA.Libs.Marionette.$(html);
    },

    applyToDom: function () {
        this.delegateEvents();
        this.bindUIElements();
        this.triggerMethod("applyToDom");
    },

    /**
     * This method unwraps the Backbone.View.
     *
     * By default, Backbone will create a <div> and render the template inside. By calling this
     * method, you can get rid of that <div>, so the main element in your template will be the
     * root element in your template.
     */
    unwrapTemplate: function() {
        if (this.$el.parent().length) {
            // If the template is already rendered in the page
            var children = this.$el.children();
            this.$el.replaceWith(children);
            this.setElement(children);
        } else {
            // If the template is in memory
            this.setElement(this.$el.children());
        }
    }
});