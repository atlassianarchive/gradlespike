AJS.namespace("JIRA.Marionette.DialogView");

/**
 * @class JIRA.Marionette.DialogView
 * @abstract
 *
 * Wraps a JIRA.FormDialog in a Marionette view
 *
 * @extends JIRA.Marionette.ItemView
 */
JIRA.Marionette.DialogView = JIRA.Marionette.ItemView.extend({

    /**
     * Internal JIRA.FormDialog instance.
     * @type {JIRA.FormDialog}
     */
    dialog: null,

    /**
     * Template used by this dialog.
     * @type {Function}
     */
    template: jQuery.noop,

    /**
     * Generates the internal JIRA.FormDialog.
     *
     * @returns {JIRA.FormDialog} The dialog.
     * @private
     */
    _generateDialog: function() {
        var data = this.serializeData();
        var template = this.getTemplate();
        var defaultDialogOptions = {
            id: this.id,
            content: function(cb) {
                cb(template(data));
            },
            submitHandler: _.bind(this._generateSubmissionXHR, this)
        };
        var dialogOptions = JIRA.Libs.Marionette.getOption(this, "dialogOptions");
        if (_.isFunction(dialogOptions)) {
            dialogOptions = dialogOptions.apply(this);
        }

        return new JIRA.FormDialog(_.extend(defaultDialogOptions, dialogOptions));
    },

    /**
     * Main render method.
     *
     * @returns {JIRA.Libs.Marionette.DialogView} This view.
     */
    render: function() {
        this.isClosed = false;

        this.triggerMethod("before:render", this);

        this.dialog = this._generateDialog();
        this.dialog.show();
        this.$el = this.dialog.get$popup();

        jQuery(this.dialog).on("Dialog.hide", _.bind(function(e, popup, reason) {
            if (reason === JIRA.Dialog.HIDE_REASON.cancel || reason === JIRA.Dialog.HIDE_REASON.escape) {
                this.close();
            }
        }, this));

        this.triggerMethod("render", this);

        return this;
    },

    close: function() {
        if (this.isClosed) {
            return;
        }

        var shouldClose = this.triggerMethod("before:close");
        if (shouldClose === false) {
            return;
        }

        jQuery(this.dialog).off("Dialog.hide");

        this.dialog.hide(true);
        this.dialog.destroy();
        this.isClosed = true;

        this.triggerMethod('close');

        // remove the view from the DOM
        this.remove();
    },

    /**
     * Constructs the XHR request used to submit the form data.
     *
     * @param {jQuery.Event} e Submit event.
     * @param {Function} ready  Callback to run when the form has been submitted.
     * @returns {jQuery.jqXHR} Request used to submit the form.
     * @private
     */
    _generateSubmissionXHR: function(e, ready) {
        e.preventDefault();

        var form = this.dialog.$form;
        var formData = JIRA.Libs.Marionette.getOption(this, "formToRequestData");
        if (_.isFunction(formData)) {
            formData = formData.call(this, form);
        }

        var request = JIRA.SmartAjax.makeRequest({
            type: form.attr("method"),
            url: form.attr('action'),
            contentType: 'application/json',
            dataType: "json",
            processData: false,
            data: JSON.stringify(formData)
        })
            .done(ready)
            .done(_.bind(this._onSubmitSuccess, this))
            .fail(_.bind(this._onSubmitError, this));

        return request;
    },

    /**
     * Handler for when the submission XHR has succeeded.
     *
     * @param {object} serverResponse Response returned by the server.
     * @private
     */
    _onSubmitSuccess: function(serverResponse) {
        this.triggerMethod('submit:success', serverResponse);
        this.close();
    },

    /**
     * Handler for when the submission XHR has failed.
     *
     * @param {jQuery.jqXHR} xhr XHR that failed.
     * @private
     */
    _onSubmitError: function(xhr) {
        this.dialog.getButtonsContainer().find('input').removeAttr('disabled');
        this.dialog.hideFooterLoadingIndicator();
        this.dialog.$form.removeClass("submitting");
        this._showErrorFromXHR(xhr);
        this.triggerMethod('submit:error');
    },

    /**
     * Display the errors related to fields (e.g. a field is required)
     *
     * @param {object} errors List of errors, in the form {fieldName: errorMessage}
     * @private
     */
    _displayFieldErrors: function(errors) {
        var popup = this.dialog.$popup;
        _.each(errors, function(message, fieldName) {
            popup.find("#" + fieldName).after("<div class='error'>" + AJS.escapeHTML(message) + "</div>");
        });
    },

    /**
     * Display the generic errors (e.g. the server returned a 500)
     *
     * @param {string[]} errorMessages List of error messages to show
     * @private
     */
    _displayGenericErrors: function(errorMessages) {
        var errorContainer = this.dialog.$popup.find(".aui-messages");

        _.each(errorMessages, function(message) {
            var messageOptions = {
                closeable: true,
                timeout: 5
            };
            messageOptions.body = AJS.escapeHTML(message);
            messageOptions.shadowed = false;
            AJS.messages.error(errorContainer, messageOptions);
        });
    },

    /**
     * Display XHR errors (e.g. network timeout)
     *
     * @param {jQuery.jqXHR} xhr Request that caused the error
     * @private
     */
    _displayXHRErrors: function(xhr) {
        var errorContainer = this.dialog.$popup.find(".aui-messages");
        errorContainer.append(JIRA.SmartAjax.buildDialogErrorContent(xhr, true));
    },

    /**
     * Generate errors from the XHR response and append them to the form
     *
     * @param {jQuery.jqXHR} xhr
     */
    _showErrorFromXHR: function(xhr) {
        this._removeErrorMessages();

        var errors;
        try {
            errors = JSON.parse(xhr.responseText);
        } catch (e) {
            errors = null;
        }

        if (errors) {
            this._displayFieldErrors(errors.errors);
            this._displayGenericErrors(errors.errorMessages);
        } else {
            this._displayXHRErrors(xhr);
        }
    },

    /**
     * @private
     */
    _removeErrorMessages: function() {
        // Remove field errors
        this.dialog.$popup.find(".error").remove();

        // Remove generic errors
        this.dialog.$popup.find(".aui-messages").empty();
    }
});