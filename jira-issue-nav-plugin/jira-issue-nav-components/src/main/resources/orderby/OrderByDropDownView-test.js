AJS.test.require("com.atlassian.jira.jira-issue-nav-components:orderby");

module('JIRA.Issues.OrderByDropDownView', {
    setup: function () {
        this.sandbox = sinon.sandbox.create();
        this.clock = this.sandbox.useFakeTimers();

        this.view = new JIRA.Issues.OrderByDropDownView({
            model: new JIRA.Issues.OrderByModel(),
            //offsetTarget: this.$('a.order-by'),
            onHideCallback: sinon.stub()
        });
        this.view.selectControl = {};
    },
    teardown: function () {
        this.sandbox.restore();
        delete this.view;
    }
});

test("Sort event is being prevented correctly", function() {
    var mockEvent = {
        preventDefault: AJS.$.noop
    };
    var mockDescriptior = {
        meta: function() {
            return { sortJql: true }
        }
    }

    AJS.InlineLayer.current = { hide: this.sandbox.stub() };
    this.sandbox.stub(JIRA.Issues.OrderByModel.prototype, "doSort");

    JIRA.bind(JIRA.Events.ISSUE_TABLE_REORDER, AJS.$.noop);
    this.view._onFieldSelected(mockEvent, mockDescriptior);
    this.clock.tick(10);
    equal(this.view.model.doSort.callCount, 1, "Default behavior is executed as expected");

    JIRA.bind(JIRA.Events.ISSUE_TABLE_REORDER, function(e) {
        e.preventDefault();
    });
    this.view._onFieldSelected(mockEvent, mockDescriptior);
    this.clock.tick(10);
    equal(this.view.model.doSort.callCount, 1, "Default behavior is prevented successfully");
});
