/**
 * Factory to create order by controls
 */
JIRA.Events.ISSUE_TABLE_REORDER = "issueTableReorder";

JIRA.Components.OrderBy = {

    create: function (options) {

        var model, view;

        options = options || {};

        model = new JIRA.Issues.OrderByModel({
            sortBy: options.sortBy,
            jql: options.jql
        });
        view = new JIRA.Issues.OrderByView({model: model});

        // publish public api
        return {
            onSort: function () {
                model.onSort.apply(model, arguments);
                return this;
            },
            offSort: function (method) {
                model.off("sort", method);
                return this;
            },
            render: function () {
                view.render();
                return this;
            },
            setElement: function (el) {
                view.setElement(el);
                return this;
            },
            setJql: function (jql) {
                model.setJql(jql);
                return this;
            },
            setSortBy: function (options) {
                model.setSortBy(options);
                return this;
            }
        };
    }
};