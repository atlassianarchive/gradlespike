AJS.test.require("com.atlassian.jira.jira-issue-nav-components:orderby");

module('JIRA.Issues.OrderByView', {
    setup: function () {
        this.sandbox = sinon.sandbox.create();
        this.view = new JIRA.Issues.OrderByView({
            model: new JIRA.Issues.OrderByModel()
        });
    },
    teardown: function () {
        this.sandbox.restore();
        delete this.view;
    }
});

test("Sort event is being prevented correctly", function() {
    var mockEvent = {
        preventDefault: AJS.$.noop,
        currentTarget: AJS.$('<div data-field-id="test"></div>')
    }

    this.sandbox.stub(JIRA.Issues.OrderByModel.prototype, "toggleSort");

    JIRA.bind(JIRA.Events.ISSUE_TABLE_REORDER, AJS.$.noop);
    this.view._onClickOrderBy(mockEvent);
    equal(JIRA.Issues.OrderByModel.prototype.toggleSort.callCount, 1, "Default behavior is executed as expected");

    JIRA.bind(JIRA.Events.ISSUE_TABLE_REORDER, function(e) {
        e.preventDefault();
    });
    this.view._onClickOrderBy(mockEvent);
    equal(JIRA.Issues.OrderByModel.prototype.toggleSort.callCount, 1, "Default behavior is prevented successfully");
});
