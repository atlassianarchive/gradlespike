AJS.namespace("JIRA.Components.Analytics.AppModule");

JIRA.Components.Analytics.AppModule = JIRA.Marionette.AppModule.extend({
    name: "analytics",
    generateMasterRequest: true,

    /**
     * @param {Object} options Options object
     * @param {boolean} [options.useLog=false] Whether this module should log events using AJS.log()
     */
    create: function(options) {
        options = _.defaults({}, options, {
            useLog: false
        });

        return new JIRA.Components.Analytics(options);
    },

    commands: {
        trigger: true,
        registerEvent: true,
        registerTracker: true
    }
});