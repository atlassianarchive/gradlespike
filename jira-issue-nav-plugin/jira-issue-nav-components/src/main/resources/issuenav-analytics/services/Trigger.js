AJS.namespace("JIRA.Components.Analytics.Services.Trigger");

/**
 * @class JIRA.Components.Analytics.Services.Trigger
 *
 * This service is responsible for triggering analytic events. It will inject the new events into the global array
 * AJS.EventQueue
 *
 * @extends JIRA.Marionette.Controller
 */
JIRA.Components.Analytics.Services.Trigger = JIRA.Marionette.Controller.extend({
    /**
     * @param {Object} options Options object
     * @param {boolean} [options.useLog=false] Whether this module should log events using AJS.log()
     *
     * @constructor
     */
    initialize: function(options) {
        this.useLog = options.useLog;
        this.allowedEvents = [];
        this.startTime = new Date().getTime();
        this.pageSessionGuid =  Math.random().toString(36).substring(2) + Math.random().toString(36).substring(2);
    },

    /**
     * Triggers an event
     *
     * @param {string} name Name of the event
     * @param {Object} [parameters] Arbitrary data to include in the event
     */
    trigger: function(name, parameters) {
        // No EventQueue, nothing to do.
        if (!AJS.EventQueue) return;

        // Event has not been registered, ignore it.
        if (!_.contains(this.allowedEvents, name)) return;

        parameters = parameters || {};

        // Add GUID and time to the parameters
        parameters.context_pageSession = this.pageSessionGuid;
        parameters.context_pageTime = new Date().getTime() - this.startTime;

        // Register an analytics object for this event.
        AJS.EventQueue.push({ name: name, properties: parameters });

        if (this.useLog) {
            var logMsg = "***** Analytics log [" + name + "]";
            logMsg += "[" + JSON.stringify(parameters) + "]";
            AJS.log(logMsg);
        }
    },

    /**
     * Registers an event. This allows this event to be triggered later.
     *
     * @param {string|string[]} name Event (or list of events) to register
     */
    registerEvent: function(name) {
        this.allowedEvents.push(name);
    }
});