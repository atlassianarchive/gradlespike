AJS.test.require("com.atlassian.jira.jira-issue-nav-components:analytics");

module('JIRA.Components.Analytics.Services.DomTracker', {

    setup: function() {
        this.triggerService = new JIRA.Components.Analytics.Services.Trigger();
        this.trackerService = new JIRA.Components.Analytics.Services.DomTracker(this.triggerService);
        sinon.stub(this.triggerService, "trigger");

        this.qunitFixture = AJS.$("#qunit-fixture");
        this.eventReceiver = AJS.$("<div id='event-receiver'></div>");
        this.qunitFixture = this.qunitFixture.append(this.eventReceiver);
    },

    teardown: function() {
        this.trackerService.clear();
    }
});

test("When registering a tracker, it fires the analytics event when the DOM event is fired", function() {
    this.trackerService.registerTracker({
        type: "click",
        context: this.qunitFixture,
        selector: "#event-receiver",
        name: "testEvent"
    });

    this.eventReceiver.click();

    ok(this.triggerService.trigger.calledOnce, "One event has been fired");
    ok(this.triggerService.trigger.firstCall.args[0], "The event 'testEvent' has been fired");
});

test("By default, a tracker uses the click event", function() {
    this.trackerService.registerTracker({
        context: this.qunitFixture,
        selector: "#event-receiver",
        name: "testEvent"
    });

    this.eventReceiver.click();

    ok(this.triggerService.trigger.calledOnce, "One event has been fired");
    ok(this.triggerService.trigger.firstCall.args[0], "The event 'testEvent' has been fired");
});


test("By default, a tracker is append to the main document", function() {
    this.trackerService.registerTracker({
        selector: "#event-receiver",
        name: "testEvent"
    });

    this.eventReceiver.click();

    ok(this.triggerService.trigger.calledOnce, "One event has been fired");
    ok(this.triggerService.trigger.firstCall.args[0], "The event 'testEvent' has been fired");
});

test("Trackers can be prevented", 1, function() {
    var spy = sinon.spy();

    this.trackerService.registerTracker({
        context: this.qunitFixture,
        selector: "#event-receiver",
        name: "testEvent1",
        handler: function(e) {
            e.preventFurtherAnalytics = true;
            spy();
        }
    });

    this.trackerService.registerTracker({
        context: this.qunitFixture,
        selector: "#event-receiver",
        name: "testEvent1",
        handler: function(e) {
            spy();
        }
    });

    this.eventReceiver.click();

    ok(spy.calledOnce, "Only the first event is triggered");
});

test("Handler receives multiple custom parameters", function() {
    var parameters = {
        testParam: "test value"
    };
    var secondParameters = {
        anotherTestParam: "Test Value"
    };

    this.trackerService.registerTracker({
        context: this.qunitFixture,
        selector: "#event-receiver",
        name: "testEvent1",
        handler: function(e, param1, param2) {
            deepEqual(param1, parameters, "First parameters are sent");
            deepEqual(param2, secondParameters, "Second parameters are sent");
        }
    });

    this.eventReceiver.trigger('click', [parameters, secondParameters]);
});

test("Clears all previous registered trackers", function() {
    this.trackerService.registerTracker({
        context: this.qunitFixture,
        selector: "#event-receiver",
        name: "testEvent1"
    });
    this.trackerService.registerTracker({
        context: this.qunitFixture,
        selector: "#event-receiver",
        name: "testEvent2"
    });
    this.trackerService.clear();
    this.eventReceiver.click();

    ok(!this.triggerService.trigger.called, "No event has been fired");
});
