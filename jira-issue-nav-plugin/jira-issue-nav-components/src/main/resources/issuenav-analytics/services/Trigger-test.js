AJS.test.require("com.atlassian.jira.jira-issue-nav-components:analytics");

module('JIRA.Components.Analytics.Services.Trigger', {

    setup: function() {
        this.sandbox = sinon.sandbox.create();

        this.oldEventQueue = AJS.EventQueue;
        AJS.EventQueue = [];
    },

    teardown: function () {
        this.sandbox.restore();
        AJS.EventQueue = this.oldEventQueue;
    }
});

test("Each instance of the service has a different GUID", function() {
    this.sandbox.spy(Math, "random");

    new JIRA.Components.Analytics.Services.Trigger();

    ok(Math.random.called, "It calls Math.random to get a random id");
});

test("Triggering an event adds it to the queue", function() {
    var trigger = new JIRA.Components.Analytics.Services.Trigger();
    trigger.registerEvent("test");
    trigger.trigger("test");

    equal(AJS.EventQueue.length, 1, "There is one event in the queue");
    equal(AJS.EventQueue[0].name, "test", "The triggered event is in the queue");
});

test("Triggering an events adds info about the time", function() {
    this.clock = this.sandbox.useFakeTimers(123);

    var trigger = new JIRA.Components.Analytics.Services.Trigger();
    trigger.registerEvent("test");
    this.clock.tick(100);
    trigger.trigger("test");

    equal(AJS.EventQueue[0].properties.context_pageTime, 100, "The event logs the time");
});

test("Triggering an event with log enabled outputs it in the console", function() {
    this.sandbox.stub(AJS,"log");

    var trigger = new JIRA.Components.Analytics.Services.Trigger({useLog: true});
    trigger.registerEvent("test");
    trigger.trigger("test");

    ok(AJS.log.calledOnce, "The event is logged in the console");
    equal(AJS.log.firstCall.args[0].indexOf('***** Analytics log [test]'), 0, "The right log info is displayed in the console");
});

test("Triggering an event with log disabled outputs noting in the console", function() {
    this.sandbox.stub(AJS,"log");

    var trigger = new JIRA.Components.Analytics.Services.Trigger({useLog: false});
    trigger.registerEvent("test");
    trigger.trigger("test");

    ok(!AJS.log.called, "AJS.log is not called");
});

test("The events contain an unique GUID", function() {
    var trigger = new JIRA.Components.Analytics.Services.Trigger();
    trigger.registerEvent("test");
    trigger.trigger("test");

    equal(AJS.EventQueue[0].properties.context_pageSession, trigger.pageSessionGuid, "The event contains an unique id");
});

test("The events contain the passed since the page was opened", function() {
    this.sandbox.useFakeTimers();

    var trigger = new JIRA.Components.Analytics.Services.Trigger();
    this.sandbox.clock.tick(100);
    trigger.registerEvent("test");
    trigger.trigger("test");

    equal(AJS.EventQueue[0].properties.context_pageTime, 100, "The event contains the time delta");
});

test("The events contain any arbitrary parameter", function() {
    var trigger = new JIRA.Components.Analytics.Services.Trigger();
    trigger.registerEvent("test");
    trigger.trigger("test", {data: true, test: 42});

    equal(AJS.EventQueue[0].properties.data, true, "The event contains the arbitrary values for parameters (1)");
    equal(AJS.EventQueue[0].properties.test, 42,   "The event contains the arbitrary values for parameters (2)");
});

test("If the event is not registered, ignore it", function() {
    var trigger = new JIRA.Components.Analytics.Services.Trigger();
    trigger.trigger("test");

    equal(AJS.EventQueue.length, 0, "The event queue is empty");
});

test("If theres is no queue, ignore all the events", function() {
    delete AJS.EventQueue;

    var trigger = new JIRA.Components.Analytics.Services.Trigger();
    trigger.trigger("test");

    ok(!("EventQueue" in AJS),"There is no Event queue");
});