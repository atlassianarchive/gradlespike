AJS.namespace("JIRA.Components.Analytics.Services.DomTracker");

/**
 * @class JIRA.Components.Analytics.Services.DomTracker
 *
 * This service is responsible for listening to DOM Trackers. A DOM Tracker is an event attached to the DOM
 * (usually to document) that will trigger an Analytic event when some DOM events happens.
 *
 * @extends JIRA.Marionette.Controller
 */
JIRA.Components.Analytics.Services.DomTracker = JIRA.Marionette.Controller.extend({
    /**
     * @constructor
     *
     * @param {JIRA.Components.Analytics.Services.Trigger} triggerService Service used for triggering the DOM events
     */
    initialize: function(triggerService) {
        this.triggerService = triggerService;
        this.trackers = [];
    },

    /**
     * Registers a DOM tracker.
     *
     * @param {Object} tracker
     * @param {string} tracker.name Analytic event name to fire
     * @param {string} tracker.selector Selector to use for detect the DOM event
     * @param {string} [tracker.type="click"] DOM Event to listen for
     * @param {Function} [tracker.handler] Handler for that event
     * @param {DOMElement} [tracker.context=document] Element used to listen for the event
     */
    registerTracker: function(tracker) {
        var triggerService = this.triggerService;

        // Set default values for the tracker
        _.defaults(tracker, {
            type: "click",
            handler: function(e, props) {
                // This is a copy of the old ClientAnalytics code, but makes no sense to allow other handlers here
                triggerService.trigger(this.name, props)
            },
            context: document
        });

        // Register the tracker
        triggerService.registerEvent(tracker.name);

        tracker.domHandler = function(e) {
            if (!e.preventFurtherAnalytics) {
                // This is a copy of the old ClientAnalytics code, but this arguments call looks odd.
                tracker.handler.apply(tracker, Array.prototype.slice.call(arguments));
            }
        };

        // Add the DOM tracker
        AJS.$(tracker.context).on(tracker.type, tracker.selector, tracker.domHandler);

        this.trackers.push(tracker);
    },

    clear: function() {
        _.each(this.trackers, function(tracker) {
            AJS.$(tracker.context).off(tracker.type, tracker.selector, tracker.domHandler);
        });
    }
});
