AJS.test.require("com.atlassian.jira.jira-issue-nav-components:analytics");

module('JIRA.Components.Analytics', {
    setup: function () {
        this.sandbox = sinon.sandbox.create();
        this.module = new JIRA.Components.Analytics();

        this.oldEventQueue = AJS.EventQueue;
        AJS.EventQueue = [];
    },

    teardown: function () {
        this.sandbox.restore();
        AJS.EventQueue = this.oldEventQueue;
    }
});

test("It passes the options to the Trigger service", function() {
    this.sandbox.stub(JIRA.Components.Analytics.Services, "Trigger");

    var options = {};
    new JIRA.Components.Analytics(options);

    ok(JIRA.Components.Analytics.Services.Trigger.calledOnce, "The service is constructed");
    ok(JIRA.Components.Analytics.Services.Trigger.calledWith(options), "The options object is passed to the service constructor");
});

test("It registers an event in the Trigger service", function() {
    this.sandbox.stub(this.module.triggerService, "registerEvent");

    this.module.registerEvent('testEvent');

    ok(this.module.triggerService.registerEvent.calledOnce, "The method registerEvent() has been called");
    equal(this.module.triggerService.registerEvent.firstCall.args[0], 'testEvent', "The event name is passed to registerEvent()");
});

test("It registers a list of events in the Trigger service", function() {
    this.sandbox.stub(this.module.triggerService, "registerEvent");

    this.module.registerEvent(['testEvent1','testEvent2']);

    ok(this.module.triggerService.registerEvent.calledTwice, "The method registerEvent has been called");
    equal(this.module.triggerService.registerEvent.firstCall.args[0], 'testEvent1', "The first event name is passed to registerEvent()");
    equal(this.module.triggerService.registerEvent.secondCall.args[0], 'testEvent2', "The second event name is passed to registerEvent()");
});

test("It triggers an event and passes the parameters a list of events in the Trigger service", function() {
    this.sandbox.stub(this.module.triggerService, "trigger");

    this.module.trigger('testEvent', {value: 42});

    ok(this.module.triggerService.trigger.calledOnce, "The method trigger() has been called");
    equal(this.module.triggerService.trigger.firstCall.args[0], 'testEvent', "The event name is passed to trigger()");
    deepEqual(this.module.triggerService.trigger.firstCall.args[1], {value: 42}, "The event parameters are passed to trigger()");
});

test("It registers a tracker in the DomTracker service", function() {
    this.sandbox.stub(this.module.trackerService, "registerTracker");

    var tracker = {
        context: this.qunitFixture,
        selector: "#event-receiver",
        name: "testEvent"
    };

    this.module.registerTracker(tracker);

    ok(this.module.trackerService.registerTracker.calledOnce, "The method registerTracker() has been called");
    equal(this.module.trackerService.registerTracker.firstCall.args[0], tracker, "The tracker is passed to registerTracker()");
});

test("It registers a list of trackers in the DomTracker service", function() {
    this.sandbox.stub(this.module.trackerService, "registerTracker");

    var trackers = [
        {
            context: this.qunitFixture,
            selector: "#event-receiver",
            name: "testEvent"
        },
        {
            context: this.qunitFixture,
            selector: "#event-receiver",
            name: "testEvent"
        }
    ];

    this.module.registerTracker(trackers);

    ok(this.module.trackerService.registerTracker.calledTwice, "The method registerTracker() has been called");

    equal(this.module.trackerService.registerTracker.firstCall.args[0], trackers[0], "The first tracker is passed to registerTracker()");
    equal(this.module.trackerService.registerTracker.secondCall.args[0], trackers[1], "The second tracker is passed to registerTracker()");
});