AJS.namespace("JIRA.Components.Analytics");

/**
 * @class JIRA.Components.Analytics
 *
 * This module provides the Analytics features. It provides two main features:
 *
 *    * Be able to trigger any arbitrary Analytic event, including extra properties.
 *    * Automatically trigger Analytic events when a DOM Event happens.
 *
 * @extends JIRA.Marionette.Controller
 */
JIRA.Components.Analytics = JIRA.Marionette.Controller.extend({
    /**
     * @constructor
     *
     * @param {Object} options Options object
     * @param {boolean} options.useLog Whether this module should log events using AJS.log()
     */
    initialize: function (options) {
        this.triggerService = new JIRA.Components.Analytics.Services.Trigger(options);
        this.trackerService = new JIRA.Components.Analytics.Services.DomTracker(this.triggerService);
    },

    /**
     * Triggers an event, adding optional parameters. The event must have been registered using {@link registerEvent}
     * before triggering it. If not, the event will be silently discarded.
     *
     * @param {string} name Event to trigger
     * @param {Object} [parameters] Arbitrary data to include with the event
     */
    trigger: function(name, parameters) {
        this.triggerService.trigger(name, parameters);
    },

    /**
     * Registers an event. Events must be registered using this method prior firing them with
     * {@link trigger}
     *
     * This method accepts both a single tracker or an array of trackers.
     *
     * @param {string|string[]} event Event to register
     */
    registerEvent: function(event) {
        var events = [].concat(event);
        _.each(events, function(event) {
            this.triggerService.registerEvent(event);
        },this)
    },

    /**
     * Registers a DOM tracker. A DOM tracker specifies a DOM event and the associated Analytic event. When the
     * DOM Event occurs, the analytic event is fired automatically.
     *
     * This method accepts both a single tracker or an array of trackers.
     *
     * @param {Object} tracker DOM Tracker
     * @param {string} tracker.name Analytic event name to fire
     * @param {string} tracker.selector Selector to use for detect the DOM event
     * @param {string} [tracker.type="click"] DOM Event to listen for
     * @param {Function} [tracker.handler] Handler for that event
     * @param {DOMElement} [tracker.context=document] Element used to listen for the event
     */
    registerTracker: function(tracker) {
        var trackers = [].concat(tracker);
        _.each(trackers, function(tracker) {
            this.trackerService.registerTracker(tracker);
        },this);
    }
});