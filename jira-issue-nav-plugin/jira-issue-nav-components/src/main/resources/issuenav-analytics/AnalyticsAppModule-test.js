AJS.test.require("com.atlassian.jira.jira-issue-nav-components:analytics");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:analytics-test");

module('JIRA.Components.Analytics.AppModule', {
    setup: function () {
        this.sandbox = sinon.sandbox.create();
        this.application = JIRA.Tests.Utils.AppModule.startApplication();
    },

    createModule: function(application) {
        return JIRA.Tests.Utils.AppModule.createModule({
            sinon: this.sandbox,
            application: application || this.application,
            moduleName: "Analytics"
        });
    },

    stubMethods: function(methods) {
        JIRA.Tests.Utils.AppModule.stubMethods({
            sinon: this.sandbox,
            methods: methods,
            moduleClass: JIRA.Components.Analytics
        });
    },

    teardown: function () {
        this.sandbox.restore();
        this.application.stop();
        delete this.application;
    }
});

test("It passes the options to the Analytics appmodule if options.useLog is true", function() {
    var appModule = new JIRA.Components.Analytics.AppModule();
    this.sandbox.stub(JIRA.Components, "Analytics");

    appModule.create({useLog: true});
    equal(JIRA.Components.Analytics.lastCall.args[0].useLog, true, "It passes true as options.useLog to the module");
});

test("It passes the options to the Analytics appmodule if options.useLog is false", function() {
    var appModule = new JIRA.Components.Analytics.AppModule();
    this.sandbox.stub(JIRA.Components, "Analytics");

    appModule.create({useLog: false});
    equal(JIRA.Components.Analytics.lastCall.args[0].useLog, false, "It passes false as options.useLog to the module");
});

test("It passes the options to the Analytics appmodule if options.useLog is undefined", function() {
    var appModule = new JIRA.Components.Analytics.AppModule();
    this.sandbox.stub(JIRA.Components, "Analytics");

    appModule.create({});
    equal(JIRA.Components.Analytics.lastCall.args[0].useLog, false, "By default, it uses options.useLog = false");
});

test("It creates a instance of Analytics", function() {
    var appModule = new JIRA.Components.Analytics.AppModule();
    ok(appModule.create() instanceof JIRA.Components.Analytics, "It creates an instance of Analytics");
});

test("Commands are registered in the application", function() {
    var commands = [
        "trigger",
        "registerEvent",
        "registerTracker"
    ];

    this.stubMethods(commands);
    JIRA.Tests.Utils.AppModule.assertCommands({
        commands: commands,
        commandsNamespace: "analytics",
        application: this.application,
        module: this.createModule()
    });
});
