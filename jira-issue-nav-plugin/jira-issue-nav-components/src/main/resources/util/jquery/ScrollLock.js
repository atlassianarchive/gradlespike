// Prevent the page from scrolling when using the mousewheel to scroll an element with vertical scrollbars

// Usage:
// $el.scrollLock();
// $el.scrollLock('.scrollable');
// $el.scrollLock(50);
// $el.scrollLock('.scrollable', 50);
;(function($) {
    var DEFAULT_SCROLL_INTERVAL = 30;

    $.fn.scrollLock = function(selector, scrollInterval) {
        if (typeof selector !== 'string') {
            scrollInterval = selector;
            selector = null;
        }
        if (!scrollInterval) {
            scrollInterval = DEFAULT_SCROLL_INTERVAL;
        }
        this.on('DOMMouseScroll mousewheel', selector, function(e) {
            e.preventDefault();
            var d;
            if (e.originalEvent.wheelDelta) d = e.originalEvent.wheelDelta / 120;
            if (e.originalEvent.detail) d = -e.originalEvent.detail / 3;
            this.scrollTop -= d * scrollInterval;
        });
    };
})(AJS.$);