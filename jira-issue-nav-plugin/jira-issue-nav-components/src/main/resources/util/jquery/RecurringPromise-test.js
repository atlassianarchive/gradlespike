AJS.test.require("com.atlassian.jira.jira-issue-nav-components:jqueryutils");

module('jQuery.RecurringPromise', {
    setup: function() {
        this.recurringPromise = jQuery.RecurringPromise();
    }
});

test("done handlers called with arguments", function() {
    expect(2);
    var deferred;
    var expectedArgs;

    this.recurringPromise.done(function(a, b) {
        deepEqual([a, b], expectedArgs, "Received correct args");
    });

    deferred = jQuery.Deferred();
    this.recurringPromise.add(deferred.promise());
    expectedArgs = [1, 2];
    deferred.resolve(1, 2);

    deferred = jQuery.Deferred();
    this.recurringPromise.add(deferred.promise());
    expectedArgs = [3, 4];
    deferred.resolve(3, 4);
});

test("fail handlers called with arguments", function() {
    expect(2);
    var deferred;
    var expectedArgs;

    this.recurringPromise.fail(function(a, b) {
        deepEqual([a, b], expectedArgs, "Received correct args");
    });

    deferred = jQuery.Deferred();
    this.recurringPromise.add(deferred.promise());
    expectedArgs = [1, 2];
    deferred.reject(1, 2);

    deferred = jQuery.Deferred();
    this.recurringPromise.add(deferred.promise());
    expectedArgs = [3, 4];
    deferred.reject(3, 4);
});

test("always handlers called with arguments", function() {
    expect(2);
    var deferred;
    var expectedArgs;

    this.recurringPromise.always(function(a, b) {
        deepEqual([a, b], expectedArgs, "Received correct args");
    });

    deferred = jQuery.Deferred();
    this.recurringPromise.add(deferred.promise());
    expectedArgs = [1, 2];
    deferred.resolve(1, 2);

    deferred = jQuery.Deferred();
    this.recurringPromise.add(deferred.promise());
    expectedArgs = [3, 4];
    deferred.reject(3, 4);
});

test("RecurringPromise forgets about any pending promises when a new one is added", function() {
    expect(1);
    this.recurringPromise.always(function(a, b) {
        deepEqual([a, b], [3, 4], "Handler called just once with correct args");
    });

    var firstDeferred = jQuery.Deferred();
    var secondDeferred = jQuery.Deferred();
    this.recurringPromise.add(firstDeferred.promise());
    this.recurringPromise.add(secondDeferred.promise());

    firstDeferred.resolve(1, 2);
    secondDeferred.resolve(3, 4);
});

test("reset() prevents handlers from being called", function() {
    var alwaysHandler = sinon.spy();
    this.recurringPromise.always(alwaysHandler);

    var deferred = jQuery.Deferred();
    this.recurringPromise.add(deferred);
    this.recurringPromise.reset();
    deferred.resolve();

    equal(alwaysHandler.callCount, 0, "handler was not called");
});

test("sequential RecurringPromises will abort previous requests, triggering their fail callbacks", function() {
    var control = this.recurringPromise.sub();
    var first, second;

    var firstDone = sinon.spy(),
        firstFail = sinon.spy(),
        secondDone = sinon.spy(),
        secondFail = sinon.spy();

    first = control.add(jQuery.Deferred());
    first.done(firstDone).fail(firstFail);

    second = control.add(jQuery.Deferred());
    second.done(secondDone).fail(secondFail);

    equal(firstDone.callCount, 0);
    equal(firstFail.callCount, 1);
    equal(secondDone.callCount, 0);
    equal(secondFail.callCount, 0);
});

test("sub-RecurringPromises abort each others' pendings but call only their own callbacks", function() {
    var sub1 = this.recurringPromise.sub();
    var sub2 = this.recurringPromise.sub();
    var sub1_doneSpy = sinon.spy();
    var sub1_failSpy = sinon.spy();
    var sub2_doneSpy = sinon.spy();
    var sub2_failSpy = sinon.spy();
    var requestAbortSpy = sinon.spy();

    sub1.done(sub1_doneSpy);
    sub1.fail(sub1_failSpy);
    sub2.done(sub2_doneSpy);
    sub2.fail(sub2_failSpy);

    var deferred1 = jQuery.Deferred();
    var deferred2 = jQuery.Deferred();
    var singleRequest = sub1.add(deferred1);
    singleRequest.fail(requestAbortSpy);
    sub2.add(deferred2);

    equal(requestAbortSpy.callCount, 1, "Sub1's fail callback was called after a promise was added to Sub2");
    equal(requestAbortSpy.args[0, 0], "abort", "Sub1's fail callback was called after a promise was added to Sub2");

    deferred1.resolve();
    deferred2.resolve();

    equal(sub1_doneSpy.callCount, 0, "Sub1's done callback was never called");
    equal(sub1_failSpy.callCount, 0, "Sub1's fail callback was never called");
    equal(sub2_failSpy.callCount, 0, "Sub2's fail callback was never called");
    equal(sub2_doneSpy.callCount, 1, "Only Sub2's done callback is called");
});
