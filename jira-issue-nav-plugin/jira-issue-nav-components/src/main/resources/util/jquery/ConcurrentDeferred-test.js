AJS.test.require("com.atlassian.jira.jira-issue-nav-components:jqueryutils");

module('jQuery.ConcurrentDeferred');

test("Wrap a deferred without updating", function() {
    var request = jQuery.Deferred();
    var concurrent = jQuery.ConcurrentDeferred(request);
    var doneSpy = sinon.spy();

    concurrent.done(doneSpy);

    equal(doneSpy.callCount, 0, "ConcurrentDeferred has not yet resolved");

    request.resolve();

    equal(doneSpy.callCount, 1, "ConcurrentDeferred was resolved");
});

test("Wrap an already-resolved deferred without updating", function() {
    var resolvedRequest = jQuery.when("SUCCESS");
    var concurrent = jQuery.ConcurrentDeferred(resolvedRequest);
    var doneSpy = sinon.spy();

    concurrent.done(doneSpy);

    equal(doneSpy.callCount, 1, "ConcurrentDeferred was resolved");
});

test("Wrap a deferred with updating", function() {
    var request = jQuery.Deferred();
    var secondRequest = jQuery.Deferred();
    var concurrent = jQuery.ConcurrentDeferred(request);
    var doneSpy = sinon.spy();
    request.abort = sinon.spy();

    concurrent.done(doneSpy);
    concurrent.update(secondRequest);

    equal(doneSpy.callCount, 0, "ConcurrentDeferred has not yet resolved");

    secondRequest.resolve();

    equal(request.abort.callCount, 1, "Initial deferred should be aborted");
    equal(doneSpy.callCount, 1, "ConcurrentDeferred was resolved");
});

test("Wrap an already-resolved deferred with updating", function() {
    var resolvedRequest = jQuery.when("SUCCESS");
    var secondRequest = jQuery.Deferred();
    var concurrent = jQuery.ConcurrentDeferred(resolvedRequest);
    var doneSpy = sinon.spy();

    concurrent.done(doneSpy);

    equal(doneSpy.callCount, 1, "ConcurrentDeferred was resolved");

    sinon.spy(concurrent, "update");
    try {
        concurrent.update(secondRequest);
    } catch (e) {}

    equal(concurrent.update.exceptions.length, 1, "An exception should be thrown");
    equal(concurrent.update.exceptions[0], "Cannot update non-pending ConcurrentDeferred", "Expects the correct exception message");
});

test("Check isPending on a wrapped deferred", function() {
    var concurrent = jQuery.ConcurrentDeferred(jQuery.Deferred());
    var secondRequest = jQuery.Deferred();

    ok(concurrent.isPending(), "Initial deferred should be in pending state");

    concurrent.update(secondRequest);

    ok(concurrent.isPending(), "Updated deferred should be in pending state");

    secondRequest.resolve();

    ok(!concurrent.isPending(), "A resolved deferred should no longer be in a pending state");
});
