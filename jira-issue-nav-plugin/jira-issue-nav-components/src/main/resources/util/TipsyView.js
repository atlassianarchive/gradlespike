
if (!jQuery.fn.tooltip) {
    jQuery.fn.tooltip = jQuery.noop;
    jQuery.fn.tipsy = jQuery.noop;
}

/**
 * Utility backbone view for creating and showing tipsy
 *
 * A number of common predefined behaviours are built into the view so tipsy creation is easy
 */
JIRA.Issues.Tipsy = JIRA.Issues.Brace.View.extend({
    events: {
        'click': "hide"
    },

    /**
     * @param options
     * @param options.tipsy Options to be passed into tipsy
     * @param options.intents Function overrides for hover intents
     * @param options.showCondition {Function | selector}
     *      Whether to show the tipsy based on the conditions defined in the function or
     *      the selecter via $el.filter()
     */
    initialize: function(options) {
        if (_.isFunction(options.showCondition)) {
            this.shouldShow = options.showCondition;
        } else if (options.showCondition) {
            this.shouldShow = _.bind(function() {
                return this.$el.filter(options.showCondition).length > 0;
            }, this);
        } else {
            this.shouldShow = function() {return true;};
        }

        this.tipsy = this.$el.tooltip(_.extend({trigger: "manual"}, options.tipsy));
        this.hoverIntent(options.intents)
    },

    hoverIntent: function(intents) {
        if (this.tipsy) {
            this.tipsy.hoverIntent(_.extend({
                interval: 200,
                over: _.bind(this.show, this),
                out: _.bind(this.hide, this)
            }, intents));
        }
        return this;
    },

    show: function() {
        if (this.shouldShow() && this.tipsy) {
            this.$el.tipsy("show");
        }
    },

    hide: function() {
        if (this.tipsy) {
            this.$el.tipsy("hide");
        }
    },

    remove: function() {
        this.hide();
        this.$el.removeData("tipsy");
    }
});

/* Wrapper for tipsy API */
JIRA.Issues.Tipsy.revalidate = function() {
    return jQuery.fn.tipsy.revalidate();
};

JIRA.Issues.Tipsy.autoBounds = function() {
    return jQuery.fn.tipsy.autoBounds.apply(jQuery.fn.tipsy, arguments);
};
