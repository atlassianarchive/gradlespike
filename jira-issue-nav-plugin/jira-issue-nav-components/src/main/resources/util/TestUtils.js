AJS.namespace("JIRA.Issues.Components");

JIRA.Issues.Components.TestUtils = {

    /**
     * Create a mock of a given class.
     *
     * @param {function} constructor The class's constructor.
     * @return {object} a mock of the given class.
     * @private
     */
    _mockOfClass: function (constructor) {
        var mock = {};
        _.each(_.functions(constructor.prototype), function (functionName) {
            mock[functionName] = sinon.stub();
        });

        return mock;
    },

    mockQueryModule: function () {
        return this._mockOfClass(JIRA.Issues.QueryModule);
    },

    mockQueryStateModel: function () {
        return new JIRA.Issues.QueryStateModel();
    },

    createSearcherCollection: function (vals, ext) {
        var searcherCollection = new JIRA.Issues.SearcherCollection(vals || [], {queryStateModel: this.mockQueryStateModel()});
        sinon.stub(searcherCollection, "_querySearchersAndValues", function() {
            return jQuery.Deferred().resolve().promise();
        });
        sinon.stub(searcherCollection, "_querySearchersByValue", function () {
            return jQuery.Deferred().resolve().promise();
        });
        return searcherCollection;
    },


    // Runs the block function with the dark feature temporarily switched on/off depending on toggle.
    // Use this for testing dark features since AJS.DarkFeature is global and thus not restored between tests.
    // Context is optional and sets the block's context (instead of requiring a _.bind()).
    darkFeature: function (darkFeatureKey, toggle, block, context) {
        var darkFeatures = {};
        darkFeatures[darkFeatureKey] = toggle;

        this.darkFeatures(darkFeatures, block, context);
    },

    /**
     * Execute a function within a dark feature configuration.
     *
     * @param {object} darkFeatures The dark features to enable/disable.
     * @param {function} block The function to execute within that dark feature configuration.
     * @param {object} context The context to invoke the function in.
     */
    darkFeatures: function (darkFeatures, block, context) {
        function setDarkFeatures (darkFeatures) {
            _.each(darkFeatures, function (value, key) {
                AJS.DarkFeatures[value ? "enable" : "disable"](key);
            });
        }

        var originalValues = {};
        _.each(darkFeatures, function (value, key) {
            originalValues[key] = AJS.DarkFeatures.isEnabled(key);
        });

        setDarkFeatures(darkFeatures);
        block.call(context);
        setDarkFeatures(originalValues);
    },

    moveDialogToQunitFixture: function(dialog){
        // Clean up the DOM
        dialog.dialog.$popup.appendTo(jQuery("#qunit-fixture"));
        dialog.dialog.$popup.css("position", "relative");
        AJS.$(".aui-blanket").remove();
        AJS.$(".jira-page-loading-indicator").remove();
        AJS.$("body").css("overflow", "inherit");
    }
};

// QUnit 1.10.0 treats global JS errors as fail
JIRA.Issues.displayFailSearchMessage = jQuery.noop;
