_.mixin({
    lambda: function(x) {
        return function() {return x;}
    },
    isNotBlank: function(object) {
        return !!object;
    }
});


if (!JIRA.Issues) {
    JIRA.Issues = {};
}

/**
 * Adds the class 'checkboxmultiselect-container' to the parent form-body div of a sparker.
 * This removes the implicit padding from the container as all other types of searchers
 * have padding by default.
 */
JIRA.bind(JIRA.Events.CHECKBOXMULITSELECT_READY, function (e, $select) {
    $select.closest(".form-body").addClass("checkboxmultiselect-container");
});