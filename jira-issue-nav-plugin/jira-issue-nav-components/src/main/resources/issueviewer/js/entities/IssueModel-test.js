AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer-test");

module('JIRA.Modules.IssueEditor', {
    setup: function () {
        this.sandbox = sinon.sandbox.create();
        this.sandbox.useFakeServer();

        this.model = new JIRA.Components.IssueViewer.Models.Issue();
    },

    teardown: function () {
        this.sandbox.restore();
    }
});

test("It knows if an issue is the current issue", function() {
    var fakeEntityModel = {id: 123};
    this.model.updateFromEntity(fakeEntityModel);
    ok(this.model.isCurrentIssue(123));

    fakeEntityModel = {id: 124};
    this.model.updateFromEntity(fakeEntityModel);
    ok(!this.model.isCurrentIssue(123));
});

