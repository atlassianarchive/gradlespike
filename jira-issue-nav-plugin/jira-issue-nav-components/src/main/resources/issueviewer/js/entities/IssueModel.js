AJS.namespace("JIRA.Components.IssueViewer.Models.Issue");

/**
 * @class JIRA.Components.IssueViewer.Models.Issue
 *
 * Model for the IssueView. It represents an issue, although most of the important fields
 * are inside the 'entity' object, not in a property of this model.
 *
 * @extends JIRA.Issues.Brace.Model
 */
JIRA.Components.IssueViewer.Models.Issue = JIRA.Issues.Brace.Model.extend({

    namedEvents: [
        /**
         * @event updated
         * Triggered when the view issue model is updated with data from server. This is the main event
         * used by the related views to known when they should refresh the rendered data.
         */
        "updated"
    ],

    namedAttributes: [
        /**
         * Contains information about the issue, including summary and issue operations
         * @type {Object}
         */
        "entity",

        /**
         * Issue id
         * @type {number}
         */
        "id",

        /**
         * Issue key
         * @type {string}
         */
        "key",

        /**
         * Representing web panels for this issue
         * @type {JIRA.Components.IssueViewer.Models.PanelsGroup}
         */
        "panels"
    ],

    /**
     * Default values for this model
     * @returns {Object} Object with default data
     */
    defaults: function () {
        return {
            entity: {},
            panels: new JIRA.Components.IssueViewer.Models.PanelsGroup()
        };
    },

    /**
     * Updates entity and panels with new data
     *
     * @param {Object} data
     * @param {Object} options
     * @param {string[]} [options.fieldsSaved]      The update may come as the result of a save. This array includes the ids of any fields that may have been saved before hand.
     * @param {string[]} [options.fieldsInProgress] Array of fields that are still in edit mode or still saving.
     * @param {boolean}  [options.initialize]       Parameter indicating if it is the first time the update has been called.
     * @param {Object}   [options.changed]          Changed data since last update
     * @param {boolean}  [options.mergeIntoCurrent] Parameter indicating if the data should be merged into current data
     *
     * //TODO initialize, changed and mergeIntoCurrent are used only by IssueHeaderView. Seems this could be simplified.
     */
    update: function (data, options) {
        var updated = false;

        // If we have new data about the issue, update the entity
        if (data.issue) {
            this.updateFromEntity(data.issue);
            updated = true;
        }

        // If we have new data about the panels, update them
        if (data.panels) {
            this.getPanels().update(data.panels, options);
            updated = true;
        }

        // If something has been updated, trigger the "updated" event so our views
        // can render the new content
        if (updated) {
            this.trigger("updated",options);
        }
    },

    /**
     * Clears this model, restoring the defaults.
     *
     * //TODO This should be moved to a higher class, it is a very common operation (that should be already
     * provided by backbone)
     */
    resetToDefault: function () {
        this.clear();
        this.set(this.defaults());
    },

    /**
     * Update our fields based on a new entity
     *
     * @param {Object} entity Object containing the issue information
     */
    updateFromEntity: function (entity) {
        this.set({
            id: entity.id,
            key: entity.key,
            entity: entity
        });
    },

    /**
     * Check if the provided id matches the current issue
     *
     * @param {string|number} issueId ID to check
     * @returns {boolean}
     */
    isCurrentIssue: function (issueId) {
        return this.get("id") == issueId;
    },

    /**
     * Check if this model contains an issue
     *
     * @returns {boolean}
     */
    hasIssue: function () {
        return this.has("id");
    },

    /**
     * Updates the model with a new issueQuery
     *
     * @param {string} query New query
     */
    updateIssueQuery: function(query) {
        if (!this.hasIssue()) return;
        this.getEntity().viewIssueQuery = query;
    }
});
