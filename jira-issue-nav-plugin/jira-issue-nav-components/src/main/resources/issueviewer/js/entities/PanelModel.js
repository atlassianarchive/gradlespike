AJS.namespace("JIRA.Components.IssueViewer.Models.Panel");

/**
 * @class JIRA.Components.IssueViewer.Models.Panel
 *
 * Model for a panel, used by PanelsView
 *
 * @extends JIRA.Issues.Brace.Model
 */
JIRA.Components.IssueViewer.Models.Panel = JIRA.Issues.Brace.Model.extend({

    namedAttributes: [
        /**
         * Contains information about the panel
         * @type {Object}
         */
        "entity",

        /**
         * Prevent panel from being updated
         * @type {boolean}
         */
        "updateLocked"
    ],

    /**
     * @constructor
     *
     * Initializes this model
     */
    initialize: function () {
        // Allowing panels to opt out of refreshing (for example a comment might be half written, we don't want to replace it.)
        JIRA.bind(JIRA.Events.LOCK_PANEL_REFRESHING, _.bind(function (e, id) {
            if (id === this.getEntity().id) {
                this.set("updateLocked", true);
            }
        }, this));
        JIRA.bind(JIRA.Events.UNLOCK_PANEL_REFRESHING, _.bind(function (e, id) {
            if (id === this.getEntity().id) {
                this.set("updateLocked", false);
            }
        }, this));
    },

    /**
     * Updates panel entity from new data
     *
     * @param {Object} entity
     * @param {Object} fieldsInProgress
     * @param {Object} fieldsSaved
     */
    update: function (entity, fieldsInProgress, fieldsSaved) {
        if (!this.getUpdateLocked()) {
            this.set("entity", entity);
            this.trigger("updated", fieldsInProgress, fieldsSaved);
        }
    }
});