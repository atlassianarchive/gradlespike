AJS.namespace("JIRA.Components.IssueViewer.Models.PanelsGroup");

/**
 * @class JIRA.Components.IssueViewer.Models.PanelsGroup
 *
 * This model contain groups of panels. It is used by BodyView to represent the left panels, right panels
 * and info panels.
 *
 * @extends JIRA.Issues.Brace.Model
 */
JIRA.Components.IssueViewer.Models.PanelsGroup = JIRA.Issues.Brace.Model.extend({

    namedAttributes: [
        /**
         * Panels on the left side
         * @type JIRA.Components.IssueViewer.Collections.Panels
         */
        "leftPanels",

        /**
         * Panels on the right side
         * @type JIRA.Components.IssueViewer.Collections.Panels
         */
        "rightPanels",

        /**
         * Panels in the middle of the view
         * @type JIRA.Components.IssueViewer.Collections.Panels
         */
        "infoPanels"
    ],


    defaults: function() {
        //This needs to be a function because otherwise the IssuePanelsCollection instances
        //will be shared among all instances of IssuePanelsGroupModel
        return {
            leftPanels: new JIRA.Components.IssueViewer.Collections.Panels([]),
            rightPanels: new JIRA.Components.IssueViewer.Collections.Panels([]),
            infoPanels: new JIRA.Components.IssueViewer.Collections.Panels([])
        }
    },

    /**
     * Updates all the panel models with new data. Creates new ones that don't exist yet.
     *
     * @param {Object} data
     * @param {Object[]} data.leftPanels  Panels for the left group
     * @param {Object[]} data.rightPanels Panels for the right group
     * @param {Object[]} data.infoPanels  Panels for the info group
     * @param {Object} options
     */
    update: function (data, options) {
        //TODO options is here only to carry the value of 'fieldsInProgress' and 'fieldsSaved' to IssuePanelView.
        //It is a long journey through lot of layers, we should find another way to do it.

        this.get("leftPanels").update(data.leftPanels, options);
        this.get("rightPanels").update(data.rightPanels, options);
        this.get("infoPanels").update(data.infoPanels, options);
    }
});
