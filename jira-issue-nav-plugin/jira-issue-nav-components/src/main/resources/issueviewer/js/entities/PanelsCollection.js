AJS.namespace("JIRA.Components.IssueViewer.Collections.Panels");

/**
 * @class JIRA.Components.IssueViewer.Collections.Panels
 *
 * Collection of panels, rendered by PanelsView
 *
 * @extend JIRA.Issues.Brace.Collections
 */
JIRA.Components.IssueViewer.Collections.Panels = JIRA.Issues.Brace.Collection.extend({

    model: JIRA.Components.IssueViewer.Models.Panel,

    /**
     * Update this collection of panels with new data
     *
     * @param {Object} data Array of entities, each entity represents a panel
     * @param {Object} options
     * @param {string[]} options.fieldsInProgress List of fields that are in progress
     * @param {string[]} options.fieldsSaved List of fields that we just saved
     */
    update: function (data, options) {
        var instance = this;

        // For each panel already present in this collection, check if it is still present in data.
        // If not, remove it from the collection.
        this.each(function (panel) {
            var panelId = panel.id;
            var panelPresentInPanelEntities = _.any(data, function (panel) {
                return panel.id === panelId
            });
            if (!panelPresentInPanelEntities) {
                this.remove(panel);
            }
        }, this);

        // For each ponel in data, check if there is already present in the collection. If it is, update it
        // with the new data. If not, add it to the collection.
        _.each(data, function (entity, index) {
            var panel = this.get(entity.id);
            if (panel) {
                panel.update(entity, options.fieldsInProgress, options.fieldsSaved);
            } else {
                this.add({id: entity.id, entity: entity}, {at: index});
            }
        }, this);
    }
});
