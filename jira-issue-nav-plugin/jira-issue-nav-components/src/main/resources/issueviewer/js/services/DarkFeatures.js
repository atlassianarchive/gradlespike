AJS.namespace("JIRA.Components.IssueViewer.Services.DarkFeatures");

/**
 * Holds the dark feature switches that affect how the KA app works.
 */
JIRA.Components.IssueViewer.Services.DarkFeatures = (function() {
    var Feature = Class.extend({
        /**
         * Creates a new Feature object for the given feature key.
         *
         * @param featureKey {String} the name of the feature
         */
        init: function(featureKey) {
            this.featureKey = featureKey;
        },

        /**
         * Returns true if this Feature is enabled.
         *
         * @return {Boolean}
         */
        enabled: function() {
            return AJS.DarkFeatures.isEnabled(this.featureKey);
        }
    });

    return {
        /**
         * If enabled, kills issue prefetching.
         */
        NO_PREFETCH: new Feature("ka.NO_PREFETCH"),

        /**
         * If enabled, project's avatar will display a popup on click
         */
        PROJECT_SHORTCUTS: new Feature("rotp.project.shortcuts")
    }
})();