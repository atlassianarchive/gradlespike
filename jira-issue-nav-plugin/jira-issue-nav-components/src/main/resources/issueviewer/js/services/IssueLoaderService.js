AJS.namespace("JIRA.Components.IssueViewer.Services.IssueLoader");

/**
 * @class JIRA.Components.IssueViewer.Services.IssueLoader
 *
 * This service is responsible from loading data for an issue.
 *
 * @extends Marionette.Controller
 */
JIRA.Components.IssueViewer.Services.IssueLoader = JIRA.Marionette.Controller.extend({
    /**
     * @event issueLoaded
     * Fired when an issue has been loaded or updated
     * @param {Object} data
     * @param {Object} meta
     * @param {Object} options
     */

    /**
     * @event error
     * Fired when there is an error loading or updating the issue
     * @param {string} reason Type of error: "auth", "forbidden", "notfound" or "generic"
     * @param {Object} options
     */

    /**
     * @constructor
     */
    initialize: function () {
        this._promiseManager = jQuery.RecurringPromise();

        this._updatingPromise = this._promiseManager.sub();
        this._updatingPromise
            .done(_.bind(this._onUpdatingDone, this))
            .fail(_.bind(this._onUpdatingError, this));

        this._loadingPromise = this._promiseManager.sub();
        this._loadingPromise
            .done(_.bind(this._onLoadingDone, this))
            .fail(_.bind(this._onLoadingError, this));
    },

    /**
     * Cancel all pending requests for our promises
     */
    cancel: function () {
        this._promiseManager.reset();
    },

    /**
     * Updates an issue
     *
     * @param {Object} options
     * @param {JIRA.Components.IssueViewer.Legacy.ViewIssueData} options.viewIssueData
     * @param {Object} options.issueEntity Issue entity to update
     * @param {boolean} options.mergeIntoCurrent whether the update should just merge into the current issue
     */
    update: function(options) {
        var issueEntity = options.issueEntity,
            mergeIntoCurrent = options.mergeIntoCurrent,
            viewIssueData = options.viewIssueData,
            detailView = !!options.detailView;

        // Add the updating task to the recurring promise
        var deferred = jQuery.Deferred(),
            recurrantControl;

        function genPromiseWrapper(taskKey) {
            return function() {
                return {
                    taskKey: taskKey,
                    data: arguments,
                    startIssueLoad: new Date()
                };
            }
        }

        recurrantControl = this._updatingPromise.add(viewIssueData.get(issueEntity.key, true, {
            issueEntity: issueEntity,
            loadingDeferred: deferred,
            mergeIntoCurrent: mergeIntoCurrent,
            startIssueLoad: new Date(),
            detailView: detailView // JRA-36659: keep track of whether we are in detail view
        }).pipe(genPromiseWrapper("issue"), genPromiseWrapper("issue")));

        recurrantControl.fail(function() {
            deferred.reject.apply(deferred, arguments);
        });

        return deferred.promise();
    },

    /**
     * Loads an issue
     *
     * @param {Object} options
     * @param {JIRA.Components.IssueViewer.Legacy.ViewIssueData} options.viewIssueData
     * @param {Object} options.issueEntity Issue entity to update

     * @returns {jQuery.Promise}
     */
    load: function (options) {
        var issueEntity = options.issueEntity;
        var viewIssueData = options.viewIssueData;
        var detailView = !!issueEntity.detailView;

        this._currentlyLoading = true;

        // Add the loading task to the recurring promise
        var deferred = jQuery.Deferred();

        this._loadingPromise.add(viewIssueData.get(issueEntity.key, false, {
            issueEntity: issueEntity,
            loadFields: false,
            loadingDeferred: deferred,
            startIssueLoad: new Date(),
            detailView: detailView // JRA-36659: keep track of whether we are in detail view
        }));

        return deferred.promise();
    },

    /**
     * Checks if there is a loading in progress
     * @returns {boolean} Whether there is a loading operation in progress
     */
    isLoading: function () {
        return this._currentlyLoading;
    },

    /**
     * Handler for loading:done, when an issue has been loaded
     *
     * @param {Object} data
     * @param {Object} meta
     * @param {Object} options
     * @private
     */
    _onLoadingDone: function (data, meta, options) {
        // Massage data
        var issueEntity = options.issueEntity;
        var issueId = issueEntity.id = data.issue.id;
        issueEntity.key = data.issue.key;
        if (!data.pager) {
            data.pager = issueEntity.pager;
        }
        JIRA.Components.IssueViewer.Legacy.IssueFieldUtil.transformFieldHtml(data);

        // Mark request as loaded
        this._currentlyLoading = false;

        // Compute loading time
        meta.loadDuration = new Date() - options.startIssueLoad;

        // Trigger our main event
        this.trigger("issueLoaded", data, meta, options);

        // Resolve the main promise
        options.loadingDeferred.resolve(data, meta, options);
    },

    /**
     * Handler for updating:done, when an issue has been updated.
     *
     * This method does virtually nothing, just call _onLoadingDone with meta.isUpdate=true
     *
     * @param {Object[]} args
     * @private
     */
    _onUpdatingDone: function(args) {
        var meta = args.data[1];
        if (meta) {
            meta.isUpdate = true;
        }
        this._onLoadingDone.apply(this, args.data);
    },

    /**
     * Handler for loading:reject, when there are errors loading an issue
     *
     * @param {Object} data
     * @param {Object} meta
     * @param {Object} options
     * @private
     */
    _onLoadingError: function (data, meta, options) {
        // Extract some data
        var issueEntity = options.issueEntity;
        var issueId = issueEntity.id;
        var props = {
            issueId: issueEntity.id,
            issueKey: issueEntity.key,
            response: data
        };

        // Mark request as loaded
        this._currentlyLoading = false;

        // Trigger our main event and resolve the promise
        var reason;
        switch (data.status) {
            case 401: reason = "auth"; break;
            case 403: reason = "forbidden"; break;
            case 404: reason = "notfound"; break;
            default:  reason = "generic"; break;
        }

        this.trigger("error", reason, props);
        options.loadingDeferred.reject();
    },

    /**
     * Handler for updating:reject, when there are errors updating an issue
     *
     * This method does virtually nothing, just call _onLoadingError with the right params
     *
     * @param {Object[]} args
     * @private
     */
    _onUpdatingError: function (args) {
        this._onLoadingError.apply(this, args.data);
    }
});