AJS.namespace("JIRA.Components.IssueViewer.Services.Metadata");

/**
 * @class JIRA.Components.IssueViewer.Services.Metadata
 *
 * This service is responsible from adding/removing metadata about an issue
 *
 * @extends Marionette.Controller
 */
JIRA.Components.IssueViewer.Services.Metadata = {
    /**
     * Removes all the issue meta data such as issue key from AJS.Meta
     *
     * @param {JIRA.Components.IssueViewer.Models.Issue} model Model used to extract the values to remove
     */
    removeIssueMetadata: function (model) {
        var issueEntity = model.getEntity();
        if (issueEntity.metadata) {
            _.each(issueEntity.metadata, function (value, key) {
                AJS.Meta.set(key, null);
            });
        }else if (AJS.Meta.get("issue-key")) {
            AJS.Meta.set("issue-key", null);
        }
    },

    /**
     * Add all the issue meta data such as issue key into AJS.Meta
     *
     * @param {JIRA.Components.IssueViewer.Models.Issue} model Model used to extract the values to add
     */
    addIssueMetadata: function (model) {
        var issueEntity = model.getEntity();
        _.each(issueEntity.metadata, function (value, key) {
            AJS.Meta.set(key, value);
        });
    }
};