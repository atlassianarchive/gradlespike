AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer-test");

module('JIRA.Components.IssueViewer', {
    setup: function () {
        this.sandbox = sinon.sandbox.create();
        this.sandbox.useFakeServer();
        this.sandbox.stub(JIRA.Messages, "showErrorMsg");

        this.module = new JIRA.Components.IssueViewer();
    },

    teardown: function () {
        this.sandbox.restore();
    }
});

test("When setting the container, it updates the container of the internal controllers", function() {
    this.module.setContainer(AJS.$("#qunit-fixture"));

    ok(this.module.errorController._$el.is(AJS.$("#qunit-fixture")), "The ErrorController's element was updated");
    ok(this.module.issueController.$el.is(AJS.$("#qunit-fixture")), "The IssueController's element was updated");
});

test("When loading a valid issue, it should appropriately set the issue meta data", function() {
    this.sandbox.stub(JIRA.Components.IssueViewer.Services.Metadata, "addIssueMetadata");

    this.module._onIssueLoaded(
        { issue: { id: 12345, key: "JRA-123"} },
        {},
        { issueEntity: {}}
    );

    ok(JIRA.Components.IssueViewer.Services.Metadata.addIssueMetadata, "Metadata service is called");
});

test("When having an error loading an issue, it should appropriately remove the issue meta data", function() {
    this.sandbox.stub(JIRA.Components.IssueViewer.Services.Metadata, "removeIssueMetadata");

    this.module.issueLoader.trigger("error", "generic", {});

    ok(JIRA.Components.IssueViewer.Services.Metadata.removeIssueMetadata, "Metadata service is called");
});

test("When loading an issue when comment is dirty, it should show a confirmation", function () {
    this.sandbox.stub(window, "confirm");

    var $form = jQuery("<form id='issue-comment-add' />").appendTo("#qunit-fixture");
    var $comment = jQuery("<textarea id='comment' name='comment'></textarea>").appendTo($form);
    $comment.val("I am a dirty dirty field");

    var response = this.module.loadIssue({id:1, key:"KEY-1"});

    ok(window.confirm.calledOnce, "Expected dirty form warning to appear");
    ok(response.isRejected(), "Expected deferred to be rejected");
});

test("When loading an issue when comment is dirty and the user does not allow the dismissal of comment, the comment is focused", function () {
    this.sandbox.stub(window, "confirm").returns(false);
    this.sandbox.spy(JIRA.Issue.CommentForm, "focus");

    var $form = jQuery("<form id='issue-comment-add' />").appendTo("#qunit-fixture");
    var $comment = jQuery("<textarea id='comment' name='comment' />").appendTo($form);
    $comment.val("I am a dirty dirty field");

    var response = this.module.canDismissComment();

    ok(!response, "Expected not to be able to dismiss comment");
    ok(JIRA.Issue.CommentForm.focus.calledOnce, "Expected comment to be focused");
});

test("When loading an issue when comment is dirty, the confirm dialog only appears once", function () {
    this.sandbox.stub(window, "confirm").returns(true);

    var $form = jQuery("<form id='issue-comment-add' />").appendTo("#qunit-fixture");
    var $comment = jQuery("<textarea id='comment' name='comment'></textarea>").appendTo($form);
    $comment.val("I am a dirty dirty field");

    this.module.canDismissComment();
    this.module.canDismissComment();
    this.module.canDismissComment();

    ok(window.confirm.calledOnce, "Expected confirm dialog to be called once");
});

test("When loading an issue when comment is dirty, the confirm dialog appears if cancelled and then invoked again", function () {
    this.sandbox.stub(window, "confirm").returns(false);

    var $form = jQuery("<form id='issue-comment-add' />").appendTo("#qunit-fixture");
    var $comment = jQuery("<textarea id='comment' name='comment'></textarea>").appendTo($form);
    $comment.val("I am a dirty dirty field");

    this.module.canDismissComment();
    this.module.canDismissComment();

    ok(window.confirm.calledTwice, "Expected confirm dialog to be called twice");
});

test("When an issue is updated, the viewIssueQuery is saved", function() {
    sinon.stub(this.module.viewIssueData,"get").returns(jQuery.Deferred());

    var query = {
        "attachmentOrder": "DESC"
    };
    this.module.model.setId("1");
    this.module.updateIssueWithQuery(query);

    ok(this.module.viewIssueData.get.calledOnce, "ViewIssueData.get is called once");
    deepEqual(this.module.viewIssueData.get.firstCall.args[2].issueEntity.viewIssueQuery, query, "New ViewIssueQuery is passsed to ViewIssueData.get");
});

test("When an issue is refreshed, update is called with mergeIntoCurrent = true by default.", function () {
    var issueLoaderSpy = sinon.spy(this.module.issueLoader, "update");
    this.module.model.set("id", "1");
    this.module.refreshIssue();
    ok(issueLoaderSpy.calledOnce, "update() should have been called once.");
    equal(issueLoaderSpy.firstCall.args[0].mergeIntoCurrent, true, "mergeIntoCurrent should equal true.");
});

test("When an issue is refreshed, update is called with mergeIntoCurrent = false when passed as options", function () {
    var issueLoaderSpy = sinon.spy(this.module.issueLoader, "update");
    this.module.model.set("id", "1");
    this.module.refreshIssue({
        mergeIntoCurrent: false
    });
    ok(issueLoaderSpy.calledOnce, "update() should have been called once.");
    equal(issueLoaderSpy.firstCall.args[0].mergeIntoCurrent, false, "mergeIntoCurrent should equal false.");
});

test("When a new issue is loaded, the old issue is closed before showing the new one", function() {
    var issueViewer = new JIRA.Components.IssueViewer();
    var issueLoader = issueViewer.issueLoader;
    var issueController = issueViewer.issueController;

    this.sandbox.stub(issueController, "close");
    issueLoader.trigger("issueLoaded",
        { issue: { id: 12345, key: "JRA-123"} },
        {},
        { issueEntity: {}}
    );
    ok(issueController.close.calledOnce, "The issue is closed");
});

test("When a new issue is loaded, the old issue is not closed if the new one is an update", function() {
    var issueViewer = new JIRA.Components.IssueViewer();
    var issueLoader = issueViewer.issueLoader;
    var issueController = issueViewer.issueController;

    this.sandbox.stub(issueController, "close");
    issueLoader.trigger("issueLoaded",
        { issue: { id: 12345, key: "JRA-123"} },
        { isUpdate: true, mergeIntoCurrent: true },
        { issueEntity: {}}
    );
    ok(!issueController.close.called, "The issue is not closed");
});

test("When a new issue is loaded, the old issue is not closed if the new one is a new set of data", function() {
    var issueViewer = new JIRA.Components.IssueViewer();
    var issueLoader = issueViewer.issueLoader;
    var issueController = issueViewer.issueController;

    this.sandbox.stub(issueController, "close");
    issueLoader.trigger("issueLoaded",
        { issue: { id: 12345, key: "JRA-123"} },
        { isUpdate: true, mergeIntoCurrent: true },
        { issueEntity: {}}
    );
    ok(!issueController.close.called, "The issue is not closed");
});

