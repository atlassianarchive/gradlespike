JIRA.Components.IssueViewer.Utils = {
    /**
     * Hides the Fancybox overlay that we use for image attachments, viewing a workflow, etc.
     */
    hideLightbox: function () {
        AJS.$.fancybox.close();
    },

    /**
     * Hides any visible drop down on the page
     */
    hideDropdown: function () {
        //HACK: Having a whitelist of dialogs that should not be closed is a maintenance nightmare,
        //but a better solution will involve refactoring AJS.InlineDialog and possibly a change of its
        //architecture/API. Related to JRADEV-21760
        if (AJS.InlineDialog.current
            && (!JIRA.Issues.IntroDialog || _.contains(JIRA.Issues.IntroDialog.dialogs, AJS.InlineDialog.current))
            && AJS.InlineDialog.current.id !== "column-picker-dialog"
            ) {
            AJS.InlineDialog.current.hide();
        }
        if (AJS.InlineLayer.current) {
            var $trigger = AJS.InlineLayer.current.options.offsetTarget();
            if ($trigger instanceof jQuery) {
                if (!$trigger.is(":visible")) {
                    AJS.InlineLayer.current.hide();
                }
            }

        }
    }
};

JIRA.Events.ISSUE_REFRESHED = "issueRefreshed";
JIRA.Events.PANEL_REFRESHED = "panelRefreshed";

JIRA.Issues.Actions = JIRA.Issues.Actions || {};
JIRA.Issues.Actions.EDIT_COMMENT= "editComment";