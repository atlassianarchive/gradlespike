AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer-test");

module('JIRA.Components.IssueViewer', {
    setup: function () {
        this.sandbox = sinon.sandbox.create();
        this.sandbox.useFakeServer();

        this.utils = JIRA.Components.IssueViewer.Utils;
    },

    teardown: function () {
        this.sandbox.restore();
    }
});

test("When a dropdown is open but the trigger is not in the DOM, it should be closed", function() {
    var $myContext = jQuery("<div><a href='#' id='mydropdown-trigger'>open</a><div id='mydropdown-content' class='hidden'>Content</div>")
        .appendTo("#qunit-fixture");
    var $trigger = $myContext.find("#mydropdown-trigger");
    var $dropdownContent = $myContext.find("#mydropdown-content");

    new AJS.Dropdown({
        trigger: $trigger,
        content: $dropdownContent
    });
    $trigger.click(); // show dropdown
    $trigger.remove();

    ok($dropdownContent.is(":visible"));
    this.utils.hideDropdown();
    ok(!$dropdownContent.is(":visible"), "Expected dropdown to be hidden as the trigger is no longer part of the dom");
});

test("When a dropdown is open and the trigger is still visible, don't close it", function() {
    var $myContext = jQuery("<div><a href='#' id='mydropdown-trigger'>open</a><div id='mydropdown-content' class='hidden'>Content</div>")
        .appendTo("#qunit-fixture");
    var $trigger = $myContext.find("#mydropdown-trigger");
    var $dropdownContent = $myContext.find("#mydropdown-content");

    new AJS.Dropdown({
        trigger: $trigger,
        content: $dropdownContent
    });
    $trigger.click(); // show dropdown

    ok($dropdownContent.is(":visible"));
    this.utils.hideDropdown();
    ok($dropdownContent.is(":visible"), "Expected dropdown NOT to be hidden as the trigger is still visible");
});
