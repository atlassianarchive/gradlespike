AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer-test");

module('JIRA.Components.IssueViewer.AppModule', {
    setup: function () {
        JIRA.Issues.LayoutPreferenceManager = JIRA.Issues.LayoutPreferenceManager || {
            getPreferredLayoutKey: function() { return "list-view"; }
        };

        this.sandbox = sinon.sandbox.create();
        this.application = JIRA.Tests.Utils.AppModule.startApplication();
    },

    createModule: function() {
        return JIRA.Tests.Utils.AppModule.createModule({
            sinon: this.sandbox,
            application: this.application,
            moduleName: "IssueViewer"
        });
    },

    stubMethods: function(methods) {
        JIRA.Tests.Utils.AppModule.stubMethods({
            sinon: this.sandbox,
            methods: methods,
            moduleClass: JIRA.Components.IssueViewer
        });
    },

    teardown: function () {
        this.sandbox.restore();
        this.application.stop();
        delete this.application;
    }
});

test("It creates a instance of IssueViewer", function() {
    var appModule = new JIRA.Components.IssueViewer.AppModule();
    ok(appModule.create() instanceof JIRA.Components.IssueViewer, "Initialize creates a instance of IssueViewer");
});

test("Commands are registered in the application", function() {
    var commands = [
        "abortPending",
        "beforeHide",
        "beforeShow",
        "removeIssueMetadata",
        "updateIssueWithQuery",
        "close",
        "setContainer",
        "dismiss"
    ];

    this.stubMethods(commands);
    JIRA.Tests.Utils.AppModule.assertCommands({
        commands: commands,
        commandsNamespace: "issueViewer",
        application: this.application,
        module: this.createModule()
    });
});

test("Requests are registered in the application", function() {
    var requests = [
        "loadIssue",
        "canDismissComment",
        "getIssueId",
        "getIssueKey",
        "refreshIssue",
        "isCurrentlyLoading"
    ];

    this.stubMethods(requests);
    JIRA.Tests.Utils.AppModule.assertRequests({
        requests: requests,
        requestsNamespace: "issueViewer",
        application: this.application,
        module: this.createModule()
    });
});

test("Events are registered in the application", function() {
    var events = [
        "loadComplete",
        "loadError",
        "close",
        "replacedFocusedPanel"
    ];
    JIRA.Tests.Utils.AppModule.assertEvents({
        events: events,
        sinon: this.sandbox,
        application: this.application,
        eventsNamespace: "issueViewer",
        module: this.createModule()
    });
});