AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer-test");

module('JIRA.Components.IssueViewer.Controllers.Issue', {
    setup: function () {
        this.sandbox = sinon.sandbox.create();

        this.model = new JIRA.Components.IssueViewer.Models.Issue();
        this.controller = new JIRA.Components.IssueViewer.Controllers.Issue({
            model: this.model
        });
        this.model.setId(123);
    },

    teardown: function () {
        this.sandbox.restore();
    },

    assertRenderEvent: function(spy, issueId, loadedFromDom) {
        ok(spy.calledOnce, "It fires a 'render' event");

        ok(typeof spy.firstCall.args[0] == "object", "The 'render' event includes a list of regions as the first argument");
        ok(spy.firstCall.args[0].pager, "The 'render' event includes a reference to the 'pager' element");

        ok(typeof spy.firstCall.args[1] == "object", "The 'render' event includes custom options as second argument");
        ok(spy.firstCall.args[1].issueId == issueId, "The options object includes issueId");
        ok(spy.firstCall.args[1].loadedFromDom == loadedFromDom, "The options object includes loadedFromDom");
    }
});

test("When showing an issue for the first time, it throws a render event", function() {
    var spy = this.sandbox.spy();
    this.controller.on("render", spy);

    this.controller.show();

    this.assertRenderEvent(spy, 123, false);
});

test("When showing an issue for the second time, it also throws a render event", function() {
    this.controller.show();
    var spy = this.sandbox.spy();
    this.controller.on("render", spy);

    this.controller.show();

    this.assertRenderEvent(spy, 123, false);
});

test("When loading an issue from the DOM, it throws a render event", function() {
    var spy = this.sandbox.spy();
    this.controller.on("render", spy);

    this.controller.applyToDom({id: 123});

    this.assertRenderEvent(spy, 123, true);
});

test("When the header is updated, it throws a render event", function() {
    this.controller.createView();

    var renderSpy = this.sandbox.spy();
    this.controller.on("render", renderSpy);

    this.model.trigger("updated",{});

    sinon.assert.calledOnce(renderSpy);
});