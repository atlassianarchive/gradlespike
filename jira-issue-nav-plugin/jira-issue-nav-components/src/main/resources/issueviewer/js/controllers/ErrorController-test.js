AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer-test");

module('JIRA.Components.IssueViewer.Controllers.Error', {
    setup: function () {
        this.sandbox = sinon.sandbox.create();

        this.controller = new JIRA.Components.IssueViewer.Controllers.Error({
            contextPath: "jira/",
            showReturnToSearchOnError: function(){return false;}
        });
    },

    teardown: function () {
        this.sandbox.restore();
    }
});

test("When rendering an error, it fires the 'render' event and provides a list of regions", function() {
    var spy = this.sandbox.spy();
    this.controller.on("render", spy);

    this.controller.render("forbidden");

    ok(spy.calledOnce, "It fires a 'render' event");
    ok(typeof spy.firstCall.args[0] == "object", "It includes a list of regions as the first argument");

    ok(spy.firstCall.args[0].pager, "It includes a reference to the 'pager' element");
});

test("When rendering an error from AJAX, it provides that information in the options argument", function() {
    var spy = this.sandbox.spy();
    this.controller.on("render", spy);

    this.controller.render("forbidden");

    ok(spy.firstCall.args[1].loadedFromDom === false, "loadedFromDom is false");
});

test("When rendering an error from DOM, it provides that information in the options argument", function() {
    var spy = this.sandbox.spy();
    this.controller.on("render", spy);

    this.controller.applyToDom("forbidden");

    ok(spy.firstCall.args[1].loadedFromDom === true, "loadedFromDom is true");
});