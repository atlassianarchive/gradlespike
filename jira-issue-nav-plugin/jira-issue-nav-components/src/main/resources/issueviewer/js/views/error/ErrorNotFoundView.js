AJS.namespace("JIRA.Components.IssueViewer.Views.ErrorNoPermission");

/**
 * @class JIRA.Components.IssueViewer.Views.ErrorNotFound
 * Renders the error for a NotFound issue
 *
 * @extends JIRA.Components.IssueViewer.Views.Error
 */
JIRA.Components.IssueViewer.Views.ErrorNotFound = JIRA.Components.IssueViewer.Views.Error.extend({
    template: JIRA.Components.IssueViewer.Templates.Error.DoesNotExist,

    /**
     * Initializes this view
     *
     * @param options
     * @param {boolean|function} [options.showReturnToSearchOnError=false] Whether the error views should display a 'Return to Search' link
     */
    initialize: function(options) {
        options = options || {};
        this.showReturnToSearchOnError = options.showReturnToSearchOnError
    },

    triggers: {
        "click #return-to-search": "returnToSearch"
    },

    serializeData: function() {
        var showReturnToSearchOnError = this.showReturnToSearchOnError;
        if (_.isFunction(showReturnToSearchOnError)){
            showReturnToSearchOnError = showReturnToSearchOnError()
        }

        return {
            hideReturnToSearch: !showReturnToSearchOnError
        };
    }
});
