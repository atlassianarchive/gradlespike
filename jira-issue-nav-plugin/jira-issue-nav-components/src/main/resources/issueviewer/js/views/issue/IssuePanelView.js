AJS.namespace("JIRA.Components.IssueViewer.Views.IssuePanel");

/**
 * @class JIRA.Components.IssueViewer.Views.IssuePanel
 *
 * View for rendering a panel
 *
 * @extends JIRA.Marionette.ItemView
 */
JIRA.Components.IssueViewer.Views.IssuePanel = JIRA.Marionette.ItemView.extend({

    template: JIRA.Components.IssueViewer.Templates.Body.issuePanel,

    modelEvents: {
        "updated": "update"
    },

    /**
     * Update this view with the new data from our model
     *
     * If edits are in progress will replace only elements that have been saved
     * If no edits are in progress will replace entire panel.
     *
     * @param {string[]} fieldsInProgress IDs for fields that are still in progress
     * @param {string[]} fieldsSaved IDs for fields that we just saved
     */
    update: function (fieldsInProgress, fieldsSaved) {
        var $new = this.renderTemplate();
        var $existing = this.$el;
        var instance = this;

        // Replacing panel content can be VERY expensive in ie8 (sometimes 7 seconds) so we only want to update
        // if the content has changed
        if ($new.text() === $existing.text()) {
            return;
        }

        // Check if there is a field in progress inside this panel
        function hasFieldInProgress() {
            return _.any(fieldsInProgress, function (id) {
                return $existing.find(JIRA.Components.IssueViewer.Legacy.IssueFieldUtil.getFieldSelector(id)).length === 1;
            });
        }

        // Replace the whole panel
        function replacePanel() {
            var panelHadFocus = $existing.find(document.activeElement).length > 0;

            // For unknown reason, jQuery's replaceWith does not work for IE8 in some cases.
            // I suspect it is related to the fact both this.$el and updatedDom.$el are a collection of *two* elements
            // This quick hack (that mostly mimics the replaceWith() implementation) works.
            var isIe8 = !!jQuery.browser.msie && jQuery.browser.version <= 8;
            if (!isIe8) {
                $existing.replaceWith($new);
            }else{
                var next = $existing[0].nextSibling;
                var parent = $existing[0].parentNode;
                $existing.remove();

                if (next) {
                    jQuery(next).before($new);
                } else {
                    jQuery(parent).append($new);
                }
            }

            if (panelHadFocus) {
                instance.trigger("replacedFocusedPanel");
            }
            instance.setElement($new);
        }

        // Trigger the internal and JIRA events related to this view
        function triggerEvents(updatedElements) {
            var modelId = instance.model.id;
            var $el = instance.$el;

            instance.trigger("panelRendered", modelId, $el);
            JIRA.trigger(JIRA.Events.PANEL_REFRESHED, [ modelId, $el, $existing]);
            _.each(updatedElements, function (updatedElement) {
                JIRA.trigger(JIRA.Events.NEW_CONTENT_ADDED, [updatedElement, JIRA.CONTENT_ADDED_REASON.panelRefreshed]);
            });
        }


        if (!hasFieldInProgress()) {
            //If no field is in progress, just replace the whole panel
            replacePanel();
            triggerEvents([instance.$el]);
        } else {
            var updates = [];
            _.each(fieldsSaved, function (id) {
                var $toReplace = $existing.find(JIRA.Components.IssueViewer.Legacy.IssueFieldUtil.getFieldSelector(id));
                if ($toReplace.length === 1) {
                    var $replaceWith = $new.find(JIRA.Components.IssueViewer.Legacy.IssueFieldUtil.getFieldSelector(id));
                    if ($replaceWith.length === 1) {
                        // The field has been saved and we need to display the new value, replace it
                        $toReplace.replaceWith($replaceWith);
                        updates.push($replaceWith);
                    } else {
                        // Remove field if it's not present in the new panel
                        // Assumes the field's container in the panel - may not work properly with plugin panels.
                        // TODO: add a class to the container to find it more reliably.
                        $toReplace.closest('li, dl').remove();
                    }
                }
            });
            triggerEvents(updates);
        }

        this.bindUIElements();
    },

    /**
     * Handler for render events, things to do after the template has been rendered
     */
    onRender: function () {
        // Discard the root element created by Backbone
        this.setElement(this.$el.children());

        // If we have a style class, apply it
        var styleClass = this.model.getEntity().styleClass;
        if (styleClass) {
            this.$el.addClass(styleClass);
        }

        JIRA.trigger(JIRA.Events.NEW_CONTENT_ADDED, [this.$el, JIRA.CONTENT_ADDED_REASON.panelRefreshed]);
    },

    /**
     * Extract the data from the model in the format needed by the template
     *
     * @returns {Object} Data to be rendered by the template
     */
    serializeData: function () {
        return this.model.getEntity();
    }
});
