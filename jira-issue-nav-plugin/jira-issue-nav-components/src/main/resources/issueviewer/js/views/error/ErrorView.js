AJS.namespace("JIRA.Components.IssueViewer.Views.Error");

/**
 * @class JIRA.Components.IssueViewer.Views.Error
 * Abstract class for all error views
 *
 * @extends JIRA.Marionette.Layout
 * @abstract
 *
 * //TODO Review ReturnToSearch functionality, some WD tests are failing
 */
JIRA.Components.IssueViewer.Views.Error = JIRA.Marionette.Layout.extend({

    className: "issue-container",

    regions: {
        pager: "#issue-header-pager"
    },

    /**
     * Remove the view
     *
     * Override Backbone's method, as we don't want to remove the container from the view.
     *
     * @return {JIRA.Components.IssueViewer.Views.Error} this
     */
    remove: function () {
        this.stopListening();
        this.$el.empty();
        return this;
    },

    /**
     * Handler for render event
     */
    onRender: function () {
        // $el has been modified outside this view, we need to restore the className
        this.$el.addClass(this.className);
        this.$el.attr("tabindex", "-1");
    }
});
