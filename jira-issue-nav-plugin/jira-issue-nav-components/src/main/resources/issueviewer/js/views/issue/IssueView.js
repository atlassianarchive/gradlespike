AJS.namespace("JIRA.Components.IssueViewer.Views.Issue");

/**
 * @class JIRA.Components.IssueViewer.Views.Issue
 *
 * Main view for rendering an issue
 *
 * @extends JIRA.Marionette.Layout
 */
JIRA.Components.IssueViewer.Views.Issue =  JIRA.Marionette.Layout.extend({
    template: JIRA.Components.IssueViewer.Templates.layout,

    className: "issue-container",

    regions: {
        header: {
            selector: ".issue-header-container",
            regionType: JIRA.Marionette.ReplaceRegion
        },
        body: {
            selector: ".issue-body-container",
            regionType: JIRA.Marionette.ReplaceRegion
        }
    },

    modelEvents: {
        "updated": "update"
    },

    /**
     * Show the loading indication
     */
    showLoading: function () {
        this.$el.addClass("loading");
    },

    /**
     * Hide the loading indication
     */
    hideLoading: function () {
        this.$el.removeClass("loading");
    },

    /**
     * Focus a comment
     *
     * @param {string} commentId Comment to focus
     */
    focusComment: function (commentId) {
        this.$("#activitymodule .focused").removeClass("focused");
        this.$("#comment-" + commentId).addClass("focused");
        this.$(".js-stalker").trigger("refresh");
    },

    /**
     * Update this view with the data form the model
     *
     * @param props
     */
    update: function (props) {
        // Only render this view if this is the initial view.
        if (props.initialize) {
            this.render();
        }

        // If a comment has been edited, focus it
        if (props.reason && props.reason.action === JIRA.Issues.Actions.EDIT_COMMENT) {
            this.focusComment(props.reason.meta.commentId);
        }

        this._bringViewIssueElementIntoView();
    },

    /**
     * Remove the view
     *
     * Override Backbone's method, as we don't want to remove the container from the view.
     *
     * @returns {*}
     */
    remove: function () {
        this.stopListening();
        this.$el.empty();
        return this;
    },

    /**
     * Handler for render events, things to do after the template has been rendered
     */
    onRender: function () {
        // $el could have been modified outside this view, we need to restore the className
        this.$el.addClass(this.className);
        this.$el.attr("tabindex", "-1");
        this._bringViewIssueElementIntoView();

        JIRA.trigger(JIRA.Events.NEW_CONTENT_ADDED, [this.$el, JIRA.CONTENT_ADDED_REASON.pageLoad]);
        if (JIRA.Events.REFRESH_TOGGLE_BLOCKS) {
            JIRA.trigger(JIRA.Events.REFRESH_TOGGLE_BLOCKS, [this.model.getId()]);
        }
    },

    onApplyToDom: function() {
        this._bringViewIssueElementIntoView();
    },

    /**
     * Bring some parts of the issue into view (eg: scrolls to focused comment)
     *
     * @private
     */
    _bringViewIssueElementIntoView: function () {
        var viewIssueQuery = this.model.get("entity").viewIssueQuery;
        if(viewIssueQuery) {
            var elementSelector;
            if(viewIssueQuery.focusedCommentId) {
                elementSelector = "#comment-" + viewIssueQuery.focusedCommentId;
            } else if(viewIssueQuery.attachmentSortBy || viewIssueQuery.attachmentOrder) {
                elementSelector = "#attachmentmodule";
            }

            if (elementSelector) {
                AJS.$(elementSelector).scrollIntoView({
                    marginBottom: 200,
                    marginTop: 200
                });
            }
        }
    }
});
