AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer-test");

module('JIRA.Components.IssueViewer.Views.IssuePanel', {
    setup: function() {
        //Stop inline layer from appending to window.top so qunit iframe runner works
        AJS.params.ignoreFrame = true;
        this.el = AJS.$("<div></div>");
        this.issuePanelModel = new JIRA.Components.IssueViewer.Models.Panel({entity: mockWebPanelRefPluginPanel});
        this.issuePanelView = new JIRA.Components.IssueViewer.Views.IssuePanel({el:this.el, model: this.issuePanelModel, issueEventBus: new JIRA.Components.IssueViewer.Legacy.IssueEventBus()});
    },
    teardown: function() {
        this.el.remove();
        if (this.$elToRemove) {
            this.$elToRemove.remove();
        }
    }
});

test("Issue Web panel renders correctly with no header", function() {
    var issuePanelModel = new JIRA.Components.IssueViewer.Models.Panel({entity: mockWebPanelNoHeader});
    var issuePanelView = new JIRA.Components.IssueViewer.Views.IssuePanel({el:this.el, model: issuePanelModel});

    var $el = issuePanelView.render().$el;

    //make sure that we don't render a header on the client-side.
    equal($el.find(".mod-header").length, 0, "No heading means no mod-header");

    //this is actually all stuff that's in the html returned by the module.
    ok($el.is("#addcomment"), "Rendered element contains correct id");
    equal($el.find(".mod-content").length, 1, "Rendered element contains mod-content");
    equal($el.find("#footer-comment-button").length, 1, "Rendered element contains module contents");
});

test("Issue Web panel renders correctly with header", function() {
    var issuePanelModel = new JIRA.Components.IssueViewer.Models.Panel({entity: mockWebPanelWithHeader});
    var issuePanelView = new JIRA.Components.IssueViewer.Views.IssuePanel({el:this.el, model: issuePanelModel});

    var $el = issuePanelView.render().$el;

    //make sure that we do render a header on the client-side.
    ok($el.hasClass("toggle-wrap"));
    equal($el.find(".mod-header h2:contains(Details)").length, 1, "Header is rendered");

    //this is actually all stuff that's in the html returned by the module.
    equal($el.find(".mod-content #issuedetails").length, 1, "Rendered element contains correct mod-content");
});

test("Issue Web panel from the reference plugin renders correctly", function() {
    var issuePanelModel = new JIRA.Components.IssueViewer.Models.Panel({entity: mockWebPanelRefPluginPanel});
    var issuePanelView = new JIRA.Components.IssueViewer.Views.IssuePanel({el:this.el, model: issuePanelModel});

    var $el = issuePanelView.render().$el;
    this.$elToRemove = $el;
    AJS.$("#qunit-fixture").append($el);
    JIRA.trigger(JIRA.Events.NEW_CONTENT_ADDED, [$el]);

    //make sure that the header has the correct controls
    var $ops = $el.find("ul.ops");
    equal($ops.length, 1, "Operations list is present");
    var $opsList = $ops.children("li");
    equal($opsList.length, 3, "Correct number of operations is present");
    equal(AJS.$($opsList[0]).find("a.icon-add16").length, 1, "First operation is correct");
    equal(AJS.$($opsList[1]).find("a.icon-edit-sml").length, 1, "Second operation is correct");
    equal(AJS.$($opsList[2]).find("a.drop-menu").length, 1, "Third operation is a dropdown");


    var $dropdown = $ops.find(".aui-dropdown-content");
    equal($dropdown.children("ul.aui-list-section").length, 3, "Dropdown contains correct subgroups");
    equal($dropdown.children("h5:contains(Drop Section)").length, 1, "Dropdown contains correct subheading");

    //check opening the dropdown works
    equal(AJS.$("body > .ajs-layer #refViewIssue-drop-add-link").length, 0, "Dropdown is currently closed");
    var $ddTrigger = AJS.$($opsList[2]).find("a.drop-menu");
    $ddTrigger.click();
    equal(AJS.$("body > .ajs-layer #refViewIssue-drop-add-link").length, 1, "Dropdown is currently open");

    //cleanup (should close the dropdown)
    $ddTrigger.click();
});


test("JRADEV-10219: Only updating sections of panel that are not in edit", function () {
    this.issuePanelView.render();
    this.issuePanelModel.update(mockWebPanelRefPluginPanelUpdated,["versions"], ["summary", "components"]);

    equal(this.issuePanelView.$el.find("#summary-val").text(), "Summary Updated");
    equal(this.issuePanelView.$el.find("#versions-val").text(), "");
    equal(this.issuePanelView.$el.find("#components-val").text(), "Components Updated");
});

test("JRADEV-10219: Update entire panel if no fields in edit", function () {
    this.issuePanelView.render();

    this.issuePanelModel.update(mockWebPanelRefPluginPanelUpdated, [],  ["summary"]);

    equal(this.issuePanelView.$el.find("#changed-body").text(), "Some changed body text");
    equal(this.issuePanelView.$el.find("#summary-val").text(), "Summary Updated");
    equal(this.issuePanelView.$el.find("#versions-val").text(), "Versions Updated");
    equal(this.issuePanelView.$el.find("#components-val").text(), "Components Updated");
});

test("Update entire panel should trigger a JIRA.Events.NEW_CONTENT_ADDED", function () {
    this.issuePanelView.render();
    var triggerSpy = sinon.spy(JIRA, "trigger");

    this.issuePanelModel.update(mockWebPanelRefPluginPanelUpdated, [],  ["summary"]);

    equal(triggerSpy.callCount, 2, "Two events are triggered");
    equal(triggerSpy.firstCall.args[0], JIRA.Events.PANEL_REFRESHED, "First event is JIRA.Events.PANEL_REFRESHED");
    equal(triggerSpy.secondCall.args[0], JIRA.Events.NEW_CONTENT_ADDED, "Second event is JIRA.Events.NEW_CONTENT_ADDED");
    equal(typeof triggerSpy.secondCall.args[1][1], "string", "Ensure second event has a reason argument");
    equal(triggerSpy.secondCall.args[1][1], JIRA.CONTENT_ADDED_REASON.panelRefreshed, "Second event reason is JIRA.CONTENT_ADDED_REASON.panelRefreshed");

    triggerSpy.restore();
});