AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer-test");

module('JIRA.Components.IssueViewer.Views.Issue', {
    setup: function() {
        this.el = AJS.$("<div class='issue-container'></div>").appendTo("#qunit-fixture");
        this.issueModel = new JIRA.Components.IssueViewer.Models.Issue();
        this.issueModel.updateFromEntity(mockIssueJSON);
        this.controller = new JIRA.Components.IssueViewer.Controllers.Issue({model: this.issueModel});
        this.controller.setElement(this.el);
        this.controller.createView();
        this.view = this.controller.view;
    },
    teardown: function() {
        this.el.remove();
    }
});

//TODO Move to IssueViewBody?
test("Issue Updated via Editing comment focuses comment", function () {
    this.view.render();

    this.issueModel.update({panels: mockWebPanelsJSON, issue: mockIssueJSON}, {
        reason: {
            action: JIRA.Issues.Actions.EDIT_COMMENT,
            meta: {
                commentId: "10270"
            }
        }
    });
    ok(this.view.$("#comment-10270").hasClass("focused"));

});


test("render() should trigger a JIRA.Events.NEW_CONTENT_ADDED", function () {
    var triggerSpy = sinon.spy(JIRA, "trigger");

    this.view.render();

    equal(triggerSpy.callCount, 3, "Three events are triggered");

    equal(triggerSpy.firstCall.args[0], JIRA.Events.NEW_CONTENT_ADDED, "First event is JIRA.Events.NEW_CONTENT_ADDED");
    equal(typeof triggerSpy.firstCall.args[1][1], "string", "Ensure first event has a reason argument");
    equal(triggerSpy.firstCall.args[1][1], JIRA.CONTENT_ADDED_REASON.panelRefreshed, "First event reason is JIRA.CONTENT_ADDED_REASON.panelRefreshed");

    equal(triggerSpy.secondCall.args[0], JIRA.Events.NEW_CONTENT_ADDED, "Second event is JIRA.Events.NEW_CONTENT_ADDED");
    equal(typeof triggerSpy.secondCall.args[1][1], "string", "Ensure second event has a reason argument");
    equal(triggerSpy.secondCall.args[1][1], JIRA.CONTENT_ADDED_REASON.pageLoad, "Second event reason is JIRA.CONTENT_ADDED_REASON.pageLoad");

    equal(triggerSpy.thirdCall.args[0], JIRA.Events.REFRESH_TOGGLE_BLOCKS, "Third event is JIRA.Events.REFRESH_TOGGLE_BLOCKS");
    equal(triggerSpy.thirdCall.args[1][0], 10000, "Third event contains the issue id");

    triggerSpy.restore();
});


/* TODO
also add an error case you want to test that it remains in edit mode on an invalid response (not sure why this is todo)
test("Model saving events call correct methods", function () {

    var editingStartedSpy = sinon.spy();
    var saveCompleteSpy = sinon.spy();
    var saveStartedSpy = sinon.spy();

    this.view = new (JIRA.Components.IssueViewer.Views.Issue.extend({
        handleEditingStarted: editingStartedSpy,
        handleSaveComplete: saveCompleteSpy,
        handleSaveStarted: saveStartedSpy
    }))({
        model: this.issueModel, el: this.el
    });

    this.issueModel.triggerEditingStarted();
    equal(editingStartedSpy.callCount, 1);
    this.issueModel.triggerSaveStarted();
    equal(saveStartedSpy.callCount, 1);
    this.issueModel.triggerSaveAndReloadComplete();
    this.issueModel.triggerSaveError();
    equal(saveCompleteSpy.callCount, 2);
});
*/
