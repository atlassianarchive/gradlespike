AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer-test");

module('JIRA.Components.IssueViewer.Views.IssueBody', {
    setup: function() {
        this.el = AJS.$("<div></div>");
        this.issuePanelsGroupModel = new JIRA.Components.IssueViewer.Models.PanelsGroup();
        this.issuePanelsGroupModel.update(mockWebPanelsJSON, {editable:true});
        this.issueModel = new JIRA.Components.IssueViewer.Models.Issue({entity:mockIssueJSON});
        this.issueModel.setPanels(this.issuePanelsGroupModel);

        this.controller = new JIRA.Components.IssueViewer.Controllers.Issue({model: this.issueModel});
        this.controller.setElement(this.el);
    },
    teardown: function() {
        this.el.remove();
    },

    assertHasModules:function(column, moduleIds) {
        var $modules = column.find(".module");
        equal($modules.length, moduleIds.length, "Column has correct number of modules");
        $modules.each(function(i, module) {
            equal(AJS.$(module).attr("id"), moduleIds[i], "Module " + moduleIds[i] + " is correct");
        });
    }
});

test("Issue Body View renders all web-panels", function() {
    this.controller.show();
    var $el = this.controller.bodyView.$el;

    var $columns = $el.find(".aui-group .aui-item");
    equal($columns.length, 2, "Correct number of columns rendered");
    var $leftColumn = AJS.$($columns[0]);
    this.assertHasModules($leftColumn, ["details-module", "descriptionmodule", "attachmentmodule", "linkingmodule", "activitymodule", "addcomment"]);
    var $rightColumn = AJS.$($columns[1]);
    this.assertHasModules($rightColumn, ["peoplemodule", "datesmodule"]);
});

test("Issue Body can add and remove web-panels", function() {
    this.controller.show();
    var $el = this.controller.bodyView.$el;

    var $columns = $el.find(".aui-group .aui-item");
    equal($columns.length, 2, "Correct number of columns rendered");
    var $leftColumn = AJS.$($columns[0]);
    this.assertHasModules($leftColumn, ["details-module", "descriptionmodule", "attachmentmodule", "linkingmodule", "activitymodule", "addcomment"]);
    var $rightColumn = AJS.$($columns[1]);
    this.assertHasModules($rightColumn, ["peoplemodule", "datesmodule"]);

    //now remove the attachment module!
    this.issuePanelsGroupModel.update(mockWebPanelsJSONNoAttachments, {editable:true});
    $columns = $el.find(".aui-group .aui-item");
    equal($columns.length, 2, "Correct number of columns rendered");
    $leftColumn = AJS.$($columns[0]);
    this.assertHasModules($leftColumn, ["details-module", "descriptionmodule", "linkingmodule", "activitymodule", "addcomment"]);
    $rightColumn = AJS.$($columns[1]);
    this.assertHasModules($rightColumn, ["peoplemodule", "datesmodule"]);

    //now add the timetracking module!
    this.issuePanelsGroupModel.update(mockWebPanelsJSONWithTimeTracking, {editable:true});
    $columns = $el.find(".aui-group .aui-item");
    equal($columns.length, 2, "Correct number of columns rendered");
    $leftColumn = AJS.$($columns[0]);
    this.assertHasModules($leftColumn, ["details-module", "descriptionmodule", "linkingmodule", "activitymodule", "addcomment"]);
    $rightColumn = AJS.$($columns[1]);
    this.assertHasModules($rightColumn, ["peoplemodule", "datesmodule", "timetracking"]);
});

test("Update from change data", function() {
    this.controller.show();
    var $el = this.controller.bodyView.$el;

    var $columns = $el.find(".aui-group .aui-item");
    equal($columns.length, 2, "Correct number of columns rendered");
    var $leftColumn = AJS.$($columns[0]);
    this.assertHasModules($leftColumn, ["details-module", "descriptionmodule", "attachmentmodule", "linkingmodule", "activitymodule", "addcomment"]);
    var $rightColumn = AJS.$($columns[1]);
    this.assertHasModules($rightColumn, ["peoplemodule", "datesmodule"]);

    var props = {
        editable: true,
        changed: {
            added: {
                panels:{
                    leftPanels: [],
                    rightPanels: ["timetracking"],
                    infoPanels: []
                }
            },
            updated: {
                panels:{
                    leftPanels: ["details-module"],
                    rightPanels: ["peoplemodule"],
                    infoPanels: []
                }
            },
            deleted: {
                panels:{
                    leftPanels: ["attachmentmodule"],
                    rightPanels: [],
                    infoPanels: []
                }
            }
        }
    };

    var newPanels = mockWebPanelsJSONNoAttachments;
    newPanels.rightPanels = mockWebPanelsJSONWithTimeTracking.rightPanels;
    newPanels.leftPanels[0].label = "Details-changed";
    newPanels.rightPanels[0].label = "People-changed";

    this.issuePanelsGroupModel.update(newPanels, props);

    $columns = $el.find(".aui-group .aui-item");
    equal($columns.length, 2, "Correct number of columns rendered");
    $leftColumn = AJS.$($columns[0]);
    this.assertHasModules($leftColumn, ["details-module", "descriptionmodule", "linkingmodule", "activitymodule", "addcomment"]);
    $rightColumn = AJS.$($columns[1]);
    this.assertHasModules($rightColumn, ["peoplemodule", "datesmodule", "timetracking"]);

    equal($leftColumn.find("#details-module .toggle-title").text(), "Details-changed");
    equal($rightColumn.find("#peoplemodule .toggle-title").text(), "People-changed");
});

test("addPanel() should trigger a JIRA.Events.NEW_CONTENT_ADDED", function () {
    var panels = this.issuePanelsGroupModel.getLeftPanels();
    this.controller.show();

    var triggerSpy = sinon.spy(JIRA, "trigger");
    panels.add({id: 'fake-panel', entity: {id: 'fake-panel'}});

    equal(triggerSpy.callCount, 1, "One event is triggered");
    equal(triggerSpy.firstCall.args[0], JIRA.Events.NEW_CONTENT_ADDED, "Event is JIRA.Events.NEW_CONTENT_ADDED");
    equal(typeof triggerSpy.firstCall.args[1][1], "string", "Ensure event has a reason argument");
    equal(triggerSpy.firstCall.args[1][1], JIRA.CONTENT_ADDED_REASON.panelRefreshed, "Event reason is JIRA.CONTENT_ADDED_REASON.panelRefreshed");

    triggerSpy.restore();
});