AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer-test");

module('JIRA.Components.IssueViewer.Views.IssueOpsbar', {
    setup: function() {
        this.el = AJS.$("<div><a href='#' id='savebar-cancel'></a><a href='#' id='savebar-save'></a></div>");
        this.issueModel = new JIRA.Components.IssueViewer.Models.Issue({ entity:mockIssueJSON });
        this.opsbarView = new JIRA.Components.IssueViewer.Views.IssueOpsbar({el:this.el, model:this.issueModel});
    },
    teardown: function() {
        this.el.remove();
    },

    findRecursivelyObjectWithPropertyAndValue: function(object, propertyName, propertyValue) {
        var res;
        for (var key in object) {
            if (object.hasOwnProperty(key)) {
                var value = object[key];
                if (key == propertyName && _.isEqual(propertyValue, value)) {
                    return object;
                }

                if (_.isArray(value)) {
                    for (var i = 0; i < value.length; i++) {
                        res = this.findRecursivelyObjectWithPropertyAndValue(value[i], propertyName, propertyValue);
                        if (res) {
                            return res;
                        }
                    }
                }

                if (_.isObject(value)) {
                    res = this.findRecursivelyObjectWithPropertyAndValue(value, propertyName, propertyValue);
                    if (res) {
                        return res;
                    }
                }

            }
        }
        return null;
    },

    groupContainsLinks: function(group, linkIds) {
        var $links = AJS.$(group).find(".toolbar-item>a");
        equal($links.length, linkIds.length, "Group has correct number of links");
        $links.each(function(i, link) {
            equal(AJS.$(link).attr("id"), linkIds[i], "Link " + linkIds[i] + " is correct");
        });
    }
});

test("Issue Opsbar top level groups", function() {
    var $el = this.opsbarView.render().$el;

    equal($el.find(".toolbar-split").length, 2, "The opsbar is split into lhs and rhs toolbars");
});

test("Issue Opsbar LHS groups", function() {
    var $el = this.opsbarView.render().$el;

    var leftGroups = $el.find(".toolbar-split-left .toolbar-group");
    equal(leftGroups.length, 3, "LHS toolbar is split into 3 subgroups");

    var editGroup = leftGroups[0];
    this.groupContainsLinks(editGroup, ["edit-issue"]);

    var issueOperationsGroup = leftGroups[1];
    this.groupContainsLinks(issueOperationsGroup, ["assign-issue", "comment-issue"]);

    var $opsDropdown = AJS.$(issueOperationsGroup).find("#opsbar-operations_more");
    ok($opsDropdown.length > 0, "Issue Ops group contains dropdown");
    equal($opsDropdown.parent().find(".aui-list-item").length, 9, "Issue operations dropdown contains the correct number of links");

    var transitionsGroup = leftGroups[2];
    this.groupContainsLinks(transitionsGroup, ["action_id_4", "action_id_5", "action_id_2"]);

    var $transitionsDropdown = AJS.$(transitionsGroup).find("#opsbar-transitions_more");
    ok($transitionsDropdown.length == 0, "Issue transitions group does not contain dropdown");
});

test("Issue Opsbar LHS groups with 2 unpromoted workflow transitions", function() {
    // let's extend our mockIssue data with an extra workflow transition
    var target = _.extend({}, mockIssueJSON);
    var links = this.findRecursivelyObjectWithPropertyAndValue(target, "links", [
        {"id": "action_id_2", "styleClass": "issueaction-workflow-transition", "label": "Close Issue", "href": "/jira/secure/WorkflowUIDispatcher.jspa?id=10000&action=2&atl_token="}
    ]).links;
    links.push({"id": "action_id_12", "styleClass": "issueaction-workflow-transition", "label": "Another Transition", "href": "/jira/secure/WorkflowUIDispatcher.jspa?id=10000&action=12&atl_token="})
    this.issueModel = new JIRA.Components.IssueViewer.Models.Issue({ entity:target});

    var $el = this.opsbarView.render().$el;

    var leftGroups = $el.find(".toolbar-split-left .toolbar-group");
    equal(leftGroups.length, 3, "LHS toolbar is split into 3 subgroups");

    var transitionsGroup = leftGroups[2];
    this.groupContainsLinks(transitionsGroup, ["action_id_4", "action_id_5"]);

    var $transitionsDropdown = AJS.$(transitionsGroup).find("#opsbar-transitions_more");
    ok($transitionsDropdown.length > 0, "Issue transitions group contains dropdown");
    equal($transitionsDropdown.parent().find(".aui-list-item").length, 2, "Issue transitions dropdown contains the correct number of links");
});


test("Issue Opsbar RHS groups", function() {
    var $el = this.opsbarView.render().$el;

    var rightGroups = $el.find(".toolbar-split-right .toolbar-group");
    equal(rightGroups.length, 1, "RHS toolbar contains only one subgroup");

    this.groupContainsLinks(rightGroups[0], ["jira-share-trigger"]);

    var $viewsDropdown = rightGroups.find(".toolbar-trigger");
    ok($viewsDropdown.length > 0, "Ops group contains dropdown");
    equal($viewsDropdown.parent().find(".aui-list-item").length, 3, "Views dropdown contains correct number of views.");
});

test("Long label is truncated", function() {
    var entity = this.issueModel.getEntity();
    entity.operations.linkGroups[0].groups[0].links[0].label = "Editing an issue using a very long operation label";

    var $el = this.opsbarView.render().$el;
    equal(jQuery.trim($el.find("#edit-issue").text()), "Editing an issue using...", "Long Edit issue label should have been truncted");
});

/**
 * If there is no content in a dropdown do not display it.
 */
test("Unecessary dropdowns", function() {
    var entity = this.issueModel.getEntity();
    // Remove contents from More Actions dropdown
    entity.operations.linkGroups[0].groups[1].groups[0].groups = [];
    // Remove links from workflow group
    entity.operations.linkGroups[0].groups[2].links = [];
    entity.operations.linkGroups[0].groups[2].groups[0].groups = [];

    var $el = this.opsbarView.render().$el;

    var $hasMoreDropdown = $el.has("span#opsbar-operations_more").length;
    ok(!$hasMoreDropdown);

    var $hasWorkflowDropdown = $el.has("span#opsbar-transitions_more").length;
    ok(!$hasWorkflowDropdown);
});

