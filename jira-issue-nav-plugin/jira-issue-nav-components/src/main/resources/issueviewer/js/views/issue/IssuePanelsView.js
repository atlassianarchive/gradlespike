AJS.namespace("JIRA.Components.IssueViewer.Views.IssuePanels");

/**
 * @class JIRA.Components.IssueViewer.Views.IssuePanels
 *
 * This view is used to render a collection of panels
 *
 * @extends Marionette.CollectionView
 */
JIRA.Components.IssueViewer.Views.IssuePanels = JIRA.Marionette.CollectionView.extend({

    itemView: JIRA.Components.IssueViewer.Views.IssuePanel,

    /**
     * Flag to known if we are updating from the DOM or rendering from a template.
     */
    _updatingFromDom: false,

    /**
     * Override Marionette's appendHtml to not ignore the index (why the default implementation ignores a documented
     * parameter?)
     *
     * See https://github.com/marionettejs/backbone.marionette/blob/master/docs/marionette.collectionview.md#collectionviews-appendhtml
     *
     * @param {JIRA.Components.IssueViewer.Views.IssuePanels} collectionView This view
     * @param {JIRA.Components.IssueViewer.Views.IssuePanel} itemView View being added
     * @param {number} index Position of the itemView
     */
    appendHtml: function (collectionView, itemView, index) {
        var children = collectionView.$el.children();
        var childrenLength = children.length;

        if (index <= 0) {
            // If we want to insert the element at the beginning, just prepend it.
            collectionView.$el.prepend(itemView.$el);
        } else if (!childrenLength || index >= childrenLength) {
            // If the collection has no children, or the desired position is bigger than the number of children,
            // append it to the end
            collectionView.$el.append(itemView.$el);
        } else {
            // Insert the child at the requested index
            itemView.$el.insertBefore(children.eq(index));
        }
    },

    /**
     * Update this view with a pre-rendered markup
     */
    applyToDom: function () {
        var instance = this;

        // We need to use this flag to prevent renderItemView() and buildItemView() to destroy
        // the existing markup
        this._updatingFromDom = true;

        // For each rendered module, capture the ID and add an item to our model, so upcoming updates
        // can address the right panel
        this.$el.children().filter('.module').each(function () {
            var id = this.id;
            if (id === "addcomment") {
                // The id in the DOM is different to the entityId
                id = "addcommentmodule";
            }
            instance.collection.add(new JIRA.Components.IssueViewer.Models.Panel({
                id: id,
                entity: {id: id}
            }));
        });

        this._updatingFromDom = false;
    },

    /**
     * Renders the item view.
     *
     * Overrides Marionette's implementation to avoid rendering the item if we are updating from the DOM
     */
    renderItemView: function () {
        if (!this._updatingFromDom) {
            JIRA.Marionette.CollectionView.prototype.renderItemView.apply(this, arguments);
        }
    },

    /**
     * Creates the view instance used to render the item.
     *
     * Overrides Marionette's implementation to use the pre-rendered DOM element if we are updating from the DOM.
     *
     * @param {JIRA.Components.IssueViewer.Models.Panel} item Model used by the item view
     * @returns {JIRA.Components.IssueViewer.Views.IssuePanel}
     */
    buildItemView: function (item) {
        var view = JIRA.Marionette.CollectionView.prototype.buildItemView.apply(this, arguments);
        if (this._updatingFromDom) {
            view.setElement(this.$el.find("#" + item.id));
        }
        return view;
    }
});
