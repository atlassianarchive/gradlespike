AJS.namespace("JIRA.Components.IssueViewer.Views.ErrorNoPermission");

/**
 * @class JIRA.Components.IssueViewer.Views.ErrorNoPermission
 * Renders the error for NotPermissions when viewing/editing an issue
 *
 * @extends JIRA.Components.IssueViewer.Views.Error
 */
JIRA.Components.IssueViewer.Views.ErrorNoPermission = JIRA.Components.IssueViewer.Views.Error.extend({
    template: JIRA.Components.IssueViewer.Templates.Error.NoPermission
});
