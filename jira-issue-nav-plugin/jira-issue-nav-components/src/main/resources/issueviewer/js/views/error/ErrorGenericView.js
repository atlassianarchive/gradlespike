AJS.namespace("JIRA.Components.IssueViewer.Views.ErrorGeneric");

/**
 * @class JIRA.Components.IssueViewer.Views.ErrorGeneric
 * Renders a generic error
 *
 * @extends JIRA.Components.IssueViewer.Views.Error
 */
JIRA.Components.IssueViewer.Views.ErrorGeneric = JIRA.Components.IssueViewer.Views.Error.extend({
    template: JIRA.Components.IssueViewer.Templates.Error.Generic
});