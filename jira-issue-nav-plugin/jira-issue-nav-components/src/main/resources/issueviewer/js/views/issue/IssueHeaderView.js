AJS.namespace("JIRA.Components.IssueViewer.Views.IssueHeader");

/**
 * @class JIRA.Components.IssueViewer.Views.IssueHeader
 *
 * View for rendering the header of an issue. It renders key, summary, breadcrumbs... plus the opsbar and pager as
 * regions
 *
 * @extends JIRA.Marionette.Layout
 */
JIRA.Components.IssueViewer.Views.IssueHeader = JIRA.Marionette.Layout.extend({
    className: "issue-header js-stalker",

    id: "stalker",

    tagName: "header",

    template: JIRA.Components.IssueViewer.Templates.Header.issueHeader,

    modelEvents: {
        "updated": "update"
    },

    regions: {
        opsbar: ".command-bar",
        pager: "#issue-header-pager"
    },

    /**
     * Extract the data from the model in the format needed by the template
     *
     * @returns {Object} Data to be rendered by the template
     */
    serializeData: function () {
        return {
            issue: this.model.getEntity(),
            hasProjectShortcut: JIRA.Components.IssueViewer.Services.DarkFeatures.PROJECT_SHORTCUTS.enabled()
        }
    },

    /**
     * Update this view with new data
     *
     * @param options
     */
    update: function (options) {
        if (options.initialize) {
            this._updateWindowTitle();
        } else {
            var editingSummary = _.include(options.fieldsInProgress, "summary");
            if (editingSummary) {
                this.renderOpsBar();
            } else {
                this.render();
            }
        }
        this.trigger("updated");
    },

    /**
     * Handler for applyToDom event, things to do after $el has been loaded from the DOM
     */
    onApplyToDom: function () {
        var view = new JIRA.Components.IssueViewer.Views.IssueOpsbar({model: this.model});
        // Since ops bar already is in the dom, we should use the current dom data as the view's element
        view.setElement(this.opsbar.el);
        this.opsbar.attachView(view);
        this.opsbar.currentView.applyToDom();
    },

    /**
     * Handler for render event, things to do after the template has been rendered
     */
    onRender: function () {
        this.renderOpsBar();
        this._updateWindowTitle();
        this.trigger("panelRendered", "header", this.$el);
    },

    /**
     * Render the operations bar
     *
     * //TODO This composition should be done by the IssueController
     */
    renderOpsBar: function () {
        this.opsbar.show(new JIRA.Components.IssueViewer.Views.IssueOpsbar({model: this.model}));
        //TODO This event should be thrown by the IssueController
        JIRA.trigger(JIRA.Events.NEW_CONTENT_ADDED, [this.$el, JIRA.CONTENT_ADDED_REASON.panelRefreshed]);
    },

    /**
     * Updates the window title to contain information about the issue
     *
     * @private
     */
    _updateWindowTitle: function () {
        var entity = this.model.getEntity();
        var key = entity.key;
        var summary = entity.summary;
        var appTitle = AJS.Meta.get("app-title");

        if (appTitle && summary && key) {
            document.title = "[" + key + "] " + summary + " - " + appTitle;
        }
    }
});
