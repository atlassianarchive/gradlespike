AJS.namespace("JIRA.Components.IssueViewer.Views.IssueOpsbar");

/**
 * @class JIRA.Components.IssueViewer.Views.IssueOpsbar
 *
 * This view renders the operations bar of an issue (i.e. the buttons and dropdowns in the header)
 *
 * @extends JIRA.Marionette.ItemView
 */
JIRA.Components.IssueViewer.Views.IssueOpsbar = JIRA.Marionette.ItemView.extend({

    template: JIRA.Components.IssueViewer.Templates.Header.opsbar,

    ui: {
        shareButton: ".viewissue-share",
        exportButton: "#viewissue-export",
        loginButton: "#ops-login-lnk",
        commentButton: "#comment-issue"
    },

    /**
     * Extract the data from the model in the format needed by the template
     *
     * @returns {Object} Data to be rendered by the template
     */
    serializeData: function () {
        return {issue: this.model.getEntity()}
    },

    /**
     * Handler for applyToDom event, things to do after $el has been loaded from the DOM
     */
    onApplyToDom: function () {
        this.onRender();
    },

    /**
     * Handler for render event, things to do after the template has been rendered
     */
    onRender: function () {
        //TODO Why do we need to mess with the DOM here? Can we move these changes to the template?

        // Hide the login button
        this.ui.loginButton.hide();

        // Ensure the comment button does not display the add comment dialog
        this.ui.commentButton.addClass("inline-comment");

        // Add the tipsy to the share button
        new JIRA.Issues.Tipsy({
            el: this.ui.shareButton,
            showCondition: ":not(.active)"
        });

        // Add the tipsy to the export button
        new JIRA.Issues.Tipsy({
            el: this.ui.exportButton,
            showCondition: ":not(.active)",
            tipsy: {
                gravity: "ne"
            }
        });
    }
});
