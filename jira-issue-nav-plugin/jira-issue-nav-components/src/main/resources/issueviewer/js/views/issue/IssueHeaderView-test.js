AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer-test");

module("JIRA.Components.IssueViewer.Views.IssueHeader", {
    setup: function () {
        this.issueModel = new JIRA.Components.IssueViewer.Models.Issue();
        this.issueModel.updateFromEntity(mockIssueJSON);
        this.headerView = new JIRA.Components.IssueViewer.Views.IssueHeader({model:this.issueModel});
    },

    checkStructure: function(options, expectedSummary) {
        var $el = this.headerView.render().$el;

        var $projectAvatar = $el.find("#project-avatar");
        ok($projectAvatar.attr("src").indexOf("secure/projectavatar?pid=10000&avatarId=10011") >=0, "Project avatar image has the correct url");

        var $breadcrumbs = $el.find(".aui-nav-breadcrumbs");
        equal($breadcrumbs.find("#project-name-val").text(), "homosapien", "Breadcrumb has correct project name");
        equal($breadcrumbs.find("#key-val").text(), "HSP-1", "Breadcrumb has correct issue key");

        var $summary = $el.find("h1");
        equal($summary.text(), expectedSummary);

        ok($el.find(".command-bar").length > 0, "Opsbar is present");
    }
});

test("IssueHeaderView structure", function() {
    this.checkStructure(null, "Bug 1", "Issue summary heading is correct");
});

test("Updating from changed data", function () {
    this.headerView.render();

    var options = {
        changed: {
            updated: {
                issue: ["summary"]
            },
            added: {},
            deleted: {}
        }
    };

    var entity = this.issueModel.getEntity();
    entity.summary = "Changed summary";

    this.checkStructure(options, entity.summary);
});

test("Subtasks maintain parent issue links", function() {
    var entity = this.issueModel.getEntity();
    entity.parent = {
        id: "test-id",
        key: "TEST-1",
        summary: "parent summary"
    };

    var $el = this.headerView.render().$el;

    var $parentSummary = $el.find("#parent_issue_summary");
    ok($parentSummary.length, "Parent link should exist");
    equal($parentSummary.text(), "TEST-1 parent summary");
    equal($parentSummary.attr("title"), "parent summary");
});

test("Window title is updated on initial update", function () {
    var spy = this.spy(this.headerView, "_updateWindowTitle");

    this.issueModel.triggerUpdated({initialize: true});

    sinon.assert.calledOnce(spy);
});

test("Window title is updated if summary isn't being edited", function () {
    var spy = this.spy(this.headerView, "_updateWindowTitle");

    this.issueModel.triggerUpdated({fieldsInProgress: []});

    sinon.assert.calledOnce(spy);
});

test("Window title isn't updated if summary is being edited", function () {
    var spy = this.spy(this.headerView, "_updateWindowTitle");

    this.issueModel.triggerUpdated({fieldsInProgress: ["summary"]});

    sinon.assert.notCalled(spy);
});

test("Header View is re-rendered when not initializing or editing summary", function () {
    var spy = this.spy();
    this.headerView.on("render", spy);

    this.issueModel.triggerUpdated({fieldsInProgress: []});

    sinon.assert.calledOnce(spy);
});

test("Header View is not re-rendered when not initializing and summary is being edited", function () {
    var spy = this.spy();
    this.headerView.on("render", spy);

    this.issueModel.triggerUpdated({fieldsInProgress: ["summary"]});

    sinon.assert.notCalled(spy);
});

test("Ops bar view is re-shown when not initializing or editing summary", function () {
    var spy = this.spy(this.headerView.opsbar, "show");

    this.issueModel.triggerUpdated({fieldsInProgress: []});

    sinon.assert.calledOnce(spy);
    ok(spy.firstCall.args[0] instanceof JIRA.Components.IssueViewer.Views.IssueOpsbar);
});

test("Ops bar view is re-shown when not initializing and summary is being edited", function () {
    var spy = this.spy(this.headerView.opsbar, "show");

    this.issueModel.triggerUpdated({fieldsInProgress: ["summary"]});

    sinon.assert.calledOnce(spy);
    ok(spy.firstCall.args[0] instanceof JIRA.Components.IssueViewer.Views.IssueOpsbar);
});

test("render()  should trigger a JIRA.Events.NEW_CONTENT_ADDED", function () {
    var triggerSpy = this.spy(JIRA, "trigger");

    this.headerView.render();

    sinon.assert.calledOnce(triggerSpy);
    equal(triggerSpy.firstCall.args[0], JIRA.Events.NEW_CONTENT_ADDED, "First event is JIRA.Events.NEW_CONTENT_ADDED");
    equal(triggerSpy.firstCall.args[1][1], JIRA.CONTENT_ADDED_REASON.panelRefreshed, "First event reason is JIRA.CONTENT_ADDED_REASON.panelRefreshed");

    triggerSpy.reset();

    this.headerView.render({
        changed: {
            updated: {
                issue: ["summary"]
            },
            added: {},
            deleted: {}
        }
    });

    sinon.assert.calledOnce(triggerSpy);
    equal(triggerSpy.firstCall.args[0], JIRA.Events.NEW_CONTENT_ADDED, "First event is JIRA.Events.NEW_CONTENT_ADDED");
    equal(triggerSpy.firstCall.args[1][1], JIRA.CONTENT_ADDED_REASON.panelRefreshed, "First event reason is JIRA.CONTENT_ADDED_REASON.panelRefreshed");

    triggerSpy.reset();

    this.headerView.render({mergeIntoCurrent: false});

    sinon.assert.calledOnce(triggerSpy);
    equal(triggerSpy.firstCall.args[0], JIRA.Events.NEW_CONTENT_ADDED, "First event is JIRA.Events.NEW_CONTENT_ADDED");
    equal(triggerSpy.firstCall.args[1][1], JIRA.CONTENT_ADDED_REASON.panelRefreshed, "First event reason is JIRA.CONTENT_ADDED_REASON.panelRefreshed");
});

test("renderOpsBar() should trigger a JIRA.Events.NEW_CONTENT_ADDED", function () {
    var triggerSpy = this.spy(JIRA, "trigger");

    this.headerView.renderOpsBar();

    sinon.assert.calledOnce(triggerSpy);
    equal(triggerSpy.firstCall.args[0], JIRA.Events.NEW_CONTENT_ADDED, "Event is JIRA.Events.NEW_CONTENT_ADDED");
    equal(typeof triggerSpy.firstCall.args[1][1], "string", "Ensure event has a reason argument");
    equal(triggerSpy.firstCall.args[1][1], JIRA.CONTENT_ADDED_REASON.panelRefreshed, "Event reason is JIRA.CONTENT_ADDED_REASON.panelRefreshed");
});

test("applyToDom() should attach an IssueOpsbar to opsbar region", function () {
    var attachViewSpy = this.spy(this.headerView.opsbar, "attachView");

    this.headerView.applyToDom();

    sinon.assert.calledOnce(attachViewSpy);
    ok(attachViewSpy.firstCall.args[0] instanceof JIRA.Components.IssueViewer.Views.IssueOpsbar, "IssueOpsbar is typ of parameter passed to attachView.");
});

test("applyToDom() should call applyToDom() on the currentView of the region", function () {
    var applyToDomSpy = this.spy();
    var issueOpsbarSpy = this.stub(JIRA.Components.IssueViewer.Views, "IssueOpsbar").returns({
        applyToDom: applyToDomSpy,
        setElement: this.spy()
    });

    this.headerView.applyToDom();

    sinon.assert.calledOnce(applyToDomSpy);
});

test("When the view is updated, it fires the 'updated' event", function() {
    var updateSpy = this.spy();
    this.headerView.on("updated", updateSpy);

    this.headerView.update({});

    sinon.assert.calledOnce(updateSpy);
});