AJS.namespace("JIRA.Components.IssueViewer.Views.IssueBody");

/**
 * @class JIRA.Components.IssueViewer.Views.IssueBody
 *
 * View used to render the body of an issue. It uses the model JIRA.Components.IssueViewer.Models.Issue.
 * This view is just a container for panels, it does not render any data by itself.
 *
 * @extend JIRA.Marionette.Layout
 */
JIRA.Components.IssueViewer.Views.IssueBody = JIRA.Marionette.Layout.extend({
    tagName: "div",

    className: "issue-body-content",

    regions: {
        leftPanels: {
            selector: ".issue-main-column",
            regionType: JIRA.Marionette.LinkRegion
        },
        rightPanels: {
            selector: ".issue-side-column",
            regionType: JIRA.Marionette.LinkRegion
        },
        infoPanels: {
            selector: ".issue-body",
            regionType: JIRA.Marionette.LinkRegion
        }
    },

    template: JIRA.Components.IssueViewer.Templates.Body.issueBody
});
