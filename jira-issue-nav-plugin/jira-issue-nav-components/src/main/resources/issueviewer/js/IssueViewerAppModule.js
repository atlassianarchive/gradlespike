AJS.namespace("JIRA.Components.IssueViewer.AppModule");

JIRA.Components.IssueViewer.AppModule = JIRA.Marionette.AppModule.extend({
    name: "issueViewer",
    generateMasterRequest: true,

    create: function(options) {
        options = _.defaults({}, options, {
            showReturnToSearchOnError: function() { return false;}
        });

        return new JIRA.Components.IssueViewer({
            showReturnToSearchOnError: options.showReturnToSearchOnError
        });
    },

    commands: function() {
        return {
            abortPending: true,
            beforeHide: true,
            beforeShow: true,
            removeIssueMetadata: true,
            updateIssueWithQuery: true,
            close: true,
            setContainer: true,
            dismiss: true
        }
    },

    requests: function() {
        return {
            loadIssue: true,
            canDismissComment: true,
            getIssueId: true,
            getIssueKey: true,
            refreshIssue: true,
            isCurrentlyLoading: true
        }
    },

    events: function() {
        return [
            "loadComplete",
            "loadError",
            "close",
            "render",
            "replacedFocusedPanel"
        ]
    }
});