AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer-test");

module('JIRA.Components.IssueViewer.Legacy.ViewIssueData', {
    setup: function() {
        this.viewIssueData = new JIRA.Components.IssueViewer.Legacy.ViewIssueData();
    },

    createData: function(fields, operationLinkGroups, toolLinks, viewLinks, leftPanels, rightPanels, infoPanels, removedContentIds, summary) {
        var data = {
            value: {
                fields: fields,
                issue: {
                    operations: {
                        linkGroups: []
                    },
                    summary: summary
                },
                panels: {
                    leftPanels: leftPanels,
                    rightPanels: rightPanels,
                    infoPanels: infoPanels
                },
                removedContentIds: removedContentIds
            }
        };

        if (!_.isEmpty(operationLinkGroups)) {
            data.value.issue.operations.linkGroups.push({
                id: "view.issue.opsbar",
                links: [],
                groups: operationLinkGroups
            });
        }

        if (!_.isEmpty(toolLinks) || !_.isEmpty(viewLinks)) {
            data.value.issue.operations.linkGroups.push({
                id: "jira.issue.tools",
                links: toolLinks,
                groups: [
                    {
                        links: viewLinks
                    }
                ]
            });
        }

        return data;
    },

    arrayContentEquals: function(arrayOne, arrayTwo) {
        if (arrayOne.length != arrayTwo.length) {
            return false;
        }

        for (var i = 0; i < arrayOne.length; i++) {
            var elementOne = arrayOne[i];

            var found = false;
            for (var j = 0; j < arrayTwo.length; j++) {
                var elementTwo = arrayTwo[j];

                if (_.isEqual(elementOne, elementTwo)) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                return false;
            }
        }

        return true;
    }
});

test("not mergeIntoCurrent", function() {
    var cachedData = {cached: true};
    var fetchedData = {fetched: true};
    var changedData = this.viewIssueData.mergeFetchedAndCached(cachedData, fetchedData, {});
    equal(changedData, null, "Changed data is null");
    ok(_.isEqual({cached: true, fetched: true}, cachedData), "Fetched data copied into cached data");
});

test("fetched error", function() {
    var cachedData = {cached: true};
    var fetchedData = {fetched: true, error: true};
    var changedData = this.viewIssueData.mergeFetchedAndCached(cachedData, fetchedData, { mergeIntoCurrent: true });
    equal(changedData, null, "Changed data is null");
    ok(_.isEqual({cached: true, fetched: true, error: true}, cachedData), "Fetched data copied into cached data");
});

test("no 'value' of 'issue' in cached data", function() {
    var cachedData = {cached: true};
    var fetchedData = {fetched: true};
    var changedData = this.viewIssueData.mergeFetchedAndCached(cachedData, fetchedData, { mergeIntoCurrent: true });
    equal(changedData, null, "Changed data is null");
    ok(_.isEqual({cached: true, fetched: true}, cachedData), "Fetched data copied into cached data");
});

test("returns null and doesn't modify cached data if nothing changed", function() {
    var cachedData = this.createData(
        [{ id: "field1", editHtml: "html1" }],
        [{ id : "group1", weight: 1, links: [{ id: "operationLink1", weight: 1 }], groups: [] }],
        [{ id: "toolLink1", weight: 1 }],
        [{ id: "viewLink1", weight: 1 }]
        [{ id: "leftPanel1", html: "html1", completeKey: "full:leftPanel1", weight: 1 }],
        [{ id: "rightPanel1", html: "html1", completeKey: "full:rightPanel1", weight: 1 }],
        [{ id: "infoPanel1", html: "html1", completeKey: "full:infoPanel1", weight: 1 }],
        {},
        "Summary"
    );

    var fetchedData = this.createData([], [], [], [], [], [], [], {});

    var changedData = this.viewIssueData.mergeFetchedAndCached(cachedData, fetchedData, { mergeIntoCurrent: true });
    equal(changedData, null, "Changed data is null");

    var expectedCachedData = this.createData(
        [{ id: "field1", editHtml: "html1" }],
        [{ id : "group1", weight: 1, links: [{ id: "operationLink1", weight: 1 }], groups: [] }],
        [{ id: "toolLink1", weight: 1 }],
        [{ id: "viewLink1", weight: 1 }]
        [{ id: "leftPanel1", html: "html1", completeKey: "full:leftPanel1", weight: 1 }],
        [{ id: "rightPanel1", html: "html1", completeKey: "full:rightPanel1", weight: 1 }],
        [{ id: "infoPanel1", html: "html1", completeKey: "full:infoPanel1", weight: 1 }],
        {},
        "Summary"
    );

    ok(_.isEqual(expectedCachedData, cachedData), "Cached data not changed");
});

test("merges changed data into cached issue and returns change info", function() {
    var cachedData = this.createData(
        [{ id: "field1", editHtml: "html1" },
            { id: "field2", editHtml: "html2" },
            { id: "field3", editHtml: "html3" }],

        [
            // Adding, deleting, changing promoted links
            { id : "group1", weight: 1, links: [
                { id: "operationLink11", label: "label1", weight: 1 },
                { id: "operationLink12", label: "label2", weight: 2 },
                { id: "operationLink13", label: "label3", weight: 3 }
            ], groups: [] },
            // Group will be removed
            { id : "group2", weight: 2, links: [
                { id: "operationLink21", label: "label1", weight: 1 }
            ], groups: [] },
            // Remove dropdown link so dropdown gets removed
            { id : "group3", weight: 4, links: [
                    { id: "operationLink31", label: "label1", weight: 1 }
                ],
                groups: [
                    {
                        links: [],
                        groups: [
                            { id: "dropdown-group1", weight: 1, links: [
                                { id: "operationDropdownLink1", label: "label1", weight: 1 }
                            ]}
                        ]
                    }
                ]
            },
            // Dropdown will be added to this group
            { id : "group4", weight: 5, links: [
                { id: "operationLink41", label: "label1", weight: 1 }
            ], groups: [] },
            // Adding, deleting, changing  links in a dropdown group
            { id : "group5", weight: 6, links: [
                { id: "operationLink51", label: "label1", weight: 1 }
            ],
                groups: [
                    {
                        links: [],
                        groups: [
                            { id: "dropdown-group51", weight: 1, links: [
                                { id: "operationDropdownLink511", label: "label1", weight: 1 },
                                { id: "operationDropdownLink512", label: "label2", weight: 2 },
                                { id: "operationDropdownLink513", label: "label3", weight: 3 }
                            ]},
                            // Dropdown group will be removed
                            { id: "dropdown-group52", weight: 2, links: [
                                { id: "operationDropdownLink521", label: "label1", weight: 1 }
                            ]}
                        ]
                    }
                ]
            },
            // Promoting link from dropdown to top-level in the same group.
            { id : "group6", weight: 7, links: [
                { id: "operationLink61", label: "label1", weight: 1 }
            ],
                groups: [
                    {
                        links: [],
                        groups: [
                            { id: "dropdown-group61", weight: 1, links: [
                                { id: "operationDropdownLink611", label: "label1", weight: 2 },
                                { id: "operationDropdownLink612", label: "label2", weight: 3 }
                            ]}
                        ]
                    }
                ]
            },
            // Moving link from one top-level group to another.
            { id : "group7", weight: 8, links: [
                { id: "operationLink71", label: "label1", weight: 1 }
            ], groups: [] },
            { id : "group8", weight: 9, links: [
                { id: "operationLink81", label: "label1", weight: 1 },
                { id: "operationLink82", label: "label2", weight: 2 }
            ], groups: [] },
            // Promoting link from dropdown to top-level in another group.
            { id : "group9", weight: 10, links: [
                { id: "operationLink91", label: "label1", weight: 1 }
            ], groups: [] },
            { id : "group10", weight: 11, links: [
            ],
                groups: [
                    {
                        links: [],
                        groups: [
                            { id: "dropdown-group101", weight: 1, links: [
                                { id: "operationDropdownLink1011", label: "label1", weight: 2 },
                                { id: "operationDropdownLink1012", label: "label2", weight: 3 }
                            ]}
                        ]
                    }
                ]
            },
            // Deleting promoted link
            { id : "group11", weight: 12, links: [
                { id: "operationLink111", label: "label1", weight: 1 },
                { id: "operationLink112", label: "label2", weight: 2 }
            ], groups: [] }
        ],

        [{ id: "toolLink1", label: "label1", weight: 1 },
            { id: "toolLink2", label: "label2", weight: 2 },
            { id: "toolLink3", label: "label3", weight: 3 }
        ],

        [{ id: "viewLink1", label: "label1", weight: 1 },
            { id: "viewLink2", label: "label2", weight: 2 },
            { id: "viewLink3", label: "label3", weight: 3 }
        ],

        [{ id: "leftPanel1", html: "html1", completeKey: "full:leftPanel1", weight: 1 },
            { id: "leftPanel2", html: "html2", completeKey: "full:leftPanel2", weight: 2 },
            { id: "leftPanel3", html: "html3", completeKey: "full:leftPanel3", weight: 3 }],

        [{ id: "rightPanel1", html: "html1", completeKey: "full:rightPanel1", weight: 1 },
            { id: "rightPanel2", html: "html2", completeKey: "full:rightPanel2", weight: 2 },
            { id: "rightPanel3", html: "html3", completeKey: "full:rightPanel3", weight: 3 }],

        [{ id: "infoPanel1", html: "html1", completeKey: "full:infoPanel1", weight: 1 },
            { id: "infoPanel2", html: "html2", completeKey: "full:infoPanel2", weight: 2 },
            { id: "infoPanel3", html: "html3", completeKey: "full:infoPanel3", weight: 3 }],

        {},

        "Summary"
    );

    var fetchedData = this.createData(
        [{ id: "field2", editHtml: "html2-changed" },
            { id: "field-new", editHtml: "html4" }],

        [
            { id : "group1", weight: 1, links: [
                { id: "operationLink12", label: "label2-changed", weight: 2 },
                { id: "operationLink-new", label: "label4", weight: 4 }
            ], groups: []},
            // New group
            { id : "group-new", weight: 3, links: [
                { id: "operationLink-new1", label: "label1", weight: 1 }
            ], groups: []},
            // Add dropdown link so dropdown gets added
            { id : "group4", weight: 5, links: [], groups: [
                {
                    links: [],
                    groups: [
                        { id: "dropdown-group41", weight: 1, links: [
                            { id: "operationDropdownLink41", label: "label1", weight: 1 }
                        ]}
                    ]
                }
            ]},
            { id : "group5", weight: 6, links: [],
                groups: [
                    {
                        links: [],
                        groups: [
                            { id: "dropdown-group51", weight: 1, links: [
                                { id: "operationDropdownLink512", label: "label2-changed", weight: 2 },
                                { id: "operationDropdownLink51-new", label: "label4", weight: 4 }
                            ]},
                            // New dropdown group
                            { id: "dropdown-group53", weight: 3, links: [
                                { id: "operationDropdownLink531", label: "label1", weight: 1 }
                            ]}
                        ]
                    }
                ]
            },
            // Promoting link from dropdown to top-level in the same group.
            { id : "group6", weight: 7, links: [
                { id: "operationDropdownLink611", label: "label1", weight: 2 }
            ], groups: []},
            // Moving link from one top-level group to another.
            { id : "group7", weight: 8, links: [
                { id: "operationLink82", label: "label2", weight: 2 }
            ], groups: [] },
            // Promoting link from dropdown to top-level in another group.
            { id : "group9", weight: 10, links: [
                { id: "operationDropdownLink1011", label: "label1", weight: 2 }
            ], groups: [] }
        ],

        [{ id: "toolLink2", label: "label2-changed", weight: 2 },
            { id: "toolLink-new", label: "label4", weight: 4 }
        ],

        [{ id: "viewLink2", label: "label2-changed", weight: 2 },
            { id: "viewLink-new", label: "label4", weight: 4 }
        ],

        [{ id: "leftPanel2", html: "html2-changed", completeKey: "full:leftPanel2", weight: 2 },
            { id: "leftPanel-new", html: "html4", completeKey: "full:leftPanel-new", weight: 4 }],

        [{ id: "rightPanel2", html: "html2-changed", completeKey: "full:rightPanel2", weight: 2 },
            { id: "rightPanel-new", html: "html4", completeKey: "full:rightPanel-new", weight: 4 }],

        [{ id: "infoPanel2", html: "html2-changed", completeKey: "full:infoPanel2", weight: 2 },
            { id: "infoPanel-new", html: "html4", completeKey: "full:infoPanel-new", weight: 4 }],

        { fields: ["field3"], panels: ["full:leftPanel3", "full:rightPanel3", "full:infoPanel3"],
            links: ["operationLink13", "operationLink21", "operationDropdownLink1", "operationDropdownLink513", "operationDropdownLink521", "operationLink112",
                "toolLink3", "viewLink3"]
        },

        "Summary-changed"
    );

    var changedData = this.viewIssueData.mergeFetchedAndCached(cachedData, fetchedData, { mergeIntoCurrent: true });

    var expectedChangedData = {
        updated: {
            fields: ["field2"],
            panels: {
                leftPanels: ["leftPanel2"],
                rightPanels: ["rightPanel2"],
                infoPanels: ["infoPanel2"]
            },
            groups: {
                "view.issue.opsbar" : ["group1", "group10", "group11", "group3", "group4", "group5", "group6", "group7", "group8", "group9"],
                "jira.issue.tools": true
            },
            issue: ["summary"]
        },
        added: {
            fields: ["field-new"],
            panels: {
                leftPanels: ["leftPanel-new"],
                rightPanels: ["rightPanel-new"],
                infoPanels: ["infoPanel-new"]
            },
            groups: {
                "view.issue.opsbar": ["group-new"]
            }
        },
        deleted: {
            fields: ["field3"],
            panels: {
                leftPanels: ["leftPanel3"],
                rightPanels: ["rightPanel3"],
                infoPanels: ["infoPanel3"]
            },
            groups: {
                "view.issue.opsbar": ["group2"]
            }
        }
    };

    ok(_.isEqual(expectedChangedData, changedData), "Changed data contains all actual changes");

    var expectedCachedData = this.createData(
        [{ id: "field1", editHtml: "html1" },
            { id: "field2", editHtml: "html2-changed" },
            { id: "field-new", editHtml: "html4" }],

        [
            { id : "group1", weight: 1, links: [
                { id: "operationLink11", label: "label1", weight: 1 },
                { id: "operationLink12", label: "label2-changed", weight: 2 },
                { id: "operationLink-new", label: "label4", weight: 4 }
            ], groups: []},
            { id : "group-new", weight: 3, links: [
                { id: "operationLink-new1", label: "label1", weight: 1 }
            ], groups: []},
            { id : "group3", weight: 4, links: [
                { id: "operationLink31", label: "label1", weight: 1 }
            ], groups: [] },
            { id : "group4", weight: 5, links: [
                    { id: "operationLink41", label: "label1", weight: 1 }
                ],
                groups: [
                {
                    links: [],
                    groups: [
                        { id: "dropdown-group41", weight: 1, links: [
                            { id: "operationDropdownLink41", label: "label1", weight: 1 }
                        ]}
                    ]
                }
            ]},
            { id : "group5", weight: 6, links: [
                { id: "operationLink51", label: "label1", weight: 1 }
            ],
                groups: [
                    {
                        links: [],
                        groups: [
                            { id: "dropdown-group51", weight: 1, links: [
                                { id: "operationDropdownLink511", label: "label1", weight: 1 },
                                { id: "operationDropdownLink512", label: "label2-changed", weight: 2 },
                                { id: "operationDropdownLink51-new", label: "label4", weight: 4 }
                            ]},
                            { id: "dropdown-group53", weight: 3, links: [
                                { id: "operationDropdownLink531", label: "label1", weight: 1 }
                            ]}
                        ]
                    }
                ]
            },
            // Promoting link from dropdown to top-level in the same group.
            { id : "group6", weight: 7, links: [
                { id: "operationLink61", label: "label1", weight: 1 },
                { id: "operationDropdownLink611", label: "label1", weight: 2 }
            ],
                groups: [
                    {
                        links: [],
                        groups: [
                            { id: "dropdown-group61", weight: 1, links: [
                                { id: "operationDropdownLink612", label: "label2", weight: 3 }
                            ]}
                        ]
                    }
                ]
            },
            // Moving link from one top-level group to another.
            { id : "group7", weight: 8, links: [
                { id: "operationLink71", label: "label1", weight: 1 },
                { id: "operationLink82", label: "label2", weight: 2 }
            ], groups: [] },
            { id : "group8", weight: 9, links: [
                { id: "operationLink81", label: "label1", weight: 1 }
            ], groups: [] },
            // Promoting link from dropdown to top-level in another group.
            { id : "group9", weight: 10, links: [
                { id: "operationLink91", label: "label1", weight: 1 },
                { id: "operationDropdownLink1011", label: "label1", weight: 2 }
            ], groups: [] },
            { id : "group10", weight: 11, links: [
            ],
                groups: [
                    {
                        links: [],
                        groups: [
                            { id: "dropdown-group101", weight: 1, links: [
                                { id: "operationDropdownLink1012", label: "label2", weight: 3 }
                            ]}
                        ]
                    }
                ]
            },
            // Deleting promoted link
            { id : "group11", weight: 12, links: [
                { id: "operationLink111", label: "label1", weight: 1 }
            ], groups: [] }
        ],

        [{ id: "toolLink1", label: "label1", weight: 1 },
            { id: "toolLink2", label: "label2-changed", weight: 2 },
            { id: "toolLink-new", label: "label4", weight: 4 }
        ],

        [{ id: "viewLink1", label: "label1", weight: 1 },
            { id: "viewLink2", label: "label2-changed", weight: 2 },
            { id: "viewLink-new", label: "label4", weight: 4 }
        ],

        [{ id: "leftPanel1", html: "html1", completeKey: "full:leftPanel1", weight: 1 },
            { id: "leftPanel2", html: "html2-changed", completeKey: "full:leftPanel2", weight: 2 },
            { id: "leftPanel-new", html: "html4", completeKey: "full:leftPanel-new", weight: 4 }],

        [{ id: "rightPanel1", html: "html1", completeKey: "full:rightPanel1", weight: 1 },
            { id: "rightPanel2", html: "html2-changed", completeKey: "full:rightPanel2", weight: 2 },
            { id: "rightPanel-new", html: "html4", completeKey: "full:rightPanel-new", weight: 4 }],

        [{ id: "infoPanel1", html: "html1", completeKey: "full:infoPanel1", weight: 1 },
            { id: "infoPanel2", html: "html2-changed", completeKey: "full:infoPanel2", weight: 2 },
            { id: "infoPanel-new", html: "html4", completeKey: "full:infoPanel-new", weight: 4 }],

        {},

        "Summary-changed"
    );

    ok(this.arrayContentEquals(expectedCachedData.value.fields, cachedData.value.fields), "Fields merged correctly");
    ok(_.isEqual(expectedCachedData.value.panels.leftPanels, cachedData.value.panels.leftPanels), "Left panels merged correctly");
    ok(_.isEqual(expectedCachedData.value.panels.rightPanels, cachedData.value.panels.rightPanels), "Right panels merged correctly");
    ok(_.isEqual(expectedCachedData.value.panels.infoPanels, cachedData.value.panels.infoPanels), "Info panels merged correctly");
    ok(_.isEqual(expectedCachedData.value.issue.operations.linkGroups[0], cachedData.value.issue.operations.linkGroups[0]), "Operation links merged correctly");
    ok(_.isEqual(expectedCachedData.value.issue.operations.linkGroups[1], cachedData.value.issue.operations.linkGroups[1]), "Tool links merged correctly");

});