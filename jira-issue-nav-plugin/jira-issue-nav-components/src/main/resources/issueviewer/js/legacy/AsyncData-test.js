AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer");
AJS.test.require("com.atlassian.jira.jira-issue-nav-components:issueviewer-test");

module('JIRA.Components.IssueViewer.Legacy.AsyncData', {
    setup: function() {
        this.asyncData = new JIRA.Components.IssueViewer.Legacy.AsyncData();
    },

    disableCache: function() {
        this.asyncData = new JIRA.Components.IssueViewer.Legacy.AsyncData({ disableCache: true });
    },

    setTestData: function() {
        this.asyncData.setMultiple({
            1: { value: 'a' },
            2: { value: 'b' },
            3: { value: 'c' },
            4: { value: 'd' },
            5: { value: 'e' }
        });
    },

    /**
     * Requests simply resolve immediately with the id + '-fetched'
     */
    overrideFetch_simple: function() {
        this.asyncData.fetch = function(id) {
            return jQuery.Deferred().resolve(id + '-fetched').promise();
        };
    },

    /**
     * Requests are not immediately resolved/rejected, but can be individually controlled
     */
    overrideFetch_manual: function() {
        var requests = [];

        this.asyncData.fetch = function() {
            var fakeRequest = jQuery.Deferred();
            fakeRequest.abort = function() { fakeRequest.reject() };
            sinon.spy(fakeRequest, 'abort');
            requests.push(fakeRequest);
            return fakeRequest;
        };

        return requests;
    },

    initChangeSpy: function() {
        if (this.changeSpy) {
            this.changeSpy.reset();
        } else {
            this.changeSpy = sinon.spy();
            this.asyncData.on('change', this.changeSpy);
        }
    }

});

test("get() without fetch()", function() {
    expect(2);
    this.setTestData();
    this.asyncData.get(1).done(function(value, meta) {
        equal(value, 'a', "Correct value");
        ok(!meta.error, "No error");
    });
});

test("get() uncached value with default fetch()", function() {
    expect(2);
    this.asyncData.get(1).fail(function(value, meta) {
        equal(value, undefined, "value is undefined");
        equal(meta.error, true, "error flag is set");
    });
});

test("get() with fetch()", function() {
    expect(2);
    this.overrideFetch_simple();
    this.asyncData.get(1).done(function(value, meta) {
        equal(value, '1-fetched', "Correct value");
        ok(!meta.error, "No error");
    });
});

test("get() with a mix of cached and fetched data", function() {
    expect(4);
    this.setTestData();
    this.overrideFetch_simple();
    this.asyncData.get(5).done(function(value, meta) {
        equal(value, 'e', "Correct cached value");
        ok(!meta.error, "No error");
    });
    this.asyncData.get(100).done(function(value, meta) {
        equal(value, '100-fetched', "Correct fetched value");
        ok(!meta.error, "No error");
    });
});

test("get() with forceFetch", function() {
    expect(2);
    this.setTestData();
    this.overrideFetch_simple();
    this.asyncData.get(5, true).done(function(value, meta) {
        equal(value, '5-fetched', "Correct fetched value");
        ok(!meta.error, "No error");
    });
});

test("get() updates cache after fetching", function() {
    expect(3);
    this.setTestData();
    this.overrideFetch_simple();
    var updatedDate = this.asyncData.getMeta(1).updated;
    this.asyncData.get(1, true).done(_.bind(function() {
        equal(this.asyncData.getMeta(1).value, '1-fetched', "Correct value");
        ok(!this.asyncData.getMeta(1).error, "No error");
        ok(this.asyncData.getMeta(1).updated !== updatedDate, "Timestamp updated");
    }, this));
});

test("get() with options passes the options to done handlers", function() {
    expect(1);
    this.overrideFetch_simple();
    this.asyncData.get(1, false, { a: 1 }).done(function(value, meta, options) {
        deepEqual(options, { a: 1 }, "Options is passed");
    });
});

test("get(): 'changed' meta property in done handler", function() {
    this.overrideFetch_simple();
    this.asyncData.get(1).done(function(value, meta) {
        ok(meta.changed, "Changed property is set after first request");
    });

    this.asyncData.get(1, true).done(function(value, meta) {
        ok(!meta.changed, "Changed property is not set after second request");
    });
});

test("get(): 'fromCache meta property in done handler", function() {
    this.setTestData();
    this.asyncData.get(1).done(function(value, meta) {
        ok(meta.fromCache, "fromCache property is set");
    });
    this.asyncData.get(1, true).done(function(value, meta) {
        ok(!meta.fromCache, "fromCache property is not set");
    });
    this.asyncData.get(10).done(function(value, meta) {
        ok(!meta.fromCache, "fromCache property is not set");
    });
});

test("set() to different value", function() {
    expect(2);
    this.setTestData();
    this.asyncData.set(1, 'AA');
    this.asyncData.get(1).done(function(value, meta) {
        equal(value, 'AA', "Correct value");
        ok(!meta.error, "No error");
    });
});

test("set() to null value", function() {
    expect(2);
    this.setTestData();
    this.asyncData.set(1, null);
    this.asyncData.get(1).done(function(value, meta) {
        equal(value, null, "Value was set to null");
        ok(!meta.error, "Error flag was not set");
    });
});

test("setError()", function() {
    expect(2);
    this.setTestData();
    this.asyncData.setError(1);
    this.asyncData.get(1).fail(function(value, meta) {
        equal(value, 'a', "Value wasn't changed");
        ok(meta.error, "Error flag was set");
    });
});

test("setError() with value", function() {
    expect(2);
    this.setTestData();
    this.asyncData.setError(1, "error message");
    this.asyncData.get(1).fail(function(value, meta) {
        equal(value, 'error message', "Value was changed");
        ok(meta.error, "Error flag was set");
    });
});

test("set an error value to a valid one", function() {
    var requests = this.overrideFetch_manual();
    this.asyncData.get(1);
    requests[0].reject('a');

    equal(this.asyncData.getMeta(1).value, 'a', "Error value is set");
    equal(this.asyncData.getMeta(1).error, true, "Error flag is set");

    this.asyncData.get(1, true);
    requests[1].resolve('aa');

    equal(this.asyncData.getMeta(1).value, 'aa', "Value is set");
    equal(this.asyncData.getMeta(1).error, false, "Error flag is set to false");
});

test("set() triggers change event when changing a value", function() {
    this.setTestData();
    this.initChangeSpy();
    this.asyncData.set(2, 'a');
    equal(this.changeSpy.callCount, 1, "change event triggered once");
});

test("set() triggers change event when setting new value", function() {
    this.setTestData();
    this.initChangeSpy();
    this.asyncData.set(100, 'Z');
    equal(this.changeSpy.callCount, 1, "change event triggered once");
});

test("set() triggers change event when setting error", function() {
    this.setTestData();
    this.initChangeSpy();
    this.asyncData.set(1, null);
    equal(this.changeSpy.callCount, 1, "change event triggered once");
});

test("set() doesn't trigger change event when nothing changed", function() {
    this.setTestData();
    this.initChangeSpy();
    this.asyncData.set(1, 'a');
    equal(this.changeSpy.callCount, 0, "change event not triggered");
});

test("setMultiple triggers only one change event", function() {
    this.setTestData();
    this.initChangeSpy();
    this.asyncData.setMultiple({
        1: { value: 'aa' },
        2: { value: 'bb' },
        3: { value: 'cc' },
        4: { value: 'dd' },
        5: { value: 'ee' },
        6: { value: 'ff' }
    });
    equal(this.changeSpy.callCount, 1, "change event triggered once");
});

test("reset()", function() {
    expect(1);
    this.setTestData();
    this.asyncData.reset();
    this.asyncData.get(1).fail(function(value, meta) {
        equal(value, undefined, "No value after reset");
    });
});

test("reset() with initial values", function() {
    expect(1);
    this.setTestData();
    this.asyncData.setMultiple({
        'a': { value: 1 },
        'b': { value: 2 }
    });
    this.asyncData.get('a').done(function(value, meta) {
        equal(value, 1, "Correct value");
    });
});

test("reset() triggers only one change event", function() {
    this.setTestData();
    this.initChangeSpy();
    this.asyncData.reset();
    equal(this.changeSpy.callCount, 1, "change event triggered once");

    this.initChangeSpy();
    this.asyncData.reset({
        'a': { value: 'b'},
        'c': { value: 'd'}
    });
    equal(this.changeSpy.callCount, 1, "change event triggered once");
});

test("get() triggers change event iff fetched value is different", function() {
    expect(2);
    this.setTestData();
    this.overrideFetch_simple();
    this.initChangeSpy();
    this.asyncData.get(1, true).done(_.bind(function() {
        equal(this.changeSpy.callCount, 1, "change event triggered once");

        this.initChangeSpy();
        this.asyncData.get(1, true).done(_.bind(function() {
            equal(this.changeSpy.callCount, 0, "Another change event is not triggered");
        }, this));
    }, this));
});

test("asynchronous get() doesn't modify the cache after a reset()", function() {
    expect(2);

    var requests = this.overrideFetch_manual();

    this.asyncData.get('SYN').done(_.bind(function(value, meta) {
        equal(value, 'ACK', "Asynchronous callback receives expected value");
        ok(_.isEmpty(this.asyncData.getMeta('SYN').value), "Cache is empty");
    }, this));
    this.asyncData.reset();
    requests[0].resolve('ACK');
});

test("concurrent get() requests both resolve but only one fetch is performed", function() {
    expect(6);

    var requests = this.overrideFetch_manual();

    var checkResponse = function(requestName, value, meta) {
        equal(value, 'ACK', requestName + ": Asynchronous callback receives expected value");
        ok(!meta.error, requestName + ": Make sure error flag is not set"); 
    };

    this.asyncData.get('SYN').done(_.bind(checkResponse, this, 'First get()'));
    equal(requests.length, 1, "A fetch was performed after the first get()");

    this.asyncData.get('SYN').done(_.bind(checkResponse, this, 'Second get()'));
    equal(requests.length, 1, "Still only one fetch was performed after the second get()");

    requests[0].resolve('ACK');
});

test("concurrent get() requests abort the initial request on force fetch", function() {
    expect(5);

    var requests = this.overrideFetch_manual();

    var checkResponse = function(requestName, value, meta) {
        equal(value, 'ACK', requestName + ": Asynchronous callback receives expected value");
        ok(!meta.error, requestName + ": Make sure error flag is not set"); 
    };

    this.asyncData.get('SYN').done(_.bind(checkResponse, this, 'First get()'));
    this.asyncData.get('SYN', true).done(_.bind(checkResponse, this, 'Second get()'));

    equal(requests[0].abort.callCount, 1, "First request is aborted after the second get()");

    requests[1].resolve('ACK');
});

test("get() works with disableCache", function() {
    expect(3);
    this.disableCache();
    this.overrideFetch_simple();
    this.asyncData.get(1).done(_.bind(function(value, meta) {
        equal(value, '1-fetched', "Value in done handler is correct");
        ok(meta.updated, 'meta.updated exists in done handler');
        ok(_.isEmpty(this.asyncData.getMeta(1)), "Cache is empty");
    }, this));
});

test("set() doesn't modify cache with disableCache", function() {
    this.disableCache();
    var changed = this.asyncData.set(1, 'a');
    equal(changed, true, "set() returns true as if the cache is modified");
    ok(_.isEmpty(this.asyncData.getMeta(1)), "But cache is still empty");
});

test("setError() doesn't modify cache with disableCache", function() {
    this.disableCache();
    var changed = this.asyncData.setError(1, 'a');
    equal(changed, true, "set() returns true as if the cache is modified");
    ok(_.isEmpty(this.asyncData.getMeta(1)), "But cache is still empty");
});

test("maxCacheSize: 1", function() {
    this.asyncData = new JIRA.Components.IssueViewer.Legacy.AsyncData({
        maxCacheSize: 1
    });
    this.overrideFetch_simple();

    this.asyncData.get(1);
    equal(this.asyncData.hasCached(1), true, "First request is added to the empty cache");

    this.asyncData.get(2);
    this.asyncData.get(3);
    equal(_.keys(this.asyncData.data).length, 1, "Cache size is limited to 1");
    equal(this.asyncData.hasCached(1), false, "First request has been removed from the cache");
    equal(this.asyncData.hasCached(2), false, "Second request has been removed from the cache");
    equal(this.asyncData.hasCached(3), true, "Third request remains in the cache");
});

test("maxCacheSize: 2", function() {
    this.asyncData = new JIRA.Components.IssueViewer.Legacy.AsyncData({
        maxCacheSize: 2
    });
    this.overrideFetch_simple();

    this.asyncData.get(1);
    equal(this.asyncData.hasCached(1), true, "First request is added to the empty cache");

    this.asyncData.get(2);
    this.asyncData.get(3);
    equal(_.keys(this.asyncData.data).length, 2, "Cache size is limited to 2");
    equal(this.asyncData.hasCached(1), false, "First request has been removed from the cache");
    equal(this.asyncData.hasCached(2), true, "Second request remains in the cache");
    equal(this.asyncData.hasCached(3), true, "Third request remains in the cache");
});

test("maxCacheSize causes the least-recently-accessed item to be removed", function() {
    this.asyncData = new JIRA.Components.IssueViewer.Legacy.AsyncData({
        maxCacheSize: 2
    });
    this.overrideFetch_simple();

    this.asyncData.get(1);
    this.asyncData.get(2);
    this.asyncData.get(1);
    this.asyncData.get(3);

    equal(this.asyncData.hasCached(2), false);
    equal(this.asyncData.hasCached(1), true);
    equal(this.asyncData.hasCached(3), true);
});

test("maxCacheSize limits setMultiple()", function() {
    this.asyncData = new JIRA.Components.IssueViewer.Legacy.AsyncData({
        maxCacheSize: 2
    });

    this.asyncData.setMultiple({
        1: { value: 'one' },
        2: { value: 'two' },
        3: { value: 'three' },
        4: { value: 'four' }
    });

    equal(_.keys(this.asyncData.data).length, 2, "Cache size is limited to 2");
});

test("maxCacheSize prevents set() from adding data to the cache if it is already full", function() {
    this.asyncData = new JIRA.Components.IssueViewer.Legacy.AsyncData({
        maxCacheSize: 2
    });
    this.overrideFetch_simple();

    this.asyncData.get(1);
    this.asyncData.get(2);

    this.asyncData.set(2, 'updated');
    equal(this.asyncData.getMeta(2).value, 'updated', "On a full cache, set() can still update cached values");

    this.asyncData.set(3, 'new');
    equal(this.asyncData.hasCached(3), false, "On a full cache, set() won't add any new values");
});

test("remove()", function() {
    this.asyncData.set(1, 'a');
    equal(this.asyncData.hasCached(1), true, "Item added to cache");
    var changed = this.asyncData.remove(1);
    equal(this.asyncData.hasCached(1), false, "Item is removed from cache");
    equal(changed, true, "remove() returns true to indicate the item was removed");
});

test("remove() item not in cache", function() {
    equal(this.asyncData.remove(1), false, "Nothing was changed, so remove() returns false");
});

test("setPending callbacks called", function() {
    expect(5);

    var doneCallback = sinon.spy();
    var failCallback = sinon.spy();

    var firstTask = jQuery.Deferred();
    firstTask.abort = sinon.spy();

    this.asyncData.setPending(1, firstTask, doneCallback, failCallback);

    var task = jQuery.Deferred();
    var pending = this.asyncData.setPending(1, task, doneCallback, failCallback);
    pending.pipe(function() {
        ok(firstTask.abort.calledOnce, "Pending task was aborted");
        ok(doneCallback.called, "Done callback called if finished successfully");
        ok(!failCallback.called, "Failure callback not called if finished successfully");
    });

    task.resolve();

    doneCallback = sinon.spy();
    failCallback = sinon.spy();

    task = jQuery.Deferred();
    pending = this.asyncData.setPending(1, task, doneCallback, failCallback);
    pending.pipe(null, function() {
        ok(!doneCallback.called, "Done callback not called if not finished successfully");
        ok(failCallback.called, "Failure callback called if not finished successfully");
    });

    task.reject();
});

test("meta.initialLoad should be true for all get() calls that piggyback onto the same AJAX fetch() call", function() {
    var fetchDeferred = jQuery.Deferred();
    this.asyncData.fetch = function() {
        return fetchDeferred.promise();
    };

    // do 2 concurrent gets. ensure that both return initialLoad=true
    this.asyncData.get('id1').done(function(value, meta) { ok(meta.initialLoad, "meta.initialLoad should be truthy for both concurrent gets that are resolved by the same fetch"); });
    this.asyncData.get('id1').done(function(value, meta) { ok(meta.initialLoad, "meta.initialLoad should be truthy for both concurrent gets that are resolved by the same fetch"); });
    fetchDeferred.resolve('id-fetched');

    // now do another get. this one should not return initialLoad=true
    this.asyncData.get('id1').done(function(value, meta) { ok(!meta.initialLoad, "meta.initialLoad should not be true for a value that was just returned from cache"); });
});
