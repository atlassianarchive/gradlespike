plan(key:'VR', name:'Issue Search - Visual Regression tests', description:'Issue Search - Visual Regression tests') {
    issueSearchLabels()
    issueSearchNotifications()
    issueSearchProject()

    repositoryPLUGIN(repoName:'JIRA Issue Search Plugin')
    repository(name:'Kickass Baseline Images')
    repository(name:'Kickass Baseline Images Sandbox')

    stage(name:"Default stage", description:'Default stage', manual: 'false'){
        job(name: 'Visual Regression Tests', key: 'VR', description: 'Visual Regression Tests' ){
            requireLocalLinux()

            artifactDefinition(name:'Screen shots', location:'PLUGIN/jira-issue-nav-plugin/target/test-reports', pattern: '**')
            artifactDefinition(name:'Unit test reports', location:'PLUGIN/jira-issue-nav-plugin/target', pattern: '**/surefire-reports/**')

            checkoutPLUGIN(repoName:'JIRA Issue Search Plugin')
            task(type:'checkout', description:'Checkout baseline images'){
                repository(name:'Kickass Baseline Images', checkoutDirectory: 'ka-baseline-images')
            }
            task(type:'checkout', description:'Checkout baseline sanbox'){
                repository(name:'Kickass Baseline Images Sandbox', checkoutDirectory: 'ka-baseline-images-sandbox')
            }
            task(type:"script", description:'Run maven via run.sh',
                script:'run.sh',
                argument:'clean verify -PrunIndividualTestSuite -DtestGroups=runVisualRegressionTest',
                workingSubDirectory: 'PLUGIN'
            )
            task(description:'Parse JUnit results',
                final:'true',
                type:'jUnitParser',
                resultsDirectory:'**/surefire-reports/*.xml'
            )
            task(type:'script',
                final:'true',
                description: 'Put newly generated images in to the sandbox',
                scriptBody:
'''
echo "Exporting screenshots to sandbox..."

export BUILDKEY="${bamboo.buildResultKey}"
export BRANCHNAME="${bamboo.repository.git.branch}"
export PLUGINCOMMIT=`cd PLUGIN && git rev-parse "$BRANCHNAME"`
export SANDBOXDIR=ka-baseline-images-sandbox/
export CHANGECOUNT=`find . -name "*.png" | grep -P '.*report_images/(?!old|boxold|boxnew|boxdiff|diff).*' | wc -l`

if (($CHANGECOUNT == 0))
then
    echo "No screenshots changed. Nothing more to do! "
    exit 0
else
    echo "It seems $CHANGECOUNT screenshots changed. Time to push some screenshots around."
fi

echo "Let $PLUGINCOMMIT go down in the annals of our screenshot history! "

find . -name "*.png" | grep -P '.*report_images/(?!old|boxold|boxnew|boxdiff|diff).*' | xargs cp -t $SANDBOXDIR

touch commitmsg.txt
echo "New screenshots added to sandbox from $BUILDKEY" >> commitmsg.txt
echo "" >> commitmsg.txt
echo "Based off commit $PLUGINCOMMIT" >> commitmsg.txt
echo "Build URL: ${bamboo.buildResultsUrl}" >> commitmsg.txt

echo "The screenshots will be committed thusly:"
cat commitmsg.txt
echo ""

cd $SANDBOXDIR
git add *.png &&
git commit --file=../commitmsg.txt &&
git push origin $BRANCHNAME &&
echo "Hooray, we pushed some screenshots around :)"

rm ../commitmsg.txt
'''
            )
        }
    }
}

// Disabled until this build is fully working
//
//plan(key:'KARMA', name: 'Issue Search - Qunit Tests (Karma based)', description:'Issue Search - Qunit Tests (Karma based)') {
//    issueSearchLabels()
//    issueSearchNotifications()
//    issueSearchProject()
//
//    repositoryPLUGIN(repoName:'JIRA Issue Search Plugin')
//    repositoryJIRA(repoName:'JIRA master stash')
//    repository(name:'Karma Qunit Plugin')
//    branches(pattern:'issue/.*')
//
//    stage(name:'Default stage', description: 'Default Stage', manual: 'false'){
//        job(name:'QUnit tests', key: 'QUNIT', description: 'QUnit tests') {
//            requireNode()
//
//            checkoutPLUGIN(repoName:'JIRA Issue Search Plugin')
//            checkoutJIRA(repoName:'JIRA master stash')
//            task(type:'checkout', description:'Checkout js-testrunner'){
//                repository(name:'Karma Qunit Plugin', checkoutDirectory: 'js-testrunner')
//            }
//            generateJIRAandPLUGINpoms()
//            task(description:'Install js-testrunner',
//                type:'script',
//                workingSubDirectory: 'js-testrunner',
//                scriptBody:
//'''
//#!/bin/bash
//npm install --registry=https://npm.atlassian.com --ca=null
//'''
//            )
//            compileJIRAWithMaven3()
//            task(description:'Run QUnit tests',
//                type:'maven3',
//                goal:'-Pqunit-tests -DskipTests -Djstestrunner.location=../../js-testrunner test',
//                buildJdk:'JDK 1.7',
//                mavenExecutable:'Maven 3',
//                environmentVariables:'MAVEN_OPTS="${bamboo.maven.opts}"',
//                workingSubDirectory:'PLUGIN/jira-issue-nav-plugin'
//            )
//            task(description:'Parse Karma results',
//                final:'true',
//                type:'jUnitParser',
//                resultsDirectory:'js-testrunner/*.xml'
//            )
//            cleanWithMaven3()
//        }
//    }
//}