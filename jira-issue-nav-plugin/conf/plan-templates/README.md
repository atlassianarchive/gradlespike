# File organization

**plan.*.groovy**

These files contains the build plans used by Issue Search Plugin. Each file contains a set of tests:

* plan.func.groovy: contains the Functional and Unit tests.
* plan.wd.groovy: contains the WebDriver tests.
* plan.release.groovy: contains the Release plans, used to deploy a new version of Issue Search
* plan.others.groovy: contains all other tests (e.g Visual Regression)

**shortcuts.groovy**

This file contains all the helper shortcuts (aka functions) used by the plans.

**load.js**

This file is the NodeJS script used to quickly deploy the plan changes to JBAC.


# Developing plan templates

## Validation

After modifying the plan template, you can ask JBAC to validate your changes. Use this system to ensure your plan templates are valid and do not contain errors.
```
# clone bamboo-plan-templates source

$ git clone git@bitbucket.org:atlassian/bamboo-plan-templates.git

$ cd bamboo-plan-templates/src/main/script

# Use validate.groovy  (if you don't have groovy just $ brew install groovy)
# $ validate.groovy <template_file> <shortcuts_file> <bamboo_url> <username>

# Example
$ ./validate.groovy <issue-search-dir>/conf/plan-templates/plan.func.groovy <issue-search-dir>/conf/plan-templates/shortcuts.groovy https://jira.bamboo.atlassian.com scinos
```

Note: check the [Getting started with Plan Templates](https://extranet.atlassian.com/display/RELENG/HOWTO+-+Getting+started+with+Plan+Templates) page to get the latest documentation.


## Applying your changes

### Normal way

There is a [Plan Template build](https://jira.bamboo.atlassian.com/browse/IS-PT) that will pick up the plan templates and modify/create the existing builds to match the templates. This build will pick the changes from the `master` branch, so you need to commit your templates first. This should be the main and canonical way of maintaining the Issue Search builds.

### Fast way

By using the NodeJS loader, you can upload your tests to JBAC using a REST endpoint. The main benefit of this approach is that you don't need to commit your changes first. This is very useful for debugging and developing purposes. Please, note that this action will effectively change the existing builds to match your templates, **this is not a dry-run**. If you break the current plans, re-run the [Plan Template build](https://jira.bamboo.atlassian.com/browse/IS-PT) to restore them.

```
# Install the node_modules, only required the first time
npm install

node load.js
> JBAC username: scinos
> JBAC password: ***

> Loading plan func
> Loading plan wd
> Loading plan release
> Loading plan others
> done!
```

## Tests

We have a sort of testing for Plan Templates. You need to manually create a branch in the [Plan Template build](https://jira.bamboo.atlassian.com/browse/IS-PT). This will validate your plan templates, but will not apply the changes to the real build plans until your changes are in `master`.


# Documentation

* [HOWTO - Getting started with Plan Templates](https://extranet.atlassian.com/display/RELENG/HOWTO+-+Getting+started+with+Plan+Templates)
* [Plan Templates DSL examples](https://extranet.atlassian.com/display/RELENG/Plan+Templates+DSL+examples)
* [Tests for the PlanTemplates plugin](https://bitbucket.org/atlassian/bamboo-plan-templates/src/master/src/test/resources/?at=master)