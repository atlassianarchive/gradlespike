checkoutPLUGIN(['repoName']) {
    task(type:'checkout', description:'Checkout Issue Search'){
        repository(name:'#repoName', checkoutDirectory: 'PLUGIN')
    }
}

checkoutJIRA(['repoName']) {
    task(type:'checkout', description:'Checkout JIRA'){
        repository(name:'#repoName', checkoutDirectory: 'JIRA')
    }
}

generateJIRAandPLUGINpoms() {
    task(description:'Generate poms to build JIRA and PLUGIN together',
        type:'script',
        script:'JIRA/bin/generate-maven-aggregator-pom.sh',
        argument: 'JIRA PLUGIN'
    )
}

compileJIRA() {
    task(description:'Build and install artifacts required by AMPS',
        type:'maven2',
        goal:'clean install -B -am -Pdistribution -pl JIRA -Dmaven.test.skip -DskipSources',
        buildJdk:'JDK 1.6',
        mavenExecutable:'Maven 2',
        environmentVariables:'MAVEN_OPTS="${bamboo.maven.opts}" M2_HOME="${bamboo.capability.system.builder.mvn2.Maven 2}" JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.6}"',
    )
}

compileJIRAWithMaven3() {
    task(description:'Build and install artifacts required by AMPS',
        type:'maven3',
        goal:'clean install -B -am -Pdistribution -pl JIRA -Dmaven.test.skip -DskipSources',
        buildJdk:'JDK 1.7',
        mavenExecutable:'Maven 3',
        environmentVariables:'MAVEN_OPTS="${bamboo.maven.opts}"',
    )
}

compileJIRAandPLUGIN() {
    task(description:'Build and install artifacts required by PLUGIN unit tests',
        type:'maven2',
        goal:'clean install -B -am -DskipTests',
        buildJdk:'JDK 1.6',
        mavenExecutable:'Maven 2',
        environmentVariables:'MAVEN_OPTS="${bamboo.maven.opts}" M2_HOME="${bamboo.capability.system.builder.mvn2.Maven 2}" JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.6}"',
    )
}

runTests(['type']) {
    task(description:'Run the #type tests with the issue-search category',
        type:'script',
        script:'run.sh',
        argument: '-B clean verify -Dselenium.browser.path=${bamboo.capability.firefox 3.6}/firefox -Djava.awt.headless=true -Dmaven.test.failure.ignore=true -PrunIndividualTestSuite -DtestGroups=run#typeTest -DnoDevMode',
        environmentVariables:'MAVEN_OPTS="${bamboo.maven.opts}" M2_HOME="${bamboo.capability.system.builder.mvn2.Maven 2}" JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.6}"',
        workingSubDirectory:'PLUGIN'
    )
    task(description:'Parse JUnit results',
        final:'true',
        type:'jUnitParser',
        resultsDirectory:'**/surefire-reports/*.xml'
    )
}

runUnitTests() {
    task(description:'Run the unit tests with the issue-search category',
        type:'maven2',
        goal:'-B test',
        buildJdk:'JDK 1.6',
        mavenExecutable:'Maven 2',
        environmentVariables:'MAVEN_OPTS="${bamboo.maven.opts}" M2_HOME="${bamboo.capability.system.builder.mvn2.Maven 2}" JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.6}"',
        workingSubDirectory:'PLUGIN',
        hasTests:'true'
    )
}

clean() {
    task(description:'Remove installed artifacts',
        final:'true',
        type:'maven2',
        goal:'build-helper:remove-project-artifact -Dbuildhelper.removeAll=false -Pdistribution -pl JIRA -am',
        buildJdk:'JDK 1.6',
        mavenExecutable:'Maven 2',
        environmentVariables:'MAVEN_OPTS="${bamboo.maven.opts}" M2_HOME="${bamboo.capability.system.builder.mvn2.Maven 2}" JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.6}"'
    )
}

cleanWithMaven3() {
    task(description:'Remove installed artifacts',
        final:'true',
        type:'maven3',
        goal:'build-helper:remove-project-artifact -Dbuildhelper.removeAll=false -Pdistribution -pl JIRA -am',
        buildJdk:'JDK 1.7',
        mavenExecutable:'Maven 3',
        environmentVariables:'MAVEN_OPTS="${bamboo.maven.opts}"'
    )
}

requireJDK() {
    requireLocalLinux()
    requirement(key:'system.jdk.JDK', condition:'exists')
}

requireNode() {
    requirement(key:'os',                          condition:'equals', value:'Linux')
    requirement(key:'system.builder.node.Node.js', condition:'exists')
}

requireLocalLinux() {
    requirement(key:'os',             condition:'equals', value:'Linux')
    requirement(key:'elastic',        condition:'equals', value:'false')
}

runJSHint() {
    task(description:'Run JSHint',
        type:'script',
        workingSubDirectory: 'PLUGIN',
        scriptBody:
'''
#!/bin/bash
npm install
./node_modules/grunt-cli/bin/grunt jshint:junit --force
'''
    )

    task(description:'Parse results',
        type:'jUnitParser',
        resultsDirectory:'PLUGIN/jshint-output.xml'
    )
}



repositoryPLUGIN(['repoName']) {
    repository(name:'#repoName')
    trigger(description:'Issue Search every 1 minute', type:'polling', strategy:'periodically', frequency:'60'){
        repository(name:'#repoName')
    }
}

repositoryJIRA(['repoName']) {
    repository(name:'#repoName')
    trigger(description:'JIRA every 6 hours', type:'polling', strategy:'periodically', frequency:'21600'){
        repository(name:'#repoName')
    }
}

issueSearchLabels() {
    label(name:'kick-ass') // The legacy label name. Shouldn't be necessary.
    label(name:'issue-search')
    label(name:'plan-templates')
}

issueSearchNotifications() {
    notification(type: 'Comment Added', recipient: 'responsible')
    notification(type: 'Job Hung', recipient: 'responsible')
    notification(type: 'Failed Jobs and First Successful', recipient: 'responsible')

    notification(type: 'Comment Added', recipient: 'watchers')
    notification(type: 'Job Hung', recipient: 'watchers')
    notification(type: 'Failed Jobs and First Successful', recipient: 'watchers')
}

issueSearchProject() {
    project(key: 'IS', name:'Issue Search Plugin', description:'Issue Search Plugin')
}

branches(['pattern']) {
    branchMonitoring(enabled: 'true',
        matchingPattern: '#pattern',
        timeOfInactivityInDays: '7',
        notificationStrategy: 'INHERIT',
        remoteJiraBranchLinkingEnabled: 'true'
    )
}

jUnitTests(['pluginRepo','jiraRepo']) {
    job(name: 'JUnit Tests', key: 'JUNIT', description: 'JUnit Tests' ){
        requireJDK()

        checkoutPLUGIN(repoName:'#pluginRepo')
        checkoutJIRA(repoName:'#jiraRepo')
        generateJIRAandPLUGINpoms()
        compileJIRAandPLUGIN()
        runUnitTests()
        clean()
    }
}

functionalTests(['pluginRepo','jiraRepo']) {
    job(name: 'Functional Tests', key: 'FUNC', description: 'Functional Tests' ){
        requireJDK()

        artifactDefinition(name:'Heap dumps', pattern:'**/*.hprof')
        artifactDefinition(name:'logs', pattern:'**/*.log')
        artifactDefinition(name:'Surefire reports', location:'PLUGIN/jira-issue-nav-plugin/target/surefire-reports', pattern:'*')
        artifactDefinition(name:'Test reports', location:'PLUGIN/jira-issue-nav-plugin/target/test-reports', pattern:'*')

        checkoutPLUGIN(repoName:'#pluginRepo')
        checkoutJIRA(repoName:'#jiraRepo')
        generateJIRAandPLUGINpoms()
        compileJIRA()
        runTests(type:'Func')
        clean()
    }
}

qunitTests(['pluginRepo','jiraRepo']) {
    job(name: 'QUnit Tests', key: 'QUNIT', description: 'QUnit Tests' ){
        requireJDK()

        artifactDefinition(name:'Heap dumps', pattern:'**/*.hprof')
        artifactDefinition(name:'logs', pattern:'**/*.log')
        artifactDefinition(name:'Surefire reports', location:'PLUGIN/jira-issue-nav-plugin/target/surefire-reports', pattern:'*')
        artifactDefinition(name:'Test reports', location:'PLUGIN/jira-issue-nav-plugin/target/test-reports', pattern:'*')

        checkoutPLUGIN(repoName:'#pluginRepo')
        checkoutJIRA(repoName:'#jiraRepo')
        generateJIRAandPLUGINpoms()
        compileJIRA()
        runTests(type:'Qunit')
        clean()
    }
}

jsHint(['pluginRepo']) {
    job(name:'JSHint', key: 'JSHINT', description: 'JSHint'){
        requireNode()
        checkoutPLUGIN(repoName:'#pluginRepo')
        runJSHint();
    }
}

hallelujahClient(['id', 'pluginRepo', 'jiraRepo']) {
    job(name: 'Hallelujah Client #id', key: 'CLIENT#id', description: 'Hallelujah Client #id' ){
        requireJDK()

        artifactDefinition(name:'logs', pattern:'**/*.log')

        task(description: 'Hallelujah Broker Url Discovery', type: 'brokerUrlDiscovery')
        checkoutPLUGIN(repoName:'#pluginRepo')
        checkoutJIRA(repoName:'#jiraRepo')
        generateJIRAandPLUGINpoms()
        task(description:'Build and install',
            type: 'maven3',
            goal: 'install -B -pl JIRA/jira-distribution/jira-func-tests-runner -am -Pdistribution -Dmaven.test.skip -Djira.minify.skip=true -Dfunc.mode.plugins -Djira.func.tests.runner.create -DobrRepository=NONE -DskipSources',
            buildJdk:'JDK 1.7',
            mavenExecutable:'Maven 3',
            environmentVariables:'MAVEN_OPTS="-Xmx512m -Xms128m -XX:MaxPermSize=256m"',
        )
        task(description:'Run Hallelujah client',
            type:'script',
            script:'run.sh',
            argument:'-B -PhallelujahClient -DnoDevMode verify',
            environmentVariables:'MAVEN_OPTS="-Xmx512m -Xms128m -XX:MaxPermSize=256m"',
            workingSubDirectory:'PLUGIN'
        )
    }
}

hallelujahServer(['pluginRepo', 'jiraRepo']) {
    job(name: 'Hallelujah Server', key: 'SERVER', description: 'Hallelujah Server' ){
        requireJDK()

        artifactDefinition(name:'Flakey tests file', pattern:'**/PLUGIN-flakyTest.txt')

        task(description: 'Hallelujah Broker Url Discovery', type: 'brokerUrlDiscovery')
        checkoutPLUGIN(repoName:'#pluginRepo')
        checkoutJIRA(repoName:'#jiraRepo')
        generateJIRAandPLUGINpoms()
        task(description:'Build and install',
            type: 'maven3',
            goal: 'install -B -pl JIRA/jira-distribution/jira-func-tests-runner -am -Pdistribution -Dmaven.test.skip -Djira.minify.skip=true -Dfunc.mode.plugins -Djira.func.tests.runner.create -DobrRepository=NONE -DskipSources',
            buildJdk:'JDK 1.7',
            mavenExecutable:'Maven 3',
            environmentVariables:'MAVEN_OPTS="-Xmx512m -Xms128m -XX:MaxPermSize=256m"',
        )
        task(description:'Run Hallelujah server',
            type: 'maven3',
            goal: '-PhallelujahServer test -Dhallelujah.retries=5',
            buildJdk:'JDK 1.7',
            mavenExecutable:'Maven 3',
            environmentVariables:'MAVEN_OPTS="-Xmx512m -Xms128m -XX:MaxPermSize=256m"',
            workingSubDirectory:'PLUGIN'
        )
        task(description:'Parse JUnit results',
            final:'true',
            type:'jUnitParser',
            resultsDirectory:'PLUGIN/Hallelujah.xml, PLUGIN/**/surefire-reports/*.xml'
        )

        //TODO: Add flakey-test-discovery task, using 'PLUGIN/PLUGIN-flakyTest.txt' as filename.
        //Raised BUILDENG-5496 for feature request
    }
}

releaseTask(['pluginRepo', 'jiraRepo']) {
    job(name:"Release", key:'RELEASE', description:'Release'){
        requireJDK()
        checkoutPLUGIN(repoName:'#pluginRepo')
        checkoutJIRA(repoName:'#jiraRepo')
        task(description:'Run release.sh',
            type:'script',
            script:'release.sh',
            argument:'--jira ../JIRA',
            environmentVariables:'M2_HOME="${bamboo.capability.system.builder.mvn2.Maven 2.1.0}" JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.6}" MAVEN_OPTS="-Xmx1536m -XX:MaxPermSize=256m"',
            workingSubDirectory: 'PLUGIN'
        )
    }
}