plan(key:'WD', name:'Issue Search - WebDriver', description:'WebDriver tests for Issue Search') {
    issueSearchLabels()
    issueSearchNotifications()
    issueSearchProject()

    repositoryPLUGIN(repoName:'JIRA Issue Search Plugin')
    repositoryJIRA(repoName:'JIRA master stash')
    branches(pattern:'issue/.*')

    dependencies(triggerOnlyAfterAllStagesGreen: 'false', automaticDependency: 'false', triggerForBranches: 'true', blockingStrategy: 'notBlock') {
        childPlan(planKey: 'IS-RELEASE')
    }

    stage(name:'Default stage', description: 'Default Stage', manual: 'false'){
        hallelujahClient(id:'01', pluginRepo:'JIRA Issue Search Plugin', jiraRepo:'JIRA master stash')
        hallelujahClient(id:'02', pluginRepo:'JIRA Issue Search Plugin', jiraRepo:'JIRA master stash')
        hallelujahClient(id:'03', pluginRepo:'JIRA Issue Search Plugin', jiraRepo:'JIRA master stash')
        hallelujahClient(id:'04', pluginRepo:'JIRA Issue Search Plugin', jiraRepo:'JIRA master stash')
        hallelujahClient(id:'05', pluginRepo:'JIRA Issue Search Plugin', jiraRepo:'JIRA master stash')
        hallelujahClient(id:'06', pluginRepo:'JIRA Issue Search Plugin', jiraRepo:'JIRA master stash')
        hallelujahServer(pluginRepo:'JIRA Issue Search Plugin', jiraRepo:'JIRA master stash')
    }
}

plan(key:'WD62', name:'Issue Search - WebDriver (6.2.x)', description:'WebDriver tests for Issue Search 6.2.x') {
    issueSearchLabels()
    issueSearchNotifications()
    issueSearchProject()

    repositoryPLUGIN(repoName:'JIRA Issue Search Plugin 6.2.x branch')
    repositoryJIRA(repoName:'JIRA 6.2.x branch')
    branches(pattern:'stable/62/(JRADEV|JDEV)-[0-9]+(-.*)?')

    dependencies(triggerOnlyAfterAllStagesGreen: 'false', automaticDependency: 'false', triggerForBranches: 'true', blockingStrategy: 'notBlock') {
        childPlan(planKey: 'IS-RELEASE62')
    }

    stage(name:'Default stage', description: 'Default Stage', manual: 'false'){
        hallelujahClient(id:'01', pluginRepo:'JIRA Issue Search Plugin 6.2.x branch', jiraRepo:'JIRA 6.2.x branch')
        hallelujahClient(id:'02', pluginRepo:'JIRA Issue Search Plugin 6.2.x branch', jiraRepo:'JIRA 6.2.x branch')
        hallelujahClient(id:'03', pluginRepo:'JIRA Issue Search Plugin 6.2.x branch', jiraRepo:'JIRA 6.2.x branch')
        hallelujahClient(id:'04', pluginRepo:'JIRA Issue Search Plugin 6.2.x branch', jiraRepo:'JIRA 6.2.x branch')
        hallelujahClient(id:'05', pluginRepo:'JIRA Issue Search Plugin 6.2.x branch', jiraRepo:'JIRA 6.2.x branch')
        hallelujahClient(id:'06', pluginRepo:'JIRA Issue Search Plugin 6.2.x branch', jiraRepo:'JIRA 6.2.x branch')
        hallelujahServer(pluginRepo:'JIRA Issue Search Plugin 6.2.x branch', jiraRepo:'JIRA 6.2.x branch')
    }
}