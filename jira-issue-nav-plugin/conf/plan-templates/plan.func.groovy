plan(key:'FUNC', name:'Issue Search - Functional and Units', description:'Functional and Unit tests for Issue Search') {
    issueSearchLabels()
    issueSearchNotifications()
    issueSearchProject()

    repositoryPLUGIN(repoName:'JIRA Issue Search Plugin')
    repositoryJIRA(repoName:'JIRA master stash')
    branches(pattern:'issue/.*')

    stage(name:'Default stage', description: 'Default Stage', manual: 'false'){
        jUnitTests(pluginRepo:'JIRA Issue Search Plugin', jiraRepo:'JIRA master stash')
        functionalTests(pluginRepo:'JIRA Issue Search Plugin', jiraRepo:'JIRA master stash')
        qunitTests(pluginRepo:'JIRA Issue Search Plugin', jiraRepo:'JIRA master stash')
        jsHint(pluginRepo:'JIRA Issue Search Plugin')
    }
}

plan(key:'FUNC62x', name:'Issue Search - Functional and Units (6.2.x)', description:'Functional and Unit tests for Issue Search 6.2.x') {
    issueSearchLabels()
    issueSearchNotifications()
    issueSearchProject()

    repositoryPLUGIN(repoName:'JIRA Issue Search Plugin 6.2.x branch')
    repositoryJIRA(repoName:'JIRA 6.2.x branch')
    branches(pattern:'stable/62/(JRADEV|JDEV)-[0-9]+(-.*)?')

    stage(name:'Default stage', description: 'Default Stage', manual: 'false'){
        jUnitTests(pluginRepo:'JIRA Issue Search Plugin 6.2.x branch', jiraRepo:'JIRA 6.2.x branch')
        functionalTests(pluginRepo:'JIRA Issue Search Plugin 6.2.x branch', jiraRepo:'JIRA 6.2.x branch')
        qunitTests(pluginRepo:'JIRA Issue Search Plugin 6.2.x branch', jiraRepo:'JIRA 6.2.x branch')
    }
}