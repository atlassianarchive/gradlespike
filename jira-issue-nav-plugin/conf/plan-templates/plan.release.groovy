plan(key:'RELEASE', name:'Issue Search - Release', description:'Release build for Issue Search') {
    issueSearchLabels()
    issueSearchNotifications()
    issueSearchProject()

    repositoryPLUGIN(repoName:'JIRA Issue Search Plugin')
    repositoryJIRA(repoName:'JIRA master stash')

    stage(name:"Default stage", description:'Default stage', manual: 'true'){
        releaseTask(pluginRepo:'JIRA Issue Search Plugin', jiraRepo:'JIRA master stash')
    }
}

plan(key:'RELEASE62', name:'Issue Search - Release (6.2.x)', description:'Release build for Issue Search 6.2.x') {
    issueSearchLabels()
    issueSearchNotifications()
    issueSearchProject()

    repositoryPLUGIN(repoName:'JIRA Issue Search Plugin 6.2.x branch')
    repositoryJIRA(repoName:'JIRA 6.2.x branch')

    stage(name:"Default stage", description:'Default stage', manual: 'true'){
        releaseTask(pluginRepo:'JIRA Issue Search Plugin 6.2.x branch', jiraRepo:'JIRA 6.2.x branch')
    }
}
