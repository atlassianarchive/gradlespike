(function() {
    "use strict";

    var fs = require('fs'),
        request = require('request'),
        async = require('async'),
        read = require ('read');

    function loadPlan(planName, username, password, cb) {
        var plan = fs.readFileSync("plan."+planName+".groovy", {encoding:"utf8"});
        var shortcuts = fs.readFileSync("shortcuts.groovy", {encoding:"utf8"});
        var json = {dsl: plan, shortcuts: shortcuts};

        console.log("Loading plans '"+planName+"'");
        request({
            url: "https://jira.bamboo.atlassian.com/rest/plantemplate/1.0/json",
            qs: {
                "os_authType": "basic"
            },
            auth: {
                user: username,
                pass: password
            },
            method: "POST",
            json: json
        }, cb);
    }

    async.series({
        username: async.apply(read, { prompt: 'JBAC username:', silent: false}),
        password: async.apply(read, { prompt: 'JBAC password:', silent: true, replace: "*"}),
    }, function(err, results){
        if (err) return console.error(err);

        var username = results.username[0];
        var password = results.password[0];

        async.series([
            async.apply(loadPlan, "func", username, password),
            async.apply(loadPlan, "wd", username, password),
            async.apply(loadPlan, "release", username, password),
            async.apply(loadPlan, "others", username, password)
        ], function(err, results) {
            if (err) return console.error(err);

            results.forEach(function(result) {
                result = result[0];
                if (result.statusCode !== 200) {
                    console.log(result.body);
                }
            });
            console.log("done!");
        });
    })
})();
