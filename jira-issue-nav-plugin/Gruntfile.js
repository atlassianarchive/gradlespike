module.exports = function (grunt) {
    var files;

    files = [
        "jira-issue-nav-components/src/main/resources/issueviewer/js/legacy/IssueEventBus.js",
        "jira-issue-nav-components/src/main/resources/issueviewer/js/views/error/ErrorGenericView.js",
        "jira-issue-nav-components/src/main/resources/issueviewer/js/views/error/ErrorNoPermissionView.js",
        "jira-issue-nav-components/src/main/resources/issueviewer/js/views/issue/IssueBodyView.js",
        "jira-issue-nav-components/src/main/resources/lib/base/Controller.js",
        "jira-issue-nav-components/src/main/resources/namespaces.js",
        "jira-issue-nav-components/src/main/resources/query/basic/CriteriaModel.js",
        "jira-issue-nav-components/src/main/resources/query/SwitcherCollection.js",
        "jira-issue-nav-components/src/main/resources/query/SwitcherModel.js"
    ];

    grunt.initConfig({
        jshint: {
            all: {
                src: files
            },

            junit: {
                options: {
                    reporter: "lib/jshint-junit-xml-formatter/jshint-junit-xml-formatter.js",
                    reporterOutput: "jshint-output.xml"
                },
                src: files
            },

            options: {
                jshintrc: ".jshintrc"
            }
        },

        pkg: grunt.file.readJSON("package.json")
    });

    grunt.loadNpmTasks("grunt-contrib-jshint");
};
