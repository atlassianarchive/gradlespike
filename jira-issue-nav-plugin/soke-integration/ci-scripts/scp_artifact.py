#!/usr/bin/env python

import re
import sys
import errno
import os
import subprocess
from optparse import OptionParser, OptionGroup

if __name__ == '__main__':
   usage = 'usage: %prog [options]'
   parser = OptionParser(usage)

   parser.add_option(
       "-d",
       "--dest",
       dest="dest",
       default="127.0.0.1",
       help="The remote host to transfer the artifact to. [default: %default]"
   )

   parser.add_option(
       "-u",
       "--username",
       dest="username",
       default="perfuser",
       help="The user to use to connect to the remote machine. [default: %default]"
   )

   parser.add_option(
       "-t",
       "--table",
       dest="table",
       default="greenhopper",
       help="The table to insert the data into. [default: %default]"
   )

   parser.add_option(
       "-i",
       "--id",
       dest="testid",
       default="RUN1",
       help="The ID of the performance test. [default: %default]"
   )

   parser.add_option(
       "-C",
       "--changeset",
       dest="changeset",
       default="??",
       help="The changeset hash of the code under test. [default: %default]"
   )

   parser.add_option(
       "-U",
       "--url",
       dest="url",
       default="http://www.atlassian.com",
       help="The metadata URL, usually a Bamboo build URL. [default: %default]"
   )

   parser.add_option(
       "-a",
       "--artifact",
       dest="artifact",
       default="report.zip",
       help="The name of the build artifact(zip) to process. [default: %default]"
   )

   parser.add_option(
       "-c",
       "--credentials",
       dest="credentials",
       default="ssh_private",
       help="The SSH private key of the user. [default: %default]"
   )

   parser.add_option(
       "-P",
       "--processor",
       dest="report_processor",
       default="to_mysql",
       help="The primeradiant report processor to process the soke report. [default: %default]"
   )

   (options, args) = parser.parse_args()
   run_id = options.testid
   db_name = options.table
   md5str = dest_name = out = None

   try:
      md5bin = 'md5sum'
      if os.uname()[0] == 'Darwin':
         md5bin = 'md5'
      md5str = subprocess.Popen([md5bin,options.artifact],stdout=subprocess.PIPE).communicate()[0]
   except Exception, e:
      print >>sys.stderr, "Failed to hash zipfile: ", e
      sys.exit(1)
   if os.uname()[0] == 'Darwin':
      tmp_name = md5str.split(' ')[3].strip()
   else:
      tmp_name = md5str.split(' ')[0]

   try:
      dest_name = "/tmp/_"+tmp_name+".zip"
      dest_str = options.username+"@"+options.dest+":"+dest_name
      output = subprocess.Popen( ['scp','-i',options.credentials
                                 ,'-o BatchMode=yes'
                                 ,'-o StrictHostKeyChecking=no'
                                 ,options.artifact
                                 ,dest_str],
                                 stdout=subprocess.PIPE).communicate()[0]
   except Exception, e:
      print >>sys.stderr, "Failed to copy artifact: ", e
      sys.exit(1)
   try:
      dst_str = options.username+"@"+options.dest
      out = subprocess.Popen( ['ssh'
                              ,'-i',options.credentials
                              ,'-o BatchMode=yes'
                              ,'-o StrictHostKeyChecking=no'
                              ,dst_str
                              ,'/opt/perfstore/' + options.report_processor + '.py'
                              ,'-z',dest_name
                              ,'-t',options.table
                              ,'-i',options.testid
                              ,'-c',options.changeset
                              ,'-u',options.url],
                              stdout=subprocess.PIPE).communicate()[0]
   except Exception, e:
      print >>sys.stderr, "Failed to process artifact: ["+out+"] ", e
      sys.exit(1)
