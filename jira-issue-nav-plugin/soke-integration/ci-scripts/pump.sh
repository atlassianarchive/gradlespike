#/bin/bash

set -vx

chmod 0600 soke-integration/ci-scripts/ssh_private
soke-integration/ci-scripts/scp_artifact.py -c soke-integration/ci-scripts/ssh_private -a soke-integration/target/report.zip -d db1-perfeng.perfeng.private.atlassian.com -t $1 -i $2 -U $3 -C $4
