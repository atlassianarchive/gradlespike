/bin/bash

# bash port of scp_artifact.py needed because cygwin doesn't support python 2.7 and I don't want to talk about it

set -vx

DEST='127.0.0.1'
REMOTE_USERNAME='perfuser'
TABLE='greenhopper'
TEST_ID='RUN1'
CHANGESET='??'
URL='http://www.atlassian.com/'
ARTIFACT='report.zip'
CREDENTIALS='ssh_private'

while getopts "c:a:d:t:i:U:C:" Option ; do
    case $Option in
        d ) DEST=$OPTARG ;;
        u ) REMOTE_USERNAME=$OPTARG ;;
        t ) TABLE=$OPTARG ;;
        i ) TEST_ID=$OPTARG ;;
        C ) CHANGESET=$OPTARG ;;
        U ) URL=$OPTARG ;;
        a ) ARTIFACT=$OPTARG ;;
        c ) CREDENTIALS=$OPTARG ;;
        * ) echo "unrecognized argument"; exit -1 ;;
    esac
done
shift $(($OPTIND - 1))

if [[ ! -e "$ARTIFACT" ]]; then 
    echo file not found $ARTIFACT
    exit 4
fi
TMP_NAME=`md5sum "$ARTIFACT" | cut -f1 -d\  `
DEST_NAME="/tmp/_${TMP_NAME}.zip"
DEST_STR="${REMOTE_USERNAME}@${DEST}:${DEST_NAME}"
scp -i "$CREDENTIALS" -o BatchMOde=yes -o StrictHostKeyChecking=no "$ARTIFACT" "$DEST_STR"
if ($? -ne 0); then
    echo failed to copy artifact
    exit 2
fi

ssh -i "$CREDENTIALS" -o BatchMode=yes -o StrictHostKeyChecking=no "${REMOTE_USERNAME}@${DEST}" /opt/perfstore/to_mysql.py -z 
if ($? -ne 0); then
    echo failed to process artifact
    exit 3
fi


