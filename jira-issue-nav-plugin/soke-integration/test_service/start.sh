#!/bin/bash
# ==================================================================================================
# This script expects a JIRA standalone server to be unpacked in the target directory, together with
# an installed dataset and an unpacked home directory. It will start JIRA and halt execution until
# the JIRA server responds. 
# ==================================================================================================

BASE=$PWD/`dirname $0`
if [ -n "$M2_HOME" ]
then
    export PATH=$PATH:$M2_HOME/bin
fi

if [ -n "$JAVA_HOME" ]
then
    export PATH=$PATH:$JAVA_HOME/bin
fi

JIRA_HOME_PATH=$BASE/target/home
if which cygpath &> /dev/null
then
    FIND_PATH=`cygpath -w /usr/bin/find`
    export JIRA_HOME=`cygpath -wm $JIRA_HOME_PATH`
else 
    FIND_PATH=`which find`
    export JIRA_HOME=$JIRA_HOME_PATH
fi

export JAVA_OPTS="$JAVA_OPTS -Xmx1024m -XX:MaxPermSize=256m -Datlassian.mail.senddisabled=true -Dorg.apache.jasper.runtime.BodyContentImpl.LIMIT_BUFFER=true -Djava.awt.headless=true"

# ==================================================================================================
# JIRA server utilities, copied from pdzwart's harness scripts 
# ==================================================================================================

# $1=Number of check loops
# $2=Check delay in seconds
# $3=Check information string
# $4=Check fail string
# $5+=Thing to execute the check
check_loop () {
    local num_checks=$1
    shift

    local check_delay=$1
    shift

    local check_count=0
    local check_info=$1
    shift

    local check_fail=$1
    shift
    
    echo -n "$check_info: "
    while [ $check_count -lt $num_checks ]
    do
        "$@"

        if [ $? -eq 0 ]
        then
            echo "Done."
            break
        else
            echo -n "."
            ((++check_count))
            sleep $check_delay
        fi
    done

    if [ ! $check_count -lt $num_checks ]
    then
        echo " FAIL! $check_fail after $check_count checks."
        exit 2
    fi
}

jira_check_available () {
    check_loop 30 10 "Waiting for $1 to start" "JIRA did not start" curl -s "http://localhost:8080/"
}

# ==================================================================================================
# Here come the actual steps for starting JIRA and running the tests 
# ==================================================================================================

# start jira
/usr/bin/find $BASE/target/jira/ -iname "start-jira.sh" -exec {} \;
# waaaaaaaait for it....
jira_check_available "jira"
