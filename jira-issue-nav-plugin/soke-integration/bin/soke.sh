#!/bin/bash
# ==================================================================================================
# This script runs the Kickass performance tests. It requires the JIRA/KA environment to be set up
# (done by test_service/start.sh).
# ==================================================================================================

if [ -n "$JAVA_HOME" ]
then
    export PATH=$PATH:$JAVA_HOME/bin
fi

# the name is hardcoded in test_service/pom.xml
SOKE_JAR=soke-issue-nav-plugin.jar

# ==================================================================================================
# Some utility scripts
# ==================================================================================================

_killjobs() {
    signal=${1:-15}

    for job in `jobs -p`
    do
        kill -$signal $job > /dev/null 2>&1
    done
}

kill_vnc() {
    if [ -n "$DISPLAY" ]
    then
        vncserver -kill $DISPLAY 2>&1
    fi
}

_exit() {
    
    if [[ $PLATFORM == "NIX" && -z "$DISPLAY" ]]; then
        kill_vnc
    fi
    _killjobs
    _killjobs 9
}

_intterm() {
    exit 1
}

trap _exit EXIT
trap _intterm INT TERM

start_vnc() {
    export DISPLAY=$(vncserver 2>&1 | perl -ne '/^New .* desktop is (.*)$/ && print"$1\n"')

    if [ -z "$DISPLAY" ]
     then
         echo "vncserver start fail, see chicken scratches and goat entrails for details."
         exit 1
    fi
}

check_soke_jar_exists() {
    if [ ! -f $SOKE_JAR ];
    then
    	echo "Executable $SOKE_JAR does not exist."
    	exit 1
    fi
}

pushd `dirname $0`/../target
if [[ $? -ne 0 ]]; then 
    echo "hey punk, I can't cd to target directory, something must have gone wong. Compile error?"
    exit 1
fi

case "`uname`" in
    Darwin*) PLATFORM="MAC"    ;;
    CYGWIN*) PLATFORM="CYGWIN" ;;
    *)       PLATFORM="NIX"  ;;
esac

echo Running on platform $PLATFORM

# Is this vnc thing actually needed? Seems to work on linux without it
if [[ $PLATFORM == "NIX" ]]; then
    start_vnc
fi

check_soke_jar_exists

# Now that everything runs (JIRA, KA, VNC), actually start the scripts.

if [[ $PLATFORM == "CYGWIN" ]]; then
    LOCAL_CONF_FILE="classes/local-ie.conf"
else
    LOCAL_CONF_FILE="classes/multipersona.conf"
fi
EXTRA_OPTIONS=""
if [[ $PLATFORM == 'NIX' ]]; then
    EXTRA_OPTIONS="-Dos.arch.override=amd64"
fi

java -Dsoke.config=$LOCAL_CONF_FILE $EXTRA_OPTIONS -Dconfig.trace=loads -cp $SOKE_JAR com.atlassian.soak.client.Main 2>&1 | tee soke-client.out

unzip report.zip -d report
popd
