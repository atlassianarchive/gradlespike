#!/bin/bash

# need postgres, so if you are on OSX and have brew, do the following
# brew install postgres
# initdb /usr/local/var/postgres
# pg_ctl -D /usr/local/var/postgres -l /usr/local/var/postgres/server.log start

require() {
    which "$1" 2>&1 >/dev/null
    if [[ $? -ne 0 ]]; then 
        echo "$1 is required but not installed"
        exit 1
    fi
}
require pg_ctl
require unzip
# headless linuxes only
if [[ -z "$DISPLAY" && `uname -s` != "Darwin" && `uname -o` != 'Cygwin' ]]; then
    require vncserver
fi

base=`dirname $0`
pushd $base/..
mvn -B -e clean install &&\
pushd test_service &&\
mvn -B clean initialize &&\
popd &&
test_service/start.sh &&\
bin/soke.sh 
test_service/stop.sh
popd
