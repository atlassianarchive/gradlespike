README

TODO this is out of date and needs to be rewritten

JIRA Kickass performance analysis using the Soke Framework

= Disclaimer =
This Readme really only focuses on how to get the testing framework installed and running on your local machine. 
There are some pointers here for other things you need to have set up and installed, such as a jira instance with Kickass,
but getting these up and running can be fiddly. (especially spinning up kickass using amps and atlas-debug). 
This readme isnt intended to cover all of these potential issues. good luck

= Abbreviations =
Soke - Soke testing framework (may also be referred to as Soak)
PO's - Page Objects
KA - Kickass (may also be spelt kickarse)
config.key.subkey => this notation refers to the nesting of config objects. eg in the example below, templates.standard.personas would give you "Fred"

= Installation =
1. Get the source from bitbucket: https://bitbucket.org/jwesleysmith/soke/ (branch: master)
2. Install mongodb (default settings) on your local machine, if you want to generate js memory heap graphs, or use the mongo sink.
3. Get your jira instance up and running, and make sure KA is installed.
4. Ensure that KA testing framework is available in your local mvn repo (maybe needed?)
5. Install test data into jira. 
	5.1 From the "jira-issue-nav-plugin/jira-issue-nav-plugin" folder> cp src/test/xml/TestNavigation.xml target/jira/home/import/
	5.2 Open up your running jira instance (localhost:2990/jira, and install the testdata. (type gg, search "restore" etc)
6. Configure soke as below
7. Navigate to the soke project folder, and run "mvn install"
8. Follow the run instructions below

= Configuration =
* All the configuration settings for the KA tests can be found in 
	soke/soke-kickarse/src/main/resources/local.conf 
* JIRA/KA
 * you need to specify the host and port of your jira instance in engine.host. Due to a bug in WebDriver, the whole url with port needs to be specified.
 * DONT USE http://localhost...  Make sure you have a proper qualified domain name.  (due to a bug in har storage)
 * eg http://josephs-mac-pro.sydney.atlassian.com:2990/jira or http://usermbpro:2990/jira
* Interactions
 * The interaction template is specified in "engine.test.template"
 * Select the interactions you want to run, under templates.<template>.personas. These must be options from the possible personas below. default for <template> is "standard"
 * For Example:
-----------------------------------------------------------------------------------------------------
       templates {

         standard {
           // number of threads to start. each thread will control one browser instance.
           threads = 1

           personas = "Fred"

           Fred {
             user = admin
             password = admin
             script = com.atlassian.soak.kickarse.Developer
             runs = 1
           }
       }
-------------------------------------------------------------------------------------------------------
  * You also need to set what output you would like from the tests. The current options are
	1. file     	: Output to a log file
	2. harstorage	: Outputting had data to our harstorage application (for analysing http-requests
	3. mongodb	: Outputting results to a mongodb (currently on your local box), from which you can generate html graphs of memory consumption

  * You select which outputs  (or sinks) you would like under engine.config.log.use
	ie. use = "file, harstorage, mongo"
  * The sinks must be selected from the options in soke/soke-core/src/main/resources/reference.conf
  * In a similar format to specifying personas, each sink's specific configuration is registered as an object in "engine.conf.log". Local.conf already has some suitable defaults set up, which should work fine. 

= Viewing Results =
  * har data can be viewed at http://cheech.sydney.atlassian.com:5000, unless you have set up your own harstorage server, (details on how to do this provided separately)
  * log files are pretty self explanatory
  * if you have mongodb installed and running, and have specified the mongo sink option (see above), then you can run a further tool to generate some memory metrics in a html page. More Charts and tools taking advantage of this will be available in the future.

= Run =
1. cd soke/soke-kickarse
2. mvn package
3. java -jar target/soke-kickarse-<version>.jar

= To Build Metrics = 
cd soke/soke-analysis
mvn scala:console
> import com.atlassian.soak.analysis._; SokeResultsPageBuilder.run
* note, running mvn scala:console will do funny things to that terminal windows once you exit the console. just open a new terminal window
