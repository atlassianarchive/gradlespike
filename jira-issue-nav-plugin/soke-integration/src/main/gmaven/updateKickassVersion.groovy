//
// Updates test_service/pom.xml to use the right kickass.version, as specified by *this* project's version.
//

final String EOL = System.getProperty("line.separator")

def originalPom = new File("${project.basedir}/test_service/pom.xml")
def modifiedPom = new File("${project.basedir}/test_service/pom.xml.new")
if (modifiedPom.exists() && !modifiedPom.delete()) {
  fail "Failed to delete ${modifiedPom.absolutePath}"
}

def wasModified = false
originalPom.eachLine({ line ->
  def matches = (line =~ "(.*<kickass.version>)(.*)(</kickass.version>.*)")
  if (matches && matches[0]) {
    if (matches[0][2] != "${project.version}") {
      log.info "Setting test_service's \${kickass.version} to ${project.version}"
      wasModified = true

      modifiedPom << matches[0][1] << "${project.version}" << matches[0][3] << EOL
      return;
    }
  }

  // just copy the line
  modifiedPom << line << EOL
})

if (wasModified && !modifiedPom.renameTo(originalPom)) {
  fail "Failed to rename ${modifiedPom.absolutePath} to ${originalPom.absolutePath}"
}
else {
  modifiedPom.delete()
}
