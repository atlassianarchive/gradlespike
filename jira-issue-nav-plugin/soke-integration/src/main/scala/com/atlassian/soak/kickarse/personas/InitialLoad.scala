package com.atlassian
package soak
package kickarse

object InitialLoad extends Configurable[InitialLoad] {
  def config: ConfigReader[InitialLoad] = {
    ConfigReader(personaConfig => new InitialLoad(personaConfig))
  }
}

class InitialLoad(personaConfig: Configuration) extends TestScript[Kickarse] {

  var dataFile = personaConfig.get[String]("datafile")
  if (dataFile.length() == 0) {
    dataFile = "perfdata.zip"
  }

  def apply(ctx: Kickarse#Context) {
    import ctx._

    implicit val enabledFeatures = new DarkFeatures(personaConfig.get[List[String]]("darkFeatures").map(a => (a, true)))

    restoreDataFromResource(dataFile)

    initialLoadFromDashboard("cold cache")

    openIssueWithLargeNumberOfCommentsFromIssuesPage(isTimed = false)
  }

}
