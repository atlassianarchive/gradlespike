package com.atlassian.soak
package kickarse

import com.atlassian.integrationtesting.ui.RefApp

/**
 * TODO: Document this file here
 */
class ExecutionConfig extends control.RemoteTest {

  /** this will be called reflectively
    * @param id : ID for this test
    * @param personaKey : name of the persona to execute (see configuration)
    */
  def runWith(id: String, template: String, personaKey: String) =
    client.Runner.config[JIRA, KickarseInteractions](new KickarseInteractions(_, _))(id, template, personaKey)
}
