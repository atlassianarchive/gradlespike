package com.atlassian.soak.kickarse

class DarkFeatures(val features: Seq[(String, Boolean)]) {
  val enabled = features filter { case (name, status) => status } map { case (name, status) => name }
  val disabled = features filter { case (name, status) => !status } map { case (name, status) => name }

  def isEnabled(darkFeatureToCheck : String ) = {
    enabled.contains(darkFeatureToCheck);
  }
}