package com.atlassian
package soak

package object kickarse {

  type JIRA = jira.pageobjects.JiraTestedProduct
  type JiraPage = jira.pageobjects.pages.AbstractJiraPage

  type Kickarse = TestSystem[JIRA] {
    type Context = KickarseInteractions
  }

  def randomString(length: Int) = List.fill(length)(scala.util.Random.nextPrintableChar).mkString
}
