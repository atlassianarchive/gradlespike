package com.atlassian.soak.kickarse

import scala.Predef._
import com.atlassian.soak.Logging
import java.io.File


class TestLogger ( interactionName:String, personaName:String)(implicit application:JIRA) extends Logging {
  import Logging._

  private val driver = application.getTester.getDriver
  private var screenShotCounter = 0

  /**
   * Returns the next screenshot counter
   * @return Integer
   */
  def getScreenShotCounter = {
    screenShotCounter = screenShotCounter + 1
    screenShotCounter
  }

  /**
   * Sends a log line to the logger
   */
  def start() {
    info("-- Starting interaction: "+interactionName)
  }

  /**
   * Logs an interaction step
   * @param description Description to log
   */
  def logStep(description:String) {
    info("   " + description)
  }

  /**
   * Logs the stop of an interaction
   */
  def stop() {
    info("-- End interaction: "+interactionName)
  }

  /**
   * Takes an screenshot
   * @param description Description to use in the screenshot filename
   */
  def takeScreenShot(description: String) {
    val fileName = "%s-%s-%s-%d-%s.png".format(
      driver.getBrowser.name,
      interactionName,
      personaName,
      getScreenShotCounter,
      description.replace(" ","_").toLowerCase
    )
    logStep("Taking ScreenShot "+fileName)
    driver.takeScreenshotTo(new File(fileName))
  }

  /**
   * Logs a step in the interaction, optionally taking a screenshot
   * @param description Description to Use
   * @param screenShot True to take an screenshot *after* the interaction
   * @param thunk Interaction to execute
   * @tparam A the type
   * @return
   */
  def step[A](description:String, screenShot:Boolean = false)(thunk: => A): A = {
    logStep(description)
    val result = thunk
    if (screenShot) takeScreenShot(description)
    result
  }

}
