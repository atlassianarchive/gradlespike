package com.atlassian
package soak
package kickarse

object IssueNav extends Configurable[IssueNav] {
  def config: ConfigReader[IssueNav] = {
    ConfigReader(personaConfig => new IssueNav(personaConfig))
  }
}

class IssueNav(personaConfig: Configuration) extends TestScript[Kickarse] {

  def apply(ctx: Kickarse#Context) {
    import ctx._

    implicit val enabledFeatures = new DarkFeatures(personaConfig.get[List[String]]("darkFeatures").map(a => (a, true)))
    val killSwitchEnabled = enabledFeatures.isEnabled("ka.KILL_SWITCH")
    implicit val personaName = personaConfig.get[String]("prependName")
    implicit val useSplitView = personaConfig.get[Boolean]("useSplitView")

    withDarkFeatures {

      if (useSplitView) {
        switchToSplitView(isTimed = false)
      } else if (!killSwitchEnabled) {
        switchToListView(isTimed = false)
      }

      initialLoadFromDashboard("warm cache")

      basicModeFilterSearchExperience

      advancedModeFilterSearchExperience

      createFilter

      if (useSplitView) {
        switchToSplitView(isTimed = true)
      } else if (!killSwitchEnabled) {
        switchToListView(isTimed = true)
      }

      goToNextIssues

      filterAndViewIssue

      filterAndEditIssue

      filterAndResolvedIssue

      openIssueWithLargeNumberOfCommentsFromIssuesPage(isTimed = true)
    }
  }
}
