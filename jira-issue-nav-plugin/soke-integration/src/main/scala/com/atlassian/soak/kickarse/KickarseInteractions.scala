package com.atlassian
package soak
package kickarse

import com.atlassian.jira.plugin.issuenav.pageobjects._
import com.atlassian.jira.plugin.issuenav.pageobjects.splitview._
import jira.plugin.issuenav.pageobjects.util.KickassLoginUtil
import scalaz.syntax.id._
import org.openqa.selenium.{By, Dimension}
import com.atlassian.jira.plugin.issuenav.pageobjects.ReadyForAction.PSYCHO_READY

class KickarseInteractions(val application: JIRA, val timed: Timer) extends browser.BrowserInteractions with Logging {

  val driver = application.getTester.getDriver
  driver.executeScript("window.onbeforeunload = function() {};")
  driver.manage.window.setSize(new Dimension(1280, 1024))

  implicit val app = application

  var personaName = ""
  var useSplitView = true

  val kickassLoginUtil = application.getPageBinder().bind(classOf[KickassLoginUtil])

  def withDarkFeatures[A](thunk: => A)(implicit darkFeatures: DarkFeatures, personaName: String, useSplitView: Boolean) = {
    if (personaName.length()>0) {
      this.personaName = "["+personaName+"]";
    }else{
      this.personaName = "";
    }

    implicit val logger = (new TestLogger("withDarkFeatures", this.personaName))
    import logger._

    logStep("Setting up dark features: " + darkFeatures.features)
    val previousValues = darkFeatures.features map { case(name, enabled) => setDarkFeature(enabled, name) }

    this.useSplitView = useSplitView

    try
      thunk
    catch {
      case _ => step("FAILED TEST", screenShot = true) {}
    } finally {
      logStep("Restoring dark features: " + previousValues)
      previousValues map { case(name, enabled) => setDarkFeature(enabled, name) }
    }
  }

  private def setDarkFeature(enabled: Boolean, darkFeatureName: String): (String, Boolean) = {
    val previousValue = application.backdoor.darkFeatures.isGlobalEnabled(darkFeatureName)
    if (enabled)
      application.backdoor.darkFeatures().enableForSite(darkFeatureName)

    else
      application.backdoor.darkFeatures().disableForSite(darkFeatureName)

    darkFeatureName -> previousValue
  }

  private def generateInteractionKey(name: String) = personaName + name

  private def goToIssuesPage = kickassLoginUtil.kickAssOnIssuesWithSysAdmin(application)

  private def waitForResultsTable(implicit darkFeatures: DarkFeatures, logger: TestLogger) = {
    if (useSplitView) {
      application.getPageBinder().bind(classOf[SplitLayout]).issueList
    } else {
      application.getPageBinder().bind(classOf[IssuesPage]).waitForResultsTable
    }
  }

  private def getIssueDetail(ip: IssuesPage)(implicit darkFeatures: DarkFeatures, logger: TestLogger) = {
    import logger._
    if (useSplitView) {
      step("Gets the selected issue") {
        ip.getSplitLayout().issue()
      }
    } else {
      step("Get the results table and navigate to the issue detail page") {
        val resultsTable = ip.getResultsTable()
        resultsTable.navigateToIssueDetailPage(resultsTable.getSelectedIssueKey)
      }
    }
  }

  private def filterAndSearchExperience(interactionName: String, queryMode: String)(implicit darkFeatures: DarkFeatures) {
    implicit val logger = (new TestLogger(interactionName, personaName))
    import logger._

    start()
    goToIssuesPage |> { ip =>

      if (queryMode == "advanced") {
        step("Switch to Advanced query mode") {
          ip.getAdvancedQuery()
        }
      }else {
        step("Switch to Basic query mode") {
          ip.getBasicQuery()
        }
      }

      step("Refresh the page") {
        application.getTester.getDriver.navigate().refresh()
      }

      step("Filter and Search in "+queryMode+" mode") {
        timed(generateInteractionKey("do a "+queryMode+" search")) {
          logStep("Select 'Bugs' filter")
          ip.getFilters.selectFavourite("Bugs")

          logStep("Waiting for results table")
          waitForResultsTable
        }
      }
    }
    stop()

  }

  private def getSecondIssueFromResultsTable(ip: IssuesPage)(implicit darkFeatures: DarkFeatures, logger: TestLogger): IssueDetailComponent = {
    import logger._

    if (useSplitView) {

      var splitLayout = step("Get split layout") {
        ip.getSplitLayout()
      }

      splitLayout = step("Get next issue") {
        splitLayout.nextIssue()
      }

      step("Get selected issue") {
        splitLayout.issue()
      }

    } else {

      val resultsTableComponent = step("Get results table") {
        ip.getResultsTable()
      }

      val selectedIssue = step("Open selected issue") {
        resultsTableComponent.openSelectedIssue(PSYCHO_READY).ready()
      }

      step("Open next issue") {
        selectedIssue.nextIssue(PSYCHO_READY).ready()
      }
    }
  }

  private def filterAndSelectIssue(isReadyForAction : IssueDetailComponent => Boolean, label : String, interactionName: String)(implicit darkFeatures: DarkFeatures) {
    implicit val logger = (new TestLogger(interactionName, personaName))
    import logger._

    start()

    goToIssuesPage |> { ip =>
      step("Do a new search") {
        ip.newSearch()
      }

      timed(generateInteractionKey("Filter and " + label)) {

        logStep("Select fitler 'Bugs'")
        ip.getFilters.selectFavourite("Bugs")

        val issue = getIssueDetail(ip)

        logStep("Check if the issue is ready for action")
        isReadyForAction(issue)
      }
    }
    stop()
  }

  def restoreDataFromResource(dataResource : String) {
    application.backdoor.dataImport.restoreDataFromResource(dataResource)
  }

  def ensureNoBannerIsPresent() {
    val licenseStatusClose: By = By.id("stp-licenseStatus-remindMeNever")

    if (driver.elementExists(licenseStatusClose)) {
      driver.findElement(licenseStatusClose).click()
      driver.waitUntilElementIsNotVisible(licenseStatusClose)
    }
  }

  def initialLoadFromDashboard(cacheStatus : String)(implicit darkFeatures: DarkFeatures) {
    implicit val logger = (new TestLogger("initialLoadFromDashboard", personaName))
    import logger._

    start()

    application.gotoLoginPage.loginAsSysadminAndGoToHome |> { dashboard =>
      timed(generateInteractionKey("Initial Page Load from the Dashboard - " + cacheStatus)) {

        ensureNoBannerIsPresent()

        logStep("Get the header")
        val header = dashboard.getHeader

        logStep("Get the issues menu")
        val issuesMenu = header.getIssuesMenu

        logStep("Click on Search For Issues")
        issuesMenu.open.searchForIssues

        logStep("Waiting for results table")
        waitForResultsTable
      }
    }
    stop()
  }

  def basicModeFilterSearchExperience(implicit darkFeatures: DarkFeatures) {
    filterAndSearchExperience("basicModeFilterSearchExperience", "basic");
  }

  def advancedModeFilterSearchExperience(implicit darkFeatures: DarkFeatures) {
    filterAndSearchExperience("advancedModeFilterSearchExperience", "advanced");
  }

  def goToNextIssues()(implicit darkFeatures: DarkFeatures) {
    implicit val logger = (new TestLogger("goToNextIssues", personaName))
    import logger._

    start()

    goToIssuesPage |> { ip =>

      step("Do a new search") {
        ip.newSearch()
      }

      val secondIssue = getSecondIssueFromResultsTable(ip)

      // time how long it takes to open the next 3 issues
      0.until(3).foldLeft(secondIssue) { (issue, n) =>

        val nextIssue = step("Clicking next on %s ".format(issue.getIssueKey)) {
          timed(generateInteractionKey("navigate to next issue from Issue page")) {
            issue.nextIssueUsingKeyboardShortcut(ReadyForAction.PSYCHO_READY)
          }
        }

        Thread.sleep(2500)
        nextIssue.ready()
      }
    }

    stop()
  }

  def openFirstIssue()(implicit darkFeatures: DarkFeatures) {
    implicit val logger = (new TestLogger("openFirstIssue", personaName))
    import logger._

    start()

    goToIssuesPage |> { ip =>

      step("Do a new search") {
        ip.newSearch()
      }

      val rt = step("Get results table") {
        ip.getResultsTable()
      }

      step("Click first issue in search results") {
        timed(generateInteractionKey("open selected issue in issue nav")) {
          rt.openSelectedIssue(PSYCHO_READY)
        }
      }
    }

    stop()
  }

  def switchToListView(isTimed: Boolean = true) {
    implicit val logger = (new TestLogger("switchToListView", personaName))
    import logger._

    goToIssuesPage |> { ip =>
      if (isTimed) {
        step("Do a new search") {
          ip.newSearch()
        }

        step("Switch to split view") {
          ip.getSplitLayout()
        }

        step("Switch to list view") {
          timed(generateInteractionKey("switch to list view from split view")) {
            ip.getListLayout()
          }
        }
      } else {
        step("Switch to list view") {
          ip.getListLayout()
        }
      }
    }

    stop()
  }

  def switchToSplitView(isTimed : Boolean = true)(implicit darkFeatures: DarkFeatures) {
    implicit val logger = (new TestLogger("switchToSplitView", personaName))
    import logger._

    goToIssuesPage |> { ip =>

      if (isTimed) {
        step("Do a new search") {
          ip.newSearch()
        }

        step("Switch to ListView") {
          ip.getListLayout()
        }

        step("Switch to SplitView") {
          timed(generateInteractionKey("switch to SplitView from ListView")) {
            ip.getSplitLayout()
          }
        }
      }
      else {
        step("Switch to SplitView") {
          ip.getSplitLayout()
        }
      }
    }

    stop()
  }

  def openIssueWithLargeNumberOfCommentsFromIssuesPage(isTimed: Boolean = true)(implicit darkFeatures: DarkFeatures) {
    implicit val logger = (new TestLogger("openIssueWithLargeNumberOfCommentsFromIssuesPage", personaName))
    import logger._

    start()
    goToIssuesPage |> { ip =>

      def run {
        if (useSplitView) {
          // SplitView mode: just do the search, we don't need to open the issue
          // because it is already there.
          ip.getAdvancedQuery().searchAndWait("issueKey = BULK-4")
        } else {
          // Non-SplitView: Do the search and then open the issue page.
          ip.getAdvancedQuery().searchAndWait("issueKey = BULK-4")
          ip.getResultsTable().openSelectedIssue(PSYCHO_READY)
        }
      }

      if (isTimed) {
        timed(generateInteractionKey("open issue with large comments"))(run)
      } else {
        run
      }
    }
    stop()
  }

  def createFilter(implicit darkFeatures: DarkFeatures) {
    implicit val logger = (new TestLogger("createFilter", personaName))
    import logger._

    start()

    goToIssuesPage |> { ip =>

      step("Reset the search") {
        ip.newSearch
      }

      val bq = step("Switch to basic query") {
        ip.getBasicQuery
      }

      timed(generateInteractionKey("create Filter Experience")) {
        ip.newSearch
        bq.project.setValueAndSubmit("QA", "Bulk Move 1")
        logStep("chosing issue type bug")
        bq.issueType.setValueAndSubmit("Bug")
        logStep("choosing issue types open and closed")
        bq.status.setValueAndSubmit("Open", "Closed")
        logStep("about to open filters dialog")
        bq.openAddCriteriaDialog.selectAndOpenResolutions.selectAndWait("Fixed")
        //add date
        waitForResultsTable
      }
    }

    stop()
  }

  def filterAndViewIssue()(implicit darkFeatures: DarkFeatures) {
    filterAndSelectIssue((idc: IssueDetailComponent) => idc.ready, "View Issue", "filterAndViewIssue")
  }

  def filterAndEditIssue()(implicit darkFeatures: DarkFeatures) {
    filterAndSelectIssue((idc: IssueDetailComponent) => idc.readyForEdit, "Edit Issue", "filterAndEditIssue")
  }

  def filterAndResolvedIssue()(implicit darkFeatures: DarkFeatures) {
    implicit val logger = (new TestLogger("filterAndResolvedIssue", personaName))
    import logger._

    start()

    goToIssuesPage |> {
      ip =>
        step("Do a new search") {
          ip.newSearch()
        }
        timed(generateInteractionKey("Filter and Select Issue to Resolve")) {

          logStep("Select fitler 'Bugs'");
          ip.getFilters.selectFavourite("Bugs")

          val issue = getIssueDetail(ip)

          logStep("Open Resolve Issue dialog");
          issue.opsBar.openResolveIssueDialog
        }
    }
    stop()
  }

  def close() {
    application.getTester.getDriver.quit()
  }

}
