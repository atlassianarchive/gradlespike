#!/bin/bash
set -u

# source the common functions
KA_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. "${KA_DIR}"/bin/release-include.sh || exit 1

# try to find Maven
if [ -f "${M2_HOME:-}/bin/mvn" ]; then
  MVN=${M2_HOME}/bin/mvn
elif [ -f "/opt/java/tools/maven/apache-maven-2.1.0/bin/mvn" ]; then
  MVN=/opt/java/tools/maven/apache-maven-2.1.0/bin/mvn
else
  MVN=mvn
fi

# the branches to release JIRA and KA from
KA_BRANCH="master"
JIRA_BRANCH="master"

function banner() {
    echo ">"
    echo "> $*"
    echo ">"
}

function print_usage() {
    echo "usage: release.sh [--dry-run] [--jira /path/to/jira]" >&2
}

function prepare_release() {
    local dry_run=$1
    local dir=$2
    local args=${3:-}

    local release_args='-DpushChanges=false -DlocalCheckout=true'
    if [ -n "${dry_run}" ]; then
        release_args="${release_args} -DdryRun=true"
    fi

    pushd "${dir}"
    ${DEBUG} "${MVN}" -B ${release_args} ${args} release:prepare || die "Failed to release:prepare in $dir"
    popd
}

function get_release_tag() {
    local dir=$1

    echo `cat "${dir}"/release.properties | grep "^scm.tag=" | cut -b 9-` # the SCM tag
}

function perform_release() {
    local dry_run=$1
    local rel_dir=$2
    local rel_tag=$3

    # try to use Bamboo's deployment credentials if available
    local settings=""
    if [ -f /opt/bamboo-agent/.m2/jira-deploy-settings.xml ]; then
        settings="-s /opt/bamboo-agent/.m2/jira-deploy-settings.xml"
    fi  # otherwise use default

    [ -n "${dry_run}" ] && { echo "Dry run requested, skipping perform_release of '${rel_tag}' in ${rel_dir}"; return; }
    pushd "${rel_dir}"
    git checkout "${rel_tag}" || die "Failed to checkout '${rel_tag}' in ${rel_dir}"
    ${DEBUG} "${MVN}" ${settings} -B clean deploy -Prelease -DskipTests -Darguments=-DskipTests || die "Failed to mvn deploy in ${rel_dir}"
    popd
}

function ka_get_jira_version() {
    echo `grep "<jira.version>.*</jira.version>" ${KA_DIR}/pom.xml | sed -e 's/.*<jira.version>\(.*\)<\/jira.version>.*/\1/'`
}

function ka_set_jira_version() {
    local jira_version=$1
    banner "Setting jira.version=${jira_version} in ${KA_DIR}"

    cat ${KA_DIR}/pom.xml | sed -e "s/\(.*<jira.version>\).*\(<\/jira.version>.*\)/\1${jira_version}\2/" > pom.xml.new || die "Failed to set jira.version in $KA_DIR"
    mv pom.xml.new pom.xml || die
    git add pom.xml || die
    git commit -m "release.sh: set JIRA version to ${jira_version}" || die
}

function ka_prepare_release() {
    local dry_run=$1
    banner "Preparing KA release"

    prepare_release "${dry_run}" "${KA_DIR}" "-Drelease.args=-DskipITs=true"
}

function ka_perform_release() {
    local dry_run=$1
    local ka_tag=$2
    banner "Performing KA release"
    perform_release "${dry_run}" "${KA_DIR}" "${ka_tag}"
}

function ka_push_changes() {
    local dry_run=$1
    local branch=$2
    local upstream=git@bitbucket.org:atlassian/jira-issue-nav-plugin.git
    banner "Push KA changes"

    [ -n "${dry_run}" ] && { echo "Dry run requested, skipping git-push for KA"; return; }
    ${DEBUG} git pull $upstream $branch || die "Failed to git-pull in $KA_DIR"
    ${DEBUG} git push --tags $upstream $branch || die "Failed to git-push in $KA_DIR"
}

function jira_clone() {
    local from=$1
    local to=$2
    local branch=$3
    banner "Cloning JIRA from ${from} (branch: ${branch})..."

    ${DEBUG} git clone -b "${branch}" "${from}" "${to}" || die "Failed to git-clone from $from"
}

function jira_print_version() {
    local jira_dir=$1
    local version=`grep "<version>.*</version>" "${jira_dir}"/pom.xml | head -2 | tail -1 | sed -e 's/.*<version>\(.*\)<\/version>.*/\1/'`
    [ -z "${version}" ] && die "Can't determine JIRA version"

    echo "${version}"
}

function jira_prepare_release() {
    local dry_run=$1
    local jira_dir=$2
    local version=$3
    banner "Prepare JIRA release ${version}"

    prepare_release "${dry_run}" "${jira_dir}" "-DreleaseVersion=${version} -DdevelopmentVersion=\${project.version} -Drelease.args=-DskipTests=true"
}

function jira_perform_release() {
    local dry_run=$1
    local jira_dir=$2
    local jira_tag=$3
    banner "Perform JIRA release"
    perform_release "${dry_run}" "${jira_dir}" "${jira_tag}"
}

function jira_push_changes {
    local dry_run=$1
    local jira_dir=$2
    local upstream=ssh://git@stash.atlassian.com:7997/JIRA/jira.git
    local branch="${JIRA_BRANCH}"
    banner "Push JIRA changes"
    [ -n "${dry_run}" ] && { echo "Dry run requested, skipping git-push for JIRA"; return; }

    local tries="0"
    local ret="1"

    ## attempt to pull-push a few times before failing
    pushd "${jira_dir}"
    while [ $ret -ne 0 -a $tries -lt 5 ]; do
        tries=$[$tries+1]

        banner "Pull JIRA changes (try: $tries)"
        ${DEBUG} git pull $upstream $branch; ret=$?
        [ $ret ] || continue

        banner "Push JIRA changes (try: $tries)"
        ${DEBUG} git push --tags $upstream $branch; ret=$?
        [ $ret ] || continue
    done

    [ $ret ] || die "Failed to git-pull or git-push in $jira_dir"
    popd
}

# get command line parameters
DEBUG=""
DRY_RUN=""
JIRA_DIR=""
while [ $# -ne 0 ] ; do
    arg=$1
    shift

    case $arg in
    "--dry-run")
        DRY_RUN="yes" ;;
    "--jira")
        JIRA_DIR="$1"; shift ;;
    "-d")
        DEBUG="echo" ;;
    *)
        print_usage; exit 1 ;;
    esac
done

# bail if no Maven
[ -x `which "${MVN}"` ] || { echo "Maven not found in \$PATH or \$M2_HOME}!" >&2; exit 1; }

# set a Git author if not already set
function set_git_author {
    GIT_AUTHOR_NAME=`git config --get user.name`
    GIT_AUTHOR_EMAIL=`git config --get user.email`
    if [ -z "$GIT_AUTHOR_NAME" -o -z "$GIT_AUTHOR_EMAIL" ]; then
        GIT_AUTHOR_NAME="JIRA Release Bot"
        GIT_AUTHOR_EMAIL="jira-release-bot@atlassian.com"
        echo "Setting git author to: $GIT_AUTHOR_NAME <$GIT_AUTHOR_EMAIL>"
        export GIT_AUTHOR_NAME
        export GIT_AUTHOR_EMAIL
    fi
}

# used to work around the fact that Bamboo gives us a detached HEAD
function git_attach_head() {
    local clone=$1
    local branch_name=$2

    # figure out the commit
    local head_branch=`git symbolic-ref -q HEAD`
    if [ -z "$head_branch" ]; then
        local head_commit=`git rev-parse HEAD`

        banner "Attaching commit $head_commit to branch $branch_name"
        # try co -b, then just co
        git checkout -b "${branch_name}" || {
          git checkout "${branch_name}" || die "error checking out ${branch_name}"
        }
        git reset --hard "${head_commit}" || die "error resetting to ${head_commit}"
    fi
}

function canonicalise() {
    local rel_path=$1

    cd -P -- "$(dirname -- "$rel_path")" && printf '%s\n' "$(pwd -P)/$(basename -- "$rel_path")"
}

# make sure there's an author
set_git_author

# figure out whether to release JIRA or not
KA_JIRA_VERSION=`ka_get_jira_version`
KA_USING_SNAPSHOT=`echo $KA_JIRA_VERSION | grep -- '-SNAPSHOT$'`

# release JIRA only if we have to
JIRA_RELEASE=''
if [ -n "$KA_USING_SNAPSHOT" ]; then
    # JIRA_DIR should be an absolute path
    if [ -z "${JIRA_DIR}" ]; then
        mkdir -p "${KA_DIR}/target"
        JIRA_DIR=`mktemp -d "${KA_DIR}/target/jira.XXX"`
        jira_clone "ssh://git@stash.atlassian.com:7997/JIRA/jira.git" "${JIRA_DIR}" "${JIRA_BRANCH}"
    else
        JIRA_DIR=`canonicalise "${JIRA_DIR}"`
    fi

    JIRA_VERSION=`jira_print_version "${JIRA_DIR}"`
    JIRA_RELEASE=${JIRA_VERSION%-SNAPSHOT}-`TZ=Australia/Sydney date "+%Y%m%d.%H%M%S"`
    echo "Releasing JIRA version $JIRA_RELEASE"

    git_attach_head "${JIRA_DIR}" "${JIRA_BRANCH}"
    jira_prepare_release "${DRY_RUN}" "${JIRA_DIR}" "${JIRA_RELEASE}"
    JIRA_TAG=`get_release_tag "${JIRA_DIR}"`
    jira_push_changes "${DRY_RUN}" "${JIRA_DIR}"
    jira_perform_release "${DRY_RUN}" "${JIRA_DIR}" "${JIRA_TAG}"

    if [ -n "${DRY_RUN}" ]; then
        echo "Dry run requested, JIRA was not deployed so I can't release KA" >&2
        exit 1
    fi
fi

# now we can cut a KA release
[ -n "${JIRA_RELEASE}" ] && ka_set_jira_version "${JIRA_RELEASE}"
git_attach_head "${KA_DIR}" "${KA_BRANCH}"
ka_prepare_release "${DRY_RUN}"
KA_TAG=`get_release_tag "${KA_DIR}"`
[ -n "${JIRA_RELEASE}" ] && ka_set_jira_version "${KA_JIRA_VERSION}"
ka_push_changes "${DRY_RUN}" "${KA_BRANCH}"
ka_perform_release "${DRY_RUN}" "${KA_TAG}"

