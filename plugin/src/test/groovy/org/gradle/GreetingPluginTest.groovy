package org.gradle

import org.gradle.GreetingTask
import org.junit.Test
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.api.Project
import static org.junit.Assert.*

class GreetingPluginTest {
    @Test
    public void greeterPluginAddsGreetingTaskToProject() {
        Project project = ProjectBuilder.builder().build()
        project.apply plugin: 'bamboo'

        assertTrue(project.tasks.hello instanceof GreetingTask)
    }
}
