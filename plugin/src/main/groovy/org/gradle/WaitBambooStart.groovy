package org.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

import java.util.concurrent.TimeUnit

class WaitBambooStart extends DefaultTask{

    @TaskAction
    def waitForBamboo() {
        String url = 'http://' + InetAddress.getLocalHost().getHostName() + ":6990/bamboo"


        int timeout = 120000
        final long end = System.nanoTime() + TimeUnit.MILLISECONDS.toNanos(timeout);
        boolean interrupted = false;
        boolean success = false;
        String lastMessage = "";

        // copied from AMPS
        while (!success && !interrupted && System.nanoTime() < end)
        {
            HttpURLConnection connection = null;
            try
            {
                URL urlToPing = new URL(url);
                connection = (HttpURLConnection) urlToPing.openConnection();
                int response = connection.getResponseCode();
                // Tomcat returns 404 until the webapp is up
                lastMessage = "Last response code is " + response;
                success = response < 400;
            }
            catch (IOException e)
            {
                lastMessage = e.getMessage();
                success = false;
            }
            finally
            {
                if (connection != null)
                {
                    try
                    {
                        connection.getInputStream().close();
                    }
                    catch (IOException e)
                    {
                        // Don't do anything
                    }
                }
            }

            if (!success)
            {
                getLog().info("Waiting for " + url);
                try
                {
                    Thread.sleep(1000);
                }
                catch (InterruptedException e)
                {
                    Thread.currentThread().interrupt();
                    interrupted = true;
                    break;
                }
            }
        }

        if (!success)
        {
            throw new RuntimeException(String.format("Bamboo didn't %s after %ds at %s. %s", "start", TimeUnit.MILLISECONDS.toSeconds(timeout), url, lastMessage));
        }
    }

}
