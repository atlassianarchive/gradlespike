package org.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class ExtractDependencies extends DefaultTask {

    @TaskAction
    def extractDependencies() {
        def providedModuleName = []
        project.getConfigurations().getByName('provided').each { dep2 ->
            providedModuleName += dep2.name
        }

        if(!project.hasProperty('extractDepencency') || !project.extractDependency){
            project.configurations.getByName('runtime').each{ dep ->
                if(!providedModuleName.contains(dep.name)){
                    ant.copy(file: dep.absoluteFile, todir: project.file('build/classes/main/META-INF/lib/') ){}
                }
            }
        }else {
            project.configurations.compile.files.each{dep ->
                if(!project.configurations.provided.contains(dep)) {
                    ant.exec(executable: "unzip") {
                        arg(value: '-q')
                        arg(value: '-d')
                        arg(value: project.file('build/classes/main'))
                        arg(value: dep)
                    }
                }
            }
        }

        // TODO also extract runtime dependencies
    }
}
