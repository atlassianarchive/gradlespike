package org.gradle

import org.gradle.api.plugins.cargo.tasks.local.LocalCargoContainerTask
import org.gradle.api.plugins.cargo.convention.Deployable

class StartBamboo extends LocalCargoContainerTask {

    StartBamboo(){
        Deployable deployable  = new Deployable()
        deployable.setFile(project.file('build/bamboo/atlassian-bamboo-web-app-5.5-m2.war'))
        deployable.setContext('bamboo')
        deployables = [deployable]

        homeDir = project.file('build/bamboo/tomcat/apache-tomcat-6.0.20')
        outputFile = project.file('build/output.log')
        jvmArgs = '-Xmx1024m -XX:MaxPermSize=256m'
        timeout = 0

        getSystemProperties().put('bamboo.home', project.file('build/bamboo/home/bamboo-plugin-test-resources-3.2.2-fix-1/bamboo-home/'))

        action = 'run'
        description = 'Starts the container, deploys WAR to it and wait for the user to press CTRL + C to stop.'

        extraClasspath = project.files('build/bamboo/hsqldb/')
    }

}
