package org.gradle.util

import org.gradle.api.artifacts.Dependency
import org.gradle.api.Project

/**
 * Created with IntelliJ IDEA.
 * User: xzhang
 * Date: 12/05/2014
 * Time: 5:28 PM
 * To change this template use File | Settings | File Templates.
 */
class DependencyHelper {

    static generateConfiguration(Project project, List<String> dependencyNotations, boolean transitive){
        def dep = []
        dependencyNotations.each{ notation ->
            dep += project.dependencies.create(notation)
        }

        def bambooConfig = project.configurations.detachedConfiguration(dep as Dependency[])
        bambooConfig.transitive = transitive
        bambooConfig
    }

}
