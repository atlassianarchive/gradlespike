package org.gradle.util

import com.google.common.collect.ImmutableMap
import com.sun.jersey.api.client.Client
import com.sun.jersey.api.client.ClientHandlerException
import com.sun.jersey.api.client.ClientRequest
import com.sun.jersey.api.client.ClientResponse
import com.sun.jersey.api.client.WebResource
import com.sun.jersey.api.client.config.ClientConfig
import com.sun.jersey.api.client.config.DefaultClientConfig
import com.sun.jersey.api.client.filter.ClientFilter
import com.sun.jersey.multipart.FormDataMultiPart
import org.apache.commons.codec.binary.Base64

import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.core.Response

/**
 * Created with IntelliJ IDEA.
 * User: xzhang
 * Date: 8/05/2014
 * Time: 4:16 PM
 * To change this template use File | Settings | File Templates.
 */
class Uploader {

    static final String PENDING_TASK_JSON = "application/vnd.atl.plugins.pending-task+json";

    public static void uploadFile(String serverUrl, File pluginFile, String username, String password) {
        ClientConfig config = new DefaultClientConfig();
        configureSSL(serverUrl, config);
        Client client = Client.create(config);
        client.addFilter(new BasicAuthFilter(username, password));
        client.setFollowRedirects(false);

        URI uri = URI.create(serverUrl);
        final FormDataMultiPart pluginPart = new FormDataMultiPart();
        pluginPart.field("plugin", pluginFile, MediaType.APPLICATION_OCTET_STREAM_TYPE);

        final MediaType multipartFormDataWithBoundary =
            new MediaType(MediaType.MULTIPART_FORM_DATA_TYPE.getType(),
                    MediaType.MULTIPART_FORM_DATA_TYPE.getSubtype(),
                    ImmutableMap.<String, String>builder().put("boundary", "some simple boundary").build());

        WebResource resource = client.resource(uri.toASCIIString());

        String token = "badtoken";
        ClientResponse tokenResponse = resource.head();
        token = tokenResponse.getMetadata().getFirst("upm-token");

        if (token == null) {
//            legacyUploadFile();
            throw new RuntimeException("Cannot get token from UPM")
            return;
        }

        ClientResponse acceptResponse = resource
                .queryParam("token", token)
                .type(multipartFormDataWithBoundary)
                .post(ClientResponse.class, pluginPart);

        try
        {
            waitForCompletion(client, acceptResponse);
        }
        catch (Exception ate)
        {
            throw new RuntimeException(" : Upload failed --- " + ate.getMessage());
        }
    }

    final static class BasicAuthFilter extends ClientFilter
    {
        private final String auth;

        BasicAuthFilter(String password, String username)
        {
            try
            {
                auth = "Basic " + new String(Base64.encodeBase64((username + ":" + password).getBytes("ASCII")));
            }
            catch (UnsupportedEncodingException e)
            {
                throw new RuntimeException("That's some funky JVM you've got there", e);
            }
        }

        public ClientResponse handle(ClientRequest cr) throws ClientHandlerException
        {
            MultivaluedMap<String, Object> headers = cr.getMetadata();
            if (!headers.containsKey(HttpHeaders.AUTHORIZATION))
            {
                headers.add(HttpHeaders.AUTHORIZATION, auth);
            }
            return getNext().handle(cr);
        }
    }

    public static void waitForCompletion(Client client, ClientResponse acceptResponse)
    {
        URI taskLocation = acceptResponse.getLocation();

        WebResource taskResource = client.resource(taskLocation);

        ClientResponse clientResponse = null;
        // set our limit to only 30 iterations before going out of the loop to prevent a possible infinite loop.
        for (int i = 0; i < 30; i++)
        {
            clientResponse = taskResource.accept(PENDING_TASK_JSON).get(ClientResponse.class);

            if (clientResponse.getStatus() != Response.Status.OK.getStatusCode())
            {
                println '-----------response -------------'
                println clientResponse
                break;
            }
            if (isTaskErrorRepresentation(clientResponse))
            {
                throw new RuntimeException(clientResponse.getEntity(String.class));
            }
            if (isTaskDone(clientResponse))
            {
                break;
            }
            try // sleep for a second, and retry to see if the task has completed
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException ex)
            {
            }
        }
    }


    private static void configureSSL(String url, ClientConfig config) throws Exception
    {
        // empty
    }

    private static boolean isTaskErrorRepresentation(ClientResponse response)
    {
        return response.getType().getSubtype().endsWith("err+json");
    }

    private static boolean isTaskDone(ClientResponse response)
    {
        return response.getType().getSubtype().endsWith("complete+json");
    }
}
