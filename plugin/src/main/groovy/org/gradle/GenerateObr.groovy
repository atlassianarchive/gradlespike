package org.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.artifacts.Configuration
import org.gradle.api.tasks.TaskAction
import org.gradle.mvn3.org.codehaus.plexus.util.FileUtils

// invoke bindex to generate obr file
class GenerateObr extends DefaultTask{


    @TaskAction
    def generateObr() {
        project.file('build/libs/obr/dependencies').mkdirs()
        FileUtils.copyFileToDirectory(project.file('build/libs/'+ project.name + '-' + project.version + '.jar'), project.file('build/libs/obr'))

        // use bundled dependency type for plugins which should be packaged in obr
        project.configurations.each{ config ->
            if('bundled'.equals(config.name)){
                project.configurations.bundled.files.each{ dep ->
                    FileUtils.copyFile(dep, project.file('build/lib/obr/dependencies/'))
                }
            }
        }

        ant.java(classname: 'org.osgi.impl.bundle.bindex.cli.Index', fork: true, dir: 'build/libs/obr', classpath: 'lib/org.osgi.impl.bundle.bindex.jar') {
            arg(value: project.name + '-' + project.version + '.jar')
            arg(value: 'dependencies/*.jar')
            arg(value: '-r')
            arg(value: 'obr.xml')
        }

        ant.jar(destFile: 'build/libs/' + project.name + '-' + project.version + '.obr', baseDir: 'build/libs/obr') {

        }
    }

}
