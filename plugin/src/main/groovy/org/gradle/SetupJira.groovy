package org.gradle

import org.gradle.api.tasks.TaskAction
import org.gradle.util.DependencyHelper

class SetupJira {

    @TaskAction
    def setupBamboo() {
        project.file('build/bamboo/home').mkdirs()

        def bambooData = DependencyHelper.generateConfiguration( project,
                ['com.atlassian.bamboo.plugins:jira-plugin-test-resources:6.2.2@zip'], false ).singleFile
        ant.unzip(src: bambooData, dest: 'build/jira/home', overwrite: false){}

        project.file('build/jira/tomcat/').mkdirs()

        def tomcat = DependencyHelper.generateConfiguration( project,
                ['org.apache.tomcat:apache-tomcat:6.0.20@zip'], false ).singleFile

        ant.unzip(src: tomcat, dest: 'build/jira/tomcat', overwrite: false){}

        def bambooWar = DependencyHelper.generateConfiguration( project,
                ['com.atlassian.bamboo:atlassian-bamboo-web-app:5.5-m2@war'], false ).singleFile

        ant.copy(file: bambooWar, todir: project.file('build/bamboo/')) {}

        project.file('build/bamboo/hsqldb/').mkdirs()

        def hsqldb = DependencyHelper.generateConfiguration(project,
                ['org.hsqldb:hsqldb:2.3.0'], false ).singleFile

        ant.copy(file: hsqldb, todir:project.file('build/bamboo/hsqldb')){}

        String hostname = InetAddress.getLocalHost().getHostName()
        ant.replace(file: project.file('build/bamboo/home/bamboo-plugin-test-resources-3.2.2-fix-1/bamboo-home/bamboo.cfg.xml'), token: '\${bambooHome}', value:project.file('build/bamboo/home/bamboo-plugin-test-resources-3.2.2-fix-1/bamboo-home'))
        ant.replace(file: project.file('build/bamboo/home/bamboo-plugin-test-resources-3.2.2-fix-1/bamboo-home//xml-data/configuration/administration.xml'), token:'http://localhost:8085', value: 'http://' + hostname + ':6990/bamboo')
    }
}
