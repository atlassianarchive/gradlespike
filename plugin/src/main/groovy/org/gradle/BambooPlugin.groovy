package org.gradle

import org.gradle.api.Project
import org.gradle.api.Plugin

class BambooPlugin implements Plugin<Project> {

    void apply(Project target) {
        target.plugins.apply("cargo")
        target.task('hello', type: GreetingTask)
        target.task('extractDependencies', type: ExtractDependencies, dependsOn: 'classes')
        target.task('deployPlugin', type: DeployPluginTask, dependsOn: ['extractDependencies', 'jar'])
        target.task('startBamboo', type: StartBamboo, dependsOn: 'setupBamboo')
        target.task('setupBamboo', type: SetupBambooTask)
        target.task('startBambooAsync', type: StartBambooAsync, dependsOn: 'setupBamboo')
        target.task('debugBamboo', type: DebugBamboo, dependsOn: 'setupBamboo')
        target.task('stopBamboo', type: StopBamboo)
        target.task('waitBambooStart', type: WaitBambooStart, dependsOn: 'startBambooAsync')
        target.task('installBundledTestPlugins', type: InstallBundledTestPlugins)
        target.task('generateObr', type: GenerateObr, dependsOn: 'jar')
        target.getTasksByName('jar', false).iterator().next().dependsOn.add('extractDependencies')

        target.cargo.containerId = 'tomcat6x'
        target.cargo.port = 6990;

    }
}
