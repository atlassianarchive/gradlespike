package org.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.gradle.util.Uploader

class DeployPluginTask extends DefaultTask {
    String serverUrl = 'http://'+ InetAddress.getLocalHost().getHostName() + ':6990/bamboo'
    String username = 'admin'
    String password = 'admin'
    File pluginFile = null;

    String UPM_ROOT_RESOURCE = '/rest/plugins/1.0/'

    @TaskAction
    def deployPlugin() {

        if(pluginFile == null)
            pluginFile = project.file('build/libs/' + project.name + "-" + project.getVersion() + '.jar')
        Uploader.uploadFile(serverUrl + UPM_ROOT_RESOURCE, pluginFile, username, password)
    }


}
