package org.gradle

import org.apache.commons.io.FileUtils
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.Upload
import org.gradle.util.Uploader

/**
 * Created with IntelliJ IDEA.
 * User: xzhang
 * Date: 8/05/2014
 * Time: 3:08 PM
 * To change this template use File | Settings | File Templates.
 */
class InstallBundledTestPlugins extends DefaultTask{

    String serverUrl = 'http://'+ InetAddress.getLocalHost().getHostName() + ':6990/bamboo'
    String UPM_ROOT_RESOURCE = '/rest/plugins/1.0/'
    String username = 'admin'
    String password = 'admin'

    @TaskAction
    def installBundleTestPlugin() {
        project.configurations.integTestBundle.each{dep->
            println dep
            Uploader.uploadFile(serverUrl + UPM_ROOT_RESOURCE, (File)dep, "admin", "admin")
        }
    }
}
