package org.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction


class PackagePluginTask extends DefaultTask {
    String greeting = 'hello from GreetingTask'

    @TaskAction
    def packagePlugin() {
        println greeting
    }
}